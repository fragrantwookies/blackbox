﻿using Advantech.Adam;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace AdamPlugin
{
    public class AdamSdk : IDisposable
    {
        private AdamSocket adamSocket;
        private Timer coilPollTimer;
        private bool running = false;

        public string IP { get; set; }
        public int Port { get; set; }
        public int ConnectTimeout { get; set; }
        public int SendReceiveTimeout { get; set; }
        public int PollFrequency { get; set; }

        private int numInputs;
        private int numOutputs;

        private bool[] lastData;

        private bool initialized = false;

        #region Error handling
        public class AdamErrorEventArgs : EventArgs
        {
            public string Message { get; set; }

            public Exception Exception { get; set; }

            public AdamErrorEventArgs()
            {
                Message = "";
                Exception = null;
            }

            public AdamErrorEventArgs(string _Message)
            {
                Message = _Message;
                Exception = null;
            }

            public AdamErrorEventArgs(string _Message, Exception _Exception)
            {
                Message = _Message;
                Exception = _Exception;
            }
        }

        public event EventHandler<EventArgs> Disconnected;

        public event EventHandler<AdamErrorEventArgs> Error;

        protected void OnError(string _Message, Exception _Exception = null)
        {
            if (Error != null)
                Error(this, new AdamErrorEventArgs(_Message, _Exception));
        }
        #endregion

        #region IOChanged
        public class IOChangedEventArgs : EventArgs
        {
            public int IONumber { get; set; }
            public bool State { get; set; }

            public IOChangedEventArgs() { }

            public IOChangedEventArgs(int _IONumber, bool _State)
            {
                IONumber = _IONumber;
                State = _State;
            }
        }

        public event EventHandler<IOChangedEventArgs> InputChanged;

        protected void OnInputChanged(int _IONumber, bool _State)
        {
            if (InputChanged != null)
                 InputChanged(this, new IOChangedEventArgs(_IONumber, _State));
        }

        public event EventHandler<IOChangedEventArgs> OutputChanged;

        protected void OnOutputChanged(int _IONumber, bool _State)
        {
            if (OutputChanged != null)
                OutputChanged(this, new IOChangedEventArgs(_IONumber, _State));
        }
        #endregion

        public AdamSdk()
        {
            ConnectTimeout = 2000;
            SendReceiveTimeout = 2000;
            PollFrequency = 500;
            coilPollTimer = new Timer();
            coilPollTimer.Elapsed += coilPollTimer_Elapsed;
        }

        #region Cleanup
        ~AdamSdk()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected void Dispose(bool disposing)
        {
            if (coilPollTimer != null)
            {
                coilPollTimer.Stop();
                coilPollTimer.Elapsed -= coilPollTimer_Elapsed;
                coilPollTimer.Dispose();
                coilPollTimer = null;
            }

            if (adamSocket != null)
            {
                try
                {
                    adamSocket.Disconnect();
                }
                catch{}
            }
            adamSocket = null;
        }
        #endregion

        [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Catch block is empty")]
        public void Connect()
        {
            try
            {
                if (adamSocket != null)
                    adamSocket.Disconnect();
            }
            catch{}

            if (adamSocket == null)
                adamSocket = new AdamSocket();
            adamSocket.SetTimeout((int)(ConnectTimeout / 3), SendReceiveTimeout, SendReceiveTimeout);
            adamSocket.Connect(IP, ProtocolType.Tcp, Port);

            if (adamSocket.Connected)
            {
                running = true;

                //if (coilPollTimer == null)
                //{
                //    coilPollTimer = new Timer();
                //    coilPollTimer.Elapsed -= coilPollTimer_Elapsed;
                //    coilPollTimer.Elapsed += coilPollTimer_Elapsed;

                //    //Poll();
                //}

                coilPollTimer.Stop();
                coilPollTimer.Interval = PollFrequency;
                coilPollTimer.Start();
            }
        }

        public void Disconnect()
        {
            running = false;

            if (adamSocket != null)
            {
                adamSocket.Disconnect();
                adamSocket = null;
            }

            if (coilPollTimer != null)
                coilPollTimer.Stop();
        }

        public bool Connected
        { get { return adamSocket != null ? adamSocket.Connected : false; } }

        private void coilPollTimer_Elapsed(object sender, ElapsedEventArgs e)
        {   
            try
            {
                coilPollTimer.Stop();

                Poll();
            }
            finally
            {
                if(running)
                    coilPollTimer.Start();
            }
        }

        private void Poll()
        {
            if ((running) && (adamSocket != null))
            {
                if (adamSocket.Connected)
                {
                    if (numInputs < 1)
                    {
                        GetAdamType();
                        lastData = new bool[numInputs + numOutputs];
                    }

                    lock (lastData)
                    {
                        bool[] diData = new bool[numInputs];
                        bool[] newData = new bool[numInputs + numOutputs];

                        try
                        {
                            if (adamSocket.Modbus().ReadCoilStatus(1, numInputs, out diData))
                            {
                                Array.Copy(diData, 0, newData, 0, numInputs);
                                if (numOutputs > 0)
                                {
                                    bool[] doData = new bool[numOutputs];
                                    if (adamSocket.Modbus().ReadCoilStatus(17, numOutputs, out doData))
                                        Array.Copy(doData, 0, newData, numInputs, numOutputs);
                                }
                                for (int i = 0; i < (numInputs); i++) //Inputs
                                {
                                    if (newData[i] != lastData[i])
                                    {
                                        if (initialized)
                                            OnInputChanged(i, newData[i]);

                                        lastData[i] = newData[i];
                                    }
                                }
                                for (int i = numInputs; i < (numInputs + numOutputs); i++) //Outputs
                                {
                                    if (newData[i] != lastData[i])
                                    {
                                        if (initialized)
                                            OnOutputChanged(i, newData[i]);

                                        lastData[i] = newData[i];
                                    }
                                }

                                initialized = true;
                            }
                            else
                            {
                                OnError(string.Format("{0}.{1}: Failed to read the current state of the inputs or outputs.", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
                            }
                        }
                        catch(Exception ex)
                        {
                            OnError(string.Format("{0}.{1}: Failed to read the current state of the inputs or outputs.", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), ex);

                            Disconnect();

                            if (Disconnected != null)
                                Disconnected(this, new EventArgs());
                        }
                    }
                }
                else
                {
                    Disconnect();

                    if (Disconnected != null)
                        Disconnected(this, new EventArgs());
                }
            }
        }

        /// <summary>
        /// We don't care about the exact type: This is only used to determine the number of inputs and outputs. For example, type 6060 can be 6066 or 6060w as well.
        /// </summary>
        private void GetAdamType()
        {
            if (adamSocket.Connected)
            {
                string moduleName = string.Empty;
                adamSocket.Configuration().GetModuleName(out moduleName);
                //adamSocket.Configuration().
                if (string.IsNullOrEmpty(moduleName))
                {// Suspected IP theft: Module is connected but not communicating.
                    //adamSocket.Configuration().
                    string apiError = "";
                    try
                    {
                        apiError = adamSocket.LastError.ToString();
                    }
                    catch (Exception)
                    {// Do nothing here: Already logging error. If we fail to get additional info from API, simply log the issue as always.
                    }
                    OnError(string.Format("{0}.{1}: Device type not found. API last error: " + apiError, 
                        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
                    moduleName = "6050";
                }
                if (moduleName.Contains("50"))
                {
                    numInputs = 12;
                    numOutputs = 6;
                }
                else if (moduleName.Contains("51"))
                {
                    numInputs = 12;
                    numOutputs = 2;
                }
                else if (moduleName.Contains("52"))
                {
                    numInputs = 8;
                    numOutputs = 8;
                }
                else if (moduleName.Contains("55"))
                {
                    numInputs = 18;
                    numOutputs = 0;
                }
                else if (moduleName.Contains("606"))
                {
                    numInputs = 6;
                    numOutputs = 6;
                }
                //lastData = new bool[numInputs + numOutputs];
            }
            else
            {// Unexpected disconnection
                OnError(string.Format("{0}.{1}: Device connection lost.", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name));
            }
        }

        public int NumOutputs
        {
            get { return numOutputs; }
        }

        public int NumInputs
        {
            get { return numInputs; }
        }
    }
}
