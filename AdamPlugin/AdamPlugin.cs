﻿using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;

namespace AdamPlugin
{
    public class AdamPlugin : INerveEventPlugin, IDisposable
    {
        AdamSdk adamSDK;

        public event EventHandler<PluginErrorEventArgs> Error;
        public event EventHandler<PluginTriggerEventArgs> Trigger;                                                                  
        public event EventHandler DeviceOnline;                                                                  
        public event EventHandler DeviceOffline;

        #region Dispose
        ~AdamPlugin()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (adamSDK != null)
            {
                adamSDK.Disconnect();
                adamSDK.InputChanged -= adamSDK_InputChanged;
                adamSDK.Error -= adamSDK_Error;
                adamSDK.Dispose();
            }
            adamSDK = null;

            Online = false;
            if (DeviceOffline != null)
                DeviceOffline(this, new EventArgs());
        }
        #endregion

        public bool Enabled { get; set; }

        public bool Online { get; set; }
        private DateTime onlineSince { get; set; }

        private string detectorNamePrefix;

        #region Properties
        private string fileName;

        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public string Description
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
            }
        }

        public string Author
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company;
            }
        }

        public string Version
        {
            get
            {
                return Assembly.GetCallingAssembly().GetName().Version.ToString();
            }
        }

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public string[] EventNames
        {
            get 
            {
                if (adamSDK != null)
                {
                    string[] result = new string[adamSDK.NumInputs + adamSDK.NumOutputs];

                    for (int i = 0; i < adamSDK.NumInputs; i++)
                    {
                        result[i] = LookupInputEventName(i);
                    } 
                    
                    for (int i = 0; i < adamSDK.NumOutputs; i++)
                    {
                        result[i] = LookupOutputEventName(i);
                    }

                    return result;
                }
                else
                    return new string[0]; 
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        #region CustomProperties
        private CustomProperties properties;
        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }

        //public string[] CustomPropertyNames
        //{
        //    get
        //    {
        //        return new string[] { "IP", "Port", "ConnectTimeout", "SendReceiveTimeout", "PollFrequency" };
        //    }
        //}

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                result.Add(new CustomPropertyDefinition("IP", "IP Address", "IP Address", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Port", "Port", "Port", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("ConnectTimeout", "Connect Timeout", "Connect Timeout", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("SendReceiveTimeout", "Send Receive Timeout", "Send Receive Timeout", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("PollFrequency", "Poll Frequency", "Poll Frequency", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("DetectorNamePrefix", "Detector Name Prefix", "Detector name will be built by concatenating the prefix and detector index", typeof(CustomProperty)));
                return result;
            }
        }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }
        #endregion
        #endregion

        public void Start()
        {            
            if (adamSDK == null)
            {
                adamSDK = new AdamSdk();
                adamSDK.InputChanged += adamSDK_InputChanged;
                adamSDK.Error += adamSDK_Error;
                //adamSDK.Disconnected += adamSDK_Disconnected;
            }

            adamSDK.Disconnected -= adamSDK_Disconnected;

            //if (adamSDK.Connected)
            adamSDK.Disconnect();

            adamSDK.IP = Properties["IP"];
            adamSDK.Port = int.Parse(Properties["Port"]);

            string err = "";
            int parsedVal = 0;

            if (TimeSpanParser.TryParse(Properties["ConnectTimeout"], out parsedVal, out err))
                adamSDK.ConnectTimeout = parsedVal;
            else if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing ConnectTimeout: {0}", err), null));

            if (TimeSpanParser.TryParse(Properties["SendReceiveTimeout"], out parsedVal, out err))
                adamSDK.SendReceiveTimeout = parsedVal;
            else if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing SendReceiveTimeout: {0}", err), null));

            if (TimeSpanParser.TryParse(Properties["PollFrequency"], out parsedVal, out err))
                adamSDK.PollFrequency = parsedVal;
            else if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing PollFrequency: {0}", err), null));

            if (!string.IsNullOrWhiteSpace(Properties["DetectorNamePrefix"]))
                detectorNamePrefix = Properties["DetectorNamePrefix"];
            else
                detectorNamePrefix = "Input";

            adamSDK.Connect();

            Thread.Sleep(100);

            bool oldstate = Online;
            Online = adamSDK != null && adamSDK.Connected;            

            if (!oldstate && Online)
            {
                onlineSince = DateTime.Now;

                if (DeviceOnline != null)
                    DeviceOnline(this, new EventArgs());
            }

            adamSDK.Disconnected += adamSDK_Disconnected;
        }

        void adamSDK_Disconnected(object sender, EventArgs e)
        {
            if (Online && (DateTime.Now - onlineSince).Milliseconds > 1000 && (DeviceOffline != null))
                DeviceOffline(this, new EventArgs());

            Online = false;
        }

        private void adamSDK_Error(object sender, AdamSdk.AdamErrorEventArgs e)
        {
            if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), e.Message, e.Exception));
        }

        private void adamSDK_InputChanged(object sender, AdamSdk.IOChangedEventArgs e)
        {
            if (Trigger != null)
                Trigger(this, new PluginTriggerEventArgs(new AdamTrigger()
                {
                    TriggerID = Guid.NewGuid(),
                    DeviceName = this.Name,
                    DetectorGroupIndex = 0,
                    DetectorIndex = e.IONumber, 
                    DetectorName = string.Format("{0}{1}", detectorNamePrefix, e.IONumber.ToString()),
                    TriggerDT = DateTime.Now,
                    EventName = LookupInputEventName(e.IONumber),
                    DetectorType = DetectorType.Digital,
                    State = e.State,
                    Data = XMLSerializer.Serialize(e)
                }));
        }

        private string LookupInputEventName(int _IONumber)
        {
            string result = Properties[string.Format("Input{0}", _IONumber.ToString())];

            return string.IsNullOrWhiteSpace(result) ? string.Format("Input{0}", _IONumber.ToString()) : result;
        }

        private string LookupOutputEventName(int _IONumber)
        {
            string result = Properties[string.Format("Output{0}", _IONumber.ToString())];

            return string.IsNullOrWhiteSpace(result) ? string.Format("Output{0}", _IONumber.ToString()) : result;
        }

        public bool Equals(INervePlugin other)
        {
            if (Author == other.Author)
                if (Description == other.Description)
                    if (FileName == other.FileName)
                        if (Host == other.Host)
                            if (Name == other.Name)
                                if (Online == other.Online)
                                    if (Version == other.Version)
                                        return true;
            return false;
        }
    }
}