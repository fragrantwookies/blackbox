using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AdamPlugin
{
    public class AdamTrigger : IEventTrigger
    {
        protected Guid eventID;
        protected string name = string.Empty;
        protected Guid triggerID;
        protected int detectorGroupIndex = -1;
        protected int detectorIndex = -1;
        protected string detectorName;
        protected string deviceName;
        protected DetectorType detectorType = DetectorType.Unknown;
        protected string location = "";
        protected string mapName = "";
        protected bool state = true;
        protected double eventValue = 0;
        protected DateTime firstTriggerDT;
        protected DateTime triggerDT;
        protected string data = "";

        #region Properties
        public Guid EventID
        {
            get { return eventID; }
            set { eventID = value; PropertyChangedHandler("EventID"); }
        }

        public Guid TriggerID
        {
            get { return triggerID; }
            set { triggerID = value; }
        }

        public int DetectorGroupIndex
        {
            get { return detectorGroupIndex; }
            set { detectorGroupIndex = value; PropertyChangedHandler("DetectorGroupIndex"); }
        }

        public int DetectorIndex
        {
            get { return detectorIndex; }
            set { detectorIndex = value; PropertyChangedHandler("DetectorIndex"); }
        }

        public string DetectorName
        {
            get { return detectorName; }
            set { detectorName = value; PropertyChangedHandler("DetectorName"); }
        }

        public string DeviceName
        {
            get { return deviceName; }
            set { deviceName = value; PropertyChangedHandler("DeviceName"); }
        }

        public DetectorType DetectorType
        {
            get { return detectorType; }
            set { detectorType = value; PropertyChangedHandler("DetectorType"); }
        }

        public string Name
        {
            get { return name; }
            set { name = value; PropertyChangedHandler("Name"); }
        }

        public string EventName
        {
            get { return Name; }
            set { Name = value; PropertyChangedHandler("EventName"); }
        }

        public string Location
        {
            get { return location; }
            set { location = value; PropertyChangedHandler("Location"); }
        }

        public string MapName
        {
            get { return mapName; }
            set { mapName = value; PropertyChangedHandler("MapName"); }
        }

        public bool State
        {
            get { return state; }
            set { state = value; PropertyChangedHandler("State"); }
        }

        public double EventValue
        {
            get { return eventValue; }
            set { eventValue = value; PropertyChangedHandler("EventValue"); }
        }

        public DateTime TriggerDT
        {
            get { return triggerDT; }
            set { triggerDT = value; PropertyChangedHandler("TriggerDT"); }
        }

        public string Data
        {
            get { return data; }
            set { data = value; ; PropertyChangedHandler("Data"); }
        }
        #endregion Properties

        #region Compare
        public int CompareTo(IEventTrigger other)
        {
            if (this.DeviceName == other.DeviceName)
                return this.DetectorName.CompareTo(other.DetectorName);
            else
                return this.DeviceName.CompareTo(other.DeviceName);
        }

        public int Compare(IEventTrigger x, IEventTrigger y)
        {
            if (x.DeviceName == y.DeviceName)
                return x.DetectorName.CompareTo(y.DetectorName);
            else
                return x.DeviceName.CompareTo(y.DeviceName);
        }

        public bool Equals(IEventTrigger other)
        {
            return this.TriggerID.Equals(other.TriggerID);
        }

        public bool Equals(Guid other)
        {
            return this.TriggerID.Equals(other);
        }
        #endregion Compare

        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }
    }
}
