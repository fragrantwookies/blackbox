﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Controls;

namespace WPFMapControl
{
    public class ImageFlasher : IDisposable
    {
        private Image img;
        private Image alertIcon;

        private static System.Windows.Threading.DispatcherTimer timer;
        private static EventHandler elapsedEventHandler;

        private static List<ImageFlasher> flashers;

        private int flashes;
        private bool visible = true;

        //public event EventHandler Stopped;

        public static void Flash(Image _img, int _flashes = 10)
        {
            if (flashers == null)
                flashers = new List<ImageFlasher>();

            flashers.Add(new ImageFlasher(_img, _flashes));

            if (timer == null)
            {
                timer = new System.Windows.Threading.DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(500);
                if (elapsedEventHandler == null)
                    elapsedEventHandler = new EventHandler(timer_Elapsed);
                timer.Tick += elapsedEventHandler;
            }

            if (!timer.IsEnabled)
                timer.Start();
        }

        /// <summary>
        /// Create one for image you'd like to flash.
        /// </summary>
        /// <param name="_img">Image control to flash</param>
        /// <param name="_flashes">How many times do you want to flash the icon</param>
        public ImageFlasher(Image _img, int _flashes = 10)
        {
            img = _img;

            alertIcon = new Image();
            alertIcon.Name = img.Name + "_Alert";
            alertIcon.Source = Properties.Resources.Alert_Icon.ToWpfBitmap();
            alertIcon.Width = img.Width;
            alertIcon.Height = img.Height;
            alertIcon.Visibility = System.Windows.Visibility.Hidden;

            Canvas.SetLeft(alertIcon, Canvas.GetLeft(img));
            Canvas.SetTop(alertIcon, Canvas.GetTop(img));

            if (img.Parent is Canvas)
            {
                ((Canvas)img.Parent).Children.Add(alertIcon);
            }

            flashes = _flashes;            
        }

        ~ImageFlasher()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool Disposing)
        {
            if (Disposing)
                GC.SuppressFinalize(this);

            //if (timer != null)
            //{
            //    timer.Stop();
            //    if (elapsedEventHandler != null)
            //        timer.Elapsed -= elapsedEventHandler;
            //    timer.Dispose();
            //    timer = null;
            //}

            try
            {
                if (alertIcon != null && alertIcon.Parent != null)
                {
                    if (alertIcon.Parent is Canvas)
                        ((Canvas)alertIcon.Parent).Children.Remove(alertIcon);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            alertIcon = null;
        }

        protected static void timer_Elapsed(object sender, EventArgs e)
        {
            timer.Stop();

            for (int i = flashers.Count - 1; i >= 0; i--)
            {
                ImageFlasher flasher = flashers[i];
                flasher.SetVisibility();

                if (flasher.flashes <= 0)
                {
                    flashers.Remove(flasher);
                    flasher.Dispose();
                }
            }

            if (flashers.Count > 0)
                timer.Start();
            else
            {
                if (timer != null)
                {
                    timer.Stop();
                    if (elapsedEventHandler != null)
                        timer.Tick -= elapsedEventHandler;
                    //timer.Dispose();
                    timer = null;
                }
            }
        }

        protected void SetVisibility()
        {
            if (visible)
            {
                img.Visibility = System.Windows.Visibility.Collapsed;
                alertIcon.Visibility = System.Windows.Visibility.Visible;                
            }
            else
            {
                alertIcon.Visibility = System.Windows.Visibility.Collapsed;
                img.Visibility = System.Windows.Visibility.Visible;                
            }

            visible = !visible;
            flashes--;

            if (flashes <= 0)
            {
                img.Visibility = System.Windows.Visibility.Visible;
                alertIcon.Visibility = System.Windows.Visibility.Hidden;

                visible = true;

                //if (Stopped != null)
                //    Stopped(this, new EventArgs());
            }

            
            if (img.Parent is Canvas)
            {
                ((Canvas)img.Parent).UpdateLayout();
            }
        }
    }
}
