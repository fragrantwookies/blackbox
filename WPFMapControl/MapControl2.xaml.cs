﻿using DrawToolsLib;
using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace WPFMapControl
{
    /// <summary>
    /// Interaction logic for MapControl2.xaml
    /// </summary>
    public partial class MapControl2 : UserControl, IDisposable
    {
        #region Fields
        public string CurrentMapName { get; set; }
        private MapDoc mapDoc;
        public MapDoc Map_Doc { get { return mapDoc; } set { mapDoc = value; } }

        private DevicesDoc devicesDoc;
        public DevicesDoc Devices_Doc { get { return devicesDoc; } set { devicesDoc = value; } }

        private List<Image> thumbnailImages;
        private List<Image> detectorImages;

        protected List<string> currentMapTree;

        protected List<IconStoryboard> IconStoryboards;
        #endregion Fields

        #region PTZ Fields
        /// <summary>
        /// Defines the current state of the mouse handling logic.
        /// </summary>
        public enum MouseHandlingMode
        {
            /// <summary>
            /// Not in any special mode.
            /// </summary>
            None,

            /// <summary>
            /// The user is left-dragging elements with the mouse.
            /// </summary>
            DraggingElements,

            /// <summary>
            /// The user is left-mouse-button-dragging to pan the viewport.
            /// </summary>
            Panning,

            /// <summary>
            /// The user is holding down shift and left-clicking or right-clicking to zoom in or out.
            /// </summary>
            Zooming,

            /// <summary>
            /// The user is holding down shift and left-mouse-button-dragging to select a region to zoom to.
            /// </summary>
            DragZooming,

            Drawing
        }

        private MouseHandlingMode mouseHandlingMode = MouseHandlingMode.None;
        private MouseButton mouseButtonDown;
        private Point origZoomAndPanControlMouseDownPoint;
        private Point origContentMouseDownPoint;
        private Rect prevZoomRect;
        private double prevZoomScale;
        private bool prevZoomRectSet = false;
        #endregion PTZ Fields

        #region Events
        public event EventHandler OnMapLoaded;
        public event EventHandler OnRequestSave;

        public class RelaySwitchMessageEventArgs : EventArgs
        {
            public RelaySwitchMessageEventArgs(RelaySwitchMessage relaySwitchMessage)
                : base()
            {
                RelaySwitchMessage = relaySwitchMessage;
            }

            public RelaySwitchMessage RelaySwitchMessage { get; set; }
        }
        public event EventHandler<RelaySwitchMessageEventArgs> RelaySwitchMessage;

        public class SnapshotRequestEventArgs : EventArgs
        {
            public SnapshotRequestEventArgs(CameraImage cameraImage)
                : base()
            {
                CameraImage = cameraImage;
            }

            public CameraImage CameraImage { get; set; }
        }
        public event EventHandler<SnapshotRequestEventArgs> SnapshotRequest;
        #endregion Events

        #region Constructors
        public MapControl2()
        {
            InitializeComponent();

            currentMapTree = new List<string>();
        }
        #endregion Constructors

        #region Cleanup
        ~MapControl2()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (mapDoc != null)
            {
                mapDoc = null;
            }

            if (currentMapTree != null)
                currentMapTree.Clear();

            //ClearIconStoryboards();
        }

        public void RemoveImages()
        {
            //ClearIconStoryboards();

            try
            {
                if (Dispatcher.CheckAccess())
                    drawingCanvas.GraphicsList.Clear();
                else
                    Dispatcher.Invoke(delegate() { RemoveImages(); });
            }
            catch(Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Cleanup

        #region Load
        /// <summary>
        /// Load the background image for the map. This function will call LoadDevices.
        /// </summary>
        /// <param name="_mapName">Specify the name of the map you would like to load. If you don't specify a name, the home map will be loaded.</param>
        public void LoadMap(string _mapName = "")
        {
            try
            {
                if (CheckAccess())
                {
                    CurrentMapName = _mapName;

                    RemoveImages();

                    if (mapDoc == null) //preferably not, should get it from server
                    {
                        mapDoc = new MapDoc("Maps");
                        mapDoc.Load();
                    }

                    if (mapDoc != null)
                    {
                        XmlNode mapNode = null;
                        if (!string.IsNullOrWhiteSpace(_mapName))
                            mapNode = mapDoc.GetMap(_mapName);

                        if (mapNode == null)
                        {
                            mapNode = mapDoc.GetHomeMap();
                            CurrentMapName = mapNode.Name;
                        }

                        if (mapNode != null)
                        {
                            if (mapNode["BackgoundImage"] != null)
                            {
                                LoadBackgroud(mapNode["BackgoundImage"].Value);

                                ZoomToExtent();

                                bool add = true;
                                if (currentMapTree.Count > 0)
                                    if (currentMapTree[currentMapTree.Count - 1] == CurrentMapName)
                                        add = false;

                                if (add)
                                    currentMapTree.Add(CurrentMapName);

                                LoadChildMaps(CurrentMapName);

                                LoadDevices(CurrentMapName);
                            }
                        }
                    }

                    if (OnMapLoaded != null)
                        OnMapLoaded(this, new EventArgs());
                }
                else
                {
                    Dispatcher.Invoke(delegate() { LoadMap(_mapName); });
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void LoadBackgroud(string imageFilePath)
        {
            try
            {
                if (drawingCanvas.CheckAccess())
                {
                    string baseDir = Assembly.GetExecutingAssembly().Location;
                    BitmapImage img = new BitmapImage(new Uri(new Uri(baseDir), imageFilePath));
                    drawingCanvas.Width = img.Width;
                    drawingCanvas.Height = img.Height;
                    drawingCanvas.Background = new ImageBrush(img);
                }
                else
                {
                    drawingCanvas.Dispatcher.Invoke(delegate() { LoadBackgroud(imageFilePath); });
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void LoadChildMaps(string _mapName)
        {
            try
            {
                if (CheckAccess())
                {
                    XmlNode mapNode = mapDoc.GetMap(_mapName);
                    if (mapNode != null && mapNode["Maps"] != null)
                    {
                        foreach (XmlNode childMapNode in mapNode["Maps"])
                        {
                            string currentMapName = _mapName + "." + childMapNode.Name;
                            string currentMapTitle = childMapNode["Title"] != null ? childMapNode["Title"].Value : currentMapName;

                            XmlNode thumbnailNode = childMapNode["ThumbnailImage"];

                            if (thumbnailNode != null)
                            {
                                BitmapSource imgS = null;

                                double imageX = 0;
                                double imageY = 0;
                                double imageWidth = 30;
                                double imageHeight = 30;
                                double rotation = 0;

                                string imgPath = thumbnailNode.Value;

                                Uri fullpath = null;
                                try
                                {
                                    fullpath = new Uri(new Uri(Assembly.GetExecutingAssembly().Location), imgPath);
                                }
                                catch (Exception ex)
                                {
                                    fullpath = null;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Could not construct image file path.", ex);
                                }

                                if (!string.IsNullOrWhiteSpace(imgPath) && System.IO.File.Exists(fullpath != null ? fullpath.LocalPath : ""))
                                    imgS = new BitmapImage(fullpath);
                                else
                                    imgS = Properties.Resources.Placeholder.ToWpfBitmap();

                                if (thumbnailNode.Attribute("Left") != null)
                                    double.TryParse(thumbnailNode.Attribute("Left").Value, out imageX);
                                if (thumbnailNode.Attribute("Top") != null)
                                    double.TryParse(thumbnailNode.Attribute("Top").Value, out imageY);
                                if (thumbnailNode.Attribute("Width") != null)
                                    double.TryParse(thumbnailNode.Attribute("Width").Value, out imageWidth);
                                if (thumbnailNode.Attribute("Height") != null)
                                    double.TryParse(thumbnailNode.Attribute("Height").Value, out imageHeight);

                                if (thumbnailNode.Attribute("Rotation") != null)
                                    double.TryParse(thumbnailNode.Attribute("Rotation").Value, out rotation);

                                if (imgS != null)
                                {
                                    GraphicsPicture img = new GraphicsPicture(imageX, imageY, imageX + imageWidth, imageY + imageHeight, rotation, imgS, drawingCanvas.ActualScale);
                                    drawingCanvas.GraphicsList.Add(img);
                                    //drawingCanvas.CaptureMouse();

                                    ToolTip tooltip = new ToolTip() { Opacity = 0.8, Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint, HasDropShadow = true };
                                    tooltip.Content = currentMapTitle;
                                    img.Tooltip = tooltip;

                                    //Image imageControl = new Image();

                                    //imageControl.Name = currentMapName.Replace('.', '_');
                                    //imageControl.Tag = thumbnailNode;

                                    //ToolTip tooltip = new ToolTip();
                                    //tooltip.Content = currentMapTitle;
                                    //imageControl.ToolTip = tooltip;

                                    //drawingCanvas.Children.Add(imageControl);
                                    //imageControl.Source = imgS;
                                    //Canvas.SetLeft(imageControl, imageX);
                                    //Canvas.SetTop(imageControl, imageY);

                                    //imageControl.Width = imageWidth;
                                    //imageControl.Height = imageHeight;

                                    //if (rotation > 0)
                                    //    imageControl.RenderTransform = new RotateTransform(rotation, imageWidth / 2, imageHeight / 2);

                                    ////imageControl.MouseDown += imageControl_MouseDown;
                                    ////imageControl.MouseUp += imageControl_MouseUp;
                                    ////imageControl.MouseMove += imageControl_MouseMove;

                                    //if (thumbnailImages == null)
                                    //    thumbnailImages = new List<Image>();

                                    //thumbnailImages.Add(imageControl);
                                }
                            }
                        }
                    }
                }
                else
                {
                    Dispatcher.Invoke(delegate() { LoadChildMaps(_mapName); });
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Load

        #region Load Devices
        public void LoadDevices(string _mapName)
        {
            try
            {
                if (devicesDoc != null)
                {
                    foreach (XmlNode deviceNode in devicesDoc)
                    {
                        if (deviceNode.Name != "Maps")
                        {
                            if (deviceNode.Name == "Cameras")
                            {
                                foreach (XmlNode cameraNode in deviceNode)
                                {
                                    LoadCamera(cameraNode, _mapName);
                                }
                            }
                            else
                            {
                                if (deviceNode["Groups"] != null)
                                {
                                    foreach (XmlNode groupNode in deviceNode["Groups"])
                                    {
                                        foreach (XmlNode ioNode in groupNode)
                                        {
                                            DevicesDoc.DeviceIOType ioType = DevicesDoc.ParseIOType(ioNode);
                                            if (ioType == DevicesDoc.DeviceIOType.AnalogInput || ioType == DevicesDoc.DeviceIOType.DigitalInput || ioType == DevicesDoc.DeviceIOType.FibreInput)
                                            {
                                                //foreach (XmlNode detectorNode in ioNode)
                                                LoadDetector(deviceNode.Name, ioNode, _mapName);
                                            }
                                            else if (ioType == DevicesDoc.DeviceIOType.RelayOut)
                                            {
                                                LoadRelay(deviceNode.Name, ioNode, _mapName);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void LoadDetector(string _deviceName, XmlNode _detectorNode, string _mapName)
        {
            try
            {
                if (CheckAccess())
                {
                    if (_detectorNode != null)
                    {
                        GraphicsPicture img = LoadMapIcon(_detectorNode["Image"], _mapName);
                        if (img != null)
                        {
                            img.Name = string.Format("Detector_{0}_{1}", _deviceName, XmlNode.ParseString(_detectorNode["Name"], ""));
                            img.Tag = _detectorNode;

                            ToolTip tooltip = new ToolTip() { Opacity = 0.8, Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint, HasDropShadow = true };
                            Detector ttDetector = new Detector(_detectorNode) { DeviceName = _deviceName};
                            tooltip.Content = ttDetector;
                            DataTemplate dt = (DataTemplate)this.FindResource("templateDetectorTooltip");
                            tooltip.ContentTemplate = dt;
                            img.Tooltip = tooltip;
                        }

                        //Image imageControl = LoadMapIcon(_detectorNode["Image"], _mapName);

                        //if (imageControl != null)
                        //{
                        //    imageControl.Name = string.Format("Detector_{0}_{1}", _deviceName, XmlNode.ParseString(_detectorNode["Name"], ""));
                        //    imageControl.Tag = _detectorNode;

                        //    ToolTip tooltip = new ToolTip();
                        //    tooltip.Opacity = 0.8;
                        //    tooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.Left;
                        //    Detector ttDetector = new Detector(_detectorNode);
                        //    ttDetector.DeviceName = _deviceName;
                        //    tooltip.Content = ttDetector;
                        //    DataTemplate dt = (DataTemplate)this.FindResource("templateDetectorTooltip");
                        //    tooltip.ContentTemplate = dt;
                        //    imageControl.ToolTip = tooltip;
                        //}
                    }
                }
                else
                    Dispatcher.Invoke(delegate() { LoadDetector(_deviceName, _detectorNode, _mapName); });
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void LoadRelay(string _deviceName, XmlNode _relayNode, string _mapName)
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (_relayNode != null)
                    {
                        GraphicsPicture img = LoadMapIcon(_relayNode["Image"], _mapName);
                        if (img != null)
                        {
                            img.Name = string.Format("Relay_{0}_{1}", _deviceName, XmlNode.ParseString(_relayNode["Name"], ""));
                            img.Tag = _relayNode;

                            ToolTip tooltip = new ToolTip() { Opacity = 0.8, Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint, HasDropShadow = true };
                            RelayNodeWrapper ttRelay = new RelayNodeWrapper(_relayNode) { DeviceName = _deviceName };
                            tooltip.Content = ttRelay;
                            DataTemplate dt = (DataTemplate)this.FindResource("templateRelayTooltip");
                            tooltip.ContentTemplate = dt;
                            img.Tooltip = tooltip;

                            ContextMenu cmenu = new System.Windows.Controls.ContextMenu() { Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint, HasDropShadow = true };
                            MenuItem mItem = new MenuItem() { Header = "Switch On", Tag = new RelaySwitchMessage(ttRelay.DeviceMac, ttRelay.DeviceName, ttRelay.Name, true) };
                            mItem.Click += RelayContextMenuItem_Click;
                            cmenu.Items.Add(mItem);

                            mItem = new MenuItem() { Header = "Switch Off", Tag = new RelaySwitchMessage(ttRelay.DeviceMac, ttRelay.DeviceName, ttRelay.Name, false) };
                            mItem.Click += RelayContextMenuItem_Click;
                            cmenu.Items.Add(mItem);

                            mItem = new MenuItem() { Header = "On for 2 seconds", Tag = new RelaySwitchMessage(ttRelay.DeviceMac, ttRelay.DeviceName, ttRelay.Name, true, 2000) };
                            mItem.Click += RelayContextMenuItem_Click;
                            cmenu.Items.Add(mItem);

                            img.ContextMenu = cmenu;
                        }

                        //Image imageControl = LoadMapIcon(_relayNode["Image"], _mapName);

                        //if (imageControl != null)
                        //{
                        //    imageControl.Name = string.Format("Relay_{0}_{1}", _deviceName, XmlNode.ParseString(_relayNode["Name"], ""));
                        //    imageControl.Tag = _relayNode;

                        //    ToolTip tooltip = new ToolTip();
                        //    tooltip.Opacity = 0.8;
                        //    tooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.Left;
                        //    RelayNodeWrapper relay = new RelayNodeWrapper(_relayNode);
                        //    relay.DeviceName = _deviceName;
                        //    tooltip.Content = relay;
                        //    DataTemplate dt = (DataTemplate)this.FindResource("templateRelayTooltip");
                        //    tooltip.ContentTemplate = dt;
                        //    imageControl.ToolTip = tooltip;

                        //    ContextMenu cmenu = new System.Windows.Controls.ContextMenu();
                        //    MenuItem mItem = new MenuItem();
                        //    mItem.Header = "Switch On";
                        //    mItem.Click += RelayContextMenuItem_Click;
                        //    mItem.Tag = new RelaySwitchMessage(relay.DeviceMac, relay.DeviceName, relay.Name, true);
                        //    cmenu.Items.Add(mItem);

                        //    mItem = new MenuItem();
                        //    mItem.Header = "Switch Off";
                        //    mItem.Click += RelayContextMenuItem_Click;
                        //    mItem.Tag = new RelaySwitchMessage(relay.DeviceMac, relay.DeviceName, relay.Name, false);
                        //    cmenu.Items.Add(mItem);

                        //    mItem = new MenuItem();
                        //    mItem.Header = "On for 2 seconds";
                        //    mItem.Click += RelayContextMenuItem_Click;
                        //    mItem.Tag = new RelaySwitchMessage(relay.DeviceMac, relay.DeviceName, relay.Name, true, 2);
                        //    cmenu.Items.Add(mItem);

                        //    cmenu.PlacementTarget = imageControl;

                        //    imageControl.ContextMenu = cmenu;
                        //}
                    }
                }
                else
                    this.Dispatcher.Invoke(delegate() { LoadRelay(_deviceName, _relayNode, _mapName); });
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void LoadCamera(XmlNode _cameraNode, string _mapName)
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (_cameraNode != null)
                    {
                        GraphicsPicture img = LoadMapIcon(_cameraNode["Image"], _mapName);
                        if (img != null)
                        {
                            img.Name = string.Format("Camera_{0}", XmlNode.ParseString(_cameraNode["Name"], ""));
                            img.Tag = _cameraNode;

                            ToolTip tooltip = new ToolTip() { Opacity = 0.8, Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint, HasDropShadow = true };
                            Camera camera = new Camera(_cameraNode);
                            tooltip.Content = camera;
                            DataTemplate dt = (DataTemplate)this.FindResource("templateCameraTooltip");
                            tooltip.ContentTemplate = dt;
                            img.Tooltip = tooltip;

                            ContextMenu cmenu = new System.Windows.Controls.ContextMenu() { Placement = System.Windows.Controls.Primitives.PlacementMode.MousePoint, HasDropShadow = true };
                            MenuItem mItem = new MenuItem() { Header = "Snapshot", Tag = CameraImage.FromNode(_cameraNode) };
                            mItem.Click += CameraContextMenuItem_Click;
                            cmenu.Items.Add(mItem);

                            img.ContextMenu = cmenu;
                        }

                        //Image imageControl = LoadMapIcon(_cameraNode["Image"], _mapName);

                        //if (imageControl != null)
                        //{
                        //    imageControl.Name = string.Format("Camera_{0}", XmlNode.ParseString(_cameraNode["Name"], ""));
                        //    imageControl.Tag = _cameraNode;

                        //    ToolTip tooltip = new ToolTip();
                        //    tooltip.Opacity = 0.8;
                        //    tooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.Left;
                        //    Camera camera = new Camera(_cameraNode);
                        //    tooltip.Content = camera;
                        //    DataTemplate dt = (DataTemplate)this.FindResource("templateCameraTooltip");
                        //    tooltip.ContentTemplate = dt;
                        //    imageControl.ToolTip = tooltip;

                        //    ContextMenu cmenu = new System.Windows.Controls.ContextMenu();
                        //    MenuItem mItem = new MenuItem();
                        //    mItem.Header = "Snapshot";
                        //    mItem.Click += CameraContextMenuItem_Click;

                        //    mItem.Tag = CameraImage.FromNode(_cameraNode);
                        //    cmenu.Items.Add(mItem);

                        //    cmenu.PlacementTarget = imageControl;

                        //    imageControl.ContextMenu = cmenu;
                        //}
                    }
                }
                else
                    this.Dispatcher.Invoke(delegate() { LoadCamera(_cameraNode, _mapName); });
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Load Devices

        #region Images
        private GraphicsPicture LoadMapIcon(XmlNode _imageNode, string _mapName)
        {
            GraphicsPicture img = null;

            try
            {
                BitmapSource imgS = null;

                double imageX = 0;
                double imageY = 0;
                double imageWidth = 30;
                double imageHeight = 30;
                double rotation = 0;

                if (_imageNode != null)
                {
                    string mapName = "";
                    if (_imageNode.Attribute("Map") != null)
                        mapName = _imageNode.Attribute("Map").Value;

                    if (mapName == _mapName)
                    {
                        //get default properties for the icon
                        XmlNode mapNode = mapDoc.GetMap(_mapName);
                        if (mapNode != null)
                        {
                            imageWidth = XmlNode.ParseDouble(mapNode["DefaultIconWidth"], imageWidth);
                            imageHeight = XmlNode.ParseDouble(mapNode["DefaultIconHeight"], imageHeight);
                        }

                        string imgPath = _imageNode.Value;

                        Uri fullpath = null;
                        try
                        {
                            fullpath = new Uri(new Uri(Assembly.GetExecutingAssembly().Location), imgPath);
                        }
                        catch (Exception ex)
                        {
                            fullpath = null;
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Could not construct image file path.", ex);
                        }

                        if (!string.IsNullOrWhiteSpace(imgPath) && System.IO.File.Exists(fullpath != null ? fullpath.LocalPath : ""))
                            imgS = new BitmapImage(fullpath);
                        else
                            imgS = Properties.Resources.Placeholder.ToWpfBitmap();

                        imageX = XmlNode.ParseDouble(_imageNode, 0, "Left");
                        imageY = XmlNode.ParseDouble(_imageNode, 0, "Top");
                        imageWidth = XmlNode.ParseDouble(_imageNode, imageWidth, "Width");
                        imageHeight = XmlNode.ParseDouble(_imageNode, imageHeight, "Height");
                        rotation = XmlNode.ParseDouble(_imageNode, 0, "Rotation");
                    }
                    else
                        imgS = null;

                }

                if (imgS != null)
                {
                    img = new GraphicsPicture(imageX, imageY, imageX + imageWidth, imageY + imageHeight, rotation, imgS, drawingCanvas.ActualScale);
                    drawingCanvas.GraphicsList.Add(img);
                    //drawingCanvas.CaptureMouse();

                    //imageControl = new Image();

                    //drawingCanvas.Children.Add(imageControl);
                    //imageControl.Source = imgS;
                    //Canvas.SetLeft(imageControl, imageX);
                    //Canvas.SetTop(imageControl, imageY);

                    //imageControl.Width = imageWidth;
                    //imageControl.Height = imageHeight;

                    //if (rotation > 0)
                    //    imageControl.RenderTransform = new RotateTransform(rotation, imageWidth / 2, imageHeight / 2);

                    //imageControl.MouseDown += imageControl_MouseDown;
                    //imageControl.MouseUp += imageControl_MouseUp;
                    //imageControl.MouseMove += imageControl_MouseMove;
                    //imageControl.MouseEnter += imageControl_MouseEnter;
                    //imageControl.MouseLeave += imageControl_MouseLeave;

                    //if (detectorImages == null)
                    //    detectorImages = new List<Image>();

                    //detectorImages.Add(imageControl);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return img;
        }
        public static void ToggleAltImage(GraphicsPicture _img, bool _altState)
        {
            try
            {
                if (_img.CheckAccess())
                {
                    if (_img != null)
                    {
                        BitmapSource imgS = null;

                        //double imageWidth = 30;
                        //double imageHeight = 30;
                        //double rotation = 0;

                        XmlNode imgNode = ((XmlNode)_img.Tag)["Image"];
                        XmlNode altImgNode = ((XmlNode)_img.Tag)["AltImage"];

                        if (imgNode != null && altImgNode != null)
                        {
                            string imgPath = _altState ? altImgNode.Value : imgNode.Value;

                            Uri fullpath = null;
                            try
                            {
                                fullpath = new Uri(new Uri(Assembly.GetExecutingAssembly().Location), imgPath);
                            }
                            catch (Exception ex)
                            {
                                fullpath = null;
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Could not construct image file path.", ex);
                            }

                            if (!string.IsNullOrWhiteSpace(imgPath) && System.IO.File.Exists(fullpath != null ? fullpath.LocalPath : ""))
                                imgS = new BitmapImage(fullpath);
                            else
                                imgS = Properties.Resources.Placeholder.ToWpfBitmap();

                            //string baseDir = Assembly.GetExecutingAssembly().Location;

                            //if (!string.IsNullOrWhiteSpace(imgPath) && System.IO.File.Exists(imgPath))
                            //    imgS = new BitmapImage(new Uri(new Uri(baseDir), imgPath));
                            //else
                            //    imgS = Properties.Resources.Placeholder.ToWpfBitmap();

                            //imageWidth = XmlNode.ParseDouble(_altState ? altImgNode : imgNode, _img.Width, "Width");
                            //imageHeight = XmlNode.ParseDouble(_altState ? altImgNode : imgNode, _img.Height, "Height");
                            //rotation = XmlNode.ParseDouble(_altState ? altImgNode : imgNode, 0, "Rotation");
                        }

                        if (imgS != null)
                        {
                            _img.Image = imgS;
                            _img.RefreshDrawing();
                            //_img.Width = imageWidth;
                            //_img.Height = imageHeight;
                        }
                    }
                }
                else
                    _img.Dispatcher.Invoke((System.Action)delegate() { ToggleAltImage(_img, _altState); });
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Images

        #region IconFlashing
        private List<GraphicsBase> GetDetectorImages(AlarmBase _alarm)
        {
            List<GraphicsBase> result = null;

            if (_alarm != null && /*detectorImages*/drawingCanvas.GraphicsList != null)
                if (_alarm.Triggers != null)
                    if (_alarm.Triggers.Count > 0)
                        foreach (IEventTrigger trigger in _alarm.Triggers)
                            foreach (GraphicsBase img in drawingCanvas.GraphicsList)
                                if (img.Name != null && img.Name.ToLower() == string.Format("Detector_{0}_{1}", trigger.DeviceName, trigger.DetectorName).ToLower())
                                {
                                    if (result == null)
                                    {
                                        result = new List<GraphicsBase>();
                                        result.Add(img);
                                    }
                                    else
                                    {
                                        if (!result.Contains(img))
                                            result.Add(img);
                                    }
                                }

            return result;
        }

        public void FlashDetector(AlarmBase _alarm)
        {
            try
            {
                if (this.CheckAccess())
                {
                    List<GraphicsBase> images = GetDetectorImages(_alarm);

                    if (images != null)
                    {
                        foreach (GraphicsBase img in images)
                        {
                            if (img is GraphicsPicture)
                                ToggleAltImage((GraphicsPicture)img, true);

                            bool existing = false;

                            if (IconStoryboards == null)
                            {
                                IconStoryboards = new List<IconStoryboard>();
                            }

                            for (int i = IconStoryboards.Count - 1; i >= 0; i--)
                            {
                                IconStoryboard iconSB = IconStoryboards[i];
                                if (img == iconSB.Image)
                                {
                                    iconSB.AddAlarm(_alarm);
                                    existing = true;
                                    if (iconSB.AlarmCount <= 1)
                                        iconSB.StoryBoard.Begin();

                                    break;
                                }
                            }

                            if (!existing)
                            {
                                Storyboard sb = new Storyboard();

                                //if (XmlNode.ParseBool(Settings.TheeSettings["AlarmAnimation"], false, "Enabled"))
                                //{
                                //    double fromWidth = img.Width;
                                //    double toWidth = img.Width * 1.2;
                                //    var daW = new DoubleAnimation(fromWidth, toWidth, new Duration(TimeSpan.FromMilliseconds(250)));

                                //    double fromHeight = img.Height;
                                //    double toHeight = img.Height * 1.2;
                                //    var daH = new DoubleAnimation(fromHeight, toHeight, new Duration(TimeSpan.FromMilliseconds(250)));

                                //    sb.AutoReverse = true;
                                //    //sb.RepeatBehavior = new RepeatBehavior(5);
                                //    sb.RepeatBehavior = RepeatBehavior.Forever;

                                //    Storyboard.SetTarget(daW, img);
                                //    Storyboard.SetTargetProperty(daW, new PropertyPath(Image.WidthProperty));
                                //    sb.Children.Add(daW);

                                //    Storyboard.SetTarget(daH, img);
                                //    Storyboard.SetTargetProperty(daH, new PropertyPath(Image.HeightProperty));
                                //    sb.Children.Add(daH);
                                //}

                                IconStoryboard iconSB = new IconStoryboard(img, sb);
                                iconSB.AddAlarm(_alarm);
                                IconStoryboards.Add(iconSB);
                                sb.Begin();
                            }
                        }
                    }
                }
                else
                    this.Dispatcher.Invoke((System.Action)delegate() { FlashDetector(_alarm); });
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void FlashDetectorStop(string _deviceName, string _detectorName)
        {
            foreach (GraphicsBase img in drawingCanvas.GraphicsList)
            {
                if (img.Name.ToLower() == string.Format("Detector_{0}_{1}", _deviceName, _detectorName).ToLower())            
                {
                    FlashDetectorStop(img);
                }
            }
        }

        public void FlashDetectorStop(GraphicsBase img)
        {
            if (IconStoryboards != null)
            {
                for (int i = IconStoryboards.Count - 1; i >= 0; i--)
                {
                    IconStoryboard iconSB = IconStoryboards[i];
                    if (img == iconSB.Image)
                    {
                        iconSB.StoryBoard.Stop();
                        IconStoryboards.Remove(iconSB);
                        break;
                    }
                }
            }
            if (img is GraphicsPicture)
                ToggleAltImage((GraphicsPicture)img, false);
        }

        protected void ClearIconStoryboards()
        {
            if (IconStoryboards != null)
            {
                for (int i = IconStoryboards.Count - 1; i >= 0; i--)
                {
                    IconStoryboard sb = IconStoryboards[i];
                    IconStoryboards.RemoveAt(i);

                    if (sb != null)
                        sb.Dispose();
                }
            }
        }
        #endregion IconFlashing

        #region Map Navigation Handlers
        /// <summary>
        /// Zoom the viewport out, centering on the specified point (in content coordinates).
        /// </summary>
        private void ZoomOut(Point contentZoomCenter)
        {
            if (zoomAndPanControl.ViewportWidth < zoomAndPanControl.ExtentWidth || zoomAndPanControl.ViewportHeight < zoomAndPanControl.ExtentHeight)
                zoomAndPanControl.ZoomAboutPoint(zoomAndPanControl.ContentScale - scrlZoomSpeed.Value, contentZoomCenter);
        }

        /// <summary>
        /// Zoom the viewport in, centering on the specified point (in content coordinates).
        /// </summary>
        private void ZoomIn(Point contentZoomCenter)
        {
            zoomAndPanControl.ZoomAboutPoint(zoomAndPanControl.ContentScale + scrlZoomSpeed.Value, contentZoomCenter);
        }

        public void ZoomToExtent()
        {
            if (this.CheckAccess())
            {
                ZoomToExtentTS();
            }
            else
            {
                this.Dispatcher.Invoke(ZoomToExtentTS);
            }
        }

        protected void ZoomToExtentTS()
        {
            zoomAndPanControl.AnimatedZoomTo(new Rect(0, 0, drawingCanvas.Width + 10, drawingCanvas.Height + 10));
        }

        /// <summary>
        /// When the user has finished dragging out the rectangle the zoom operation is applied.
        /// </summary>
        private void ApplyDragZoomRect()
        {
            // Record the previous zoom level, so that we can jump back to it when the backspace key is pressed.
            SavePrevZoomRect();

            // Retreive the rectangle that the user draggged out and zoom in on it.
            double contentX = Canvas.GetLeft(dragZoomBorder);
            double contentY = Canvas.GetTop(dragZoomBorder);
            double contentWidth = dragZoomBorder.Width;
            double contentHeight = dragZoomBorder.Height;
            zoomAndPanControl.AnimatedZoomTo(new Rect(contentX, contentY, contentWidth, contentHeight));

            FadeOutDragZoomRect();
        }

        // Fade out the drag zoom rectangle.
        private void FadeOutDragZoomRect()
        {
            ZoomAndPan.AnimationHelper.StartAnimation(dragZoomBorder, Border.OpacityProperty, 0.0, 0.1,
                delegate(object sender, EventArgs e)
                {
                    dragZoomCanvas.Visibility = Visibility.Collapsed;
                });
        }

        // Record the previous zoom level, so that we can jump back to it when the backspace key is pressed.
        private void SavePrevZoomRect()
        {
            prevZoomRect = new Rect(zoomAndPanControl.ContentOffsetX, zoomAndPanControl.ContentOffsetY, zoomAndPanControl.ContentViewportWidth, zoomAndPanControl.ContentViewportHeight);
            prevZoomScale = zoomAndPanControl.ContentScale;
            prevZoomRectSet = true;
        }

        /// <summary>
        /// Initialise the rectangle that the use is dragging out.
        /// </summary>
        private void InitDragZoomRect(Point pt1, Point pt2)
        {
            SetDragZoomRect(pt1, pt2);

            dragZoomCanvas.Visibility = Visibility.Visible;
            dragZoomBorder.Opacity = 0.5;
        }

        /// <summary>
        /// Update the position and size of the rectangle that user is dragging out.
        /// </summary>
        private void SetDragZoomRect(Point pt1, Point pt2)
        {
            double x, y, width, height;

            //
            // Determine x,y,width and height of the rect inverting the points if necessary.
            // 

            if (pt2.X < pt1.X)
            {
                x = pt2.X;
                width = pt1.X - pt2.X;
            }
            else
            {
                x = pt1.X;
                width = pt2.X - pt1.X;
            }

            if (pt2.Y < pt1.Y)
            {
                y = pt2.Y;
                height = pt1.Y - pt2.Y;
            }
            else
            {
                y = pt1.Y;
                height = pt2.Y - pt1.Y;
            }

            // Update the coordinates of the rectangle that is being dragged out by the user.
            // The we offset and rescale to convert from content coordinates.
            Canvas.SetLeft(dragZoomBorder, x);
            Canvas.SetTop(dragZoomBorder, y);
            dragZoomBorder.Width = width;
            dragZoomBorder.Height = height;
        }

        private void zoomAndPanControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!drawingCanvas.EditMode)
            {
                drawingCanvas.Focus();
                Keyboard.Focus(drawingCanvas);

                mouseButtonDown = e.ChangedButton;
                origZoomAndPanControlMouseDownPoint = e.GetPosition(zoomAndPanControl);
                origContentMouseDownPoint = e.GetPosition(drawingCanvas);

                if ((Keyboard.Modifiers & ModifierKeys.Shift) != 0 &&
                    (e.ChangedButton == MouseButton.Left ||
                     e.ChangedButton == MouseButton.Right))
                {
                    // Shift + left- or right-down initiates zooming mode.
                    mouseHandlingMode = MouseHandlingMode.Zooming;
                }
                else if (mouseButtonDown == MouseButton.Left)
                {
                    // Just a plain old left-down initiates panning mode.
                    mouseHandlingMode = MouseHandlingMode.Panning;
                }

                if (mouseHandlingMode != MouseHandlingMode.None)
                {
                    // Capture the mouse so that we eventually receive the mouse up event.
                    zoomAndPanControl.CaptureMouse();
                    e.Handled = true;
                }
            }
        }

        private void zoomAndPanControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!drawingCanvas.EditMode)
            {
                if (mouseHandlingMode != MouseHandlingMode.None)
                {
                    if (mouseHandlingMode == MouseHandlingMode.Zooming)
                    {
                        if (mouseButtonDown == MouseButton.Left)
                        {
                            // Shift + left-click zooms in on the content.
                            ZoomIn(origContentMouseDownPoint);
                        }
                        else if (mouseButtonDown == MouseButton.Right)
                        {
                            // Shift + left-click zooms out from the content.
                            ZoomOut(origContentMouseDownPoint);
                        }
                    }
                    else if (mouseHandlingMode == MouseHandlingMode.DragZooming)
                    {
                        // When drag-zooming has finished we zoom in on the rectangle that was highlighted by the user.
                        ApplyDragZoomRect();
                    }

                    zoomAndPanControl.ReleaseMouseCapture();
                    mouseHandlingMode = MouseHandlingMode.None;
                    e.Handled = true;
                }
            }
        }

        private void zoomAndPanControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (!drawingCanvas.EditMode)
            {
                switch (mouseHandlingMode)
                {
                    case MouseHandlingMode.Panning:
                        {
                            // The user is left-dragging the mouse.
                            // Pan the viewport by the appropriate amount.
                            Point curContentMousePoint = e.GetPosition(drawingCanvas);
                            Vector dragOffset = curContentMousePoint - origContentMouseDownPoint;

                            zoomAndPanControl.ContentOffsetX -= dragOffset.X;
                            zoomAndPanControl.ContentOffsetY -= dragOffset.Y;

                            e.Handled = true;
                        }
                        break;
                    case MouseHandlingMode.Zooming:
                        {
                            Point curZoomAndPanControlMousePoint = e.GetPosition(zoomAndPanControl);
                            Vector dragOffset = curZoomAndPanControlMousePoint - origZoomAndPanControlMouseDownPoint;
                            double dragThreshold = 10;
                            if (mouseButtonDown == MouseButton.Left &&
                                (Math.Abs(dragOffset.X) > dragThreshold ||
                                 Math.Abs(dragOffset.Y) > dragThreshold))
                            {
                                // When Shift + left-down zooming mode and the user drags beyond the drag threshold,
                                // initiate drag zooming mode where the user can drag out a rectangle to select the area
                                // to zoom in on.
                                mouseHandlingMode = MouseHandlingMode.DragZooming;
                                Point curContentMousePoint = e.GetPosition(drawingCanvas);
                                InitDragZoomRect(origContentMouseDownPoint, curContentMousePoint);
                            }

                            e.Handled = true;
                        }
                        break;
                    case MouseHandlingMode.DragZooming:
                        {
                            // When in drag zooming mode continously update the position of the rectangle
                            // that the user is dragging out.
                            Point curContentMousePoint = e.GetPosition(drawingCanvas);
                            SetDragZoomRect(origContentMouseDownPoint, curContentMousePoint);

                            e.Handled = true;
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private void zoomAndPanControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;

            if (e.Delta > 0)
            {
                Point curContentMousePoint = e.GetPosition(drawingCanvas);
                ZoomIn(curContentMousePoint);
            }
            else if (e.Delta < 0)
            {
                Point curContentMousePoint = e.GetPosition(drawingCanvas);
                ZoomOut(curContentMousePoint);
            }
        }

        /// <summary>
        /// Event raised when the user has double clicked in the zoom and pan control.
        /// </summary>
        private void zoomAndPanControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Shift) == 0)
            {
                Point doubleClickPoint = e.GetPosition(drawingCanvas);
                zoomAndPanControl.AnimatedSnapTo(doubleClickPoint);
            }
        }

        private void imageControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                drawingCanvas.Focus();
                Keyboard.Focus(drawingCanvas);

                if (e.ClickCount == 2)
                {
                    Image image = (Image)sender;
                    if (!image.Name.StartsWith("Detector_") && !image.Name.StartsWith("Relay_") && !image.Name.StartsWith("Camera_"))
                        LoadMap(image.Name.Replace('_', '.'));
                }
                else if ((Keyboard.Modifiers & ModifierKeys.Shift) == 0 && mouseHandlingMode == MouseHandlingMode.None)
                {
                    mouseHandlingMode = MouseHandlingMode.DraggingElements;
                    origContentMouseDownPoint = e.GetPosition(drawingCanvas);

                    Image image = (Image)sender;
                    image.CaptureMouse();

                    e.Handled = true;
                }
            }
        }

        private void imageControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                if (mouseHandlingMode == MouseHandlingMode.DraggingElements)
                {
                    mouseHandlingMode = MouseHandlingMode.None;

                    Image image = (Image)sender;
                    image.ReleaseMouseCapture();

                    if (image.Tag is XmlNode)
                    {
                        XmlNode node = (XmlNode)image.Tag;
                        if (!node.Name.Contains("Image"))
                        {
                            if (node["Image"] != null)
                                node = node["Image"];
                            else if (node["ThumbnailImage"] != null)
                                node = node["ThumbnailImage"];
                        }

                        if (node.Name == "Image" || node.Name == "ThumbnailImage")
                        {
                            node.Attribute("Left").Value = Canvas.GetLeft(image).ToString();
                            node.Attribute("Top").Value = Canvas.GetTop(image).ToString();
                        }
                    }

                    e.Handled = true;
                }
            }
        }
        #endregion Map Navigation Handlers

        #region Icon Handlers
        private void SetDetectorNodePosition(XmlNode _detectorNode, double _left, double _top)
        {
            try
            {
                if (_detectorNode["Image"] != null)
                {
                    XmlNode imageNode = _detectorNode["Image"];

                    if (imageNode.Attribute("Left") != null)
                        imageNode.Attribute("Left").Value = _left.ToString();
                    else
                        imageNode.AddAttribute(new XmlItem("Left", _left.ToString(), imageNode.AttributeCount));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void imageControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseHandlingMode == MouseHandlingMode.DraggingElements)
            {
                Point curContentPoint = e.GetPosition(drawingCanvas);
                Vector rectangleDragVector = curContentPoint - origContentMouseDownPoint;

                // When in 'dragging' mode update the position of the rectangle as the user drags it.

                origContentMouseDownPoint = curContentPoint;

                Image image = (Image)sender;
                Canvas.SetLeft(image, Canvas.GetLeft(image) + rectangleDragVector.X);
                Canvas.SetTop(image, Canvas.GetTop(image) + rectangleDragVector.Y);

                e.Handled = true;
            }
        }

        private void RelayContextMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (e.Source is MenuItem)
            {
                MenuItem mItem = ((MenuItem)e.Source);

                if (mItem.Tag is RelaySwitchMessage)
                {
                    RelaySwitchMessage relayMsg = (RelaySwitchMessage)mItem.Tag;

                    if (RelaySwitchMessage != null)
                        RelaySwitchMessage(this, new RelaySwitchMessageEventArgs(relayMsg));
                }
            }
        }

        private void CameraContextMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (e.Source is MenuItem)
            {
                MenuItem mItem = ((MenuItem)e.Source);

                if (mItem.Tag is CameraImage)
                {
                    CameraImage img = (CameraImage)mItem.Tag;

                    if (SnapshotRequest != null)
                        SnapshotRequest(this, new SnapshotRequestEventArgs(img));
                }
            }
        }

        private void drawingCanvas_IconMouseEnter(object sender, MouseEventArgs e)
        {
            MyZoom.ToggleZoom();
        }

        private void drawingCanvas_IconMouseLeave(object sender, MouseEventArgs e)
        {
            if(MyZoom.IsVisible)
                MyZoom.ToggleZoom();
        }

        private void drawingCanvas_NewDrawingAdded(object sender, DrawingEventArgs e)
        {
            
        }
        #endregion Icon Handlers

        #region Toolbar Handlers
        private void tbbHome_Click(object sender, RoutedEventArgs e)
        {
            currentMapTree.Clear();
            LoadMap("");
        }

        private void tbbBack_Click(object sender, RoutedEventArgs e)
        {
            if (currentMapTree != null && currentMapTree.Count > 1)
            {
                currentMapTree.RemoveAt(currentMapTree.Count - 1);
                LoadMap(currentMapTree[currentMapTree.Count - 1]);
            }
        }

        private void tbbExtent_Click(object sender, RoutedEventArgs e)
        {
            ZoomToExtent();
        }

        private void tbbSave_Click(object sender, RoutedEventArgs e)
        {
            if (OnRequestSave != null)
                OnRequestSave(this, new EventArgs());
        }

        private void tbbToggleEditMode_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.EditMode = !drawingCanvas.EditMode;

            tbbToggleEditModeImage.Source = drawingCanvas.EditMode ? Properties.Resources.EditMapX.ToWpfBitmap() : Properties.Resources.EditMap.ToWpfBitmap();
            tbbToggleEditMode.ToolTip = drawingCanvas.EditMode ? "Turn Edit Mode Off" : "Turn Edit Mode On";
        }

        private void tbbAddIcon_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.Tool = ToolType.Picture;
        }

        private void tbbAddZone_Click(object sender, RoutedEventArgs e)
        {
            drawingCanvas.Tool = ToolType.PolyLine;
        }
        #endregion Toolbar Handlers
    }
}
