﻿using DrawToolsLib;
using eNerve.DataTypes;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace WPFMapControl
{
    public class IconStoryboard : IDisposable
    {
        public GraphicsBase Image { get; private set; }
        public Storyboard StoryBoard { get; private set; }

        private List<AlarmBase> _alarms;

        public IconStoryboard(GraphicsBase _image, Storyboard _storyBoard)
        {
            Image = _image;
            //originalWidth = _image.Width;
            //originalHeight = _image.Height;

            StoryBoard = _storyBoard;
        }

        ~IconStoryboard()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (_alarms != null)
            {
                for (int i = _alarms.Count - 1; i >= 0; i--)
                {
                    _alarms[i].Resolved -= alarm_Resolved;

                    _alarms.RemoveAt(i);
                }
            }

            StoryBoard.Stop();
        }

        public void AddAlarm(AlarmBase _alarm)
        {
            if (_alarm != null)
            {
                if (_alarms == null)
                {
                    _alarms = new List<AlarmBase>();
                    _alarms.Add(_alarm);
                }
                else
                {
                    if (_alarms.Count <= 0)
                        _alarms.Add(_alarm);
                    else
                    {
                        int sr = _alarms.BinarySearch(_alarm);

                        if (sr < 0)
                            _alarms.Insert(~sr, _alarm);
                    }
                }

                _alarm.Resolved += alarm_Resolved;
            }
        }

        public void RemoveAlarm(AlarmBase _alarm)
        {
            if (_alarms != null)
            {
                int sr = _alarms.BinarySearch(_alarm);

                if (sr >= 0)
                    _alarms.RemoveAt(sr);
            }
        }

        public int AlarmCount
        {
            get
            {
                return _alarms.Count;
            }
        }

        void alarm_Resolved(object sender, AlarmResolvedEventArgs e)
        {
            if (sender is AlarmBase)
            {
                AlarmBase alarm = (AlarmBase)sender;
                alarm.Resolved -= alarm_Resolved;

                RemoveAlarm(alarm);

                if (AlarmCount <= 0)
                {
                    StoryBoard.Stop();

                    RestoreImage();
                }
            }
        }

        private void RestoreImage()
        {
            try
            {
                if (Image.Dispatcher.CheckAccess())
                {
                    if (Image is GraphicsPicture)
                    MapControl2.ToggleAltImage((GraphicsPicture)Image, false);
                }
                else
                    Image.Dispatcher.Invoke(RestoreImage);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
    }
}
