﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Timers;

using eNervePluginInterface;
using eNervePluginInterface.CameraInterfaces;
using NETSDKHelper;

namespace RevoPlugin
{
    public class RevoCamera : ICameraPlugin, ILiveCamera, IPlaybackCamera, ISnapshotCamera, IExportCamera, IPtzCamera
    {
        private IntPtr apiHandle = IntPtr.Zero;
        private bool connected;
        private IntPtr displayHandle;
        private IntPtr downloadHandle;
        private string downloadFileName;
        private Timer downloadTimer;
        private readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0);
        private string fileName;
        [NonSerialized]
        private ICameraServerPlugin parent;
        private CustomProperties properties;
        [NonSerialized]
        private RevoServer RevoServer;

        public bool Admin { get; set; }

        public string Author
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company; }
        }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                result.Add(new CustomPropertyDefinition("ChannelId", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("DisplayName", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Name", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Parent", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("PluginType", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Ptz", typeof(CustomProperty)));
                return result;
            }
        }

        public string Description
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description; }
        }

        public bool Enabled
        { get; set; }

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public INerveHostPlugin Host{ get; set; }

        public string Name { get; set; }

        public bool Online { get { return connected; } set { connected = value; } }
        
        public ICameraServerPlugin Parent{ get { return parent; } set { parent = value; } }
        
        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }

        public string Version
        {
            get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        public event EventHandler DeviceOffline;
        public event EventHandler DeviceOnline;
        public event EventHandler<PluginErrorEventArgs> Error;

        public override bool Equals(object obj)
        {
            RevoCamera other = obj as RevoCamera;
            if (other == null)
                return false;
            else
            {
                return (other.Name == Name && other.FileName == fileName);
            }
        }

        /// <summary>
        /// Assumption: Camera is already in playback. Seek time must be in local time.
        /// </summary>
        /// <param name="seekTime"></param>
        /// <returns></returns>
        public bool GoToTime(DateTime seekTimeLocal)
        {
            bool result = false;
            DateTime seekTimeUtc = seekTimeLocal.ToUniversalTime();
            if (seekTimeUtc > DateTime.UtcNow)
                seekTimeUtc = DateTime.UtcNow;

            Recording recording = GetRecording(seekTimeUtc);
            if (recording == null)
            {
                Debug.Write("Error going to time in REVO api.");
                return result;
            }

            if (displayHandle == IntPtr.Zero)
                return result;
            if (apiHandle != IntPtr.Zero)
                NETDEVSDK.NETDEV_StopPlayBack(apiHandle);
            RevoServer.RemoveDisplay(displayHandle);

            NETDEV_PLAYBACKINFO_S playbackInfo = new NETDEV_PLAYBACKINFO_S() { tBeginTime = recording.Start, tEndTime = recording.End, hPlayWnd = displayHandle,
                dwLinkMode = (int)NETDEV_PROTOCOL_E.NETDEV_TRANSPROTOCOL_RTPTCP };

            apiHandle = NETDEVSDK.NETDEV_PlayBackByName(RevoServer.ServerHandle, ref playbackInfo);
            if (apiHandle == IntPtr.Zero)
            {// FAILED TO GO TO PLAYBACK
                Debug.Write("Error going to time in REVO api.");
            }
            else
            {
                RevoServer.AddDisplay(displayHandle, apiHandle);
                result = true;
            }

            return result;
        }

        public void GoToLive(IntPtr _displayHandle)
        {
            if (connected)
            {
                var _apiHandle = RevoServer.GetApiHandle(_displayHandle);
                if (_apiHandle != IntPtr.Zero)
                    NETDEVSDK.NETDEV_StopPlayBack(_apiHandle);
                RevoServer.RemoveDisplay(_displayHandle);
            }
            else
            {
                Connect();
            }
            displayHandle = _displayHandle;
            NETDEV_PREVIEWINFO_S stNetInfo = new NETDEV_PREVIEWINFO_S() { dwChannelID = int.Parse(Properties["ChannelId"]), hPlayWnd = displayHandle,
                dwStreamType = (int)NETDEV_LIVE_STREAM_INDEX_E.NETDEV_LIVE_STREAM_INDEX_AUX, dwLinkMode = (int)NETDEV_PROTOCOL_E.NETDEV_TRANSPROTOCOL_RTPTCP /*only support*/ };

            apiHandle = NETDEVSDK.NETDEV_RealPlay(RevoServer.ServerHandle, ref stNetInfo, IntPtr.Zero, IntPtr.Zero);
            if (apiHandle == IntPtr.Zero)
            {
                connected = false;
                Debug.Write("Something's wrong - could not get camera handle from REVO API.");
            }
            else
            {
                connected = true;
                RevoServer.AddDisplay(displayHandle, apiHandle);
            }
        }

        public bool GoToPlayback(IntPtr _displayHandle)
        {
            if (connected)
            {
                var _apiHandle = RevoServer.GetApiHandle(_displayHandle);
                if (_apiHandle != IntPtr.Zero)
                    NETDEVSDK.NETDEV_StopRealPlay(_apiHandle);
            }
            else
            {
                Connect();
            }
            Recording latest = GetLastRecording();
            if (latest == null)
                return false;
            RevoServer.RemoveDisplay(displayHandle);

            NETDEV_PLAYBACKINFO_S playbackInfo = new NETDEV_PLAYBACKINFO_S() { tBeginTime = latest.Start, tEndTime = latest.End, hPlayWnd = displayHandle,
                dwLinkMode = (int)NETDEV_PROTOCOL_E.NETDEV_TRANSPROTOCOL_RTPTCP };

            apiHandle = NETDEVSDK.NETDEV_PlayBackByName(RevoServer.ServerHandle, ref playbackInfo);
            if (apiHandle == IntPtr.Zero)
            {// FAILED TO GO TO PLAYBACK
                Debug.Write("Error going to playback in REVO api.");
                return false;
            }
            else
            {
                RevoServer.AddDisplay(displayHandle, apiHandle);
                return true;
            }

        }

        private Recording GetLastRecording()
        {
            Recording lastRecording = null;
            if (RevoServer.ServerHandle == IntPtr.Zero)
                return lastRecording;

            NETDEV_FILECOND_S stFileCond = new NETDEV_FILECOND_S();
            stFileCond.szFileName = new char[64];
            stFileCond.szReserve = new byte[104];
            stFileCond.dwChannelID = int.Parse(Properties["ChannelId"]);

            stFileCond.tEndTime = (long)(DateTime.UtcNow - epoch).TotalSeconds;
            stFileCond.tBeginTime = stFileCond.tEndTime - 600;

            IntPtr recordingHandle = NETDEVSDK.NETDEV_FindFile(RevoServer.ServerHandle, ref stFileCond);
            if (recordingHandle == IntPtr.Zero)
            {
                return lastRecording;
            }
            else
            {
                lastRecording = new Recording();
                NETDEV_FINDDATA_S recordingData = new NETDEV_FINDDATA_S();
                while (NETDEVSDK.NETDEV_FindNextFile(recordingHandle, ref recordingData) == NETDEVSDK.TRUE)
                {// Should only ever loop once - while works for longer periods of recording.
                    lastRecording.Start = recordingData.tBeginTime;
                    lastRecording.End = recordingData.tEndTime;
                }
                return lastRecording;
            }
        }

        private Recording GetRecording(DateTime utcSeekTime)
        {
            Recording recording = null;
            if (RevoServer.ServerHandle == IntPtr.Zero)
                return recording;

            NETDEV_FILECOND_S stFileCond = new NETDEV_FILECOND_S() { szFileName = new char[64], szReserve = new byte[104], dwChannelID = int.Parse(Properties["ChannelId"]),
                tBeginTime = (long)(utcSeekTime - epoch).TotalSeconds};
            if (utcSeekTime.AddSeconds(1800) > DateTime.UtcNow)
                stFileCond.tEndTime = (long)(DateTime.UtcNow - epoch).TotalSeconds;
            else
                stFileCond.tEndTime = (long)(utcSeekTime - epoch).TotalSeconds + 3600;

            IntPtr recordingHandle = NETDEVSDK.NETDEV_FindFile(RevoServer.ServerHandle, ref stFileCond);
            if (recordingHandle == IntPtr.Zero)
            {
                return recording;
            }
            else
            {
                recording = new Recording();
                NETDEV_FINDDATA_S recordingData = new NETDEV_FINDDATA_S();
                while (NETDEVSDK.NETDEV_FindNextFile(recordingHandle, ref recordingData) == NETDEVSDK.TRUE)
                {// Should only ever loop once - while works for longer periods of recording.
                    recording.Start = recordingData.tBeginTime;
                    recording.End = recordingData.tEndTime;
                }
                return recording;
            }
        }

        public bool Pause()
        {
            bool success = false;
            int result = 0;
            if (NETDEVSDK.NETDEV_PlayBackControl(apiHandle, (int)NETDEV_VOD_PLAY_CTRL_E.NETDEV_PLAY_CTRL_PAUSE, ref result) == NETDEVSDK.TRUE)
            {
                success = true;
            }
            else
            {
                Debug.Write("Error pausing playback video " + Name);
            }
            return success;
        }

        public bool Play()
        {
            bool success = false;
            int result = 0;
            if (NETDEVSDK.NETDEV_PlayBackControl(apiHandle, (int)NETDEV_VOD_PLAY_CTRL_E.NETDEV_PLAY_CTRL_RESUME, ref result) == NETDEVSDK.TRUE)
            {
                success = true;
            }
            else
            {
                Debug.Write("Error resuming playback video " + Name);
            }
            return success;
        }

        public void SnapShot(IntPtr handle, string fileName)
        {
            if (RevoServer == null)
                RevoServer = parent as RevoServer;
            if (RevoServer != null)
            {
                if (RevoServer.ServerHandle == IntPtr.Zero)
                    return;

                if (NETDEVSDK.NETDEV_CapturePicture(RevoServer.GetApiHandle(handle), fileName, (int)NETDEV_PICTURE_FORMAT_E.NETDEV_PICTURE_JPG) != NETDEVSDK.TRUE)
                {
                    Debug.Write("Error saving snapshot");
                }
            }

        }

        /// <summary>
        /// Method to start plugin - not relevant to cameras.
        /// </summary>
        public void Start()
        {
            throw new NotImplementedException();
        }

        public void StopLive(IntPtr displayHandle)
        {
            if (RevoServer == null)
                RevoServer = parent as RevoServer;
            if (RevoServer != null)
            {
                var apiHandle = RevoServer.GetApiHandle(displayHandle);
                if ((apiHandle == IntPtr.Zero) || (RevoServer.ServerHandle == IntPtr.Zero))
                    return;
                if (NETDEVSDK.NETDEV_StopRealPlay(apiHandle) == NETDEVSDK.TRUE)
                {
                    RevoServer.RemoveDisplay(displayHandle);
                }
                else
                    Debug.Write("Error stopping live video " + Name);
                displayHandle = IntPtr.Zero; 
            }
        }

        private void Connect()
        {
            RevoServer = parent as RevoServer;
            if (RevoServer == null)
            {
                connected = false;
                return;
            }
            if (RevoServer.IsConnected)
            {
                if ((RevoServer.ServerHandle == IntPtr.Zero) || (displayHandle == IntPtr.Zero))
                    return;
                connected = true;
                //if (cameraHandle != IntPtr.Zero)
                //    if (NETDEVSDK.NETDEV_StopRealPlay(cameraHandle) == NETDEVSDK.TRUE)
                //        cameraHandle = IntPtr.Zero;

            }
        }

        public string Export(DateTime start, DateTime end, string name, IntPtr displayHandle)
        {
            if (RevoServer == null)
                RevoServer = parent as RevoServer;
            if (RevoServer == null)
                return "Error: Revo Server not connected.";
            else
            {
                if (RevoServer.ServerHandle == IntPtr.Zero)
                    return "Error: Revo Server not connected.";

                NETDEV_PLAYBACKCOND_S playbackInfo = new NETDEV_PLAYBACKCOND_S();
                playbackInfo.byRes = new byte[260];
                playbackInfo.hPlayWnd = displayHandle;
                playbackInfo.dwDownloadSpeed = (int)NETDEV_E_DOWNLOAD_SPEED_E.NETDEV_DOWNLOAD_SPEED_EIGHT;
                playbackInfo.tBeginTime = (long)(start.ToUniversalTime() - epoch).TotalSeconds;
                playbackInfo.tEndTime = (long)(end.ToUniversalTime() - epoch).TotalSeconds;
                playbackInfo.dwChannelID = int.Parse(Properties["ChannelId"]);

                if (System.IO.File.Exists(name + ".mp4"))
                {
                    int i = 1;
                    while (System.IO.File.Exists(string.Format("{0}_{1}.mp4", name, i)))
                        i++;
                    name += "_" + i;
                }

                name += ".mp4";

                downloadHandle = NETDEVSDK.NETDEV_GetFileByTime(RevoServer.ServerHandle, ref playbackInfo, name, (int)NETDEV_MEDIA_FILE_FORMAT_E.NETDEV_MEDIA_FILE_MP4);

                if (downloadHandle == IntPtr.Zero)
                    return "Error exporting video: " + NETDEVSDK.NETDEV_GetLastError();
                else
                {
                    downloadFileName = name;
                    downloadTimer = new Timer((end - start).TotalSeconds / 8 * 1000);
                    downloadTimer.Elapsed += DownloadTimer_Elapsed;
                    downloadTimer.AutoReset = false;
                    downloadTimer.Start();
                    return "Downloading export.";
                }
                    
            }
        }

        private void DownloadTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            downloadTimer.Elapsed -= DownloadTimer_Elapsed;
            downloadTimer.Stop();
            if (NETDEVSDK.NETDEV_StopGetFile(downloadHandle) == NETDEVSDK.TRUE)
            {// Success

            }
            else
            {

            }
        }

        public bool PanTilt(int x, int y)
        {
            if (RevoServer == null)
                RevoServer = parent as RevoServer;
            if (RevoServer == null)
            {
                return false;
            }
            else
            {
                if (RevoServer.ServerHandle == IntPtr.Zero)
                    return false;
                int result = 0;
                if (x > 0)// Go right
                {
                    if (y > 0) // Go up
                    {
                        result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_RIGHTUP, 5);
                    }
                    else if (y < 0) // Go down
                    {
                        result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_RIGHTDOWN, 5);
                    }
                    else // No vertical movement
                    {
                        result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_PANRIGHT, 5);
                    }
                }
                else if (x < 0) // Go Left
                {
                    if (y > 0) // Go up
                    {
                        result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_LEFTUP, 5);
                    }
                    else if (y < 0) // Go down
                    {
                        result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_LEFTDOWN, 5);
                    }
                    else // No vertical movement
                    {
                        result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_PANLEFT, 5);
                    }
                }
                else // No horizontal movement.
                {
                    if (y > 0) // Go up
                    {
                        result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_TILTUP, 5);
                    }
                    else if (y < 0) // Go down
                    {
                        result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_TILTDOWN, 5);
                    }
                    else // No vertical movement
                    {
                        return StopPanTilt();
                    }
                }

                if (result == NETDEVSDK.TRUE)
                {
                    return true;
                }
                else
                {
                    Debug.Write("Error while sending pan / tilt " + Name + " - error " + NETDEVSDK.NETDEV_GetLastError());
                    return false;
                }
            }
        }

        public bool StopPanTilt()
        {
            return StopZoom();
        }

        public bool Zoom(int factor)
        {
            if (RevoServer == null)
                RevoServer = parent as RevoServer;
            if (RevoServer == null)
            {
                return false;
            }
            else
            {
                if (RevoServer.ServerHandle == IntPtr.Zero)
                    return false;
                int result = 0;
                if (factor > 0)
                    result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_ZOOMTELE, 5);
                else if (factor < 0)
                    result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_ZOOMWIDE, 5);
                else
                    result = NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_ALLSTOP, 0);

                if (result == NETDEVSDK.TRUE)
                {
                    return true;
                }
                else
                {
                    Debug.Write("Error while zooming " + Name + " - error " + NETDEVSDK.NETDEV_GetLastError());
                    return false;
                }
            }
        }

        public bool StopZoom()
        {
            if (RevoServer == null)
                RevoServer = parent as RevoServer;
            if (RevoServer == null)
            {
                return false;
            }
            else
            {
                if (RevoServer.ServerHandle == IntPtr.Zero)
                    return false;
                if (NETDEVSDK.NETDEV_PTZControl_Other(RevoServer.ServerHandle, int.Parse(Properties["ChannelId"]), (int)NETDEV_PTZ_E.NETDEV_PTZ_ALLSTOP, 0) == NETDEVSDK.TRUE)
                {
                    return true;
                }
                else
                {
                    Debug.Write("Error while stopping zoom " + Name + " - error " + NETDEVSDK.NETDEV_GetLastError());
                    return false;
                }
            }
        }
    }
}
