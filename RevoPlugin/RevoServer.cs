﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Runtime.InteropServices;

using eNervePluginInterface;
using NETSDKHelper;

namespace RevoPlugin
{
    public class RevoServer : ICameraServerPlugin
    {
        [NonSerialized]
        private ObservableCollection<ICameraPlugin> cameras;
        private bool connected;

        [NonSerialized]
        private IntPtr deviceHandle;
        [NonSerialized]
        private Dictionary<IntPtr, IntPtr> displays;
        [NonSerialized]
        private IntPtr realHandle;

        [NonSerialized]
        private static Dictionary<object, NETDEVSDK.NETDEV_AlarmMessCallBack_PF> sdkCallBackFuncList = new Dictionary<object, NETDEVSDK.NETDEV_AlarmMessCallBack_PF>();
        [NonSerialized]
        private static Dictionary<object, NETDEVSDK.NETDEV_ExceptionCallBack_PF> sdkExceptionCallBackList = new Dictionary<object, NETDEVSDK.NETDEV_ExceptionCallBack_PF>();

        private string fileName;
        private int portNo;
        private CustomProperties properties;
        
        public void AddDisplay(IntPtr displayHandle, IntPtr apiHandle)
        {
            if (displays == null)
                displays = new Dictionary<IntPtr, IntPtr>();
            else if (displays.ContainsKey(displayHandle))
                displays.Remove(displayHandle);
            displays.Add(displayHandle, apiHandle);
        }

        public string Author
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company; }
        }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                //result.Add(new CustomPropertyDefinition("IP", typeof(CustomProperty)));
                //result.Add(new CustomPropertyDefinition("Port", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Username", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Password", typeof(CustomProperty)));
                return result;
            }
        }

        public IntPtr GetApiHandle(IntPtr displayHandle)
        {
            if (displays == null)
                return IntPtr.Zero;
            if (displays.ContainsKey(displayHandle))
                return displays[displayHandle];
            else return IntPtr.Zero;

        }

        public void RemoveDisplay(IntPtr displayHandle)
        {
            if (displays != null)
                displays.Remove(displayHandle);
        }
        
        public override string ToString()
        {
            return Name;
        }
        
        public string Description
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description; }
        }

        public bool Enabled
        { get; set; }

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        /// <summary>
        /// This property is known to cause serialization issues over the network. Ensure it's set to null before serializing, and investigate removing it all together.
        /// </summary>
        [field:NonSerialized]        
        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public bool Online { get { return connected; } set { connected = value; } }

        public IntPtr ServerHandle { get { return deviceHandle; } }

        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }

        public string Version
        {
            get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        [field:NonSerialized]
        public ObservableCollection<ICameraPlugin> Cameras
        {
            get {
                if (cameras == null)
                    cameras = new ObservableCollection<ICameraPlugin>();
                return cameras;
            }
            set
            {
                if (value != null)
                    cameras = value;
            }
        }

        public bool IsConnected
        {
            get
            {
                return connected;
            }
        }

        [field:NonSerialized]
        public event EventHandler<AddCameraEventArgs> AddCameraEvent;
        [field: NonSerialized]
        public event EventHandler DeviceOffline;
        [field: NonSerialized]
        public event EventHandler DeviceOnline;
        [field: NonSerialized]
        public event EventHandler<PluginErrorEventArgs> Error;

        public void Start()
        {
            if (NETDEVSDK.NETDEV_Init() != NETDEVSDK.TRUE)
            {
                OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                           MethodBase.GetCurrentMethod().Name), "Unable to initialize SDK.", new Exception()));
                if (DeviceOffline!= null)
                {
                    DeviceOffline(this, new EventArgs());
                }
                return;
            }

            if (realHandle != IntPtr.Zero)
                NETDEVSDK.NETDEV_StopRealPlay(realHandle);

            NETDEVSDK.NETDEV_Logout(deviceHandle);
            realHandle = IntPtr.Zero;
            deviceHandle = IntPtr.Zero;

            // For testing - unlikely to be the case in the field.
            var configuredAddress = Properties["DeviceAddress"];
            if (string.IsNullOrEmpty(configuredAddress))
                configuredAddress = "192.168.1.232";


            IntPtr deviceInfo = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(NETDEV_DEVICE_INFO_S)));
            deviceHandle = NETDEVSDK.NETDEV_Login(configuredAddress, 0, Properties["Username"], Properties["Password"], deviceInfo);

            if (deviceHandle == IntPtr.Zero)
            {
                OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                           MethodBase.GetCurrentMethod().Name), "Unable to connect to Revo server.", new Exception()));
            }
            else
            {
                sdkCallBackFuncList.Clear();
                sdkCallBackFuncList.Add(this, AlarmMessCallBack);

                IntPtr alarmCallBack = Marshal.GetFunctionPointerForDelegate(sdkCallBackFuncList[this]);
                NETDEVSDK.NETDEV_SetAlarmCallBack(deviceHandle, alarmCallBack, IntPtr.Zero);

                sdkExceptionCallBackList.Clear();
                sdkExceptionCallBackList.Add(this, ExceptionCallBack);

                IntPtr exceptionCallBack = Marshal.GetFunctionPointerForDelegate(sdkExceptionCallBackList[this]);
                NETDEVSDK.NETDEV_SetExceptionCallBack(exceptionCallBack, IntPtr.Zero);

                int channelCount = 32;
                IntPtr videoChannelList = new IntPtr();
                videoChannelList = Marshal.AllocHGlobal(32 * Marshal.SizeOf(typeof(NETDEV_VIDEO_CHL_DETAIL_INFO_S)));

                if (NETDEVSDK.NETDEV_QueryVideoChlDetailList(deviceHandle, ref channelCount, videoChannelList) == NETDEVSDK.TRUE)
                {
                    connected = true;
                    if (DeviceOnline != null)
                    {
                        DeviceOnline(this, new EventArgs());
                    }
                    NETDEV_VIDEO_CHL_DETAIL_INFO_S channelItem;
                    for (int i = 0; i < channelCount; i++)
                    {
                        IntPtr itemPointer = new IntPtr(videoChannelList.ToInt32() + Marshal.SizeOf(typeof(NETDEV_VIDEO_CHL_DETAIL_INFO_S)) * i);
                        channelItem = (NETDEV_VIDEO_CHL_DETAIL_INFO_S)Marshal.PtrToStructure(itemPointer, typeof(NETDEV_VIDEO_CHL_DETAIL_INFO_S));

                        if (channelItem.enStatus == (int)NETDEV_CHANNEL_STATUS_E.NETDEV_CHL_STATUS_ONLINE)
                        {
                            OnAddCameraEvent(new AddCameraEventArgs(typeof(RevoCamera), "Camera " + channelItem.dwChannelID, channelItem.dwChannelID, this));
                        }
                    }
                }
                else
                {
                    OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                              MethodBase.GetCurrentMethod().Name), "Unable to get cameras from Revo server.", new Exception()));
                }
            }
            
        }

        public void Stop()
        {
            sdkCallBackFuncList.Clear();
            sdkExceptionCallBackList.Clear();
            connected = false;
        }
        private void AlarmMessCallBack(IntPtr lpUserID, int dwChannelID, NETDEV_ALARM_INFO_S stAlarmInfo, IntPtr lpBuf, int dwBufLen, IntPtr lpUserData)
        {
            if (lpUserID == deviceHandle)
            {
                //MessageBox.Show(stAlarmInfo.dwAlarmType.ToString());
            }
        }

        private void ExceptionCallBack(IntPtr lpUserID, int dwType, IntPtr stAlarmInfo, IntPtr lpExpHandle, IntPtr lpUserData)
        {
            if ((int)NETDEV_EXCEPTION_TYPE_E.NETDEV_EXCEPTION_EXCHANGE == dwType)
            {
                //MessageBox.Show("the device is offline");
            }
        }

        private void OnAddCameraEvent(AddCameraEventArgs e)
        {
            if ((AddCameraEvent != null) && (e != null))
            {
                AddCameraEvent(this, e);
            }
        }


        private void OnError(PluginErrorEventArgs e)
        {
            if ((Error != null) && (e != null))
                Error(this, e);
        }

        public override bool Equals(object obj)
        {
            RevoServer other = obj as RevoServer;
            if (other == null)
                return false;
            else
            {
                return (other.Name == Name && other.FileName == fileName);
            }
        }
    }
}
