using System;
using System.Collections.Generic;

namespace RevoPlugin
{
    public class Recording
    {
        public string Text = string.Empty;
        public string Value = string.Empty;

        public long End;
        public long Start;

        public override string ToString()
        {
            return Text;
        }
    }
}
