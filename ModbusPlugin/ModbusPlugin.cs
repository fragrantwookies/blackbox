﻿using eNervePluginInterface;
using System;
using System.Collections.Generic;
using Modbus;
using System.Reflection;
using eThele.Essentials;

namespace ModbusPlugin
{
    public class ModbusPlugin : INerveEventPlugin, IDisposable
    {
        IMaster master;

        public event EventHandler<PluginTriggerEventArgs> Trigger;                                                                  
        public event EventHandler<PluginErrorEventArgs> Error;                                                                  
        public event EventHandler DeviceOnline;                                                                  
        public event EventHandler DeviceOffline;

        #region Dispose
        ~ModbusPlugin()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            try
            {
                if (master != null)
                    master.Disconnect();

                IDisposable disposable = master as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
            catch(Exception ex)
            {
                if (Error != null)
                    Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex));
            }
        }
        #endregion


        public bool Enabled
        { get; set; }

        public bool Online { get; set; }

        #region Properties
        public string[] EventNames
        {
            get { return new string[] {"FlagChanged", "RegisterValueChanged"}; }
        }
        public bool Equals(INervePlugin other)
        {
            return (Name == other.Name && FileName == other.FileName);
        }

        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public string Description
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description; }
        }

        public string Author
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company; }
        }

        public string Version
        {
            get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
        }

        private string fileName;

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 
        #endregion

        #region Custom Properties
        private CustomProperties properties;
        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }

        //public string[] CustomPropertyNames
        //{
        //    get
        //    {
        //        return new string[] { "ComPort", "ReadInterval", "ReverseBits", "Registers" };
        //    }
        //}

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                result.Add(new CustomPropertyDefinition("ComPort", "Com Port", "Com Port", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("ReadInterval", "Read Interval", "Read Interval", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("ReverseBits", "Reverse Bits", "Reverse Bits", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("RegisterOffset", "Register Offset", "Register Offset", typeof(CustomProperty)));

                CustomPropertyDefinition cpdRegisters = new CustomPropertyDefinition("Registers", typeof(CustomPropertyList));
                CustomPropertyDefinition cpdRegister = new CustomPropertyDefinition("Register", typeof(CustomPropertyList));
                cpdRegister.ChildDefinitions.Add(new CustomPropertyDefinition("SlaveAddress", "Slave Address", "Slave Address", typeof(CustomProperty)));
                cpdRegister.ChildDefinitions.Add(new CustomPropertyDefinition("RegisterAddress", "Register Address", "Register Address", typeof(CustomProperty)));
                cpdRegister.ChildDefinitions.Add(new CustomPropertyDefinition("Flags", "Flags", "Treat value as bit flags", typeof(CustomProperty)));
                cpdRegister.ChildDefinitions.Add(new CustomPropertyDefinition("Mask", "Mask", "Mask out flags you are not interested in e.g. 11000000 will only use first to flags.", typeof(CustomProperty)));
                cpdRegisters.ChildDefinitions.Add(cpdRegister);
                result.Add(cpdRegisters);
                return result;
            }
        }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }
        #endregion

        public void Start()
        {
            if(master != null)
            {
                master.Disconnect();

                IDisposable disposable = master as IDisposable;

                if(disposable != null)
                    disposable.Dispose();
            }

            int readInterval;
            string err;
            if (!TimeSpanParser.TryParse(Properties["ReadInterval"], out readInterval, out err))
            {
                readInterval = 50;
                if (Error != null)
                    Error.Invoke(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing ReadInterval: {0}", err), null));
            }

            bool reverseBits;
            if(!bool.TryParse(Properties["ReverseBits"], out reverseBits))
                reverseBits = false;

            master = new RtuMaster() { PortName = Properties["ComPort"], ReadInterval = readInterval, ReverseRegisterFlags = reverseBits, BaudRate = 19200, DataBits = 8, StopBits = System.IO.Ports.StopBits.One, Parity = System.IO.Ports.Parity.None };
            
            List<SlaveRegister> registers = CreateRegisters();
            master.RegistersToMonitor = registers != null ? new List<SlaveRegister>(registers) : new List<SlaveRegister>();

            master.Error += master_Error;
            master.RegisterChangedFlag += master_RegisterChangedFlag;
            master.RegisterChangedAnalog += master_RegisterChangedAnalog;

            Online = master.Connect();
            if (Online)
            {
                if (DeviceOnline != null)
                    DeviceOnline(this, new EventArgs());
            }
        }

        private List<SlaveRegister> CreateRegisters()
        {
            List<SlaveRegister> registers = new List<SlaveRegister>();

            CustomPropertyList registerList = Properties.FindProperty("Registers") as CustomPropertyList;
            if (registerList != null && registerList.Properties.Count > 0)
            {
                foreach(ICustomProperty registerItem in registerList.Properties)
                {          
                    CustomPropertyList registerListItem = registerItem as CustomPropertyList;
                    if(registerListItem != null)
                    {
                        byte slaveAddress;
                        if(!byte.TryParse(registerListItem["SlaveAddress"], out slaveAddress))
                            slaveAddress = 0;

                        ushort registerAddress;
                        if(!ushort.TryParse(registerListItem["RegisterAddress"], out registerAddress))
                            registerAddress = 0;

                        bool flags;
                        if (!bool.TryParse(registerListItem["Flags"], out flags))
                            flags = false;

                        registers.Add(new SlaveRegister(slaveAddress, registerAddress, registerListItem["Mask"]) { Name = registerListItem.PropertyName, RegisterType = flags ? RegisterType.Flags : RegisterType.Analog });
                    }
                }
            }

            return registers;
        }

        void master_RegisterChangedFlag(object sender, RegisterEventFlagArgs e)
        {
            if (Trigger != null)
                Trigger(this, new PluginTriggerEventArgs(new ModbusTrigger()
                {
                    TriggerID = Guid.NewGuid(),
                    DeviceName = this.Name,
                    DetectorGroupIndex = e.RegisterAddress,
                    DetectorIndex = e.FlagIndex,
                    DetectorName = string.Format("{0} - Flag{1}", e.RegisterName, e.FlagIndex.ToString()),
                    TriggerDT = DateTime.Now,
                    EventName = string.Format("{0} - Flag{1}", e.RegisterName, e.FlagIndex.ToString()),
                    DetectorType = DetectorType.Digital,
                    State = e.FlagState,
                    Data = XMLSerializer.Serialize(e)
                }));
        }

        void master_RegisterChangedAnalog(object sender, RegisterEventAnalogArgs e)
        {
            if (Trigger != null)
                Trigger(this, new PluginTriggerEventArgs(new ModbusTrigger()
                {
                    TriggerID = Guid.NewGuid(),
                    DeviceName = this.Name,
                    DetectorGroupIndex = e.SlaveAddress,
                    DetectorIndex = e.RegisterAddress,
                    DetectorName = e.RegisterName,
                    TriggerDT = DateTime.Now,
                    EventName = e.RegisterName,
                    DetectorType = DetectorType.Analog,
                    EventValue = e.Value,
                    Data = XMLSerializer.Serialize(e)
                }));
        }

        void master_Error(object sender, ErrorEventArgs e)
        {
 	        if(Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), e.Description, e.Exception));
        }
    }
}
