﻿using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModbusPlugin
{
    class ModbusTcpPlugin : INerveEventPlugin, IDisposable
    {
        public event EventHandler<PluginTriggerEventArgs> Trigger;

        public string[] EventNames
        {
            get { throw new NotImplementedException(); }
        }

        public bool Enabled
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public INerveHostPlugin Host
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Description
        {
            get { throw new NotImplementedException(); }
        }

        public string Author
        {
            get { throw new NotImplementedException(); }
        }

        public string Version
        {
            get { throw new NotImplementedException(); }
        }

        public string FileName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool Online
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        public CustomProperties Properties
        {
            get { throw new NotImplementedException(); }
        }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get { throw new NotImplementedException(); }
        }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public event EventHandler<PluginErrorEventArgs> Error;

        public event EventHandler DeviceOnline;

        public event EventHandler DeviceOffline;

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
