﻿using eNervePluginInterface;
using eNervePluginInterface.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramPlugin
{
    public class TelegramAction : PluginAction
    {
        public TelegramAction() : base() { }

        public TelegramAction(string _name, bool _value, string _pluginName, int _actionOrder = 0) : base(_name, _value, _pluginName, _actionOrder) { }

        [DisplayInPluginActionPropertiesList]
        public string ChannelID { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string MessageText { get; set; }

        public System.DateTime TimeStamp { get; set; }
    }
}
