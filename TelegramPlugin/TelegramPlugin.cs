﻿using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Telegram.Bot;

namespace TelegramPlugin
{
    public class TelegramPlugin : INerveActionPlugin, IDisposable
    {
        private TelegramBotClient botClient;

        private int timeOut = 120000;
        private System.Timers.Timer processTimer = null;

        public event EventHandler<PluginErrorEventArgs> Error;
        public event EventHandler DeviceOnline;
        public event EventHandler DeviceOffline;

        #region Dispose
        ~TelegramPlugin()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            Online = false;

            if (processTimer != null)
            {
                processTimer.Stop();
                processTimer.Elapsed -= processTimer_Elapsed;
                processTimer.Dispose();
                processTimer = null;
            }
        } 
        #endregion

        public void Start()
        {
            try
            {
                botClient = new TelegramBotClient(Properties["BotToken"]);

                int readTimeOut;
                string err;
                if (TimeSpanParser.TryParse(Properties["TimeOut"], out readTimeOut, out err))
                    timeOut = readTimeOut > 5000 ? readTimeOut : 5000; //timeout should be at least 5 seconds.
                else
                {
                    if (Error != null)
                    {
                        Error.Invoke(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing TimeOut: {0}", err), null));
                    }
                }

                Online = true;

                if (DeviceOnline != null)
                    DeviceOnline(this, new EventArgs());

                if (processTimer == null)
                {
                    processTimer = new System.Timers.Timer();
                    processTimer.Interval = 1000;
                    processTimer.Elapsed += processTimer_Elapsed;
                }

                processTimer.Start();
            }
            catch(Exception ex)
            {
                Online = false;
            }
        }

        #region Properties
        public bool Online { get; set; }

        public bool Enabled { get; set; }

        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public string Description
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
            }
        }

        public string Author
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company;
            }
        }

        public string Version
        {
            get
            {
                return Assembly.GetCallingAssembly().GetName().Version.ToString();
            }
        }

        private string fileName;

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 
        #endregion

        #region Custom properties
        private CustomProperties properties;
        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
        }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                result.Add(new CustomPropertyDefinition("BotToken", "Bot Token", "Bot Token", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("TimeOut", "Timeout", "Timeout", typeof(CustomProperty)));
                return result;
            }
        }
        #endregion

        public Type GetActionType(string actionType = "")
        {
            return typeof(TelegramAction);
        }

        public IEnumerable<Type> GetActionTypes()
        {
            return new Type[] { typeof(TelegramAction) };
        }
        
        public bool PerformActionOld(string eventName, IEnumerable<IEventTrigger> triggers, IAction action)
        {
            bool success = false;

            TelegramAction telegramAction = action as TelegramAction;   

            if (botClient != null && telegramAction != null)
            {
                Debug.WriteLine(string.Format("Telegram action: {0}", telegramAction.MessageText));

                if (PingHost(@"web.telegram.org"))
                {
                    string triggermsgs = "";
                    foreach (IEventTrigger trigger in triggers)
                        triggermsgs += string.Format("\n\nDevice: <i>{0}</i>\nDetector: <i>{1}</i>\nDate: <i>{2}</i>\nValue: <i>{3}</i>\nSent: <i>{4}</i>", trigger.DeviceName, trigger.DetectorName, trigger.TriggerDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), trigger.DetectorType == DetectorType.Analog ? trigger.EventValue.ToString() : trigger.State.ToString(), DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                    string msg = string.Format("<b>{0} - {1}</b>{2}", eventName, telegramAction.MessageText, triggermsgs);

                    DateTime startTimeStamp = DateTime.Now;

                    Task<Telegram.Bot.Types.Message> result = botClient.SendTextMessageAsync(telegramAction.ChannelID, msg, false, false, 0, null, Telegram.Bot.Types.Enums.ParseMode.Html);

                    if (result.Wait(10000))
                    {
                        Debug.WriteLine(string.Format("Time to send to telegram: {0}", (DateTime.Now - startTimeStamp).TotalMilliseconds.ToString()));

                        if (result.Result.Text.Contains(telegramAction.MessageText))
                            success = true;
                        else
                            Debug.WriteLine(string.Format("Telegram replied, but message didn't match."));
                            //if (Error != null)
                            //    Error(this, new PluginErrorEventArgs("PerformAction", string.Format("Telegram replied, but message didn't match. {0} vs {1}", telegramAction.MessageText, result.Result.Text), null));
                    }
                    else

                        Debug.WriteLine(string.Format("Sending of Telegram message timed out."));
                        //if (Error != null)
                        //    Error(this, new PluginErrorEventArgs("PerformAction", "Sending of Telegram message timed out.", null));
                }
                else
                {
                    Debug.WriteLine(string.Format("Failed to ping telegram.org."));
                    //if (Error != null)
                    //    Error(this, new PluginErrorEventArgs("PerformAction", "Failed to ping telegram.org", null));
                }
            }

            return success;
        }

        public bool PerformAction(string eventName, IEnumerable<IEventTrigger> triggers, IAction action)
        {
            bool success = false;

            TelegramAction telegramAction = action as TelegramAction;

            if (botClient != null && telegramAction != null)
            {
                Debug.WriteLine(string.Format("Telegram action: {0}", telegramAction.MessageText));

                if (PingHost(@"web.telegram.org"))
                {
                    string triggermsgs = "";
                    foreach (IEventTrigger trigger in triggers)
                        triggermsgs += string.Format("\n\nDevice: <i>{0}</i>\nDetector: <i>{1}</i>\nDate: <i>{2}</i>\nValue: <i>{3}</i>", trigger.DeviceName, trigger.DetectorName, trigger.TriggerDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), trigger.DetectorType == DetectorType.Analog ? trigger.EventValue.ToString() : trigger.State.ToString());

                    string msg = string.Format("<b>{0} - {1}</b>{2}", eventName, telegramAction.MessageText, triggermsgs);

                    TelegramMessage message = new TelegramMessage() { ChannelID = telegramAction.ChannelID, Message = msg, OriginalMessage = telegramAction.MessageText, Retries = 0 };

                    lock (messageQueue)
                    {
                        messageQueue.Enqueue(message);
                    }

                    success = true;
                }
                else
                {
                    Debug.WriteLine(string.Format("Failed to ping telegram.org."));
                    if (Error != null)
                        Error(this, new PluginErrorEventArgs("PerformAction", "Failed to ping telegram.org", null));
                }
            }

            return success;
        }

        private class TelegramMessage
        {
            public string ChannelID { get; set; }
            public string Message { get; set; }
            public string OriginalMessage { get; set; }
            public int Retries { get; set; }
        }

        private Queue<TelegramMessage> messageQueue = new Queue<TelegramMessage>();

        private bool busyProcessingMessages = false;

        void processTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            processTimer.Stop();
            ProcessQueue();
            processTimer.Start();
        }

        private void ProcessQueue()
        {
            try
            {
                if (!busyProcessingMessages)
                {
                    busyProcessingMessages = true;

                    while (messageQueue.Count > 0)
                    {
                        TelegramMessage msg = null;

                        lock (messageQueue)
                        {
                            msg = messageQueue.Dequeue();
                        }

                        if (msg != null)
                        {
                            DateTime startTimeStamp = DateTime.Now;

                            bool success = false;

                            string retries = "";
                            if (msg.Retries > 0)
                                retries = string.Format("\n\U000026A0 Retry#: {0} (Time out: {1}s)", msg.Retries.ToString(), (timeOut / 1000).ToString());

                            Task<Telegram.Bot.Types.Message> result = botClient.SendTextMessageAsync(msg.ChannelID, string.Format("{0}\nSent: <i>{1}</i>{2}", msg.Message, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), retries), false, false, 0, null, Telegram.Bot.Types.Enums.ParseMode.Html);

                            if (result.Wait(timeOut > 5000 ? timeOut : 5000))
                            {
                                Debug.WriteLine(string.Format("Time to send to telegram: {0}", (DateTime.Now - startTimeStamp).TotalMilliseconds.ToString()));

                                if (result.Result.Text.Contains(msg.OriginalMessage))
                                    success = true;
                                else
                                {
                                    Debug.WriteLine(string.Format("Telegram replied, but message didn't match."));
                                    if (Error != null)
                                        Error(this, new PluginErrorEventArgs("PerformAction", string.Format("Telegram replied, but original message is not part of the reply: {0} vs {1}", msg.OriginalMessage, result.Result.Text), null));
                                }
                            }
                            else
                            {
                                //if we time out we want to give telegram some breathing space. We'll increase it exponencially based on number of retries for this specific message.
                                //obviously this will keep the whole queue waiting, so we don't want to make the wait too long. We'll therefore cap it on 5 minutes.
                                int sleepTime = (int)(Math.Pow(2, (msg.Retries + 1)));
                                sleepTime = (sleepTime > 300 ? 300 : sleepTime) * 1000; //cap it on 5 minutes 

                                Debug.WriteLine(string.Format("Sending of Telegram message timed out. Will wait for {0} millisconds.", sleepTime.ToString()));
                                if (Error != null)
                                    Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Sending of Telegram message timed out.", null));
                                Thread.Sleep(sleepTime);
                            }

                            if (!success && msg.Retries++ < 10)
                            {
                                lock (messageQueue)
                                {
                                    messageQueue.Enqueue(msg);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (Error != null)
                    Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Sending of Telegram message timed out.", ex));
            }
            finally
            {
                busyProcessingMessages = false;
            }
        }

        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }
    }
}
