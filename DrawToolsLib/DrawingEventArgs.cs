using System;
using System.Collections.Generic;

namespace DrawToolsLib
{
    public class DrawingEventArgs : EventArgs
    {
        public GraphicsBase Drawing {get; set;}

        public DrawingEventArgs(GraphicsBase _Drawing)
        {
            Drawing = Drawing;
        }
    }
}
