﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DrawToolsLib
{
    class ToolPicture : ToolRectangleBase
    {
        DrawingCanvas drawingCanvas;

        public ToolPicture(DrawingCanvas drawingCanvas)
        {
            this.drawingCanvas = drawingCanvas;
            MemoryStream stream = new MemoryStream(Properties.Resources.Rectangle);
            ToolCursor = new Cursor(stream);
        }

        public override void OnMouseDown(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(drawingCanvas);            

            AddNewObject(drawingCanvas,
                new GraphicsPicture(
                p.X,
                p.Y,
                p.X + 1,
                p.Y + 1,
                0,
                Properties.Resources.placeholder.ToWpfBitmap(),
                drawingCanvas.ActualScale));
        }

        public override void OnMouseUp(DrawingCanvas drawingCanvas, MouseButtonEventArgs e)
        {
            base.OnMouseUp(drawingCanvas, e);
        }
    }
}
