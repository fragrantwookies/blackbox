﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace DrawToolsLib
{
    public class PropertiesGraphicsPicture : PropertiesGraphicsBase
    {
        private BitmapSource img;
        private double rotation;
        private double left;
        private double top;
        private double right;
        private double bottom;

        public PropertiesGraphicsPicture() {}

        public PropertiesGraphicsPicture(GraphicsPicture graphicsPicture)
        {
            if (graphicsPicture == null)
            {
                throw new ArgumentNullException("graphicsPicture");
            }

            this.img = graphicsPicture.Image;
            this.rotation = graphicsPicture.Rotation;
            this.left = graphicsPicture.Left;
            this.top = graphicsPicture.Top;
            this.right = graphicsPicture.Right;
            this.bottom = graphicsPicture.Bottom;
            this.actualScale = graphicsPicture.ActualScale;
            this.ID = graphicsPicture.Id;
            this.selected = graphicsPicture.IsSelected;
        }

        public override GraphicsBase CreateGraphics()
        {
            GraphicsBase b = new GraphicsPicture(left, top, right, bottom, rotation, img, actualScale);

            if (this.ID != 0)
            {
                b.Id = this.ID;
                b.IsSelected = this.selected;
            }

            return b;
        }

        #region Properties
        public BitmapSource Image
        {
            get { return img; }
            set { img = value; }
        }

        public double Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }

        public double Left
        {
            get { return left; }
            set { left = value; }
        }

        public double Top
        {
            get { return top; }
            set { top = value; }
        }

        public double Right
        {
            get { return right; }
            set { right = value; }
        }

        public double Bottom
        {
            get { return bottom; }
            set { bottom = value; }
        }
        #endregion Properties
    }
}
