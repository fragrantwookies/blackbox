﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace DrawToolsLib
{
    public class GraphicsPicture : GraphicsRectangleBase
    {
        private BitmapSource img;
        private double rotation;

        private List<Tuple<BitmapSource, int>> frames;
        private int frameIndex = 0;

        private Timer gifFrameTimer;

        #region Constructors

        public GraphicsPicture(double left, double top, double right, double bottom, double _rotation, string imageFilePath, double actualScale)
        {
            this.rectangleLeft = left;
            this.rectangleTop = top;
            this.rectangleRight = right;
            this.rectangleBottom = bottom;
            this.rotation = _rotation;

            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(imageFilePath);
            bitmap.EndInit();

            this.img = bitmap;

            InitFrames(this.img);

            this.ActualScale = actualScale;
        }

        public GraphicsPicture(double left, double top, double right, double bottom, double _rotation, BitmapSource image, double actualScale)
        {
            this.rectangleLeft = left;
            this.rectangleTop = top;
            this.rectangleRight = right;
            this.rectangleBottom = bottom;
            this.rotation = _rotation;
            this.img = image;
            InitFrames(image);

            this.ActualScale = actualScale;
        }

        private void InitFrames(BitmapSource image)
        {
            if (image != null)
            {
                BitmapDecoder decoder = GetDecoder(image);

                if (decoder.Frames.Count > 1)
                {
                    if (frames == null)
                        frames = new List<Tuple<BitmapSource, int>>();
                    else
                        frames.Clear();
                    BitmapSource prevFrame = null;
                    FrameInfo prevInfo = null;
                    foreach (var rawFrame in decoder.Frames)
                    {
                        var info = GetFrameInfo(rawFrame);
                        var frame = MakeFrame(image, rawFrame, info, prevFrame, prevInfo);

                        frames.Add(new Tuple<BitmapSource, int>(frame, (int)info.Delay.TotalMilliseconds));
                    }                    

                    if(gifFrameTimer == null)
                    {
                        gifFrameTimer = new Timer();
                        gifFrameTimer.Elapsed += gifFrameTimer_Elapsed;
                    }

                    frameIndex = 0;

                    gifFrameTimer.Interval = frames[frameIndex].Item2;
                }
                else
                {
                    if (frames != null)
                        frames.Clear();
                }
            }
        }

        void gifFrameTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (gifFrameTimer.Enabled)
                gifFrameTimer.Stop();

            if (this.Dispatcher.CheckAccess())
                this.RefreshDrawing();
            else
                this.Dispatcher.Invoke((Action)(() => this.RefreshDrawing()));

            if (!gifFrameTimer.Enabled)
                gifFrameTimer.Start();
        }
        #endregion Constructors

        #region Properties        
        public BitmapSource Image
        {
            get { return img; }
            set 
            { 
                img = value;
                InitFrames(img);
                this.RefreshDrawing();
            }
        }

        public double Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }
        #endregion Properties

        private DrawingContext currentDrawingContext;
        
        public override void Draw(System.Windows.Media.DrawingContext drawingContext)
        {
            if (drawingContext == null)
            {
                throw new ArgumentNullException("drawingContext");            
            }

            lock (drawingContext)
            {
                if (gifFrameTimer != null && gifFrameTimer.Enabled && frames != null && frames.Count > 0)
                    gifFrameTimer.Stop();

                if (currentDrawingContext != drawingContext)
                    currentDrawingContext = drawingContext;

                Rect rect = Rectangle;

                drawingContext.PushClip(new RectangleGeometry(rect));

                BitmapSource frame = null;

                if (frames != null && frames.Count > 0)
                {
                    if (frameIndex >= frames.Count)
                        frameIndex = 0;

                    frame = frames[frameIndex].Item1;
                    gifFrameTimer.Interval = frames[frameIndex].Item2;
                    frameIndex++;
                }
                else
                    frame = img;

                if (rotation > 0)
                {
                    var transform = new RotateTransform(rotation, frame.Width / 2, frame.Height / 2);
                    drawingContext.PushTransform(transform);
                }

                drawingContext.DrawImage(frame, rect);

                drawingContext.Pop();

                if (IsSelected)
                {
                    drawingContext.DrawRectangle(
                        null,
                        new Pen(new SolidColorBrush(graphicsObjectColor), ActualLineWidth),
                        rect);
                }

                // Draw tracker
                base.Draw(drawingContext);
            }

            if (gifFrameTimer != null && !gifFrameTimer.Enabled && frames != null && frames.Count > 0)
                gifFrameTimer.Start();
        }        

        public override bool Contains(Point point)
        {
            return this.Rectangle.Contains(point);
        }

        public override PropertiesGraphicsBase CreateSerializedObject()
        {
            return new PropertiesGraphicsPicture(this);
        }

        #region Animated Gifs
        private static BitmapDecoder GetDecoder(BitmapSource image)
        {
            BitmapDecoder decoder = null;
            var frame = image as BitmapFrame;
            if (frame != null)
                decoder = frame.Decoder;

            if (decoder == null)
            {
                var bmp = image as BitmapImage;
                if (bmp != null)
                {
                    if (bmp.StreamSource != null)
                    {
                        bmp.StreamSource.Position = 0;
                        decoder = BitmapDecoder.Create(bmp.StreamSource, bmp.CreateOptions, bmp.CacheOption);
                    }
                    else if (bmp.UriSource != null)
                    {
                        Uri uri = bmp.UriSource;
                        if (bmp.BaseUri != null && !uri.IsAbsoluteUri)
                            uri = new Uri(bmp.BaseUri, uri);
                        decoder = BitmapDecoder.Create(uri, bmp.CreateOptions, bmp.CacheOption);
                    }
                }
            }

            return decoder;
        }

        private static BitmapSource MakeFrame(BitmapSource fullImage, BitmapSource rawFrame, FrameInfo frameInfo, BitmapSource previousFrame, FrameInfo previousFrameInfo)
        {
            DrawingVisual visual = new DrawingVisual();
            using (var context = visual.RenderOpen())
            {
                if (previousFrameInfo != null && previousFrame != null &&
                    previousFrameInfo.DisposalMethod == FrameDisposalMethod.Combine)
                {
                    var fullRect = new Rect(0, 0, fullImage.PixelWidth, fullImage.PixelHeight);
                    context.DrawImage(previousFrame, fullRect);
                }

                context.DrawImage(rawFrame, frameInfo.Rect);
            }
            var bitmap = new RenderTargetBitmap(
                fullImage.PixelWidth, fullImage.PixelHeight,
                fullImage.DpiX, fullImage.DpiY,
                PixelFormats.Pbgra32);
            bitmap.Render(visual);
            return bitmap;
        }

        private class FrameInfo
        {
            public TimeSpan Delay { get; set; }
            public FrameDisposalMethod DisposalMethod { get; set; }
            public double Width { get; set; }
            public double Height { get; set; }
            public double Left { get; set; }
            public double Top { get; set; }

            public Rect Rect
            {
                get { return new Rect(Left, Top, Width, Height); }
            }
        }

        private enum FrameDisposalMethod
        {
            Replace = 0,
            Combine = 1,
            RestoreBackground = 2,
            RestorePrevious = 3
        }

        private static FrameInfo GetFrameInfo(BitmapFrame frame)
        {
            var frameInfo = new FrameInfo
            {
                Delay = TimeSpan.FromMilliseconds(100),
                DisposalMethod = FrameDisposalMethod.Replace,
                Width = frame.PixelWidth,
                Height = frame.PixelHeight,
                Left = 0,
                Top = 0
            };

            BitmapMetadata metadata;
            try
            {
                metadata = frame.Metadata as BitmapMetadata;
                if (metadata != null)
                {
                    const string delayQuery = "/grctlext/Delay";
                    const string disposalQuery = "/grctlext/Disposal";
                    const string widthQuery = "/imgdesc/Width";
                    const string heightQuery = "/imgdesc/Height";
                    const string leftQuery = "/imgdesc/Left";
                    const string topQuery = "/imgdesc/Top";

                    var delay = metadata.GetQueryOrNull<ushort>(delayQuery);
                    if (delay.HasValue)
                        frameInfo.Delay = TimeSpan.FromMilliseconds(10 * delay.Value);

                    var disposal = metadata.GetQueryOrNull<byte>(disposalQuery);
                    if (disposal.HasValue)
                        frameInfo.DisposalMethod = (FrameDisposalMethod)disposal.Value;

                    var width = metadata.GetQueryOrNull<ushort>(widthQuery);
                    if (width.HasValue)
                        frameInfo.Width = width.Value;

                    var height = metadata.GetQueryOrNull<ushort>(heightQuery);
                    if (height.HasValue)
                        frameInfo.Height = height.Value;

                    var left = metadata.GetQueryOrNull<ushort>(leftQuery);
                    if (left.HasValue)
                        frameInfo.Left = left.Value;

                    var top = metadata.GetQueryOrNull<ushort>(topQuery);
                    if (top.HasValue)
                        frameInfo.Top = top.Value;
                }
            }
            catch (NotSupportedException)
            {
            }

            return frameInfo;
        }
        #endregion
    }

    public static class BitmapMetaExtension
    {
        public static T? GetQueryOrNull<T>(this BitmapMetadata metadata, string query)
        where T : struct
        {
            if (metadata.ContainsQuery(query))
            {
                object value = metadata.GetQuery(query);
                if (value != null)
                    return (T)value;
            }
            return null;
        }
    }
}
