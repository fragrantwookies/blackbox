﻿using eNervePluginInterface;
using eNervePluginInterface.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnmpPlugin
{
    class SendV1TrapAction : PluginAction
    {
        public SendV1TrapAction() : base() { }

        public SendV1TrapAction(string _name, bool _value, string _pluginName, int _actionOrder = 0) : base(_name, _value, _pluginName, _actionOrder) { }

        [DisplayInPluginActionPropertiesList]
        public string IPAddress { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string Port { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string OID { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string Community { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string Variables { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string GenericCode { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string SpecificCode { get; set; }
    }
}
