﻿using eNervePluginInterface;
using eNervePluginInterface.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnmpPlugin
{
    class SnmpGetAction : PluginAction
    {
        public SnmpGetAction() : base() { }

        public SnmpGetAction(string _name, bool _value, string _pluginName, int _actionOrder = 0) : base(_name, _value, _pluginName, _actionOrder) { }

        [DisplayInPluginActionPropertiesList]
        public string Version { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string IPAddress { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string Port { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string OID { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string Community { get; set; }
    }
}
