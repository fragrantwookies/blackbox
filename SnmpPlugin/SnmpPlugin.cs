﻿using eNervePluginInterface;
using eThele.Essentials;
using Lextm.SharpSnmpLib;
using Lextm.SharpSnmpLib.Messaging;
using Lextm.SharpSnmpLib.Pipeline;
using SnmpPlugin.Wizard;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SnmpPlugin
{
    public class SnmpPlugin : INerveEventPlugin, INerveActionPlugin, IDisposable
    {
        private SnmpEngine engine;

        public event EventHandler<PluginTriggerEventArgs> Trigger;                                                                  
        public event EventHandler<PluginErrorEventArgs> Error;                                                                  
        public event EventHandler DeviceOnline;                                                                  
        public event EventHandler DeviceOffline;

        private const int defaultGetSetPort = 161; 
        private const int defaultReceivePort = 162;
        private const string defaultCommunity = "public";
        private const string defaultVersion = "V2";
        private const int defaultGetTimeout = 250;
        private const int defaultGlobalPollInterval = 1000;

        private bool IShouldPoll = false;

        private DateTime timeEnabledUtc;

        private Dictionary<string, double> previousValues = new Dictionary<string, double>();

        #region Dispose
        ~SnmpPlugin()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {   
            Online = false;

            IShouldPoll = false;

            if (engine != null)
            {
                engine.Stop();
                engine.Dispose();
                engine = null;
            }            

            if (previousValues != null)
                previousValues.Clear();
        }
        #endregion
        
        public bool Online { get; set; }

        public bool Enabled { get; set; }

        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public string Description
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
            }
        }

        public string Author
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company;
            }
        }

        public string Version
        {
            get
            {
                return Assembly.GetCallingAssembly().GetName().Version.ToString();
            }
        }

        private string fileName;

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public string[] EventNames
        {
            get 
            {
                return new string[] { "V1TrapReceived", "V2TrapReceived", "SnmpGetReceived" };
            }
        }

        public bool HasWizard
        {
            get
            {
                return true;
            }
        }

        private IPluginWizard wizard = null;

        public IPluginWizard Wizard
        {
            get
            {
                if (wizard == null)
                    wizard = new SnmpWizard();

                return wizard;
            }
        } 

        private CustomProperties properties;
        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
        }


        private List<CustomPropertyDefinition> customPropertyDefinitions = null;

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                if (customPropertyDefinitions != null)
                    return customPropertyDefinitions;
                else
                {
                    customPropertyDefinitions = new List<CustomPropertyDefinition>();
                    customPropertyDefinitions.Add(new CustomPropertyDefinition("Port", "Snmp Port", "Snmp Port", typeof(CustomProperty)) { DefaultValue = defaultGetSetPort.ToString() });
                    customPropertyDefinitions.Add(new CustomPropertyDefinition("GlobalPollInterval", "Global Poll Interval", "Interval between Polls (Default is 1000ms)", typeof(CustomProperty)) { DefaultValue = "1000" });
                    CustomPropertyDefinition cpdCommunityNames = new CustomPropertyDefinition("CommunityNames", "Community Names", "Community Names", typeof(CustomPropertyList));
                    cpdCommunityNames.ChildDefinitions.Add(new CustomPropertyDefinition("CommunityName", "Community Name", "Community Name", typeof(CustomProperty)));
                    customPropertyDefinitions.Add(cpdCommunityNames);

                    CustomPropertyDefinition cpdSnmpGetPolls = new CustomPropertyDefinition("GetPolls", "Get Polls", "List of Gets to poll", typeof(CustomPropertyList));
                    CustomPropertyDefinition cpdSnmpGetPoll = new CustomPropertyDefinition("GetPoll", "Get Poll Item", "Item to do a get poll on", typeof(CustomPropertyList));
                    cpdSnmpGetPoll.ChildDefinitions.Add(new CustomPropertyDefinition("IP", "IP Address", "IP Address", typeof(CustomProperty)));
                    cpdSnmpGetPoll.ChildDefinitions.Add(new CustomPropertyDefinition("Port", "Port", string.Format("Port (Default is {0})", defaultGetSetPort.ToString()), typeof(CustomProperty)) { DefaultValue = defaultGetSetPort.ToString() });
                    cpdSnmpGetPoll.ChildDefinitions.Add(new CustomPropertyDefinition("Community", "Community", string.Format("Community (Default is {0})", defaultCommunity), typeof(CustomProperty)) { DefaultValue = defaultCommunity });
                    cpdSnmpGetPoll.ChildDefinitions.Add(new CustomPropertyDefinition("Version", "Version", string.Format("Version (Default is {0})", defaultVersion), typeof(CustomProperty)) { DefaultValue = defaultVersion });
                    cpdSnmpGetPoll.ChildDefinitions.Add(new CustomPropertyDefinition("TimeOut", "TimeOut", string.Format("TimeOut (Default is {0}ms)", defaultGetTimeout.ToString()), typeof(CustomProperty)) { DefaultValue = defaultGetTimeout.ToString() });
                    cpdSnmpGetPoll.ChildDefinitions.Add(new CustomPropertyDefinition("Interval", "Custom Interval", "Make it bigger than 0 (ms) to create a custom interval, otherwise the global timer will be used", typeof(CustomProperty)) { DefaultValue = "0" });
                    CustomPropertyDefinition cpdOids = new CustomPropertyDefinition("OIDs", "OIDs", "List of OIDs", typeof(CustomPropertyList));
                    cpdOids.ChildDefinitions.Add(new CustomPropertyDefinition("OID", "OID", "OID", typeof(CustomProperty)));
                    cpdSnmpGetPoll.ChildDefinitions.Add(cpdOids);

                    cpdSnmpGetPolls.ChildDefinitions.Add(cpdSnmpGetPoll);
                    customPropertyDefinitions.Add(cpdSnmpGetPolls);
                    return customPropertyDefinitions;
                }
            }
        }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }

        public void Start()
        {
            if (engine == null)
            {
                //Create the SNMP engine and handlers
                var v1Handler = new TrapV1MessageHandler();
                var v2Handler = new TrapV2MessageHandler();
                var informHandler = new InformRequestMessageHandler();

                CustomPropertyList communityNamesProperties = Properties.FindProperty("CommunityNames") as CustomPropertyList;

                IMembershipProvider[] memberships = null;

                if (communityNamesProperties != null)
                {
                    var v1Memberships = communityNamesProperties.Properties.Select(each => (IMembershipProvider)new Version1MembershipProvider(new OctetString(each.PropertyValue), new OctetString(each.PropertyValue)));
                    var v2Memberships = communityNamesProperties.Properties.Select(each => (IMembershipProvider)new Version2MembershipProvider(new OctetString(each.PropertyValue), new OctetString(each.PropertyValue)));
                    memberships = v1Memberships.Concat(v2Memberships).ToArray();            
                }

                engine = new SnmpEngine(new SnmpApplicationFactory(new ObjectStore(), new ComposedMembershipProvider(memberships),
                    new MessageHandlerFactory(new HandlerMapping[] 
                { 
                    new HandlerMapping("v1", "TRAPV1", v1Handler),
                    new HandlerMapping("v2,v3", "TRAPV2", v2Handler),
                    new HandlerMapping("v2,v3", "INFORM", informHandler)
                })), new Listener(), new EngineGroup());

                //Add the bindings to recieve all local trap messages on default port.
                engine.Listener.ClearBindings();
                int port;
                if (!int.TryParse(Properties["Port"], out port))
                    port = defaultReceivePort;
                if (port == 0)
                    port = defaultReceivePort;

                if (Socket.OSSupportsIPv4)
                    engine.Listener.AddBinding(new IPEndPoint(IPAddress.Any, port));

                if (Socket.OSSupportsIPv6)
                    engine.Listener.AddBinding(new IPEndPoint(IPAddress.IPv6Any, port));

                //Add handlers                
                v1Handler.MessageReceived += v1Handler_MessageReceived;
                v2Handler.MessageReceived += v2Handler_MessageReceived;
                engine.ExceptionRaised += engine_ExceptionRaised;
            }
            else
                engine.Stop();

            engine.Start();

            Thread pollThread = new Thread(() => StartPolling());
            pollThread.Start();

            timeEnabledUtc = DateTime.Now;

            Online = true;
            if (DeviceOnline != null)
                DeviceOnline(this, new EventArgs());
        }
                
        /// <summary>
        /// This should be threaded
        /// </summary>
        private void StartPolling()
        {
            IShouldPoll = true;

            List<GetPollProperties> getPollPropertiesList = new List<GetPollProperties>();

            int globalPollInterval;
            string err;
            if(!TimeSpanParser.TryParse(Properties["GlobalPollInterval"], out globalPollInterval, out err))
                globalPollInterval = defaultGlobalPollInterval;
            if(globalPollInterval <= 0)
                globalPollInterval = defaultGlobalPollInterval;

            CustomPropertyList gets = Properties.FindProperty("GetPolls") as CustomPropertyList;
            
            if(gets != null)
            {
                foreach(ICustomProperty g in gets.Properties)
                {
                    CustomPropertyList get = g as CustomPropertyList;

                    if(get != null)
                    {         
                        IPAddress ip;
                        if (!IPAddress.TryParse(get["IP"], out ip))
                            continue;

                        int port;
                        if (!int.TryParse(get["Port"], out port))
                            port = defaultGetSetPort;
                        if (port == 0)
                            port = defaultGetSetPort;

                        string community = get["Community"];
                        if (string.IsNullOrWhiteSpace(community))
                            community = defaultCommunity;

                        string version = get["Version"];
                        if (string.IsNullOrWhiteSpace(version))
                            version = defaultVersion;

                        int timeout;
                        if (!TimeSpanParser.TryParse(get["TimeOut"], out timeout, out err))
                        {
                            timeout = defaultGetTimeout;
                            if (Error != null)
                                Error.Invoke(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing TimeOut: {0}", err), null));
                        }

                        int interval;
                        if (!TimeSpanParser.TryParse(get["Interval"], out interval, out err))
                        {
                            interval = 0;
                            if (Error != null)
                                Error.Invoke(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing Interval: {0}", err), null));
                        }

                        CustomPropertyList oids = get.FindProperty("OIDs") as CustomPropertyList;

                        List<Variable> oidVars = new List<Variable>();

                        if (oids != null && oids.Properties != null)
                        {   
                            foreach(ICustomProperty oid in oids.Properties)
                            {
                                Variable oidVariable = new Variable(new ObjectIdentifier(oid.PropertyValue));
                                oidVars.Add(oidVariable);
                            }
                        }

                        if (oidVars.Count <= 0)
                            continue;

                        GetPollProperties getPollProperties = new GetPollProperties() { Address = new IPEndPoint(ip, port), Community = new OctetString(community), Version = ParseSnmpVersion(version), Timeout = timeout, Interval = interval, OIDs = oidVars };

                        getPollPropertiesList.Add(getPollProperties);
                    }
                }
            }

            if(getPollPropertiesList.Count > 0)
            {
                for (int i = getPollPropertiesList.Count - 1; i >= 0; i-- )
                {
                    GetPollProperties getPollProperties = getPollPropertiesList[i];

                    if (getPollProperties.Interval > 0)
                    {
                        Thread pollThread = new Thread(() => Poll(getPollProperties, getPollProperties.Interval));
                        pollThread.Start();

                        getPollPropertiesList.RemoveAt(i);
                    }
                }
            }

            //if there's anything left, well poll them together here
            if (getPollPropertiesList.Count > 0)
            {
                Stopwatch stopwatch = new Stopwatch();

                while (IShouldPoll)
                {
                    stopwatch.Start();

                    foreach (GetPollProperties getPollProperties in getPollPropertiesList)
                    {
                        try
                        {
                            IList<Variable> results = Messenger.Get(getPollProperties.Version, getPollProperties.Address, getPollProperties.Community, getPollProperties.OIDs, getPollProperties.Timeout);

                            foreach (Variable var in results)
                            {
                                string oid = var.Id.ToString();
                                double val;
                                if (double.TryParse(var.Data.ToString(), out val))
                                {
                                    if (IsDifferent(string.Format("{0}#{1}", getPollProperties.IPAddress.ToString(), oid), val))
                                    {
                                        if (Trigger != null)
                                        {
                                            Trigger(this, new PluginTriggerEventArgs(new SnmpTrigger()
                                            {
                                                TriggerID = Guid.NewGuid(),
                                                DeviceName = this.Name,
                                                DetectorGroupIndex = -1,
                                                DetectorIndex = -1,
                                                DetectorName = string.Format("{0}#{1}#{2}", "SnmpGetReceived", getPollProperties.IPAddress.ToString(), var.Id.ToString().TrimStart('.')),
                                                TriggerDT = DateTime.Now,
                                                DetectorType = eNervePluginInterface.DetectorType.Analog,
                                                EventValue = val
                                            }));
                                        }
                                    }
                                }
                            }
                        }
                        catch (Lextm.SharpSnmpLib.Messaging.TimeoutException ex)
                        { }
                        catch (Exception ex)
                        { }
                    }

                    stopwatch.Stop();

                    Debug.WriteLine(string.Format("GlobalPoll time elapsed: {0}", stopwatch.ElapsedMilliseconds.ToString()));

                    if (IShouldPoll)
                    {
                        int sleeptime = globalPollInterval - (int)stopwatch.ElapsedMilliseconds;

                        Thread.Sleep(sleeptime > 0 ? sleeptime : 0);
                    }

                    stopwatch.Reset();
                }                
            }                    
        }

        /// <summary>
        /// This should also get it's own thread
        /// </summary>
        /// <param name="_getPollProperties"></param>
        /// <param name="_interval">If this is bigger than 0, it should not exit, but keep looping with interval (minus previous time elapsed) as sleep</param>
        private void Poll(GetPollProperties _getPollProperties, int _interval = 0)
        {
            Stopwatch stopwatch = new Stopwatch();

            while (IShouldPoll)
            {
                stopwatch.Start();

                try
                {
                    IList<Variable> results = Messenger.Get(_getPollProperties.Version, _getPollProperties.Address, _getPollProperties.Community, _getPollProperties.OIDs, _getPollProperties.Timeout);

                    ThreadPool.QueueUserWorkItem(state => ProcessResult(results, _getPollProperties));                    
                }
                catch (Lextm.SharpSnmpLib.Messaging.TimeoutException ex)
                {}
                catch(Exception ex)
                {}

                stopwatch.Stop();

                Debug.WriteLine(string.Format("{0} poll time elapsed: {1}", _getPollProperties.IPAddress.ToString(), stopwatch.ElapsedMilliseconds.ToString()));

                if (IShouldPoll)
                {
                    int sleeptime = _interval - (int)stopwatch.ElapsedMilliseconds;

                    Thread.Sleep(sleeptime > 0 ? sleeptime : 0);
                }

                stopwatch.Reset();
            }
        }

        private void ProcessResult(IList<Variable> results, GetPollProperties _getPollProperties)
        {
            foreach (Variable var in results)
            {
                string oid = var.Id.ToString();
                double val;
                if (double.TryParse(var.Data.ToString(), out val))
                {
                    if (IsDifferent(string.Format("{0}#{1}", _getPollProperties.IPAddress.ToString(), oid), val))
                    {
                        if (Trigger != null)
                        {
                            Trigger(this, new PluginTriggerEventArgs(new SnmpTrigger()
                            {
                                TriggerID = Guid.NewGuid(),
                                DeviceName = this.Name,
                                DetectorGroupIndex = -1,
                                DetectorIndex = -1,
                                DetectorName = string.Format("{0}#{1}#{2}", "SnmpGetReceived", _getPollProperties.IPAddress.ToString(), oid.TrimStart('.')),
                                TriggerDT = DateTime.Now,
                                DetectorType = eNervePluginInterface.DetectorType.Analog,
                                EventValue = val,
                                State = (val == 0 ? false : true)
                            }));
                        }
                    }
                }
            }
        }

        private bool IsDifferent(string _key, double _val)
        {
            bool result = true;

            if (previousValues == null)
                previousValues = new Dictionary<string, double>();
            else
            {
                double previousVal;
                if (!previousValues.TryGetValue(_key, out previousVal))
                {
                    lock (previousValues)
                    {
                        previousValues.Add(_key, _val);
                    }
                }
                else if (previousVal == _val)
                    result = false;
                else
                {
                    lock (previousValues)
                    {
                        previousValues[_key] = _val;
                    }
                }
            }

            return result;
        }

        private Tuple<string, string>[] ConvertVariablesToTupleArray(IList<Variable> variables)
        {
            Tuple<string, string>[] result = new Tuple<string, string>[variables.Count];

            for (int i = 0; i < variables.Count; i++ )
            {
                Variable v = variables[i];
                result[i] = new Tuple<string, string>(v.Id.ToString(), v.Data.ToString());
            }

            return result;
        }

        private void v1Handler_MessageReceived(object sender, TrapV1MessageReceivedEventArgs e)
        {
            if (Trigger != null)
            {
                var ip = e.TrapV1Message.AgentAddress.ToString();
                var oid = e.TrapV1Message.Enterprise.ToString();
                var community = e.TrapV1Message.Community.ToString();
                var variables = e.TrapV1Message.Variables();
                string serialisedVars = XMLSerializer.Serialize(ConvertVariablesToTupleArray(variables));

                Trigger(this, new PluginTriggerEventArgs(new SnmpTrigger()
                    {
                        TriggerID = Guid.NewGuid(),
                        DeviceName = this.Name,
                        DetectorGroupIndex = -1,
                        DetectorIndex = -1,
                        DetectorName = string.Format("{0}#{1}#{2}", "V1TrapReceived", ip, oid),
                        TriggerDT = DateTime.Now,
                        DetectorType = eNervePluginInterface.DetectorType.Digital,
                        State = true,
                        Data = serialisedVars
                    }));
            }
        }

        private void v2Handler_MessageReceived(object sender, TrapV2MessageReceivedEventArgs e)
        {
            if (Trigger != null)
            {
                var ip = e.Sender.Address.ToString();
                var oid = e.TrapV2Message.Enterprise.ToString();
                var community = e.TrapV2Message.Community().ToString();
                var variables = e.TrapV2Message.Variables();
                string serialisedVars = XMLSerializer.Serialize(ConvertVariablesToTupleArray(variables));

                Trigger(this, new PluginTriggerEventArgs(new SnmpTrigger()
                    {
                        TriggerID = Guid.NewGuid(),
                        DeviceName = this.Name,
                        DetectorGroupIndex = -1,
                        DetectorIndex = -1,
                        DetectorName = string.Format("{0}#{1}#{2}", "V2TrapReceived", ip, oid),
                        TriggerDT = DateTime.Now,
                        DetectorType = eNervePluginInterface.DetectorType.Digital,
                        State = true,
                        Data = serialisedVars
                    }));
            }
        }

        private void engine_ExceptionRaised(object sender, ExceptionRaisedEventArgs e)
        {
            if(Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", e.Exception));
        }

        private static Dictionary<string, Type> ActionTypeDictionary = new Dictionary<string, Type>(StringComparer.InvariantCultureIgnoreCase) 
        { 
            { "SnmpSetAction", typeof(SnmpSetAction) }, 
            { "SnmpGetAction", typeof(SnmpGetAction) } 
        };

        public Type GetActionType(string actionType = "")
        {
            Type result = null;
            try
            {
                result = ActionTypeDictionary[actionType];
            }
            catch (KeyNotFoundException)
            {
                result = typeof(SnmpGetAction);
            }            
            
            return result;
        }

        public IEnumerable<Type> GetActionTypes()
        {
            return ActionTypeDictionary.Values.ToArray<Type>();
        }

        private VersionCode ParseSnmpVersion(string _versionString)
        {
            VersionCode result;

            switch(_versionString)
            {
                case"V1":
                    result = VersionCode.V1;
                    break;
                case "V2":
                    result = VersionCode.V2;
                    break;
                default:
                    result = VersionCode.V3;
                    break;
            }

            return result;
        }

        private GenericCode ParseSnmpV1TrapGenericCode(string _genericCode)
        {
            GenericCode result;

            switch(_genericCode)
            {
                case "ColdStart":
                case "0":
                    result = GenericCode.ColdStart;
                    break;
                case "WarmStart":
                case "1":
                    result = GenericCode.WarmStart;
                    break;
                case "LinkDown":
                case "2":
                    result = GenericCode.LinkDown;
                    break;
                case "LinkUp":
                case "3":
                    result = GenericCode.LinkUp;
                    break;
                case "AuthenticationFailure":
                case "4":
                    result = GenericCode.AuthenticationFailure;
                    break;
                case "EgpNeighborLoss":
                case "5":
                    result = GenericCode.EgpNeighborLoss;
                    break;
                case "EnterpriseSpecific":
                case "6":
                    result = GenericCode.EnterpriseSpecific;
                    break;
                default:
                    result = GenericCode.ColdStart;
                    break;
            }

            return result;
        }

        private List<Variable> ParseVariablesToList(string variables)
        {
            char variableSeperator = ","[0];
            char valueSeperator = "="[0];

            if (variables == null)
                return new List<Variable>();

            return variables.Split(variableSeperator)
                .Where(each => each.Contains(valueSeperator))
                .Select(each => new { Key = each.Split(valueSeperator)[0], Value = each.Split(valueSeperator)[1] })
                .Select(each => new Variable(new ObjectIdentifier(each.Key), new OctetString(each.Value)))
                .ToList();
        }

        public bool PerformAction(string eventName, IEnumerable<IEventTrigger> triggers, IAction action)
        {
            bool success = false;

            try
            {
                if (action is SnmpGetAction)
                {
                    SnmpGetAction snmpGetAction = (SnmpGetAction)action;
                    int port;
                    if (!int.TryParse(snmpGetAction.Port, out port))
                        port = defaultGetSetPort;
                    if (port == 0)
                        port = defaultGetSetPort;

                    if (string.IsNullOrEmpty(snmpGetAction.Community))
                        snmpGetAction.Community = defaultCommunity;

                    var ipAddress = IPAddress.Parse(snmpGetAction.IPAddress);

                    IPEndPoint ipendpoint = new IPEndPoint(ipAddress, port);
                    OctetString octetstring = new OctetString(snmpGetAction.Community);
                    List<Variable> variables = new List<Variable> { new Variable(new ObjectIdentifier(snmpGetAction.OID)) };

                    var result = Messenger.Get(ParseSnmpVersion(snmpGetAction.Version), ipendpoint, octetstring, variables, 60000);

                    if (Trigger != null)
                    {
                        string serialisedVars = XMLSerializer.Serialize(ConvertVariablesToTupleArray(result));

                        Trigger(this, new PluginTriggerEventArgs(new SnmpTrigger()
                        {
                            TriggerID = Guid.NewGuid(),
                            DeviceName = this.Name,
                            DetectorGroupIndex = -1,
                            DetectorIndex = -1,
                            DetectorName = string.Format("{0}#{1}#{2}", "SnmpGetReceived", snmpGetAction.IPAddress, snmpGetAction.OID),
                            TriggerDT = DateTime.Now,
                            DetectorType = eNervePluginInterface.DetectorType.Digital,
                            State = true,
                            Data = serialisedVars
                        }));
                    }
                }
                else if (action is SnmpSetAction)
                {
                    SnmpSetAction snmpSetAction = (SnmpSetAction)action;
                    int port;
                    if (!int.TryParse(snmpSetAction.Port, out port))
                        port = defaultGetSetPort;
                    if (port == 0)
                        port = defaultGetSetPort;

                    if (string.IsNullOrEmpty(snmpSetAction.Community))
                        snmpSetAction.Community = defaultCommunity;

                    var ipAddress = IPAddress.Parse(snmpSetAction.IPAddress);

                    IPEndPoint ipendpoint = new IPEndPoint(ipAddress, port);
                    OctetString octetstring = new OctetString(snmpSetAction.Community);
                    List<Variable> variables = new List<Variable> { new Variable(new ObjectIdentifier(snmpSetAction.OID), new OctetString(snmpSetAction.NewValue)) };

                    Messenger.Set(ParseSnmpVersion(snmpSetAction.Version), ipendpoint, octetstring, variables, 60000);
                }
                else if (action is SendV1TrapAction)
                {
                    SendV1TrapAction sendV1TrapAction = (SendV1TrapAction)action;
                    int port;
                    if (!int.TryParse(sendV1TrapAction.Port, out port))
                        port = defaultGetSetPort;
                    if (port == 0)
                        port = defaultGetSetPort;

                    if (string.IsNullOrEmpty(sendV1TrapAction.Community))
                        sendV1TrapAction.Community = defaultCommunity;

                    var ipAddress = IPAddress.Parse(sendV1TrapAction.IPAddress);
                    var timeStamp = Convert.ToUInt32((DateTime.UtcNow - timeEnabledUtc).TotalMilliseconds / 100);
                    int specificCode;
                    if (!int.TryParse(sendV1TrapAction.SpecificCode, out specificCode))
                        specificCode = 0;

                    Messenger.SendTrapV1(new IPEndPoint(ipAddress, port), IPAddress.Loopback, new OctetString(sendV1TrapAction.Community),
                        new ObjectIdentifier(sendV1TrapAction.OID), ParseSnmpV1TrapGenericCode(sendV1TrapAction.GenericCode), specificCode, timeStamp, ParseVariablesToList(sendV1TrapAction.Variables));
                }
                else if (action is SendV2TrapAction)
                {
                    SendV2TrapAction sendV2TrapAction = (SendV2TrapAction)action;
                    int port;
                    if (!int.TryParse(sendV2TrapAction.Port, out port))
                        port = defaultGetSetPort;
                    if (port == 0)
                        port = defaultGetSetPort;

                    if (string.IsNullOrEmpty(sendV2TrapAction.Community))
                        sendV2TrapAction.Community = defaultCommunity;

                    var ipAddress = IPAddress.Parse(sendV2TrapAction.IPAddress);
                    var timeStamp = Convert.ToUInt32((DateTime.UtcNow - timeEnabledUtc).TotalMilliseconds / 100);

                    int requestIdentifier;
                    if (!int.TryParse(sendV2TrapAction.RequestIdentifier, out requestIdentifier))
                        requestIdentifier = 0;

                    Messenger.SendTrapV2(requestIdentifier, VersionCode.V2, new IPEndPoint(ipAddress, port), new OctetString(sendV2TrapAction.Community),
                        new ObjectIdentifier(sendV2TrapAction.OID), timeStamp, ParseVariablesToList(sendV2TrapAction.Variables));
                }
            }
            catch(Exception ex)            
            {
                if (Error != null)
                    Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex));
            }

            return success;
        }

        public bool Equals(INervePlugin other)
        {
            if (Author == other.Author
                    && Description == other.Description
                    && FileName == other.FileName
                    && Host == other.Host
                    && Name == other.Name
                    && Online == other.Online
                    && Version == other.Version)
                return true;
            else
                return false;
        }
    }
}
