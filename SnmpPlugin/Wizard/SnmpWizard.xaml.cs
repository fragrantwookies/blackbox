﻿using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_UI;

namespace SnmpPlugin.Wizard
{
    /// <summary>
    /// Interaction logic for SnmpWizard.xaml
    /// </summary>
    public partial class SnmpWizard : UserControl, IPluginWizard, IDisposable
    {
        UIDevice backupDevice = null;
        UIDevice device = null;

        List<UIAlarmName> templates = new List<UIAlarmName>();

        List<UICondition> conditionsToAdd = null;
        List<UICondition> conditionsToRemove = null;

        List<UIAlarmName> actionsToAdd = null;
        List<UIAlarmName> actionsToRemove = null;

        public SnmpWizard()
        {
            InitializeComponent();
        }

        ~SnmpWizard()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(backupDevice != null)
                backupDevice.Dispose();

            if(device != null)
                device.Dispose();

            ClearConditionsToAdd();

            ClearConditionsToRemove();

            ClearActionsToAdd();

            ClearActionsToRemove();
        }

        private string PluginName
        {
            get
            {
                string result = "";

                if (!string.IsNullOrWhiteSpace(UIDevice))
                {
                    UIDevice device = XMLSerializer.Deserialize(this.UIDevice) as UIDevice;

                    if (device != null)
                        result = device.Name;
                }

                if (string.IsNullOrWhiteSpace(result) && Plugin != null)
                {
                    result = Plugin.Name;
                }

                return result;
            }
        }

        private void ClearConditionsToAdd()
        {
            if (conditionsToAdd != null)
            {
                for (int i = conditionsToAdd.Count - 1; i >= 0; i--)
                {
                    UICondition c = conditionsToAdd[i];
                    conditionsToAdd.RemoveAt(i);
                    c.Dispose();
                }
            }
        }

        private void ClearConditionsToRemove()
        {
            if (conditionsToRemove != null)
            {
                for (int i = conditionsToRemove.Count - 1; i >= 0; i--)
                {
                    UICondition c = conditionsToRemove[i];
                    conditionsToRemove.RemoveAt(i);
                    c.Dispose();
                }
            }
        }

        private void ClearActionsToAdd()
        {
            if (actionsToAdd != null)
            {
                for (int i = actionsToAdd.Count - 1; i >= 0; i--)
                {
                    UIAlarmName a = actionsToAdd[i];
                    actionsToAdd.RemoveAt(i);
                    a.Dispose();
                }
            }
        }

        private void ClearActionsToRemove()
        {
            if (actionsToRemove != null)
            {
                for (int i = actionsToRemove.Count - 1; i >= 0; i--)
                {
                    UIAlarmName a = actionsToRemove[i];
                    actionsToRemove.RemoveAt(i);
                    a.Dispose();
                }
            }
        }

        public INervePlugin Plugin { get; set; }

        public string UIDevice
        {
            get
            {
                return XMLSerializer.Serialize(backupDevice);
            }
            set
            {
                backupDevice = XMLSerializer.Deserialize(value) as UIDevice;
            }
        }

        public string[] TemplateActions
        {
            set
            {
                if(value != null && value.Length > 0)
                {
                    foreach (string t in value)
                    {
                        UIAlarmName template = XMLSerializer.Deserialize(t) as UIAlarmName;

                        if(template != null)
                            templates.Add(template);
                    }
                }
            }
        }

        [field:NonSerialized]
        [NonSerializedXML]
        public event EventHandler<PluginWizardSavedDetailsEventArgs> Saved;

        private void xInvert_Click(object sender, RoutedEventArgs e)
        {
            if (xInvert.IsChecked.Value)
            {
                xKeep.IsEnabled = false;
                xUpdate.IsEnabled = false;
            }
            else
            {
                xKeep.IsEnabled = true;
                xUpdate.IsEnabled = true;
            }
        }

        private void xKeep_Click(object sender, RoutedEventArgs e)
        {
            if (xKeep.IsChecked.Value)
            {
                xInvert.IsEnabled = true;
                xUpdate.IsEnabled = true;
                xUpdate.IsChecked = true;
            }
            else
            {
                xInvert.IsEnabled = false;
                xUpdate.IsEnabled = false;
                xUpdate.IsChecked = false;
            }
        }

        private void btnRun_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lstLog.Items.Clear();
                lstLog.Visibility = System.Windows.Visibility.Visible;

                bool keep = xKeep.IsChecked.Value;
                bool update = xUpdate.IsChecked.Value;
                bool invert = xInvert.IsChecked.Value;

                device = ObjectCopier.CloneXML<UIDevice>(backupDevice);
                UICustomPropertyList polls = FindGetPolls(device);

                ClearConditionsToAdd();

                ClearConditionsToRemove();

                ClearActionsToAdd();

                ClearActionsToRemove();                                

                if (!keep)
                {
                    DeleteCondition("*", "*");
                    DeleteAction("*", "*");

                    if (polls != null)
                        polls.Clear();

                    ClearGroupsAndDetectors();
                }

                List<Tuple<string, string>> ipList = GetIPAddressesAndFriendlyNames();
                List<Tuple<string, string, string>> oidList = GetOidsFriendlyNamesAndAlarmNames();

                if (!invert)
                {
                    CreateBasicProperties();

                    UICustomPropertyList propGetpolls = FindPropertyByName("GetPolls", device.Properties) as UICustomPropertyList;

                    if (keep)
                    {
                        foreach (var ip in ipList)
                        {
                            UICustomPropertyList poll = FindPollByIP(ip.Item1, polls);

                            if (poll != null)
                            {
                                if (update) //if it was found only update it when the option to update is set to true.
                                {
                                    foreach (var oid in oidList)
                                    {
                                        DeleteOidProperty(oid.Item1, poll);
                                        AddOidProperty(oid.Item1, poll);
                                        lstLog.Items.Add(string.Format("Added OID: {0} to {1}", oid.Item1, ip.Item1));

                                        DeleteDetector(string.Format("SnmpGetReceived#{0}#{1}", ip.Item1, oid.Item1));
                                        UIDetector detector = CreateDetector(ip.Item1, oid.Item1, GetGroup(ip.Item1));

                                        DeleteCondition(ip.Item1, oid.Item1);
                                        DeleteAction(ip.Item1, oid.Item1);

                                        if (detector != null)
                                            CreateCondition(string.Format("{0} {1}", ip.Item2, oid.Item2), ip.Item1, oid.Item1, detector);

                                        CreateAction(string.Format("{0} {1}", ip.Item2, oid.Item2), ip.Item1, oid.Item1, oid.Item3);
                                    }
                                }
                            }
                            else
                            {
                                propGetpolls.Properties.Add(CreatePoll(ip.Item1, oidList, txtPort.Text, txtCommunity.Text, txtVersion.Text, txtTimeout.Text));

                                lstLog.Items.Add(string.Format("Created poll: {0}", ip.Item1));

                                if (update)
                                {
                                    foreach (var oid in oidList)
                                        DeleteDetector(string.Format("SnmpGetReceived#{0}#{1}", ip.Item1, oid.Item1));
                                }

                                foreach (var oid in oidList)
                                {
                                    UIDetector detector = CreateDetector(ip.Item1, oid.Item1, GetGroup(ip.Item1));

                                    if (update)
                                    {
                                        DeleteCondition(ip.Item1, oid.Item1);
                                        DeleteAction(ip.Item1, oid.Item1);
                                    }

                                    if (detector != null)
                                        CreateCondition(string.Format("{0} {1}", ip.Item2, oid.Item2), ip.Item1, oid.Item1, detector);

                                    CreateAction(string.Format("{0} {1}", ip.Item2, oid.Item2), ip.Item1, oid.Item1, oid.Item3);
                                }
                            }
                        }
                    }
                    else //list will be clear se we can just add
                    {
                        foreach (var ip in ipList)
                        {
                            propGetpolls.Properties.Add(CreatePoll(ip.Item1, oidList, txtPort.Text, txtCommunity.Text, txtVersion.Text, txtTimeout.Text));

                            lstLog.Items.Add(string.Format("Created poll: {0}", ip.Item1));

                            foreach (var oid in oidList)
                            {
                                UIDetector detector = CreateDetector(ip.Item1, oid.Item1, GetGroup(ip.Item1));

                                if (detector != null)
                                    CreateCondition(string.Format("{0} {1}", ip.Item2, oid.Item2), ip.Item1, oid.Item1, detector);

                                CreateAction(string.Format("{0} {1}", ip.Item2, oid.Item2), ip.Item1, oid.Item1, oid.Item3);
                            }
                        }
                    }
                }
                else //delete
                {
                    if (ipList.Count > 0 && !string.IsNullOrWhiteSpace(ipList[0].Item1))
                    {
                        foreach (var ip in ipList)
                        {
                            UICustomPropertyList poll = FindPollByIP(ip.Item1, polls);
                            if (oidList.Count > 0 && !string.IsNullOrWhiteSpace(oidList[0].Item1))
                            {
                                foreach (var oid in oidList)
                                {
                                    DeleteOidProperty(oid.Item1, poll);
                                    lstLog.Items.Add(string.Format("OID removed from {0}: {1}", ip.Item1, oid.Item1));

                                    DeleteDetector(string.Format("SnmpGetReceived#{0}#{1}", ip.Item1, oid.Item1));

                                    DeleteCondition(ip.Item1, oid.Item1);
                                    DeleteAction(ip.Item1, oid.Item1);
                                }
                            }
                            else
                            {
                                DeletePollByIP(ip.Item1, polls);
                                poll.Clear();
                                poll.Dispose();
                                lstLog.Items.Add(string.Format("Removed poll: {0}", ip.Item1));
                                DeleteDetectorsPartialMatch(string.Format("SnmpGetReceived#{0}#", ip.Item1), "");
                                DeleteCondition(ip.Item1, "*");
                                DeleteAction(ip.Item1, "*");
                            }
                        }
                    }
                    else if (oidList.Count > 0 && !string.IsNullOrWhiteSpace(oidList[0].Item1))
                    {
                        foreach (UICustomPropertyList poll in polls.Properties)
                        {
                            foreach (var oid in oidList)
                            {
                                //UICustomProperty propOid = FindOidProperty(oid.Item1, poll);
                                //if (propOid != null)
                                //{
                                //    poll.Properties.Remove(propOid);
                                //    propOid.Dispose();
                                //    lstLog.Items.Add(string.Format("OID removed from {0}: {1}", FindPropertyByName("IP", poll).Value, oid.Item1));

                                //    DeleteCondition("*", oid.Item1);
                                //    DeleteAction("*", oid.Item1);
                                //}

                                DeleteOidProperty(oid.Item1, poll);
                                lstLog.Items.Add(string.Format("OID removed from {0}: {1}", FindPropertyByName("IP", poll).Value, oid.Item1));

                                DeleteCondition("*", oid.Item1);
                                DeleteAction("*", oid.Item1);
                            }
                        }

                        foreach(var oid in oidList)
                            DeleteDetectorsPartialMatch("SnmpGetReceived#", oid.Item1);
                    }
                    else
                    {
                        if(polls != null)
                            polls.Clear();

                        lstLog.Items.Add("Removed all polls");

                        ClearGroupsAndDetectors();

                        DeleteCondition("*", "*");
                        DeleteAction("*", "*");
                    }
                }

                lstLog.Items.Add("Done");
            }
            catch(Exception ex)
            {
                lstLog.Items.Add(string.Format("Error occurred: {0}", ex.Message));
            }
        }

        #region Detectors
        
        private UIDetector FindDetector(string _detectorName)
        {
            UIDetector result = null;

            if (device != null && device.Groups != null && device.Groups.Count > 0)
            {
                foreach (UIDetectorGroup group in device.Groups)
                {
                    bool found = false;

                    foreach (UIIOItem ioItem in group.IOs)
                    {
                        UIDetector detector = ioItem as UIDetector;

                        if (detector != null && detector.Name == _detectorName)
                        {
                            result = detector;
                            found = true;
                            break;
                        }
                    }

                    if (found)
                        break;
                }
            }

            return result;
        }

        private UIDetectorGroup GetGroup(string _groupName)
        {
            UIDetectorGroup result = null;

            foreach(UIDetectorGroup group in device.Groups)
            {
                if(group.Name == _groupName)
                {
                    result = group;
                    break;
                }
            }

            if (result == null)
            {
                result = new UIDetectorGroup() { Name = _groupName };
                result.Parent = device;
                device.Groups.Add(result);
                lstLog.Items.Add(string.Format("Group added: {0}", _groupName));
            }

            return result;
        }

        private UIDetector CreateDetector(string _ip, string _oid, UIDetectorGroup _group)
        {
            UIDetector detector = new UIDetector(true)
            {
                Name = string.Format("SnmpGetReceived#{0}#{1}", _ip, _oid),
                IOType = "AnalogIn"
            };

            detector.Parent = _group;

            int i = _group.IOs.IndexOf(detector);
            if (i < 0)
            {
                _group.IOs.Add(detector);

                lstLog.Items.Add(string.Format("Detector added: {0}", detector.Name));
            }
            else
                detector = _group.IOs[i] as UIDetector;

            return detector;
        }

        private void DeleteDetector(string _detectorName)
        {
            if (device != null && device.Groups != null && device.Groups.Count > 0)
            {
                for (int i = device.Groups.Count - 1; i >= 0; i--)
                {
                    UIDetectorGroup group = device.Groups[i];

                    if (group.IOs != null && group.IOs.Count > 0)
                    {
                        for (int j = group.IOs.Count - 1; j >= 0; j--)
                        {
                            UIDetector detector = group.IOs[j] as UIDetector;

                            if (detector != null && detector.Name == _detectorName)
                            {
                                group.IOs.RemoveAt(j);
                                detector.Dispose();
                                lstLog.Items.Add(string.Format("Removed detector: {0}", _detectorName));
                            }
                        }
                    }
                }
            }
        }

        private void DeleteDetectorsPartialMatch(string _startsWith, string _endsWith)
        {
            if (device != null && device.Groups != null && device.Groups.Count > 0)
            {
                for (int i = device.Groups.Count - 1; i >= 0; i--)
                {
                    UIDetectorGroup group = device.Groups[i];

                    if (group.IOs != null && group.IOs.Count > 0)
                    {
                        for (int j = group.IOs.Count - 1; j >= 0; j--)
                        {
                            UIDetector detector = group.IOs[j] as UIDetector;

                            if (detector != null 
                                && (detector.Name.Contains(_startsWith) || string.IsNullOrWhiteSpace(_startsWith))
                                && (detector.Name.Contains(_endsWith) || string.IsNullOrWhiteSpace(_endsWith)))
                            {
                                string detectorName = detector.Name;
                                group.IOs.RemoveAt(j);
                                detector.Dispose();
                                lstLog.Items.Add(string.Format("Removed detector: {0}", detectorName));
                            }
                        }
                    }
                }
            }
        }

        private void ClearGroupsAndDetectors()
        {
            if (device != null && device.Groups != null && device.Groups.Count > 0)
            {
                for (int i = device.Groups.Count - 1; i >= 0; i--)
                {
                    UIDetectorGroup group = device.Groups[0];

                    if (group.IOs != null && group.IOs.Count > 0)
                    {
                        for (int j = group.IOs.Count - 1; j >= 0; j--)
                        {
                            UIIOItem item = group.IOs[j];

                            group.IOs.RemoveAt(j);
                            item.Dispose();
                        }
                    }

                    device.Groups.RemoveAt(i);
                    group.Dispose();
                }

                lstLog.Items.Add("Cleared all detectors");
            }
        }

        #endregion
                
        #region Properties
        private void CreateBasicProperties()
        {
            UICustomPropertyList propGetpolls = FindPropertyByName("GetPolls", device.Properties) as UICustomPropertyList;
            if (propGetpolls == null)
            {
                propGetpolls = new UICustomPropertyList(GetDefinition("GetPolls", this.Plugin.CustomPropertyDefinitions));
                device.Properties.Add(propGetpolls);
            }

            UICustomProperty propIsPlugin = FindPropertyByName("IsPlugin", device.Properties);
            if (propIsPlugin == null)
            {
                propIsPlugin = new UICustomProperty(GetDefinition("IsPlugin", this.Plugin.CustomPropertyDefinitions)) { Value = true.ToString() };
                device.Properties.Add(propIsPlugin);
            }

            UICustomProperty propPort = FindPropertyByName("Port", device.Properties);
            if (propPort == null)
            {
                propPort = new UICustomProperty(GetDefinition("Port", this.Plugin.CustomPropertyDefinitions));
                device.Properties.Add(propPort);
            }

            UICustomProperty propGlobalPollInterval = FindPropertyByName("GlobalPollInterval", device.Properties);
            if (propGlobalPollInterval == null)
            {
                propGlobalPollInterval = new UICustomProperty(GetDefinition("GlobalPollInterval", this.Plugin.CustomPropertyDefinitions));
                device.Properties.Add(propGlobalPollInterval);
            }

            UICustomPropertyList propCommunityNames = FindPropertyByName("CommunityNames", device.Properties) as UICustomPropertyList;
            if (propCommunityNames == null)
            {
                CustomPropertyDefinition defCommunityNames = GetDefinition("CommunityNames", this.Plugin.CustomPropertyDefinitions);
                propCommunityNames = new UICustomPropertyList(defCommunityNames);
                propCommunityNames.Properties.Add(new UICustomProperty(GetDefinition("CommunityName", defCommunityNames.ChildDefinitions)) { Value = "public" });
                device.Properties.Add(propCommunityNames);
            }

            lstLog.Items.Add("Created basic properties");
        }

        private static CustomPropertyDefinition GetDefinition(string _propertyName, List<CustomPropertyDefinition> _definitions)
        {
            CustomPropertyDefinition result = null;

            if (_definitions != null)
            {
                foreach (CustomPropertyDefinition def in _definitions)
                {
                    if (def.PropertyName == _propertyName)
                    {
                        result = def;
                        break;
                    }
                }
            }

            return result;
        }
        
        private UICustomPropertyList CreatePoll(string _ip, List<Tuple<string, string, string>> _oids, string _port, string _community, string _version, string _timeout)
        {
            UICustomPropertyList poll = null;

            if (!string.IsNullOrWhiteSpace(_ip) && _oids != null && _oids.Count > 0)
            {
                CustomPropertyDefinition defPolls = GetDefinition("GetPolls", this.Plugin.CustomPropertyDefinitions);
                CustomPropertyDefinition defPoll = GetDefinition("GetPoll", defPolls.ChildDefinitions);
                CustomPropertyDefinition defIp = GetDefinition("IP", defPoll.ChildDefinitions);
                CustomPropertyDefinition defPort = GetDefinition("Port", defPoll.ChildDefinitions);
                CustomPropertyDefinition defCommunity = GetDefinition("Community", defPoll.ChildDefinitions);
                CustomPropertyDefinition defVersion = GetDefinition("Version", defPoll.ChildDefinitions);
                CustomPropertyDefinition defTimeOut = GetDefinition("TimeOut", defPoll.ChildDefinitions);
                CustomPropertyDefinition defInterval = GetDefinition("Interval", defPoll.ChildDefinitions);
                CustomPropertyDefinition defOids = GetDefinition("OIDs", defPoll.ChildDefinitions);
                CustomPropertyDefinition defOid = GetDefinition("OID", defOids.ChildDefinitions);


                poll = new UICustomPropertyList(defPoll);

                UICustomProperty propIP = FindPropertyByName(defIp.PropertyName, poll);
                if (propIP != null)
                    propIP.Value = _ip;
                else
                    poll.Properties.Add(new UICustomProperty(defIp) { Value = _ip });

                UICustomProperty propPort = FindPropertyByName(defPort.PropertyName, poll);
                if (propPort != null)
                    propPort.Value = _port;
                else
                    poll.Properties.Add(new UICustomProperty(defPort) { Value = _port });

                UICustomProperty propCommunity = FindPropertyByName(defCommunity.PropertyName, poll);
                if (propCommunity != null)
                    propCommunity.Value = _community;
                else
                    poll.Properties.Add(new UICustomProperty(defCommunity) { Value = _community });

                UICustomProperty propVersion = FindPropertyByName(defVersion.PropertyName, poll);
                if (propVersion != null)
                    propVersion.Value = _version;
                else
                    poll.Properties.Add(new UICustomProperty(defVersion) { Value = _version });

                UICustomProperty propTimeOut = FindPropertyByName(defTimeOut.PropertyName, poll);
                if (propTimeOut != null)
                    propTimeOut.Value = _timeout;
                else
                    poll.Properties.Add(new UICustomProperty(defTimeOut) { Value = _timeout });

                UICustomProperty propInterval = FindPropertyByName(defInterval.PropertyName, poll);
                if (propInterval != null)
                    propInterval.Value = "0";
                else
                    poll.Properties.Add(new UICustomProperty(defInterval) { Value = "0" });

                UICustomPropertyList propOids = FindPropertyByName(defOids.PropertyName, poll) as UICustomPropertyList;
                if (propOids == null)
                {
                    propOids = new UICustomPropertyList(defOids);
                    poll.Properties.Add(propOids);
                }

                propOids.Clear();

                foreach (var oid in _oids)
                    propOids.Properties.Add(new UICustomProperty(defOid) { Value = oid.Item1 });
            }

            return poll;
        }

        private static UICustomPropertyList FindGetPolls(WPF_UI.UIDevice _device)
        {
            UICustomPropertyList result = null;

            foreach (UICustomProperty prop in _device.Properties)
            {
                if (prop.Name.Equals("GetPolls", StringComparison.InvariantCultureIgnoreCase) && prop is UICustomPropertyList)
                {
                    result = prop as UICustomPropertyList;
                    break;
                }
            }

            return result;
        }

        private static UICustomProperty FindOidProperty(string _oid, UICustomPropertyList _pollProperty)
        {
            UICustomProperty result = null;

            if (_pollProperty != null)
            {
                UICustomPropertyList oids = FindPropertyByName("OIDs", _pollProperty) as UICustomPropertyList;
                if (oids != null)
                {
                    foreach (UICustomProperty oid in oids.Properties)
                    {
                        if (oid.Value.Equals(_oid, StringComparison.InvariantCultureIgnoreCase))
                        {
                            result = oid;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        private static void AddOidProperty(string _oid, UICustomPropertyList _pollProperty)
        {
            if (_pollProperty != null)
            {
                CustomPropertyDefinition defOids = GetDefinition("OIDs", _pollProperty.PropertyDefinition.ChildDefinitions);
                CustomPropertyDefinition defOid = GetDefinition("OID", defOids.ChildDefinitions);

                UICustomPropertyList propOids = FindPropertyByName(defOids.PropertyName, _pollProperty) as UICustomPropertyList;
                if (propOids == null)
                {
                    propOids = new UICustomPropertyList(defOids);
                    _pollProperty.Properties.Add(propOids);
                }

                propOids.Properties.Add(new UICustomProperty(defOid) { Value = _oid });
            }
        }

        private static void DeleteOidProperty(string _oid, UICustomPropertyList _pollProperty)
        {
            if (_pollProperty != null)
            {
                UICustomPropertyList oids = FindPropertyByName("OIDs", _pollProperty) as UICustomPropertyList;
                if (oids != null && oids.Properties.Count > 0)
                {
                    for (int i = oids.Properties.Count - 1; i >= 0; i--)
                    {
                        UICustomProperty oid = oids.Properties[i];

                        if (oid.Value.Equals(_oid, StringComparison.InvariantCultureIgnoreCase))
                        {
                            oids.Properties.RemoveAt(i);
                        }
                    }
                }
            }
        }

        private static UICustomPropertyList FindPollByIP(string _ipAddress, UICustomPropertyList _polls)
        {
            UICustomPropertyList result = null;

            if (_polls != null)
            {
                foreach (UICustomPropertyList poll in _polls.Properties)
                {
                    UICustomProperty p = FindPropertyByName("IP", poll);

                    if (p != null && p.Value != null && p.Value.Equals(_ipAddress, StringComparison.InvariantCultureIgnoreCase))
                    {
                        result = poll as UICustomPropertyList;
                        break;
                    }
                }
            }

            return result;
        }

        private static void DeletePollByIP(string _ipAddress, UICustomPropertyList _polls)
        {
            if (_polls != null)
            {
                for (int i = _polls.Properties.Count - 1; i >= 0; i--)
                {
                    UICustomPropertyList poll = _polls.Properties[i] as UICustomPropertyList;
                    if (poll != null)
                    {
                        UICustomProperty p = FindPropertyByName("IP", poll);

                        if (p != null && p.Value != null && p.Value.Equals(_ipAddress, StringComparison.InvariantCultureIgnoreCase))
                        {
                            _polls.Properties.RemoveAt(i);
                        }
                    }
                }
            }
        }

        private static UICustomProperty FindPropertyByName(string _name, UICustomPropertyList _list)
        {
            return FindPropertyByName(_name, _list.Properties);
        }

        private static UICustomProperty FindPropertyByName(string _name, ObservableCollection<UICustomProperty> _list)
        {
            UICustomProperty result = null;

            if (_list != null)
            {
                foreach (UICustomProperty p in _list)
                {
                    if (p.Name.Equals(_name, StringComparison.InvariantCultureIgnoreCase))
                        result = p;
                }
            }

            return result;
        }
        #endregion
        
        private List<Tuple<string, string>> GetIPAddressesAndFriendlyNames()
        {
            List<Tuple<string, string>> resultList = new List<Tuple<string, string>>();

            List<string> lines = GetLinesFromTextbox(txtIpAdresses);

            if(lines != null && lines.Count > 0)
            {
                foreach(string line in lines)
                {
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        string[] items = line.Split(new char[] { ';', ',' });

                        if (items != null)
                        {
                            Tuple<string, string> item = null;
                            if (items.Length == 1)
                                item = new Tuple<string, string>(items[0], "");
                            else if (items.Length >= 2)
                                item = new Tuple<string, string>(items[0], items[1]);

                            if (item != null)
                                resultList.Add(item);
                        }
                    }
                }
            }

            return resultList;
        }

        private List<Tuple<string, string, string>> GetOidsFriendlyNamesAndAlarmNames()
        {
            List<Tuple<string, string, string>> resultList = new List<Tuple<string, string, string>>();

            List<string> lines = GetLinesFromTextbox(txtOids);

            if (lines != null && lines.Count > 0)
            {
                foreach (string line in lines)
                {
                    if (!string.IsNullOrWhiteSpace(line))
                    {
                        string[] items = line.Split(new char[] { ';', ',' });

                        if (items != null)
                        {
                            Tuple<string, string, string> item = null;
                            if (items.Length == 1)
                                item = new Tuple<string, string, string>(items[0], "", "");
                            else if (items.Length == 2)
                                item = new Tuple<string, string, string>(items[0], items[1], "");
                            else if (items.Length >= 3)
                                item = new Tuple<string, string, string>(items[0], items[1], items[2]);

                            if (item != null)
                                resultList.Add(item);
                        }
                    }
                }
            }

            return resultList;
        }

        private static List<string> GetLinesFromTextbox(TextBox _textbox)
        {
            List<string> items = new List<string>();

            if(_textbox != null)
            {
                for (int i = 0; i < _textbox.LineCount; i++)
                {
                    string line = _textbox.GetLineText(i).Trim();

                    if(!string.IsNullOrWhiteSpace(line))
                        items.Add(line);
                }
            }

            return items;
        }

        #region Conditions
        private UICondition CreateCondition(string _name, string _ip, string _oid, UIDetector _detector)
        {
            UICondition resultCondition = null;

            if (xConditions.IsChecked.Value)
            {
                int cooldown;
                if (!int.TryParse(txtCondCooldown.Text, out cooldown))
                    cooldown = 0;

                resultCondition = new UICondition()
                {
                    Name = _name,
                    HiddenField = string.Format("{0};{1};{2}", PluginName, _ip, _oid),
                    TimeFrame = 0,
                    Cooldown = cooldown,
                    PercentageTriggersNeeded = 100,
                    CauseAlarm = cbxCondCauseAlarm.IsChecked.Value
                };

                int triggersToRaiseEvent;
                if (!int.TryParse(txtCondTriggersToRaiseAlarm.Text, out triggersToRaiseEvent))
                    triggersToRaiseEvent = 1;

                int triggersToRaiseEventTimeframe;
                if (!int.TryParse(txtCondTriggersToRaiseAlarmTimeframe.Text, out triggersToRaiseEventTimeframe))
                    triggersToRaiseEventTimeframe = 0;

                double minThreshold;
                if (!double.TryParse(txtCondMinThreshold.Text, out minThreshold))
                    minThreshold = 0;

                double maxThreshold;
                if (!double.TryParse(txtCondMaxThreshold.Text, out maxThreshold))
                    maxThreshold = 0;

                UITrigger trigger = new UITrigger()
                {
                    DeviceName = PluginName,
                    DetectorName = _detector.Name,
                    IsAnalog = cbxCondIsAnalog.IsChecked.Value,
                    TrueState = cbxCondTrueState.IsChecked.Value,
                    FalseState = cbxCondFalseState.IsChecked.Value,
                    TriggersToRaiseEvent = triggersToRaiseEvent,
                    TriggersToRaiseEventTimeFrame = triggersToRaiseEventTimeframe,
                    MaxThreshold = maxThreshold,
                    MinThreshold = minThreshold
                };

                resultCondition.Triggers.Add(trigger);
                trigger.Condition = resultCondition;

                if (conditionsToAdd == null)
                    conditionsToAdd = new List<UICondition>();

                conditionsToAdd.Add(resultCondition);

                lstLog.Items.Add(string.Format("Create Condition for: {0} {1}", _ip, _oid));
            }

            return resultCondition;
        }

        private void DeleteConditions(UICustomPropertyList _polls)
        {
            if (xConditions.IsChecked.Value)
            {
                for (int i = _polls.Properties.Count - 1; i >= 0; i--)
                {
                    UICustomPropertyList poll = _polls.Properties[i] as UICustomPropertyList;
                    if (poll != null)
                    {
                        UICustomProperty ip = FindPropertyByName("IP", poll);

                        if (ip != null && ip.Value != null)
                        {
                            UICustomPropertyList oids = FindPropertyByName("OIDs", poll) as UICustomPropertyList;
                            if (oids != null)
                            {
                                foreach (UICustomProperty oid in oids.Properties)
                                {
                                    if (oid != null && !string.IsNullOrWhiteSpace(oid.Value))
                                    {
                                        DeleteCondition(ip.Value, oid.Value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void DeleteCondition(string _ip, string _oid)
        {
            if (xConditions.IsChecked.Value)
            {
                if (conditionsToRemove == null)
                    conditionsToRemove = new List<UICondition>();

                conditionsToRemove.Add(new UICondition() { Name = "Remove", HiddenField = string.Format("{0};{1};{2}", PluginName, _ip, _oid) });

                lstLog.Items.Add(string.Format("Remove Condition for: {0} {1}", _ip, _oid));
            }
        }
        #endregion

        #region Actions
        private UIAlarmName CreateAction(string _name, string _ip, string _oid, string _template)
        {
            UIAlarmName resultAction = null;

            if (xActions.IsChecked.Value)
            {
                resultAction = new UIAlarmName()
                {
                    Name = _name,
                    HiddenField = string.Format("{0};{1};{2}", PluginName, _ip, _oid)
                };

                foreach (UIAlarmName a in templates)
                {
                    if (a.Name == _template)
                    {
                        foreach (UIAction action in a.Actions)
                            resultAction.Actions.Add(ObjectCopier.CloneXML<UIAction>(action));

                        break;
                    }
                }

                if (actionsToAdd == null)
                    actionsToAdd = new List<UIAlarmName>();

                actionsToAdd.Add(resultAction);

                lstLog.Items.Add(string.Format("Create action for: {0} {1}", _ip, _oid));
            }

            return resultAction;
        }

        private void DeleteActions(UICustomPropertyList _polls)
        {
            if (xActions.IsChecked.Value)
            {
                for (int i = _polls.Properties.Count - 1; i >= 0; i--)
                {
                    UICustomPropertyList poll = _polls.Properties[i] as UICustomPropertyList;
                    if (poll != null)
                    {
                        UICustomProperty ip = FindPropertyByName("IP", poll);

                        if (ip != null && ip.Value != null)
                        {
                            UICustomPropertyList oids = FindPropertyByName("OIDs", poll) as UICustomPropertyList;
                            if (oids != null)
                            {
                                foreach (UICustomProperty oid in oids.Properties)
                                {
                                    if (oid != null && !string.IsNullOrWhiteSpace(oid.Value))
                                    {
                                        DeleteAction(ip.Value, oid.Value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void DeleteAction(string _ip, string _oid)
        {
            if (xActions.IsChecked.Value)
            {
                if (actionsToRemove == null)
                    actionsToRemove = new List<UIAlarmName>();

                actionsToRemove.Add(new UIAlarmName() { Name = "Remove", HiddenField = string.Format("{0};{1};{2}", PluginName, _ip, _oid) });

                lstLog.Items.Add(string.Format("Remove Action for: {0} {1}", _ip, _oid));
            }
        }
        #endregion

        private string[] CreateSerializedList<T>(List<T> list)
        {
            int listCount = list != null ? list.Count : 0;

            string[] resultList = new string[listCount];

            for(int i = 0; i < listCount; i++)
            {
                resultList [i] = XMLSerializer.Serialize(list[i]);
            }

            return resultList;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (Saved != null && device != null)
                Saved(this, new PluginWizardSavedDetailsEventArgs()
                {
                    DeviceName = device.Name,
                    DeviceDetails = XMLSerializer.Serialize(device),
                    Conditions = CreateSerializedList(conditionsToAdd),
                    ConditionsToRemove = CreateSerializedList(conditionsToRemove),
                    Actions = CreateSerializedList(actionsToAdd),
                    ActionsToRemove = CreateSerializedList(actionsToRemove)
                });

            Window.GetWindow(this).Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }    
    }
}
