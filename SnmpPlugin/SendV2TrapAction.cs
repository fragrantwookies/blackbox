﻿using eNervePluginInterface;
using eNervePluginInterface.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnmpPlugin
{
    class SendV2TrapAction : PluginAction
    {
        public SendV2TrapAction() : base() { }

        public SendV2TrapAction(string _name, bool _value, string _pluginName, int _actionOrder = 0) : base(_name, _value, _pluginName, _actionOrder) { }

        [DisplayInPluginActionPropertiesList]
        public string IPAddress { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string Port { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string OID { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string Community { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string Variables { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string RequestIdentifier { get; set; }
    }
}
