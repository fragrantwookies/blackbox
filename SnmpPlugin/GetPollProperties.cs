﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Lextm.SharpSnmpLib;

namespace SnmpPlugin
{
    public class GetPollProperties
    {
        private const int defaultGetSetPort = 161;

        public IPEndPoint Address { get; set; }

        public int Port 
        {
            get
            {
                if (Address != null)
                    return Address.Port;
                else
                    return defaultGetSetPort;
            }
            set
            {
                if (Address != null)
                    Address.Port = value;
                else
                    Address = new IPEndPoint(IPAddress.Parse("127.0.0.1"), value);
            }
        }

        public IPAddress IPAddress
        {
            get
            {
                if (Address != null)
                    return Address.Address;
                else
                    return null;
            }
            set
            {
                if (Address != null)
                    Address.Address = value;
                else
                    Address = new IPEndPoint(value, defaultGetSetPort);
            }
        }

        public List<Variable> OIDs { get; set; }

        public OctetString Community { get; set; }

        public VersionCode Version { get; set; }

        public int Timeout { get; set; }

        public int Interval { get; set; }
    }
}
