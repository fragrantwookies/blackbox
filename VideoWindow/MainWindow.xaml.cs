﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using BlackBoxEngine;
using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;
using Dragablz;

namespace VideoWindow
{
    public class InterTabClient : IInterTabClient
    {
        public INewTabHost<Window> GetNewHost(IInterTabClient interTabClient, object partition, TabablzControl source)
        {
            var window = new MainWindow();
            window.WindowStyle = WindowStyle.None;
            return new NewTabHost<MainWindow>(window, source);
        }

        public TabEmptiedResponse TabEmptiedHandler(TabablzControl tabControl, Window window)
        {
            return TabEmptiedResponse.CloseWindowOrLayoutBranch;
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable, INotifyPropertyChanged
    {
        private CameraLayouts cameraLayout;
        private GridLength cameraListWidth;
        public static List<MainWindow> children;
        private CameraPlugin dragCamera;
        private Dictionary<string, string> cameraPositions;
        private bool cameraPositionsPopulated = false;
        private ObservableCollection<CameraControl> cameras;
        public static ObservableCollection<ICameraPlugin> CameraList;
        public static Client client;
        private FrameworkElement dragged;
        IPEndPoint serverDetails;
        private List<ICameraServerPlugin> servers;
        private Dictionary<string, Thread> serverThreads;
        private User user;

        #region PropertyChanged
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(_propertyName));
        }
        #endregion PropertyChanged

        public MainWindow()
        {
            if (client == null)
            {
                WindowState = WindowState.Maximized;
                Initialize("RIO test video", new User("Hendrik", ""), CameraLayouts.One);
                if (IPAddress.TryParse("192.168.1.230", out IPAddress address))
                {
                    serverDetails = new IPEndPoint(address, 4050);
                }
                Connect();
            }
            else
            {
                Initialize("RIO test video", new User("Hendrik", ""), CameraLayouts.One);
                lvCameras.DataContext = CameraList;
                if (cameraListGrid.Width.Value > 30)
                    ExpandMouseDown(this, null);
                if (children == null)
                    children = new List<MainWindow>();
                children.Add(this);
                //WindowStyle = WindowStyle.None;
            }
        }

        public MainWindow(string title, User _user, CameraLayouts _cameraLayout, IPEndPoint _serverDetails, Dictionary<string, string> _cameraPositions = null)
        {
            serverDetails = _serverDetails;
            Initialize(title, _user, _cameraLayout);
            Connect();
            cameraPositions = _cameraPositions;
        }

        ~MainWindow()
        {
            Dispose(false);
        }

        public void CloseMe()
        {
            try
            {
                if (Dispatcher.CheckAccess())
                    Close();
                else
                    Dispatcher.Invoke(Close);
            }
            catch (TaskCanceledException)
            {
                //bury it
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Unexpected error while closing.", ex);
            }
        }

        private void Connect()
        {
            client = new Client();
            client.ConnectionStatusChanged += Client_ConnectionStatusChanged;
            client.OnObjectMessageReceived += Client_OnObjectMessageReceived;
            client.Connect(serverDetails.Address.ToString(), serverDetails.Port, 0);
        }

        private void Client_ConnectionStatusChanged(object sender, Client.ConnectState connectionState)
        {
            switch (connectionState)
            {
                case Client.ConnectState.Connected:
                    client.SendMessage(MessageType.CameraLayouts);
                    break;
                case Client.ConnectState.Disconnected:
                    CloseMe();
                    break;
            }
        }

        private void Client_OnObjectMessageReceived(object sender, CommsNode.ObjectMessageEventArgs e)
        {
            if (CheckAccess())
            {
                switch (e.Message_Type)
                {
                    case MessageType.VideoCameraAdded:
                        client.SendMessage(MessageType.CameraLayouts, user);
                        break;
                    case MessageType.CameraLayouts: // Load servers, cameras, and layouts for this user, including cameras.
                        if (e.Message is ObservableCollection<ICameraPlugin> cameraList)
                        {
                            foreach (ICameraPlugin camera in cameraList)
                            {
                                string name = camera.Name;
                                if (camera is CameraPlugin plugin)
                                {
                                    name = plugin.DisplayName;
                                }
                                CameraControl control = new CameraControl(camera, (int)ViewGrid.ActualWidth, (int)ViewGrid.ActualHeight);
                                control.CameraClosedEvent += Control_CameraClosedEvent;
                                TabItem item = new TabItem
                                {
                                    Header = name,
                                    Content = control
                                };
                                ViewLayout.Items.Add(item);
                            }
                            CameraList = cameraList;
                            lvCameras.DataContext = CameraList;
                            /*cameras = new ObservableCollection<CameraControl>(); // Replace any existing (stale?) cameras.
                            foreach (var cameraPlugin in cameraList)
                            {
                                if (cameraPlugin != null)
                                {
                                    if (servers != null)
                                    {
                                        try
                                        {
                                            cameraPlugin.Parent = servers.FirstOrDefault(x => x.Name == cameraPlugin.Properties["Server"]);
                                        }
                                        catch (Exception ex)
                                        {
                                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error setting camera's parent in camera list.", ex);
                                        }
                                    }
                                    cameras.Add(new CameraControl(cameraPlugin));
                                }
                            }*/
                            cameraPositionsPopulated = true;
                        }
                        else
                        {
                            if (e.Message is List<ICameraServerPlugin> servers)
                            {
                                serverThreads = new Dictionary<string, Thread>(servers.Count);
                                //foreach (var camera in cameras)
                                //{
                                //    try
                                //    {
                                //        camera.Parent = servers.FirstOrDefault(x => x.Name == camera.Properties["Parent"]);
                                //    }
                                //    catch (Exception ex)
                                //    {
                                //        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error setting camera's parent from server list.", ex);
                                //    }
                                //}
                                foreach (var server in servers)
                                {
                                    if (server.Enabled)
                                    {
                                        Thread thread = new Thread(new ThreadStart(server.Start)) { Name = server.Name };

                                        thread.Start();
                                        serverThreads.Add(server.Name, thread);
                                        Thread.Sleep(100);// Allow server to start up
                                    }
                                }
                                client.SendMessage(MessageType.CameraLayouts, user);
                            }
                        }
                        break;
                    case MessageType.ForceLogoff:
                    case MessageType.UserLoggedOff:
                        var logOffUser = (e.Message as User);
                        if ((logOffUser != null) && (logOffUser.UserName == user.UserName))
                        {
                            CloseMe();
                        }
                        break;
                }
            }
            else
            {
                Dispatcher.Invoke(() => { Client_OnObjectMessageReceived(sender, e); });
            }
        }

        /// <summary>
        /// Unsure if this is raised on the first window, containing window, or both.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Control_CameraClosedEvent(object sender, CameraClosedEventArgs e)
        {
            if (ViewLayout.Items.Count == 1)
                CloseMe();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                GC.SuppressFinalize(this);
                if (dragCamera != null)
                {
                    dragCamera = null;
                }
                /*if (populateCameraTimer != null)
                {
                    populateCameraTimer.Dispose();
                    populateCameraTimer = null;
                }*/
                if (user != null)
                {
                    user.Dispose();
                    user = null;
                }
            }
            try
            {
                if (client != null)
                {
                    client.Disconnect();
                    client.ConnectionStatusChanged -= Client_ConnectionStatusChanged;
                    client.OnObjectMessageReceived -= Client_OnObjectMessageReceived;
                    client.Dispose();

                    client = null;
                }
                if (serverThreads != null)
                {
                    foreach (var serverThread in serverThreads)
                    {
                        serverThread.Value.Abort();
                    }
                    serverThreads.Clear();
                    serverThreads = null;
                }

                if (cameras != null && cameras.Count > 0)
                {
                    foreach (var camera in cameras)
                    {
                        if (camera != null)
                        {
                            camera.BtnClose_Click(this, new RoutedEventArgs());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write("Unexpected error " + ex);
                //throw;
            }
            CloseMe();
        }

        private void GridSplitter_MouseEnter(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.SizeWE;
        }

        private void GridSplitter_MouseLeave(object sender, MouseEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private void Initialize(string title, User _user, CameraLayouts _cameraLayout)
        {
            InitializeComponent();
            Title = title;
            user = _user;
            cameraLayout = _cameraLayout;
        }

        private void CameraControl_SendMessageEvent(SendMessageEventArgs e)
        {
            if (client.ConnectionStatus == Client.ConnectState.Connected)
            {
                client.SendMessage(e.MessageType, e.Message);
            }
        }

        private void DoDragDrop()
        {
            if ((dragCamera != null) && (dragged != null))
            {
                System.Windows.Forms.DataObject data = new System.Windows.Forms.DataObject(dragCamera);
                DragDrop.DoDragDrop(dragged, data, DragDropEffects.All);
                dragged = null;
            }
        }

        private void DragDrop_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragged == null)
            {
                //activeControl = null; // Remove active control so that PTZ commands aren't routed incorrectly.
                return;
            }
            try
            {
                if (e.LeftButton == MouseButtonState.Released)
                {
                    dragged = null;
                    return;
                }
                else
                {
                    dragCamera = dragged.DataContext as CameraPlugin;
                    if (dragCamera == null)
                    {
                        DragMove();
                    }
                    else
                    {
                        Dispatcher.Invoke(DoDragDrop);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error while dragging camera.", ex);
            }
        }

        private void DragDrop_StartDragging(object sender, MouseButtonEventArgs e)
        {
            if (dragged != null)
                return;

            UIElement lb = (UIElement)sender;
            if (lb != null)
            {
                UIElement element = lb.InputHitTest(e.GetPosition(lb)) as UIElement;
                while (element != null)
                {
                    dragged = (FrameworkElement)element;
                    if (dragged != null)
                        break;
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }
            }  
        }

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (lvCameras.Visibility == Visibility.Visible)
                {
                    lvCameras.Visibility = Visibility.Collapsed;
                    gridSplitter.Visibility = Visibility.Collapsed;
                    btnExpand.Text = "▲";
                    cameraListWidth = cameraListGrid.Width;
                    cameraListGrid.Width = new GridLength(30);
                }
                else
                {
                    lvCameras.Visibility = Visibility.Visible;
                    gridSplitter.Visibility = Visibility.Visible;
                    btnExpand.Text = "▼";
                    cameraListGrid.Width = cameraListWidth;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error changing camera list visibility.", ex);
            }
        }

        private void ViewGridReady(object sender, RoutedEventArgs e)
        {
            //GenerateLayout();
        }

        private void GenerateLayout()
        {
            int numCols = 1;
            int numRows = 1;
            switch (cameraLayout)
            {
                case CameraLayouts.One:
                    break;
                case CameraLayouts.TwoByTwo:
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    break;
                case CameraLayouts.FivePlusOne:
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    break;
                case CameraLayouts.ThreeByThree:
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    break;
                case CameraLayouts.TwelvePlusOne:
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    break;
                case CameraLayouts.FourByFour:
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    ViewGrid.ColumnDefinitions.Add(new ColumnDefinition());
                    numCols++;
                    ViewGrid.RowDefinitions.Add(new RowDefinition());
                    numRows++;
                    break;
            }
            for (int row = 0; row < numRows; row++)
            {
                for (int column = 0; column < numCols; column++)
                {
                    if (row == 0 && column == 1 && cameraLayout == CameraLayouts.FivePlusOne)
                    {
                        column++;
                    }
                    if (row == 1 && column == 0 && cameraLayout == CameraLayouts.FivePlusOne)
                    {
                        column += 2;
                    }
                    if (row == 1 && column == 2 && cameraLayout == CameraLayouts.TwelvePlusOne)
                    {
                        column++;
                    }
                    if (row == 2 && column == 1 && cameraLayout == CameraLayouts.TwelvePlusOne)
                    {
                        column += 2;
                    }
                    Border borderImage = new Border
                    {
                        AllowDrop = true,
                        Background = new SolidColorBrush(Colors.Black),
                        BorderThickness = new Thickness(2),
                        BorderBrush = new SolidColorBrush(Colors.Silver),
                        Name = "BRD_" + row + "_" + column
                    };
                    CameraControl cameraControl = new CameraControl();

                    Grid.SetRow(cameraControl, row);
                    Grid.SetColumn(cameraControl, column);
                    Grid.SetRow(borderImage, row);
                    Grid.SetColumn(borderImage, column);
                    if (row == 0 && column == 0 && cameraLayout == CameraLayouts.FivePlusOne)
                    {
                        Grid.SetColumnSpan(cameraControl, 2);
                        Grid.SetRowSpan(cameraControl, 2);
                        Grid.SetColumnSpan(borderImage, 2);
                        Grid.SetRowSpan(borderImage, 2);
                    }
                    if (row == 1 && column == 1 && cameraLayout == CameraLayouts.TwelvePlusOne)
                    {
                        Grid.SetColumnSpan(cameraControl, 2);
                        Grid.SetRowSpan(cameraControl, 2);
                        Grid.SetColumnSpan(borderImage, 2);
                        Grid.SetRowSpan(borderImage, 2);
                    }
                    ViewGrid.Children.Add(borderImage);
                    ViewGrid.Children.Add(cameraControl);
                }
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (children != null && children.Count > 0)
            {
                children.Remove(this);
            }
            else
            {
                Dispose(true);
            }
        }

        /// <summary>
        /// Position is a compound string: x_y_span. Span must always be 1 or 2 to maintain aspect ratio.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewGrid_Drop(object sender, DragEventArgs e)
        {
            if (dragCamera != null)
            {
                if (CheckAccess())
                {
                    lock (dragCamera)
                    {
                        if (e.Source is UIElement source)
                        {
                            int column = Grid.GetColumn(source);
                            int row = Grid.GetRow(source);
                            int columnSpan = Grid.GetColumnSpan(source);
                            int rowSpan = Grid.GetRowSpan(source);
                            try
                            {
                                if (ViewLayout.Items.Count >= 1)
                                {
                                    try
                                    {
                                        int index = ViewLayout.Items.IndexOf(ViewLayout.Items.Cast<TabItem>().First(item => item.Name == dragCamera.DisplayName));
                                        ViewLayout.SelectedIndex = index;
                                    }
                                    catch (Exception ex)
                                    {// CameraControl isn't in this window - connect it.
                                        if (ViewLayout.Items[0] is TabItem tabItem)
                                        {
                                            if (tabItem.Content is CameraControl control)
                                            {
                                                tabItem.Header = dragCamera.DisplayName;
                                                control.SetPlugin(dragCamera);
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception)// CameraControl doesn't exist anymore - don't panic.
                            { }
                            string position = column + "_" + row + "_" + columnSpan; if (cameraPositions == null)
                            {
                                cameraPositions = new Dictionary<string, string>
                                {
                                    { position, dragCamera.Name }
                                };
                            }
                            else if (cameraPositions.ContainsKey(position))
                            {
                                
                                cameraPositions[position] = dragCamera.Name;
                            }
                            else
                            {
                                cameraPositions.Add(position, dragCamera.Name);
                            }
                            /*CameraControl cameraControl = new CameraControl(dragCamera, (int)ViewGrid.ColumnDefinitions[column].ActualWidth, (int)ViewGrid.RowDefinitions[row].ActualHeight);
                            cameraControl.SendMessageEvent += CameraControl_SendMessageEvent;
                            ViewGrid.Children.Add(cameraControl);
                            Grid.SetColumn(cameraControl, column);
                            Grid.SetRow(cameraControl, row);
                            if (columnSpan > 1)
                                Grid.SetColumnSpan(cameraControl, columnSpan);
                            if (rowSpan > 1)
                                Grid.SetRowSpan(cameraControl, rowSpan);
                            cameraControl.BringIntoView();*/
                            
                            client.SendMessage(MessageType.CameraLayouts, new VideoHostConfiguration(cameraLayout, cameraPositions, Title, user));
                        }
                    }
                    dragCamera = null;
                }
                else
                {
                    Dispatcher.Invoke(() => { ViewGrid_Drop(sender, e); });
                }
            }
        }

        private void ViewGrid_MouseEnter(object sender, MouseEventArgs e)
        {
            if (dragCamera != null)
                Mouse.SetCursor(Cursors.Hand);
        }

        private void ViewLayout_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (ViewLayout.SelectedContent is CameraControl control)
            {
                Point pt = e.GetPosition((UIElement)sender);

                HitTestResult result = VisualTreeHelper.HitTest(control, pt);
                if (result != null && result.VisualHit.GetType() == typeof(Border))
                {
                    control.VideoPlayer_MouseUp(sender, e);
                }
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ViewLayout.Width = ViewGrid.Width;
            ViewLayout.Height = ViewGrid.Height;
        }

        private void ViewLayout_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
