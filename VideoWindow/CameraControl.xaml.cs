﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Timers;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;

using AForge.Vision.Motion;

namespace VideoWindow
{
    /// <summary>
    /// Interaction logic for CameraControl.xaml
    /// </summary>
    public partial class CameraControl : UserControl, INotifyPropertyChanged
    {
        #region PropertyChanged
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(_propertyName));
        }
        #endregion PropertyChanged
        public event EventHandler<CameraClosedEventArgs> CameraClosedEvent;

        //private OzekiCamera camera;
        private CameraPlugin cameraPlugin;
        public Visibility ControlVisibility
        { get { return controlVisibility; } set { controlVisibility = value; PropertyChangedHandler("ControlVisibility"); } }
        private Visibility controlVisibility = Visibility.Collapsed;
        public string DateLabel { get { return dateLabel; } set { dateLabel = value; PropertyChangedHandler("DateLabel"); } }
        private string dateLabel;
        private int detectedObjectsCount = -1;
        private readonly Timer displayTimer;

        private float motionAlarmLevel = 0.015f;
        private bool motionDetection = false;
        MotionDetector motionDetector = new MotionDetector(new TwoFramesDifferenceDetector(), new MotionAreaHighlighting());
        private int motionFlash = 0;

        public delegate void SendMessageEventHandler(SendMessageEventArgs e);
        public SendMessageEventHandler SendMessageEvent;

        public Visibility PlaybackVisibility
        {
            get { return playbackVisibility; }
            set
            {
                playbackVisibility = value;
                if (value == Visibility.Visible)
                    StreamPickerVisibility = Visibility.Hidden;
                else StreamPickerVisibility = Visibility.Visible;
                PropertyChangedHandler("PlaybackVisibility");
            }
        }

        internal void SetPlugin(CameraPlugin plugin)
        {
            cameraPlugin = plugin;
            if (videoPlayer.IsPlaying)
                videoPlayer.Stop();
            /*if (cameraPlugin.Streams == null || cameraPlugin.Streams.Count == 0 || string.IsNullOrWhiteSpace(cameraPlugin.Streams[0].RtspUri))
            {
                if (cameraPlugin.Name.Contains("rtsp"))
                    cameraPlugin.Streams.Add(new StreamDefinition("Default RTSP", new Resolution((int)ActualHeight, (int)ActualWidth),
                        cameraPlugin.Name.Replace(@"rtsp://", @"rtsp://admin:admin@")));
                else
                {// Construct a guess for RTSP.
                    string[] parts = cameraPlugin.Name.Split(new char[] { ':', '/' }, StringSplitOptions.RemoveEmptyEntries);
                    string format = string.Format(@"rtsp://{0}:554", parts[1]);
                    if (parts[1].EndsWith("111") || parts[1].EndsWith("199"))
                        format += "/stream1";
                    cameraPlugin.Streams.Add(new StreamDefinition("Guessed RTSP", new Resolution((int)ActualHeight, (int)ActualWidth),
                        format));
                }

            }*/
            if (cameraPlugin.Streams != null && cameraPlugin.Streams.Count > 0)
            {
                int index = 0;
                if (plugin.Streams.Count > 1)
                {
                    int currentRes = (int)(Height * Width);
                    while (currentRes < plugin.Streams[index].TotalPixels && index < (plugin.Streams.Count - 1))
                        index++;
                }
                videoPlayer.Open(cameraPlugin.Streams[index].RtspUri);
                videoPlayer.Play();
                cbbStreams.DataContext = cameraPlugin.Streams;
                cbbStreams.SelectedIndex = index;
                PropertyChangedHandler("Streams");
            }
        }
        public Visibility StreamPickerVisibility
        { get { return streamPickerVisibility; } set { streamPickerVisibility = value; PropertyChangedHandler("StreamPickerVisibility"); } }

        private Visibility playbackVisibility = Visibility.Collapsed;
        private Visibility streamPickerVisibility = Visibility.Collapsed;
        private DateTime seekTime;
        public ObservableCollection<StreamDefinition> Streams { get { return new ObservableCollection<StreamDefinition>(cameraPlugin.Streams); } }
        public string TimeLabel { get { return timeLabel; } set { timeLabel = value; PropertyChangedHandler("TimeLabel"); } }
        private string timeLabel;

        public VideoMode VideoMode
        {
            get { return videoMode; }
            set
            {
                switch (value)
                {
                    case VideoMode.None:
                        break;
                    case VideoMode.Live:
                        PlaybackVisibility = Visibility.Hidden;
                        break;
                    case VideoMode.Playback:
                        PlaybackVisibility = Visibility.Visible;
                        break;
                }
                PropertyChangedHandler("VideoMode");
            }
        }
        private VideoMode videoMode;

        ~CameraControl()
        {
            if (videoPlayer != null)
            {
                if (videoPlayer.IsPlaying)
                    videoPlayer.Stop();
            }
            videoPlayer.MouseUp -= VideoPlayer_MouseUp;
            videoPlayer.PreviewMouseDoubleClick -= VideoPlayer_MouseUp;
            /*if (connector != null && camera != null)
            {
                connector.Disconnect(camera.VideoChannel, frameCapture);
                connector.Disconnect(frameCapture, imageProcessor);
                connector.Disconnect(camera.VideoChannel, snapshotHandler);
                connector.Disconnect(imageProcessor, provider);
                camera.Stop();
            }
            if (videoViewer != null)
            {
                videoViewer.MouseClick -= VideoViewer_MouseClick;
                videoViewer.Stop();
                videoViewer.Dispose();
                videoViewer = null;
            }*/
        }

        public CameraControl()
        {
            InitializeComponent();
            string folder = string.Format("{0}\\Ethele\\RioSoft\\VideoWindow\\ffmpeg\\", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData));
            Unosquare.FFME.MediaElement.FFmpegDirectory = folder;
            controlGridRow.Height = new GridLength(0);
            videoPlayer.PreviewMouseDoubleClick += VideoPlayer_MouseUp;
        }

        public CameraControl(ICameraPlugin _cameraPlugin, int width, int height)
        {
            InitializeComponent();
            string folder = string.Format("{0}\\Ethele\\RioSoft\\VideoWindow\\ffmpeg\\", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData));
            Unosquare.FFME.MediaElement.FFmpegDirectory = folder;
            controlGridRow.Height = new GridLength(0);
            videoPlayer.PreviewMouseDoubleClick += VideoPlayer_MouseUp;

            if (_cameraPlugin is CameraPlugin plugin)
            {
                videoMode = VideoMode.Live;
                //controlGridRow.Height = new GridLength(0);
                cameraPlugin = plugin;
                if (videoPlayer.IsPlaying)
                    videoPlayer.Stop();
                int index = 0;
                if (plugin.Streams.Count > 1)
                {
                    int currentRes = height * width;
                    while (currentRes < plugin.Streams[index].TotalPixels && index < (plugin.Streams.Count - 1))
                        index++;
                }
                /*if (cameraPlugin.Streams != null && cameraPlugin.Streams.Count > 0)
                {
                    videoPlayer.Open(cameraPlugin.Streams[index].RtspUri);
                    videoPlayer.Play();
                }*/
                cbbStreams.DataContext = cameraPlugin.Streams;
                cbbStreams.SelectedIndex = index;
                ControlVisibility = Visibility.Visible;
                /*provider = new DrawingImageProvider();
                videoViewer.SetImageProvider(provider);
                frameCapture = new FrameCapture();
                frameCapture.SetInterval(1000);
                frameCapture.OnFrameCaptured += FrameCapture_OnFrameCaptured;
                faceDetector = ImageProcesserFactory.CreateFaceDetector();
                faceDetector.DrawColor = System.Drawing.Color.FromArgb(0, 144, 255);
                faceDetector.DrawThickness = 5;
                faceDetector.MinSize = new System.Drawing.Size(50, 50);
                connector = new MediaConnector();
                plateRecognizer = ImageProcesserFactory.CreateLicensePlateRecognizer();
                plateRecognizer.DrawColor = System.Drawing.Color.FromArgb(255, 144, 0);//e-Thele orange
                motionDetector = ImageProcesserFactory.CreateMotionDetector();
                motionDetector.ShowImage = true;
                imageProcessor = new ImageProcesserHandler();
                //imageProcessor.AddProcesser(faceDetector);
                imageProcessor.AddProcesser(motionDetector);
                imageProcessor.AddProcesser(plateRecognizer);
                snapshotHandler = new SnapshotHandler();*/
                
                /*videoPlayer.Height = height;
                videoPlayer.Width = width;*/
                /*camera = new OzekiCamera(cameraPlugin.Name);
                camera.RtspUriReceived += Camera_RtspUriReceived;
                camera.CameraStateChanged += Camera_CameraStateChanged;
                connector.Connect(camera.VideoChannel, frameCapture);
                connector.Connect(frameCapture, imageProcessor);
                connector.Connect(camera.VideoChannel, snapshotHandler);
                connector.Connect(imageProcessor, provider);
                //connector.Connect(camera.VideoChannel, provider);

                //camera.Resolution = new Ozeki.Media.Resolution((int)Width, (int)Height);
                videoViewer.Start();
                camera.Start();
                imageProcessor.Start();*/
            }
        }

        /*private void Camera_RtspUriReceived(object sender, CameraRtspUriEventArgs e)
        {
            if (CheckAccess())
            {
                if (cameraPlugin.Streams == null)
                    cameraPlugin.Streams = new List<StreamDefinition>();
                StreamDefinition currentStreamDef = null;
                foreach (IPCameraStream stream in camera.AvailableStreams)
                {
                    if (stream.VideoEncoding != null && stream.VideoEncoding.Resolution != null)
                    {
                        StreamDefinition streamDef = new StreamDefinition(stream.Name, new eNervePluginInterface.Resolution(stream.VideoEncoding.Resolution.Height, stream.VideoEncoding.Resolution.Width), stream.RtspUri);
                        int index = cameraPlugin.Streams.BinarySearch(streamDef);
                        if (index < 0)
                            cameraPlugin.Streams.Insert(~index, streamDef);
                        if (camera.CurrentStream.Name == streamDef.Name)
                            currentStreamDef = streamDef;
                    }
                }
                cbbStreams.DataContext = Streams;
                if (currentStreamDef == null)
                    cbbStreams.SelectedIndex = 0;
                else
                    cbbStreams.SelectedItem = currentStreamDef;

                PropertyChangedHandler("Streams");
            }
            else
            {
                Dispatcher.Invoke(() => { Camera_RtspUriReceived(sender, e); });
            }
        }*/

        internal void BtnClose_Click(object sender, RoutedEventArgs e)
        {
            /*if (videoViewer != null)
            {
                camera.Disconnect();
                connector.Disconnect(camera.VideoChannel, provider);
                videoViewer.Stop();
                camera = null;
                videoViewer.Dispose();
                videoViewer = null;
            }*/
            if (videoPlayer != null)
            {
                if (videoPlayer.IsPlaying)
                    videoPlayer.Stop();
                videoPlayer.UpdateLayout();
            }
            PlaybackVisibility = Visibility.Collapsed;
            ControlVisibility = Visibility.Collapsed;
            controlGridRow.Height = new GridLength(0);
            VideoMode = VideoMode.None;
            OnCameraClosedEvent(new CameraClosedEventArgs(Name));
            // TODO notyify MainWindow that camera has been removed, so that server can be udpated.
        }

        private void BtnExport_Click(object sender, RoutedEventArgs e)
        {
            motionDetection = !motionDetection;
            /*ExportWindow exportWindow = new ExportWindow(Camera.DisplayName);
            if (exportWindow.ShowDialog() == true)
            {
                string result;
                if (exportWindow.Start > exportWindow.End)
                {
                    result = Camera.Export(exportWindow.End, exportWindow.Start, string.Format("{0}\\{1}", exportWindow.Target, Camera.DisplayName.Substring(Camera.DisplayName.LastIndexOf('.') + 1)),
                        DisplayHandle);
                }
                else
                {
                    result = Camera.Export(exportWindow.Start, exportWindow.End, string.Format("{0}\\{1}", exportWindow.Target, Camera.DisplayName.Substring(Camera.DisplayName.LastIndexOf('.') + 1)),
                        DisplayHandle);
                }
                if (string.IsNullOrEmpty(result) || result.StartsWith("Error", StringComparison.OrdinalIgnoreCase))
                {
                    // Sum ting wong
                    MessageBox.Show("Error - could not perform export.");
                }
                else
                {
                    // Report success.
                    MessageBox.Show("Success - downloading exported video file to " + result);
                }
            }*/
        }

        private void BtnPause_Click(object sender, RoutedEventArgs e)
        {
            /*if (Camera != null)
            {
                if (Camera.Pause())
                    displayTimer.Stop();
            }*/
        }

        private void BtnPlay_Click(object sender, RoutedEventArgs e)
        {
            /*if (Camera != null)
            {
                if (Camera.Play())
                    displayTimer.Start();
            }*/
        }

        /// <summary>
        /// TODO Ensure file name is a configurable field with variables (camera name, datetime, etc.)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSnapshot_Click(object sender, RoutedEventArgs e)
        {
            if (videoPlayer.IsPlaying)
            {
                try
                {
                    string fileName = string.Format("{0}\\{1}", Environment.GetFolderPath(Environment.SpecialFolder.MyPictures), cameraPlugin.Properties["DisplayName"]);
                    if (VideoMode == VideoMode.Playback)
                        fileName += seekTime.ToShortTimeString().Replace(":", "");
                    else
                        fileName += DateTime.Now.ToShortTimeString().Replace(":", "");
                    fileName = fileName.Replace(" ", "") + ".png";
                    Size size = new Size(videoPlayer.NaturalVideoWidth, videoPlayer.NaturalVideoHeight);
                    RenderTargetBitmap rtb = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96, 96, System.Windows.Media.PixelFormats.Pbgra32);
                    videoPlayer.Measure(size);
                    videoPlayer.Arrange(new Rect(size));
                    rtb.Render(videoPlayer);
                    PngBitmapEncoder encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(rtb));
                    using (Stream stream = File.Create(fileName))
                    {
                        encoder.Save(stream);
                    }
                    //videoPlayer.GetCurrentFrame().Save(fileName);
                    //snapshotHandler.TakeSnapshot().ToImage().Save(fileName);
                    System.Threading.Tasks.Task.Run(() => CheckForLicencePlate(fileName));
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add("BtnSnapshot_Click", "", ex);
                }
            }
        }

        private void CheckForLicencePlate(string imageName)
        {
            try
            {
                string filename = string.Format("{0}\\Ethele\\RioSoft\\VideoWindow\\openalpr_32\\alpr.exe", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData));
                if (File.Exists(filename) && File.Exists(imageName))
                {
                    Process plateProcess = new Process();
                    plateProcess.StartInfo.FileName = filename;
                    plateProcess.StartInfo.CreateNoWindow = true;
                    plateProcess.StartInfo.Arguments = string.Format("-c EU -n 1 -j {0}", imageName);
                    plateProcess.StartInfo.RedirectStandardOutput = true;
                    plateProcess.StartInfo.UseShellExecute = false;
                    plateProcess.StartInfo.RedirectStandardError = true;
                    plateProcess.Start();
                    JavaScriptSerializer ser = new JavaScriptSerializer();
                    if (ser.Deserialize(plateProcess.StandardOutput.ReadToEnd(), typeof(LicensePlateResults)) is LicensePlateResults results)
                    {
                        if (results.Results != null && results.Results.Count > 0)
                        {
                            foreach (Result result in results.Results)
                            {
                                System.Drawing.Rectangle cropRect = new System.Drawing.Rectangle((int)result.Coordinates[0].X, (int)result.Coordinates[0].Y, 
                                    (int)(result.Coordinates[2].X - result.Coordinates[0].X) + 5, (int)(result.Coordinates[2].Y - result.Coordinates[0].Y) + 5);
                                System.Drawing.Bitmap source = System.Drawing.Image.FromFile(imageName) as System.Drawing.Bitmap;
                                System.Drawing.Bitmap target = new System.Drawing.Bitmap(cropRect.Width, cropRect.Height);
                                using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(target))
                                {
                                    g.DrawImage(source, new System.Drawing.Rectangle(0, 0, target.Width, target.Height), cropRect, System.Drawing.GraphicsUnit.Pixel);
                                    Alarm alarm = new Alarm("License Plate Detected " + result.Plate);
                                    source = new System.Drawing.Bitmap(source, 320, 240);
                                    EventImage sourceImage = new EventImage
                                    {
                                        CameraAddress = imageName,
                                        CameraID = cameraPlugin.Name,
                                        ImageData = Images.ImageToByte(source)
                                    };
                                    alarm.AddImage(sourceImage);
                                    EventImage targetImage = new EventImage
                                    {
                                        CameraAddress = "Captured plate",
                                        CameraID = cameraPlugin.Name,
                                        ImageData = Images.ImageToByte(target)
                                    };
                                    alarm.AddImage(targetImage);
                                    OnSendMessage(MessageType.Alarm, alarm);
                                }
                            }
                        }
                    }
                    plateProcess.Close();
                    plateProcess.Dispose();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add("CheckForLicencePlate", "", ex);
            }
        }

        private void BtnSwitch_Click(object sender, RoutedEventArgs e)
        {
            if (videoPlayer.MediaState == MediaState.Play || videoPlayer.MediaState == MediaState.Pause)
            {
                if (mediaInfoLabel.ActualHeight == 0)
                    mediaInfoLabel.Height = 20;
                else
                    mediaInfoLabel.Height = 0;
            }
            /*if (VideoMode == VideoMode.Playback)
            {
                PlaybackVisibility = Visibility.Collapsed;
                btnSwitch.ToolTip = VideoMode.ToString();
                Camera.GoToLive(DisplayHandle);
                VideoMode = VideoMode.Live;

            }
            else if (VideoMode == VideoMode.Live)
            {
                btnSwitch.ToolTip = VideoMode.ToString();
                if (Camera.GoToPlayback(DisplayHandle))
                {
                    seekTime = DateTime.Now.AddSeconds(-60);
                    TimeLabel = seekTime.ToString("hh:mm:ss");
                    DateLabel = seekTime.ToString("d");
                    displayTimer.Start();
                    VideoMode = VideoMode.Playback;
                    PlaybackVisibility = Visibility.Visible;
                }
            }
            else
            {
                PlaybackVisibility = Visibility.Collapsed;
                Height = 0;
            }*/
        }

        /*private void Camera_CameraStateChanged(object sender, CameraStateEventArgs e)
        {
            if (e.State == CameraState.Streaming)
            {
                if (CheckAccess())
                {
                    double newHeight = ActualHeight - controlGrid.ActualHeight;
                    if (videoViewer.ActualHeight != newHeight)
                        videoViewer.Height = newHeight;
                    if (videoViewer.ActualWidth != ActualWidth)
                        videoViewer.Width = ActualWidth;
                    //frameCapture.Start();
                }
                else
                {
                    Dispatcher.Invoke(() => { Camera_CameraStateChanged(sender, e); });
                }
            }
            else if (e.State == CameraState.Disconnected)
            {
                if (camera != null)
                {
                    if (camera.VideoChannel != null)
                    {
                        string[] nameParts = cameraPlugin.Name.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        if (nameParts.Length > 1)
                        {
                            if (IPAddress.TryParse(nameParts[1], out IPAddress address))
                            {
                                camera = new OzekiCamera(string.Format("{0}:80;Username=admin;Password=admin;Transport=UDP;", address));
                                connector.Connect(camera.VideoChannel, frameCapture);
                                connector.Connect(frameCapture, imageProcessor);
                                //connector.Connect(camera.VideoChannel, provider);
                                connector.Connect(camera.VideoChannel, snapshotHandler);
                                connector.Connect(imageProcessor, provider);

                                //camera.Resolution = new Ozeki.Media.Resolution((int)Width, (int)Height);
                                //videoViewer.Start();
                                camera.Start();
                            }
                        }
                    }
                }
            }
        }*/

        private void ChangeDate(object sender, MouseButtonEventArgs e)
        {
            displayTimer.Stop();
            datePicker.ShowDropDownButton = true;
        }

        private void DatePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            datePicker.Visibility = Visibility.Collapsed;
            if (datePicker.Value.HasValue)
            {
                seekTime = datePicker.Value.Value;
                /*if (Camera.GoToTime(seekTime))
                {
                    TimeLabel = seekTime.ToString("hh:mm:ss");
                    DateLabel = seekTime.ToString("d");
                }*/
            }
            displayTimer.Start();
        }

        private void OnCameraClosedEvent(CameraClosedEventArgs e)
        {
            if ((CameraClosedEvent != null) && (e != null))
            {
                CameraClosedEvent(this, e);
            }
        }

        private void OnSendMessage(MessageType type, object data)
        {
            SendMessageEvent?.Invoke(new SendMessageEventArgs(type, data));
        }

        private void Slider_DragCompleted(object sender, RoutedEventArgs e)
        {
            //if (videoPlayer.IsPlaying)
            {
                //if (Camera.GoToTime(seekTime))
                    TimeLabel = seekTime.ToString("hh:mm:ss");
                timeSlider.Value = 1800;
            }
            if (displayTimer != null)
                displayTimer.Start();
        }

        private void Slider_DragStarted(object sender, RoutedEventArgs e)
        {
            if (displayTimer != null)
                displayTimer.Stop();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (e.NewValue != 1800)
            {
                /*if (sender is Slider slider)
                {
                    seekTime = seekTime.AddSeconds((int)(e.NewValue - e.OldValue));
                    timeSlider.AutoToolTipFormat = seekTime.ToString("hh:mm:ss");
                }*/
            }
        }

        private void HandlePlayerEvent(object sender, RoutedEventArgs e)
        {

            if (e.RoutedEvent.Name == "StreamStarted")
            {
                controlGridRow.Height = new GridLength(60);
            }
            else if (e.RoutedEvent.Name == "StreamFailed")
            {
                controlGridRow.Height = new GridLength(0);
                ControlVisibility = Visibility.Collapsed;
                //videoPlayer.Height = videoPlayer.ActualHeight + 60;
            }
            else if (e.RoutedEvent.Name == "StreamStopped")
            {
                controlGridRow.Height = new GridLength(0);
                ControlVisibility = Visibility.Collapsed;
                //videoPlayer.Height = videoPlayer.ActualHeight + 60;
            }
        }

        private void VideoViewer_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (controlGridRow.ActualHeight == 0)
                controlGridRow.Height = new GridLength(60);
            else
            {
                controlGridRow.Height = new GridLength(0);
                ControlVisibility = Visibility.Collapsed;
                videoPlayer.Height = videoPlayer.ActualHeight;
            }
        }

        private void CbbStreams_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if ((e.AddedItems != null) && (e.AddedItems.Count > 0))
                {
                    if (e.AddedItems[0] is StreamDefinition streamDefinition)
                    {
                        //if (videoPlayer.Source != streamDefinition.RtspUri)
                        {
                            videoPlayer.Source = streamDefinition.RtspUri;
                            videoPlayer.Visibility = Visibility.Hidden;
                            progressBar.Visibility = Visibility.Visible;
                        }
                        /*if (camera.AvailableStreams != null && camera.AvailableStreams.Count > 0)
                        {
                            foreach (IPCameraStream stream in camera.AvailableStreams)
                            {
                                if (stream.Name == streamDefinition.Name)
                                {
                                    camera.Start(stream);
                                    return;
                                }
                            }
                        }*/
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void VideoPlayer_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (videoPlayer.MediaState == MediaState.Play || videoPlayer.MediaState == MediaState.Pause)
            {
                if (Grid.GetColumn(this) == Grid.GetColumn((UIElement)e.Source) && Grid.GetRow(this) == Grid.GetRow((UIElement)e.Source))
                {
                    if (controlGridRow.ActualHeight == 0)
                        controlGridRow.Height = new GridLength(60);
                    else
                    {
                        controlGridRow.Height = new GridLength(0);
                        //videoRow.Height = new GridLength(videoRow.ActualHeight + 60);
                    }
                }
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ((UIElement)Parent).MouseUp += VideoPlayer_MouseUp;
        }

        private void VideoPlayer_MediaStateChanged(object sender, Unosquare.FFME.Events.MediaStateChangedRoutedEventArgs e)
        {
            mediaStateLabel.Content = e.MediaState.ToString();
            if (e.MediaState != e.OldMediaState)
            switch (e.MediaState)
            {
                case MediaState.Manual:
                    break;
                case MediaState.Play:
                        {
                            progressBar.Visibility = Visibility.Hidden;
                            videoPlayer.Visibility = Visibility.Visible;
                        }
                    break;
                case MediaState.Close: // Error state
                        {

                        }
                    break;
                case MediaState.Pause:
                    break;
                case MediaState.Stop: // Clean stop
                        {

                        }
                    break;
                default:
                    break;
            }
        }

        private void VideoPlayer_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
        }


        private void VideoPlayer_RenderingVideo(object sender, Unosquare.FFME.Events.RenderingVideoEventArgs e)
        {
            mediaInfoLabel.Content = string.Format("Codec: {0} Bit Rate: {1}kb/s FPS: {2}", e.Stream.CodecName, e.EngineState.DecodingBitRate / 1000, e.Stream.FPS);
            if (motionDetection)
            {
                float motionLevel = motionDetector.ProcessFrame(e.Bitmap.CreateDrawingBitmap());

                if (motionLevel > motionAlarmLevel)
                {
                    // flash for 2 seconds
                    motionFlash = (2 * 5);
                }

                // check objects' count
                if (motionDetector.MotionProcessingAlgorithm is BlobCountingObjectsProcessing)
                {
                    BlobCountingObjectsProcessing countingDetector = (BlobCountingObjectsProcessing)motionDetector.MotionProcessingAlgorithm;
                    detectedObjectsCount = countingDetector.ObjectsCount;
                }
                else
                {
                    detectedObjectsCount = -1;
                }
            }
        }
    }
}
