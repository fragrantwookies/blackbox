﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace VideoWindow
{
    /// <summary>
    /// Interaction logic for ExportWindow.xaml
    /// </summary>
    public partial class ExportWindow : Window
    {
        public DateTime End { get; set; }
        public DateTime Start { get; set; }
        public string Target { get; set; }
        public ExportWindow(string cameraName)
        {
            InitializeComponent();
            
            lblName.Content = cameraName;
        }
        public ExportWindow()
        {
            InitializeComponent();
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void lblFolder_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Target = folderBrowser.SelectedPath;
            }
        }

        private void startTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            DateTime newStart = (DateTime)e.NewValue;
            if (newStart != null)
            {
                Start = new DateTime(newStart.Year, newStart.Month, newStart.Day, newStart.Hour, newStart.Minute, newStart.Second, DateTimeKind.Local);
            }
        }

        private void endTime_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            DateTime newEnd = (DateTime)e.NewValue;
            if (newEnd != null)
            {
                End = new DateTime(newEnd.Year, newEnd.Month, newEnd.Day, newEnd.Hour, newEnd.Minute, newEnd.Second, DateTimeKind.Local);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Target = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);
        }
    }
}
