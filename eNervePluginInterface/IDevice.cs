﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public interface IDevice
    {
        string IPAddress { get; set; }
        string Port { get; set; }
        string DeviceMac { get; set; }
        string DeviceName { get; set; }

        List<IDetector> Detectors { get; set; }
    }
}
