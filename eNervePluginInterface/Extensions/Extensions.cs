﻿using eNervePluginInterface.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public static class Extensions
    {
        public static bool DisplayInPluginActionPropertiesList(this PropertyInfo propertyInfo)
        {
            if (propertyInfo == null)
            {
                throw new ArgumentNullException("fieldInfo");
            }

            object[] attributes = propertyInfo.GetCustomAttributes(typeof(DisplayInPluginActionPropertiesListAttribute), false);
            return attributes != null ? attributes.Length > 0 : false;
        }
    }
}
