﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public class PluginWizardSavedDetailsEventArgs : EventArgs
    {
        public PluginWizardSavedDetailsEventArgs() : base()
        {            
        }

        public string DeviceName { get; set; }

        public string DeviceDetails { get; set; }

        /// <summary>
        /// list of xml serialized UICondition items
        /// </summary>
        public string[] Conditions { get; set; }
        
        /// <summary>
        /// list of xml serialized UICondition items
        /// </summary>
        public string[] ConditionsToRemove { get; set; }

        /// <summary>
        /// list of xml serialized UIAction items
        /// </summary>
        public string[] Actions { get; set; }

        /// <summary>
        /// list of xml serialized UIAction items
        /// </summary>
        public string[] ActionsToRemove { get; set; }
    }
}
