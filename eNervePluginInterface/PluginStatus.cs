﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public enum PluginStatus
    {
        Disabled = 0, // Grey
        Offline = 1, // Red
        Online = 2, // Blue
        Connecting = 3, // Green
        NonFatalError = 4, // Yellow
        Unrecoverable = 5 // Red and strikethrough
    }
}
