﻿using System;


namespace eNervePluginInterface
{
    public class AddCameraEventArgs : EventArgs
    {
        public bool Admin { get; set; }
        public Type CameraType { get; set; }
        public int Channel { get; set; }
        public string Name { get; set; }
        public ICameraServerPlugin Parent { get; set; }
        public ICameraPlugin Plugin { get; set; }
        public bool Ptz { get; set; }
        public AddCameraEventArgs() { }
        public AddCameraEventArgs(Type cameraType, string name, int channel, ICameraServerPlugin parent, bool admin = false, bool ptz = false)
        {
            Admin = admin;
            CameraType = cameraType;
            Channel = channel;
            Name = name;
            Parent = parent;
            Ptz = ptz;
        }
        public AddCameraEventArgs(CameraPlugin cameraPlugin, ICameraServerPlugin parent, bool admin = false, bool ptz = false)
        {
            Admin = admin;
            CameraType = cameraPlugin.GetType();
            Channel = 0; // Use as index of available streams to display by default.
            Name = cameraPlugin.Name;
            Parent = parent;
            Ptz = ptz;
            Plugin = cameraPlugin;
        }
    }
}
