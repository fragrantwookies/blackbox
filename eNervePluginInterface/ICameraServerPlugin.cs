﻿using System;
using System.Collections.ObjectModel;

namespace eNervePluginInterface
{
    public interface ICameraServerPlugin : INervePlugin
    {
        event EventHandler<AddCameraEventArgs> AddCameraEvent;
                
        //ObservableCollection<ICameraPlugin> Cameras { get; set; }

        void Stop();
    }
}
