using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    public class CustomProperty : ICustomProperty
    {
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }

        public CustomProperty(string name, string value)
        {
            PropertyName = name;
            PropertyValue = value;
        }

        public int CompareTo(ICustomProperty other)
        {
            return this.PropertyName.CompareTo(other.PropertyName);
        }

        public int Compare(ICustomProperty x, ICustomProperty y)
        {
            return x.PropertyName.CompareTo(y.PropertyName);
        }
    }
}