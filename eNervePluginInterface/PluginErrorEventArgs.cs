using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    public class PluginErrorEventArgs : EventArgs
    {
        public PluginErrorEventArgs(string functionName, string description, Exception ex)
            : base()
        {
            FunctionName = functionName;
            Description = description;
            Exception = ex;
        }
        public PluginErrorEventArgs()
        {}

        public string FunctionName { get; set; }
        public string Description { get; set; }
        public Exception Exception { get; set; }
    }
}
