﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public interface INerveSnapshotPlugin
    {
        event EventHandler<ImageCapturedEventArgs> ImageCaptured;
    }
}
