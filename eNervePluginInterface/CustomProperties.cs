using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    public class CustomProperties
    {
        private List<ICustomProperty> properties;

        public CustomProperties()
        {
            if (properties == null)
                properties = new List<ICustomProperty>();
        }

        public void ClearProperties()
        {
            properties.Clear();
        }

        /// <summary>
        /// Add a new property unless it already exists, in which case the value will be updated.
        /// </summary>
        /// <param name="propName">Name of the property</param>
        /// <param name="propValue">Value of the property</param>
        /// <returns></returns>
        public int AddProperty(string propName, string propValue)
        {
            int result = -1;
            CustomProperty newProperty = new CustomProperty(propName, propValue);

            int pos = properties.BinarySearch(newProperty);

            if (pos < 0)
            {
                properties.Insert(~pos, newProperty);
                result = ~pos;
            }
            else
            {
                CustomProperty resultProperty = properties[pos] as CustomProperty;
                if (resultProperty != null)
                {
                    resultProperty.PropertyValue = propValue;
                    result = pos;
                }
            }

            return result;
        }

        public int AddProperty(ICustomProperty property)
        {
            int result = -1;
            //CustomProperty newProperty = new CustomProperty(propName, propValue);

            int pos = properties.BinarySearch(property);

            if (pos < 0)
            {
                properties.Insert(~pos, property);
                result = ~pos;
            }
            else
            {
                if (properties[pos] is CustomProperty && property is CustomProperty)
                {
                    ((CustomProperty)properties[pos]).PropertyValue = ((CustomProperty)property).PropertyValue;

                    result = pos;
                }
                else if (properties[pos] is CustomPropertyList && property is CustomPropertyList)
                {
                    ((CustomPropertyList)properties[pos]).Properties = ((CustomPropertyList)property).Properties;

                    result = pos;
                }
            }

            return result;
        }

        public string this[string propName]
        {
            get
            {
                string result = "";

                ICustomProperty newProperty = new CustomProperty(propName, "");

                int pos = properties.BinarySearch(newProperty);
                if (pos >= 0)
                {
                    CustomProperty resultProperty = properties[pos] as CustomProperty;

                    if (resultProperty != null)
                        result = resultProperty.PropertyValue;
                }

                return result;
            }
            set
            {
                AddProperty(propName, value);
            }
        }

        public ICustomProperty FindProperty(string propName)
        {
            ICustomProperty result = null;

            ICustomProperty newProperty = new CustomProperty(propName, "");

            int pos = properties.BinarySearch(newProperty);
            if(pos >= 0)
                result = properties[pos] as ICustomProperty;

            return result;
        }

        public IReadOnlyList<ICustomProperty> GetProperties()
        {
            return properties;
        }
    }
}
