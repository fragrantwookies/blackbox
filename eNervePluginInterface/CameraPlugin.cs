﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace eNervePluginInterface
{
    public class CameraPlugin : ICameraPlugin, IEquatable<CameraPlugin>
    {
        public ICameraServerPlugin Parent { get; set; }
        public bool Admin { get; set; }
        public bool Enabled { get; set; }
        public INerveHostPlugin Host { get; set; }
        public string Name { get; set; }
        public List<StreamDefinition> Streams { get; set; }        

        public string Description
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description; }
        }

        public string DisplayName
        {
            get
            {
                if (Properties.FindProperty("DisplayName") != null)
                {
                    var displayName = Properties.FindProperty("DisplayName").PropertyValue;
                    if (string.IsNullOrEmpty(displayName))
                        return Name;
                    else return displayName;
                }
                else
                {
                    return Name;
                }
            }
        }

        public string Author { get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company; } }

        public string Version { get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); } }

        public string FileName { get { return Assembly.GetCallingAssembly().Location; } set { } }
        public bool Online { get; set; }

        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>
                {
                    new CustomPropertyDefinition("DisplayName", typeof(CustomProperty)),
                    new CustomPropertyDefinition("Server", typeof(CustomProperty)),
                    new CustomPropertyDefinition("Uri", typeof(CustomProperty))
                };
                return result;
            }
        }

        public bool HasWizard { get { return false; } }

        public IPluginWizard Wizard { get { return null; } }

        public event EventHandler<PluginErrorEventArgs> Error;
        public event EventHandler DeviceOnline;
        public event EventHandler DeviceOffline;

        private CustomProperties properties;

        public void Start()
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object other)
        {
            if (other != null)
            {
                if (other is CameraPlugin plugin)
                {
                    return Equals(plugin);
                }
            }
            return false;
        }

        public bool Equals(CameraPlugin other)
        {
            return string.Equals(Properties["Uri"], other.Properties["Uri"], StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
