﻿using System;
using System.Collections.Generic;

namespace eNervePluginInterface
{
    public class StreamDefinition : IComparable<StreamDefinition>, IComparer<StreamDefinition>
    {
        public StreamDefinition(string name, Resolution res, Uri uri)
        {
            Name = name;
            Resolution = res;
            RtspUri = uri;
        }
        public int TotalPixels { get { return Resolution.Height * Resolution.Width; } }
        public string DisplayName { get { return ToString(); } }
        public string Name { get; set; }
        public Resolution Resolution { get; set; }
        public Uri RtspUri { get; set; }

        public int Compare(StreamDefinition x, StreamDefinition y)
        {
            return x.CompareTo(y);
        }

        public int CompareTo(StreamDefinition other)
        {
            return TotalPixels.CompareTo(other.TotalPixels);
        }
        public override string ToString()
        {
            return Name + " " + Resolution.Width + "x" + Resolution.Height;
        }
    }
}
