﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public class ImageCapturedEventArgs : EventArgs
    {
        public byte[] ImageData { get; set; }
        public string EventName { get; set; }
        public DateTime TimeStamp { get; set; }

        public ImageCapturedEventArgs()
        {
            ImageData = null;
            EventName = "";
            TimeStamp = DateTime.MinValue;
        }

        public ImageCapturedEventArgs(byte[] _ImageData, string _EventName, DateTime _TimeStamp)
        {
            ImageData = _ImageData;
            EventName = _EventName;
            TimeStamp = _TimeStamp;
        }
    }
}
