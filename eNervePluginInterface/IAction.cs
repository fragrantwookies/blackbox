﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{

    public interface IAction : IComparable<IAction>, IComparer<IAction>
    {
        string Name { get; set; }
        bool Value { get; set; }
        int ActionOrder { get; set; }
        EventActionType ActionType { get; set; } 
      
        int CompareByActionOrder(IAction x, IAction y);
        int CompareByName(IAction x, IAction y);
    }
}
