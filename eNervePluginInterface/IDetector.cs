﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public interface IDetector
    {
        /// <summary>
        /// Unique name of detector
        /// </summary>
        string Name { get; set; }
        
        /// <summary>
        /// Name of event that will be created when event gets triggered
        /// </summary>
        string EventName { get; set; }
        
        /// <summary>
        /// Description of the physical location of the detector. Something like Boardroom, Server room, or north east corner of perimiter fence. 
        /// Not very important for the software, but could be useful extra info to opperators.
        /// </summary>
        string Location { get; set; }

        DetectorType DetectorType { get; set; }

        /// <summary>
        /// Can be used to identify detectors on complex devices where detectors are grouped or if they are on extension cards
        /// </summary>
        int MajorIndex { get; set; }
        
        /// <summary>
        /// Identify detector on device or extention card.
        /// </summary>
        int MinorIndex { get; set; }

        string ImagePath { get; set; }
        string ImageMap { get; set; }
        string ImageX { get; set; }
        string ImageY { get; set; }
        string ImageWidth { get; set; }
        string ImageHeight { get; set; }
        string ImageRotation { get; set; }

        string DeviceName { get; set; }        
        string DeviceAddress { get; set; }
        string DeviceIP { get; set; }
    }
}
