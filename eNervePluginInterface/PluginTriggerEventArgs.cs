using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    public class PluginTriggerEventArgs : EventArgs
    {
        public PluginTriggerEventArgs(IEventTrigger trigger)
            : base()
        {
            Trigger = trigger;
        }
        public IEventTrigger Trigger { get; set; }
    }
}
