﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

using eThele.Essentials;
using System.Globalization;
using System.Threading;
using System.Net;

namespace eNervePluginInterface
{
    public class PluginManager : IDisposable, INerveHostPlugin
    {
        private bool active = false;

        public static XMLDoc PluginsDoc { get; set; }

        private static ObservableCollection<INervePlugin> plugins;

        private static ObservableCollection<INervePlugin> pluginTypes;

        private ObservableCollection<ICameraPlugin> cameraPlugins;

        private ObservableCollection<ICameraPlugin> cameraPluginTypes;

        public event EventHandler<PluginTriggerEventArgs> OnAlarmTrigger;

        public event EventHandler<ImageCapturedEventArgs> ImageCaptured;

        //public event EventHandler<PluginStatusEventArgs> PluginOnline;
        //public event EventHandler<PluginStatusEventArgs> PluginOffline;
        public event EventHandler<PluginStatusEventArgs> PluginStatus;

        public List<ICameraServerPlugin> CameraServers
        { get; set; }

        public ObservableCollection<ICameraPlugin> ConfiguredCameras
        {
            get
            {
                if (cameraPlugins == null)
                    cameraPlugins = new ObservableCollection<ICameraPlugin>();
                return cameraPlugins;
            }
            set { cameraPlugins = value; }
        }

        public PluginManager(XMLDoc devicesDoc)
        {
            plugins = new ObservableCollection<INervePlugin>();
            PluginsDoc = devicesDoc;
        }

        #region Cleanup
        ~PluginManager()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            ClearPlugins();

            if (PluginsDoc != null)
                PluginsDoc.Dispose();
        }

        public void ClearPlugins()
        {
            try
            {
                if (plugins != null)
                {
                    for (int i = plugins.Count - 1; i >= 0; i--)
                    {
                        //plugins[i].Enabled = false;

                        //INerveEventPlugin eventPlugin = plugins[i] as INerveEventPlugin;
                        //if (eventPlugin != null)
                        //    eventPlugin.Trigger -= eventPlugin_AlarmTrigger;

                        //INerveSnapshotPlugin snapshotPlugin = plugins[i] as INerveSnapshotPlugin;
                        //if (snapshotPlugin != null)
                        //    snapshotPlugin.ImageCaptured -= snapshotPlugin_ImageCaptured;

                        //ICameraServerPlugin cameraServerPlugin = plugins[i] as ICameraServerPlugin;
                        //if (cameraServerPlugin != null)
                        //    cameraServerPlugin.AddCameraEvent -= CameraServer_AddCameraEvent;

                        //plugins[i].Error -= plugin_Error;

                        //if (plugins[i] is IDisposable)
                        //    ((IDisposable)plugins[i]).Dispose();

                        DestroyPlugin(plugins[i]);

                        plugins.RemoveAt(i);
                    }
                }
                if (cameraPlugins != null)
                {
                    for (int i = cameraPlugins.Count - 1; i >= 0; i--)
                    {
                        //if (cameraPlugins[i] is IDisposable)
                        //    ((IDisposable)cameraPlugins[i]).Dispose();

                        DestroyPlugin(cameraPlugins[i]);

                        cameraPlugins.RemoveAt(i);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void DestroyPlugin(INervePlugin _plugin)
        {
            if (plugins != null)
            {
                _plugin.Enabled = false;

                INerveEventPlugin eventPlugin = _plugin as INerveEventPlugin;
                if (eventPlugin != null)
                    eventPlugin.Trigger -= eventPlugin_AlarmTrigger;

                INerveSnapshotPlugin snapshotPlugin = _plugin as INerveSnapshotPlugin;
                if (snapshotPlugin != null)
                    snapshotPlugin.ImageCaptured -= snapshotPlugin_ImageCaptured;

                ICameraServerPlugin cameraServerPlugin = _plugin as ICameraServerPlugin;
                if (cameraServerPlugin != null)
                    cameraServerPlugin.AddCameraEvent -= CameraServer_AddCameraEvent;

                _plugin.DeviceOnline -= plugin_DeviceOnline;
                _plugin.DeviceOffline -= plugin_DeviceOffline;
                _plugin.Error -= plugin_Error;

                if (_plugin is IDisposable)
                    ((IDisposable)_plugin).Dispose();
            }
        }
        #endregion Cleanup

        System.Timers.Timer checkPluginsTimer;

        public void Start()
        {
            active = true;
            LoadPlugins();

            if (checkPluginsTimer == null)
            {
                checkPluginsTimer = new System.Timers.Timer() { Interval = 30000 };
                checkPluginsTimer.Elapsed += checkPluginsTimer_Elapsed;
            }
            checkPluginsTimer.Start();
        }

        void checkPluginsTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (checkPluginsTimer != null)
            {
                checkPluginsTimer.Stop();
            }

            if (active)
            {
                for (int i = plugins.Count - 1; i >= 0; i--)
                {
                    INervePlugin p = plugins[i];
                    if (p == null)
                    {
                        plugins.Remove(p);

                        LoadPlugins();
                    }
                    else
                    {
                        if (!p.Online && p.Enabled)
                        {
                            ThreadPool.QueueUserWorkItem(state => { RestartPlugin(p); });
                            Thread.Sleep(100);
                        }

                        //RaisePluginStatus(p);
                    }
                }
                if (checkPluginsTimer != null)
                {
                    checkPluginsTimer.Start();
                }
            }            
        }

        private void RaisePluginStatus(INervePlugin plugin)
        {
            if (plugin != null && PluginStatus != null)
            {
                PluginStatus status = eNervePluginInterface.PluginStatus.Offline;

                if (plugin.Enabled)
                {
                    if (plugin.Online)
                        status = eNervePluginInterface.PluginStatus.Online;
                    else
                        status = eNervePluginInterface.PluginStatus.Offline;
                }
                else
                    status = eNervePluginInterface.PluginStatus.Disabled;

                PluginStatus(this, new PluginStatusEventArgs(plugin, status));
            }
        }

        public void Stop()
        {
            active = false;
            if (checkPluginsTimer != null)
            {
                checkPluginsTimer.Stop();
            }

            ClearPlugins();
            //foreach (var plugin in plugins)
            //{
            //    if (plugin != null)
            //        plugin.Enabled = false;
            //}
        }

        #region Load Plug-ins

        /// <summary>
        /// Loads customproperties from the doc into the plugin based on the plugin's CustomPropertyNames. Nodes will be created if none are found.
        /// </summary>
        /// <param name="plugin">The plugin to load the custom properties into.</param>
        public static void GetCustomProperties(INervePlugin plugin, bool isCamera = false)
        {
            bool modifyFile = false;
            try
            {
                if (plugin != null)
                {
                    if (plugin.CustomPropertyDefinitions != null)
                    {
                        XmlNode pluginNode;
                        if (isCamera)
                        {
                            var camera = (plugin as ICameraPlugin);
                            var camerasNode = PluginsDoc[camera.Parent.Name]["Cameras"];
                            if (camerasNode == null)
                            {
                                camerasNode = PluginsDoc[camera.Parent.Name].Add("Cameras", "");
                                modifyFile = true;
                            }
                            pluginNode = camerasNode[plugin.Name];
                            if (pluginNode == null)
                            {
                                pluginNode = camerasNode.Add(plugin.Name, "");
                                modifyFile = true;
                            }
                        }
                        else
                        {
                            pluginNode = PluginsDoc[plugin.Name];
                            if (pluginNode == null)
                            {
                                pluginNode = PluginsDoc.Add(plugin.Name, "");
                                modifyFile = true;
                                //pluginsDoc.Save();
                            }
                        }

                        var propertiesNode = pluginNode["Properties"];

                        //create nodes for expected properties that don't exist in the document
                        foreach (CustomPropertyDefinition propertyDefinition in plugin.CustomPropertyDefinitions)
                        {
                            if (propertiesNode == null)
                            {
                                propertiesNode = pluginNode.Add("Properties", "");// Adjust property loading to work with plugins :)
                                propertiesNode.Add(propertyDefinition.PropertyName, ""); // TODO get default value from plugin interface.
                                modifyFile = true;
                            }
                            else
                            {
                                XmlNode propertyNode = propertiesNode[propertyDefinition.PropertyName];
                                if (propertyNode == null)
                                {
                                    propertiesNode.Add(propertyDefinition.PropertyName, "");
                                    modifyFile = true;
                                    //pluginsDoc.Save();
                                }
                            }
                        }
                        //read the values of the properties
                        foreach (XmlNode propertyNode in pluginNode)
                        {
                            if (propertyNode.Name == "Properties")
                            {
                                foreach (var property in propertyNode.Children)
                                {
                                    plugin.Properties.AddProperty(ParseProperty(property));
                                }
                            }
                        }
                        if ((isCamera) && (pluginNode.Attribute("Enabled") == null))
                        {
                            pluginNode.AddAttribute("Enabled", true.ToString());
                            modifyFile = true;
                        }
                    }
                    INerveEventPlugin eventPlugin = plugin as INerveEventPlugin;
                    if (eventPlugin != null)
                    {
                        string[] miscEvents = eventPlugin.MiscellaneousEventNames;
                        if (miscEvents != null && miscEvents.Length > 0)
                        {
                            var pluginNode = PluginsDoc[plugin.Name];
                            if (plugin == null)
                            {
                                pluginNode = PluginsDoc.Add(plugin.Name, "");
                                modifyFile = true;
                            }
                            var eventsNode = pluginNode["MiscellaneousEvents"];
                            if (eventsNode == null)
                            {
                                eventsNode = pluginNode.Add("MiscellaneousEvents", "");
                                modifyFile = true;
                            }
                            foreach (string miscEvent in miscEvents)
                            {
                                eventsNode.Add(miscEvent, "");
                            }
                        }
                    }
                }
                if (modifyFile)
                    PluginsDoc.Save();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public static ICustomProperty ParseProperty(XmlNode _PropNode)
        {
            ICustomProperty result = null;

            if (_PropNode.HasChildren)
            {
                CustomPropertyList propList = (CustomPropertyList)(result = new CustomPropertyList(_PropNode.Name));

                foreach (XmlNode childNode in _PropNode.Children)
                    propList.AddProperty(ParseProperty(childNode));
            }
            else
                result = new CustomProperty(_PropNode.Name, _PropNode.Value);

            return result;
        }

        public static ObservableCollection<INervePlugin> GetPluginList()
        {
            var pluginList = new ObservableCollection<INervePlugin>();
            try
            {
                foreach (var plugin in plugins)
                {
                    if (plugin == null)
                    {
                        plugins.Remove(plugin);
                    }
                    else
                    {
                        INervePlugin pluginItem = (INervePlugin)Activator.CreateInstance(plugin.GetType());
                        if (pluginItem != null)
                        {
                            pluginItem.Host = plugin.Host; // Ensure there's no circular reference, so that the reference isn't circular, otherwise you'll have a circular reference.
                            pluginItem.Name = plugin.Name;

                            GetCustomProperties(plugin);
                            //pluginItem.Properties = plugin.Properties;

                            pluginList.Add(pluginItem);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add("GetPluginList", "Exception occurred while preparing plugin instances to send over the network", ex);
            }
            return pluginList;
        }

        public static ObservableCollection<INervePlugin> GetPluginTypes(bool refresh = true)
        {
            if ((refresh) || (pluginTypes == null))
            {
                pluginTypes = new ObservableCollection<INervePlugin>();
                try
                {
                    //string pluginsFolder = string.Format("{0}\\Ethele\\RioSoft\\plugins", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData));
                    //string pluginsFolder = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "Plugins");
                    string pluginsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Ethele\\RioSoft\\Plugins");
                    Assembly pluginAssembly;

                    foreach (var candidateFile in Directory.GetFiles(pluginsFolder))
                    {
                        try
                        {
                            pluginAssembly = Assembly.LoadFrom(candidateFile);
                            foreach (var type in pluginAssembly.GetTypes())
                            {
                                if (type.IsPublic && !type.IsAbstract && type.GetInterface("eNervePluginInterface.INervePlugin", true) != null && type.GetInterface("eNervePluginInterface.ICameraPlugin", true) == null)
                                {
                                    INervePlugin plugin = (INervePlugin)Activator.CreateInstance(pluginAssembly.GetType(type.ToString()));

                                    //plugin.Host = this;
                                    plugin.Name = PrettifyString(type.Name);
                                    plugin.FileName = candidateFile;

                                    pluginTypes.Add(plugin);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error reading installed plugin types from {0}", candidateFile), ex);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error reading installed plugin types", ex);
                }
            }
            return pluginTypes;
        }

        public static List<string> GetActionProperties(string actionName)
        {
            List<string> actionPropertiesResult = new List<string>();

            if (pluginTypes != null)
            {
                foreach (INervePlugin plugin in pluginTypes)
                {
                    INerveActionPlugin actionPlugin = plugin as INerveActionPlugin;

                    if (actionPlugin != null && actionPlugin.Name == actionName)
                    {
                        foreach (PropertyInfo propInfo in actionPlugin.GetActionType(actionName).GetProperties())
                        {
                            if (propInfo.DisplayInPluginActionPropertiesList())
                                actionPropertiesResult.Add(propInfo.Name);
                        }

                        break;
                    }
                }
            }

            return actionPropertiesResult;
        }

        /// <summary>
        /// Format strings for display. TLA's are retained and spaces added before each word.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string PrettifyString(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return string.Empty;
            }
            else
            {
                StringBuilder newName = new StringBuilder(name[0].ToString());
                if (name.Length > 2)
                {
                    for (int i = 1; i < name.Length; i++)
                    {
                        //if (!char.IsWhiteSpace(name[i]) && !char.IsWhiteSpace(name[i - 1]) && (char.IsUpper(name[i])) && (name.Length > i + 1) && !(char.IsWhiteSpace(name[i + 1])) && !(char.IsUpper(name[i + 1])))
                        if (!char.IsWhiteSpace(name[i]) && !char.IsWhiteSpace(name[i - 1]) && (char.IsUpper(name[i])) && (!char.IsUpper(name[i-1])))
                            newName.Append(" " + name[i]);
                        else
                            newName.Append(name[i].ToString());

                    }
                    return newName.ToString();
                }
                else
                {
                    return name;
                }
            }
        }

        public void LoadPlugins()
        {
            bool modifyFile = false;// Indicate if the file should be modified. This happens if an interface or plugin DLL is updated with new properties.
            try
            {
                //ClearPlugins();
                GetPluginTypes(false); // Ensure plugin types are loaded before trying to instantiate one.

                List<string> pluginsForTheKeeping = new List<string>(); //plugins in this list should not be removed.

                foreach (XmlNode deviceNode in PluginsDoc)
                {
                    try
                    {
                        var propertiesNode = deviceNode["Properties"];
                        if (propertiesNode != null)
                        {
                            var isPluginNode = propertiesNode.FindChildNode("IsPlugin");
                            if (isPluginNode == null)
                            {
                                propertiesNode.Add("IsPlugin", "false");
                                modifyFile = true;
                            }
                            else if (isPluginNode.Value.Equals("True", StringComparison.OrdinalIgnoreCase))
                            {
                                var pluginTypeNode = propertiesNode.FindChildNode("PluginType");
                                if (pluginTypeNode == null)
                                {
                                    propertiesNode.Add("PluginType", "");
                                    modifyFile = true;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(pluginTypeNode.Value))
                                    {
                                        var pluginType = pluginTypes.FirstOrDefault(x => PrettifyString(pluginTypeNode.Value).Contains(x.Name));
                                        if (pluginType != null)
                                        {
                                            lock (plugins)
                                            {
                                                INervePlugin existingPlugin = plugins.FirstOrDefault(x => x.Name == deviceNode.Name);

                                                if (existingPlugin != null)
                                                {
                                                    existingPlugin.Enabled = XmlNode.ParseBool(deviceNode, false, "Enabled");
                                                    ThreadPool.QueueUserWorkItem((state => { RestartPlugin(existingPlugin); }));
                                                }
                                                else
                                                    ThreadPool.QueueUserWorkItem((state => { LoadPlugin(deviceNode.Name, pluginType); }));

                                                pluginsForTheKeeping.Add(deviceNode.Name);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
                    }
                }

                lock (plugins)
                {
                    for (int i = plugins.Count - 1; i >= 0; i--)
                    {
                        INervePlugin plugin = plugins[i];
                        if (!pluginsForTheKeeping.Contains(plugin.Name))
                        {
                            plugins.RemoveAt(i);
                            DestroyPlugin(plugin);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }

            if (modifyFile)
                PluginsDoc.Save();
        }

        public void LoadPlugin(string _name, INervePlugin pluginType)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(_name))
                {
                    if (plugins == null)
                        plugins = new ObservableCollection<INervePlugin>();

                    INervePlugin plugin = (INervePlugin)Activator.CreateInstance(pluginType.GetType());
                    if (plugin != null)
                    {
                        plugin.Host = this;
                        plugin.Name = _name;
                        plugin.FileName = pluginType.FileName;
                        GetCustomProperties(plugin);
                        lock (plugins)
                        {
                            plugins.Add(plugin);
                        }

                        INerveEventPlugin eventPlugin = plugin as INerveEventPlugin;
                        if (eventPlugin != null)
                        {
                            eventPlugin.Trigger -= eventPlugin_AlarmTrigger; // make sure it can't be registered more than once
                            eventPlugin.Trigger += eventPlugin_AlarmTrigger;
                        }

                        INerveSnapshotPlugin snapshotPlugin = plugin as INerveSnapshotPlugin;
                        if (snapshotPlugin != null)
                        {
                            snapshotPlugin.ImageCaptured -= snapshotPlugin_ImageCaptured; // make sure it can't be registered more than once
                            snapshotPlugin.ImageCaptured += snapshotPlugin_ImageCaptured;
                        }

                        plugin.Error -= plugin_Error; // make sure it can't be registered more than once
                        plugin.DeviceOnline -= plugin_DeviceOnline; // make sure it can't be registered more than once
                        plugin.DeviceOffline -= plugin_DeviceOffline; // make sure it can't be registered more than once

                        plugin.Error += plugin_Error;
                        plugin.DeviceOnline += plugin_DeviceOnline;
                        plugin.DeviceOffline += plugin_DeviceOffline;

                        bool enabled = true;
                        var pluginNode = PluginsDoc[plugin.Name];
                        if ((pluginNode != null) && (pluginNode.AttributeCount > 0))
                        {
                            if (PluginsDoc[plugin.Name].Attribute("Enabled") == null)
                            {
                                PluginsDoc[plugin.Name].AddAttribute("Enabled", enabled.ToString());
                            }
                            else
                                enabled = XmlNode.ParseBool(pluginNode, enabled, "Enabled");
                        }
                        else
                        {
                            PluginsDoc[plugin.Name].AddAttribute("Enabled", enabled.ToString());
                        }
                        plugin.Enabled = enabled;

                        #region Cameras
                        if (plugin is ICameraServerPlugin cameraServer)
                        {
                            if (CameraServers == null)
                                CameraServers = new List<ICameraServerPlugin>();
                            if (!CameraServers.Contains(cameraServer))
                                CameraServers.Add(cameraServer);
                            bool admin = false;
                            bool ptz = false;
                            cameraServer.AddCameraEvent += CameraServer_AddCameraEvent;
                            // TODO load Camera types from server plugin DLL
                            LoadCameraPluginType(plugin);

                            // Now that plugins are loaded, cameras can be populated.
                            if ((cameraServer.Enabled) && (PluginsDoc[plugin.Name] != null) && (PluginsDoc[plugin.Name]["Cameras"] != null))
                            {
                                lock (PluginsDoc)
                                {
                                    foreach (XmlNode cameraNode in PluginsDoc[plugin.Name]["Cameras"].Children)
                                    {
                                        enabled = true;
                                        if (cameraNode.AttributeCount > 0)
                                        {
                                            if (cameraNode.Attribute("Enabled") == null)
                                            {
                                                cameraNode.AddAttribute("Enabled", enabled.ToString());
                                            }
                                            else
                                                enabled = XmlNode.ParseBool(cameraNode, enabled, "Enabled");
                                            if (cameraNode.Attribute("Admin") == null)
                                            {
                                                cameraNode.AddAttribute("Admin", enabled.ToString());
                                            }
                                            else
                                                admin = XmlNode.ParseBool(cameraNode, admin, "Admin");
                                            if (cameraNode.Attribute("Ptz") == null)
                                            {
                                                cameraNode.AddAttribute("Ptz", enabled.ToString());
                                            }
                                            else
                                                ptz = XmlNode.ParseBool(cameraNode, ptz, "Ptz");

                                        }
                                        else
                                        {
                                            cameraNode.AddAttribute("Admin", admin.ToString());
                                            cameraNode.AddAttribute("Enabled", enabled.ToString());
                                            cameraNode.AddAttribute("Ptz", ptz.ToString());
                                        }
                                        if (enabled)
                                        {
                                            var propertiesNode = cameraNode["Properties"];
                                            if (propertiesNode == null)
                                            {
                                                cameraNode.Add("Properties", "");
                                            }
                                            else
                                            {
                                                var pluginTypeNode = propertiesNode.FindChildNode("PluginType");
                                                if (pluginTypeNode == null)
                                                {
                                                    propertiesNode.Add("PluginType", "");
                                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Incomplete or invalid XML for item " + cameraNode.Name);
                                                }
                                                else
                                                {
                                                    if (!string.IsNullOrEmpty(pluginTypeNode.Value))
                                                    {
                                                        var cameraType = cameraPluginTypes.FirstOrDefault(x => PrettifyString(pluginTypeNode.Value).Contains(x.Name));
                                                        if (cameraType == null)
                                                        {
                                                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Unable to load camera {0} as the plugin type is not loaded.", cameraNode.Name));
                                                        }
                                                        else
                                                        {
                                                            try
                                                            {
                                                                if (!(plugins.FirstOrDefault(x => PrettifyString(propertiesNode["Parent"].Value).Contains(x.Name)) is ICameraServerPlugin serverInstance))
                                                                {
                                                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Unable to obtain camera server type from XML.");
                                                                }
                                                                else
                                                                {
                                                                    CameraServer_AddCameraEvent(null, new AddCameraEventArgs(cameraType.GetType(), cameraNode.Name, int.Parse(propertiesNode["ChannelId"].Value), serverInstance, admin, ptz));
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        if (plugin.Enabled)
                            plugin.Start();

                        RaisePluginStatus(plugin);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void LoadCameraPluginType(INervePlugin cameraServer)
        {
            if (cameraPluginTypes == null)
                cameraPluginTypes = new ObservableCollection<ICameraPlugin>();
            if (plugins.Contains(cameraServer))
            {
                Assembly pluginAssembly = Assembly.LoadFrom(cameraServer.FileName);
                foreach (var type in pluginAssembly.GetTypes())
                {
                    if (type.IsPublic && !type.IsAbstract && type.GetInterface("eNervePluginInterface.ICameraPlugin", true) != null)
                    {
                        ICameraPlugin plugin = (ICameraPlugin)Activator.CreateInstance(pluginAssembly.GetType(type.ToString()));

                        //plugin.Host = this;
                        plugin.Name = PrettifyString(type.Name);
                        plugin.FileName = cameraServer.FileName;

                        if (!cameraPluginTypes.Contains(plugin))
                            cameraPluginTypes.Add(plugin);
                    }
                }
            }
            else
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Camera server does not exist, or is not configured - can't load cameras.");
            }

        }

        /// <summary>
        /// Load camera event - can be raised from Camera Server or directly called on load from xml
        /// </summary>
        /// <param name="sender">Null if loaded from xml</param>
        /// <param name="e"></param>
        private void CameraServer_AddCameraEvent(object sender, AddCameraEventArgs e)
        {
            if (cameraPluginTypes != null && cameraPluginTypes.Count > 0)
            {
                lock (cameraPluginTypes)
                {
                    if (string.IsNullOrEmpty(e.Name))
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Unable to instantiate camera without a name.");
                    }
                    else
                    {
                        if (e.Plugin is CameraPlugin cameraPlugin)
                        {
                            cameraPlugin.Admin = e.Admin;
                            cameraPlugin.FileName = e.Parent.FileName;
                            cameraPlugin.Host = this;
                            cameraPlugin.Name = e.Name;
                            cameraPlugin.Parent = e.Parent;
                            //GetCustomProperties(cameraPlugin as INervePlugin, true);

                            cameraPlugin.Properties["Server"] = e.Parent.Name;

                            var modifyFile = false;
                            // Ideally this would be generic, and you would populate each configured value into cameraPlugin.Properties
                            if (sender != null)
                            {
                                var camerasNode = PluginsDoc[cameraPlugin.Parent.Name]["Cameras"];
                                if (camerasNode == null)
                                {
                                    camerasNode = PluginsDoc[cameraPlugin.Parent.Name].Add("Cameras", "");
                                    modifyFile = true;
                                }
                                var cameraNode = camerasNode[cameraPlugin.Name];
                                if (cameraNode == null)
                                {
                                    cameraNode = camerasNode.Add(cameraPlugin.Name, "");
                                    modifyFile = true;
                                }
                                var streamsNode = cameraNode["Streams"];
                                if (streamsNode == null)
                                {
                                    streamsNode = cameraNode.Add("Streams", XMLSerializer.Serialize(cameraPlugin.Streams));
                                    modifyFile = true;
                                }
                                else
                                {
                                    string serializedStreams = XMLSerializer.Serialize(cameraPlugin.Streams);
                                    if (streamsNode.Value != serializedStreams)
                                    {
                                        streamsNode.Value = serializedStreams;
                                        modifyFile = true;
                                    }
                                }
                                var propertiesNode = cameraNode["Properties"];
                                if (propertiesNode == null)
                                {
                                    propertiesNode = cameraNode.Add("Properties", "");
                                    modifyFile = true;
                                }
                                foreach (var propertyDefinition in cameraPlugin.CustomPropertyDefinitions)
                                {
                                    var property = propertiesNode[propertyDefinition.PropertyName];
                                    if (property == null)
                                    {
                                        modifyFile = true;
                                        propertiesNode.Add(propertyDefinition.PropertyName, cameraPlugin.Properties[propertyDefinition.PropertyName]);
                                    }
                                    else
                                    {
                                        if (property.Value != cameraPlugin.Properties[propertyDefinition.PropertyName])
                                        {
                                            property.Value = cameraPlugin.Properties[propertyDefinition.PropertyName];
                                            modifyFile = true;
                                        }
                                    }
                                }
                            }

                            if (cameraPlugins == null)
                            {
                                cameraPlugins = new ObservableCollection<ICameraPlugin>
                                {

                                    //cameraPlugin.Parent.Cameras.Add(cameraPlugin);
                                    cameraPlugin
                                };
                                //SaveCameraPlugin(cameraPlugin);
                            }
                            else
                            {
                                if (cameraPlugins.Contains(cameraPlugin))
                                {
                                    var cameraNode = PluginsDoc[e.Parent.Name]["Cameras"][cameraPlugin.Name];
                                    if (cameraNode == null) // Camera can't be in cameraPlugins if no node exists because plugin is populated from doc. Black hole scenario but coded for justin.
                                    {
                                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("ERROR: Could not find {0} in plugins doc.", e.Name));
                                        //SaveCameraPlugin(cameraPlugin);
                                    }
                                    else if (sender != null)
                                    {// Camera already in doc and already configured, but being populated by camera server - compare values.
                                        //if (cameraNode.Name != cameraPlugin.Name)// Impossible
                                        //{
                                        //    modifyFile = true;
                                        //    cameraNode.Name = cameraPlugin.Name;
                                        //}
                                        cameraPlugin.Admin = XmlNode.ParseBool(cameraNode, false, "Admin");
                                        if (modifyFile)
                                        {
                                            SaveCameraPlugin(cameraPlugin);
                                            modifyFile = false;
                                        }
                                        // TODO compare all values and flag modifyFile if anything changes.

                                    }
                                }
                                else
                                {// Cameraplugins exists but this camera is yet to be added.
                                    cameraPlugins.Add(cameraPlugin);
                                    SaveCameraPlugin(cameraPlugin);
                                }
                            }

                            if (modifyFile && sender != null)
                                PluginsDoc.Save();
                            // Don't start the plugin as it shouldn't run on the server. These values are propagated to the client, and the plugin will be started there.
                        }
                        else
                        {
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Could not instantiate " + e.CameraType);
                        }
                    }
                }
            }
            else
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "No types loaded to instantiate.");
            }
        }

        /// <summary>
        /// Saves the camera plugin to the pluginsDoc file.
        /// </summary>
        /// <param name="cameraPlugin"></param>
        private void SaveCameraPlugin(ICameraPlugin cameraPlugin)
        {
            var modifyFile = false;

            if (PluginsDoc[cameraPlugin.Parent.Name] == null)// Can't happen but let's be exhaustive.
                PluginsDoc.Add(cameraPlugin.Parent.Name, "");
            if (PluginsDoc[cameraPlugin.Parent.Name]["Cameras"] == null)
                PluginsDoc[cameraPlugin.Parent.Name].Add("Cameras", "");
            var cameraNode = PluginsDoc[cameraPlugin.Parent.Name]["Cameras"][cameraPlugin.Name];
            if (cameraNode == null)
            {// Create node
                modifyFile = true;
                PluginsDoc[cameraPlugin.Parent.Name]["Cameras"].Add(cameraPlugin.Name, "");
            }
            else
            {// Update node
                if (cameraNode.Name != cameraPlugin.Name)
                {// Should be impossible as the name is used for indexing.
                    cameraNode.Name = cameraPlugin.Name;
                    modifyFile = true;
                }
                //if (cameraNode.Parent.Value != cameraPlugin.Parent.Name)
                //{
                //    cameraNode.Parent.Value = cameraPlugin.Parent.Name;
                //    modifyFile = true;
                //}
                var propertiesNode = cameraNode["Properties"];
                if (propertiesNode == null)
                {
                    cameraNode.Add("Properties", "");
                }

                foreach (var propertyDefinition in cameraPlugin.CustomPropertyDefinitions)
                {
                    var property = propertiesNode[propertyDefinition.PropertyName];
                    if (property == null)
                    {
                        modifyFile = true;
                        propertiesNode.Add(propertyDefinition.PropertyName, cameraPlugin.Properties[propertyDefinition.PropertyName]);
                    }
                    else
                    {
                        if (property.Value != cameraPlugin.Properties[propertyDefinition.PropertyName])
                        {
                            property.Value = cameraPlugin.Properties[propertyDefinition.PropertyName];
                            modifyFile = true;
                        }
                    }
                }
            }

            if (modifyFile)
                PluginsDoc.Save();
        }

        void plugin_DeviceOnline(object sender, EventArgs e)
        {
            if (sender is INervePlugin plugin)
            {
                RaisePluginStatus(plugin);

                //if (PluginOnline != null)
                //    PluginOnline(this, new PluginStatusEventArgs((INervePlugin)sender, eNervePluginInterface.PluginStatus.Online));
            }
        }

        void plugin_DeviceOffline(object sender, EventArgs e)
        {
            if (sender is INervePlugin plugin)
            {
                RaisePluginStatus(plugin);

                //if (PluginOffline != null)
                //    PluginOffline(this, new PluginStatusEventArgs((INervePlugin)sender, eNervePluginInterface.PluginStatus.Offline));

                //if ((active) && (plugin.Enabled) && (!plugin.Online))
                //    ThreadPool.QueueUserWorkItem(state => { RestartPlugin(plugin); });
            }
        }
        #endregion Load Plug-ins

        public static INervePlugin PluginByName(string _pluginName)
        {
            INervePlugin result = null;
            foreach (INervePlugin plugin in plugins)
            {
                if (plugin != null)
                {
                    if (plugin.Name.EndsWith(_pluginName, StringComparison.OrdinalIgnoreCase))
                    {
                        result = plugin;
                        break;
                    }
                }
            }
            return result;
        }

        public bool PerformAction(string _eventName, IEnumerable<IEventTrigger> _triggers, PluginAction _action)
        {
            bool success = false;

            try
            {
                bool found = false;

                foreach (INervePlugin plugin in plugins)
                {
                    if (plugin != null)
                    {
                        if (plugin.Name.EndsWith(_action.PluginName, StringComparison.OrdinalIgnoreCase) && plugin is INerveActionPlugin)
                        {
                            if (_triggers != null)
                                //foreach (IEventTrigger trigger in _triggers)
                                success = ((INerveActionPlugin)plugin).PerformAction(_eventName, _triggers, _action);

                            found = true;
                            break;
                        }
                    }
                    else
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Something went wrong with the plugin, it is null");
                    }
                }

                if (!found)
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Could not find plugin: {0}", _action.PluginName));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return success;
        }

        public Type GetActionType(string _pluginName, string _actionName)
        {
            Type result = null;

            lock (plugins)
            {
                foreach (INerveActionPlugin plugin in plugins.OfType<INerveActionPlugin>())
                {
                    if (plugin.Name.EndsWith(_pluginName, StringComparison.OrdinalIgnoreCase))
                    {
                        result = plugin.GetActionType(_actionName);
                        break;
                    }
                }
            }

            return result;
        }

        public IAction CreateAction(string _pluginName, string _actionName)
        {
            IAction result = null;

            try
            {
                Type actionType = GetActionType(_pluginName, _actionName);

                if(actionType != null)
                    result = (IAction)Activator.CreateInstance(actionType);
                else
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Couldn't find action plugin: {0}, action: {1}", _pluginName, _actionName));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Couldn't load action plugin: {0}, action: {1}", _pluginName, _actionName), ex);
            }

            return result;
        }

        private void RestartPlugin(INervePlugin _plugin)
        {
            DateTime firstTry = DateTime.Now;
            //double pluginRestartTimeoutMinutes = XmlNode.ParseDouble(Settings.TheeSettings["PluginRestartTimeoutMinutes"], 2);

            if ( _plugin != null && _plugin.Enabled && !_plugin.Online/* && ((DateTime.Now - firstTry).TotalMinutes < pluginRestartTimeoutMinutes)*/)
            {
                try
                {
                    _plugin.Start();
                    //Thread.Sleep(1000);
                    //if (_plugin != null && _plugin.Enabled && !_plugin.Online && ((DateTime.Now - firstTry).TotalMinutes < pluginRestartTimeoutMinutes))
                    //    _plugin.Start();
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                        "Error restarting plugin " + _plugin, ex);
                    return;
                }
            }

            RaisePluginStatus(_plugin);
        }

        public static void SetActionProperty(IAction _action, string _propertyName, string _propertyVal)
        {
            try
            {
                Type actionType = _action.GetType();

                Type propertyType = actionType.GetProperty(_propertyName).PropertyType;
                object val = null;

                if (propertyType.IsEnum)
                {
                    if (!string.IsNullOrWhiteSpace(_propertyVal))
                        val = Enum.Parse(propertyType, _propertyVal);
                }
                else if (propertyType == typeof(bool))
                    val = bool.Parse(_propertyVal);
                else if (propertyType == typeof(byte))
                    val = byte.Parse(_propertyVal);
                else if (propertyType == typeof(sbyte))
                    val = sbyte.Parse(_propertyVal);
                else if (propertyType == typeof(Int16))
                    val = Int16.Parse(_propertyVal);
                else if (propertyType == typeof(UInt16))
                    val = UInt16.Parse(_propertyVal);
                else if (propertyType == typeof(int) || propertyType == typeof(Int32))
                    val = int.Parse(_propertyVal);
                else if (propertyType == typeof(uint) || propertyType == typeof(UInt32))
                    val = uint.Parse(_propertyVal);
                else if (propertyType == typeof(Int64) || propertyType == typeof(long))
                    val = Int64.Parse(_propertyVal);
                else if (propertyType == typeof(UInt64) || propertyType == typeof(ulong))
                    val = UInt64.Parse(_propertyVal);
                else if (propertyType == typeof(double))
                    val = double.Parse(_propertyVal);
                else if (propertyType == typeof(float) || propertyType == typeof(Single))
                    val = float.Parse(_propertyVal);
                else if (propertyType == typeof(string))
                    val = _propertyVal;
                else if (propertyType == typeof(DateTime))
                    val = DateTime.ParseExact(_propertyVal, "o", DateTimeFormatInfo.InvariantInfo);
                else if (propertyType == typeof(TimeSpan))
                    val = TimeSpan.Parse(_propertyVal);
                else if (propertyType == typeof(Guid))
                    val = Guid.Parse(_propertyVal);

                if (val != null)
                    actionType.GetProperty(_propertyName).SetValue(_action, val);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void AddCameraServer(IPEndPoint server)
        {
            ICameraServerPlugin cameraServer = (ICameraServerPlugin)Activator.CreateInstance(typeof(CameraServerPlugin));
            cameraServer.Properties["IP"] = server.Address.ToString();

            int index = -1;
            if (CameraServers == null)
                CameraServers = new List<ICameraServerPlugin>();
            else
                index = CameraServers.IndexOf(cameraServer);
            if (index >= 0)
            {
                CameraServers[index].Properties["Port"] = server.Port.ToString();// Port can change on every run
                cameraServer.Name = CameraServers[index].Name; // Name must be loaded from file.
            }
            else
            {
                cameraServer.Properties["IP"] = server.Address.ToString();
                cameraServer.Properties["Port"] = server.Port.ToString();
                cameraServer.Name = PrettifyString(cameraServer.GetType().Name);
                CameraServers.Add(cameraServer);
                plugins.Add(cameraServer);
                LoadCameraPluginType(cameraServer);
                lock (PluginsDoc)
                {
                    XmlNode serverNode = PluginsDoc[cameraServer.Name];
                    if (serverNode == null)
                        serverNode = PluginsDoc.Add(cameraServer.Name, "");

                    if (serverNode["Properties"] == null)
                    {
                        serverNode.Add("Properties", "");
                    }
                    if (serverNode["Properties"]["IP"] == null)
                        serverNode["Properties"].Add("IP", server.Address.ToString());
                    else
                        serverNode["Properties"]["IP"].Value = server.Address.ToString();
                    if (serverNode["Properties"]["Port"] == null)
                        serverNode["Properties"].Add("Port", server.Port.ToString());
                    else
                        serverNode["Properties"]["Port"].Value = server.Port.ToString();
                    if (serverNode["Properties"]["IsPlugin"] == null)
                        serverNode["Properties"].Add("IsPlugin", true.ToString());
                    else
                        serverNode["Properties"]["IsPlugin"].Value = true.ToString();
                    if (serverNode["Properties"]["PluginType"] == null)
                        serverNode["Properties"].Add("PluginType", cameraServer.Name);
                    else
                        serverNode["Properties"]["PluginType"].Value = cameraServer.Name;
                    if (serverNode.Attribute("Enabled") == null)
                        serverNode.AddAttribute("Enabled", true.ToString());
                    else
                        cameraServer.Enabled = XmlNode.ParseBool(serverNode, true, "Enabled");
                    PluginsDoc.Save();
                }
            }
        }

        #region Eventhandler methods
        void plugin_Error(object sender, PluginErrorEventArgs e)
        {
            string pluginName = "Plugin";
            if (sender is INervePlugin)
                pluginName = ((INervePlugin)sender).Name;

            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", pluginName, e.FunctionName), e.Description, e.Exception);
        }

        private void eventPlugin_AlarmTrigger(object sender, PluginTriggerEventArgs e)
        {
            try
            {
                if (OnAlarmTrigger != null)
                    OnAlarmTrigger(sender, e);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        void snapshotPlugin_ImageCaptured(object sender, ImageCapturedEventArgs e)
        {
            try
            {
                if (ImageCaptured != null)
                    ImageCaptured(sender, e);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        /// <summary>
        /// Camera added via message from VideoServer
        /// </summary>
        /// <param name="rtsp">CameraRTSPstreamADDRESS_e.Device.Uri.AbsoluteUri</param>
        /// <param name="serverAddress">Camera server addie</param>
        public void AddVideoCamera(CameraPlugin camera, IPAddress serverAddress)
        {
            if (CameraServers != null && CameraServers.Count > 0)
            {
                AddCameraEventArgs addCamera = new AddCameraEventArgs(camera, CameraServers.FirstOrDefault(x => x.Properties["IP"] == serverAddress.ToString()));
                if (addCamera.Parent != null)
                {
                    CameraServer_AddCameraEvent(addCamera.Parent, addCamera);
                }
            }
        }

        #endregion Eventhandler methods
    }
}
