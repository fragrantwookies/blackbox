﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public delegate void PluginErrorEventHandler(object sender, PluginErrorEventArgs e);

    public interface INervePlugin : IEquatable<object>
    {
        bool Enabled { get; set; }
        INerveHostPlugin Host { get; set; }
        string Name { get; set; }
        string Description { get; }
        string Author { get; }
        string Version { get; }
        string FileName { get; set; }

        bool Online { get; set; }

        CustomProperties Properties { get; }        

        /// <summary>
        /// Names and types of the Custom Properties you expect to be set. Return null if none needed.
        /// </summary>
        List<CustomPropertyDefinition> CustomPropertyDefinitions { get; }

        bool HasWizard { get; }

        IPluginWizard Wizard { get; }

        void Start();

        [field: NonSerialized]
        [NonSerializedXML]        
        event EventHandler<PluginErrorEventArgs> Error;

        [field: NonSerialized]
        [NonSerializedXML]
        event EventHandler DeviceOnline;

        [field: NonSerialized]
        [NonSerializedXML]
        event EventHandler DeviceOffline;
    }
}
