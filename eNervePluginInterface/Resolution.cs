﻿namespace eNervePluginInterface
{
    public class Resolution
    {
        public Resolution(int height, int width)
        {
            Height = height;
            Width = width;
        }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
