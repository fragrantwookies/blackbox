﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Sockets;
using System.Reflection;

namespace eNervePluginInterface
{
    public class CameraServerPlugin : ICameraServerPlugin, IEquatable<CameraServerPlugin>
    {
        [NonSerialized]
        private ObservableCollection<ICameraPlugin> cameras;
        public bool Enabled { get ; set ; }
        public INerveHostPlugin Host { get; set; }
        public string Name { get; set; }

        public string Description
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description; }
        }

        public string Author
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company; }
        }

        public string Version
        {
            get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
        }

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }
        public bool Online { get; set; }

        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }
        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>
                {
                    new CustomPropertyDefinition("Port", typeof(CustomProperty)),
                    new CustomPropertyDefinition("IP", typeof(CustomProperty))
                };
                return result;
            }
        }

        public ObservableCollection<ICameraPlugin> Cameras
        {
            get
            {
                if (cameras == null)
                    cameras = new ObservableCollection<ICameraPlugin>();
                return cameras;
            }
            set
            {
                if (value != null)
                    cameras = value;
            }
        }

        public bool HasWizard { get { return false; } }

        public IPluginWizard Wizard { get { return null; } }

        public event EventHandler<AddCameraEventArgs> AddCameraEvent;
        public event EventHandler<PluginErrorEventArgs> Error;
        public event EventHandler DeviceOnline;
        public event EventHandler DeviceOffline;

        private string fileName;
        private CustomProperties properties;

        public void Start()
        {
            if (int.TryParse(Properties["Port"], out int port))
            {
                using (TcpClient tcp = new TcpClient())
                {
                    try
                    {
                        tcp.Connect(Properties["IP"], port);
                        tcp.Close();
                        Online = true;
                        DeviceOnline?.Invoke(this, new EventArgs());
                    }
                    catch (Exception)
                    {
                        DeviceOffline?.Invoke(this, new EventArgs());
                    }
                }
            }
        }

        public void Stop()
        {
            Online = false;
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CameraServerPlugin);
        }

        public bool Equals(CameraServerPlugin other)
        {
            if (other == null)
                return false;
            else
                return string.Equals(Properties["IP"], other.Properties["IP"], StringComparison.InvariantCultureIgnoreCase);
        }
    }
}
