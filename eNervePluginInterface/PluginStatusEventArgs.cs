using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    public class PluginStatusEventArgs : EventArgs
    {
        public PluginStatusEventArgs(INervePlugin plugin, PluginStatus _Status)
            : base()
        {
            Plugin = plugin;
            Status = _Status;
        }
        public PluginStatusEventArgs()
        {
            Status = PluginStatus.Disabled;
        }

        public INervePlugin Plugin { get; set; }
        public PluginStatus Status { get; set; }
    }
}
