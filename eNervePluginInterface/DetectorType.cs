using System;
using System.Collections.Generic;

namespace eNervePluginInterface
{
    [Serializable]
    public enum DetectorType : byte { Unknown = 0, Analog = 1, Digital = 2, Fibre = 3};
}
