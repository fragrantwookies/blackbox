﻿using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    [Serializable]
    public class PluginDeviceStatus
    {
        public string DeviceName { get; set; }
        public PluginStatus Status { get; set; }
    }
}
