using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    public class CustomPropertyList : ICustomProperty
    {
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }

        private List<ICustomProperty> properties;

        public List<ICustomProperty> Properties
        {
            get
            {
                if (properties == null)
                    properties = new List<ICustomProperty>();

                return properties;
            }
            set
            {
                properties = value;
            }
        }

        public CustomPropertyList(string name)
        {
            PropertyName = name;
        }

        public int AddProperty(ICustomProperty property)
        {
            int result = -1;
            if (property != null)
            {
                int pos = Properties.BinarySearch(property);

                if (pos < 0)
                {
                    Properties.Insert(~pos, property);
                    result = ~pos;
                }
                else
                {
                    if (Properties[pos] is CustomProperty && property is CustomProperty)
                    {
                        if(((CustomProperty)Properties[pos]).PropertyValue != ((CustomProperty)property).PropertyValue)
                            Properties.Insert(pos, property);

                        result = pos;
                    }
                    else if (Properties[pos] is CustomPropertyList && property is CustomPropertyList)
                    {
                        Properties.Insert(pos, property);

                        result = pos;
                    }
                }
            }

            return result;
        }

        public string this[string propName]
        {
            get
            {
                string result = "";

                ICustomProperty newProperty = new CustomProperty(propName, "");

                int pos = Properties.BinarySearch(newProperty);
                CustomProperty resultProperty;
                if (pos >= 0)
                    resultProperty = Properties[pos] as CustomProperty;
                else
                    resultProperty = null;

                if (resultProperty != null)
                    result = resultProperty.PropertyValue;

                return result;
            }
        }

        public ICustomProperty FindProperty(string propName)
        {
            ICustomProperty result = null;

            ICustomProperty newProperty = new CustomProperty(propName, "");

            int pos = Properties.BinarySearch(newProperty);
            if(pos >= 0)
                result = Properties[pos] as ICustomProperty;

            return result;
        }

        public int CompareTo(ICustomProperty other)
        {
            return this.PropertyName.CompareTo(other.PropertyName);
        }

        public int Compare(ICustomProperty x, ICustomProperty y)
        {
            return x.PropertyName.CompareTo(y.PropertyName);
        }
    }
}
