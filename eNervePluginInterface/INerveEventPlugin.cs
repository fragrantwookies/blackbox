using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    public interface INerveEventPlugin : INervePlugin
    {
        event EventHandler<PluginTriggerEventArgs> Trigger;

        string[] EventNames { get; }
        string[] MiscellaneousEventNames { get; }
    }
}
