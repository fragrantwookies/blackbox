﻿using System;
using System.Collections.Generic;

namespace eNervePluginInterface
{
    public interface IEventTrigger : IComparer<IEventTrigger>, IComparable<IEventTrigger>, IEquatable<IEventTrigger>, IEquatable<Guid>
    {
        /// <summary>
        /// This can only be use for triggers that cause only one alarm
        /// </summary>
        Guid EventID { get; set; }
        Guid TriggerID { get; set; }
        string DeviceName { get; set; }
        string DetectorName { get; set; }

        int DetectorGroupIndex { get; set; }
        int DetectorIndex { get; set; }

        /// <summary>
        /// This will be used in the system to identify the alarm/event. Don't leave it blank.
        /// </summary>
        string EventName { get; set; }

        string Location { get; set; }
        string MapName { get; set; }
        DetectorType DetectorType { get; set; }
        bool State { get; set; }
        double EventValue { get; set; }

        /// <summary>
        /// This can be used by plugins to pass data unknown to the engine.
        /// </summary>
        string Data { get; set; }
        
        /// <summary>
        /// The time the trigger was created. Multiple events in a certain timeframe might be needed before an alarm trigger is created.
        /// </summary>
        DateTime TriggerDT { get; set; }
    }     
}
