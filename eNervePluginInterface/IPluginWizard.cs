﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public interface IPluginWizard
    {
        INervePlugin Plugin { get; set; } 

        string UIDevice { get; set; }

        string[] TemplateActions { set; }

        event EventHandler<PluginWizardSavedDetailsEventArgs> Saved;
    }
}
