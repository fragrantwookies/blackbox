using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    /// <summary>
    /// This is intended to give impromptu digital out switching capability to a plugin
    /// </summary>
    public interface INerveDigitalOutputSwitchPlugin : INervePlugin
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputName"></param>
        /// <param name="value"></param>
        /// <param name="duration"></param>
        void SwitchOutput(int index, bool value, int duration = 0);
    }
}
