﻿namespace eNervePluginInterface
{
    public interface ICameraPlugin : INervePlugin
    {
        ICameraServerPlugin Parent { get; set; }

        bool Admin { get; set; }
        
    }
}
