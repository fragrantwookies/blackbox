﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    public class CustomPropertyDefinition
    {
        public string PropertyName { get; set; }
        public string DisplayName { get; set; }
        public string ToolTip { get; set; }
        public string DefaultValue { get; set; }
        public Type PropertyType { get; set; }

        private List<CustomPropertyDefinition> childDefinitions;
        public List<CustomPropertyDefinition> ChildDefinitions 
        { 
            get
            {
                if (childDefinitions == null)
                    childDefinitions = new List<CustomPropertyDefinition>();

                return childDefinitions;
            }
            set
            {
                childDefinitions = value;
            }
        }

        public CustomPropertyDefinition() {}

        public CustomPropertyDefinition(string _PropertyName, Type _PropertyType)
        {
            PropertyName = _PropertyName;
            PropertyType = _PropertyType;
        }

        public CustomPropertyDefinition(string _PropertyName, string _DisplayName, string _ToolTip, Type _PropertyType)
        {
            PropertyName = _PropertyName;
            DisplayName = _DisplayName;
            ToolTip = _ToolTip;
            PropertyType = _PropertyType;
        }
    }
}
