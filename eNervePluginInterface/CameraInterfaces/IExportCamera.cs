﻿using System;

namespace eNervePluginInterface.CameraInterfaces
{
    public interface IExportCamera
    {
        string Export(DateTime start, DateTime end, string folder, IntPtr displayHandle);
    }
}
