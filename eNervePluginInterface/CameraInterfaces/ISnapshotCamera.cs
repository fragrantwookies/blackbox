﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface.CameraInterfaces
{
    public interface ISnapshotCamera
    {
        void SnapShot(IntPtr handle, string fileName);

    }
}
