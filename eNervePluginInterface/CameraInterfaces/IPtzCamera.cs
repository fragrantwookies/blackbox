﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface.CameraInterfaces
{
    /// <summary>
    /// Provides standard PTZ commands. Methods return true on success.
    /// </summary>
    public interface IPtzCamera
    {
        /// <summary>
        /// Pan and Tilt with the provided X and Y vectors
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        bool PanTilt(int x, int y);

        /// <summary>
        /// Stop pan and tilt. Optional - depends on camera.
        /// </summary>
        /// <returns></returns>
        bool StopPanTilt();

        /// <summary>
        /// Zoom to the provided factor.
        /// </summary>
        /// <param name="factor"></param>
        /// <returns></returns>
        bool Zoom(int factor);

        /// <summary>
        /// Optional - stop last zoom command.
        /// </summary>
        /// <returns></returns>
        bool StopZoom();
    }
}
