﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface.CameraInterfaces
{
    /// <summary>
    /// Returns true for success, false for failure. 0 - 100 zoom (0 minimum, 100 maximum)
    /// </summary>
    interface IZoom
    {
        bool Zoom(int zoom);
    }
}
