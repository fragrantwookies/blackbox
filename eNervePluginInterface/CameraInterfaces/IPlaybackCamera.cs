﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface.CameraInterfaces
{
    public interface IPlaybackCamera
    {
        bool GoToTime(DateTime localTime);

        bool GoToPlayback(IntPtr handle);

        bool Pause();

        bool Play();
    }
}
