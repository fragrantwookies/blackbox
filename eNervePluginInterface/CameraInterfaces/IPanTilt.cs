﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface.CameraInterfaces
{
    interface IPanTilt
    {
        /// <summary>
        /// Sends a pan / tilt command. Returns True if successful, false if failed. Pan and Tilt are both 0 - 100
        /// </summary>
        /// <param name="pan"></param>
        /// <param name="tilt"></param>
        /// <returns></returns>
        bool PanTilt(int pan, int tilt);
    }
}
