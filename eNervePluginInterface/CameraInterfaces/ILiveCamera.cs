﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface.CameraInterfaces
{
    public interface ILiveCamera
    {
        void GoToLive(IntPtr handle);

        void StopLive(IntPtr handle);
    }
}
