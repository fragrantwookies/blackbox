using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    /// <summary>
    /// This is intended to give impromptu relay or digital out switching capability to a plugin
    /// </summary>
    public interface INerveRelaySwitchPlugin : INervePlugin
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputName"></param>
        /// <param name="value"></param>
        /// <param name="duration"></param>
        void SwitchRelay(int index, bool value, int duration = 0);
    }
}
