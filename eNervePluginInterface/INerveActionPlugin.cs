using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    public interface INerveActionPlugin : INervePlugin
    {
        Type GetActionType(string _actionName = "");

        IEnumerable<Type> GetActionTypes();

        bool PerformAction(string eventName, IEnumerable<IEventTrigger> triggers, IAction action);
    }
}
