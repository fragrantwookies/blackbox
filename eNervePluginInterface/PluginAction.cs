﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNervePluginInterface
{
    [Serializable]
    public class PluginAction : IAction
    {
        public PluginAction() : base() { }

        public PluginAction(string _name, bool _value, string _pluginName, int _actionOrder = 0)
        {
            Name = _name;
            Value = _value;
            ActionOrder = _actionOrder;
            ActionType = EventActionType.Plugin;
            PluginName = _pluginName;
        }

        public string PluginName { get; set; }

        public string Name { get; set; }

        public bool Value { get; set; }

        public int ActionOrder { get; set; }

        public EventActionType ActionType { get; set; }

        public int CompareTo(IAction other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public int Compare(IAction x, IAction y)
        {
            return x.Name.CompareTo(y.Name);
        }

        public int CompareByActionOrder(IAction x, IAction y)
        {
            if (x.ActionOrder == y.ActionOrder)
            {
                return x.Name.CompareTo(y.Name);
            }
            else
                return x.ActionOrder.CompareTo(y.ActionOrder);
        }


        public int CompareByName(IAction x, IAction y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }
}
