﻿using System;

namespace eNervePluginInterface
{
    public interface IUserInterfacePlugin
    {
        string PluginName { get; }
        string Description { get; }
        string Author { get; }
        string Version { get; }
        string FileName { get; set; }

        #region External Connection
        void Start();
        void Stop();
        #endregion External Connection

        #region Events
        event EventHandler<PluginErrorEventArgs> Error;
        #endregion Events
    }
}
