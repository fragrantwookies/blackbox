using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    [Serializable]
    public enum EventActionType : byte 
    { 
        Unknown = 0, 
        DB = 1, 
        Log = 2, 
        Siren = 3, 
        Relay = 4, 
        Camera = 5, 
        Email = 6, 
        Escalation = 7, 
        SiteInstruction = 8, 
        Plugin = 9, 
        AutoResolve = 10
    }
}
