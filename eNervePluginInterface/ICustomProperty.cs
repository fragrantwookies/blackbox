using System;
using System.Collections.Generic;
using System.Linq;

namespace eNervePluginInterface
{
    public interface ICustomProperty : IComparable<ICustomProperty>, IComparer<ICustomProperty>
    {
        string PropertyName { get; set; }
        string PropertyValue { get; set; }
    }
}
