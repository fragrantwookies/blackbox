﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestActivation
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            lstLog.Items.Clear();

            lstLog.Items.Add(string.Format("Activation Key: {0}", LoadActivationKey()));
            lstLog.Items.Add(string.Format("Product Key: {0}", LoadProductKey()));

            lstLog.Items.Add(string.Format("Product KeyV2: {0}", LoadProductKeyV2()));
        }

        private string LoadActivationKey()
        {
            string activationKey = "";

            try
            {
                using (RegistryKey servicekey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Ethele\\eNerve"))
                {
                    if (servicekey != null)
                    {
                        activationKey = (string)servicekey.GetValue("ActivationCode", "");

                        servicekey.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                lstLog.Items.Add(string.Format("Error loading activation key: {0}", ex.Message));
            }

            return activationKey;
        }

        private string LoadProductKey()
        {
            string processorid = "FreePass";

            try
            {
                using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_Processor"))
                {
                    foreach (ManagementObject mobject in searcher.Get())
                    {
                        processorid = (string)mobject.Properties["ProcessorID"].Value;

                        if (processorid != "FreePass")
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                lstLog.Items.Add(string.Format("Unable to retrieve product key: {0} - {1}", ex.Message, ex.InnerException));
            }

            return processorid;
        }

        private string LoadProductKeyV2()
        { 
            string processorid = "FreePass";

            try
            {
                System.Diagnostics.Process proc = System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo() { FileName = @"wmic", Arguments = "cpu get ProcessorId", UseShellExecute = false, RedirectStandardOutput = true, CreateNoWindow = true });
                while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine().Trim();
                    lstLog.Items.Add(line);
                    if (!string.IsNullOrWhiteSpace(line) && line != "ProcessorId")
                    {
                        processorid = line;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                lstLog.Items.Add(string.Format("Unable to retrieve product key V2: {0}", ex.Message));
            }

            return processorid;
        }
    }
}
