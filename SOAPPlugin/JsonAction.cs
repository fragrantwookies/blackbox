﻿using eNervePluginInterface.CustomAttributes;

namespace SOAPPlugin
{
    class JsonAction : eNervePluginInterface.PluginAction
    {
        public JsonAction() : base() { }

        [DisplayInPluginActionPropertiesList]
        public string ServiceAddress { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ServiceMethod { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ServicePassword { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ServiceUsername { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string JsonString { get; set; }
    }
}
