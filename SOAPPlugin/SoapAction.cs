﻿using eNervePluginInterface.CustomAttributes;

namespace SOAPPlugin
{
    class SoapAction : eNervePluginInterface.PluginAction
    {
        public SoapAction() : base() { }

        [DisplayInPluginActionPropertiesList]
        public string ServiceAddress { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string SoapEnvelope { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string SoapHeader { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ServicePassword { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string SoapString { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ServiceUsername { get; set; }
    }
}
