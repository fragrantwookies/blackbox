﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;
using System.Xml;

using eNervePluginInterface;
using eThele.Essentials;

namespace SOAPPlugin
{
    public class SOAPPlugin : INerveActionPlugin, INerveEventPlugin, IDisposable, INotifyPropertyChanged
    {
        private const int defaultPollInterval = 10000;
        Timer onlineTimer;

        ~SOAPPlugin()
        {
            Dispose(false);
        }

        public string Author
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company;
            }
        }

        private List<CustomPropertyDefinition> customPropertyDefinitions = null;

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                if (customPropertyDefinitions != null)
                    return customPropertyDefinitions;
                else
                {
                    customPropertyDefinitions = new List<CustomPropertyDefinition>();
                    customPropertyDefinitions.Add(new CustomPropertyDefinition("OnlineInterval", "Online Interval",
                        "Interval between polls for connectivity (default 10s)", typeof(CustomProperty))
                    { DefaultValue = defaultPollInterval.ToString() });

                    return customPropertyDefinitions;
                }
            }
        }

        public string Description
        {
            get
            { 
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
            }
        }

        public bool Enabled { get; set; }

        public string[] EventNames
        {
            get { return new string[] { "ResponseReceived" }; }
        }

        private string fileName;

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public bool HasWizard
        {
            get
            { return false; }
        }

        public INerveHostPlugin Host { get; set; }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }

        public string Name { get; set; }

        public bool Online { get; set; }

        private CustomProperties properties;

        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }

        public string Version
        {
            get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        }

        public event EventHandler DeviceOffline;
        public event EventHandler DeviceOnline;
        public event EventHandler<PluginErrorEventArgs> Error;
        public event EventHandler<PluginTriggerEventArgs> Trigger;
        public event PropertyChangedEventHandler PropertyChanged;

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            //Dispose(true);
        }

        private static Dictionary<string, Type> ActionTypeDictionary = new Dictionary<string, Type>(StringComparer.InvariantCultureIgnoreCase)
        {
            { "SoapAction", typeof(SoapAction) }, { "JsonAction", typeof(JsonAction) }
        };

        public Type GetActionType(string actionType = "")
        {
            Type result = null;
            try
            {
                result = ActionTypeDictionary[actionType];
            }
            catch (KeyNotFoundException)
            {
                result = typeof(SoapAction);
            }

            return result;
        }

        public IEnumerable<Type> GetActionTypes()
        {
            return ActionTypeDictionary.Values.ToArray();
        }

        public bool PerformAction(string eventName, IEnumerable<IEventTrigger> triggers, IAction action)
        {
            if (onlineTimer == null)
                CreateTimer();
            else
                onlineTimer.Stop();

            bool success = false;
            
            if (IsConnectedToInternet())
            {
                if (!Online)
                {
                    if (DeviceOnline != null)
                        DeviceOnline(this, new EventArgs());
                }

                SoapAction soapAction = action as SoapAction;
                if (soapAction != null)
                {
                    if (string.IsNullOrWhiteSpace(soapAction.ServiceAddress))
                    {
                        OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                            string.Format("Missing service address", action.ToString()), new Exception()));
                        return false;
                    }
                    try
                    {
                        using (HttpClient client = new HttpClient())
                        {
                            string response;
                            Uri uri = new Uri(soapAction.ServiceAddress);
                            XmlDocument soapEnvelope = CreateSoapEnvelope(soapAction.SoapEnvelope, soapAction.SoapHeader, soapAction.SoapString);
                            HttpWebRequest request = CreateWebRequest(uri, soapAction.SoapHeader);
                            using (Stream stream = request.GetRequestStream())
                            {
                                soapEnvelope.Save(stream);
                            }
                            IAsyncResult result = request.BeginGetResponse(null, null);
                            result.AsyncWaitHandle.WaitOne();

                            using (WebResponse webResponse = request.EndGetResponse(result))
                            {
                                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                                {
                                    response = rd.ReadToEnd();
                                }
                            }
                            if (!string.IsNullOrWhiteSpace(response))
                            {
                                success = true;
                                if (Trigger != null)
                                {
                                    Trigger(this, new PluginTriggerEventArgs(new ResponseTrigger()
                                    {
                                        TriggerID = Guid.NewGuid(),
                                        Data = response,
                                        DetectorGroupIndex = 0,
                                        DetectorIndex = 0,
                                        DetectorName = soapAction.Name,
                                        DetectorType = DetectorType.Unknown,
                                        DeviceName = Name,
                                        EventName = EventNames[0],
                                        State = success,
                                        TriggerDT = DateTime.Now
                                    }));
                                }
                            }
                        }
                    }
                    catch (UriFormatException ex)
                    {
                        OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                            string.Format("Invalid SOAP service address", action.ToString()), ex));
                        return false;
                    }
                }
                else
                {
                    JsonAction jsonAction = action as JsonAction;
                    if (jsonAction != null)
                    {
                        if (string.IsNullOrWhiteSpace(jsonAction.ServiceAddress))
                        {
                            OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                                string.Format("Unable to send SMS - Missing Portal Password", action.ToString()), new Exception()));
                            return false;
                        }
                        try
                        {
                            var webRequest = (HttpWebRequest)WebRequest.Create(jsonAction.ServiceAddress);
                            webRequest.ContentType = "application/json";
                            if (string.IsNullOrWhiteSpace(jsonAction.ServiceMethod))
                            {
                                if (string.IsNullOrWhiteSpace(jsonAction.JsonString))
                                    jsonAction.ServiceMethod = "GET";
                                else
                                    jsonAction.ServiceMethod = "POST";
                            }
                            webRequest.Method = jsonAction.ServiceMethod;
                            if (jsonAction.ServiceMethod == "POST")
                            {
                                using (var streamWriter = new StreamWriter(webRequest.GetRequestStream()))
                                {
                                    streamWriter.Write(FixJsonString(jsonAction.JsonString));
                                    streamWriter.Flush();
                                    streamWriter.Close();
                                }
                            }
                            var response = (HttpWebResponse)webRequest.GetResponse();

                            string result;
                            using (var streamReader = new StreamReader(response.GetResponseStream()))
                            {
                                result = streamReader.ReadToEnd();
                            }
                            if (!string.IsNullOrWhiteSpace(result))
                            {
                                success = true;
                                if (Trigger != null)
                                {
                                    Trigger(this, new PluginTriggerEventArgs(new ResponseTrigger()
                                    {
                                        TriggerID = Guid.NewGuid(),
                                        Data = result,
                                        DetectorGroupIndex = 0,
                                        DetectorIndex = 0,
                                        DetectorName = jsonAction.Name,
                                        DetectorType = DetectorType.Unknown,
                                        DeviceName = Name,
                                        EventName = EventNames[0],
                                        State = success,
                                        TriggerDT = DateTime.Now
                                    }));
                                }
                            }
                        }
                        catch (UriFormatException ex)
                        {
                            OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                                string.Format("Invalid JSON service address", action.ToString()), ex));
                            return false;
                        }
                    }
                }
            }
            else
            {
                if (Online)
                    if (DeviceOffline != null)
                        DeviceOffline(this, new EventArgs());

                onlineTimer.Start();
            }
            return success;
        }

        private string FixJsonString(string jsonString)
        {
            string result = string.Empty;
            if (jsonString.Contains(@"\"))
                result = jsonString.Replace(@"\", "");
            else
                result = jsonString;

            if (!jsonString.Contains("\""))
            {
                StringBuilder newString = new StringBuilder();
                char[] splitters = new char[] {':', ',', '}' };
                foreach (char character in jsonString)
                {
                    if (character == '{')
                        newString.Append("{\"");
                    else if (character == '}')
                        newString.Append("\"}");
                    else if (splitters.Contains(character))
                    {
                        newString.Append("\"" + character + "\"");
                    }
                    else
                    {
                        newString.Append(character);
                    }
                }
                result = newString.ToString();
            }

            return result;
        }

        [DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        public void Start()
        {
            CreateTimer();
            Online = IsConnectedToInternet();
            RaiseStatus(Online);
        }

        private static XmlDocument CreateSoapEnvelope(string soapEnvelope, string soapHeader, string soapString)
        {
            XmlDocument soapEnvelopeDocument = new XmlDocument();

            if (soapEnvelope.Contains("</"))
                soapEnvelope = soapEnvelope.Substring(0, soapEnvelope.IndexOf("</"));
            if (soapString.Contains("Body"))
            {
                soapEnvelopeDocument.LoadXml(soapEnvelope + soapHeader + soapString + "</soapenv:Envelope>");
            }
            else
            {
                soapEnvelopeDocument.LoadXml(soapEnvelope + soapHeader + "<soapenv:Body>" +
                    soapString + "</soapenv:Body></soapenv:Envelope>");
            }
            //soapEnvelopeDocument.LoadXml(@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:xsi=""http://www.w3.org/1999/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/1999/XMLSchema""><SOAP-ENV:Body><RIOPlugin xmlns=""http://tempuri.org/"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/""><int1 xsi:type=""xsd:integer"">12</int1><int2 xsi:type=""xsd:integer"">32</int2></RIOPlugin></SOAP-ENV:Body></SOAP-ENV:Envelope>");
            return soapEnvelopeDocument;
        }

        private void CreateTimer()
        {
            int pollMS = defaultPollInterval;
            string err;
            if (TimeSpanParser.TryParse(Properties["OnlineInterval"], out pollMS, out err))
            {
                if (pollMS > 3600000)
                {
                    pollMS = 3600000;
                    Properties["OnlineInterval"] = "1h";
                    PropertyChangedHandler("Properties");
                }
                else if (pollMS < 10000)
                {
                    Properties["OnlineInterval"] = "10s";
                    PropertyChangedHandler("Properties");
                    pollMS = 10000;
                }
            }
            else
            {
                Properties["OnlineInterval"] = defaultPollInterval / 1000 + "s";
                PropertyChangedHandler("Properties");
            }

            onlineTimer = new Timer(pollMS);
            onlineTimer.Elapsed += timerElapsed;
            onlineTimer.AutoReset = false;
        }

        private static HttpWebRequest CreateWebRequest(Uri url, string header)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAP:Action");
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Accept = "/";
            webRequest.Method = "POST";
            return webRequest;
        }

        protected virtual void Dispose(bool disposing)
        {
            Online = false;
            if (onlineTimer != null)
            {
                onlineTimer.Elapsed -= timerElapsed;
                onlineTimer.Dispose();
                onlineTimer = null;
            }
        }

        private static bool IsConnectedToInternet()
        {
            int Desc;
            return InternetGetConnectedState(out Desc, 0);
        }

        private void OnError(PluginErrorEventArgs e)
        {
            if ((Error != null) && (e != null))
                Error(this, e);
        }

        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(_propertyName));
        }

        private void RaiseStatus(bool online)
        {
            if (online)
            {
                if (DeviceOnline != null)
                    DeviceOnline(this, new EventArgs());
            }
            else
            {
                if (DeviceOffline != null)
                    DeviceOffline(this, new EventArgs());
            }
        }

        private void timerElapsed(object sender, ElapsedEventArgs e)
        {
            if (onlineTimer == null)
                CreateTimer();
            else
            {
                onlineTimer.Stop();

                if (IsConnectedToInternet())
                {
                    if (!Online)
                    {
                        if (DeviceOnline != null)
                            DeviceOnline(this, new EventArgs());
                        Online = true;
                    }
                }
                else
                {
                    if (Online)
                    {
                        if (DeviceOffline != null)
                            DeviceOffline(this, new EventArgs());
                        Online = false;
                    }
                }
                onlineTimer.Start();
            }
        }
    }
}
