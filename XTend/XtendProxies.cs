﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using XPointAudiovisual.Core;
using XtendExtensionMethods;
#endregion

namespace XPointAudiovisual.Xtend {

    public class XEProxy : IDisposable {
        private XCProxy parent;
        public XCProxy Parent { get { return parent; } }

        public byte Index;
        protected string TypeName = "Unknown";
        protected string FuncName = "";

        public List<XMBase> Maps = new List<XMBase>();
        public XMBase this[ int anIndex ] { get { return Maps.First( f => (int)f.MapType == anIndex ); } }
        public XMBase this[ string anIndex ] { get { return Maps.First( f => (int)f.MapType == int.Parse( anIndex ) ); } }

        public List<byte[]> Send ( List<byte[]> aMsgComponentsList ) {
            aMsgComponentsList.Insert( 0, new byte[] { Index } );
            List<byte[]> response = parent.Send( aMsgComponentsList );
            return response;
        }
        // __NOTE__ for development only - features of this nature normally implemented by application.
        public override string ToString () {
            var s = new StringBuilder();
            s.Append( "  Element -- " );
            s.Append( Index );
            s.Append( " -- " + TypeName );
            if ( FuncName != "" )
                s.Append( " -- " + FuncName );
            return s.ToString();
        }
        public XEProxy ( XCProxy aParent ) {
            parent = aParent;
        }
        //public XEProxy ( XCProxy aParent, string aTypeName ) {
        //    parent = aParent;
        //    TypeName = aTypeName;
        //}
        public XEProxy ( XCProxy aParent, string aTypeName, string aRoleName ) {
            parent = aParent;
            TypeName = aTypeName;
            FuncName = aRoleName;
        }
        protected virtual void Dispose ( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
            if ( Maps != null ) {
                foreach ( var i in Maps )
                    i.Dispose();
                Maps.Clear();
                Maps = null;
            }
        }
        ~XEProxy () {
            Dispose( false );
        }
        public void Dispose () {
            Dispose( true );
        }
    }

    public class XCProxy : IDisposable {
        private XHProxy parent;
        public XHProxy Parent { get { return parent; } }

        public byte Index;
        public XCardType CardType;
        protected string ModelName = "Unknown";

        // __NOTE__ "Admin" is now a property returning Elems[0], thus avoiding a dual reference.
        //   this also simplifies some of the Dispose code.
        //public XEAdmin Admin;
        public XEAdmin Admin { get { return (XEAdmin)( Elems[ 0 ] ); } }

        public List<XEProxy> Elems = new List<XEProxy>();
        public XEProxy this[ int anIndex ] { get { return Elems.First( f => f.Index == anIndex ); } }
        public XEProxy this[ string anIndex ] { get { return Elems.First( f => f.Index == int.Parse( anIndex ) ); } }

        public List<byte[]> Send ( List<byte[]> aMsgComponentsList ) {
            aMsgComponentsList.Insert( 0, new byte[] { Index } );
            List<byte[]> response = parent.Send( aMsgComponentsList );
            return response;
        }

        public void Enumerate () {
            // get all children.
        }
        protected void AssignElementIndexes () {
            foreach ( XEProxy e in Elems )
                e.Index = (byte)( Elems.IndexOf( e ) );
        }
        // __NOTE__ for development only - features of this nature normally implemented by application.
        public override string ToString () {
            var s = new StringBuilder();
            s.Append( "  Card -- " );
            s.Append( Index );
            s.Append( " -- " );
            s.Append( ModelName );
            return s.ToString();
        }
        public XCProxy ( XHProxy aParent, XCardType aCardType, bool anIsHubFlag = false ) {
            parent = aParent;
            CardType = aCardType;
            // there is always an Admin element on every card, but not all cards have certain features.
            Elems.Add( new XEAdmin( this, anIsHubFlag ) );
        }
        protected virtual void Dispose ( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
            if ( Elems != null ) {
                foreach ( var i in Elems )
                    i.Dispose();
                Elems.Clear();
                Elems = null;
            }
            // Admin appears in the Elements list and should not be Disposed again.
            //Admin = null;
        }
        ~XCProxy () {
            Dispose( false );
        }
        public void Dispose () {
            Dispose( true );
        }
    }

    class AsyncTcpRxState {
        public TcpClient Client;
        public byte[] RxBuffer = new byte[ 1024 * 16 ];
        public AsyncTcpRxState ( TcpClient aClient ) {
            Client = aClient;
        }
    }

    public class XHProxy : IDisposable {
        public MacAddress MacAddress;
        public IPAddress IpAddress;
        //public UInt16 XonAddress;
        public string Info;

        private Guid id = Guid.NewGuid();

        // default comms timeout is 20 millisecs for local area hubs.
        // (typically, most comms is expected to complete in ~ 1-5 millisecs)
        //private DateTime LastSend = DateTime.Now;
        //public int Timeout = 20;
        private bool isAttached = false;
        private bool isTerminated = false;
        private bool isAttaching = false;
        private bool isTestingAttached = false;

        private List<XLinkPacket> responseAwaitingPackets = new List<XLinkPacket>();
        private List<XLinkPacket> responsePackets = new List<XLinkPacket>();
        private List<XLinkPacket> incomingMessagePackets = new List<XLinkPacket>();

        public TcpClient Client;

        public List<XCProxy> Cards = new List<XCProxy>( 16 );
        public XCProxy this[ int anIndex ] { get { return Cards.First( f => f.Index == anIndex ); } }
        public XCProxy this[ string anIndex ] { get { return Cards.First( f => f.Index == int.Parse( anIndex ) ); } }

        public XCProxy MasterCard { get { return Cards[ 0 ]; } }
        //// all cards have their own admin elements, always at element-0.
        //// the *hub* admin element is therefore always card-0 (master) element-0.
        //// on the master card this includes some features not present on other cards.
        //private XEAdmin adminElement = null;
        //public XEAdmin Admin {
        //    get {
        //        if ( adminElement == null )
        //            try { adminElement = (XEAdmin)( Cards[ 0 ].Elems[ 0 ] ); }
        //            catch { }
        //        return adminElement;
        //    }
        //}
        //public Registration 

        // prevents multiple messages to any single section from being in flight at the same time!
        // reduces concurrency issues by linearizing access to individual elements.
        // but still have to be explicitly careful with concurrency between different sections.
        public object SendAccessLock = new object();

        //public void CheckFullAddressString () {
        //    if ( this.FullAddressString() == "000000-000000" ) {
        //    }
        //}

        public void Detach () {
            //CheckFullAddressString();
            if ( RouteIndex != 0 )
                return;
            try {
                if ( Client != null ) {
                    try {
                        if ( Client.Connected ) {
                            if ( Client.GetStream() != null )
                                Client.GetStream().Close();
                            if ( Client.Client != null ) {
                                //Client.Client.Shutdown( SocketShutdown.Both );
                                Client.Client.Close();
                            }
                            if ( Log.Events.IsTracing( Log.Level.Debug ) ) {
                                Log.Events.Trace( Log.Level.Debug, "<<" + this.FullAddressString() + " has been detached.>>" );
                            }
                        }
                    }
                    catch { }
                    try {
                        Client.Close();
                        Client = null;
                        if ( Log.Events.IsTracing( Log.Level.Debug ) ) {
                            Log.Events.Trace( Log.Level.Debug, "<<" + this.FullAddressString() + " has been closed.>>" );
                        }
                    }
                    catch { }
                }
            }
            catch { }
            finally {
                Client = null;
                isAttached = false;
                isAttaching = false;
            }
        }
        public bool Attach () {
            //CheckFullAddressString();
            if ( RouteIndex != 0 )
                return this.RouteProviders[ 0 ].Attach();
            bool result = false;
            if ( !isAttaching ) {
                try {
                    isAttaching = true;
                    Client = new TcpClient();
                    Client.NoDelay = true;
                    Client.Client.SetSocketOption( SocketOptionLevel.Tcp, SocketOptionName.BsdUrgent, true );
                    Client.Client.SetSocketOption( SocketOptionLevel.Tcp, SocketOptionName.Expedited, true );
                    // __TODO__ 2. change this to BeginConnect with a timeout.
                    // it is currently locking up the whole system when it blocks.
                    // currently this could block indefinitely, which would destroy the parallelism of the system.
                    // could happen if a message is sent to a device which has gone offline.
                    // problem is with synchronization, difficult to make calling code hang around waiting for connection.
                    Client.Connect( IpAddress, (int)IPCommonPorts.XTEND );
                    try {
                        ReceiveInitiate( new AsyncTcpRxState( Client ) );
                        result = isAttached = true;
                    }
                    catch {
                        if (Log.Debug.IsTracing(Log.Level.Error))
                            Log.Debug.Trace("error initiating receive from Xtend device during Attach()");
                        result = isAttached = false;
                    }
                    Log.Level lev = isAttached ? Log.Level.Debug : Log.Level.Error;
                    if ( Log.Events.IsTracing( lev ) ) {
                        Log.Events.Trace( lev, "<<" + this.FullAddressString() 
                            + (isAttached ? " has been" : "has *not* been" ) +" attached.>>" );
                        Log.Events.Trace( lev, //
                            "<<" + Client.Client.LocalEndPoint.ToString() + " / " + Client.Client.RemoteEndPoint.ToString() + ">>" );
                    }
                }
                finally {
                    isAttaching = false;
                }
            }
            return result;
        }
        public bool AttachByNeed ( bool aPositiveCheckFlag ) {
            //CheckFullAddressString();
            if ( RouteIndex != 0 ) {
                if ( this.RouteProviders.Count == 0 )
                    return false;
                return this.RouteProviders[ 0 ].AttachByNeed( aPositiveCheckFlag );
            } else {
                if ( isAttaching )
                    return true;
                else {
                    // this test has the potential to throw an exception, so we wrap it and fix up if it does fail.
                    // note that if this passes it indicates the connection is healthy, so a failure means it needs work.
                    try {
                        //if ( InnerIsAttached() ) return true;
                        bool attached = aPositiveCheckFlag ? InnerIsPositivelyAttached() : InnerIsNominallyAttached();
                        if ( attached ) return true;
                    }
                    catch {
                        // if there was a problem evaluating the 'good-connection' conditions, detach then attach.
                        try { Detach(); }
                        catch { }
                    }
                    return Attach();
                }
            }
        }
        private bool InnerIsNominallyAttached () {
            if ( RouteIndex != 0 )
                return this.RouteProviders[ 0 ].InnerIsNominallyAttached();
            else
                return isAttached && ( Client != null ) && Client.Connected && ( Client.GetStream() != null );
        }
        private bool InnerIsPositivelyAttached () {
            //CheckFullAddressString();
            if ( RouteIndex != 0 )
                return this.RouteProviders[ 0 ].InnerIsPositivelyAttached();
            else {
                bool result = true;
                if ( !InnerIsNominallyAttached() )
                    return false;
                try {
                    isTestingAttached = true;
                    this.MasterCard.Admin.Service.Ping();
                    //Client.GetStream().Write( new byte[ 0 ], 0, 0 );
                }
                catch {
                    result = false;
                    isTestingAttached = false;
                    Detach();
                }
                finally {
                    isTestingAttached = false;
                }
                return result;
            }
        }
        public bool IsAttached () {
            //CheckFullAddressString();
            try { return InnerIsNominallyAttached(); }
            catch { return false; }
        }

        private void ReceiveInitiate ( AsyncTcpRxState aState ) {
            try {
                if ( ( aState != null ) && aState.Client.Connected )
                    Client.Client.BeginReceive( aState.RxBuffer, 0, aState.RxBuffer.Length, SocketFlags.None,
                        new AsyncCallback( ReceiveComplete ), aState );
            }
            catch ( Exception exc ) {
                if (Log.Debug.IsTracing(Log.Level.Error))
                    Log.Debug.TraceException("error initiating receive from Xtend device", exc);
                throw exc;
            }
        }
        // __TODO__ 6. change from all this slicing to using indexes for greater efficiency.
        private byte[] prebuf = new byte[] { };
        private byte[] header = BitConverter.GetBytes( (uint)0xC3A53A5C );

        private void ProcessFramePackets ( List<XRioMsg> aMsgList ) {
            //CheckFullAddressString();
            foreach ( var msg in aMsgList ) {
                // __TODO__ 1. think about indexing errors here, what could happen.
                var c = this[ msg.Card ];
                var e = c[ msg.Element ];
                var m = e[ msg.Map ];
                m.MapResponder( msg.Field, msg.Args );
            }
        }
        private void ProcessXonFramePackets ( XLinkPacket aPacket ) {
            // deconstruct the frame to determine the destination.
            var xonSrcAddr = BitConverter.ToUInt16( aPacket.Frame.Body, 5 );
            var seqNum = BitConverter.ToUInt16( aPacket.Frame.Body, 7 );
            var msgList = XRioMsg.AsMsgs( aPacket.Frame.Body.Slice( 9, aPacket.Frame.Body.Length - 12 ) );
            ProcessFramePackets( msgList );
        }
        // all these magic numbers below are part of the XON message definition.
        // note that the PortMask value is neither checksummed nor sent by fibre.
        private class XonMessageFrame {
            public byte[] Body;
            public byte PortMask;
            public byte FrameType;
            public byte DataLength;
            public UInt16 DstRouteIndex;
            public UInt16 SrcRouteIndex;
            public UInt16 SeqNumber;
            public byte[] Payload;
            public UInt16 CrcCheck;
            public byte XorCheck;

            public XonMessageFrame ( byte[] aDataArray ) {
                Body = aDataArray;
                PortMask = (byte)( Body[ 0 ] & ~0x80 );
                FrameType = (byte)( Body[ 1 ] & ~0x80 );
                DataLength = Body[ 2 ];
                DstRouteIndex = BitConverter.ToUInt16( Body, 3 );
                SrcRouteIndex = BitConverter.ToUInt16( Body, 5 );
                SeqNumber = BitConverter.ToUInt16( Body, 7 );
                // don't want the payload length included.
                Payload = Body.Slice( 9 + 2, DataLength - 2 );
                CrcCheck = BitConverter.ToUInt16( Body, 9 + DataLength );
                XorCheck = Body[ 9 + DataLength + 2 ];
                UInt16 checkBytes = (UInt16)( Body.Length - 4 );
                UInt16 CalcCrcCheck = new XCrc().BufferCalc( Body.Slice( 1, checkBytes ), checkBytes );
                // note that the xor-check includes the previous CRC bytes!
                byte CalcXorCheck = 0xFF;
                for ( int i = 1; i <= ( checkBytes + 2 ); ++i )
                    CalcXorCheck ^= Body[ i ];
            }
            public XonMessageFrame (
                int aPortMask, int aFrameType,
                int aDstRouteIndex, int aSrcRouteIndex, int aSeqNumber, byte[] aPayload ) {
                DstRouteIndex = (UInt16)aDstRouteIndex;
                SrcRouteIndex = (UInt16)aSrcRouteIndex;
                SeqNumber = (UInt16)aSeqNumber;
                Payload = aPayload;
                Body = new byte[ 9 + Payload.Length + 3 ];
                Body[ 0 ] = (byte)aPortMask;
                Body[ 1 ] = (byte)aFrameType;
                Body[ 2 ] = (byte)Payload.Length;
                BitConverter.GetBytes( DstRouteIndex ).CopyTo( Body, 3 );
                BitConverter.GetBytes( aSrcRouteIndex ).CopyTo( Body, 5 );
                BitConverter.GetBytes( aSeqNumber ).CopyTo( Body, 7 );
                aPayload.CopyTo( Body, 9 );
                // calc dual CRC values.
                UInt16 checkBytes = (UInt16)( Body.Length - 4 );
                UInt16 CalcCrcCheck = new XCrc().BufferCalc( Body.Slice( 1, checkBytes ), checkBytes );
                CrcCheck = CalcCrcCheck;
                BitConverter.GetBytes( CrcCheck ).CopyTo( Body, Body.Length - 3 );
                // note that the xor-check includes the previous CRC bytes!
                byte CalcXorCheck = 0xFF;
                for ( int i = 1; i <= ( checkBytes + 2 ); ++i )
                    CalcXorCheck ^= Body[ i ];
                XorCheck = CalcXorCheck;
                Body[ Body.Length - 1 ] = XorCheck;
            }
        }
        private class XonAnswerFrame {
            public byte[] Body;
            public byte PortMask;
            public byte FrameType;
            public byte DataLength;
            public UInt16 SrcRouteIndex;
            public UInt16 DstRouteIndex;
            public UInt16 SeqNumber;
            public UInt16 AnsNumber;
            public byte[] Payload;
            public UInt16 CrcCheck;
            public byte XorCheck;

            public XonAnswerFrame ( byte[] aDataArray ) {
                Body = aDataArray;
                PortMask = (byte)( Body[ 0 ] & ~0x80 );
                FrameType = (byte)( Body[ 1 ] & ~0x80 );
                DataLength = Body[ 2 ];
                DstRouteIndex = BitConverter.ToUInt16( Body, 3 );
                SrcRouteIndex = BitConverter.ToUInt16( Body, 5 );
                SeqNumber = BitConverter.ToUInt16( Body, 7 );
                AnsNumber = BitConverter.ToUInt16( Body, 9 );
                // don't want the payload length included.
                Payload = DataLength != 0 ? Body.Slice( 11 + 2, DataLength - 2 ) : new byte[ 0 ];
                CrcCheck = BitConverter.ToUInt16( Body, 11 + DataLength );
                XorCheck = Body[ 11 + DataLength + 2 ];
                UInt16 checkBytes = (UInt16)( Body.Length - 4 );
                UInt16 CalcCrcCheck = new XCrc().BufferCalc( Body.Slice( 1, checkBytes ), checkBytes );
                // note that the xor-check includes the previous CRC bytes!
                byte CalcXorCheck = 0xFF;
                for ( int i = 1; i <= ( checkBytes + 2 ); ++i )
                    CalcXorCheck ^= Body[ i ];
            }
            public XonAnswerFrame (
                int aPortMask, int aFrameType,
                int aDstRouteIndex, int aSrcRouteIndex, int aSeqNumber, int anAnsNumber, byte[] aPayload ) {
                DstRouteIndex = (UInt16)aDstRouteIndex;
                SrcRouteIndex = (UInt16)aSrcRouteIndex;
                SeqNumber = (UInt16)aSeqNumber;
                AnsNumber = (UInt16)anAnsNumber;
                Payload = aPayload;
                Body = new byte[ 11 + Payload.Length + 3 ];
                Body[ 0 ] = (byte)aPortMask;
                Body[ 1 ] = (byte)aFrameType;
                Body[ 2 ] = (byte)Payload.Length;
                BitConverter.GetBytes( DstRouteIndex ).CopyTo( Body, 3 );
                BitConverter.GetBytes( aSrcRouteIndex ).CopyTo( Body, 5 );
                BitConverter.GetBytes( aSeqNumber ).CopyTo( Body, 7 );
                BitConverter.GetBytes( anAnsNumber ).CopyTo( Body, 9 );
                UInt16 checkBytes = (UInt16)( Body.Length - 4 );
                UInt16 CalcCrcCheck = new XCrc().BufferCalc( Body.Slice( 1, checkBytes ), checkBytes );
                CrcCheck = CalcCrcCheck;
                BitConverter.GetBytes( CrcCheck ).CopyTo( Body, Body.Length - 3 );
                // note that the xor-check includes the previous CRC bytes!
                byte CalcXorCheck = 0xFF;
                for ( int i = 1; i <= ( checkBytes + 2 ); ++i )
                    CalcXorCheck ^= Body[ i ];
                XorCheck = CalcXorCheck;
                Body[ Body.Length - 1 ] = XorCheck;
            }
        }

        private void RegisterRemoteHub ( XonMessageFrame anXonMsg ) {
            //CheckFullAddressString();
            byte[] payload = anXonMsg.Payload;
            IPEndPoint srcEndpoint = (IPEndPoint)Client.Client.RemoteEndPoint;
            var ops = new List<XRioMsg>();
            ops.Add( new XRioMsg( 0, 0, (int)EXMapType.Routing, 0, //
                XMapRouting.Routing.Address, BitConverter.GetBytes( anXonMsg.SrcRouteIndex ) ) );
            byte[] macArgs = new byte[ 8 ];
            macArgs[ 0 ] = 6;
            macArgs[ 1 ] = 0;
            payload.Slice( 7, 6 ).CopyTo( macArgs, 2 );
            XRioMsg macReport = new XRioMsg( 0, 0, (int)EXMapType.Registration, //
                (int)EXFieldAccessMode.Push, XMapRegistration.PublishMac.Address, //
                macArgs );
            ops.Add( macReport );
            string description = "";
            int cardType = BitConverter.ToUInt16( payload, 0x0D );
            switch ( cardType ) {
                case (int)XCardType.FSS_Controller_M0601_v0200:
                    description = "XPAV RFS-1 Remote Fibre Station";
                    break;
            }
            byte[] descArgs = new byte[ 2 + description.Length ];
            descArgs[ 0 ] = (byte)( descArgs.Length - 2 );
            descArgs[ 1 ] = 0;
            for ( int i = 0; i < description.Length; ++i )
                descArgs[ i + 2 ] = (byte)description[ i ];
            ops.Add( new XRioMsg( 0, 0, (int)EXMapType.Registration, 0, //
                XMapRegistration.PublishDesc.Address, descArgs ) );
            ops.Add( new XRioMsg( 0, 0, 0, 0, (int)EXMapType.Status, new byte[] { 0x01, 0x01, 0, 0 } ) );
            ops.Add( new XRioMsg( 0, 0, (int)EXMapType.Registration, 0, //
                XMapRegistration.PublishCard.Address,
                new byte[] { 0, (byte)( cardType & 0xFF ), (byte)( ( cardType >> 8 ) & 0xFF ) } ) );
            int cardsIndex, cardsStartIndex = 0x0f;
            for ( cardsIndex = cardsStartIndex; cardsIndex < payload.Length; ) {
                byte slaveCardSlot = payload[ cardsIndex ];
                int slaveCardType = BitConverter.ToUInt16( payload, cardsIndex + 1 );
                ops.Add( new XRioMsg( 0, 0, (int)EXMapType.Registration, 0, //
                    XMapRegistration.PublishCard.Address,
                    new byte[] { slaveCardSlot, (byte)( slaveCardType & 0xFF ), (byte)( slaveCardType >> 8 ) } ) );
                cardsIndex += 3;
            }
            XFinder.DiscRspData discData = new XFinder.DiscRspData( srcEndpoint, macReport, ops );
            XFinder.UpdateRemoteDiscovery( discData );
        }
        private static object thread_lock = new object();

        private List<XLinkPacket> packets = new List<XLinkPacket>();
        private void DequeueFramePackets () {
            while ( packets.Count != 0 ) {
                XLinkPacket packet = null;
                lock ( packets ) {
                    try {
                        if ( packets.Count != 0 ) {
                            packet = packets[ 0 ];
                            packets.RemoveAt( 0 );
                        }
                    }
                    catch {
                        Debug.WriteLine( "packet queue was already empty!" );
                        continue;
                    }
                }
                if ( packet == null )
                    continue;
                var protocolHeader = packet.Frame.Body[ 1 ];
                if ( ( protocolHeader & 0x80 ) == 0 ) {
                    // if this is *not* an extended packet format, then it is a standard XTEND packet.
                    // in that case it must be coming to this hub proxy, so handle it as usual.
                    var msgList = XRioMsg.AsMsgs( packet.Frame.Body );
                    ProcessFramePackets( msgList );
                } else {

                    //lock ( thread_lock ) 
                    {
                        // this is an XON packet, and needs to be unpacked.
                        if ( ( protocolHeader == 0x81 ) || ( protocolHeader == 0x83 ) ) {
                            // this is an XON message, so give it to the real hub proxy owner.
                            var xonMessage = new XonMessageFrame( packet.Frame.Body );
                            var remote = XIO.DetectedHubs.FirstOrDefault( x => x.RouteIndex == xonMessage.SrcRouteIndex );
                            byte[] payload = xonMessage.Payload;
                            // __TODO__ 3.  !!! this is a kluge, needs to be generalized.
                            bool isRegistering = ( payload[ 0 ] == 0 ) && ( payload[ 1 ] == 0 )
                                    && ( payload[ 2 ] == (int)EXMapType.Registration )
                                    && ( payload[ 3 ] == XMapRegistration.PublishCompact.Address );
                            if ( ( remote == null ) && !isRegistering )
                                continue;
                            if ( isRegistering ) {
                                //lock ( thread_lock ) {
                                RegisterRemoteHub( xonMessage );
                                //}
                            } else {
                                // ProcessFramePackets( XRioMsg.AsMsgs( xonMessage.Payload ) );
                                lock ( thread_lock ) {
                                    byte[] p = new byte[ xonMessage.DataLength ];
                                    p[ 0 ] = (byte)( payload.Length & 0xFF );
                                    p[ 1 ] = (byte)( payload.Length >> 8 );
                                    Array.Copy( payload, 0, p, 2, payload.Length );
                                    remote.ProcessFramePackets( XRioMsg.AsMsgs( p ) );
                                }
                            }
                        } else if ( protocolHeader == 0x82 ) {
                            // this is an XON answer, so send it to the right place.
                            var xonMessage = new XonAnswerFrame( packet.Frame.Body );
                            var remote = XIO.DetectedHubs.FirstOrDefault( x => x.RouteIndex == xonMessage.SrcRouteIndex );
                            if ( remote == null )
                                continue;
                            XLinkPacket waiter = null;
                            lock ( remote.responseAwaitingPackets )
                                waiter = remote.responseAwaitingPackets.FirstOrDefault( x => xonMessage.AnsNumber == x.RouteSeq );
                            if ( waiter == null ) {
                                Debug.WriteLine( this.MacAddress + " received packet without response waiting." );
                                Debug.Indent();
                                foreach ( byte b in xonMessage.Body ) {
                                    Debug.Write( b.ToString( "x" ) );
                                }
                                Debug.WriteLine( "" );
                                Debug.Unindent();
                            } else {
                                packet.UniqueID = waiter.UniqueID;
                                lock ( remote.responsePackets )
                                    remote.responsePackets.Add( packet );
                                waiter.Signal.Set();
                            }
                        }
                    }


                }

            }
        }

        private void HandleExternallyInitiatedFrame ( XLinkPacket packet ) {
            // __TODO__ 6. fix this for performance, there's too much locking.
            // we could probably use some sort of optimistic queue.
            lock ( packets )
                packets.Add( packet );
            XIO.Do( "dequeue frame packets", () => DequeueFramePackets() );
        }
        private void HandleResponseFrame ( XLinkPacket aPacket ) {
            XLinkPacket waiter = null;
            lock ( responseAwaitingPackets )
                waiter = responseAwaitingPackets.FirstOrDefault( x => aPacket.UniqueID == x.UniqueID );
            if ( waiter != null ) {
                lock ( responsePackets )
                    responsePackets.Add( aPacket );
                waiter.Signal.Set();
            }
        }

        private object lockObject = new object();
        private void ReceiveComplete ( IAsyncResult anAsyncResult ) {
            var state = (AsyncTcpRxState)( anAsyncResult.AsyncState );
            if ( !state.Client.Connected )
                return;
            var client = state.Client.Client;
            try {
                int read = client.EndReceive( anAsyncResult );
                if ( read == 0 ) {
                    isTerminated = true;
                    Detach();
                    return;
                } 
                byte[] buf = new byte[ prebuf.Length + read ];
                Array.Copy( prebuf, buf, prebuf.Length );
                Array.Copy( state.RxBuffer, 0, buf, prebuf.Length, read );
                while ( true ) {
                    // needs at least the header, length, CRC and footer.
                    if ( buf.Length < 16 )
                        break;
                    int begin = 0, next, length;
                    if ( ( begin = buf.Match( header, 0 ) ) == -1 ) {
                        // must still keep at least some of the tail in case it is a partial header, rest to follow.
                        int keep = Math.Min( buf.Length, 3 );
                        buf = buf.Slice( buf.Length - keep, keep );
                        break;
                    }
                    length = BitConverter.ToInt32( buf, begin + 4 );
                    // handle case of length > amount of data available.
                    // __TODO__ 1. consider failure mode where length has been corrupted.
                    if ( buf.Length < ( 4 + 4 + length + 4 + 4 ) )
                        break;
                    // __TODO__ 1. handle where length is more than should be allowed based on limiting of frame sizes.
                    if ( BitConverter.ToUInt32( buf, begin + length + 12 ) != ~0xC3A53A5C ) {
                        // although the header exists, something is wrong with the packet.
                        // so toss away this header, and wait for the next one to arrive.
                        // (note that first char of header never appears in next three chars, so can discard all four.)
                        // can't just toss the whole buffer, might contain another header somewhere.
                        buf = buf.Slice( 4, buf.Length - 4 );
                        continue;
                    }
                    next = length + 16;
                    var data = buf.Slice( begin, next );
                    if ( buf.Length == data.Length )
                        buf = new byte[] { };
                    else if ( begin == 0 )
                        buf = buf.Slice( next, buf.Length - next );
                    else {
                        buf = buf.Slice( begin + next, buf.Length - ( begin + next ) );
                    }
                    //lock ( lockObject ) 
                    {
                        var packet = XLinkPacket.FromNetworkBytes( data );
                        if ( packet.Frame.Protocol == XtendProtocolSelect.XtendInitiate )
                            // __NOTE__ we shuffle handling off into the threadpool, otherwise we get reentrancy problems.
                            HandleExternallyInitiatedFrame( packet );
                        else if ( packet.Frame.Protocol == XtendProtocolSelect.XtendRespond ) {
                            HandleResponseFrame( packet );
                        }
                    }
                }
                prebuf = buf;
            }
            catch ( ObjectDisposedException ) {
                isTerminated = true;
            }
            catch ( SocketException ) {
                isTerminated = true;
            }
            finally {
                if ( isTerminated ) {
                    isTerminated = false;
                    Detach();
                } else
                    try {
                        ReceiveInitiate( state );
                    }
                    catch ( Exception exc) {
                        if ( Log.Debug.IsTracing( Log.Level.Error ) )
                            Log.Debug.TraceException( "error initiating receive from Xtend device", exc );
                    }
            }
        }

        Stopwatch lastSent = new Stopwatch();
        private bool Enqueued ( XLinkPacket aPacket ) {
            try {
                // now poke this out the Ethernet port, seems to probably be alive.
                // __TODO__ 1. make this be handled by a separate thread to prevent blockages if remote is down.
                if ( !Client.Connected )
                    return false;
                NetworkStream stream = Client.GetStream();
                if ( stream == null )
                    return false;
                TimeSpan SinceLastSent = lastSent.Elapsed;
                if ( SinceLastSent.Milliseconds < 10 ) {
                    int sleepPeriod = 10 - SinceLastSent.Milliseconds + (UInt16)( new Random().Next( 5 ) );
                    Thread.Sleep( sleepPeriod );
                }
                var buffer = aPacket.ToNetworkBytes();
                stream.Write( buffer, 0, buffer.Length );
                ( lastSent = new Stopwatch() ).Start();
                return true;
            }
            catch {
                return false;
            }
        }

        private bool Dispatch ( XLinkPacket aPacket ) {
            // __TODO__ 1. make this be handled by a separate thread to prevent blockages.
            //CheckFullAddressString();
            if ( !isTestingAttached )
                AttachByNeed( false );
            if ( this.RouteIndex == 0 )
                return Enqueued( aPacket );
            else {
                if ( this.RouteProviders.Count == 0 )
                    return false;
                return this.RouteProviders[ 0 ].Enqueued( aPacket );
            }
        }

        //        private int ResponseTimeout = 1000;
        private int ResponseTimeout = 1000;
        public void SetResponseTimeout ( int aResponseTimeout ) { ResponseTimeout = aResponseTimeout; }

        //byte[] enc = new byte[] { 0x05, 0x06, 0x09, 0x0A };
        //byte[] ManchesterEncodeByte ( byte val ) {
        //    UInt16 result = 0;
        //    for ( int i = 0; i < 4; ++i, val >>= 2 )
        //        result = (UInt16)( ( result << 4 ) | enc[ val & 0x03 ] );
        //    return new byte[] { (byte)( result & 0xFF ), (byte)( result >> 8 ) };
        //}
        //byte[] MakeManchesterBuffer ( byte[] data ) {
        //    byte[] man = new byte[ data.Length * 2 ];
        //    for ( int i = 0; i < data.Length; ++i )
        //        ManchesterEncodeByte( data[ i ] ).CopyTo( man, 2 * i );
        //    return man;
        //}

        volatile bool trapThisResponse = false;
        private static object lockResponses = new object();

        // __TODO__ 2. look into queuing to manage reentrancy issues.
        public List<byte[]> Send ( List<byte[]> aMsgComponentsList ) {
            //CheckFullAddressString();
            lock ( SendAccessLock ) {
                byte[] prefix = new byte[] { 
                aMsgComponentsList[ 0 ][ 0 ], aMsgComponentsList[ 1 ][ 0 ], aMsgComponentsList[ 2 ][ 0 ] };
                aMsgComponentsList.RemoveRange( 0, 3 );
                int length = 0;
                for ( int next = 0; next < aMsgComponentsList.Count; ++next ) // total space required to package commands.
                    length += 2 + 3 + aMsgComponentsList[ next ].Length; // part-length + prefix + operator + field + data.
                byte[] data = new byte[ length ];
                int count = 0;
                bool requiresResponse = false;
                foreach ( byte[] ba in aMsgComponentsList ) {
                    if ( !requiresResponse ) {
                        // __WARN__ this index has caused trouble before when it was hardcoded.
                        // __NOTE__ that the index is less 3 because that is the size of the stripped-off prefix.
                        var op = (EXFieldAccessMode)ba[ XMBase.ResModeOfs - 3 ];
                        if ( ( op == EXFieldAccessMode.Query )
                            || ( op == EXFieldAccessMode.Order )
                            || ( op == EXFieldAccessMode.Push ) )
                            requiresResponse = true;
                    }
                    Array.Copy( BitConverter.GetBytes( (ushort)( ba.Length + 3 ) ), 0, data, count, 2 );
                    Array.Copy( prefix, 0, data, count + 2, 3 );
                    Array.Copy( ba, 0, data, count + 2 + 3, ba.Length );
                    count += 2 + 3 + ba.Length;
                }

                trapThisResponse = false;

                // clear out responses queue of all unawaited messages.
                // could be done with a manager-style poll, but rather do the work as needed.
                // __NOTE__ we have to do this clumsy dual-list thing because we can't modify while looping.
                if ( responsePackets.Count != 0 ) {
                    List<XLinkPacket> removals = null;
                    IEnumerable<XLinkPacket> responses = null;
                    lock ( responsePackets )
                        responses = responsePackets.Where( x => true );
                    foreach ( var r in responses ) {
                        // check if there is actually a message awaiting this response, if not kill it.
                        IEnumerable<XLinkPacket> all = null;
                        lock ( responseAwaitingPackets )
                            all = responseAwaitingPackets.Where( x => r.UniqueID == x.UniqueID );
                        // if there is a first packet to be removed, define some space for it.
                        if ( ( all.Count() == 0 ) && ( removals == null ) )
                            removals = new List<XLinkPacket>();
                        removals.Add( r );
                    }
                    if ( removals != null )
                        lock ( responsePackets )
                            foreach ( var r in removals ) {
                                responsePackets.Remove( r );
                            }
                }
                // __TODO__ 0. if encrypting key then secure else open.
                // this should be a feature of the hub, ascertained at registration time.
                XLinkPacket packet = new XLinkPacket( XKeys.XPAV, data );
                if ( this.RouteIndex != 0 ) {
                    trapThisResponse = true;
                    var xonMsg = new XonMessageFrame( 0x83, 0x81, this.RouteIndex, 0, RouteTxSeq++, data );
                    packet = new XLinkPacket( XKeys.XPAV, xonMsg.Body, RouteIndex, xonMsg.SeqNumber );
                }
                if ( ( packet.RequiresResponse = requiresResponse ) )
                    lock ( responseAwaitingPackets ) {
                        if ( trapThisResponse )
                            trapThisResponse = trapThisResponse;
                        responseAwaitingPackets.Add( packet );
                    }
                var dispatched = Dispatch( packet );
                if ( !dispatched )
                    throw new XDeliveryFailureException( "could not dispatch" );

                // if it should be responded to, then wait around until it is, or timeout & return a null failure.
                List<byte[]> result = null;
                if ( requiresResponse ) {
                    // __NOTE__ responses must also have a word length prefix per individual part, like commands.
                    // __TODO__ 2. use a more representative *dynamic* timeout for an Ethernet-connected real-time device.
                    //    this could be a configuration value, or something calculated based on history, or both.
                    // __NOTE__ the connection takes longer for the first message, so we preconnect.
                    Stopwatch timer = new Stopwatch();
                    timer.Start();
                    bool signalled = packet.Signal.WaitOne( ResponseTimeout );
                    timer.Stop();
                    long delay = timer.ElapsedMilliseconds;
                    // check if result succeeded or not, will always return eventually.
                    XLinkPacket response = null;
                    //IEnumerable<XLinkPacket> allResponsesIterator = null;
                    //lock ( responsePackets )
                    //    allResponsesIterator = responsePackets.Where( x => packet.UniqueID == x.UniqueID );
                    //var allResponsesList = new List<XLinkPacket>();
                    //foreach ( var r in allResponsesIterator )
                    //    allResponsesList.Add( r );
                    //if ( allResponsesList.Count != 0 )
                    //    lock ( responsePackets )
                    //        foreach ( var r in allResponsesList )
                    //            responsePackets.Remove( ( response = r ) );

                    IEnumerable<XLinkPacket> allResponsesIterator = null;
                    lock ( responsePackets )
                        allResponsesIterator = responsePackets.Where( x => packet.UniqueID == x.UniqueID );
                    var allResponsesList = new List<XLinkPacket>();
                    foreach ( var r in allResponsesIterator )
                        allResponsesList.Add( r );
                    if ( allResponsesList.Count != 0 )
                        lock ( responsePackets )
                            foreach ( var r in allResponsesList )
                                responsePackets.Remove( ( response = r ) );

                    // __NOTE__ only remove after clearing responses list!
                    // otherwise has a race hazard with some other thread clearing unawaited responses.
                    lock ( responseAwaitingPackets )
                        responseAwaitingPackets.Remove( packet );
                    if ( signalled && ( response != null ) ) {
                        //return XRioMsg.AsRawSegs( response.Body );
                        if ( trapThisResponse ) {
                            trapThisResponse = false;
                        }
                        byte[] body = this.RouteIndex == 0 ? response.Body : new XonAnswerFrame( response.Body ).Payload;
                        return XRioMsg.AsRawSegs( body );
                    } else {
                        // __TODO 0. !!! check performance with replacement !!!
                        // also ensure that new value is not in the same range as the old.
                        //this.RouteSeq = (UInt16)( new Random().Next( 0x10000 ) );
                        Debug.WriteLine( this.MacAddress + " response timed out." );
                        throw new XDeliveryFailureException( this.MacAddress + " response timed out" );
                    }
                }
                return result;
            }
        }
        public UInt16 RouteIndex = 0;
        public UInt16 RouteTxSeq = (UInt16)( new Random().Next( 0x10000 ) );
        // __TODO__ 0. sort out rx sequencing, looks like currently no checks!
        public UInt16 RouteRxSeq = 0;
        public List<XHProxy> RouteProviders = new List<XHProxy>();

        // for development only - features of this nature normally implemented by application.
        public override string ToString () {
            var s = new StringBuilder();
            //s.Append( name );
            s.Append( "  " );
            s.Append( IpAddress.ToString() );
            s.Append( "  /  " );
            s.Append( MacAddress.ToString() );
            s.Append( "  /  " );
            s.Append( Info );
            return s.ToString();
        }
        public string FullAddressString () {
            if ( ( this.Client != null ) && ( this.Client.Connected ) ) {
                var ep = (IPEndPoint)Client.Client.RemoteEndPoint;
                return MacAddress.ToConfigString() + ", at " + ep.Address.ToString() + ":" + ep.Port.ToString();
            } else return MacAddress.ToConfigString();
        }

        public XHProxy ( byte[] aMacAddress, IPAddress anIpAddress, byte[] anInfo ) {
            this.MacAddress = new Xtend.MacAddress( aMacAddress );
            this.IpAddress = anIpAddress;
            this.Info = BinEnc.Get.GetString( anInfo );
        }
        protected virtual void Dispose ( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
            Detach();
            if ( MacAddress != null ) {
                MacAddress.Dispose();
                MacAddress = null;
            }
            if ( Cards != null ) {
                foreach ( var i in Cards )
                    i.Dispose();
                Cards.Clear();
                Cards = null;
            }
            if ( responseAwaitingPackets != null ) {
                lock ( responseAwaitingPackets ) {
                    foreach ( var i in responseAwaitingPackets )
                        i.Dispose();
                    responseAwaitingPackets.Clear();
                }
                responseAwaitingPackets = null;
            }
            if ( responsePackets != null ) {
                lock ( responsePackets ) {
                    foreach ( var i in responsePackets )
                        i.Dispose();
                    responsePackets.Clear();
                }
                responsePackets = null;
            }
            if ( incomingMessagePackets != null ) {
                foreach ( var i in incomingMessagePackets )
                    i.Dispose();
                incomingMessagePackets.Clear();
                incomingMessagePackets = null;
            }
            prebuf = header = null;
            SendAccessLock = null;
        }
        ~XHProxy () {
            Dispose( false );
        }
        public void Dispose () {
            Dispose( true );
        }
    }

}


