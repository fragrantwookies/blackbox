﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Runtime.InteropServices;
using System.Threading;
using XPointAudiovisual.Core;
using XtendExtensionMethods;
#endregion

namespace XPointAudiovisual.Xtend {

    #region Utility items
    public class MacAddress : IDisposable {
        public static byte[] Broadcast = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
        public byte[] Bytes = new byte[ 6 ];
        public MacAddress( byte[] anAddressArray ) {
            anAddressArray.CopyTo( Bytes, 0 );
        }
        public bool Equals( MacAddress aMacAddress ) {
            for ( int i = 0; i < 6; ++i )
                if ( Bytes[ i ] != aMacAddress.Bytes[ i ] )
                    return false;
            return true;
        }
        public override string ToString() {
            List<string> macFields = new List<string>();
            foreach ( byte b in Bytes )
                macFields.Add( b.ToString( "X2" ) );
            return String.Join( ":", macFields.ToArray() );
        }
        public string ToConfigString() {
            if ( Bytes == null ) return "000000-000000";
            List<string> macFields = new List<string>();
            foreach ( byte b in Bytes )
                macFields.Add( b.ToString( "X2" ) );
            var val = String.Join( "", macFields.ToArray() ).ToLower();
            return val.Substring( 0, 6 ) + "-" + val.Substring( 6 );
        }
        public string ToCanonicalString() {
            return ToString().Replace( ":", "" ).ToUpper();
        }
        public MacAddress( string anAddressString ) {
            string[] parts = anAddressString.Split( new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries );
            for ( int i = 0; i < 6; ++i )
                Bytes[ i ] = byte.Parse( parts[ i ], System.Globalization.NumberStyles.AllowHexSpecifier );
        }
        protected virtual void Dispose( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
            Bytes = null;
        }
        ~MacAddress() {
            Dispose( false );
        }
        public void Dispose() {
            Dispose( true );
        }
    }
    public static class XKeys {

        // keys to prevent spurious packets from being accepted as real.
        // received packets must contain a known key at a specific fixed location, else discarded.

        public static Guid XPAV = new Guid( "C1D0844D-6FB7-42A5-9927-7080539E2976" );
        // 0x4D,0x84,0xD0,0xC1,0xB7,0x6F,0xA5,0x42,0x99,0x27,0x70,0x80,0x53,0x9E,0x29,0x76
        public static Guid Ethele = new Guid( "93232F02-13D6-48C8-AEB9-9B75747F02D1" );
        // 0x02,0x2F,0x23,0x93,0xD6,0x13,0xC8,0x48,0xAE,0xB9,0x9B,0x75,0x74,0x7F,0x02,0xD1

        // "XPAVSec" and "EtheleSec" GUIDs with additional safety by timestamp and encryption.
        public static Guid XPAVSec = new Guid( "FD02CB40-79AC-4356-BD2D-B9A95B3D617C" );
        // 0x40,0xCB,0x02,0xFD,0xAC,0x79,0x56,0x43,0xBD,0x2D,0xB9,0xA9,0x5B,0x3D,0x61,0x7C
        public static Guid EtheleSec = new Guid( "79B352B9-45DB-4271-98C7-E30E325394EE" );
        // 0xB9,0x52,0xB3,0x79,0xDB,0x45,0x71,0x42,0x98,0xC7,0xE3,0x0E,0x32,0x53,0x94,0xEE

        // low-level no-parameter commands that do not follow Xtend packet formats.
        // used for simple UDP-based system commands to avoid complex protocol handling.
        // therefore does not require any sophisticated protocol handlers to be running,
        //   only simple comparison of bytes with basic following fields (non-standardized).

        // "Find" from server broadcasts an unaddressed request to all remotes to present selves.
        // "Present" from remote informs broadcasts to master of presence and basic info.
        // "Register" from server to specific remote indicates remote is now known to server.

        //public static Guid Find = new Guid( "727777A5-FCF8-4C51-A1D9-24B8930CFDC6" );
        //// 0xA5,0x77,0x77,0x72,0xF8,0xFC,0x51,0x4C,0xA1,0xD9,0x24,0xB8,0x93,0x0C,0xFD,0xC6
        //public static Guid Present = new Guid( "1D0E925C-74AC-4747-9A2C-9E582ECD01A1" );
        //// 0x5C,0x92,0x0E,0x1D,0xAC,0x74,0x47,0x47,0x9A,0x2C,0x9E,0x58,0x2E,0xCD,0x01,0xA1
        //public static Guid Registered = new Guid( "A4407A16-68F1-4815-A63F-D12C38C5FF62" );
        //// 0x16,0x7A,0x40,0xA4,0xF1,0x68,0x15,0x48,0xA6,0x3F,0xD1,0x2C,0x38,0xC5,0xFF,0x62

        // "Update" tells remote device to begin code update cycle (erase, reboot, BOOTP, TFTP)

        public static Guid Update = new Guid( "CC509E61-85AF-4669-95A6-4296B7AFDBFE" );
        // 0x61,0x9E,0x50,0xCC,0xAF,0x85,0x69,0x46,0x95,0xA6,0x42,0x96,0xB7,0xAF,0xDB,0xFE

        // "Secure" sends a secure shared-key value to the device as a following 16-byte GUID argument.
        // should not be used on an open network, only on direct Ether-to-Ether link to avoid being exposed.
        // this should only be used during initial setup as a configuration function.
        // following context value is thereafter used as the salt for any secure comms implementation.

        public static Guid Secure = new Guid( "A71B74B3-6419-4829-AF00-500668AC54CB" );
        // 0xB3,0x74,0x1B,0xA7,0x19,0x64,0x29,0x48,0xAF,0x00,0x50,0x06,0x68,0xAC,0x54,0xCB
    }
    #endregion

    // *** packet objects and associated frame definitions for various (mostly IP) protocols. ***

    // every packet def contains a private frame (class) that surfaces the fields to be packed for network transmission.
    // (the type and instance are private to prevent external access, the fields are public for simple reflection.)
    // data is stored in the private frames in the local endianness for each field of the frame.
    // network byte-order is achieved by having the data pass through the network serializer reflector.
    // this avoids having to write code to handle, and manually maintain exact offsets into, an explicit byte array.

    // __NOTE__ IP-based messages are always big-endian, but XTEND is always little-endian.
    // As a rule, Xtend devices only ever see XTEND messages at the application level.

    #region BOOTP
    public enum BootpOperation { None, Request, Reply }
    public enum BootpHardwareType { None, Ethernet }

    public class BootpPacket {
        [StructLayout( LayoutKind.Sequential, Pack = 1 )]
        private class BootpFrame {
            public byte Operation;                            // 1 = request, 2 = reply
            public byte HardwareType = (byte)BootpHardwareType.Ethernet; // 1 = Ethernet
            public byte HardwareAddressLength = 6; // Ethernet MAC
            public byte HopCount;                            // used by gateways for cross-gateway booting.
            public uint TransactionID;                      // IP port.
            public ushort BootingSince;                     // time in seconds since client seg started booting.
            public ushort Flags;                                // BOOTP flags
            public uint ClientKnownIPAddress;          // client's IP IpAddress, if it knows it.
            public uint ClientAssignedIPAddress;       // client's IP IpAddress, as assigned by BOOTP instance.
            public uint ServerIPAddress;                   // BOOTP instance's (here) IP IpAddress.
            public uint GatewayIPAddress;                // if booting cross-gateway

            [MarshalAs( UnmanagedType.ByValArray, SizeConst = 16 )]
            public byte[] ClientHardwareAddress = new byte[ 16 ];      // client's MAC address
            [MarshalAs( UnmanagedType.ByValArray, SizeConst = 64 )]
            public byte[] ServerName = new byte[ 64 ];                      // name or nickname of instance to handle BOOTP request.
            [MarshalAs( UnmanagedType.ByValArray, SizeConst = 128 )]
            public byte[] FileName = new byte[ 128 ];                          // name of boot file to be loaded via TFTP.
            [MarshalAs( UnmanagedType.ByValArray, SizeConst = 64 )]
            public byte[] VendorData = new byte[ 64 ];                        // unused for BOOTP
        }
        private BootpFrame frame;

        // surface the frame's values in a more safe and friendly manner.
        public BootpOperation Operation { get { return (BootpOperation)frame.Operation; } set { frame.Operation = (byte)value; } }
        public BootpHardwareType HardwareType { get { return (BootpHardwareType)frame.HardwareType; } }
        public byte HardwareAddressLength { get { return frame.HardwareAddressLength; } }
        public byte HopCount { get { return frame.HopCount; } }
        public uint TransactionID { get { return frame.TransactionID; } }
        public ushort BootingSince { get { return frame.BootingSince; } }
        public ushort Flags { get { return frame.Flags; } }
        public uint ClientKnownIPAddress { get { return frame.ClientKnownIPAddress; } }
        public uint ClientAssignedIPAddress { get { return frame.ClientAssignedIPAddress; } set { frame.ClientAssignedIPAddress = value; } }
        public uint ServerIPAddress { get { return frame.ServerIPAddress; } set { frame.ServerIPAddress = value; } }
        public uint GatewayIPAddress { get { return frame.GatewayIPAddress; } }
        public byte[] ClientHardwareAddress {
            get { byte[] cha = new byte[ 6 ]; Array.Copy( frame.ClientHardwareAddress, cha, 6 ); return cha; }
        }
        public string ServerName { get { return ""; } }
        public string FileName {
            get { return BinEnc.Get.GetString( frame.FileName, 0, frame.FileName.Length ); }
            set {
                byte[] fn = BinEnc.Get.GetBytes( value );
                Array.ConstrainedCopy( fn, 0, frame.FileName, 0, fn.Length );
                frame.FileName[ fn.Length ] = 0;
            }
        }

        // constructors for packets used by applications, not for network bytes conversions.
        // this avoids code in which the flow of network to/from object is obscured.
        public BootpPacket() { }
        public BootpPacket( BootpOperation anOperation, IPAddress aServerAddress ) {
            frame = new BootpFrame();
            frame.Operation = (byte)anOperation;
            frame.ServerIPAddress = BitConverter.ToUInt32( aServerAddress.GetAddressBytes(), 0 );
        }
        // methods for converting between packets and network bytes.
        public static BootpPacket FromNetworkBytes( byte[] aDataArray ) {
            BootpPacket pkt = new BootpPacket();
            pkt.frame = (BootpFrame)NetworkSerializer.FromNetworkBytes( typeof( BootpFrame ), aDataArray );
            return pkt;
        }
        public byte[] ToNetworkBytes() {
            return NetworkSerializer.ToNetworkBytes( typeof( BootpFrame ), frame );
        }
    }
    #endregion

    #region TFTP

    public enum TftpOperation {
        None, RRQ, WRQ, Data, Ack, Error, OptionAck
    }
    public enum TftpErrors {
        Undefined, FileNotFound, AccessViolation, AllocError,
        IllegalOperation, UnknownTID, FileExists, InvalidUser
    }

    // this class exists largely for two reasons - to define a subclass-shared referencing type,
    //   and to encapsulate the mapping from the TFTP packet operation to a packet/frame type.
    public class TftpPacket {
        public TftpPacket() { }
        readonly string[] requestMode = { "netascii", "octet", "mail" };
        public virtual TftpOperation Operation { get { return TftpOperation.None; } }
        public static TftpPacket FromNetworkBytes( byte[] aDataArray ) {
            int operation = ( (int)aDataArray[ 0 ] << 8 ) + (int)aDataArray[ 1 ];
            switch ( (TftpOperation)operation ) {
                case TftpOperation.RRQ: return TftpReadRequestPacket.FromNetworkBytes( aDataArray );
                case TftpOperation.Data: return TftpDataPacket.FromNetworkBytes( aDataArray );
                case TftpOperation.Ack: return TftpAckPacket.FromNetworkBytes( aDataArray );
                case TftpOperation.Error: return TftpErrorPacket.FromNetworkBytes( aDataArray );
                // __TODO__ 9. currently no support for client write requests, implement later if needed.
                // __NOTE__ write requests are never seen in the real world when using TFTP for code updates.
                //case TftpOperation.None: 
                //case TftpOperation.WRQ:
                default:
                    return null;
            }
        }
        public byte[] ToNetworkBytes( object aFrame ) {
            return NetworkSerializer.ToNetworkBytes( aFrame.GetType(), aFrame );
        }
    }

    public class TftpReadRequestPacket : TftpPacket {
        private ReadRequestFrame frame;
        [StructLayout( LayoutKind.Sequential, Pack = 1 )]
        private class ReadRequestFrame {
            public ushort Operation;
            [MarshalAs( UnmanagedType.LPStr )]
            public string FileName;
            [MarshalAs( UnmanagedType.LPStr )]
            public string TransferMode;
        }
        public override TftpOperation Operation { get { return (TftpOperation)frame.Operation; } }
        public string FileName { get { return frame.FileName; } }
        public string TransferMode { get { return frame.TransferMode; } }

        // methods for converting between packets and network bytes.
        // have to manually marshal because the automarshalling expects strings as pointers!
        public static new TftpReadRequestPacket FromNetworkBytes( byte[] aDataArray ) {
            TftpReadRequestPacket pkt = new TftpReadRequestPacket();
            pkt.frame = new ReadRequestFrame();
            Array.Reverse( aDataArray, 0, 2 );
            pkt.frame.Operation = BitConverter.ToUInt16( aDataArray, 0 );
            char[] delimiter = "\0".ToCharArray();
            string[] strData = BinEnc.Get.GetString( aDataArray, 2, aDataArray.Length - 2 )
                .Split( delimiter, StringSplitOptions.RemoveEmptyEntries );
            pkt.frame.FileName = strData[ 0 ];
            pkt.frame.TransferMode = strData[ 1 ].ToLower();
            return pkt;
        }
        public byte[] ToNetworkBytes() {
            return null;
        }
    }

    public class TftpAckPacket : TftpPacket {
        private AckFrame frame;
        [StructLayout( LayoutKind.Sequential, Pack = 1 )]
        private class AckFrame {
            public ushort Operation;
            public ushort BlockNumber;
        }
        public override TftpOperation Operation { get { return (TftpOperation)frame.Operation; } }
        public ushort BlockNumber { get { return frame.BlockNumber; } }

        // methods for converting between packets and network bytes.
        public static new TftpAckPacket FromNetworkBytes( byte[] aDataArray ) {
            TftpAckPacket pkt = new TftpAckPacket();
            pkt.frame = (AckFrame)NetworkSerializer.FromNetworkBytes( typeof( AckFrame ), aDataArray );
            return pkt;
        }
        public byte[] ToNetworkBytes() { return base.ToNetworkBytes( frame ); }
    }

    public class TftpErrorPacket : TftpPacket {
        private ErrorFrame frame;
        [StructLayout( LayoutKind.Sequential, Pack = 1 )]
        private class ErrorFrame {
            public ushort Operation;
            public ushort ErrorCode;
            [MarshalAs( UnmanagedType.LPStr )]
            public string ErrorMessage;
        }
        public override TftpOperation Operation { get { return (TftpOperation)frame.Operation; } }
        public TftpErrors ErrorCode { get { return (TftpErrors)frame.ErrorCode; } }
        public string ErrorMessage { get { return frame.ErrorMessage; } }

        // methods for converting between packets and network bytes.
        public static new TftpErrorPacket FromNetworkBytes( byte[] aDataArray ) {
            TftpErrorPacket pkt = new TftpErrorPacket();
            pkt.frame = (ErrorFrame)NetworkSerializer.FromNetworkBytes( typeof( ErrorFrame ), aDataArray );
            return pkt;
        }
        public byte[] ToNetworkBytes() { return base.ToNetworkBytes( frame ); }
    }

    // __TODO__ 9. add special handling for an incoming data array (when implementing WRQ support).
    // __NOTE__ write requests are never seen in the real world when using TFTP for code updates.
    // the marshaller doesn't know how to calculate the size of a variable length unmanaged array.
    // going out (to-bytes) is not a problem, because the local frame field contains the necessary info.

    public class TftpDataPacket : TftpPacket {
        private DataFrame frame;
        [StructLayout( LayoutKind.Sequential, Pack = 1 )]
        private class DataFrame {
            public ushort Operation;
            public ushort BlockNumber;
            public byte[] Data;
        }
        public override TftpOperation Operation { get { return (TftpOperation)frame.Operation; } }
        public TftpErrors BlockNumber { get { return (TftpErrors)frame.BlockNumber; } }
        public byte[] Data { get { return frame.Data; } }

        // constructors for packets used by applications, not for network bytes conversions.
        // this avoids code in which the flow of network to/from object is obscured.
        public TftpDataPacket() { }
        public TftpDataPacket( int aBlockNumber, byte[] aDataArray ) {
            frame = new DataFrame();
            frame.Operation = (ushort)TftpOperation.Data;
            frame.BlockNumber = (ushort)aBlockNumber;
            frame.Data = aDataArray;
        }
        private int FrameDataSize() {
            return 2 + 2 + frame.Data.Length;
        }
        // methods for converting between packets and network bytes.
        public static new TftpDataPacket FromNetworkBytes( byte[] aDataArray ) {
            TftpDataPacket pkt = new TftpDataPacket();
            pkt.frame = (DataFrame)NetworkSerializer.FromNetworkBytes( typeof( DataFrame ), aDataArray );
            return pkt;
        }
        public byte[] ToNetworkBytes() {
            var data = new byte[ FrameDataSize() ];
            var op = IPAddress.HostToNetworkOrder( (short)frame.Operation );
            var bn = IPAddress.HostToNetworkOrder( (short)frame.BlockNumber );
            Array.Copy( BitConverter.GetBytes( op ), 0, data, 0, 2 );
            Array.Copy( BitConverter.GetBytes( bn ), 0, data, 2, 2 );
            Array.Copy( frame.Data, 0, data, 4, frame.Data.Length );
            return data;
        }
    }
    #endregion

    #region Xtend Messaging Description
    // body is always expected to be in standard XTEND format, incl. layout, checksums, stuffing etc.
    // XTEND encoding always prefixes the body data with a 4-byte data-length field.
    // GUID header is for source check and additional synchronization.

    // various options available: datagram-open, datagram-secure, streamed-open, streamed-secure.
    // also dedicated selectors for exclusive link channels eg. XPAV/XPAVSec, Ethele/EtheleSec etc.

    // destination delivery is optionally datagram (UDP) or streamed (TCP), both using port #7531
    // in some cases (mostly discovery), message contains MAC address of target for extra checking.
    // relies on code in the client device monitoring this port number for a packet with appropriate shape and data.
    // arriving frames that do not match a standard key are always discarded, both open and secure.

    #region Xtend Datagram
    // intention is to be able to always reach and discover all Xtend devices by broadcast.
    // __NOTE__ *no* support for clean communications: no guaranteed delivery, ordered, at-most-once etc.
    // this link can only be used for stateless, unacknowledged, possibly multiply-repeated messages.
    #endregion
    #region Xtend Streamed
    #endregion

    // message protection is open (minimal) or secure (maximal) protection.
    // open messages can only be used with devices having no secure key (password) defined.
    // secure messages can only be used with devices having a defined secure key.
    // these two options are always completely mutually exclusive, 
    //     as otherwise a secure device could easily be subverted by an insecure message.
    // secure adds timestamp and encrypts open messages, otherwise exactly the same format.
    // messages never bother with a digest, always encrypted in secure mode.

    #region Xtend Open
    #endregion
    #region Xtend Secure
    // packets with XPAVSec and EtheleSec key GUIDs, as for XPAV and Ethele GUIDs,
    // but the inner portion of the frame is also always fully encrypted.
    // this includes the standard UID and timestamp, preventing spoofing and replays.
    #endregion
    #endregion

    #region Xtend basic link frame definition

    // this is the byte-array implementation for an XLP frame.

    public enum XtendProtocolSelect {
        // separating initiate and respond is for efficiency.
        // otherwise we have to dig too deep into a received packet before dispatching it.
        XtendInitiate = 1,
        XtendRespond
    };

    public class XLinkFrame : IDisposable {
        public byte[] RawBytes;
        public XLinkFrame( int aSize ) {
            RawBytes = new byte[ aSize ];
        }
        public XLinkFrame( byte[] aDataArray ) {
            RawBytes = aDataArray;
            // various packet checks before returning ostensibly OK results.
            // first check that header is valid.
            if ( Header != 0xC3A53A5C )
                throw new XBadPacketException( "invalid frame header" );
            // frame length header is length of frame less 16 bytes.
            // where 16 = length of Header + Length + Checksum + Footer.
            // check that received packet length is same as frame length expects.            
            if ( Length != ( RawBytes.Length - 16 ) )
                throw new XBadPacketException( "invalid frame length" );
            // check that key is a well-known one.
            var guidKey = new Guid( Key );
            if ( !XKeys.Ethele.Equals( guidKey )
                && !XKeys.EtheleSec.Equals( guidKey )
                && !XKeys.XPAV.Equals( guidKey )
                && !XKeys.XPAVSec.Equals( guidKey ) )
                throw new XBadPacketException( "invalid frame key" );
            // check that it is a supported protocol.
            if ( ( Protocol != XtendProtocolSelect.XtendInitiate )
                && ( Protocol != XtendProtocolSelect.XtendRespond ) )
                throw new XBadPacketException( "invalid frame protocol" );
            // check that the frame footer is where expected, and value is correct.
            if ( Footer != ~0xC3A53A5C )
                throw new XBadPacketException( "invalid frame footer" );
            // check that checksum passes.
            // __TODO__ 0. currently not implemented in Stellaris!
            //ushort[] crc = IbmCrc16.CalcThree( RawBytes.Slice( 4, RawBytes.Length - 12 ) );
            //if (( crc[1] != BitConverter.ToUInt16( RawBytes, RawBytes.Length - 8 ))
            //    || ( crc[2] != BitConverter.ToUInt16( RawBytes, RawBytes.Length - 6 )))
            //    throw new XBadPacketException( "invalid frame footer" );

            // *** decryption for inner data if enabled. *** 
            if ( Secure ) {
                var decrypt = RawBytes.Slice( 24, RawBytes.Length - 32 );
                // __TODO__ 0. perform the decryption.
                Array.Copy( decrypt, 0, RawBytes, 24, decrypt.Length );
            }
            // check that stuffed body has length less or equal to total - overheads.
            var actualBodyLen = BitConverter.ToInt32( RawBytes, 42 );
            if ( actualBodyLen > ( RawBytes.Length - 54 ) )
                throw new XBadPacketException( "invalid frame body length" );
        }
        protected virtual void Dispose( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
            RawBytes = null;
        }
        ~XLinkFrame() {
            Dispose( false );
        }
        public void Dispose() {
            Dispose( true );
        }
        // fixed value used for partial synchronization on the start of frame.
        public uint Header {
            get { return BitConverter.ToUInt32( RawBytes, 0 ); }
            set { Array.Copy( BitConverter.GetBytes( value ), 0, RawBytes, 0, 4 ); }
        }

        // data count *following* the length, does not include header's, or footer's, or own length.
        // includes length of any random filler bytes included for encryption purposes.
        public uint Length {
            get { return BitConverter.ToUInt32( RawBytes, 4 ); }
            set { Array.Copy( BitConverter.GetBytes( value ), 0, RawBytes, 4, 4 ); }
        }

        // distributor/manufacturer GUID, ignored if no match.
        public byte[] Key {
            get { return RawBytes.Slice( 8, 16 ); }
            set { Array.Copy( value, 0, RawBytes, 8, 16 ); }
        }

        // *** if secure, then the frame is encrypted from here onwards ***
        // a unique 8-byte random code to help identify and bind responses to messages.
        // also helps prevent replay attacks on responses, and salts encryption.
        public byte[] UniqueID {
            get { return RawBytes.Slice( 24, 8 ); }
            set { Array.Copy( value, 0, RawBytes, 24, 8 ); }
        }

        // 8-byte timestamp in 100-nansecond ticks since start of epoch.
        // if encrypted, timestamp prevents replay attacks, also salts encryption.
        // otherwise helps with debug and sequencing, keeps format same as secure packets.
        public long Timestamp {
            get { return BitConverter.ToInt64( RawBytes, 32 ); }
            set { Array.Copy( BitConverter.GetBytes( value ), 0, RawBytes, 32, 8 ); }
        }

        // selector for driver/handler to be used on body data.
        public XtendProtocolSelect Protocol {
            get { return (XtendProtocolSelect)BitConverter.ToUInt16( RawBytes, 40 ); }
            set { Array.Copy( BitConverter.GetBytes( (int)value ), 0, RawBytes, 40, 2 ); }
        }

        // data transferred by application, will have its own public format.
        public byte[] Body {
            get { return RawBytes.Slice( 46, BitConverter.ToInt32( RawBytes, 42 ) ); }
            set {
                Array.Copy( BitConverter.GetBytes( value.Length ), 0, RawBytes, 42, 4 );
                Array.Copy( value, 0, RawBytes, 46, value.Length );
            }
        }

        // filler must have extra unused bytes in to make up the 16-byte encryption block.
        // this must be filled with random numbers to help prevent attacks based on known zero data.
        // (note that no length field is appended for the filler, it is simply a series of nonsense bytes.)
        public byte[] Filler {
            get { return null; }
            set {
                XLinkMarker.Randomizer.NextBytes( value );
                Array.Copy( value, 0, RawBytes, RawBytes.Length - 8 - value.Length, value.Length );
            }
        }

        // fixed value used for desynchronization with the end of frame.
        // note that this is not included in the encrypted portion of the frame!
        // otherwise the receiver would have to decrypt everything to reliably desynch.
        public uint Checksum {
            get { return BitConverter.ToUInt32( RawBytes, RawBytes.Length - 8 ); }
            set { Array.Copy( BitConverter.GetBytes( value ), 0, RawBytes, RawBytes.Length - 8, 4 ); }
        }

        // act of writing footer performs encryption and checksum calculations!
        public uint Footer {
            get { return BitConverter.ToUInt32( RawBytes, RawBytes.Length - 4 ); }
            set {
                if ( Secure ) {
                    var encrypt = RawBytes.Slice( 24, RawBytes.Length - 32 );
                    // __TODO__ 0. perform the encryption.
                    Array.Copy( encrypt, 0, RawBytes, 24, encrypt.Length );
                }
                // calculate checksum and write to buffer.
                ushort[] crc = IbmCrc16.CalcThree( RawBytes.Slice( 4, RawBytes.Length - 12 ) );
                Array.Copy( BitConverter.GetBytes( crc[ 1 ] ), 0, RawBytes, RawBytes.Length - 8, 2 );
                Array.Copy( BitConverter.GetBytes( crc[ 2 ] ), 0, RawBytes, RawBytes.Length - 6, 2 );
                // now write the footer value into the array.
                Array.Copy( BitConverter.GetBytes( value ), 0, RawBytes, RawBytes.Length - 4, 4 );
            }
        }

        public bool Secure {
            get {
                var guid = new Guid( Key );
                return guid.Equals( XKeys.XPAVSec ) || guid.Equals( XKeys.EtheleSec );
            }
        }
    }

    // __TODO__ 0. move to individual hub proxies, so we can properly handle *incoming* timestamps.
    public sealed class XLinkMarker {
        public static Random Randomizer = new Random();
        private static object tickLock = new object();
        private static long prevTicksOut = 0;
        public static XLinkMarker Instance { get { return Nested.Instance; } }
        private class Nested {
            static Nested() { }
            public static readonly XLinkMarker Instance = new XLinkMarker();
        }
        static public long GetNextTimestampOut() {
            long currTicksOut;
            lock ( tickLock ) {
                currTicksOut = DateTime.Now.Ticks;
                if ( currTicksOut <= prevTicksOut )
                    currTicksOut = ++prevTicksOut;
                prevTicksOut = currTicksOut;
            }
            return currTicksOut;
        }
        static public byte[] GetNextUniqueID() {
            var ret = new byte[ 8 ];
            Randomizer.NextBytes( ret );
            return ret;
        }
        private XLinkMarker() { }
    }
    #endregion

    #region XLP (Xtend Link Protocol)

    // XLP packets are the wrappers for RIO messages to be packaged inside them.
    // they are essentially a thin friendly-facade over an XLinkFrame.

    public class XLinkPacket : IDisposable {
        public XLinkFrame Frame;
        public bool RequiresResponse = false;
        public AutoResetEvent Signal = new AutoResetEvent( false );
        public ulong UniqueID;
        public UInt16 RouteIndex = 0;
        public UInt16 RouteSeq = 0;

        // methods for converting between packets and network bytes.
        public static XLinkPacket FromNetworkBytes( byte[] aDataArray ) {
            XLinkPacket pkt = new XLinkPacket();
            var frame = pkt.Frame = new XLinkFrame( aDataArray );
            pkt.UniqueID = BitConverter.ToUInt64( pkt.Frame.UniqueID, 0 );
            return pkt;
        }
        public byte[] ToNetworkBytes() { return Frame.RawBytes; }
        public byte[] Body { get { return Frame.Body; } }

        // constructors for packets used by applications, not for network bytes conversions.
        // this avoids code in which the flow of network to/from object is obscured.
        private void __Init( Guid aGuid, byte[] aDataArray ) {
            var secureSize = 8 + 8 + 2 + 4 + aDataArray.Length;
            var fillerSize = ( 16 - secureSize % 16 ) % 16;
            secureSize += fillerSize;
            var innerSize = 16 + secureSize;
            Frame = new XLinkFrame( 4 + 4 + innerSize + 4 + 4 );
            Frame.Header = 0xC3A53A5C;
            Frame.Length = (uint)( innerSize );
            Frame.Key = aGuid.ToByteArray();
            var uid = XLinkMarker.GetNextUniqueID();
            Frame.UniqueID = uid;
            this.UniqueID = BitConverter.ToUInt64( uid, 0 );
            Frame.Timestamp = XLinkMarker.GetNextTimestampOut();
            Frame.Protocol = XtendProtocolSelect.XtendInitiate;
            Frame.Body = ( aDataArray == null ? new byte[] { } : aDataArray );
            Frame.Filler = new byte[ fillerSize ];
            // ! active assignment - writing to footer encrypts data and calculates checksum !
            Frame.Footer = ~0xC3A53A5C;
            // note that the encryption has now been done, checksum calculated.
        }
        private XLinkPacket() { }
        // __TODO__ 5. with List<byte[]> data for multi-packets
        public XLinkPacket ( Guid aGuid, byte[] aDataArray ) {
            __Init( aGuid, aDataArray );
        }
        public XLinkPacket ( Guid aGuid, byte[] aDataArray, UInt16 aRouteIndex, UInt16 aRouteSeq ) {
            __Init( aGuid, aDataArray );
            RouteIndex = aRouteIndex;
            RouteSeq = aRouteSeq;
        }
        protected virtual void Dispose ( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
            if ( Signal != null ) {
                Signal.Dispose();
                Signal = null;
            }
            if ( Frame != null ) {
                Frame.Dispose();
                Frame = null;
            }
        }
        ~XLinkPacket() {
            Dispose( false );
        }
        public void Dispose() {
            Dispose( true );
        }
    }
    #endregion

    #region Xtend Remote IO operational messages (XRM) protocol

    // XRM is the message format describing communications with RIO devices.
    // XRM assumes a hub/card/element physical layout, and formats messages accordingly.
    // XRM records are packaged in the body of an XLP frame, and are in the following format:
    //      size, card, element, map, mode, field, args

    public class XRioMsg {
        private const int recOfs = 2;
        private const int recLenSize = 2;
        public byte[] AsBytes;
        public byte Card, Element, Map, Mode, Field;
        public byte[] Args;
        // check if it matches expected message.
        public bool Matches( byte aCard, byte anElement, EXMapType aMap, byte aField ) {
            return ( Card == aCard )
                && ( Element == anElement )
                && ( Map == (byte)aMap )
                && ( Field == aField );
        }
        // split the frame body into distinct byte arrays mapped to XRM segments.
        public static List<byte[]> AsRawSegs( byte[] aDataArray ) {
            var result = new List<byte[]>();
            var body = aDataArray;
            var totalLength = body.Length;
            var summedLength = 0;
            while ( summedLength < totalLength ) {
                var opLength = BitConverter.ToUInt16( body, summedLength );
                summedLength += 2;
                var nextItem = body.Slice( summedLength, opLength );
                result.Add( nextItem );
                summedLength += opLength;
            }
            return result;
        }
        public static List<XRioMsg> AsMsgs( List<byte[]> aRawSegList ) {
            var result = new List<XRioMsg>();
            foreach ( var rs in aRawSegList )
                result.Add( new XRioMsg( rs ) );
            return result;
        }
        public static List<XRioMsg> AsMsgs( byte[] aRawBody ) {
            var segs = AsRawSegs( aRawBody );
            return AsMsgs( segs );
        }
        private void __Build( int aCard, int anElement, int aMap, int aMode, int aField, byte[] anArgsArray ) {
            Card = (byte)aCard;
            Element = (byte)anElement;
            Map = (byte)aMap;
            Mode = (byte)aMode;
            Field = (byte)aField;
            Args = anArgsArray;
            AsBytes = BytesList.CountedBytes( new List<byte[]> { new byte[] { Card, Element, Map, Field, Mode }, anArgsArray } );
        }
        // for use with individual expression of all items in the record, incl. mode etc.
        public XRioMsg( int aCard, int anElement, int aMap, int aMode, int aField, byte[] anArgsArray ) {
            __Build( aCard, anElement, aMap, aMode, aField, anArgsArray );
        }
        // for use with default return of Map.Set/Get() return value.
        public XRioMsg( int aCard, int anElement, int aMap, byte[] aFldArray ) {
            int field = aFldArray[ 0 ];
            int mode = aFldArray[ 1 ];
            byte[] argsArray = aFldArray.Slice( 2, aFldArray.Length - 2 );
            __Build( aCard, anElement, aMap, mode, field, argsArray );
        }
        // for use with incoming data in byte[] format (NO length prefix!)
        public XRioMsg( byte[] aByteArray ) {
            if ( aByteArray.Length < 5 )
                throw new XBadPacketException( "malformed RIO message" );
            AsBytes = aByteArray;
            Card = aByteArray[ 0 ];
            Element = aByteArray[ 1 ];
            Map = aByteArray[ 2 ];
            Field = aByteArray[ 3 ];
            Mode = aByteArray[ 4 ];
            Args = aByteArray.Slice( 5, aByteArray.Length - 5 );
        }
    }

    #endregion


    #region IANA port definitions

    // XTEND port is not registered, but there is no existing usage known.
    // __NOTE__ to avoid conflicts with various default ephemeral port ranges:
    //    port number > [1025..5000] (BSD,Linux), < 49152+ [..WinXP], < 32767+ (IANA,Solaris,AIX,Win7..)

    public enum IPCommonPorts {
        Reserved = 0,
        Echo = 7,
        Discard = 9,
        Daytime = 13,
        QOTD = 17,
        Chargen = 19,
        FTP_Data = 20,
        FTP_Control = 21,
        SSH = 22,
        Telnet = 23,
        SMTP = 25,
        Time = 37,
        WINS_Replication = 42,
        WhoIs = 43,
        TACACS = 49,
        DNS = 53,
        DHCP_BOOTP_Server = 67,
        DHCP_BOOTP_Client = 68,
        TFTP = 69,
        Gopher = 70,
        Finger = 79,
        HTTP = 80,
        Kerberos = 88,
        MS_Exchange = 102,
        POP3 = 110,
        Ident = 113,
        Usenet = 119,
        NTP = 123,
        MS_RPC = 135,
        Netbios_NameService = 137,
        Netbios_DatagramService,
        Netbios_SessionService,
        IMAP4 = 143,
        SNMP = 161,
        SNMP_Trap = 162,
        XDMCP = 177,
        BGP = 179,
        AppleTalk_RoutingMaintenance = 201,
        AppleTalk_NameBinding,
        AppleTalk_Echo = 204,
        AppleTalk_ZoneInformation = 206,
        BGMP = 264,
        TSP = 318,
        LDAP = 389,
        HTTPS = 443,
        MS_DS = 445,
        KPassword = 464,
        SMTP_SSL = 465,
        Rexec = 512,
        Rlogin_TCP = 513,
        Who_UDP = 513,
        Shell_TCP = 514,
        Syslog_UDP = 514,
        Spooler = 515,
        RIP = 520,
        RIP_NG = 521,
        UUCP = 540,
        DHCP_Client = 546,
        DHCP_Server = 547,
        RTSP = 554,
        NNTP_SSL = 563,
        //SMTP = 587,
        MS_DCOM = 593,
        LDAP_SSL = 636,
        MS_Exchange_Routing = 691,
        VMWare_Server = 902,
        FTP_SSL_Data = 989,
        FTP_SSL_Control = 990,
        IMAP4_SSL = 993,
        POP3_SSL = 995,
        //MS_RPC = 1025,

        XTEND = 7531,
    };

    public enum IPAllocatedPorts {
        Reserved = 0,
        TcpPortServiceMultiplexer = 1,
        CompressNetManagement,
        CompressNetProcess,
        RemoteJobEntry = 5,
        Echo = 7,
        Discard = 9,
        SystatActiveUsers = 11,
        Daytime = 13,
        QuoteOfTheDay = 17,
        MessageSendProtocol,
        CharacterGenerator,
        FtpData,
        FtpControl,
        SecureShell,
        Telnet,
        SimpleMailTransferProtocol = 25,
        MsgIcp = 29,
        MsgAuthentication = 31,
        DisplaySupportProtocol = 33,
        Time = 37,
        RouteAccessProtocol,
        ResourceLocationProtocol,
        Graphics = 41,
        HostNameServer,
        WhoIs,
        MpmFlags,
        MessageProcessingProtocolReceive,
        MessageProcessingProtocolSend,
        DigitalAuditDaemon = 48,
        RemoteMailCheckingProtocol = 50,
        XnsTime = 52,
        DomainNameServer,

        BootPServer = 67,
        BootPClient = 68,

        XTEND = 7531,
        XTEND2 = 7532,
    };
    #endregion
}

