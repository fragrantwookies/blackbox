﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Timers;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using XPointAudiovisual.Core;
using XtendExtensionMethods;
#endregion

namespace XPointAudiovisual.Xtend {

    // a utility class to broadcast presence queries to the local area network.
    // it uses the XTEND port (7531) to query attached devices, expecting a specific return if they are eligible.
    // packets are keyed with preconfigured GUIDs and MAC addresses to avoid erroneous responses.

    public delegate void RunHandler ();

    public class XFinder : IDisposable {
        // __NOTE__ devices are able to register asynchronously (without polling).
        // the listener is always running, and will accept publications whenever they are received.

        private string roleTag;
        private bool bwInviterIsBusy = false, bwListenerIsBusy = false;
        private BackgroundWorker bwInviter = new BackgroundWorker();
        private BackgroundWorker bwListener = new BackgroundWorker();
        private bool isTerminating = false;
        private static bool isDisposing = false;

        // newlyPolledHubs is a list of *new* devices found in the most recent registration invite cycle.
        private static List<XHProxy> newlyPolledHubs = new List<XHProxy>();
        // newlyPolledHubs is a list of devices found in the most recent registration invite cycle.
        private static List<XHProxy> newlyRegisteredHubs = new List<XHProxy>();
        private List<Socket> xtendUdpSockets = new List<Socket>();
        //private SynchronizedCollection<Socket> xtendUdpSockets = new SynchronizedCollection<Socket>();

        // __NOTE__ these might not be current to actual instancing, as they are only updated by polling!
        //  therefore the sockets list might contain sockets for interfaces that no longer exist at any given time!
        // __NOTE__ this is a locally stored list to avoid missing changes when other threads induce XNetwork updates,
        //   which would cause a locally stored flag of changes to be cleared.
        // __TODO__ 6. keep a hash of the items stored in the list for faster testing.
        private List<UnicastIPAddressInformation> currNetworkInterfaces = new List<UnicastIPAddressInformation>();
        private bool matchesIPAddress ( IPEndPoint anEndpoint, IPAddress anIPAddress ) {
            return anEndpoint.Address.Equals( anIPAddress );
        }
        private void MakeFinderUdpSocket ( IPAddress anAddress, int aPort ) {
            try {
                var s = new Socket( AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp );
                s.ExclusiveAddressUse = false;
                s.Bind( new IPEndPoint( anAddress, aPort ) );
                s.EnableBroadcast = true;
                lock ( xtendUdpSockets )
                    xtendUdpSockets.Add( s );
                if ( Log.Events.IsTracing( Log.Level.Debug ) )
                    Log.Events.Trace( Log.Level.Debug, "<<XFinder.Listener>> added socket " + ( (IPEndPoint)s.LocalEndPoint ).ToString() );
            }
            catch {
            }
        }
        private void UpdateFinderUdpSockets () {
            // update the current network interfaces list, only update the servicing sockets list if that has changed.
            XNetworkInterfaces.Instance.Update();
            if ( !XNetworkInterfaces.Instance.Changed( currNetworkInterfaces ) )
                return;
            if ( Log.Events.IsTracing( Log.Level.Debug ) )
                Log.Events.Trace( Log.Level.Debug, "<<XFinder.Listener>> updating changes to status of network interfaces." );
            currNetworkInterfaces = XNetworkInterfaces.Instance.Clone();
            // if a network interface is no longer present, remove its associated socket from the instantiated sockets list.
            List<Socket> goneSockets;
            lock ( xtendUdpSockets )
                goneSockets = xtendUdpSockets.FindAll( s => !currNetworkInterfaces.Exists(
                  ai => matchesIPAddress( (IPEndPoint)( s.LocalEndPoint ), ai.Address ) ) );
            if ( goneSockets.Count != 0 )
                foreach ( Socket s in goneSockets ) {
                    if ( Log.Events.IsTracing( Log.Level.Debug ) )
                        Log.Events.Trace( Log.Level.Debug, "<<XFinder.Listener>> removed socket " + ( (IPEndPoint)s.LocalEndPoint ).ToString() );
                    s.Close();
                    lock ( xtendUdpSockets )
                        xtendUdpSockets.Remove( s );
                }
            // if a network interface exists that is not currently serviced with an instantiated socket, add it.
            var newInterfaces = currNetworkInterfaces.FindAll(
                 ai => !xtendUdpSockets.Exists(
                     s => matchesIPAddress( (IPEndPoint)( s.LocalEndPoint ), ai.Address ) ) );
            //if ( xtendUdpSockets.Find( s => s.LocalEndPoint.Equals( new IPEndPoint( IPAddress.Any, 7532 ) ) ) == null )
            //    MakeFinderUdpSocket( IPAddress.Any, 7532 );
            foreach ( var ai in newInterfaces ) {
                MakeFinderUdpSocket( ai.Address, (int)IPAllocatedPorts.XTEND );
                //MakeFinderUdpSocket( ai.Address, (int)IPAllocatedPorts.XTEND2 );
            }
        }
        private string AsDeviceAddressFormat ( string anAddressStr ) {
            return anAddressStr.Substring( 0, 6 ) + "-" + anAddressStr.Substring( 7 );
        }
        public struct DiscRspData {
            public IPEndPoint SrcEndPoint;
            public XRioMsg MacReport;
            public List<XRioMsg> Operations;
            public DiscRspData ( IPEndPoint aSrcEndPoint, XRioMsg aMacReport, List<XRioMsg> anOperationsList ) {
                SrcEndPoint = aSrcEndPoint;
                MacReport = aMacReport;
                Operations = anOperationsList;
            }
        }

        // prevent multiple overlapping discovery attempts.
        // this was already done, but the possibility of race conditions existed.
        // __TODO__ 0. make this work the way it should.
        // __NOTE__ that the locking is per endpoint only, not all sharing a single lock.
        private static object deviceDiscoveryLock = new object();
        private static object remoteDeviceDiscoveryLock = new object();
        private static object discoveringLocksLocker = new object();
        private static List<MacAddress> currDiscovering = new List<MacAddress>();

        enum ECardChanges { None, Fewer, More, Different };

        private static void AddRouteProvider ( XHProxy aHub, IPAddress anAddress ) {
            XHProxy provider = XIO.DetectedHubs.FirstOrDefault( x => anAddress.Equals( x.IpAddress ) && ( x.RouteIndex == 0 ) );
            if ( provider == null )
                return;
            if ( !aHub.RouteProviders.Contains( provider ) )
                aHub.RouteProviders.Add( provider );
        }

        // wait for responses from any eligible devices on the remote or local area network.
        private static void UpdateDiscovery ( DiscRspData aData, bool anIsRemotedFlag ) {
            lock ( deviceDiscoveryLock ) {
                var macAddr = new MacAddress( aData.MacReport.Args.Slice( 2, 6 ) );
                lock ( discoveringLocksLocker ) {
                    // __TODO__ 0. by locking on the MAC we have lost the ability to route through more than one network hub!
                    // incoming endpoint is not the same for different messages, must test similarity not sameness.
                    // endpoint test doesn't work for routing and drops messages, using the MAC instead.
                    // __NOTE__ routing over non-Ethernet media introduces significant latency complexities!
                    if ( currDiscovering.FirstOrDefault( ma => ma.Equals( macAddr ) ) != null )
                        return;
                    currDiscovering.Add( macAddr );
                }
                try {
                    // give any other asynchronous registration processes a chance to attach themselves to the current-discovering list.
                    // this will help prevent the user callback from being dispatched too soon, thinking these are done.
                    // it also gets duplicate-attempt threadpool threads out of the way quicker.
                    Thread.Sleep( 10 );
                    // now start the real work of registering.
                    string endpointAddress = aData.SrcEndPoint.Address.ToString();
                    // check if the frame was from a routed hub, and if so remove the routing message before standard processing.
                    // __NOTE__ this approach is a legacy fix, to avoid changing the remainder of the existing code in server and devices,
                    //    it works by restoring the multiple message format to what would have been expected without routing.
                    XRioMsg routingMsg = null;
                    if ( aData.Operations[ 0 ].Matches( 0, 0, EXMapType.Routing, (byte)XMapRouting.Routing.Address ) ) {
                        routingMsg = aData.Operations[ 0 ];
                        aData.Operations.RemoveAt( 0 );
                    }
                    if ( Log.Events.IsTracing( Log.Level.Debug ) ) {
                        string msg = routingMsg == null ? " " : " rerouting ";
                        Log.Events.Trace( Log.Level.Debug,
                            "<<XFinder.Listener.UDR>> has received a valid" + msg
                            + "Xtend registration packet from "
                            + ( anIsRemotedFlag ? "remote hub " : "network hub " )
                            + macAddr.ToConfigString() + " through " + endpointAddress );
                    }
                    XHProxy knownHub, hub;
                    // the MAC address we get will be the actual one from the hub, even if routed via another.
                    lock ( XIO.DetectedHubs )
                        knownHub = XIO.DetectedHubs.FirstOrDefault( x => macAddr.Equals( x.MacAddress ) );

                    if ( Log.Events.IsTracing( Log.Level.Debug ) ) {
                        if ( knownHub == null ) {
                            Log.Events.Trace( Log.Level.Debug,
                            "<<XFinder.Listener.UDR>> found newly detected hub " + macAddr.ToConfigString() + " through " + endpointAddress );
                        } else {
                            Log.Events.Trace( Log.Level.Debug,
                            "<<XFinder.Listener.UDR>> found previously detected hub " + knownHub.FullAddressString() );
                        }
                    }
                    // if we didn't find an existing matching hub record, now is the time to create one.
                    var appDesc = aData.Operations[ 1 ].Args;
                    if ( ( hub = knownHub ) == null )
                        hub = new XHProxy(
                            aData.MacReport.Args.Slice( 2, 6 ), aData.SrcEndPoint.Address, appDesc.Slice( 2, appDesc.Length - 2 ) );
                    // the hub (if already known) could have rebooted with a new hardware configuration.
                    // we need to *always* check the reported hardware configuration against our records, whether null or not.
                    List<XCProxy> cards = new List<XCProxy>();
                    for ( int cardOffset = 3; cardOffset < aData.Operations.Count; ++cardOffset ) {
                        var cardRep = aData.Operations[ cardOffset ];
                        var card = XCardTypeLookup.Get( hub, (XCardType)BitConverter.ToUInt16( cardRep.Args, 1 ) );
                        card.Index = cardRep.Args[ 0 ];
                        cards.Add( card );
                    }
                    // if we did not find an already-registered hub with the given MAC address, manage the new one.
                    if ( knownHub == null ) {
                        // if this is a routed hub, make sure it keeps track of that fact!
                        // remember the stated site index, and the IP address of the hub through which a route is provided.
                        // __TODO__ 0. if the site index for the device is "dynamic-unallocated", assign an index to it.
                        // __TODO__ 0. warn if there are duplicated indices
                        if ( routingMsg != null ) {
                            hub.RouteIndex = BitConverter.ToUInt16( routingMsg.Args, 0 );
                            AddRouteProvider( hub, aData.SrcEndPoint.Address );
                        }
                        // now add the reported cards to the hub proxy object.
                        hub.Cards = cards;
                        // now add this hub to the list of detected hubs, but make *sure* some other thread hasn't already.
                        // this could happen because of the interleaved handling of broadcast-invitation responses.
                        XHProxy ensureUniqueHub = null;
                        lock ( XIO.DetectedHubs ) {
                            ensureUniqueHub = XIO.DetectedHubs.FirstOrDefault( x => macAddr.Equals( x.MacAddress ) );
                            if ( ensureUniqueHub == null )
                                XIO.DetectedHubs.Add( hub );
                        }
                        // (code below is separated out to reduce lock time on DetectedHubs list.)
                        if ( ensureUniqueHub != null ) {
                            // this is a significant and unexpected error, the code should never reach here.
                            if ( Log.Events.IsTracing( Log.Level.Critical ) )
                                Log.Events.Trace( Log.Level.Critical,
                                    "<<XFinder.Listener.UDR>> hub attach race for " + ensureUniqueHub.FullAddressString() + " !" );
                            hub.Dispose();
                            hub = null;
                        } else {
                            if ( Log.Events.IsTracing( Log.Level.Debug ) )
                                Log.Events.Trace( Log.Level.Debug,
                                    "<<XFinder.Listener.UDR>> attaching newly detected hub " + hub.FullAddressString() );
                            // only attach this hub if it is not already attached.
                            hub.AttachByNeed( true );
                        }
                    } else {
                        // multiple possible route providers for a routed hub?
                        // if this is a routed hub that has been seen before, then maybe this is via an alternative router.
                        if ( routingMsg != null )
                            AddRouteProvider( hub, aData.SrcEndPoint.Address );
                        // this hub was around before, but check it hasn't changed state.
                        // it might have changed cards since it was last synchronized with.
                        // __NOTE__ this might have been contributing to errors, by losing handler associations.
                        // if for some reason the master card loses (temporarily) a record of an associated card,
                        //   it could report itself as having a different configuration.
                        //   (this could be because a card was rebooting when requested to reregister itself.)
                        // then the card list is replaced here, but previously installed drivers were not copied over.
                        ECardChanges cardChanges = ECardChanges.None;
                        if ( cards.Count != hub.Cards.Count ) {
                            cardChanges = cards.Count < hub.Cards.Count ? ECardChanges.Fewer : ECardChanges.More;
                        } else {
                            for ( int i = 0; i < hub.Cards.Count; ++i )
                                if ( hub.Cards[ i ].GetType() != cards[ i ].GetType() ) {
                                    cardChanges = ECardChanges.Different;
                                    break;
                                }
                        }
                        if ( cardChanges != 0 ) {
                            string hfad = hub.FullAddressString();
                            if ( Log.Events.IsTracing( Log.Level.Standard ) )
                                Log.Events.Trace( Log.Level.Standard,
                                "<<XFinder.Listener.UDR>> " + hfad + " has changed card configuration!" );
                            switch ( cardChanges ) {
                                case ECardChanges.Fewer:
                                    if ( Log.Events.IsTracing( Log.Level.Standard ) )
                                        Log.Events.Trace( Log.Level.Standard,
                                        "<<XFinder.Listener.UDR>> " + hfad + " has reported fewer cards!" );
                                    break;
                                case ECardChanges.More:
                                    if ( Log.Events.IsTracing( Log.Level.Standard ) )
                                        Log.Events.Trace( Log.Level.Standard,
                                        "<<XFinder.Listener.UDR>> " + hfad + " has reported more cards!" );
                                    break;
                                case ECardChanges.Different:
                                    // hub.Cards = cards;
                                    if ( Log.Events.IsTracing( Log.Level.Standard ) )
                                        Log.Events.Trace( Log.Level.Standard,
                                        "<<XFinder.Listener.UDR>> " + hfad + " has reported different cards!" );
                                    break;
                            }
                        }
                        // the network-routing hub might also have rebooted or lost connectivity meanwhile.
                        // connection is over TCP, registration is over UDP, so the fact of registering doesn't indicate connection.
                        // check the TCP connection context was and has remained active.
                        var routingHub = hub.RouteIndex == 0 ? hub : hub.RouteProviders[ 0 ];
                        if ( !aData.SrcEndPoint.Address.Equals( routingHub.IpAddress ) //
                        || ( !routingHub.Client.Connected ) ) {
                            routingHub.Detach();
                            routingHub.IpAddress = aData.SrcEndPoint.Address;
                            routingHub.Attach();
                            foreach ( var h in XIO.DetectedHubs ) {
                                if ( ( h.RouteIndex != 0 ) && h.RouteProviders.Contains( routingHub ) )
                                    h.IpAddress = routingHub.IpAddress;
                            }
                        }
                        try {
                            hub.AttachByNeed( true );
                        }
                        catch ( Exception e ) {
                            if ( Log.Events.IsTracing( Log.Level.Debug ) )
                                Log.Events.Trace( Log.Level.Debug,
                                "<<XFinder.Listener.UDR>> failed to Attach/Retach " + hub.FullAddressString() );
                            if ( Log.Events.IsTracing( Log.Level.DeepDebug ) ) {
                                Log.Events.Trace( Log.Level.DeepDebug, e.Message );
                                Log.Events.Trace( Log.Level.DeepDebug, e.StackTrace );
                            }
                        }
                    }
                    if ( hub != null ) {
                        lock ( newlyPolledHubs )
                            if ( !newlyPolledHubs.Exists( x => hub.MacAddress.Equals( x.MacAddress ) ) )
                                newlyPolledHubs.Add( hub );
                        if ( knownHub == null ) {
                            lock ( newlyRegisteredHubs )
                                if ( !newlyRegisteredHubs.Exists( x => hub.MacAddress.Equals( x.MacAddress ) ) )
                                    newlyRegisteredHubs.Add( hub );
                        }
                        // tell the hub we have seen it, must ignore future invites (until dismissed or rebooted).
                        // initializing a device is taken care of by the polling cycle that handles the devices.
                        try {
                            // socket needs to be connected before trying to enroll, so give that a chance to happen.
                            // __TODO__ 4. get rid of the timing-dependent delays!
                            Thread.Sleep( 10 );
                            hub.MasterCard.Admin.Registration.Enroll();
                        }
                        catch ( Exception e ) {
                            if ( Log.Events.IsTracing( Log.Level.Debug ) )
                                Log.Events.Trace( Log.Level.Debug,
                                "<<XFinder.Listener.UDR>> hub " + hub.FullAddressString() + " Enroll() failed with error \"" + e.Message + "\"" );
                            if ( Log.Events.IsTracing( Log.Level.DeepDebug ) ) {
                                Log.Events.Trace( Log.Level.DeepDebug, e.Message );
                                Log.Events.Trace( Log.Level.DeepDebug, e.StackTrace );
                            }
                        }
                    }
                }
                finally {
                    lock ( discoveringLocksLocker ) {
                        currDiscovering.Remove( macAddr );
                    }
                }
            }
        }

        // handle responses from any eligible devices on the fibre network.
        public static void UpdateRemoteDiscovery ( DiscRspData aData ) {
            UpdateDiscovery( aData, true );
        }
        // wait for responses from any eligible devices on the local area network.
        private void UpdateDiscoveryResponses ( DiscRspData aData ) {
            UpdateDiscovery( aData, false );
        }
        
        // wait for responses from any eligible devices on the local area network.
        private void ReadDiscoveryResponses ( List<Socket> aSocketsList ) {
            if ( ( aSocketsList == null ) || ( aSocketsList.Count == 0 ) ) {
                if ( Log.Events.IsTracing( Log.Level.DeepDebug ) )
                    Log.Events.Trace( Log.Level.DeepDebug, "<<XFinder.Listener>> has no sockets to examine for possible discovery responses." );
                return;
            }
            // check if anything has sent a response.
            // if so, add the source address to the buffer, else go back to waiting.
            //Log.Events.Trace( Log.Level.Debug, "<<XFinder.Listener>> examining sockets for possible discovery responses." );
            var buffer = new byte[ 1024 * 16 ];
            foreach ( Socket s in aSocketsList ) {
                // read Available first in a try block in case there is something wrong with the socket.
                try { if ( s.Available == 0 ) continue; }
                catch { continue; }
                // now do the proper loop through the socket data.
                while ( s.Available != 0 ) {
                    EndPoint srcEndpoint = new IPEndPoint( 0, 0 );
                    var count = s.ReceiveFrom( buffer, ref srcEndpoint );
                    if ( count == 0 )
                        continue;
                    // ignore incoming packets that are actually outgoing (ie. what we sent) !
                    // we scan through all our local sockets to ensure the source isn't one of our own sockets.
                    if ( aSocketsList.Exists( t => ( (IPEndPoint)srcEndpoint ).Address.Equals( ( (IPEndPoint)t.LocalEndPoint ).Address ) ) )
                        continue;

                    // check that the message is legitimate.
                    string endpointAddress = ( (IPEndPoint)srcEndpoint ).Address.ToString();
                    if ( Log.Events.IsTracing( Log.Level.Debug ) )
                        Log.Events.Trace( Log.Level.Debug, "<<XFinder.Listener>> has data from IP address " + endpointAddress );
                    var pkt = XLinkPacket.FromNetworkBytes( buffer.Slice( 0, count ) );
                    List<XRioMsg> ops = new List<XRioMsg>();
                    var protocolHeader = pkt.Frame.Body[ 0 ];
                    if ( ( protocolHeader & 0x80 ) == 0 ) {
                        ops.AddRange( XRioMsg.AsMsgs( pkt.Body ) );
                    } else {
                        ops.Add( new XRioMsg( 0, 0, (int)EXMapType.Routing, 0, XMapRouting.Routing.Address, null ) );
                        ops.AddRange( XRioMsg.AsMsgs( pkt.Body ) );
                    }
                    // note that expected return format is a single frame with: mac + info + cards reports.
                    // __NOTE__ there is a new possibility, that the above is preceded with a "Routing" message.
                    XRioMsg macReport = null;
                    // must be a service message from card-0, admin element, service map,
                    //   and must also be either a routing message or a mac-publish message, else ignore it.
                    if ( ops[ 0 ].Matches( 0, 0, EXMapType.Service, (byte)XMapRegistration.PublishMac.Address ) )
                        macReport = ops[ 0 ];
                    else if ( ops[ 0 ].Matches( 0, 0, EXMapType.Registration, (byte)XMapRegistration.PublishMac.Address ) )
                            macReport = ops[ 0 ];
                    else if ( ops[ 0 ].Matches( 0, 0, EXMapType.Routing, (byte)XMapRouting.Routing.Address )
                        && ops[ 1 ].Matches( 0, 0, EXMapType.Registration, (byte)XMapRegistration.PublishMac.Address ) )
                        macReport = ops[ 1 ];
                    else {
                        int x = 0;
                        int f = x;
                        continue;
                    }

                    // looks like a legitimate response, so add the address to the respondees buffer.
                    // note that this is not a distinct buffer, devices will appear for every time they respond.
                    // this is for every interface they support, and every time they are polled.
                    // __NOTE__ these are some very kludged-up message handlers here.
                    // we avoid an app-style callback interface because it would just be clumsy here.
                    XIO.Do( "update discovery responses", //
                        () => { UpdateDiscoveryResponses( new DiscRspData( (IPEndPoint)srcEndpoint, macReport, ops ) ); } );
                    Thread.Sleep( 0 );
                }
            }
            Thread.Sleep( 10 );
        }
        private DateTime lastListenerLoop = DateTime.Now;
        private DateTime lastNetworkInterfacesUpdate = DateTime.MinValue;

        // basic work function.
        private void bwListener_DoWork ( object aSender, DoWorkEventArgs aDWEAItem ) {
            aDWEAItem.Result = null;

            // open UDP sockets on all available network interfaces on this machine.
            // note that this includes AutoIP addresses *if* the local machine has one or AutoIP enabled.
            lastListenerLoop = DateTime.Now;
            // it is *critical* to wrap the listener thread in a try/catch block, because unhandled exceptions will abort the app.
            try {
                while ( true ) {
                    // the listener should just run indefinitely, and is even auto-restarted if it somehow terminates.
                    // but if cancelled, bail out now; this should only be set if terminating.
                    bool signalled = false;
                    if ( !isDisposing && !isTerminating && !bwInviter.CancellationPending )
                        signalled = AwakenListener.WaitOne( 100 );
                    // there is *meant* to be no 'else' here - it applies both for loop restart, and for awakening.
                    if ( isDisposing || isTerminating || bwListener.CancellationPending ) {
                        if ( Log.Events.IsTracing( Log.Level.Debug ) )
                            Log.Events.Trace( Log.Level.Debug, "<<XFinder.Listener>> current instance was cancelled." );
                        aDWEAItem.Cancel = true;
                        return;
                    }
                    bwListenerIsBusy = true;
                    if ( Log.Events.IsTracing( Log.Level.ExtraDeepDebug ) )
                        Log.Events.Trace( Log.Level.ExtraDeepDebug,
                        "<<XFinder.Listener>> was woken by " + ( signalled ? "raised semaphore." : "semaphore timeout." ) );
                    if ( lastNetworkInterfacesUpdate.MillisecondsSince() >= 3000 ) {
                        if ( Log.Events.IsTracing( Log.Level.DeepDebug ) )
                            Log.Events.Trace( Log.Level.DeepDebug, "<<updating XFinder UDP sockets>>" );
                        lastNetworkInterfacesUpdate = DateTime.Now;
                        UpdateFinderUdpSockets();
                    }
                    lock ( newlyPolledHubs )
                        newlyPolledHubs.Clear();
                    ReadDiscoveryResponses( xtendUdpSockets );
                    // __TODO__ 2. find a way to avoid this timing-dependent approach.
                    // it isn't quite as bad as it seems, because the listener will be around in another 100 ms anyway.
                    // registration to the newlyPolledHubs list takes a little while, and uses pooled threads,
                    //   so give those threads a chance to run before expecting results from them.
                    Thread.Sleep( 10 );
                    // once the registration processes have started running, wait around until they have completed.
                    // this might take a while because it involves possible timeouts, so continue as soon as they are done.
                    int waits = 0;
                    while ( ( ++waits < 100 ) && ( currDiscovering.Count != 0 ) )
                        Thread.Sleep( 5 );
                    // now we have either waited long enough, or they have finished.
                    if ( ( newlyPolledHubs.Count != 0 ) && ( currPollDiscoveryHandler != null ) ) {
                        if ( Log.Events.IsTracing( Log.Level.DeepDebug ) )
                            Log.Events.Trace( Log.Level.DeepDebug,
                            "<<XFinder.Listener>> running RunHandler for " + newlyPolledHubs.Count.ToString() + " new response(s)." );
                        currPollDiscoveryHandler();
                    }
                    lastListenerLoop = DateTime.Now;
                    bwListenerIsBusy = false;
                    if ( Log.Events.IsTracing( Log.Level.ExtraDeepDebug ) )
                        Log.Events.Trace( Log.Level.ExtraDeepDebug, "<<XFinder.Listener>> gone to sleep." );
                }
            }
            catch ( Exception e ) {
                if ( Log.Events.IsTracing( Log.Level.Error ) )
                    Log.Events.TraceCondition( Log.Level.Error, "ERROR: XFinder.Listener has terminated unexpectedly." );
                if ( Log.Events.IsTracing( Log.Level.DeepDebug ) ) {
                    Log.Events.Trace( Log.Level.DeepDebug, e.Message );
                    Log.Events.Trace( Log.Level.DeepDebug, e.StackTrace );
                }
            }
            finally {
                // we only restart the listener if the Finder is not currently terminating.
                // if this worker bails out, we need to restart it, but we can't do it here.
                // the IsHealthy function call allows an external user to control the overall state.
                bwListenerIsBusy = false;
                Debug.WriteLine( "exited registration-listener worker task" );
            }
        }

        // __TODO__ 0. if the hub is rebooted, a resynch fails (but a restart is fine)!
        //    probably something to do with not clearing the old hub record soon enough.

        private void SendRegistrationBroadcast ( XFArray aServiceArray, int aRepeatCount, int aDelay, DoWorkEventArgs aDWEAItem ) {
            // __NOTE__ this is a broadcast invitation, and handbuilt from message parts!
            //var msg = new XRioMsg( -1, 0, (byte)EXMapType.Registration, aServiceArray.Write( MacAddress.Broadcast ) );
            var msg = new XRioMsg( -1, 0, (byte)EXMapType.Service, aServiceArray.Write( MacAddress.Broadcast ) );
            //var pkt0 = new XLinkPacket( XKeys.XPAV, new byte[0]);
            var pkt = new XLinkPacket( XKeys.XPAV, msg.AsBytes );
            var bc = new XBroadcaster( (int)IPCommonPorts.XTEND, pkt.ToNetworkBytes(), aRepeatCount, aDelay );
            bc.Sockets = xtendUdpSockets;
            bc.Broadcast( aDWEAItem );
            bc.Dispose();
            // stack refs need not be set to null.
        }

        private AutoResetEvent AwakenInviter = new AutoResetEvent( false );
        private AutoResetEvent AwakenListener = new AutoResetEvent( false );
        private bool bwOutstandingInvite = false;
        private DateTime lastInviterRequest = DateTime.Now;
        private DateTime lastInviterLoop = DateTime.Now;
        private DateTime lastInviterDismiss = DateTime.Now;

        // inviter sends broadcast message inviting remote devices to publish their details.
        // devices will not register if they have been enrolled, but that might be after they receive duplicate invites.
        private void bwInviter_DoWork ( object aSender, DoWorkEventArgs aDWEAItem ) {
            // ensure the network interfaces have been interrogated at least once.
            // it is *critical* to wrap the inviter thread in a try/catch block, because unhandled exceptions will abort the app.
            try {
                while ( true ) {
                    // the inviter should just run indefinitely, and is even auto-restarted if it somehow terminates.
                    // but if cancelled, bail out now; this should only be set if terminating.
                    bool signalled = false;
                    if ( !isDisposing && !isTerminating && !bwInviter.CancellationPending )
                        signalled = AwakenInviter.WaitOne( 100 );
                    // there is *meant* to be no 'else' here - it applies both for loop restart, and for awakening.
                    if ( isDisposing || isTerminating || bwInviter.CancellationPending ) {
                        if ( Log.Events.IsTracing( Log.Level.Debug ) )
                            Log.Events.Trace( Log.Level.Debug, "<<XFinder.Inviter>> current instance was cancelled." );
                        aDWEAItem.Cancel = true;
                        return;
                    }
                    // if not signalled (ie. if timed out) just continue waiting.
                    if ( !signalled ) {
                        if ( Log.Events.IsTracing( Log.Level.ExtraDeepDebug ) )
                            Log.Events.Trace( Log.Level.ExtraDeepDebug, "<<XFinder.Inviter>> was woken by timeout." );
                        continue;
                    }
                    if ( Log.Events.IsTracing( Log.Level.Debug ) )
                        Log.Events.Trace( Log.Level.Debug, "<<XFinder.Inviter>> was woken by raising semaphore." );
                    bwInviterIsBusy = true;
                    lastInviterLoop = DateTime.Now;
                    // __TODO__ 2. decide if this broadcast dismiss should only be sent irregularly, or if known missing etc.
                    // if the network interface fails, the devices will think they have no need to reregister.
                    // therefore we must tell them that it is necessary, by sending the dismiss-these message.
                    // however, we would like to only send it when it is absolutely necessary, so look into that.
                    // would it be possible to send dismiss messages to only the missing devices, if that is optimal?
                    if ( Log.Events.IsTracing( Log.Level.Debug ) )
                        Log.Events.Trace( Log.Level.Debug, "<<XFinder.Inviter>> performing broadcast DismissThese transmission." );
                    SendRegistrationBroadcast( XMapRegistration.DismissThese, 3, 10, aDWEAItem );
                    Thread.Sleep( 10 );
                    if ( Log.Events.IsTracing( Log.Level.Debug ) )
                        Log.Events.Trace( Log.Level.Debug, "<<XFinder.Inviter>> performing multiple broadcast Invite transmission." );
                    if ( Log.Events.IsTracing( Log.Level.Debug ) )
                        Log.Events.Trace( Log.Level.Debug, "<<XFinder.Inviter>> sets <<XFinder.Listener>> semaphore." );
                    int repeats = 0;
                    while ( ++repeats <= 3 ) {
                        SendRegistrationBroadcast( XMapRegistration.Invite, 1, 0, aDWEAItem );
                        Thread.Sleep( 10 );
                        AwakenListener.Set();
                        Thread.Sleep( 10 );
                    }
                    if ( Log.Events.IsTracing( Log.Level.Debug ) )
                        Log.Events.Trace( Log.Level.Debug, "<<XFinder.Inviter>> completed broadcast requests." );
                    Thread.Sleep( 50 );
                    ++Scans;
                    bwInviterIsBusy = false;
                    bwOutstandingInvite = false;
                    if ( Log.Events.IsTracing( Log.Level.DeepDebug ) )
                        Log.Events.Trace( Log.Level.DeepDebug, "<<XFinder.Inviter>> sleeps until <<XFinder.Inviter>> semaphore is raised." );
                }
            }
            catch ( Exception e ) {
                if ( Log.Events.IsTracing( Log.Level.Error ) )
                    Log.Events.TraceCondition( Log.Level.Error, "ERROR: XFinder.Inviter has terminated unexpectedly." );
                if ( Log.Events.IsTracing( Log.Level.DeepDebug ) ) {
                    Log.Events.Trace( Log.Level.DeepDebug, e.Message );
                    Log.Events.Trace( Log.Level.DeepDebug, e.StackTrace );
                }
            }
            finally {
                // force the 'healthy' check into a failing state.
                bwOutstandingInvite = true;
                Debug.WriteLine( "exited registration-inviter worker task" );
            }
        }

        public bool IsBusy () { return bwInviterIsBusy; }
        public bool IsReady () { return ( XIO.Finder.Scans != 0 ) && !XIO.Finder.IsBusy(); }

        private RunHandler currPollDiscoveryHandler = null;
        public XFinder Run ( RunHandler aHandler ) {
            currPollDiscoveryHandler = aHandler;
            return Run();
        }
        public XFinder Run () {
            if ( isDisposing )
                return null;
            if ( Log.Events.IsTracing( Log.Level.DeepDebug ) )
                Log.Events.Trace( Log.Level.DeepDebug, "<<XFinder>> start to Run()." );
            XFinder instance = this;
            // if the live Finder instance is unhealthy for some reason, start a new instance.
            if ( ( instance == null ) || !instance.IsHealthy() ) {
                string tag = instance == null ? "null" : instance.roleTag;
                if ( instance != null ) {
                    instance.ForceTerminate();
                    if ( Log.Events.IsTracing( Log.Level.Debug ) )
                        Log.Events.Trace( Log.Level.Debug, "<<XFinder>> current instance (" + tag + ") was judged unhealthy" );
                }
                if ( Log.Events.IsTracing( Log.Level.Debug ) )
                    Log.Events.Trace( Log.Level.Debug, "<<XFinder>> starting new instance (" + tag + ")" );
                instance = new XFinder( roleTag );
            }
            instance.lastInviterRequest = DateTime.Now;
            instance.bwOutstandingInvite = true;
            instance.AwakenInviter.Set();
            return instance;
        }
        public void SetPollDiscoveryHandler ( RunHandler aHandler ) {
            if ( Log.Events.IsTracing( Log.Level.DeepDebug ) )
                Log.Events.Trace( Log.Level.DeepDebug, "<<XFinder>> set new RunHandler callback delegate." );
            currPollDiscoveryHandler = aHandler;
        }

        private void DoTerminate ( bool aForceFlag ) {
            // there might be a race condition with the workers if Inviters are disposed while waited on.
            // the docs say that undefined behaviour results from using outstanding references after closing.
            // therefore we have made some effort to ensure the workers bail out as fast as possible,
            //    when the inviters are also about to be disposed of.
            // if not forcing the termination, we watch the isBusy signal to ensure the workers have exited.
            isTerminating = true;
            if ( bwInviter != null ) {
                if ( bwInviter.WorkerSupportsCancellation )
                    bwInviter.CancelAsync();
                if ( AwakenInviter != null )
                    AwakenInviter.Set();
                // we need this because the Inviter thread could start up just before being terminated.
                // then we get a problem with resources being ripped out from under it.
                Thread.Sleep( 1 );
                if ( !aForceFlag )
                    while ( bwInviterIsBusy )
                        Thread.Sleep( 1 );
                bwInviter.DoWork -= doInviterWorkEventHandler;
                bwInviter.Dispose();
                bwInviter = null;
            }
            if ( AwakenInviter != null ) {
                AwakenInviter.Close();
                AwakenInviter = null;
            }
            if ( bwListener != null ) {
                if ( bwListener.WorkerSupportsCancellation )
                    bwListener.CancelAsync();
                if ( AwakenListener != null )
                    AwakenListener.Set();
                // we need this because the Listener thread could start up just before being terminated.
                // then we get a problem with resources being ripped out from under it.
                Thread.Sleep( 1 );
                if ( !aForceFlag )
                    while ( bwListenerIsBusy )
                        Thread.Sleep( 1 );
                bwListener.DoWork -= doListenerWorkEventHandler;
                bwListener.Dispose();
                bwListener = null;
            }
            if ( AwakenListener != null ) {
                AwakenListener.Close();
                AwakenListener = null;
            }
            // we do not disconnect all connected hubs just because Finder has had a problem.
            // hubs should only disconnect when they cannot be pinged or comms fails.
            // here we just clear out some admin records, and ensure a clean state for the next start.
            lock ( newlyPolledHubs )
                newlyPolledHubs.Clear();
            lock ( newlyRegisteredHubs )
                newlyRegisteredHubs.Clear();
            lock ( xtendUdpSockets ) {
                foreach ( Socket s in xtendUdpSockets )
                    s.Close();
                xtendUdpSockets.Clear();
            }
        }
        public void Terminate () { DoTerminate( false ); }
        public void ForceTerminate () { DoTerminate( true ); }

        public bool IsHealthy () {
            // ONLY IF DEBUGGING !!!
            //return true;
            if ( lastListenerLoop.MillisecondsSince() > 1000 ) {
                if ( Log.Events.IsTracing( Log.Level.Debug ) )
                    Log.Events.Trace( Log.Level.Debug, "<<XFinder>> current Listener instance exceeds LastListenerLoop timeout." );
                return false;
            } else if ( bwOutstandingInvite && ( lastInviterRequest.MillisecondsSince() > 1000 ) ) {
                if ( Log.Events.IsTracing( Log.Level.Debug ) )
                    Log.Events.Trace( Log.Level.Debug, "<<XFinder>> current Inviter instance exceeds OutstandingInvite and LastInviterRequest timeout." );
                return false;
            } else
                return true;
        }
        public IEnumerable<XHProxy> Found () {
            lock ( newlyPolledHubs )
                return newlyPolledHubs.Distinct<XHProxy>();
        }
        public IEnumerable<XHProxy> Registered () {
            lock ( newlyRegisteredHubs )
                return newlyRegisteredHubs.Distinct<XHProxy>();
        }
        public int Scans { get; set; }

        //public void SetScanPeriod( int aMillisecPeriod ) { scanPeriod = aMillisecPeriod; }
        public void OnProgressChanged ( ProgressChangedEventHandler aHandler ) { bwInviter.ProgressChanged += aHandler; }
        public void OnCompleted ( RunWorkerCompletedEventHandler aHandler ) { bwInviter.RunWorkerCompleted += aHandler; }

        public void OnProgressChangedRemove ( ProgressChangedEventHandler aHandler ) { bwInviter.ProgressChanged -= aHandler; }
        public void OnCompletedRemove ( RunWorkerCompletedEventHandler aHandler ) { bwInviter.RunWorkerCompleted -= aHandler; }

        private DoWorkEventHandler doInviterWorkEventHandler;
        private void CreateInviterWorker () {
            doInviterWorkEventHandler = new DoWorkEventHandler( bwInviter_DoWork );
            bwInviter.DoWork += doInviterWorkEventHandler;
            bwInviter.WorkerSupportsCancellation = true;
            bwInviter.RunWorkerAsync();
        }
        private DoWorkEventHandler doListenerWorkEventHandler;
        private void CreateListenerWorker () {
            doListenerWorkEventHandler = new DoWorkEventHandler( bwListener_DoWork );
            bwListener.DoWork += doListenerWorkEventHandler;
            bwListener.WorkerSupportsCancellation = true;
            bwListener.RunWorkerAsync();
        }

        // default initialization.
        public XFinder ( string aTag ) {
            isTerminating = false;
            isDisposing = false;
            roleTag = aTag;
            UpdateFinderUdpSockets();
            // the listener and inviter, always running (unless failed, when IsHealthy() should see a problem).
            CreateListenerWorker();
            CreateInviterWorker();
        }
        protected virtual void Dispose ( bool disposing ) {
            isDisposing = true;
            if ( disposing )
                GC.SuppressFinalize( this );
            ForceTerminate();
        }
        ~XFinder () {
            Dispose( false );
        }
        public void Dispose () {
            Dispose( true );
        }
    }
}



