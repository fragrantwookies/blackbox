﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using XPointAudiovisual.Core;

#endregion

namespace XPointAudiovisual.Xtend {

    // public interfaces.
    // ------------------

    public delegate void XIOAction();
    public delegate bool XIOCondition();

    public enum ActionMode { Quiet, Report };
    interface IActionReport {
        DateTime Timestamp { get; set; }
        Guid Binding { get; set; }
        string Message { get; set; }
        Exception HadException { get; set; }
    }

    // note that XIO is a static class and cannot actually implement this interface!
    // it is specfied here for documentation purposes only, to show available XIO features.

    interface IXIO {
        // TCP port #7531 hubs only!
        // Known is all hubs which have recently been connected to the system, Connected is currently available.
        List<XHProxy> DetectedHubs { get; }
        List<XHProxy> ConnectedHubs { get; }

        // "Finder" service instance packaged in XIO.
        XFinder Finder { get; }

        // Independent concurrent operations handling by threadpool.

        // 'Do' actions have both Guid and non-Guid versions.
        // Guid versions will store records of error, completion etc in the event notification queue.
        // *** It is the caller's responsibility to flush the queue of any records it initiates ***
        // __TODO__ 5. rather return a reference to an object that is automatically collected if dropped?

        // will report events (initiated, invoked, completed, terminated, failed) if selected report mode.
        // otherwise defaults to quiet mode, ie. no reporting.
        // note that exceptions thrown in the action are *always* buried.

        Guid DoIf( XIOCondition aCondition, XIOAction anAction, ActionMode aMode );
        void DoIf( XIOCondition aCondition, XIOAction anAction );
        Guid Do( XIOAction anAction, ActionMode aMode );
        void Do( XIOAction anAction );

        Guid DoWhile( XIOCondition aCondition, int aPeriod, XIOAction anAction, ActionMode aMode );
        void DoWhile( XIOCondition aCondition, int aPeriod, XIOAction anAction );
        Guid DoForever( int aPeriod, XIOAction anAction, ActionMode aMode );
        void DoForever( int aPeriod, XIOAction anAction );
        Guid DoTimes( int aCount, int aPeriod, XIOAction anAction, ActionMode aMode );
        void DoTimes( int aCount, int aPeriod, XIOAction anAction );

        // must be called by the application.
        void Open();
        void Close();
    }


    // implementations.
    // -------------------

    public static class XIO {
        // hubs which have reported their presence by means of registration messages.
        // these have not necessarily been initialized or even connected yet.
        public static List<XHProxy> DetectedHubs = new List<XHProxy>();
        public static List<XHProxy> DetectedHubsCopy { get { return DetectedHubs.LockedCopy<XHProxy>(); } }

        //public static List<XHProxy> AttachedHubs = new List<XHProxy>();
        private static bool isClosing = false;

        private static XFinder currFinder = null;
        public static XFinder Finder { get { return currFinder; } }

        public static List<ActionReport> ActionReports = new List<ActionReport>();
        public class ActionReport {
            public DateTime Timestamp { get; set; }
            public Guid Binding { get; set; }
            public string Message { get; set; }
            public Exception HadException { get; set; }
            public ActionReport( Guid aBinding, string aMessage, Exception anException ) {
                this.Timestamp = DateTime.Now;
                this.Binding = aBinding;
                this.HadException = anException;
                this.Message = aMessage;
            }
        }

        //  turn this into a threadpool operation and dispatch it.
        // buries any exceptions, else the whole process will be terminated!
        // wraps the raw lambda in another delegate that handles exceptions,
        //      and threadpools that instead.
        private class ActionState {
            string Description = "";
            private ActionMode Mode;
            public Guid Binding;
            private bool MustReport;
            private XIOCondition Condition;
            private XIOAction Action;
            private void doIfActionCallback( Object aStateObject ) {
                Thread.CurrentThread.Name = Description;
                if ( XIO.isClosing ) return;
                try {
                    if ( this.Condition() ) {
                        //Debug.WriteLine("starting action - " + Description +" " + Action.Target.ToString());
                        if ( MustReport ) ActionReports.Add( new ActionReport( Binding, "invoked", null ) );
                        this.Action();
                        if ( MustReport ) ActionReports.Add( new ActionReport( Binding, "performed", null ) );
                        //Debug.WriteLine( "finishing action - " + Description + " " + Action.Target.ToString() );
                    }
                }
                catch ( Exception e ) {
                    if ( MustReport ) ActionReports.Add( new ActionReport( Binding, "failed", e ) );
                    //Debug.WriteLine( "failed action - " + Description + " " + Action.Target.ToString() );
                }
            }
            //public ActionState ( XIOCondition aCondition, XIOAction anAction, ActionMode aMode ) {
            //    this.Mode = aMode;
            //    this.MustReport = ( aMode == ActionMode.Report );
            //    this.Binding = this.MustReport ? Guid.NewGuid() : Guid.Empty;
            //    this.Condition = aCondition;
            //    this.Action = anAction;
            //    ThreadPool.QueueUserWorkItem( doIfActionCallback );
            //}
            public ActionState (string aDescription, XIOCondition aCondition, XIOAction anAction, ActionMode aMode ) {
                this.Description = aDescription;
                this.Mode = aMode;
                this.MustReport = ( aMode == ActionMode.Report );
                this.Binding = this.MustReport ? Guid.NewGuid() : Guid.Empty;
                this.Condition = aCondition;
                this.Action = anAction;
                ThreadPool.QueueUserWorkItem( doIfActionCallback );
            }
        }
        private static Guid DoIfHelper ( XIOCondition aCondition, XIOAction anAction, ActionMode aMode ) {
            return new ActionState( "", aCondition, anAction, aMode ).Binding;
        }
        private static Guid DoIfHelper (string aDescription, XIOCondition aCondition, XIOAction anAction, ActionMode aMode ) {
            return new ActionState(aDescription, aCondition, anAction, aMode ).Binding;
        }
        public static Guid DoIf ( XIOCondition aCondition, XIOAction anAction, ActionMode aMode ) {
            return DoIfHelper( aCondition, anAction, aMode );
        }
        public static void DoIf( XIOCondition aCondition, XIOAction anAction ) {
            DoIf( aCondition, anAction, ActionMode.Quiet );
        }
        public static Guid Do ( XIOAction anAction, ActionMode aMode ) {
            return DoIfHelper( () => true, anAction, aMode );
        }
        public static Guid Do (string aDescription, XIOAction anAction, ActionMode aMode ) {
            return DoIfHelper( aDescription, () => true, anAction, aMode );
        }
        public static void Do ( XIOAction anAction ) {
            Do( anAction, ActionMode.Quiet );
        }
        public static void Do ( string aDescription, XIOAction anAction ) {
            Do( aDescription, anAction, ActionMode.Quiet );
        }
        // creates a timer, turns this into a threadpool operation and dispatches it.
        // takes care of exceptions, else the whole process would be terminated!
        private static List<ActionWhileState> ActionWhileStates = new List<ActionWhileState>();
        private class ActionWhileState : IDisposable {
            public Guid Binding;
            private ActionMode mode;
            private bool mustReport;
            private XIOCondition condition;
            private XIOAction action;
            private System.Threading.Timer timer;
            private void doWhileActionCallback( Object aStateObject ) {
                if ( XIO.isClosing ) return;
                try {
                    if ( this.condition() ) {
                        if ( mustReport ) ActionReports.Add( new ActionReport( Binding, "invoked", null ) );
                        this.action();
                        if ( mustReport ) ActionReports.Add( new ActionReport( Binding, "performed", null ) );
                    }
                    else {
                        timer.Dispose();
                        ActionWhileStates.Remove( this );
                        if ( mustReport ) ActionReports.Add( new ActionReport( Binding, "terminated", null ) );
                    }
                }
                catch ( Exception e ) {
                    if ( mustReport ) ActionReports.Add( new ActionReport( Binding, "failed", e ) );
                }
            }
            public ActionWhileState( XIOCondition aCondition, int aPeriod, XIOAction anAction, ActionMode aMode ) {
                this.mode = aMode;
                this.mustReport = ( aMode == ActionMode.Report );
                this.Binding = this.mustReport ? Guid.NewGuid() : Guid.Empty;
                condition = aCondition;
                action = anAction;
                ActionWhileStates.Add( this );
                timer = new System.Threading.Timer( doWhileActionCallback, this, 0, aPeriod );
                if ( mustReport )
                    ActionReports.Add( new ActionReport( Binding, "initiated", null ) );
            }

            ~ActionWhileState() {
                Dispose( false );
            }
            public void Dispose() {
                Dispose( true );
            }
            protected virtual void Dispose( bool disposing ) {
                if ( disposing )
                    GC.SuppressFinalize( this );
                if ( timer != null )
                    timer.Dispose();
            }
        }
        private static Guid doWhileHelper( XIOCondition aCondition, int aPeriod, XIOAction anAction, ActionMode aMode ) {
            return new ActionWhileState( aCondition, aPeriod, anAction, aMode ).Binding;
        }
        public static Guid DoWhile( XIOCondition aCondition, int aPeriod, XIOAction anAction, ActionMode aMode ) {
            return doWhileHelper( aCondition, aPeriod, anAction, aMode );
        }
        public static void DoWhile( XIOCondition aCondition, int aPeriod, XIOAction anAction ) {
            DoWhile( aCondition, aPeriod, anAction, ActionMode.Quiet );
        }
        public static Guid DoForever( int aPeriod, XIOAction anAction, ActionMode aMode ) {
            return doWhileHelper( () => true, aPeriod, anAction, aMode );
        }
        public static void DoForever( int aPeriod, XIOAction anAction ) {
            DoForever( aPeriod, anAction, ActionMode.Quiet );
        }
        public static Guid DoTimes( int aCount, int aPeriod, XIOAction anAction, ActionMode aMode ) {
            int count = aCount;
            return doWhileHelper( () => count-- > 0, aPeriod, anAction, aMode );
        }
        public static void DoTimes( int aCount, int aPeriod, XIOAction anAction ) {
            DoTimes( aCount, aPeriod, anAction, ActionMode.Quiet );
        }

        // application interface.
        // -----------------------
        public static void Find( RunHandler aHandler ) {
            if ( currFinder == null )
                currFinder = new XFinder( "XIO" );
            currFinder.SetPollDiscoveryHandler( aHandler );
            currFinder = XIO.Finder.Run();
        }
        public static void Open( RunHandler aHandler ) {
            ThreadPool.SetMinThreads( 32, 256 );
            // handle possibility of multiple Open() calls by forcibly closing first if needed.
            if ( currFinder != null )
                Close();
            Do( () => Find( aHandler ) );
        }
        public static void Open() {
            Open( null );
        }
        public static void Close() {
            try {
                // let any potential live actions know they should not run from now on.
                XIO.isClosing = true;
                if ( currFinder != null ) {
                    currFinder.Dispose();
                    currFinder = null;
                }
                // clear out any outstanding actions or reports.
                if ( ActionWhileStates != null ) {
                    foreach ( var aws in ActionWhileStates )
                        aws.Dispose();
                    ActionWhileStates.Clear();
                }
                if ( ActionReports != null )
                    ActionReports.Clear();
                // now clear out the records of any existing detected hubs.
                if ( XIO.DetectedHubs != null ) {
                    lock ( XIO.DetectedHubs ) {
                        foreach ( var hub in XIO.DetectedHubs )
                            hub.Dispose();
                        XIO.DetectedHubs.Clear();
                    }
                }
            }
            finally {
                XIO.isClosing = false;
            }
        }
    }
    // __TODO__ 5. some sort of helper object to ensure the XIO static class gets closed on program termination.

}

