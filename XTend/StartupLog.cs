using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Globalization;

namespace XPointAudiovisual.Core
{
    public static class Log {
        public static RuntimeLog Events = new RuntimeLog();
        public static RuntimeLog Debug = new RuntimeLog();

        public enum Level { 
            ExtraDeepDebug = -2, DeepDebug, Debug, 
            Verbose, Informative = 3, Standard = 5, Warning = 7, Error = 9, Critical = 10 }
    }
    public class RuntimeLog {
        //StreamWriter sw;
        string logFileName;
        string PrepareLogFile( string mode, string ext ) {
            return "";
        }
        public string text = "";

        // accepts only serious logging messages by default.
        public int FilterLevel = -2;

        StringBuilder indent = new StringBuilder( "" );
        List<string> entries = new List<string>();
        private object fileLock = new object();

        public string Filtered( int level, string str ) { return level >= FilterLevel ? str : ""; }

        enum TraceKind { none = 0x00, entry, data, exit };

        TraceKind lastKind = TraceKind.none;
        DateTime lastDttm = DateTime.MinValue;
        public RuntimeLog( string mode ) {
            // create a new file with current date and time in the name.
            logFileName = PrepareLogFile( mode, "txt" );
        }

        public RuntimeLog() {
            // create a new file with current date and time in the name.
            logFileName = PrepareLogFile( "", "txt" );
        }


        // <NOTE> we open and close every time we write to ensure the data is flushed.
        //  otherwise we risk crashing with uncommitted writes, thereby losing half the value of the log.

        private void WriteNewLine() {
            return;
            text += "\r\n";
        }

        private void CheckDateTime( string str ) {
            return;
            DateTime thisDttm = DateTime.Now;
            if ( ( str != "" ) && ( ( thisDttm - lastDttm ).TotalMilliseconds >= 2 ) ) {
                if ( thisDttm.Date != lastDttm.Date )
                    text += ( "[" + thisDttm.ToString( "yy/MM/dd" ) + "]\r\n" );
                lastDttm = thisDttm;
                text += ("[" + thisDttm.ToString("HH:mm:ss.fff") + "]  ");
            }
            else
                text += ("                ");
        }

        private void WriteStrings( string prefix, string[] str ) {
            return;
                    foreach ( string s in str ) {
                        text += (prefix);
                        if ( s != "" )
                            CheckDateTime( s );
                        text += (indent + s);
                        text += ("\r\n");
                    }
        }

        private void Write( string prefix, string str ) {
            return;
            text += (prefix);
                    if ( str != "" )
                        CheckDateTime( str );
                    text += (indent + str);
                    //if ( str != "" )
                    //    CheckDateTime( str );
                    text += ("\r\n");
        }

        private DateTime last = DateTime.MinValue;

        public bool IsTracing( Log.Level level ) { return (int)level >= FilterLevel; }

        public void Trace( string str ) {
            WriteStrings( "", str.Split( '\n' ) );
            lastKind = TraceKind.data;
        }
        public void Trace( bool cond, string str ) { if ( cond ) Trace( str ); }
        public void Trace( bool cond, int ms, string str ) {
            if ( cond && ( ( DateTime.Now - last ).TotalMilliseconds > ms ) )
                Trace( str );
            last = DateTime.Now;
        }
        public void Trace( int level, string str ) { if ( level >= FilterLevel ) Trace( str ); }
        public void Trace( Log.Level level, string str ) { if ( (int)level >= FilterLevel ) Trace( str ); }

        public void TraceEntry( string str ) {
            lock ( fileLock ) {
                Write( ( ( lastKind == TraceKind.entry ) || ( lastKind == TraceKind.exit ) ? "\r\n" : "" ), "begin " + str );
                indent.Append( "   " );
                entries.Insert( 0, str );
                lastKind = TraceKind.entry;
            }
            //if (HasVisibleBanner())
            //    DoWriteBanner("begin " + str);
        }
        public void TraceEntry( bool cond, string str ) { if ( cond ) TraceEntry( str ); }
        public void TraceEntry( bool cond, int ms, string str ) {
            if ( cond && ( ( DateTime.Now - last ).TotalMilliseconds > ms ) )
                TraceEntry( str );
            last = DateTime.Now;
        }
        public void TraceEntry( int level, string str ) { if ( level >= FilterLevel ) TraceEntry( str ); }
        public void TraceEntry( Log.Level level, string str ) { if ( (int)level >= FilterLevel ) TraceEntry( str ); }

        public void TraceExit() {
            string str = "";
            lock ( fileLock ) {
                if ( indent.Length != 0 )
                    indent.Length -= 3;
                if ( entries.Count != 0 ) {
                    Write( ( lastKind == TraceKind.exit ) ? "\r\n" : "", ".done " + entries[ 0 ] );
                }
                entries.RemoveAt( 0 );
                lastKind = TraceKind.exit;
            }
            //if (HasVisibleBanner())
            //    DoWriteBanner("done " + str);
        }
        public void TraceExit( int level ) { if ( level >= FilterLevel ) TraceExit(); }
        public void TraceExit( Log.Level level ) { if ( (int)level >= FilterLevel ) TraceExit(); }

        public void TraceCondition( string str ) {
            try {
                Take();
                WriteNewLine();
                Trace( "--------------------" );
                WriteStrings( "", str.Split( '\n' ) );
                Trace( "--------------------" );
                WriteNewLine();
            }
            finally {
                Give();
            }
        }
        public void TraceCondition( bool cond, string str ) { if ( cond ) TraceCondition( str ); }
        public void TraceCondition( bool cond, int ms, string str ) {
            if ( cond && ( ( DateTime.Now - last ).TotalMilliseconds > ms ) )
                TraceCondition( str );
            last = DateTime.Now;
        }
        public void TraceCondition( int level, string str ) { if ( level >= FilterLevel ) TraceCondition( str ); }
        public void TraceCondition( Log.Level level, string str ) { if ( (int)level >= FilterLevel ) TraceCondition( str ); }

        public void TraceEnvironment( string str ) {
            try {
                Take();
                WriteNewLine();
                Trace( "--------------------" );
                WriteStrings( "", str.Split( '\n' ) );
                WriteNewLine();
                WriteStrings( "", Environment.StackTrace.Split( '\n' ) );
                Trace( "--------------------" );
                WriteNewLine();
            }
            finally {
                Give();
            }
        }
        public void TraceEnvironment( bool cond, string str ) { if ( cond ) TraceEnvironment( str ); }
        public void TraceEnvironment( bool cond, int ms, string str ) {
            if ( cond && ( ( DateTime.Now - last ).TotalMilliseconds > ms ) )
                TraceEnvironment( str );
            last = DateTime.Now;
        }
        public void TraceEnvironment( int level, string str ) { if ( level >= FilterLevel ) TraceEnvironment( str ); }
        public void TraceEnvironment( Log.Level level, string str ) { if ( (int)level >= FilterLevel ) TraceEnvironment( str ); }

        public void TraceException( bool cond, string str, Exception e ) { if ( cond ) TraceException( str, e ); }
        public void TraceException( string str, Exception e ) {
            try {
                Take();
                WriteNewLine();
                Trace( "--------------------" );
                WriteStrings( "", str.Split( '\n' ) );
                Trace( e.GetType().ToString() + ": \"" + e.Message + "\"" );
                Exception ie = e;
                while ( ie.InnerException != null ) {
                    Trace( "(Inner: \"" + ie.InnerException.Message + "\")" );
                    ie = ie.InnerException;
                }
                bool gaps = ( ( e.Source != null ) || ( e.TargetSite != null ) || ( e.StackTrace != null ) );
                if ( gaps ) {
                    WriteNewLine();
                    if ( e.Source != null )
                        Trace( "Source: " + e.Source );
                    if ( e.TargetSite != null )
                        Trace( "Target site: " + e.TargetSite );
                    WriteNewLine();
                    if ( e.StackTrace != null )
                        WriteStrings( "", e.StackTrace.Split( '\n' ) );
                }
                Trace( "--------------------" );
                WriteNewLine();
            }
            finally {
                Give();
            }
        }
        public void TraceException( int level, string str, Exception e ) { if ( level >= FilterLevel ) TraceException( str, e ); }
        public void TraceException( Log.Level level, string str, Exception e ) { if ( (int)level >= FilterLevel ) TraceException( str, e ); }

        public void Take() { Monitor.Enter( fileLock ); }
        public void Give() { Monitor.Exit( fileLock ); }
    }


    public class LogEntryExit : IDisposable {
        private int level = 5;
        public LogEntryExit( string str ) { if ( level >= Log.Events.FilterLevel )  Log.Events.TraceEntry( str ); }
        public LogEntryExit( int aLevel, string str ) { Log.Events.TraceEntry( level = (int)aLevel, str ); }
        public LogEntryExit( Log.Level aLevel, string str ) { Log.Events.TraceEntry( level = (int)aLevel, str ); }
        public void Dispose() { Log.Events.TraceExit( level ); }
    }

}

