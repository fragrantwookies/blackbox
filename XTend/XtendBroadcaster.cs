﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Reflection;
using XPointAudiovisual.Core;
#endregion

namespace XPointAudiovisual.Xtend {
    public class XBroadcaster : IDisposable {

        // __NOTE__ the broadcaster never receives.

        private bool ownSockets = false;
        public List<Socket> sockets;
        public List<Socket> Sockets { get { return sockets; } set { sockets = value; } }

        private int port, repeat, delay;
        private byte[] message;

        private bool terminated = false;
        public bool Terminated { get { return terminated; } set { if ( value ) terminated = true; } }

        public object Open () {
            // open UDP sockets on all available network interfaces on this machine.
            // note that this includes AutoIP addresses *if* the local machine has one or AutoIP enabled.
            ownSockets = ( sockets == null );
            if ( ownSockets ) {
                sockets = new List<Socket>();
                XNetworkInterfaces.Instance.Update();
                foreach ( var ai in XNetworkInterfaces.Instance.EnlistedInterfaces ) {
                    var s = new Socket( AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp );
                    s.ExclusiveAddressUse = false;
                    s.Bind( new IPEndPoint( ai.Address, port ) );
                    s.EnableBroadcast = true;
                    lock ( sockets )
                        sockets.Add( s );
                }
            }
            return sockets;
        }

        // broadcast a query to the specified port, which is presumably monitored by the targets.
        public void Send () {
            // simple broadcast invitation pkt.
            // send it to every interface we could find on this machine.
            lock ( sockets ) {
                var remoteEndPoint = new IPEndPoint( IPAddress.Broadcast, port );
                foreach ( var s in sockets )
                    try {
                        // broadcast UDP datagram message in expected format.
                        int sentCount = s.SendTo( message, remoteEndPoint );
                        if ( Log.Events.IsTracing( Log.Level.ExtraDeepDebug ) )
                            Log.Events.Trace( Log.Level.ExtraDeepDebug,
                                "<<Xtend.Broadcaster>> sent to " + remoteEndPoint.Address
                                + ", port " + remoteEndPoint.Port
                                + " from " + s.LocalEndPoint );
                    }
                    catch {
                        // we don't care too much if this doesn't work for any individual socket.
                        // __TODO__ 5. set a flag to have a manager examine the sockets for problems.
                        if ( Log.Events.IsTracing( Log.Level.ExtraDeepDebug ) )
                            Log.Events.Trace( Log.Level.ExtraDeepDebug,
                                "<<Xtend.Broadcaster>> failed to send to " + remoteEndPoint.Address
                                + ", port " + remoteEndPoint.Port
                                + " from " + s.LocalEndPoint );
                    }
            }
        }


        // broadcast a query to the specified port, which is presumably monitored by the targets.
        public void SendMultiple () {
            // simple broadcast invitation pkt.
            // send it multiple times, with a delay, to every interface we could find on this machine.
            if ( sockets.Count == 0 ) return;
            for ( int i = 0; i < repeat; ++i ) {
                if ( terminated ) break;
                Send();
                Thread.Sleep( delay );
            }
            if ( Log.Events.IsTracing( Log.Level.Debug ) )
                Log.Events.Trace( Log.Level.Debug, "<<Xtend.Broadcaster>> finished sending multiple messages." );
        }

        // clean up the sockets we have opened.
        public void Close () {
            if ( ownSockets && ( sockets != null ) )
                lock ( sockets ) {
                    foreach ( Socket s in sockets )
                        s.Close();
                    sockets.Clear();
                    sockets = null;
                }
        }

        public void Broadcast ( DoWorkEventArgs aDWEAItem ) {
            try {
                if ( ( aDWEAItem.Result = this.Open() ) != null )
                    this.SendMultiple();
            }
            finally {
                this.Close();
            }
        }

        // constructor basically just stores the args to local vars.
        public XBroadcaster ( int aPort, byte[] aMessage, int aRepeatCount, int aDelay ) {
            this.port = aPort;
            this.repeat = aRepeatCount;
            this.delay = aDelay;
            this.message = aMessage;
        }
        protected virtual void Dispose ( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
            this.Close();
        }
        ~XBroadcaster () {
            Dispose( false );
        }
        public void Dispose () {
            Dispose( true );
        }

    }
}

