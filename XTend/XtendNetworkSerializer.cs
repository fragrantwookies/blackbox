﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Reflection;
#endregion

// handle conversion between local machine and network endiannesses.
// note that this cannot handle classes, only primitives, arrays, structs and nested instances of these.
// any struct using this functionality is assumed to desire network byte order encode/decode.
// this removes the need to put 'BigEndian' attributes on all non-byte fields of a data structure.

// __NOTE__ hasn't been tested with nested instances!

public static class NetworkSerializer {

    // must use type context (no generic) to allow recursive calls.
    private static void HandleEndianness( Type aType, byte[] aDataArray, int aStartIndex ) {
        // if running on a big-endian machine, then same as network byte order, so bail out.
        if ( !BitConverter.IsLittleEndian ) return;
        // little-endian machine, so check to see if anything needs mangling to network byte order.
        foreach ( FieldInfo f in aType.GetFields() ) {
            if ( f.FieldType.IsPrimitive && ( Marshal.SizeOf( f.FieldType ) <= 1 ) )
                continue;
            // it's something bigger than a byte, so probably needs some kind of endianness handling.
            int offset = Marshal.OffsetOf( aType, f.Name ).ToInt32() + aStartIndex;
            // check if it's a primitive, and if so just reverse the bytes in it.
            if ( f.FieldType.IsPrimitive )
                Array.Reverse( aDataArray, offset, Marshal.SizeOf( f.FieldType ) );
            // else if it's a section type (struct), apply this handling to the whole struct.
            else if ( f.FieldType.IsValueType )
                HandleEndianness( f.FieldType, aDataArray, offset );
            // else if it's an array, apply this handling to each element of the array.
            else if ( f.FieldType.IsArray ) {
                object[] attrs = f.GetCustomAttributes( false );
                if ( ( attrs.Count() > 0 ) && ( attrs[ 0 ] is MarshalAsAttribute ) ) {
                    // get these values outside of the loop!
                    Type elementType = f.FieldType.GetElementType();
                    int elementSize = Marshal.SizeOf( elementType );
                    // we don't bother handling arrays whose elements are only one byte in size.
                    if ( elementSize > 1 ) {
                        int arraySize = ( (MarshalAsAttribute)( attrs[ 0 ] ) ).SizeConst;
                        // note that this could be an array of structs etc.
                        // therefore can't just do primitive reversals.
                        if ( elementType.IsPrimitive )
                            for ( int i = 0; i < arraySize; ++i )
                                Array.Reverse( aDataArray, offset + ( i * elementSize ), elementSize );
                        else
                            for ( int i = 0; i < arraySize; ++i )
                                HandleEndianness( elementType, aDataArray, offset + ( i * elementSize ) );
                    }
                }
            }
        }
    }

    public static object FromNetworkBytes( Type aType, byte[] aDataArray ) {
        GCHandle handle = default( GCHandle );
        object result = null;
        HandleEndianness( aType, aDataArray, 0 );
        try {
            // pin the incoming working memory, then marshal the struct to its address.
            handle = GCHandle.Alloc( aDataArray, GCHandleType.Pinned );
            IntPtr rawDataPtr = handle.AddrOfPinnedObject();
            result =Marshal.PtrToStructure( rawDataPtr, aType );
        }
        finally {
            // free the pinning handle to the raw data memory.
            handle.Free();
        }
        return result;
    }

    public static byte[] ToNetworkBytes( Type aType, object aDataObject ) {
        byte[] rawData = null;
        GCHandle handle = default( GCHandle );
        try {
            // get memory for result, pin the memory to work in, then marshal to raw data.
            rawData = new byte[ Marshal.SizeOf( aDataObject ) ];
            handle = GCHandle.Alloc( rawData, GCHandleType.Pinned );
            IntPtr rawDataPtr = handle.AddrOfPinnedObject();
            Marshal.StructureToPtr( aDataObject, rawDataPtr, false );
        }
        finally {
            // free the pinning handle to the raw data result memory.
            if ( handle != null )
                handle.Free();
        }
        HandleEndianness( aType, rawData, 0 );
        return rawData;
    }
}
