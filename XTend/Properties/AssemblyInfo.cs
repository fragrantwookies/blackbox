﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "XtendIO" )]
[assembly: AssemblyDescription( "XTEND device support" )]
[assembly: AssemblyConfiguration( "" )]
[assembly: AssemblyCompany( "XPoint Audiovisual" )]
[assembly: AssemblyProduct( "XtendIO" )]
[assembly: AssemblyCopyright( "Copyright © XPAV 2012" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible( false )]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid( "6f861349-6f19-4a5b-929a-64472aad6193" )]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion( "1.10.17156.1193" )]
[assembly: AssemblyFileVersion( "1.10.16133.1152" )]
