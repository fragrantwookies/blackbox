using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Globalization;


namespace XPointAudiovisual.Core
{    
    public static class DttmStdFmt {
        // default encoding for standard parsing and formatting.
        public static IFormatProvider Get = new CultureInfo( "fr-FR" );
        public static string ToStdFmtString( this DateTime aDttm ) { return aDttm.ToString( DttmStdFmt.Get ); }
        public static DateTime Parse( string aDttmStr ) { return DateTime.Parse( aDttmStr, DttmStdFmt.Get ); }
        public static bool TryParse( string aDttmStr, out DateTime aDateTimeRec ) {
            return DateTime.TryParse( aDttmStr, DttmStdFmt.Get, System.Globalization.DateTimeStyles.None, out aDateTimeRec );
        }
    }
    public static class Extensions {
        public static byte[] Slice( this byte[] aByteArray, int aStartPos, int aLength ) {
            var result = new byte[ aLength ];
            if ( aLength != 0 )
                Array.Copy( aByteArray, aStartPos, result, 0, aLength );
            return result;
        }
        public static int Match( this byte[] aByteArray, byte[] aMatchArray, int aStartPos ) {
            int pos = aStartPos, distance = aByteArray.Length - aMatchArray.Length;
            while ( pos <= distance ) {
                bool dismatched = false;
                int ofs = 0;
                foreach ( byte b in aMatchArray )
                    if ( b != aByteArray[ pos + ofs++ ] ) {
                        dismatched = true;
                        break;
                    }
                if ( !dismatched ) return pos;
                ++pos;
            }
            return -1;
        }
        public static List<T> LockedCopy<T>( this List<T> aList )  {
            var newList = new List<T> ( aList.Count );
            lock ( aList ) {
                newList.AddRange( aList );
                return newList;
            }
        }

    }
    public class Utilities {
        public static UInt32 ToUInt32( byte[] a, int i ) { return ( ( ( (uint)a[ i + 3 ] * 256 ) + a[ i + 2 ] ) * 256 + a[ i + 1 ] ) * 256 + a[ i ]; }
        public static UInt32 ToUInt32( List<byte> a, int i ) { return ( ( ( (uint)a[ i + 3 ] * 256 ) + a[ i + 2 ] ) * 256 + a[ i + 1 ] ) * 256 + a[ i ]; }

        public static UInt32 HexToUInt32( String hexStr ) {
            return Convert.ToUInt32( hexStr, 16 );
        }

        public static UInt32 DottedToUInt32( String dotAddress ) {
            UInt32 macAddress = 0;
            string[] macFields = dotAddress.Split( '.' );
            for ( int index = 0; index <= 3; ++index ) {
                macAddress <<= 8;
                macAddress = macAddress + Byte.Parse( macFields[ index ] );
            }
            return macAddress;
        }


        public static String ToHexStr( UInt32 val ) {
            return "0x" + val.ToString( "X" );
        }

        public static String ToString( byte[] val, string fmt ) {
            StringBuilder builder = new StringBuilder();
            for ( int index = 0; index < val.Length; ++index )
                builder.Append( val[ index ].ToString( fmt ) );
            return builder.ToString();
        }

        public static String DottedToHex( String dotAddress ) {
            return ToHexStr( DottedToUInt32( dotAddress ) );
        }



        public static byte[] ToBytesArray( UInt32 val ) {
            byte[] b = new byte[ 4 ];
            for ( int index = 0; index <= 3; ++index, val >>= 8 )
                b[ 3 - index ] = (byte)( val & 0xFF );
            return b;
        }

        public static byte[] ToBytesArray( UInt16 val ) {
            return new byte[] { (byte)( ( val >> 8 ) & 0xFF ), (byte)( val & 0xFF ) };
        }

        public static byte[] ToBytesArray( String str ) {
            byte[] res = new byte[ str.Length / 2 ];
            for ( int i = 0; i < res.Length; ++i )
                res[ i ] = (byte)( UInt32.Parse( str.Substring( i * 2, 2 ), NumberStyles.HexNumber ) );
            return res;
        }



        public static String ToDotted( UInt32 address ) {
            string[] macFields = new string[ 4 ];
            for ( int index = 3; index >= 0; --index, address >>= 8 )
                macFields[ index ] = ( (byte)( address & 0xFF ) ).ToString();
            return String.Join( ".", macFields );
        }

        public static String ToDotted( byte[] address ) {
            List<string> macFields = new List<string>();
            foreach ( byte b in address )
                macFields.Add( b.ToString() );
            return String.Join( ".", macFields.ToArray() );
        }

        public static String HexToDotted( String hexAddress ) {
            return ToDotted( HexToUInt32( hexAddress ) );
        }



        public static String ToGuidString( List<byte> address ) { return ToGuidString( address.ToArray() ); }

        public static String ToGuidString( byte[] address ) {
            int index;
            StringBuilder builder = new StringBuilder( 35 );
            for ( index = 0; index <= 3; ++index ) {
                uint val = Utilities.ToUInt32( address, index * 4 );
                builder.Append( val.ToString( "X8" ) );
                if ( index != 3 ) builder.Append( "." );
            }
            return builder.ToString();
        }

        public static byte[] GuidStringToBytesArray( String GuidStr ) {
            string[] parts = GuidStr.Split( '.' );
            byte[] res = new byte[ 16 ];
            for ( int i = 0; i <= 3; ++i )
                ToBytesArray( UInt32.Parse( parts[ i ], NumberStyles.HexNumber ) ).CopyTo( res, i * 4 );
            return res;
        }


        public static string DttmForFileName( DateTime dttm ) {
            DateTimeFormatInfo dtfi = new CultureInfo( "fr-FR", false ).DateTimeFormat;
            return dttm.ToString( dtfi.UniversalSortableDateTimePattern ).Replace( 'Z', ' ' ).Replace( ':', '.' );
        }

        public static byte[] Extract( byte[] b, int start, int count ) {
            byte[] res = new byte[ count ];
            for ( int i = 0; i < count; ++i )
                res[ i ] = b[ start + i ];
            return res;
        }

        public static List<byte> ToList( byte[] b ) {
            List<byte> res = new List<byte>( b.GetLength( 0 ) );
            res.AddRange( b );
            return res;
        }

    }
}
