﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Reflection;
#endregion

namespace XPointAudiovisual.Xtend {
    public class XNetworkInterfaces {
        public static XNetworkInterfaces Instance = new XNetworkInterfaces();
        public XNetworkInterfaces() {
        }

        private object aLock = new object();
        public List<UnicastIPAddressInformation> EnlistedInterfaces = new List<UnicastIPAddressInformation>();
        private List<NetworkInterface> prevUpInterfaces = new List<NetworkInterface>();

        public List<UnicastIPAddressInformation> Clone() {
            lock ( aLock ) {
                return new List<UnicastIPAddressInformation>( EnlistedInterfaces );
            }
        }
        public bool Changed( List<UnicastIPAddressInformation> aPreviousList ) {
            lock ( aLock ) {
                if ( aPreviousList.Count != EnlistedInterfaces.Count ) return true;
                else if ( aPreviousList.Intersect( EnlistedInterfaces ).Count() != aPreviousList.Count ) return true;
                else return false;
            }
        }
        public class InterfaceComparer : IEqualityComparer<NetworkInterface> {
            public bool Equals( NetworkInterface a, NetworkInterface b ) {
                if ( Object.ReferenceEquals( a, b ) )
                    return true;
                else if ( Object.ReferenceEquals( a, null ) || Object.ReferenceEquals( b, null ) )
                    return false;
                var apa = a.GetPhysicalAddress();
                var bpa = b.GetPhysicalAddress();
                return apa.Equals( bpa );
            }
            public int GetHashCode( NetworkInterface ni ) {
                return ni.GetHashCode();
            }
        }
        public void Update() {
            lock ( aLock ) {
                // if nothing is available, clear enlisted interfaces and return immediately.
                if ( !NetworkInterface.GetIsNetworkAvailable() ) {
                    EnlistedInterfaces.Clear();
                    return;
                }
                // else get a buffer of all available and usable IPv4 network interfaces.
                // first check if the list of all available interfaces has changed - if not bail out immediately.
                // else there's a difference, so update the stored variables keeping track of previous state of all interfaces.
                // __NOTE__ sometimes Windows removes the interface from the list, sometimes it only changes the status,
                //    so quickly check the operational status of each adaptor is up before doing the return test.
                var currAllInterfaces = NetworkInterface.GetAllNetworkInterfaces().ToList<NetworkInterface>();
                var currUpInterfaces = new List<NetworkInterface>();
                foreach ( var ni in currAllInterfaces )
                    if ( ni.OperationalStatus == OperationalStatus.Up )
                        currUpInterfaces.Add( ni );
                if ( prevUpInterfaces.SequenceEqual( currUpInterfaces, new InterfaceComparer() ) )
                    return;
                prevUpInterfaces = currUpInterfaces;
                // the list of all available interfaces has changed, so rebuild the list of eligible (enlisted) ones.
                EnlistedInterfaces.Clear();
                foreach ( var ni in currUpInterfaces ) {
                    // make sure this is a useful NIC: it's up, IPv4, not loopback, and a known type
                    if ( ( ni.OperationalStatus != OperationalStatus.Up ) || !ni.Supports( NetworkInterfaceComponent.IPv4 ) )
                        continue;
                    var nit = ni.NetworkInterfaceType;
                    if ( ( nit == NetworkInterfaceType.Loopback ) || ( nit == NetworkInterfaceType.Unknown ) )
                        continue;
                    // get all IPv4-supporting instances, add them to the buffer of eligible enlisted interfaces.
                    foreach ( UnicastIPAddressInformation ai in ni.GetIPProperties().UnicastAddresses )
                        if ( ai.Address.AddressFamily == AddressFamily.InterNetwork )
                            EnlistedInterfaces.Add( ai );
                }
            }
        }
    }
}

