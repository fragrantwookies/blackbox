#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Threading;
using System.Text;
#endregion

namespace XtendExtensionMethods {
    public static class XtendExtensions {
        //public static byte[] Slice( this byte[] aByteArray, int aStartPos, int aLength ) {
        //    var result = new byte[ aLength ];
        //    if ( aLength != 0 )
        //        Array.Copy( aByteArray, aStartPos, result, 0, aLength );
        //    return result;
        //}
        //public static int Match( this byte[] aByteArray, byte[] aMatchArray, int aStartPos ) {
        //    int pos = aStartPos, distance = aByteArray.Length - aMatchArray.Length;
        //    while ( pos <= distance ) {
        //        bool dismatched = false;
        //        int ofs = 0;
        //        foreach ( byte b in aMatchArray )
        //            if ( b != aByteArray[ pos + ofs++ ] ) {
        //                dismatched = true;
        //                break;
        //            }
        //        if ( !dismatched ) return pos;
        //        ++pos;
        //    }
        //    return -1;
        //}
        public static double MillisecondsSince( this DateTime then ) {
            return ( DateTime.Now - then ).TotalMilliseconds;
        }
    }
}

namespace XPointAudiovisual.Xtend {

    // all Xtend exceptions must inherit from this, allows trapping en masse.
    public class XException : System.SystemException {
        public XException() { }
        public XException( string aMessage ) : base( aMessage ) { }
        public XException( string aMessage, Exception anInner ) : base( aMessage, anInner ) { }
    }
    // broken packet layouts, eg. CRC, header etc.
    public class XBadPacketException : XException {
        public XBadPacketException() { }
        public XBadPacketException( string aMessage ) : base( aMessage ) { }
        public XBadPacketException( string aMessage, Exception anInner ) : base( aMessage, anInner ) { }
    }
    // errors in messages, eg. bad parameters, unknown commands etc.
    public class XBadMessageException : XException {
        public XBadMessageException() { }
        public XBadMessageException( string aMessage ) : base( aMessage ) { }
        public XBadMessageException( string aMessage, Exception anInner ) : base( aMessage, anInner ) { }
    }
    // inability to perform acknowledged message delivery, timed out.
    public class XDeliveryFailureException : XException {
        public XDeliveryFailureException() { }
        public XDeliveryFailureException( string aMessage ) : base( aMessage ) { }
        public XDeliveryFailureException( string aMessage, Exception anInner ) : base( aMessage, anInner ) { }
    }


    public class CircularBuffer<T> {
        private T[] buffer;
        private int count, head, tail;
        public CircularBuffer( int aLength ) {
            buffer = new T[ aLength ];
            Clear();
        }
        public int Length { get { return buffer.Length; } }
        public int Count { get { return count; } }
        public int Space { get { return Length - count; } }

        public void Clear() { count = head = tail = 0; }

        public int Put( T aValue ) {
            if ( Space <= 0 )
                return 0;
            lock ( buffer ) {
                buffer[ head++ ] = aValue;
                ++count;
                return 1;
            }
        }
        public int Put( T[] aValuesArray ) {
            if ( ( Space <= 0 ) || ( aValuesArray.Length <= 0 ) )
                return 0;
            lock ( buffer ) {
                int puts = Space < aValuesArray.Length ? Space : aValuesArray.Length;
                int follow = Length - head;
                int seg = puts <= follow ? puts : follow;
                Array.ConstrainedCopy( aValuesArray, 0, buffer, head, seg );
                head += seg;
                if ( puts > seg ) {
                    seg = puts - seg;
                    Array.ConstrainedCopy( aValuesArray, seg, buffer, 0, seg );
                    head += seg;
                }
                count += puts;
                return puts;
            }
        }
        public T Get() {
            lock ( buffer ) {
                --count;
                return buffer[ tail++ ];
            }
        }
        // __TODO__ 9. implement this, copies an array of T of size count to result index.
        // __NOTE__ this is currently never referenced!
        public int Get( T[] aBufferArray, int anIndex, int aCount ) {
            throw new Exception( "call to unimplemented method!" );
            //lock ( buffer ) {
            //    this.count -= aCount;
            //    return 0;
            //}
        }
    }


    public class BlockingQueue<T> : IDisposable
        where T : class, IDisposable {

        private Queue<T> inputQueue = new Queue<T>();
        private EventWaitHandle itemInEventHandle = new AutoResetEvent( false );
        private Object syncRoot = new Object();

        public int Count() { return inputQueue.Count; }

        public void Clear() {
            inputQueue.Clear();
            itemInEventHandle.Reset();
        }

        public void Put( T anItem ) {
            // enqueue the data then bump any threads waiting for a packet.
            lock ( syncRoot ) {
                inputQueue.Enqueue( anItem );
                itemInEventHandle.Set();
            }
        }

        public T Get() { return Get( Timeout.Infinite ); }

        public T Get( int aTimeout ) {
            T item = null;
            // if there's something already in the queue, return that immediately,
            //   but be sure it's still there after locking as someone else could sneak in and take it seg,
            //   between the notification and the point we are able to lock it and take it for ourselves.
            lock ( syncRoot )
                if ( inputQueue.Count != 0 )
                    item = inputQueue.Dequeue();
            // if there's nothing already in the queue, or someone sneaked the only entry out from under us
            //   wait till something else arrives (with possible timeout).
            while ( item == null ) {
                if ( !itemInEventHandle.WaitOne( aTimeout, false ) )
                    return null;
                // to get here, something was inserted during a wait.
                lock ( syncRoot )
                    if ( inputQueue.Count != 0 )
                        item = inputQueue.Dequeue();
            }
            return item;
        }

        public T GetNoWait() {
            T item = null;
            lock ( syncRoot )
                if ( inputQueue.Count != 0 )
                    item = inputQueue.Dequeue();
            return item;
        }

        public WaitHandle Handle { get { return itemInEventHandle; } }

        protected virtual void Dispose( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
            lock ( syncRoot ) {
                if ( inputQueue != null ) {
                    foreach ( var i in inputQueue )
                        i.Dispose();
                    inputQueue.Clear();
                    inputQueue = null;
                }
                itemInEventHandle.Close();
            }
            syncRoot = null;
        }
        ~BlockingQueue() {
            Dispose( false );
        }
        public void Dispose() {
            Dispose( true );
        }
    }


    public static class BinEnc {
        // default encoding for binary manipulations of byte data.
        public static Encoding Get = Encoding.GetEncoding( 850 );

        // test code to ensure default string <-> binary encoding (850) actually works.
        //byte[] testa = new byte[ 256 ];
        //int elem;
        //for ( elem = 0; elem < 256; ++elem )
        //    testa[ elem ] = (byte)elem;
        //string tests = BinEnc.Get.GetString(testa);
        //byte[] testb = BinEnc.Get.GetBytes( tests );
        //for ( elem = 0; elem < 256; ++elem )
        //    if ( testa[ elem ] != testb[ elem ] )
        //        throw new Exception();
    }


    public static class BytesList {
        public static int Count( List<byte[]> aPartsList ) {
            int count = 0;
            foreach ( byte[] b in aPartsList )
                count += b.Length;
            return count;
        }
        public static byte[] Bytes( List<byte[]> aPartsList ) {
            int count = Count( aPartsList );
            var result = new byte[ count ];
            count = 0;
            foreach ( byte[] b in aPartsList ) {
                Array.Copy( b, 0, result, count, b.Length );
                count += b.Length;
            }
            return result;
        }
        public static byte[] CountedBytes( List<byte[]> aPartsList ) {
            int count = Count( aPartsList );
            aPartsList.Insert( 0, BitConverter.GetBytes( (short)count ) );
            return Bytes( aPartsList );
        }
    }


}



