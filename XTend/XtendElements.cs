﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#endregion

namespace XPointAudiovisual.Xtend {

    // proxy for common control element, one in every card, always element 0.
    public class XEAdmin : XEProxy {
        public XMService Service { get; private set; }
        public XMStatistics Statistics { get; private set; }
        public XMLog Log { get; private set; }
        public XMConfiguration Configuration { get; private set; }
        public XMCalibration Calibration { get; private set; }
        public XMRegistration Registration { get; private set; }
        public XMStatus Status { get; private set; }
        public XEAdmin ( XCProxy aParent, bool anIsHubFlag = false )
            : base( aParent, "Administration", "" ) {
            // this is a hub card, so we add statistics, log, registration and routing support to the Admin element.
            // these are normally set to null by default, as most cards are not hubs, and it clutters the GUI,
            // but be aware that they will raise an exception if dereferenced for a non-hub.
            Maps.Add( Service = new XMService( this ) );
            Maps.Add( Configuration = new XMConfiguration( this ) );
            if ( anIsHubFlag ) {
                Maps.Add( Statistics = new XMStatistics( this ) );
                Maps.Add( Log = new XMLog( this ) );
            }
            Maps.Add( Calibration = new XMCalibration( this ) );
            if ( anIsHubFlag )
                Maps.Add( Registration = new XMRegistration( this ) );
            Maps.Add( Status = new XMStatus( this ) );
        }
    }

    // proxy for network interface element.
    public class XEEthernetNetworkIntf : XEProxy {
        public XMRouting Routing { get; private set; }
        public XMNetwork Network { get; private set; }
        public XMStatus Status { get; private set; }
        public XEEthernetNetworkIntf ( XCProxy aParent )
            : base( aParent, "10/100 Ethernet", "LAN" ) {
            Maps.Add( Routing = new XMRouting( this ) );
            Maps.Add( Network = new XMNetwork( this ) );
            Maps.Add( Status = new XMStatus( this ) );
        }
    }
    public class XEEthernetPoENetworkIntf : XEProxy {
        public XMRouting Routing { get; private set; }
        public XMNetwork Network { get; private set; }
        public XMStatus Status { get; private set; }
        public XEEthernetPoENetworkIntf ( XCProxy aParent )
            : base( aParent, "10/100 Ethernet with PoE", "LAN" ) {
            // __TODO__ 1. use this to set the static/DNS/UPnP address mode and value.
            Maps.Add( Routing = new XMRouting( this ) );
            Maps.Add( Network = new XMNetwork( this ) );
            Maps.Add( Status = new XMStatus( this ) );
        }
    }
    public class XESerialRS232DCE : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMSerialPort SerialPort;
        public readonly XMRS232DCE RS232;
        public XESerialRS232DCE ( XCProxy aParent )
            : base( aParent, "RS232 DCE DB9-Female", "Serial Port" ) {
            Maps.Add( Status = new XMStatus( this ) );
            SerialPort = new XMSerialPort( this );
            RS232 = new XMRS232DCE( this );
            Maps.Add( SerialPort );
            Maps.Add( RS232 );
        }
    }
    public class XESerialRS232DTE : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMSerialPort SerialPort;
        public readonly XMRS232DTE RS232;
        public XESerialRS232DTE ( XCProxy aParent )
            : base( aParent, "RS232 DTE DB9-Male", "Serial Port" ) {
            Maps.Add( Status = new XMStatus( this ) );
            SerialPort = new XMSerialPort( this );
            RS232 = new XMRS232DTE( this );
            Maps.Add( SerialPort );
            Maps.Add( RS232 );
        }
    }
    public class XESerialMultiPhy : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMSerialPort SerialPort;
        public readonly XMRS232DCE RS232;
        public XESerialMultiPhy ( XCProxy aParent )
            : base( aParent, "RS232 DCE / DTE / RS485 / RS422", "Serial Port" ) {
            Maps.Add( Status = new XMStatus( this ) );
            SerialPort = new XMSerialPort( this );
            RS232 = new XMRS232DCE( this );
            Maps.Add( SerialPort );
            Maps.Add( RS232 );
        }
    }
    // proxy for Modbus serial card hardware, either RS232, RS485 or RS422 enabled, one at a time.
    public class XESerialModbusMultiPhy : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMSerialPort SerialPort;
        public readonly XMRS232DCE RS232;
        public readonly XMModbus Modbus;
        public XESerialModbusMultiPhy ( XCProxy aParent )
            : base( aParent, "Modbus RS232 DCE / DTE / RS485 / RS422", "Modbus" ) {
            Maps.Add( Status = new XMStatus( this ) );
            Modbus = new XMModbus( this );
            SerialPort = new XMSerialPort( this );
            RS232 = new XMRS232DCE( this );
            Maps.Add( SerialPort );
            Maps.Add( RS232 );
            Maps.Add( Modbus );
        }
    }

    // proxy for single digital input element.
    public class XEDigitalInput : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMDigitalInput DigitalInput;
        public XEDigitalInput ( XCProxy aParent )
            : base( aParent, "Digital Input", "" ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( DigitalInput = new XMDigitalInput( this ) );
        }
        public XEDigitalInput ( XCProxy aParent, string aRoleName )
            : base( aParent, "Digital Input" , aRoleName ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( DigitalInput = new XMDigitalInput( this ) );
        }
    }

    // proxy for single digital input element.
    public class XEDigitalOutput : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMDigitalOutput DigitalOutput;
        public XEDigitalOutput ( XCProxy aParent )
            : base( aParent, "Digital Output","" ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( DigitalOutput = new XMDigitalOutput( this ) );
        }
        public XEDigitalOutput ( XCProxy aParent, string aRoleName )
            : base( aParent, "Digital Output", aRoleName ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( DigitalOutput = new XMDigitalOutput( this ) );
        }
    }

    // proxy for single analog input element.
    public class XEAnalogInput : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMAnalogInput AnalogInput;
        public XEAnalogInput ( XCProxy aParent, double aRange, int aCalibration )
            : base( aParent, "Analog Input" ,"") {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( AnalogInput = new XMAnalogInput( this, aRange, aCalibration ) );
        }
        public XEAnalogInput ( XCProxy aParent, double aRange, int aCalibration, string aRoleName )
            : base( aParent, "Analog Input",  aRoleName ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( AnalogInput = new XMAnalogInput( this, aRange, aCalibration ) );
        }
    }

    // proxy for single analog input element.
    public class XEMonitoredInput : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMMonitoredInput MonitoredInput;
        public XEMonitoredInput ( XCProxy aParent, string aRoleName )
            : base( aParent, "Monitored Input", aRoleName ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( MonitoredInput = new XMMonitoredInput( this ) );
        }
        public XEMonitoredInput ( XCProxy aParent )
            : base( aParent, "Monitored Input","" ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( MonitoredInput = new XMMonitoredInput( this ) );
        }
    }

    // proxy for single analog input monitor element.
    // monitor keeps track of average and noise values.
    public class XEAnalogMonitor : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMAnalogMonitor AnalogMonitor;
        public XEAnalogMonitor ( XCProxy aParent )
            : base( aParent, "Analog Monitor","" ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( AnalogMonitor = new XMAnalogMonitor( this ) );
        }
    }

    // proxy for single analog input monitor element.
    // monitor keeps track of average and noise values.
    // simplest interface is as a digital active/inactive input.
    // filter-bank interface has much more information, but eats bandwidth.
    public class XEFibreSensor : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMDigitalInput DigitalInput;
        public readonly XMFibrePort FibrePort;
        public XEFibreSensor ( XCProxy aParent, string aRoleName )
            : base( aParent, "Fibre Sensor", aRoleName ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( DigitalInput = new XMDigitalInput( this ) );
            // fibre sensors default to 1000 millisecond period filtering.
            DigitalInput.LastActiveFilterPeriod = TimeSpan.FromMilliseconds( 1000 );
            Maps.Add( FibrePort = new XMFibrePort( this ) );
        }
        public XEFibreSensor ( XCProxy aParent )
            : base( aParent, "Fibre Sensor","" ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( DigitalInput = new XMDigitalInput( this ) );
            // fibre sensors default to 1000 millisecond period filtering.
            DigitalInput.LastActiveFilterPeriod = TimeSpan.FromMilliseconds( 1000 );
            Maps.Add( FibrePort = new XMFibrePort( this ) );
        }
    }


    // proxy for single relay output element.
    public class XERelayOutput : XEProxy {
        public XMStatus Status { get; private set; }
        public readonly XMRelayOutput RelayOutput;
        public XERelayOutput ( XCProxy aParent )
            : base( aParent, "Relay Output","" ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( RelayOutput = new XMRelayOutput( this ) );
        }
        public XERelayOutput ( XCProxy aParent, string aRoleName )
            : base( aParent, "Relay Output", aRoleName ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( RelayOutput = new XMRelayOutput( this ) );
        }
    }

    public class XEOnWireDevice : XEProxy {
        public bool Present = false;
        public XMStatus Status { get; private set; }
        public readonly XMOnWireDevice Device;
        public XEOnWireDevice ( XCProxy aParent )
            : base( aParent, "OnWire Device","" ) {
            Maps.Add( Status = new XMStatus( this ) );
            Maps.Add( Device = new XMOnWireDevice( this ) );
        }
    }

}

