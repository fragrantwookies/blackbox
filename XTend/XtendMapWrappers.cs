﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XPointAudiovisual.Core;
using XtendExtensionMethods;
using System.IO.Ports;
using System.Threading;
#endregion

namespace XPointAudiovisual.Xtend {

    // interfaces and enumerations giving access to supported functionality.
    // ---------------------------------------------------------------------------

    public struct XMError {
        public byte Code;
        public byte Action;
        public ushort Index;
        public ushort Offset;
        public byte[] Data;
        public string Message;
    }

    //// single-flag property access to basic (shared) map status conditions.
    //public interface IXMBase {
    //    bool MapIsActive { get; set; }
    //    // can only clear a map error, not set one.
    //    bool MapHasError { get; set; }
    //    bool MapIsLogging { get; set; }
    //    bool MapIsDebugging { get; set; }
    //    //XMError GetErrorInfo ();
    //}
    public interface IXMFlags<T> {
        List<T> GetStatusConditions ();
        bool GetIsStatusCondition ( T aCondition );
        void SetStatusConditions ( bool aState, List<T> anEventList );
        void SetIsStatusCondition ( bool aState, T aCondition );
        //List<T> GetNotifyingEvents ();
        //bool GetIsNotifyingEvent ( T anEvent );
        //void SetNotifyingEvents ( bool aState, List<T> anEventList );
        //void SetIsNotifyingEvent ( bool aState, T anEvent );
    }

    public enum EServiceConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        // rebooted condition is only actually used in admin element.
        Rebooted = 8,
        Enrolled,
    }
    public interface IXMService {
        void UpdateFirmware ( byte[] aMacAddress );
        void RequestStatusUpdate ();
    }
    public interface IXMStatistics {
    }
    public interface IXMLog {
    }
    public interface IXMRegistration {
        void Invite ( byte[] aMacAddress );
        void Enroll ();
    }
    public interface IXMRouting {
    }
    public interface IXMStatus {
    }

    public enum EDigitalInputConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        IsActive = 8,
        IsEnabled = 9,
    }
    public delegate void DigitalInputEventHandler ( object aSender, EDigitalInputConditions anEvent, bool aStatus );
    public interface IXMDigitalInput {
        XMFlags<EDigitalInputConditions> Flags { get; }
        bool IsActiveLow { get; set; }
        UInt16 DebouncePeriod { get; set; }
        bool IsEnabled { get; set; }
        bool IsActive { get; }
        DigitalInputEventHandler OnEvent { get; }
    }

    public enum EMonitoredInputConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        IsActive = 8,
        IsEnabled = 9,
    }
    public enum EMonitoredInputStates {
        Inactive = 0,
        Active,
        Disconnected,
        Fault
    }
    public delegate void MonitoredInputEventHandler ( object aSender, EMonitoredInputConditions anEvent, bool aStatus );
    public delegate void MonitoredInputStateChangeHandler ( object aSender, EMonitoredInputStates aState );
    public interface IXMMonitoredInput {
        XMFlags<EMonitoredInputConditions> Flags { get; }
        EMonitoredInputStates LogicalState { get; }
        MonitoredInputEventHandler OnEvent { get; }
    }

    public enum EDigitalOutputConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        IsActive = 8,
        IsEnabled = 9,
    }
    public delegate void DigitalOutputEventHandler ( object aSender, EDigitalOutputConditions anEvent, bool aStatus );
    public interface IXMDigitalOutput {
        XMFlags<EDigitalOutputConditions> Flags { get; }
        bool IsActiveLow { get; set; }
        bool IsEnabled { get; set; }
        bool IsActive { get; set; }
        DigitalOutputEventHandler OnEvent { get; }
    }

    public enum EAnalogInputConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        IsActive = 8,
        IsEnabled = 9,
    }
    public delegate void AnalogInputEventHandler ( object aSender, EAnalogInputConditions anEvent, bool aStatus );
    public interface IXMAnalogInput {
        XMFlags<EAnalogInputConditions> Flags { get; }
        byte FilterDepth { get; set; }
        float Hysteresis { get; set; }
        float Value { get; }
        bool IsActive { get; }
        bool IsEnabled { get; set; }
        double Range { get; }
        AnalogInputEventHandler OnEvent { get; }
    }

    public enum EAnalogMonitorConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        IsActive = 8,
        IsEnabled = 9,
    }
    public delegate void AnalogMonitorEventHandler ( object aSender, EAnalogMonitorConditions anEvent, bool aStatus );
    public interface IXMAnalogMonitor {
        XMFlags<EAnalogMonitorConditions> Flags { get; }
        bool IsActive { get; }
        bool IsEnabled { get; set; }
        double Range { get; }
        AnalogMonitorEventHandler OnEvent { get; }
    }

    public enum EFilterBankConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        IsActive = 8,
        IsEnabled = 9,
    }
    public delegate void FilterBankEventHandler ( object aSender, EFilterBankConditions anEvent, bool aStatus );
    public delegate void FilterBankChangeHandler ( object aSender, int aValue );
    public interface IXMFilterBank {
        XMFlags<EFilterBankConditions> Flags { get; }
        //bool IsEnabled { get; set; }
        void GetChannelState ( int aChannel, out int aNoiseLevel, out int aSignalLevel );
        FilterBankEventHandler OnEvent { get; set; }
        FilterBankChangeHandler OnSignalLevelChange { get; set; }
    }

    public enum EFibrePortConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        IsActive = 8,
        IsEnabled = 9,
    }
    public delegate void FibrePortEventHandler ( object aSender, EFibrePortConditions anEvent, bool aStatus );
    public delegate void FibrePortChangeHandler ( object aSender, int aValue );
    public interface IXMFibrePort {
        XMFlags<EFibrePortConditions> Flags { get; }
        FibrePortEventHandler OnEvent { get; set; }
        FibrePortChangeHandler OnSignalLevelChange { get; set; }
    }

    public enum ERelayOutputConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        IsActive = 8,
        IsEnabled = 9,
    }
    public delegate void RelayOutputEventHandler ( object aSender, ERelayOutputConditions anEvent, bool aStatus );
    public interface IXMRelayOutput {
        XMFlags<ERelayOutputConditions> Flags { get; }
        bool StartupActive { get; }
        UInt16 SettleActiveDelay { set; }
        UInt16 SettleInactiveDelay { set; }
        bool IsActive { get; set; }
        bool IsEnabled { get; set; }
        RelayOutputEventHandler OnEvent { get; }
    }

    // activities common to essentially all serial ports.
    public enum ESerialPortConditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        PortIsOpen = 8,
        AutobaudIsActive,
        BreakCharReceived,
        // port TX buffer status.
        TxBufferIsEmpty = 16,
        TxBufferIsBelowHalfFull,
        TxBufferIsFull,
        TxBufferHasOverflowed,
        // port RX buffer status.
        RxBufferIsEmpty = 24,
        RxBufferIsBelowHalfFull,
        RxBufferIsFull,
        RxBufferHasOverflowed,
    }

    public delegate void SerialReceiveDataHandler ( object aSender, byte[] aDataArray );
    public delegate void SerialErrorHandler ( object aSender, XMError anError );
    public delegate void SerialEventHandler ( object aSender, ESerialPortConditions anEvent, bool aStatus );

    public interface IXMSerialPort {
        XMFlags<ESerialPortConditions> Flags { get; }

        void SetConfiguration ( int aBaudrate, byte aDataBits, byte aStopBits, System.IO.Ports.Parity aParity );
        void GetConfiguration ( out int aBaudrate, out byte aDataBits, out byte aStopBits, out System.IO.Ports.Parity aParity );
        void ReplaceErrorChar ( bool aMustFlag, byte aValue );
        bool DiscardNull { get; set; }
        bool IsOpen { get; set; }

        void GetTransmitState ( out int aSize, out int aUsedCount, out int aSpaceCount );
        void TransmitData ( byte[] aDataArray );
        void TransmitData ( string aDataString );
        void GetReceiveState ( out int aSize, out int aUsedCount, out int aSpaceCount );
        // any threshold match will result in data being reported immediately.
        void ResetAllReceiveThresholds ();
        void SetReceiveCountThreshold ( int aCount );
        void SetReceiveTimeoutThreshold ( int aTimeout );
        void ResetReceiveMatchThresholds ();
        void SetReceiveMatchThreshold ( string aValue );
        void SetReceiveMatchThreshold ( byte[] aValue );

        // this is the revector for responders to unsolicited messages from the RIO stations.
        // the model is basically the same as the standard MSWindows OnEvent GUI model.
        // method delegates are given a callback address, and if not null, call that for handling.
        SerialReceiveDataHandler OnReceiveData { get; }
        SerialErrorHandler OnError { get; }
        SerialEventHandler OnEvent { get; }
    }

    // activities related to generic RS232 interfaces, ignoring gender differences.
    public enum ERS232Handshake { None = 0, XOnXOff, RtsCts, RtsCtsOld, Both }

    // numbered in same order as on a male 9-way D-type (PC/DTE).
    // but if sent to DCE, will move appropriate pins as for own format (RD/TD!)
    public enum ERS232Pins {
        DataCarrierDetect = 1,
        ReceiveData,
        TransmitData,
        DataTerminalReady,
        SignalGround,
        DataSetReady,
        RequestToSend,
        ClearToSend,
        RingIndicator,
    }

    // flags hold current state of port *specific*to* RS232 (vs serial)
    public enum ERS232Conditions {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // pin conditions.
        DataCarrierDetectActive = 8,
        ReceiveDataBusy,
        TransmitDataBusy,
        DataTerminalReadyActive,
        SignalGroundActive, // dummy, keeps linear sequence for pins/conditions.
        DataSetReadyActive,
        RequestToSendActive,
        ClearToSendActive,
        RingIndicatorActive,
        // timed disconnect signal.
        RemoteIsConnected = 24,
        PermanentBreak,
        FlowControlActive,
    }
    public delegate void RS232EventHandler ( object aSender, ERS232Conditions anEvent, bool aStatus );

    public interface IXMRS232 {
        XMFlags<ERS232Conditions> Flags { get; }
        bool GetPinIsActive ( ERS232Pins aPin );
        void SetPinActive ( ERS232Pins aPin );
        void SetPinInactive ( ERS232Pins aPin );
        void SetPinState ( bool aState, ERS232Pins aPin );
        void SetHandshakeMode ( ERS232Handshake aMode );
        bool PassiveReceiveOnly { get; set; }
        RS232EventHandler OnEvent { get; }
    }





    // class implementations of preceding interfaces.
    // --------------------------------------------------

    //public class XMBase : IXMBase, IDisposable {
    public class XMBase : IDisposable {
        public readonly EXMapType MapType;
        public readonly XEProxy Parent;
        public List<XField> Fields { get; protected set; }

        public const int ResCardOfs = 0;
        public const int ResElemOfs = 1;
        public const int ResMapOfs = 2;
        public const int ResFieldOfs = 3;
        public const int ResModeOfs = 4;
        public const int ResValOfs = 5;

        // handles the correct dispatching of the message once constructed.
        // check for possible error return and raise an exception if needed.
        // do it here and not in the individual wrappers, because of the factorization.
        // caller (wrapper code) must trap if semantically necessary, otherwise let exception go.
        // but note that a given message might not required a response.
        // __TODO__ 8. see if this could be encoded in the field definitions directly.
        // __TODO__ 4. check this with actual forced errors.
        protected List<byte[]> Send ( List<byte[]> aCmdList ) {
            aCmdList.Insert( 0, new byte[] { (byte)MapType } );
            List<byte[]> response = Parent.Send( aCmdList );
            if ( response != null ) {
                // order of message bytes is card/element/map/field/mode/args.
                for ( int i = 0; i < response.Count; ++i )
                    if ( response[ i ][ ResModeOfs ] == 0xFF )
                        throw new XBadMessageException(
                            "argument " + i.ToString() + " " + XFieldAccessErrorStrings.Get( response[ i ][ ResValOfs ] ) );
            }
            return response;
        }
        // for simple single-command sends, do the required list creation here.
        protected List<byte[]> Send ( byte[] aCmdList ) { return Send( new List<byte[]> { aCmdList } ); }

        // field address for map status is always the same, no matter what map is queried!
        // generic status bits have been assigned the lower 8-bit range of all map status words.
        // note that int-based flag reads and writes are not limited to any specific group of status enumerations.
        // by converting status/event enumerations to bit-flags, this code handles all status types.
        public int ReadMapStatusFlags () {
            return BitConverter.ToInt32( Send( __XMapBase.MapStatusFlagsGet.Read() )[ 0 ], ResValOfs );
        }
        public void WriteMaskedMapStatusFlags ( bool aState, int aFlagsMask ) {
            Send( ( aState ? __XMapBase.MapStatusFlagsSet : __XMapBase.MapStatusFlagsClear ).Write( aFlagsMask ) );
        }
        // converting between flags and enumerated conditions, to be inherited.
        public List<T0> ConditionsListFromMask<T0> ( int flags, Func<int, T0> aCnvFunc ) {
            var events = new List<T0>();
            for ( int i = 0; ( i <= 31 ) && ( flags != 0 ); ++i, flags >>= 1 )
                if ( ( flags & 1 ) != 0 )
                    events.Add( aCnvFunc( i ) );
            return events;
        }
        public int MaskFromConditionsList<T0> ( List<T0> aConditionsList, Func<T0, int> aCnvFunc ) {
            int mask = 0;
            foreach ( var e in aConditionsList )
                mask |= ( 1 << aCnvFunc( e ) );
            return mask;
        }

        // conversions from generic EMapStatusConditions to int-based.
        // these are more efficient functions that avoid the list-based manipulations.
        private void WriteSingleMapStatusFlag ( bool aState, EMapStatusConditions aCondition ) {
            WriteMaskedMapStatusFlags( aState, 1 << (int)aCondition );
        }
        private bool ReadSingleMapStatusFlag ( EMapStatusConditions aCondition ) {
            return ( ReadMapStatusFlags() & ( 1 << (int)EMapStatusConditions.MapIsActive ) ) != 0;
        }

        // single-flag property access to basic (shared) map status conditions.
        public bool MapIsActive {
            set { WriteSingleMapStatusFlag( value, EMapStatusConditions.MapIsActive ); }
            get { return ReadSingleMapStatusFlag( EMapStatusConditions.MapIsActive ); }
        }
        // can only clear a map error, not set one.
        public bool MapHasError {
            set { if ( !value ) WriteSingleMapStatusFlag( value, EMapStatusConditions.MapHasError ); }
            get { return ReadSingleMapStatusFlag( EMapStatusConditions.MapHasError ); }
        }
        public bool MapIsLogging {
            set { WriteSingleMapStatusFlag( value, EMapStatusConditions.MapIsLogging ); }
            get { return ReadSingleMapStatusFlag( EMapStatusConditions.MapIsLogging ); }
        }
        public bool MapIsDebugging {
            set { WriteSingleMapStatusFlag( value, EMapStatusConditions.MapIsDebugging ); }
            get { return ReadSingleMapStatusFlag( EMapStatusConditions.MapIsDebugging ); }
        }

        public virtual void MapResponder ( byte aField, byte[] anArgsArray ) { }

        public override string ToString () {
            var s = new StringBuilder();
            s.Append( "  Map -- " );
            s.Append( (int)MapType );
            s.Append( " -- " );
            s.Append( TypeName );
            return s.ToString();
        }
        protected string TypeName = "Unknown";
        public XMBase ( XEProxy aParent, EXMapType aMapType ) {
            Parent = aParent;
            MapType = aMapType;
        }
        public XMBase ( XEProxy aParent, EXMapType aMapType, string aTypeName ) {
            Parent = aParent;
            MapType = aMapType;
            TypeName = aTypeName;
        }
        public XMBase ( XEProxy aParent, EXMapType aMapType, string aTypeName, string aRoleName ) {
            Parent = aParent;
            MapType = aMapType;
            TypeName = aTypeName;
        }
        protected virtual void Dispose ( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
            if ( Fields != null ) {
                foreach ( var f in Fields )
                    f.Dispose();
                Fields.Clear();
                Fields = null;
            }
        }
        ~XMBase () {
            Dispose( false );
        }
        public void Dispose () {
            Dispose( true );
        }
    }

    // use generics to avoid rewriting tons of flag handling code with every new map.
    // now we just create a Flags<Type> object + some extras with every map, and it's all handled in here.
    public class XMFlags<T1> : IXMFlags<T1> {
        XMBase container;
        Func<int, T1> fromIntConv;
        Func<T1, int> toIntConv;

        public List<T1> GetStatusConditions () {
            return container.ConditionsListFromMask<T1>( container.ReadMapStatusFlags(), fromIntConv );
        }
        public bool GetIsStatusCondition ( T1 aCondition ) {
            return ( container.ReadMapStatusFlags() & ( 1 << toIntConv( aCondition ) ) ) != 0;
        }
        public void SetStatusConditions ( bool aState, List<T1> anEventList ) {
            container.WriteMaskedMapStatusFlags( aState, container.MaskFromConditionsList<T1>( anEventList, toIntConv ) );
        }
        public void SetIsStatusCondition ( bool aState, T1 aCondition ) {
            container.WriteMaskedMapStatusFlags( aState, ( 1 << toIntConv( aCondition ) ) );
        }

        //public List<T1> GetNotifyingEvents () {
        //    return container.ConditionsListFromMask<T1>( container.ReadNotifierFlags(), fromIntConv );
        //}
        //public bool GetIsNotifyingEvent ( T1 anEvent ) {
        //    return ( container.ReadNotifierFlags() & ( 1 << toIntConv( anEvent ) ) ) != 0;
        //}
        //public void SetNotifyingEvents ( bool aState, List<T1> anEventList ) {
        //    container.WriteMaskedNotifierFlags( aState, container.MaskFromConditionsList<T1>( anEventList, toIntConv ) );
        //}
        //public void SetIsNotifyingEvent ( bool aState, T1 anEvent ) {
        //    container.WriteMaskedNotifierFlags( aState, ( 1 << toIntConv( anEvent ) ) );
        //}

        public XMFlags ( XMBase aContainer, Func<int, T1> aFromIntConv, Func<T1, int> aToIntConv ) {
            this.container = aContainer;
            fromIntConv = aFromIntConv;
            toIntConv = aToIntConv;
        }
    }


    public class XMService : XMBase, IXMService {
        public void UpdateFirmware ( byte[] aMacAddress ) {
            Send( XMapService.UpdateFirmware.Write( aMacAddress ) );
        }
        public void Ping () { Send( XMapService.Ping.Write() ); }
        public void RequestStatusUpdate () { Send( XMapService.StatusUpdateRequest.Write() ); }
        public XMService ( XEProxy aParent )
            : base( aParent, EXMapType.Service, "Service" ) {
            Fields = XMapService.GetClonedFields();
        }
    }

    public class XMStatistics : XMBase, IXMStatistics {
        public XMStatistics ( XEProxy aParent )
            : base( aParent, EXMapType.Statistics, "Statistics" ) {
            Fields = XMapStatistics.GetClonedFields();
        }
    }

    public class XMLog : XMBase, IXMLog {
        public XMLog ( XEProxy aParent )
            : base( aParent, EXMapType.Log, "Log" ) {
            Fields = XMapLog.GetClonedFields();
        }
    }

    public class XMRegistration : XMBase, IXMRegistration {
        public void Invite ( byte[] aMacAddress ) { Send( XMapRegistration.Invite.Write( aMacAddress ) ); }
        public void Enroll () { Send( XMapRegistration.Enroll.Write() ); }
        public XMRegistration ( XEProxy aParent )
            : base( aParent, EXMapType.Registration, "Registration" ) {
            Fields = XMapRegistration.GetClonedFields();
        }
    }

    public class XMRouting : XMBase, IXMRouting {
        public XMRouting ( XEProxy aParent )
            : base( aParent, EXMapType.Routing, "Routing" ) {
            Fields = XMapRouting.GetClonedFields();
        }
    }

    public class XMStatus : XMBase, IXMStatus {
        public XMStatus ( XEProxy aParent )
            : base( aParent, EXMapType.Status, "Status" ) {
            Fields = XMapStatus.GetClonedFields();
        }
        public XMError GetErrorInfo () {
            var cmdList = new List<byte[]> {
                XMapStatus.ErrorCode.Read(),
                XMapStatus.ErrorAction.Read(),
                XMapStatus.ErrorIndex.Read(), 
                XMapStatus.ErrorOffset.Read(),
                XMapStatus.ErrorData.Read(),
                //XMapStatus.ErrorMessage.Read(),
            };
            // comes back as a list of bytes to allow removal of prefixes.
            // only item remaining by here should be the returned data.
            var ans = Send( cmdList );
            var res = new XMError();
            res.Code = ans[ 0 ][ ResValOfs ];
            res.Action = ans[ 1 ][ ResValOfs ];
            res.Index = BitConverter.ToUInt16( ans[ 2 ], ResValOfs );
            res.Offset = BitConverter.ToUInt16( ans[ 3 ], ResValOfs );

            ushort dataLen = BitConverter.ToUInt16( ans[ 4 ], ResValOfs );
            res.Data = ans[ 4 ].Slice( ResValOfs + 2, dataLen );

            //ushort messageOfs = (ushort)( ResValOfs + dataLen + 6 + 2 );
            //ushort messageLen = BitConverter.ToUInt16( ret, messageOfs );
            //res.Message = BinEnc.Get.GetString( ret, ResValOfs + messageOfs + 2, messageLen );

            return res;
        }
        // enable or disable event notifications for this remote element map.
        public int ReadNotifierFlags () {
            return BitConverter.ToInt32( Send( XMapStatus.EventNotifyFlagsGet.Read() )[ 0 ], ResValOfs );
        }
        public void WriteMaskedNotifierFlags ( bool aState, int aFlagsMask ) {
            Send( ( aState ? XMapStatus.EventNotifyFlagsSet : XMapStatus.EventNotifyFlagsClear )
                .Write( aFlagsMask ) );
        }

    }

    public class XMNetwork : XMBase, IXMRouting {
        public XMNetwork ( XEProxy aParent )
            : base( aParent, EXMapType.Network, "Network" ) {
            Fields = XMapNetwork.GetClonedFields();
        }
    }


    // __TODO__ 0. permanent settings?
    public class XMConfiguration : XMBase {
        public XMConfiguration ( XEProxy aParent )
            : base( aParent, EXMapType.Configuration, "Configuration" ) {
            Fields = XMapConfiguration.GetClonedFields();
        }
        public void CommitChanges () {
            Send( XMapConfiguration.CommitChanges.Write() );
        }
        public void UndoChanges () {
            Send( XMapConfiguration.UndoChanges.Write() );
        }
        public void LoadDefaults () {
            Send( XMapConfiguration.LoadDefaults.Write() );
        }
        public byte[] GetCurrentSettings () {
            return Send( XMapConfiguration.CurrentSettings.Read() )[ 0 ];
        }
    }

    public class XMCalibration : XMBase {
        public XMCalibration ( XEProxy aParent )
            : base( aParent, EXMapType.Calibration, "Calibration" ) {
            Fields = XMapCalibration.GetClonedFields();
        }
        // running a calibration in an uncalibrated environment will probably disable the card.
        // therefore we protect the call to perform calibration by pre-authorizing.
        private bool isAuthorizedCalibration = false;
        public void AuthorizeFactoryCalibration () {
            isAuthorizedCalibration = true;
        }
        public void Calibrate () {
            if ( !isAuthorizedCalibration )
                throw new XException( "invalid request for calibration, factory use only" );
            isAuthorizedCalibration = false;
            Send( XMapCalibration.Calibrate.Write() );
        }
    }


    public class XMDigitalInput : XMBase, IXMDigitalInput {
        private XMFlags<EDigitalInputConditions> __Flags;
        public XMFlags<EDigitalInputConditions> Flags { get { return __Flags; } }

        public bool IsActiveLow {
            get { return Send( XMapDigitalInput.ActiveLow.Read() )[ 0 ][ ResValOfs ] != 0; }
            set { Send( XMapDigitalInput.ActiveLow.Write( value ) ); }
        }
        public UInt16 DebouncePeriod {
            get { return BitConverter.ToUInt16( Send( XMapDigitalInput.DebouncePeriod.Read() )[ 1 ], ResValOfs ); }
            set { Send( XMapDigitalInput.DebouncePeriod.Write( value ) ); }
        }
        public bool IsActive {
            get { return Flags.GetIsStatusCondition( EDigitalInputConditions.IsActive ); }
        }
        public bool IsEnabled {
            get { return Flags.GetIsStatusCondition( EDigitalInputConditions.IsEnabled ); }
            set { Flags.SetIsStatusCondition( value, EDigitalInputConditions.IsEnabled ); }
        }

        // handler delegate type and instance for incoming digital input events.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public DigitalInputEventHandler OnEvent { get; set; }
        // time based filtering of events for designating container elements.
        // __WARN__ this does not currently distinguish between active and inactive events!
        private DateTime LastActiveFilterEvent = DateTime.Now;
        internal TimeSpan LastActiveFilterPeriod = TimeSpan.FromMilliseconds( 0 );

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            if ( OnEvent == null )
                return;
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
            // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
            // can we do this on a helper thread that queues these events?
            if ( aField == XMapDigitalInput.IsActive.Address ) {
                if ( LastActiveFilterPeriod.Milliseconds == 0 ) {
                    OnEvent( this, EDigitalInputConditions.IsActive, anArgsArray[ 0 ] != 0 );
                } else {
                    if (( DateTime.Now - LastActiveFilterEvent ).Milliseconds > LastActiveFilterPeriod.Milliseconds )
                        OnEvent( this, EDigitalInputConditions.IsActive, anArgsArray[ 0 ] != 0 );
                    LastActiveFilterEvent = DateTime.Now;
                }
            } else if ( aField == XMapDigitalInput.EventNotification.Address ) {
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<EDigitalInputConditions> conditions = ConditionsListFromMask( eventMask, i => (EDigitalInputConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            }
        }
        public XMDigitalInput ( XEProxy aParent )
            : base( aParent, EXMapType.DigitalInput, "Digital Input" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<EDigitalInputConditions>( this, ( i ) => (EDigitalInputConditions)i, ( c ) => (int)c );
            Fields = XMapDigitalInput.GetClonedFields();
        }
        public XMDigitalInput ( XEProxy aParent, string aRoleName )
            : base( aParent, EXMapType.DigitalInput, "Digital Input", aRoleName ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<EDigitalInputConditions>( this, ( i ) => (EDigitalInputConditions)i, ( c ) => (int)c );
            Fields = XMapDigitalInput.GetClonedFields();
        }
    }

    //public delegate void AnalogInputChangeHandler ( object aSender, int aValue );
    public delegate void AnalogInputChangeHandler ( object aSender, float aValue );

    public class XMAnalogInput : XMBase, IXMAnalogInput {
        private XMFlags<EAnalogInputConditions> __Flags;
        public XMFlags<EAnalogInputConditions> Flags { get { return __Flags; } }

        public double Range { get; private set; }
        public int Calibration { get; private set; }

        public byte FilterDepth {
            get { return Send( XMapAnalogInput.FilterDepth.Read() )[ 0 ][ ResValOfs ]; }
            set { Send( XMapAnalogInput.FilterDepth.Write( value ) ); }
        }
        public float Hysteresis {
            get { return BitConverter.ToSingle( Send( XMapAnalogInput.Hysteresis.Read() )[ 1 ], ResValOfs ); }
            set { Send( XMapAnalogInput.Hysteresis.Write( value ) ); }
        }
        public float Value {
            get { return BitConverter.ToSingle( Send( XMapAnalogInput.Value.Read() )[ 1 ], ResValOfs ); }
            set { Send( XMapAnalogInput.Value.Write( value ) ); }
        }
        public bool IsActive {
            get { return Flags.GetIsStatusCondition( EAnalogInputConditions.IsActive ); }
        }
        public bool IsEnabled {
            get { return Flags.GetIsStatusCondition( EAnalogInputConditions.IsEnabled ); }
            set { Flags.SetIsStatusCondition( value, EAnalogInputConditions.IsEnabled ); }
        }

        // handler delegate type and instance for incoming digital input events.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public AnalogInputEventHandler OnEvent { get; set; }
        public AnalogInputChangeHandler OnChange { get; set; }

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == XMapAnalogInput.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
                // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<EAnalogInputConditions> conditions = ConditionsListFromMask( eventMask, i => (EAnalogInputConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
                //} else if ( ( aField == XMapAnalogInput.Value.Address ) ) {
                //    byte[] slice = anArgsArray.Slice( 0, 4 );
                //    float val = BitConverter.ToSingle( slice, 0 );
            } else if ( ( aField == XMapAnalogInput.Value.Address ) && ( OnChange != null ) ) {
                byte[] slice = anArgsArray.Slice( 0, 4 );
                float val = BitConverter.ToSingle( slice, 0 );
                try { OnChange( this, val ); }
                catch { }
            }
        }
        public XMAnalogInput ( XEProxy aParent, double aRange, int aCalibration )
            : base( aParent, EXMapType.AnalogInput, "Analog Input" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<EAnalogInputConditions>( this, ( i ) => (EAnalogInputConditions)i, ( c ) => (int)c );
            Range = aRange;
            Calibration = aCalibration;
            Fields = XMapAnalogInput.GetClonedFields();
        }
    }

    // __TODO__ 0. bit of a kludge here, assumes 25000 ticks per millisec, because MSP430s running at 25MHz.
    public struct __XtendTimestampA {
        public UInt64 Timestamp;
        public __XtendTimestampA ( byte[] aByteArray ) {
            Timestamp = 0;
            for ( int i = 7; i >= 2; --i )
                Timestamp = ( Timestamp << 8 ) + aByteArray[ i ];
            Timestamp *= 1000;
            int ShortTicks = ( (int)aByteArray[ 1 ] << 8 ) + aByteArray[ 0 ];
            Timestamp += (UInt64)( ShortTicks * 1024 / 25000 );
        }
    }
    public struct AnalogMonitorReading {
        public int ChannelNumber;
        byte[] Bytes;
        public UInt16 Value;
        public UInt64 Timestamp;
        public void Process () {
            Timestamp = new __XtendTimestampA( Bytes ).Timestamp;
            int val = ( (int)( Bytes[ 9 ] ) << 8 ) + (int)Bytes[ 8 ];
            Value = (UInt16)val;
        }
        public AnalogMonitorReading ( int aChannel, byte[] aByteArray ) {
            ChannelNumber = aChannel;
            Bytes = aByteArray;
            Value = 0;
            Timestamp = 0;
        }
    }
    public struct AnalogMonitorState {
        public int ChannelNumber;
        byte[] Bytes;
        public UInt32 CurrentValue;
        public UInt32 LatchedValue;
        public UInt32 DebounceValue;
        public UInt32 AverageValue;
        public UInt32 NoiseLevel;
        public UInt64 Timestamp;
        private UInt32 GetUInt32 ( int anOffset ) {
            UInt32 result = 0;
            for ( int i = 3; i >= 0; --i )
                result = ( result << 8 ) + Bytes[ anOffset + i ];
            return result;
        }
        public void Process () {
            Timestamp = new __XtendTimestampA( Bytes ).Timestamp;
            CurrentValue = GetUInt32( 8 );
            LatchedValue = GetUInt32( 12 );
            DebounceValue = GetUInt32( 16 );
            AverageValue = GetUInt32( 20 );
            NoiseLevel = GetUInt32( 24 );
        }
        public AnalogMonitorState ( int aChannel, byte[] anArgsArray ) {
            ChannelNumber = aChannel;
            Bytes = anArgsArray;
            CurrentValue = 0;
            LatchedValue = 0;
            DebounceValue = 0;
            AverageValue = 0;
            NoiseLevel = 0;
            Timestamp = 0;
            Process();
        }
    }

    public struct AnalogMonitorDisturbance {
        public int ChannelNumber;
        byte[] Bytes;
        public UInt32 Tracking;
        public UInt32 Average;
        public UInt32 NoiseLevel;
        public UInt64 Timestamp;
        private UInt32 GetUInt32 ( int anOffset ) {
            UInt32 result = 0;
            for ( int i = 3; i >= 0; --i )
                result = ( result << 8 ) + Bytes[ anOffset + i ];
            return result;
        }
        public void Process () {
            Timestamp = new __XtendTimestampA( Bytes ).Timestamp;
            Average = GetUInt32( 8 );
            NoiseLevel = GetUInt32( 12 );
            Tracking = GetUInt32( 16 );
        }
        public AnalogMonitorDisturbance ( int aChannel, byte[] anArgsArray ) {
            ChannelNumber = aChannel;
            Bytes = anArgsArray;
            Average = 0;
            NoiseLevel = 0;
            Tracking = 0;
            Timestamp = 0;
        }
    }
    public delegate void AnalogMonitorReadingsUpdateHandler ( object aSender, AnalogMonitorReading aReadings );
    public delegate void AnalogMonitorStateUpdateHandler ( object aSender, AnalogMonitorState aState );
    public delegate void AnalogMonitorDisturbanceUpdateHandler ( object aSender, AnalogMonitorDisturbance aDisturbance );

    public class XMAnalogMonitor : XMBase, IXMAnalogMonitor {
        private XMFlags<EAnalogMonitorConditions> __Flags;
        public XMFlags<EAnalogMonitorConditions> Flags { get { return __Flags; } }

        public double Range { get; private set; }
        public int Calibration { get; private set; }

        public void SetConfiguration ( bool anActiveLowFlag, int aDebouncePeriod ) {
            var cmdList = new List<byte[]> {
                XMapDigitalInput.ActiveLow.Write(anActiveLowFlag),
                XMapDigitalInput.DebouncePeriod.Write(aDebouncePeriod),
            };
            var ret = Send( cmdList );
        }
        public void GetConfiguration ( out bool anActiveLowFlag, out int aDebouncePeriod ) {
            var cmdList = new List<byte[]> {
                XMapDigitalInput.ActiveLow.Read(),
                XMapDigitalInput.DebouncePeriod.Read(),
            };
            var ret = Send( cmdList );
            anActiveLowFlag = ret[ 0 ][ ResValOfs ] != 0;
            aDebouncePeriod = BitConverter.ToUInt16( ret[ 1 ], ResValOfs );
        }
        public bool IsActive {
            get { return Flags.GetIsStatusCondition( EAnalogMonitorConditions.IsActive ); }
        }
        public bool IsEnabled {
            get { return Flags.GetIsStatusCondition( EAnalogMonitorConditions.IsEnabled ); }
            set { Flags.SetIsStatusCondition( value, EAnalogMonitorConditions.IsEnabled ); }
        }

        // handler delegate type and instance for incoming digital input events.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public AnalogMonitorEventHandler OnEvent { get; set; }
        public AnalogMonitorReadingsUpdateHandler OnReadingsUpdate { get; set; }
        public AnalogMonitorStateUpdateHandler OnFilteredStateUpdate { get; set; }
        public AnalogMonitorDisturbanceUpdateHandler OnDisturbance { get; set; }

        int[] prevIntegratedVals = new int[ 4 ];
        int[] prevFilteredVals = new int[ 20 ];

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == XMapAnalogMonitor.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
                // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<EAnalogMonitorConditions> conditions = ConditionsListFromMask( eventMask, i => (EAnalogMonitorConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            } else if ( ( aField == XMapAnalogMonitor.OversampledValue.Address ) && ( OnReadingsUpdate != null ) ) {
                var readings = new AnalogMonitorReading( this.Parent.Index, anArgsArray );
                try { OnReadingsUpdate( this, readings ); }
                catch { }
            } else if ( ( aField == XMapAnalogMonitor.SlowFilteredValue.Address ) && ( OnFilteredStateUpdate != null ) ) {
                var filtered = new AnalogMonitorState( this.Parent.Index, anArgsArray );
                try { OnFilteredStateUpdate( this, filtered ); }
                catch { }
            } else if ( ( aField == XMapAnalogMonitor.DisturbanceEvent.Address ) && ( OnDisturbance != null ) ) {
                var dist = new AnalogMonitorDisturbance( this.Parent.Index, anArgsArray );
                try { OnDisturbance( this, dist ); }
                catch { }
            }
        }
        public XMAnalogMonitor ( XEProxy aParent )
            : base( aParent, EXMapType.AnalogMonitor, "Analog Monitor" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<EAnalogMonitorConditions>( this, ( i ) => (EAnalogMonitorConditions)i, ( c ) => (int)c );
            Fields = XMapAnalogMonitor.GetClonedFields();
        }
    }










    public class XMMonitoredInput : XMBase, IXMMonitoredInput {
        private XMFlags<EMonitoredInputConditions> __Flags;
        public XMFlags<EMonitoredInputConditions> Flags { get { return __Flags; } }

        public EMonitoredInputStates LogicalState {
            get { return (EMonitoredInputStates)( Send( XMapMonitoredInput.LogicalState.Read() )[ 0 ][ ResValOfs ] ); }
        }

        // handler delegate type and instance for incoming digital input events.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public MonitoredInputEventHandler OnEvent { get; set; }
        public MonitoredInputStateChangeHandler OnStateChange { get; set; }

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == XMapMonitoredInput.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
                // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                // can we do this on a helper thread that queues these events?
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<EMonitoredInputConditions> conditions = ConditionsListFromMask( eventMask, i => (EMonitoredInputConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            } else if ( ( aField == XMapMonitoredInput.LogicalState.Address ) && ( OnStateChange != null ) ) {
                try { OnStateChange( this, (EMonitoredInputStates)( anArgsArray[ 0 ] ) ); }
                catch { }
            }
        }
        public XMMonitoredInput ( XEProxy aParent )
            : base( aParent, EXMapType.MonitoredInput, "Monitored Input" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<EMonitoredInputConditions>( this, ( i ) => (EMonitoredInputConditions)i, ( c ) => (int)c );
            Fields = XMapMonitoredInput.GetClonedFields();
        }
    }














    public class XMDigitalOutput : XMBase, IXMDigitalOutput {
        private XMFlags<EDigitalOutputConditions> __Flags;
        public XMFlags<EDigitalOutputConditions> Flags { get { return __Flags; } }

        public bool IsActiveLow {
            get { return Send( XMapDigitalOutput.ActiveLow.Read() )[ 0 ][ ResValOfs ] != 0; }
            set { Send( XMapDigitalOutput.ActiveLow.Write( value ) ); }
        }

        public bool IsActive {
            get { return Flags.GetIsStatusCondition( EDigitalOutputConditions.IsActive ); }
            set { Flags.SetIsStatusCondition( value, EDigitalOutputConditions.IsActive ); }
        }
        public bool IsEnabled {
            get { return Flags.GetIsStatusCondition( EDigitalOutputConditions.IsEnabled ); }
            set { Flags.SetIsStatusCondition( value, EDigitalOutputConditions.IsEnabled ); }
        }

        // handler delegate type and instance for incoming digital input events.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public DigitalOutputEventHandler OnEvent { get; set; }

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == XMapDigitalOutput.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
                // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<EDigitalOutputConditions> conditions = ConditionsListFromMask( eventMask, i => (EDigitalOutputConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            }
            // if ( ( aField == XMapDigitalOutput.IsActive.Address ) && ( OnEvent != null ) ) {
            //    OnEvent( this, EDigitalOutputConditions.IsActive, anArgsArray[0] != 0 );
            //}
        }
        public XMDigitalOutput ( XEProxy aParent )
            : base( aParent, EXMapType.DigitalOutput, "Digital Output" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<EDigitalOutputConditions>( this, ( i ) => (EDigitalOutputConditions)i, ( c ) => (int)c );
            Fields = XMapDigitalOutput.GetClonedFields();
        }
    }

    public class XMRelayOutput : XMBase, IXMRelayOutput {
        private XMFlags<ERelayOutputConditions> __Flags;
        public XMFlags<ERelayOutputConditions> Flags { get { return __Flags; } }

        public UInt16 SettleActiveDelay {
            get { return BitConverter.ToUInt16( Send( XMapRelayOutput.SettleActiveDelay.Read() )[ 1 ], ResValOfs ); }
            set { Send( XMapRelayOutput.SettleActiveDelay.Write( value ) ); }
        }
        public UInt16 SettleInactiveDelay {
            get { return BitConverter.ToUInt16( Send( XMapRelayOutput.SettleInactiveDelay.Read() )[ 1 ], ResValOfs ); }
            set { Send( XMapRelayOutput.SettleInactiveDelay.Write( value ) ); }
        }
        public bool StartupActive {
            get { return Send( XMapRelayOutput.StartupActive.Read() )[ 0 ][ ResValOfs ] != 0; }
            set { Send( XMapRelayOutput.StartupActive.Write( value ) ); }
        }
        public bool IsActive {
            get { return Flags.GetIsStatusCondition( ERelayOutputConditions.IsActive ); }
            set { Flags.SetIsStatusCondition( value, ERelayOutputConditions.IsActive ); }
        }
        public bool IsEnabled {
            get { return Flags.GetIsStatusCondition( ERelayOutputConditions.IsEnabled ); }
            set { Flags.SetIsStatusCondition( value, ERelayOutputConditions.IsEnabled ); }
        }

        // handler delegate type and instance for incoming digital input events.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public RelayOutputEventHandler OnEvent { get; set; }

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == XMapRelayOutput.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
                // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<ERelayOutputConditions> conditions = ConditionsListFromMask( eventMask, i => (ERelayOutputConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            }
        }
        public XMRelayOutput ( XEProxy aParent )
            : base( aParent, EXMapType.RelayOutput, "Relay Output" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<ERelayOutputConditions>( this, ( i ) => (ERelayOutputConditions)i, ( c ) => (int)c );
            Fields = XMapRelayOutput.GetClonedFields();
        }
    }

    // __TODO__ fix this - not an analogue.
    public delegate void OnWireDeviceChangeHandler ( object aSender, int aValue );

    public class XMOnWireDevice : XMBase, IXMAnalogInput {
        private XMFlags<EAnalogInputConditions> __Flags;
        public XMFlags<EAnalogInputConditions> Flags { get { return __Flags; } }

        public double Range { get; private set; }
        public int Calibration { get; private set; }

        public byte FilterDepth {
            get { return Send( XMapAnalogInput.FilterDepth.Read() )[ 0 ][ ResValOfs ]; }
            set { Send( XMapAnalogInput.FilterDepth.Write( value ) ); }
        }
        public float Hysteresis {
            get { return BitConverter.ToSingle( Send( XMapAnalogInput.Hysteresis.Read() )[ 1 ], ResValOfs ); }
            set { Send( XMapAnalogInput.Hysteresis.Write( value ) ); }
        }
        public float Value {
            get { return BitConverter.ToSingle( Send( XMapAnalogInput.Value.Read() )[ 1 ], ResValOfs ); }
            set { Send( XMapAnalogInput.Value.Write( value ) ); }
        }
        public bool IsActive {
            get { return Flags.GetIsStatusCondition( EAnalogInputConditions.IsActive ); }
        }
        public bool IsEnabled {
            get { return Flags.GetIsStatusCondition( EAnalogInputConditions.IsEnabled ); }
            set { Flags.SetIsStatusCondition( value, EAnalogInputConditions.IsEnabled ); }
        }

        // handler delegate type and instance for incoming digital input events.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public AnalogInputEventHandler OnEvent { get; set; }
        public AnalogInputChangeHandler OnChange { get; set; }

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == XMapAnalogInput.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
                // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<EAnalogInputConditions> conditions = ConditionsListFromMask( eventMask, i => (EAnalogInputConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            } else if ( ( aField == XMapAnalogInput.Value.Address ) && ( OnChange != null ) ) {
                int val = ( anArgsArray[ 1 ] << 8 ) + anArgsArray[ 0 ];
                try { OnChange( this, val ); }
                catch { }
            }
        }
        public XMOnWireDevice ( XEProxy aParent )
            : base( aParent, EXMapType.OnWireDevice, "OnWire Device" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<EAnalogInputConditions>( this, ( i ) => (EAnalogInputConditions)i, ( c ) => (int)c );
            Range = 0;
            Calibration = 0;
            Fields = XMapAnalogInput.GetClonedFields();
        }
    }

    public class XMFilterBank : XMBase, IXMFilterBank {
        private XMFlags<EFilterBankConditions> __Flags;
        public XMFlags<EFilterBankConditions> Flags { get { return __Flags; } }

        // __NOTE__ setting sensitivity to 0 disables the channel!
        public void SetConfiguration ( int aChannel, int aSensitivity ) { }
        public void GetConfiguration ( int aChannel, out int aSensitivityLevel ) {
            aSensitivityLevel = 0;
        }
        public int GetChannelCount () {
            return Send( XMapFilterBank.ChannelCount.Read() )[ 0 ][ ResValOfs ];
        }
        public int GetSamplingFrequency () {
            return Send( XMapFilterBank.SamplingFrequency.Read() )[ 0 ][ ResValOfs ];
        }
        public void GetChannelState ( int aChannel, out int aNoiseLevel, out int aSignalLevel ) {
            var res = Send( XMapFilterBank.SamplingFrequency.Read() )[ 0 ][ ResValOfs ];
            aNoiseLevel = aSignalLevel = 0;
        }

        // handler delegate type and instance for incoming digital input events.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public FilterBankEventHandler OnEvent { get; set; }
        public FilterBankChangeHandler OnSignalLevelChange { get; set; }

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == XMapFilterBank.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
                // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<EFilterBankConditions> conditions = ConditionsListFromMask( eventMask, i => (EFilterBankConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            } else if ( ( aField == XMapFilterBank.ChannelSignalLevels.Address ) && ( OnSignalLevelChange != null ) ) {
                int val = ( anArgsArray[ 1 ] << 8 ) + anArgsArray[ 0 ];
                try { OnSignalLevelChange( this, val ); }
                catch { }
            }
        }
        public XMFilterBank ( XEProxy aParent )
            : base( aParent, EXMapType.FilterBank, "Filter Bank" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<EFilterBankConditions>( this, ( i ) => (EFilterBankConditions)i, ( c ) => (int)c );
            Fields = XMapFilterBank.GetClonedFields();
        }
    }

    public class XMFibrePort : XMBase, IXMFibrePort {
        private XMFlags<EFibrePortConditions> __Flags;
        public XMFlags<EFibrePortConditions> Flags { get { return __Flags; } }

        //// __NOTE__ setting sensitivity to 0 disables the channel!
        //public void SetConfiguration ( int aChannel, int aSensitivity ) { }
        //public void GetConfiguration ( int aChannel, out int aSensitivityLevel ) {
        //    aSensitivityLevel = 0;
        //}
        public void SetSensitivity ( float aSensitivity ) {
            Send( XMapFibrePort.BaseSensitivity.Write( aSensitivity ) );
        }
        //public float GetSensitivity () {
        //    return BitConverter.ToSingle( Send( XMapFibrePort.BaseSensitivity.Read() )[ 0 ], ResValOfs ); 
        //}
        public void SetAmbientNoiseFactor ( byte aFactor ) {
            Send( XMapFibrePort.AmbientNoiseFactor.Write( aFactor ) );
        }
        public void SetAmbientPeakFactor ( byte aFactor ) {
            Send( XMapFibrePort.AmbientPeakFactor.Write( aFactor ) );
        }
        // handler delegate type and instance for incoming digital input events.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public FibrePortEventHandler OnEvent { get; set; }
        public FibrePortChangeHandler OnSignalLevelChange { get; set; }

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == XMapFibrePort.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
                // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<EFibrePortConditions> conditions = ConditionsListFromMask( eventMask, i => (EFibrePortConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            }
        }
        public XMFibrePort ( XEProxy aParent )
            : base( aParent, EXMapType.FibrePort, "Fibre Port" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<EFibrePortConditions>( this, ( i ) => (EFibrePortConditions)i, ( c ) => (int)c );
            Fields = XMapFibrePort.GetClonedFields();
        }
    }


    // activities common to essentially all serial ports.
    public class XMSerialPort : XMBase, IXMSerialPort {

        private XMFlags<ESerialPortConditions> __Flags;
        public XMFlags<ESerialPortConditions> Flags { get { return __Flags; } }

        public void SetConfiguration ( int aBaudrate, byte aDataBits, byte aStopBits, System.IO.Ports.Parity aParity ) {
            var cmdList = new List<byte[]> {
                XMapSerialPort.Baudrate.Write(aBaudrate),
                XMapSerialPort.DataBits.Write(aDataBits),
                XMapSerialPort.StopBits.Write(aStopBits),
                XMapSerialPort.Parity.Write((byte)aParity)
            };
            var ret = Send( cmdList );
        }
        public void GetConfiguration ( out int aBaudrate, out byte aDataBits, out byte aStopBits, out System.IO.Ports.Parity aParity ) {
            var cmdList = new List<byte[]> {
                XMapSerialPort.Baudrate.Read(),
                XMapSerialPort.DataBits.Read(),
                XMapSerialPort.StopBits.Read(),
                XMapSerialPort.Parity.Read()
            };
            var ret = Send( cmdList );
            aBaudrate = BitConverter.ToInt32( ret[ 0 ], ResValOfs );
            aDataBits = ret[ 1 ][ ResValOfs ];
            aStopBits = ret[ 2 ][ ResValOfs ];
            aParity = (System.IO.Ports.Parity)ret[ 3 ][ ResValOfs ];
        }
        // can't be a property, because it needs two arguments; no marker value exists!
        public void ReplaceErrorChar ( bool aMustFlag, char aValue ) { ReplaceErrorChar( aMustFlag, (byte)aValue ); }
        public void ReplaceErrorChar ( bool aMustFlag, byte aValue ) {
            var cmdList = new List<byte[]>() { 
                XMapSerialPort.MustReplaceErrorChar.Write( aMustFlag ) };
            if ( aMustFlag )
                cmdList.Add( XMapSerialPort.ReplaceErrorCharWith.Write( aValue ) );
            Send( cmdList );
        }
        public bool DiscardNull {
            get { return Send( XMapSerialPort.MustDiscardNull.Read() )[ 0 ][ ResValOfs ] != 0; }
            set { Send( XMapSerialPort.MustDiscardNull.Write( value ) ); }
        }

        public bool IsOpen {
            get { return Flags.GetIsStatusCondition( ESerialPortConditions.PortIsOpen ); }
            set { Flags.SetIsStatusCondition( value, ESerialPortConditions.PortIsOpen ); }
        }

        // buffer size is kept to allow support for local buffering of data if exceeds remote buffer size.
        // __TODO__ 4. add support for tx-buffer-half-empty notifications.
        // __TODO__ 2. not reentrant, and blocking the receive of the answer.
        private int remoteTransmitBufferSizeLazy = -1;
        private int remoteTransmitBufferSize {
            get {
                // make sure this happens only *after* it has connected.
                if ( remoteTransmitBufferSizeLazy == -1 )
                    remoteTransmitBufferSizeLazy = BitConverter.ToInt32(
                            Send( XMapSerialPort.TxBufferSize.Read() )[ 0 ], ResValOfs );
                return remoteTransmitBufferSizeLazy;
            }
        }

        public void GetTransmitState ( out int aSize, out int aUsedCount, out int aSpaceCount ) {
            var cmdList = new List<byte[]> {
                XMapSerialPort.TxBufferSize.Read(),
                XMapSerialPort.TxDataCount.Read(),
                XMapSerialPort.TxSpaceCount.Read()
            };
            var ret = Send( cmdList );
            aSize = BitConverter.ToInt32( ret[ 0 ], ResValOfs );
            aUsedCount = BitConverter.ToInt32( ret[ 1 ], ResValOfs );
            aSpaceCount = BitConverter.ToInt32( ret[ 2 ], ResValOfs );
        }
        public void TransmitData ( byte[] aDataArray ) {
            // check this does not exceed available space in card's transmit buffer.
            //   if so, it will have to be buffered locally and sent in parts, but otherwise it goes out in one shot.
            // note that it *sleeps* between sending partial buffers, which eats up a whole thread context.
            // __TODO__ 6. this does not handle the situation with many transmissions of slightly less than max.
            // need to be a bit more sophisticated here with interlocking of events and testing of conditions.
            int txMax = remoteTransmitBufferSize / 2;
            while ( aDataArray.Length > txMax ) {
                int size, used, space = 0;
                while ( space < txMax ) {
                    // have to delay here until we confirm that the TX buffer has some space.
                    GetTransmitState( out size, out used, out space );
                    if ( space < txMax )
                        Thread.Sleep( 10 );
                }
                byte[] txData = aDataArray.Slice( 0, txMax );
                aDataArray = aDataArray.Slice( txMax, aDataArray.Length - txMax );
                Send( XMapSerialPort.TxBufferPipe.Write( txData ) );
            }
            Send( XMapSerialPort.TxBufferPipe.Write( aDataArray ) );
        }
        public void TransmitData ( string aDataString ) {
            TransmitData( BinEnc.Get.GetBytes( aDataString ) );
        }
        public void GetReceiveState ( out int aSize, out int aUsedCount, out int aSpaceCount ) {
            var cmdList = new List<byte[]> {
                XMapSerialPort.RxBufferSize.Read(),
                XMapSerialPort.RxDataCount.Read(),
                XMapSerialPort.RxSpaceCount.Read()
            };
            var ret = Send( cmdList );
            aSize = BitConverter.ToInt32( ret[ 0 ], ResValOfs );
            aUsedCount = BitConverter.ToInt32( ret[ 1 ], ResValOfs );
            aSpaceCount = BitConverter.ToInt32( ret[ 2 ], ResValOfs );
        }
        // any threshold match will result in data being reported immediately.
        // but note that data will not be returned until it matches if match thresholds have been defined!
        private List<string> RxMatchThresholds = new List<string>();
        private string RxMatchBuffer = "";

        public void ResetAllReceiveThresholds () {
            ResetReceiveMatchThresholds();
            var data = new byte[] { };
            List<byte[]> cmdList = new List<byte[]>() { 
                XMapSerialPort.RxCountThreshold.Write( 0),
                XMapSerialPort.RxTimeoutThreshold.Write( 0),
            };
            Send( cmdList );
        }
        public void SetReceiveCountThreshold ( int aCount ) {
            Send( XMapSerialPort.RxCountThreshold.Write( aCount ) );
        }
        public void SetReceiveTimeoutThreshold ( int aTimeout ) {
            Send( XMapSerialPort.RxTimeoutThreshold.Write( aTimeout ) );
        }
        public void ResetReceiveMatchThresholds () {
            RxMatchThresholds.Clear();
        }
        public void SetReceiveMatchThreshold ( string aValue ) {
            RxMatchThresholds.Add( aValue );
        }
        public void SetReceiveMatchThreshold ( byte[] aValue ) {
            SetReceiveMatchThreshold( BinEnc.Get.GetString( aValue ) );
        }

        // this is the revector for responders to unsolicited messages from the RIO stations.
        // the model is basically the same as the standard MSWindows OnEvent GUI model.
        // method delegates are given a callback address, and if not null, call that for handling.
        // __NOTE__ the responder handlers need a wrapper to unpack from the Xtend byte[] format to .NET
        // app code expects to see some sort of reasonably understandable parameters, not Xtend bytes.

        // handler delegate type and instance for incoming serial data.
        // __NOTE__ these are using auto-implemented anonymous backing fields.
        public SerialReceiveDataHandler OnReceiveData { get; set; }
        public SerialErrorHandler OnError { get; set; }
        public SerialEventHandler OnEvent { get; set; }

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == XMapSerialPort.RxBufferPipe.Address ) && ( OnReceiveData != null ) ) {
                var data = anArgsArray.Slice( 2, anArgsArray.Length - 2 );
                // if no match thresholds have been defined, just pass the data on to the event handler.
                if ( RxMatchThresholds.Count == 0 )
                    try { OnReceiveData( this, data ); }
                    catch { }
                    // otherwise there are match thresholds, so wait until one matches before returning data.
                else {
                    RxMatchBuffer += BinEnc.Get.GetString( data );
                    bool hadAMatch = false;
                    do {
                        foreach ( string match in RxMatchThresholds ) {
                            var index = RxMatchBuffer.IndexOf( match );
                            if ( index != -1 ) {
                                hadAMatch = true;
                                data = BinEnc.Get.GetBytes( RxMatchBuffer.Substring( 0, index + match.Length ) );
                                RxMatchBuffer = RxMatchBuffer.Substring( index + match.Length );
                                try { OnReceiveData( this, data ); }
                                catch { }
                                break;
                            }
                        }
                    } while ( hadAMatch );
                }
                //} else if ( ( aField == XMapSerialPort.ErrorNotification.Address ) && ( OnError != null ) ) {
                //    // __NOTE__ this only gets a notification, then *requests* the info if a handler has been set.
                //    try { OnError( this, GetErrorInfo() ); }
                //    catch { }
            } else if ( ( aField == XMapSerialPort.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __TODO__ 1. this is currently evaluated inline with the TCP receive, might need to make it a thread.
                // however that does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<ESerialPortConditions> conditions = ConditionsListFromMask( eventMask, i => (ESerialPortConditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            }
        }
        public XMSerialPort ( XEProxy aParent )
            : base( aParent, EXMapType.SerialPort, "Serial Port" ) {
            // new flags object, including conversion functions to satisfy the type checker ...
            __Flags = new XMFlags<ESerialPortConditions>( this, ( i ) => (ESerialPortConditions)i, ( c ) => (int)c );
            Fields = XMapSerialPort.GetClonedFields();
        }
    }

    // worker class for activities related to generic RS232 interfaces, ignoring gender differences.
    public class XMRS232 : XMBase, IXMRS232 {
        private XMFlags<ERS232Conditions> __Flags;
        public XMFlags<ERS232Conditions> Flags { get { return __Flags; } }

        private ERS232Conditions ConditionFromPin ( ERS232Pins aPin ) {
            // pins and pin conditions are aligned, to the extent of having a dummy signal ground condition.
            // note that pins are defined to begin with index 1, rather than 0, so we must accommodate this.
            return (ERS232Conditions)( (int)aPin + ( (int)ERS232Conditions.DataCarrierDetectActive - (int)ERS232Pins.DataCarrierDetect ) );
        }
        // overridden by the DTE and DCE inheritors to indicate settable pins.
        protected virtual void CheckPinIsSettable ( ERS232Pins aPin ) { }
        public bool GetPinIsActive ( ERS232Pins aPin ) {
            return Flags.GetIsStatusCondition( ConditionFromPin( aPin ) );
        }
        public void SetPinActive ( ERS232Pins aPin ) {
            CheckPinIsSettable( aPin );
            Flags.SetIsStatusCondition( true, ConditionFromPin( aPin ) );
        }
        public void SetPinInactive ( ERS232Pins aPin ) {
            CheckPinIsSettable( aPin );
            Flags.SetIsStatusCondition( false, ConditionFromPin( aPin ) );
        }
        public void SetPinState ( bool aState, ERS232Pins aPin ) {
            if ( aState ) SetPinActive( aPin );
            else SetPinInactive( aPin );
        }

        public void SetHandshakeMode ( ERS232Handshake aMode ) {
            Send( __XMapRS232.HandshakeMode.Write( (int)aMode ) );
        }

        public bool PassiveReceiveOnly {
            get { return Send( __XMapRS232.PassiveReceiveOnly.Read() )[ 0 ][ ResValOfs ] != 0; }
            set { Send( __XMapRS232.PassiveReceiveOnly.Write( value ) ); }
        }

        public RS232EventHandler OnEvent { get; set; }

        public override void MapResponder ( byte aField, byte[] anArgsArray ) {
            // __NOTE__ can't have a switch on field addresses here because Address is not a constant value.
            if ( ( aField == __XMapRS232.EventNotification.Address ) && ( OnEvent != null ) ) {
                // __NOTE__ this is evaluated in the threadpool, out of line with the main thread or TCP receive handler.
                // this does raise some thorny issues re ordering of events in the threadpool (ie. they aren't).
                int eventMask = BitConverter.ToInt32( anArgsArray.Slice( 0, 4 ), 0 );
                int statusMask = BitConverter.ToInt32( anArgsArray.Slice( 4, 4 ), 0 );
                List<ERS232Conditions> conditions = ConditionsListFromMask( eventMask, i => (ERS232Conditions)i );
                foreach ( var c in conditions )
                    try { OnEvent( this, c, ( statusMask & ( 1 << (int)c ) ) != 0 ); }
                    catch { }
            }
        }
        public XMRS232 ( XEProxy aParent, EXMapType aMapType )
            : base( aParent, aMapType, "RS232" ) {
            __Flags = new XMFlags<ERS232Conditions>( this, ( i ) => (ERS232Conditions)i, ( c ) => (int)c );
            Fields = __XMapRS232.GetClonedFields();
        }
    }

    // activities related to DTE style interfaces, ie. pin control etc. as for PC-side connections.
    public class XMRS232DTE : XMRS232 {
        protected override void CheckPinIsSettable ( ERS232Pins aPin ) {
            switch ( aPin ) {
                case ERS232Pins.DataTerminalReady:
                case ERS232Pins.RequestToSend:
                    break;
                default:
                    throw new XBadMessageException( "cannot set or clear this pin, not an output" );
            }
        }
        public XMRS232DTE ( XEProxy aParent )
            : base( aParent, EXMapType.RS232DTE ) {
            TypeName = "RS232 DTE";
        }
    }

    // activities related to DCE style interfaces, ie. pin control etc. as for modem-side connections.
    public class XMRS232DCE : XMRS232 {
        protected override void CheckPinIsSettable ( ERS232Pins aPin ) {
            switch ( aPin ) {
                case ERS232Pins.DataCarrierDetect:
                case ERS232Pins.DataSetReady:
                case ERS232Pins.ClearToSend:
                case ERS232Pins.RingIndicator:
                    break;
                default:
                    throw new XBadMessageException( "cannot set or clear this pin, not an output" );
            }
        }
        public XMRS232DCE ( XEProxy aParent )
            : base( aParent, EXMapType.RS232DCE ) {
            TypeName = "RS232 DCE";
        }
    }

    // *** wrapper class for the Modbus map class. ***

    // it takes care of properly constructing the packet that must be sent for any given command.
    public class XMModbus : XMBase {
        // wrap message body with map index, Modbus device address, and command strobe.
        private byte[] DispatchModbusCommand ( int aDevice, List<byte[]> aCmdList ) {
            aCmdList.Insert( 0, XMapModbus.DestinationDevice.Write( aDevice ) );
            aCmdList.Add( XMapModbus.SendCommand.Write() );
            return Send( aCmdList )[ 0 ];
        }
        // returns an array with one byte of state per coil read (not one bit as per Modbus).
        public byte[] ReadCoils ( int aDevice, int aStartIndex, int aCount ) {
            var cmdList = new List<byte[]> {
                XMapModbus.ReadCoilsStartAddress.Write(aStartIndex),
                XMapModbus.ReadCoilsCount.Write(aCount),
            };
            var answer = DispatchModbusCommand( aDevice, cmdList );
            // __TODO__ 5. process result, not including timeouts and errors, those are dealt with in Send.
            return null;
        }
        public XMModbus ( XEProxy aParent )
            : base( aParent, EXMapType.Modbus, "Modbus" ) {
            Fields = XMapModbus.GetClonedFields();
        }
    }

}




