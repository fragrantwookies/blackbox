﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Text;
using System.Collections.Generic;
using System.Threading;
#endregion

namespace XPointAudiovisual.Xtend {

    // every card (including the master card) has an element-0, the administration element for that card.
    // this element typically handles things like ping, reset etc.

    // cards can implement virtual elements which might use up one or more physical elements.
    //   eg. x 8 digital input card could implement x 4 Wiegand elements, each using x 2 inputs.
    //   these physical input elements would then be reserved and unavailable for other uses.

    // __TODO__ 5. identify which map fields are persisted into flash!
    // the idea is to avoid repetitive writes to flash because it will decay over time.
    // at the very least, remotes must not write into flash if value is already the same as there.

    // __TODO__ 5. needs something equivalent to MIB interface.
    //   also startup behaviour must be specified somehow.

    public enum EXMapType : byte {
        // management resources.
        Service = 0x00,
        Configuration,
        Statistics,
        Log,
        Calibration,
        Registration,
        Routing,
        Network,
        Status,
        Diagnostics,

        // IO resources.
        DigitalInput = 0x10,
        AnalogInput = 0x11,
        DigitalOutput = 0x12,
        RelayOutput = 0x13,
        PwmOutput = 0x14,
        AnalogOutput = 0x15,

        Wiegand0 = 0x16,
        Wiegand1 = 0x17,

        AudioInput = 0x18,
        AudioOutput = 0x19,

        AnalogMonitor = 0x1A,
        FilterBank = 0x1B,
        FibrePort = 0x1C,

        MonitoredInput = 0x1D,

        OnWireDevice,

        ADCBank = 0x1F,

        // component resources.
        RealtimeClock = 0x30,
        TextScreen,
        GraphicScreen,
        Keypad,
        Keyboard,
        Joystick,
        Buzzer,
        LED,

        // communications.
        SerialPort = 0x40,
        RS232DTE,                   // PC-side, normally 9D male.
        RS232DCE,                   // modem-side, normally 9D female.
        RS485,
        RS422,
        CanBus,
        Ethernet,
        Modem,
        RF2400,
        RF868,
        RF433,
        GPRS,
        IRdA,
        OnWireBus,

        Wiegand, // virtual, uses two digital inputs with Wiegand0/1 enabled.

        // protocols.
        Modbus = 0x60,
        ModbusAscii,
        ModbusRTU,
        ModbusAsciiLinkOnly,
        ModbusRTULinkOnly,
        PNet,
        VideoBlox,

        // activities.
        ModbusPoll = 0x70, // destination, command, values, frequency
        
        Private = 0xFD,
        Virtual = 0xFE,
        Unused = 0xFF,
    };

    // every element has multiple map interfaces, including at least the Service and Configuration interfaces.

    // priorities are:  -1 = low, 0 = medium, 1 = high.
    // all messages are medium by default, and by default preserve ordering.

    // all fields are R/W unless specified R/O or W/O
    // consecutive fields are handled consecutively, according to definition and data.
    // a field may have multiple consecutive subfields, this is definition dependent.

    // sending 0xFFFF as a count to an array field generally means "all".
    // if a count is sent that is greater than available, all available will be returned.
    // "list" and "pipe" map fields expect a stream of data, in or out, 
    //    which always implies a 16-bit count to follow the map field address.
    // a "pipe" differs from a list in that items in a pipe are removed when read.

    // "get" and "set" are always invoked from outside the field.
    // "push" and "pull" are invoked by the field itself, and must be listened for
    //   in other words, they are not solicited by the server, but by the slave.

    public enum EXFieldAccessMode : byte {
        Query = 0x01, Order, Push, Pull,
        Answer = 0x11, Confirm, Accept, Deliver,
        CasualQuery = 0x21, CasualOrder, CasualPush, CasualPull,
        CasualAnswer = 0x31, CasualConfirm, CasualAccept, CasualDeliver,
        Error = 0xFF,
    }
    public enum EXFieldUsage : ushort {
        IsGuiVisible = 0x0001,
        IsGuiReadable = 0x0002,
        IsGuiWritable = 0x0004,
        IsGuiInvokable = 0x0008,
        IsGuiEditable = 0x0010,

        IsOper = 0x0100,
        IsSupv = 0x0200,
        IsAdmin = 0x0400,
        IsInst = 0x0800,
        IsManf = 0x1000,

        IsPersistent = 0x8000
    }

    public enum EXFieldAccessErrors {
        UnspecifiedError = 0,
        UnknownField,
        UnknownMap,
        UnknownElement,
        UnknownCard,
        AttemptedWriteOfReadonlyField,
        BadParameters,
        CardCommFailed,
        CardResponseTimedOut,
    }

    public static class XFieldAccessErrorStrings {
        public static string Get ( int anError ) {
            switch ( (EXFieldAccessErrors)anError ) {
                case EXFieldAccessErrors.UnspecifiedError:
                    return "unspecified error";
                case EXFieldAccessErrors.UnknownField:
                    return "attempted access of an unknown field";
                case EXFieldAccessErrors.UnknownMap:
                    return "attempted access of an unknown map";
                case EXFieldAccessErrors.UnknownElement:
                    return "attempted access of an unknown element";
                case EXFieldAccessErrors.UnknownCard:
                    return "attempted access of an unknown card";
                case EXFieldAccessErrors.AttemptedWriteOfReadonlyField:
                    return "attempted write of a readonly field";
                case EXFieldAccessErrors.BadParameters:
                    return "bad parameters supplied for field access";
                case EXFieldAccessErrors.CardCommFailed:
                    return "card communications failed";
                case EXFieldAccessErrors.CardResponseTimedOut:
                    return "card response timed out";
            }
            return "unknown error";
        }
        public static string Get ( EXFieldAccessErrors anError ) { return Get( (int)anError ); }
    }

    public class XField {
        protected readonly byte fieldAddress;
        protected readonly string displayText;
        protected readonly bool isReadOnly = false;
        protected readonly bool isDatagram = false;
        protected readonly int priority = 0;
        protected readonly bool isOrdered = true;

        protected readonly EXFieldUsage usage;

        protected virtual string ValueString () {
            return "xxx";
        }
        public string ToolTipText { get; private set; }

        public override string ToString () {
            var s = new StringBuilder();
            s.Append( "  Field -- " );
            s.Append( this.Address.ToString( "X2" ) );
            s.Append( " -- " );
            s.Append( this.DisplayText );
            if ( ( usage & GuiInvokeUsage ) == GuiInvokeUsage )
                s.Append( " !" );
            else if ( ( usage & GuiReadUsage ) == GuiReadUsage )
                s.Append( " = " + this.ValueString() );
            return s.ToString();
        }

        private byte[] buildArray ( bool hasLenPrefix, int aLength, byte[] aDataArray, EXFieldAccessMode aMode ) {
            var result = new byte[ 1 + 1 + ( hasLenPrefix ? 2 : 0 ) + aLength ];
            result[ 0 ] = (byte)( (int)fieldAddress );
            result[ 1 ] = (byte)( (int)aMode | ( isDatagram ? 0x20 : 0 ) );
            if ( hasLenPrefix )
                Array.Copy( BitConverter.GetBytes( (short)aLength ), 0, result, 2, 2 );
            if ( aLength != 0 )
                Array.Copy( aDataArray, 0, result, ( hasLenPrefix ? 4 : 2 ), aLength );
            return result;
        }
        public byte[] Read () {
            return buildArray( false, 0, null, EXFieldAccessMode.Query );
        }
        public byte[] Post ( bool aHasLenPrefix, int aLength, byte[] aDataArray ) {
            return buildArray( aHasLenPrefix, aLength, aDataArray, EXFieldAccessMode.Push );
        }
        private byte[] buildWrite ( bool aHasLenPrefix, int aLength, byte[] aDataArray ) {
            if ( isReadOnly ) throw new XBadMessageException( "attempted to write a read-only field" );
            return buildArray( aHasLenPrefix, aLength, aDataArray, EXFieldAccessMode.Order );
        }
        protected byte[] buildWrite ( byte[] aDataArray ) { return buildWrite( false, aDataArray.Length, aDataArray ); }
        protected byte[] buildWrite ( int aLength, byte[] aDataArray ) { return buildWrite( true, aLength, aDataArray ); }

        public byte Address { get { return fieldAddress; } }
        public string DisplayText { get { return displayText; } }
        public EXFieldUsage Usage { get { return usage; } }

        internal virtual XField CloneFor ( __XMapBase aParent ) {
            return new XField( this.Address, this.DisplayText, this.ToolTipText, this.usage, this.isReadOnly, this.isDatagram, this.priority, this.isOrdered );
        }
        public const EXFieldUsage DefaultUsage = 0;
        public const EXFieldUsage GuiVisibleUsage = EXFieldUsage.IsGuiVisible;
        public const EXFieldUsage GuiReadUsage = GuiVisibleUsage | EXFieldUsage.IsGuiReadable;
        public const EXFieldUsage GuiWriteUsage = GuiVisibleUsage | EXFieldUsage.IsGuiWritable;
        public const EXFieldUsage GuiReadWriteUsage = GuiReadUsage | GuiWriteUsage;
        public const EXFieldUsage GuiInvokeUsage = GuiVisibleUsage | EXFieldUsage.IsGuiInvokable;
        public const EXFieldUsage GuiEditUsage = GuiVisibleUsage | EXFieldUsage.IsGuiEditable;

        public const EXFieldUsage ManfUpUsage = GuiReadWriteUsage | EXFieldUsage.IsManf;
        public const EXFieldUsage InstUpUsage = ManfUpUsage | EXFieldUsage.IsInst;
        public const EXFieldUsage AdminUpUsage = InstUpUsage | EXFieldUsage.IsAdmin;
        public const EXFieldUsage SupvUpUsage = AdminUpUsage | EXFieldUsage.IsSupv;
        public const EXFieldUsage OperUpUsage = SupvUpUsage | EXFieldUsage.IsOper;

        public const EXFieldUsage SysConfig = XField.AdminUpUsage | EXFieldUsage.IsPersistent;

        protected XField (
            int anAddress, string aDisplayText, string aToolTipText = "",
            EXFieldUsage aUsage = DefaultUsage,
            bool anIsReadOnly = false, bool anIsDatagram = false, int aPriority = 0, bool anIsOrdered = true ) {
            fieldAddress = (byte)anAddress;
            displayText = aDisplayText;
            ToolTipText = aToolTipText;
            usage = aUsage;
            isReadOnly = anIsReadOnly;
            isDatagram = anIsDatagram;
            priority = aPriority;
            isOrdered = anIsOrdered;
        }
        protected virtual void Dispose ( bool disposing ) {
            if ( disposing )
                GC.SuppressFinalize( this );
        }
        ~XField () {
            Dispose( false );
        }
        public void Dispose () {
            Dispose( true );
        }
    }

    // void fields are generally strobes, where an action is desired, but does not require data.
    public class XFVoid : XField {
        public byte[] Write () { return buildWrite( new byte[] { } ); }
        internal override XField CloneFor ( __XMapBase aParent ) {
            return new XFVoid( this.Address, this.DisplayText, this.ToolTipText, this.usage, this.isReadOnly, this.isDatagram, this.priority, this.isOrdered );
        }
        public XFVoid (
            int anAddress, string aDisplayText, string aToolTipText = "",
            EXFieldUsage aUsage = DefaultUsage,
            bool anIsReadOnly = false, bool anIsDatagram = false, int aPriority = 0, bool anIsOrdered = true ) :
            base( anAddress, aDisplayText, aToolTipText, aUsage, anIsReadOnly, anIsDatagram, aPriority, anIsOrdered ) { }
    }
    public class XFBool : XField {
        public byte[] Write ( bool aValue ) { return buildWrite( new byte[] { (byte)( aValue ? 1 : 0 ) } ); }
        internal override XField CloneFor ( __XMapBase aParent ) {
            return new XFBool( this.Address, this.DisplayText, this.ToolTipText, this.usage, this.isReadOnly, this.isDatagram, this.priority, this.isOrdered );
        }
        protected override string ValueString () {
            return "true/false";
        }
        public XFBool (
           int anAddress, string aDisplayText, string aToolTipText = "",
            EXFieldUsage aUsage = DefaultUsage,
            bool anIsReadOnly = false, bool anIsDatagram = false, int aPriority = 0, bool anIsOrdered = true ) :
            base( anAddress, aDisplayText, aToolTipText, aUsage, anIsReadOnly, anIsDatagram, aPriority, anIsOrdered ) { }
    }
    public class XFByte : XField {
        public byte[] Write ( int aValue ) { return buildWrite( new byte[] { (byte)aValue } ); }
        internal override XField CloneFor ( __XMapBase aParent ) {
            return new XFByte( this.Address, this.DisplayText, this.ToolTipText, this.usage, this.isReadOnly, this.isDatagram, this.priority, this.isOrdered );
        }
        protected override string ValueString () {
            return "+ 00";
        }
        public XFByte (
            int anAddress, string aDisplayText, string aToolTipText = "",
            EXFieldUsage aUsage = DefaultUsage,
           bool anIsReadOnly = false, bool anIsDatagram = false, int aPriority = 0, bool anIsOrdered = true ) :
            base( anAddress, aDisplayText, aToolTipText, aUsage, anIsReadOnly, anIsDatagram, aPriority, anIsOrdered ) { }
    }
    public class XFWord : XField {
        public byte[] Write ( int aValue ) { return buildWrite( BitConverter.GetBytes( (ushort)( aValue & 0xFFFF ) ) ); }
        internal override XField CloneFor ( __XMapBase aParent ) {
            return new XFWord( this.Address, this.DisplayText, this.ToolTipText, this.usage, this.isReadOnly, this.isDatagram, this.priority, this.isOrdered );
        }
        protected override string ValueString () {
            return "+ 0000";
        }
        public XFWord (
           int anAddress, string aDisplayText, string aToolTipText = "",
            EXFieldUsage aUsage = DefaultUsage,
           bool anIsReadOnly = false, bool anIsDatagram = false, int aPriority = 0, bool anIsOrdered = true ) :
            base( anAddress, aDisplayText, aToolTipText, aUsage, anIsReadOnly, anIsDatagram, aPriority, anIsOrdered ) { }
    }
    public class XFInteger : XField {
        public byte[] Write ( int aValue ) { return buildWrite( BitConverter.GetBytes( (int)( aValue & 0xFFFFFFFF ) ) ); }
        internal override XField CloneFor ( __XMapBase aParent ) {
            return new XFInteger( this.Address, this.DisplayText, this.ToolTipText, this.usage, this.isReadOnly, this.isDatagram, this.priority, this.isOrdered );
        }
        protected override string ValueString () {
            return "+/- 00000000";
        }
        public XFInteger (
            int anAddress, string aDisplayText, string aToolTipText = "",
            EXFieldUsage aUsage = DefaultUsage,
            bool anIsReadOnly = false, bool anIsDatagram = false, int aPriority = 0, bool anIsOrdered = true ) :
            base( anAddress, aDisplayText, aToolTipText, aUsage, anIsReadOnly, anIsDatagram, aPriority, anIsOrdered ) { }
    }
    public class XFFloat : XField {
        public byte[] Write ( float aValue ) { return buildWrite( BitConverter.GetBytes( aValue ) ); }
        internal override XField CloneFor ( __XMapBase aParent ) {
            return new XFFloat( this.Address, this.DisplayText, this.ToolTipText, this.usage, this.isReadOnly, this.isDatagram, this.priority, this.isOrdered );
        }
        protected override string ValueString () {
            return "+/- 999.9999";
        }
        public XFFloat (
            int anAddress, string aDisplayText, string aToolTipText = "",
            EXFieldUsage aUsage = DefaultUsage,
            bool anIsReadOnly = false, bool anIsDatagram = false, int aPriority = 0, bool anIsOrdered = true ) :
            base( anAddress, aDisplayText, aToolTipText, aUsage, anIsReadOnly, anIsDatagram, aPriority, anIsOrdered ) { }
    }
    public class XFArray : XField {
        public byte[] Read ( int length ) { return new byte[] { fieldAddress, (byte)( length & 0xFF ), (byte)( length >> 8 ) }; }
        public byte[] Write ( byte[] data ) { return buildWrite( data.Length, data ); }
        public byte[] Write ( int length, byte[] data ) { return buildWrite( length, data ); }
        internal override XField CloneFor ( __XMapBase aParent ) {
            return new XFArray( this.Address, this.DisplayText, this.ToolTipText, this.usage, this.isReadOnly, this.isDatagram, this.priority, this.isOrdered );
        }
        protected override string ValueString () {
            return "[0..k]";
        }
        public XFArray (
            int anAddress, string aDisplayText, string aToolTipText = "",
            EXFieldUsage aUsage = DefaultUsage,
            bool anIsReadOnly = false, bool anIsDatagram = false, int aPriority = 0, bool anIsOrdered = true ) :
            base( anAddress, aDisplayText, aToolTipText, aUsage, anIsReadOnly, anIsDatagram, aPriority, anIsOrdered ) { }
    }
    public class XFString : XField {
        public byte[] Read ( int length ) { return new byte[] { fieldAddress, (byte)( length & 0xFF ), (byte)( length >> 8 ) }; }
        public byte[] Write ( byte[] data ) { return buildWrite( data.Length, data ); }
        public byte[] Write ( int length, byte[] data ) { return buildWrite( length, data ); }
        internal override XField CloneFor ( __XMapBase aParent ) {
            return new XFString( this.Address, this.DisplayText, this.ToolTipText, this.usage, this.isReadOnly, this.isDatagram, this.priority, this.isOrdered );
        }
        protected override string ValueString () {
            return "\"string\"";
        }
        public XFString (
            int anAddress, string aDisplayText, string aToolTipText = "",
            EXFieldUsage aUsage = DefaultUsage,
            bool anIsReadOnly = false, bool anIsDatagram = false, int aPriority = 0, bool anIsOrdered = true ) :
            base( anAddress, aDisplayText, aToolTipText, aUsage, anIsReadOnly, anIsDatagram, aPriority, anIsOrdered ) { }
    }

    // __NOTE__ mask fields are used to set or clear bits in flag fields.
    // first send a message setting the mask fields of the bits to be modified.
    // these can be bits that will be changing low or high, not just one or the other.
    // then send a message with the new values of those bits in the flags field.
    // only those flags which are set in the mask will be changed.

    // *all* maps *always* reserve field 0x00 as the "map status flags" field,
    //    and field 0x01 as the map control field.

    // all map conditions must have these generic items cloned at the following locations.
    public enum EMapStatusConditions {
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
    }

    // inherited by every map class, all have these following fields by default.
    // note that a given map class might not define a use for any of the error or status flags.
    public class __XMapBase {

        // raw read of the status flags word.
        public static readonly XFInteger MapStatusFlagsGet = new XFInteger( 0x00, "Map Status Flags Get", "Get the status flags for this map." );
        public static readonly XFInteger MapStatusFlagsSet = new XFInteger( 0x01, "Map Status Flags Set", "Set the status flags for this map." );
        public static readonly XFInteger MapStatusFlagsClear = new XFInteger( 0x02, "Map Status Flags Clear", "Clear the status flags for this map." );

        public static readonly XFArray ErrorNotification = new XFArray( 0x0B, "Error Notification", "A notification of an error occurrence." );
        public static readonly XFArray EventNotification = new XFArray( 0x0C, "Event Notification", "A notification of an event occurrence." );

        // this method intended to be overridden by subclasses.
        public static List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
                MapStatusFlagsGet , MapStatusFlagsSet , MapStatusFlagsClear , 
                 };
            return AppendLiveFields( null, fields );
        }
        // this method intended to be inherited by subclasses.
        protected static List<XField> AppendLiveFields ( List<XField> aFieldList, XField[] aFieldArray ) {
            if ( aFieldList == null )
                aFieldList = new List<XField>();
            if ( aFieldArray != null )
                foreach ( var f in aFieldArray )
                    aFieldList.Add( f.CloneFor( null ) );
            return aFieldList;
        }
    }

    public class XMapStatus : __XMapBase {
        // raw read of the *element* status flags word.
        public static readonly XFInteger StatusFlagsGet = //
            new XFInteger( 0x00, "Status Flags Get", "Get the status flags for this element.", XField.GuiReadUsage, true );
        public static readonly XFInteger StatusFlagsSet = new XFInteger( 0x01, "Status Flags Set", "Set the status flags for this element." );
        public static readonly XFInteger StatusFlagsClear = new XFInteger( 0x02, "Status Flags Clear", "Clear the status flags for this element." );

        // fields used when sending notifications.
        // error code = 0 for no error currently active.
        // error data is always cleared after reading.
        // note that these are semantic errors relevant to this specific field's activities.
        // they have nothing to do with comms or general device errors.
        public static readonly XFByte ErrorCode = new XFByte( 0x05, "Error Code", "The most recent error code.", XField.GuiReadUsage, true );
        public static readonly XFByte ErrorAction = new XFByte( 0x06, "Error Action", "The most recent error action.", 0, true );
        public static readonly XFWord ErrorIndex = new XFWord( 0x07, "Error Index", "The most recent error index (if any).", 0, true );
        public static readonly XFWord ErrorOffset = new XFWord( 0x08, "Error Offset", "The most recent error offset.", 0, true );
        public static readonly XFArray ErrorData = new XFArray( 0x09, "Error Data", "The most recent error data.", 0, true );
        public static readonly XFString ErrorMessage = new XFString( 0x0A, "Error Message", "The most recent error message.", 0, true );

        // an event is always a change in status.
        public static readonly XFInteger EventNotifyFlagsGet = new XFInteger( 0x10, "Event Notifier Flags", "Get notifying event flags.", 0, true );
        public static readonly XFInteger EventNotifyFlagsSet = new XFInteger( 0x11, "Event Notifier Flags Set", "Set notifying event flags." );
        public static readonly XFInteger EventNotifyFlagsClear = new XFInteger( 0x12, "Event Notifier Flags Clear", "Clear notifying event flags." );
        public static readonly XFInteger EventNotifyFlagsEdit =
            new XFInteger( 0x13, "Edit Event Notifier Flags", "Select or deselect events to be notified.", XField.GuiEditUsage | XField.AdminUpUsage, true );

        public static new List<XField> GetClonedFields () {
            var fields = new XField[] { 
                StatusFlagsGet, StatusFlagsSet, StatusFlagsClear, 
                ErrorCode, ErrorAction, ErrorIndex, ErrorOffset, ErrorData, ErrorMessage ,ErrorNotification, 
                EventNotifyFlagsGet,EventNotifyFlagsSet,EventNotifyFlagsClear,EventNotification, EventNotifyFlagsEdit,
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }
    public class XMapService : __XMapBase {
        public static readonly XFWord Sleep = new XFWord( 0x20, "Sleep", "Put the device into sleep mode." );
        public static readonly XFWord Wake = new XFWord( 0x21, "Wake", "Wake the device from sleep mode." );
        // reboots receiving device (only master to slave).
        // only reboots after completely sending out ack packet & logging message.
        public static readonly XFWord Reboot = new XFWord( 0x22, "Reboot", "Cause the device to reboot." );

        // device maintenance.
        // ----------------------
        public static readonly XFArray UpdateFirmware =
            new XFArray( 0x30, "Update Firmware", "Download new firmware code into the device.",
                XField.GuiInvokeUsage | EXFieldUsage.IsPersistent, false );

        // device security.
        // -----------------
        public static readonly XFBool IsSecureKeyed =
            new XFBool( 0x40, "Is Secure Keyed", "Query if device has a security key.", XField.GuiReadUsage, true );
        public static readonly XFArray WriteSecureKey =
            new XFArray( 0x41, "Write Secure Key", "Write a permanent once-off security key to the device.", 0, false, true );

        // device presence & responsivity.
        // -------------------------------------
        // just to test comm connection & session resync, no side-effects.
        // always acked, otherwise *sender* doesn't know if OK.
        // but slaves can send un-acked reverse pings (heartbeat) on a regular basis.
        public static readonly XFVoid Ping = new XFVoid( 0x50, "Ping", "Ping to test the device link.", XField.GuiInvokeUsage );

        // no data, just a regular heartbeat notification from source.
        // can be either addressed or broadcast, acked or not; defaults to broadcast/unacked.
        // use helps avoid bidirectional PC data comms congestion (from slow OS responses).
        public static readonly XFVoid Heartbeat = new XFVoid( 0x51, "Heartbeat", "Device liveness and link confirmation.", 0, false, true );

        // receiver returns all data in the initial packet.
        public static readonly XFArray Echo = new XFArray( 0x52, "Echo", "Echo to test the device connection." );

        public static readonly XFVoid StatusUpdateRequest = //
            new XFVoid( 0x53, "Status Update Request", "Get current device status.", XField.GuiInvokeUsage );

        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
                Sleep , Wake, Reboot, 
            UpdateFirmware,IsSecureKeyed,WriteSecureKey,
            Ping,Heartbeat,Echo,StatusUpdateRequest,
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }
    public class XMapStatistics : __XMapBase {
        public static new List<XField> GetClonedFields () {
            return AppendLiveFields( __XMapBase.GetClonedFields(), null );
        }
    }
    public class XMapLog : __XMapBase {
        public static new List<XField> GetClonedFields () {
            return AppendLiveFields( __XMapBase.GetClonedFields(), null );
        }
    }

    public class XMapRegistration : __XMapBase {
        // device boot-status & bus/system registration.
        // ------------------------------------------------------
        // invitation to publish, can be addressed or broadcast.
        // data = 6-byte MAC# address of device or all 0xFF.
        public static readonly XFArray Invite = new XFArray( 0x60, "Invite", "Invite a device to publish enrollment data.", 0, false, true );

        // invited or newly-booted device places details on the network.
        // can be automatic broadcast (default, always uses MAC addressing)
        //  or invited only directed/broadcast (less flooding, uses invite address)
        // not acked by default (don't want retries of something that is unwanted).
        public static readonly XFArray PublishMac = new XFArray( 0x61, "Publish Mac", "Publish the device MAC address.", 0, false, true );
        public static readonly XFString PublishDesc = new XFString( 0x62, "Publish Desc", "Publish the device description.", 0, false, true );
        public static readonly XFArray PublishCard = new XFArray( 0x63, "Publish Card", "Publish a card-info record.", 0, false, true );
        public static readonly XFArray PublishCompact = new XFArray( 0x64, "Publish Compact", "Publish a compact device enrollment record.", 0, false, true );

        // master/chief accepts slave/member after it has published its presence.
        // sees slave publishing, recognizes, enrolls it (stops it auto-publishing).
        public static readonly XFVoid Enroll = new XFVoid( 0x70, "Enroll", "Enroll a device with the server.", 0, false );
        public static readonly XFVoid Dismiss = new XFVoid( 0x71, "Dismiss", "Undo a device enrollment.", 0, false );
        public static readonly XFArray DismissThese = new XFArray( 0x72, "Dismiss These", "Undo enrollment of listed devices.", 0, false, true );

        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
            Invite,PublishMac,PublishDesc,PublishCard, PublishCompact,
            Enroll,Dismiss,DismissThese,
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapRouting : __XMapBase {
        // a device's site indexing number, either hard or soft.
        // __TODO__ 0. keep a dynamic list of all allocated soft indexes to ensure no duplicates,
        //    and also to know what value to hand out next for auto-indexing.
        // soft indexes are allocated dynamically by the server, and only if a device has no hard index.
        // to avoid collisions, hard indexes must only be allocated manually by explicit configuration.
        public static readonly XFWord SoftIndex =
            new XFWord( 0x54, "Soft Index", "Auto-allocated device system index.", XField.GuiReadUsage, false, false );
        public static readonly XFWord HardIndex =
            new XFWord( 0x55, "Hard Index", "Manually-allocated device system index", XField.GuiReadWriteUsage | XField.SysConfig, false, false );

        // a prefixing virtual message from a non-Ethernet hub has been re-routed through this device.
        // the site index of the re-routed device is included as the Routing argument.
        // the re-routing applies to all subsequent message blocks in this frame.
        // (if there is no Routing message, the normal source or destination Ethernet hub is assumed.)
        public static readonly XFBool Routing = new XFBool( 0x56, "Routing", "Virtual routing prefix (internal use only)." );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
            SoftIndex,HardIndex,Routing,
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapNetwork : __XMapBase {
        public static readonly XFArray MacAddress = new XFArray( 0x57, "Mac Address", "The device MAC address.", XField.GuiReadUsage );
        // select between static, DNS and UPnP allocation of address.
        public static readonly XFByte IpAddressAllocation =
            new XFByte( 0x58, "IP Address Allocation", "Select static, DNS or UPnP IP address mode.", XField.GuiReadWriteUsage | XField.SysConfig );
        public static readonly XFArray IpAddress = new XFArray( 0x59, "IP Address", "Allocated IP address.", XField.GuiReadWriteUsage | XField.SysConfig );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
            MacAddress,IpAddressAllocation,IpAddress,
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    // the Configuration map lists all possible maps for this element, as well as all enabled.
    // currently active maps must be disabled before enabling conflicting maps.
    // also includes mechanisms for managing settings etc.

    public class XMapConfiguration : __XMapBase {
        //GetSupport = 0x00,
        //GetName = 0x01,
        //GetType,
        //GetManufacturer,
        //GetModelNumber,
        //GetVersions,
        //GetSerialNumber,
        //GetXtendAttributes,
        //GetSupportedMaps,
        //GetDipSwitch,
        //SetName = 0x81,
        //RepName,
        //RepDipSwitch,

        public static readonly XFArray SupportedMapsList = new XFArray( 0x20, "Supported Maps List", "The list of maps supported by this element.", 0, true );
        public static readonly XFArray ActiveMapsList = new XFArray( 0x21, "Active Maps List", "The list of all maps currently active for this element.", 0, true );
        public static readonly XFVoid DisableAllMaps = new XFVoid( 0x22, "Disable All Maps", "Disable all maps for this element." );

        public static readonly XFVoid CommitChanges =
            new XFVoid( 0x30, "Commit Changes", "Commit all configuration changes.", //
                XField.AdminUpUsage | XField.GuiInvokeUsage | EXFieldUsage.IsPersistent );
        public static readonly XFVoid UndoChanges =
            new XFVoid( 0x31, "Undo Changes", "Undo all configuration changes since last committed.", //
                XField.AdminUpUsage | XField.GuiInvokeUsage | EXFieldUsage.IsPersistent );
        public static readonly XFVoid LoadDefaults =
            new XFVoid( 0x32, "Load Defaults", "Load default configuration settings.", //
                XField.AdminUpUsage | XField.GuiInvokeUsage | EXFieldUsage.IsPersistent );
        public static readonly XFArray CurrentSettings =
            new XFArray( 0x33, "Get Current Settings", "Get current configuration settings.", //
                XField.AdminUpUsage | XField.GuiInvokeUsage );

        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
                SupportedMapsList,ActiveMapsList,DisableAllMaps,
                CommitChanges,UndoChanges,LoadDefaults,CurrentSettings
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    // calibration is for factory usage only.
    public class XMapCalibration : __XMapBase {
        public static readonly XFVoid Calibrate =
            new XFVoid( 0x20, "Calibrate", "Manufacturer use only !!!", XField.ManfUpUsage | EXFieldUsage.IsPersistent | XField.GuiInvokeUsage );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { Calibrate };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    // statistics map

    //    GetBusStatistics = 0x01,
    //GetDeviceErrorStatistics,


    // logging

    //RepRebootEvent = 0x81,
    //RepEnrolmentEvent,
    //RepFailureEvent,
    //RepSecurityEvent,
    //RepAuditedEvent,

    // Conditions are enumerated as per bit indexes in the flags word.

    // GPIO definitions.
    // ------------------

    public class XMapDigitalInput : __XMapBase {
        public static readonly XFBool ActiveLow = new XFBool( 0x20, "Active Low", "If input is active low.", XField.SysConfig );
        public static readonly XFWord DebouncePeriod = //
            new XFWord( 0x21, "Debounce Period", "Debounce period for this input.", XField.SysConfig );
        //public static readonly XFWord DebounceCount = new XFWord( 0x22 );
        public static readonly XFBool IsActive = new XFBool( 0x30, "Is Active", "Logical active state of this input.", XField.GuiReadUsage );
        public static readonly XFBool IsHighLevel = new XFBool( 0x31, "Is High Level", "Physical state of this input.", XField.GuiReadUsage );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { ActiveLow, DebouncePeriod, IsActive, IsHighLevel };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapDigitalOutput : __XMapBase {
        public static readonly XFBool ActiveLow = new XFBool( 0x20, "Active Low", "If output is active low.", XField.SysConfig );
        public static readonly XFBool IsActive = new XFBool( 0x30, "Is Active", "Logical active state of this output.", XField.GuiReadWriteUsage );
        public static readonly XFBool IsHighLevel = new XFBool( 0x31, "Is High Level", "Physical state of this output.", XField.GuiReadWriteUsage );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { ActiveLow, IsActive, IsHighLevel };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapAnalogInput : __XMapBase {
        public static readonly XFByte FilterDepth = new XFByte( 0x20, "Filter Depth", "Number of IIR filter stages.", XField.SysConfig );
        public static readonly XFFloat Hysteresis = new XFFloat( 0x21, "Hysteresis", "Required change before reporting.", XField.SysConfig );
        public static readonly XFFloat Value = new XFFloat( 0x30, "Value", "Filtered input value.", XField.GuiReadUsage );
        public static readonly XFFloat RawValue = new XFFloat( 0x31, "Raw Value", "Current unfiltered input value.", XField.GuiReadUsage );
        //public static readonly XFWord RawValue = new XFWord( 0x31, "Raw Value" );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            //var fields = new XField[] { FilterDepth, Hysteresis, Value, RawValue };
            var fields = new XField[] { FilterDepth, Hysteresis, Value, RawValue };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapMonitoredInput : __XMapBase {
        public static readonly XFByte LogicalState = new XFByte( 0x30, "Logical State", "Logical current state of this input.", XField.GuiReadUsage );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { LogicalState };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }


    public class XMapAnalogMonitor : __XMapBase {
        public static readonly XFWord OversampledValue = new XFWord( 0x20, "Oversampled Value", "", XField.GuiReadUsage );
        public static readonly XFArray SlowFilteredValue = new XFArray( 0x21, "Slow Filtered Value", "", XField.GuiReadUsage );
        public static readonly XFWord DisturbanceEvent = new XFWord( 0x22, "Disturbance Event", "", XField.GuiReadUsage );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { OversampledValue, SlowFilteredValue, DisturbanceEvent };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapRelayOutput : __XMapBase {
        public static readonly XFBool StartupActive = new XFBool( 0x20, "Startup Active", "", XField.SysConfig );
        public static readonly XFWord SettleActiveDelay = new XFWord( 0x21, "Settle Active Delay", "", XField.SysConfig );
        public static readonly XFWord SettleInactiveDelay = new XFWord( 0x22, "Settle Inactive Delay", "", XField.SysConfig );
        public static readonly XFBool IsActive = new XFBool( 0x30, "Is Active", "", XField.GuiReadWriteUsage );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { StartupActive, SettleActiveDelay, SettleInactiveDelay, IsActive };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapFilterBank : __XMapBase {
        public static readonly XFWord ChannelCount = new XFWord( 0x20, "Channel Count", "", XField.GuiReadUsage );
        public static readonly XFWord SamplingFrequency = new XFWord( 0x21, "Sampling Frequency", "", XField.GuiReadUsage );
        public static readonly XFArray ChannelFrequencies = new XFArray( 0x22, "Channel Frequencies", "", XField.GuiReadUsage );
        public static readonly XFArray ChannelAverageLevels = new XFArray( 0x30, "Average Levels", "", XField.GuiReadUsage );
        public static readonly XFArray ChannelSignalLevels = new XFArray( 0x31, "Signal Levels", "", XField.GuiReadUsage );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
                ChannelCount, SamplingFrequency, ChannelFrequencies,
                ChannelAverageLevels, ChannelSignalLevels, 
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapFibrePort : __XMapBase {
        // filter configuration.
        public static readonly XFWord EnabledChannels = new XFWord( 0x20, "Enabled Channels", "", XField.SysConfig );
        public static readonly XFFloat BaseSensitivity = new XFFloat( 0x21, "Base Sensitivity", "", XField.SysConfig );
        // if any given channel sensitivity is set to zero, that channel is essentially disabled.
        public static readonly XFArray ChannelSensitivities = new XFArray( 0x22, "Channel Sensitivities", "", XField.SysConfig );
        // ambient is the signal activity on the low frequency bands, is used to determine environmental effects.
        // the idea is that wind, rain etc, will cause noise at these frequencies,
        //    and this is indicative of the possibility of greater spurious events at the monitored frequencies.
        public static readonly XFByte AmbientNoiseFactor = new XFByte( 0x23, "Ambient Noise Factor", "", XField.SysConfig );
        public static readonly XFByte AmbientPeakFactor = new XFByte( 0x24, "Ambient Peak Factor", "", XField.SysConfig );
        // reporting-periods returns reports at given period, x 16-bit millisec values, one value each for:
        //     inactive condition, active condition
        public static readonly XFArray ReportingPeriods = new XFArray( 0x30, "Reporting Periods", "", XField.SysConfig );
        // x4 array of x 16-bit flags for which specific data fields to report under following conditions:
        //     inactive, start-of-event, active (during-event), end-of-event.
        // individual fields are as specified for ReportUpdate message below.
        public static readonly XFArray ReportingFields = new XFArray( 0x31, "Reporting Fields", "", XField.SysConfig );
        // data fields are sent in order as follows (some can be missing), if the appropriate bit-flag is set:
        //     x 1 x 16-bit reported-fields-flags in this message.
        //     x 1 composite currently-active-channels-flags word
        //     x 4 sensors individual currently-active-channels-flags word
        //     x 4 sensors relative contributory value byte (normalized to 8 bits)
        //     x 8-bit level activity, summed over x 4 sensors, one per active channel, 0x80 = active level
        //     x 8-bit level activity, one per x 4 sensors, one per active channel, 0x80 = active level
        public static readonly XFArray ReportUpdate = new XFArray( 0x32, "Report Update" );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
                EnabledChannels, 
                BaseSensitivity, ChannelSensitivities, 
                AmbientNoiseFactor, AmbientPeakFactor, 
                ReportingPeriods, ReportingFields,
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapADCBank : __XMapBase {
        public static readonly XFArray OversampledValues = new XFArray( 0x20, "Oversampled Values", "", XField.GuiReadUsage );
        public static readonly XFArray SlowFilteredValues = new XFArray( 0x21, "Slow Filtered Values", "", XField.GuiReadUsage );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { OversampledValues, SlowFilteredValues };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }


    // serial comms definitions.
    // ---------------------------

    public class XMapSerialPort : __XMapBase {
        // StatusFlags is R/O, current state of port, eg. logging enabled etc *specific* to serial ports.
        // report condition changes, nulls discarded, empty-tx/full-rx buffer.
        // __NOTE__ this does not include RS232 pin changes etc. which are handled in XMapRS232.

        //// port is opened or not, write 0 to close, non-0 to open.
        //public static readonly XFBool IsOpen = new XFBool( 0x20 );

        // signed 32-bit integer baudrate value.
        public static readonly XFInteger Baudrate = new XFInteger( 0x21, "Baudrate", "", XField.SysConfig );
        // number of data bits per character.
        public static readonly XFByte DataBits = new XFByte( 0x22, "Data Bits", "", XField.SysConfig );
        // number of stop bits per character.
        public static readonly XFByte StopBits = new XFByte( 0x23, "Stop Bits", "", XField.SysConfig );
        // as for System.IO.Ports.Parity enum.
        public static readonly XFByte Parity = new XFByte( 0x24, "Parity", "", XField.SysConfig );
        // if framing & parity errors on receive must be replaced.
        public static readonly XFBool MustReplaceErrorChar = new XFBool( 0x25, "Must Replace Error Char", "", XField.SysConfig );
        // char to replace broken receive chars (if enabled).
        public static readonly XFByte ReplaceErrorCharWith = new XFByte( 0x26, "Replace Error Char With", "", XField.SysConfig );
        // if must throw away all received nulls.
        public static readonly XFBool MustDiscardNull = new XFBool( 0x27, "Must Discard Null", "", XField.SysConfig );

        // __NOTE__ buffer sizes and counts are int (signed 32-bit),
        //    but only word length data maximum may be transported in any one gulp!

        // R/O, size of transmit buffer in chars, fixed in hardware.
        public static readonly XFInteger TxBufferSize = new XFInteger( 0x30, "Tx Buffer Size", "", XField.GuiReadUsage, true );
        // count of data currently contained in transmit buffer.
        public static readonly XFInteger TxDataCount = new XFInteger( 0x31, "Tx Data Count", "", XField.GuiReadUsage, true );
        public static readonly XFInteger TxSpaceCount = new XFInteger( 0x32, "Tx Space Count", "", XField.GuiReadUsage, true );
        public static readonly XFVoid TxDataClear = new XFVoid( 0x33, "Tx Data Clear", "", XField.AdminUpUsage | XField.GuiInvokeUsage );
        // read/write access to bytes stored in transmit queue.
        // (note that reading removes from head of queue)
        public static readonly XFArray TxBufferPipe = new XFArray( 0x34, "Tx Buffer Pipe" );

        // R/O, size of receive buffer in chars, fixed in hardware.
        public static readonly XFInteger RxBufferSize = new XFInteger( 0x40, "Rx Buffer Size", "", XField.GuiReadUsage, true );
        // count of data currently contained in receive buffer.
        public static readonly XFInteger RxDataCount = new XFInteger( 0x41, "Rx Data Count", "", XField.GuiReadUsage, true );
        public static readonly XFInteger RxSpaceCount = new XFInteger( 0x42, "Rx Space Count", "", XField.GuiReadUsage, true );
        public static readonly XFVoid RxDataClear = new XFVoid( 0x43, "Rx Data Clear", "", XField.AdminUpUsage | XField.GuiInvokeUsage );
        // read/write access to bytes stored in receive queue.
        // (note that reading removes from head of queue)
        public static readonly XFArray RxBufferPipe = new XFArray( 0x44, "Rx Buffer Pipe" );

        // select receive buffer notification thresholds: count, timeout, matches.
        // thresholding can be enabled/disabled only by value, typically 0 or "" to disable.
        // reports data if count greater or equal to threshold count.
        // set value to zero to disable thresholding on the count.
        public static readonly XFWord RxCountThreshold = new XFWord( 0x52, "Rx Count Threshold", "", XField.SysConfig );
        // reports data if quiet for same or longer than timeout (aMillisecPeriod).
        // set value to zero to disable thresholding by timeout.
        public static readonly XFWord RxTimeoutThreshold = new XFWord( 0x53, "Rx Timeout Threshold", "", XField.SysConfig );
        // reports data if incoming char matches specified any of given string values.
        // set value to "" (empty string) to disable matching on this value.
        public static readonly XFArray RxMatch1Threshold = new XFArray( 0x54, "Rx Match 1 Threshold", "", XField.SysConfig );
        public static readonly XFArray RxMatch2Threshold = new XFArray( 0x55, "Rx Match 2 Threshold", "", XField.SysConfig );
        public static readonly XFArray RxMatch3Threshold = new XFArray( 0x56, "Rx Match 3 Threshold", "", XField.SysConfig );
        public static readonly XFArray RxMatch4Threshold = new XFArray( 0x57, "Rx Match 4 Threshold", "", XField.SysConfig );

        public static readonly XFVoid AutobaudStart = new XFVoid( 0x60, "Autobaud Start", "", XField.AdminUpUsage | XField.GuiInvokeUsage );

        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
                Baudrate,DataBits,StopBits,Parity,
                MustReplaceErrorChar,ReplaceErrorCharWith,MustDiscardNull,
                TxBufferSize,TxDataCount,TxSpaceCount,TxDataClear,TxBufferPipe,
                RxBufferSize,RxDataCount,RxSpaceCount,RxDataClear,RxBufferPipe,
                RxCountThreshold,RxTimeoutThreshold,
                RxMatch1Threshold,RxMatch2Threshold,RxMatch3Threshold,RxMatch4Threshold,
                AutobaudStart
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }


    public class XMapRS485 : __XMapBase {
    }
    public class XMapRS422 : __XMapBase {
    }



    public class __XMapRS232 : __XMapBase {
        // __NOTE__ this is handled through the status flags!
        // in and out states of various pins, eg. CTS (in), RTS (out) etc. 
        // (some are R/O, exactly which depends on DTE/DCE (male/female))
        //public static readonly XFByte GetPinState = new XFByte( 0x20, true );
        //public static readonly XFByte SetPinActive = new XFByte( 0x21 );
        //public static readonly XFByte SetPinInactive = new XFByte( 0x22 );

        // as for System.IO.Ports.Handshake enum.
        public static readonly XFByte HandshakeMode = new XFByte( 0x30, "Handshake Mode", "", XField.SysConfig );

        // following is only supported by some implementations.
        // switches off all drivers on output pins, all input pins set hi-impedance except RX.
        public static readonly XFBool PassiveReceiveOnly = new XFBool( 0x31, "Passive Receive Only", "", XField.SysConfig );
        public static new List<XField> GetClonedFields () {
            // all fields defined for the full map type.
            var fields = new XField[] { 
                HandshakeMode,PassiveReceiveOnly,
            };
            return AppendLiveFields( __XMapBase.GetClonedFields(), fields );
        }
    }

    public class XMapRS232DCE : __XMapRS232 {
    }

    public class XMapRS232DTE : __XMapRS232 {
    }

    // Modbus "LinkOnly" maps accept an array, and assume it is a complete and legitimate packet.
    // the map will manage only the timing associated with the selected protocol (Ascii or RTU).
    // returned data is also transparent to the card, and returned directly to the caller with no checking.
    class XMapModbusAsciiLinkOnly : __XMapBase {
        public static readonly XFArray Command = new XFArray( 0x20, "Command" );
    }
    class XMapModbusRTULinkOnly : __XMapBase {
        public static readonly XFArray Command = new XFArray( 0x20, "Command" );
    }

    // these maps will accept commands on the "Modbus" maps,
    //   and format the data, and handle the link layer appropriately for either Ascii or RTU, as selected.
    // they don't actually have any fields (except interface status) but are checked for being enabled 
    //  to select which of the formatting and link layer behaviour is required.
    class XMapModbusAscii : __XMapBase {
    }
    class XMapModbusRTU : __XMapBase {
    }


    // this map is shared by ModbusAscii and ModbusRTU.
    // handling of the messages in the card is different, depends on which map is enabled.
    public class XMapModbus : __XMapBase {
        // shared commands: set destination device address, strobe to send message out.
        // if destination address is not changed, it will be reused unchanged on next message.
        public static readonly XFByte DestinationDevice = new XFByte( 0x70, "DestinationDevice" );
        public static readonly XFVoid SendCommand = new XFVoid( 0x71, "SendCommand" );

        // specific function commands.
        public static readonly XFWord ReadCoilsStartAddress = new XFWord( 0x20, "ReadCoilsStartAddress" );
        public static readonly XFWord ReadCoilsCount = new XFWord( 0x21, "ReadCoilsCount" );

        public static readonly XFWord ReadDiscreteInputsStartAddress = new XFWord( 0x22, "ReadDiscreteInputsStartAddress" );
        public static readonly XFWord ReadDiscreteInputsCount = new XFWord( 0x23, "ReadDiscreteInputsCount" );

        public static readonly XFWord ReadHoldingRegistersStartAddress = new XFWord( 0x24, "ReadHoldingRegistersStartAddress" );
        public static readonly XFWord ReadHoldingRegistersCount = new XFWord( 0x25, "ReadHoldingRegistersCount" );

        public static readonly XFWord ReadInputRegistersStartAddress = new XFWord( 0x26, "ReadInputRegistersStartAddress" );
        public static readonly XFWord ReadInputRegistersCount = new XFWord( 0x27, "ReadInputRegistersCount" );

        public static readonly XFWord WriteSingleCoilAddress = new XFWord( 0x28, "WriteSingleCoilAddress" );
        public static readonly XFByte WriteSingleCoilValue = new XFByte( 0x29, "WriteSingleCoilValue" );

        public static readonly XFWord WriteCoilsStartAddress = new XFWord( 0x2A, "WriteCoilsStartAddress" );
        public static readonly XFWord WriteCoilsCount = new XFWord( 0x2B, "WriteCoilsCount" );
        public static readonly XFArray WriteCoilsValue = new XFArray( 0x2C, "WriteCoilsValue" );

        public static readonly XFWord WriteSingleRegisterAddress = new XFWord( 0x2D, "WriteSingleRegisterAddress" );
        public static readonly XFWord WriteSingleRegisterValue = new XFWord( 0x2E, "WriteSingleRegisterValue" );

        public static readonly XFWord WriteRegistersStartAddress = new XFWord( 0x2F, "WriteRegistersStartAddress" );
        public static readonly XFWord WriteRegistersCount = new XFWord( 0x30, "WriteRegistersCount" );
        public static readonly XFArray WriteRegistersValue = new XFArray( 0x31, "WriteRegistersValue" );

        public static readonly XFVoid ReadExceptionStatus = new XFVoid( 0x32, "ReadExceptionStatus" );

        public static readonly XFByte WriteDiagnosticsSubfunction = new XFByte( 0x33, "WriteDiagnosticsSubfunction" );
        public static readonly XFByte WriteDiagnosticsData = new XFByte( 0x34, "WriteDiagnosticsData" );

        public static readonly XFVoid ReadCommEventCounter = new XFVoid( 0x35, "ReadCommEventCounter" );

        public static readonly XFVoid ReadCommEventLog = new XFVoid( 0x36, "ReadCommEventLog" );

        public static readonly XFVoid ReportSlaveID = new XFVoid( 0x37, "ReportSlaveID" );

        // read and write file records are limited to a single record at a time.
        public static readonly XFByte ReadFileRecordReferenceType = new XFByte( 0x38, "ReadFileRecordReferenceType" );
        public static readonly XFWord ReadFileRecordFileNumber = new XFWord( 0x39, "ReadFileRecordFileNumber" );
        public static readonly XFWord ReadFileRecordRecordNumber = new XFWord( 0x3A, "ReadFileRecordRecordNumber" );
        public static readonly XFWord ReadFileRecordRecordLength = new XFWord( 0x3B, "ReadFileRecordRecordLength" );

        public static readonly XFByte WriteFileRecordReferenceType = new XFByte( 0x3C, "WriteFileRecordReferenceType" );
        public static readonly XFWord WriteFileRecordFileNumber = new XFWord( 0x3D, "WriteFileRecordFileNumber" );
        public static readonly XFWord WriteFileRecordRecordNumber = new XFWord( 0x3E, "WriteFileRecordRecordNumber" );
        public static readonly XFWord WriteFileRecordRecordLength = new XFWord( 0x3F, "WriteFileRecordRecordLength" );
        public static readonly XFArray WriteFileRecordData = new XFArray( 0x40, "WriteFileRecordData" );

        public static readonly XFWord MaskWriteRegisterAddress = new XFWord( 0x41, "MaskWriteRegisterAddress" );
        public static readonly XFWord MaskWriteRegisterAndMask = new XFWord( 0x42, "MaskWriteRegisterAndMask" );
        public static readonly XFWord MaskWriteRegisterOrMask = new XFWord( 0x43, "MaskWriteRegisterOrMask" );

        public static readonly XFWord ReadWriteRegistersReadStartAddress = new XFWord( 0x44, "ReadWriteRegistersReadStartAddress" );
        public static readonly XFWord ReadWriteRegistersReadCount = new XFWord( 0x45, "ReadWriteRegistersReadCount" );
        public static readonly XFWord ReadWriteRegistersWriteStartAddress = new XFWord( 0x46, "ReadWriteRegistersWriteStartAddress" );
        public static readonly XFWord ReadWriteRegistersWriteCount = new XFWord( 0x47, "ReadWriteRegistersWriteCount" );
        public static readonly XFArray ReadWriteRegistersWriteValue = new XFArray( 0x48, "ReadWriteRegistersWriteValue" );

        public static readonly XFWord ReadFifoQueueStartAddress = new XFWord( 0x49, "ReadFifoQueueStartAddress" );

        public static readonly XFByte ReadDeviceIdentificationCode = new XFByte( 0x4A, "ReadDeviceIdentificationCode" );
        public static readonly XFByte ReadDeviceIdentificationObject = new XFByte( 0x4B, "ReadDeviceIdentificationObject" );
    }

    // if ( element implements ModbusRTU ) {
    //     package command in RTU format and send.
    // } else if ModbusAscii ...
    // else an error if received a Modbus command but no active Modbus protocol interface.


}




//// keypad management.
//// -------------------------
//public enum KeypadProc {
//    GetSupport = 0x00,

//    GetConfig = 0x01,
//    ReadStates = 0x02,

//    SetConfig = 0x81,
//    Flush = 0x82,
//    RepEvent = 0x83,
//    Remoted = 0x84,
//}



//// text screen management.
//// ------------------------------
//public enum TextScreenProc {
//    GetSupport = 0x00,

//    GetConfig = 0x01,
//    GetXYPos = 0x02,

//    SetConfig = 0x81,
//    SetXYPos = 0x82,
//    Write = 0x83,
//    XYClearEoln = 0x84,
//    Clear = 0x85,
//    XYWrite = 0x86,
//    ClearEoln = 0x87,
//    XYWriteClearEoln = 0x88,
//    ClearLn = 0x89,
//}



//// LED management.
//// ---------------------
//public enum LedsProc {
//    GetSupport = 0x00,

//    GetConfig = 0x01,
//    GetState = 0x02,
//    GetSelStates = 0x03,

//    SetConfig = 0x81,
//    SetState = 0x82,
//    SetSelStatesSame = 0x83,
//    SetSelStatesEach = 0x84,
//    SetAllStatesSame = 0x85,
//}



//// Buzzer management.
//// ------------------------
//public enum BuzzerProc {
//    GetSupport = 0x00,

//    GetConfig = 0x01,
//    GetState = 0x02,

//    SetConfig = 0x81,
//    SetState = 0x82,
//    Pulse = 0x83,
//    Play = 0x84,
//}


//// Joystick management.
//// -------------------------
//public enum JoystickProc {
//    GetSupport = 0x00,

//    GetConfig = 0x01,
//    GetStatus = 0x02,

//    SetConfig = 0x81,
//    RepStatus = 0x82,
//    Remoted = 0x83,
//}





