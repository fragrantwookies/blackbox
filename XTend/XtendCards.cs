﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
#endregion

namespace XPointAudiovisual.Xtend {

    // card-specific proxy classes that surface the appropriate elements for given card hardware.
    public enum XCardType {
        EmptySlot = 0,

        // network (PoE) powered versions of network interface card.
        Ether_POE_001_100 = 1, // no secondary function elements.
        Ether_POE_002_100, // with RS232 DCE.
        Ether_POE_M0003_v0100, // with RS232 DTE.
        Ether_POE_004_100, // with RS485.
        Ether_POE_005_100, // with RS422.
        Ether_POE_006_100, // with CAN.

        // externally-powered-only versions of network interface card.
        Ether_PEX_011_100 = 11, // no secondary function elements.
        Ether_PEX_012_100, // with RS232 DCE.
        Ether_PEX_013_100, // with RS232 DTE.
        Ether_PEX_014_100, // with RS485.
        Ether_PEX_015_100, // with RS422.
        Ether_PEX_016_100, // with CAN.

        // serial port cards.
        SerialRS232DCE_DB9Female_M0101_v0100 = 101,
        SerialRS232DTE_DB9Male_M0102_v0100,
        SerialRS485_RJ45_103_100,
        SerialRS422_RJ45_104_100,
        SerialMultiPhy_M0105_v0100,

        // GPIO cards.
        GPIO_2RelayOut_4DigitalIn_M0201_v0100 = 201,
        GPIO_8AnalogIn_5V_ADC10_M0202_v0100 = 202,
        GPIO_8AnalogIn_24Vdc_ADC10_M0203_v0100 = 203,
        GPIO_8DigitalOut_M0204_v0100 = 204,

        // access control cards.
        AC_2RelayOut_2DigitalOut_1WiegandIn_M0301_v0100 = 301,
        REF_2RelayOut_2OneWire_1AC_M0302_v0100 = 302,

        // environmental sensing cards.
        ES_Fibre_Disturbance_4Sensor_M0401_v0100 = 401, // deprecated!
        FS_Fibre_Sensor_2Zone_Looped_M0402_v0100 = 402,
        FS_Fibre_Sensor_2Zone_Node_M0403_v0100 = 403,
        FS_Fibre_Sensor_2Zone_Base_M0404_v0100 = 404,
        FS_Fibre_Sensor_1Zone_Node_M0405_v0100 = 405,

        GPIO_8OnWire_M0501_v0100 = 501,

        // FSS controller card.
        FSS_Controller_M0601_v0200 = 601,

        // standalone IO bricks.
        CES1_Ether_POE_RS232DTE_MXP3101_v0100 = 3101,
    };

    public class XCProxy_Empty_Slot : XCProxy {
        public XCProxy_Empty_Slot ( XHProxy aParent )
            : base( aParent, XCardType.EmptySlot ) {
            ModelName = "Empty Slot";
            // there's nothing in an empty slot, so get rid of all default elements.
            if ( Elems != null ) {
                foreach ( var e in Elems )
                    e.Dispose();
                Elems.Clear();
            }
        }
    }

    public class XCProxy_Hub_Master_M0003_v0100 : XCProxy {
        public XEEthernetPoENetworkIntf Ethernet;
        public XESerialRS232DTE SerialRS232DTE;
        public XCProxy_Hub_Master_M0003_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.Ether_POE_001_100, true ) {
            ModelName = "XPAV RIO Hub Master, PoE + RS232 DTE, Model 0003, Version 01.00";
            Elems.Add( Ethernet = new XEEthernetPoENetworkIntf( this ) );
            Elems.Add( SerialRS232DTE = new XESerialRS232DTE( this ) );
            AssignElementIndexes();
        }
    }

    public class XCProxy_Hub_Master_M0011_v0100 : XCProxy {
        public XEEthernetPoENetworkIntf Ethernet;
        public XESerialRS232DTE SerialRS232DTE;
        public XCProxy_Hub_Master_M0011_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.Ether_PEX_011_100, true ) {
            ModelName = "XPAV RIO Hub Master, PExt, Model 0001, Version 01.00";
            Elems.Add( Ethernet = new XEEthernetPoENetworkIntf( this ) );
            AssignElementIndexes();
        }
    }

    public class XCProxy_FSS_Hub_Master_M0601_v0200 : XCProxy {
        public XEAnalogInput Temperature;
        public XEAnalogInput Battery;
        public XEAnalogInput SolarPanel;
        public XEAnalogInput OpticalTamper;
        public XEMonitoredInput SwitchTamper;
        public XEMonitoredInput RFRemote;
        public XEMonitoredInput Passive1;
        public XEMonitoredInput Passive2;
        public XEDigitalOutput Strobe1;
        public XEDigitalOutput Strobe2;
        public XEDigitalOutput Signal1;
        public XEDigitalOutput Signal2;
        public XCProxy_FSS_Hub_Master_M0601_v0200 ( XHProxy aParent )
            : base( aParent, XCardType.Ether_POE_001_100, true ) {
            ModelName = "XPAV FSS Hub Master, Solar + Fibre + Strobes + Passives, Model 0601, Version 02.00";
            Elems.Add( Temperature = new XEAnalogInput( this, 0, 0, "Temperature" ) );
            Elems.Add( Battery = new XEAnalogInput( this, 0, 0, "Battery" ) );
            Elems.Add( SolarPanel = new XEAnalogInput( this, 0, 0, "Solar Panel" ) );
            Elems.Add( OpticalTamper = new XEAnalogInput( this, 100.0, 0, "Optical Tamper" ) );
            Elems.Add( SwitchTamper = new XEMonitoredInput( this, "Switch Tamper" ) );
            Elems.Add( RFRemote = new XEMonitoredInput( this, "RF Remote" ) );
            Elems.Add( Passive1 = new XEMonitoredInput( this, "Passive In 1" ) );
            Elems.Add( Passive2 = new XEMonitoredInput( this, "Passive In 2" ) );
            Elems.Add( Strobe1 = new XEDigitalOutput( this, "Strobe Out 1" ) );
            Elems.Add( Strobe2 = new XEDigitalOutput( this, "Strobe Out 2" ) );
            Elems.Add( Signal1 = new XEDigitalOutput( this, "Signal Out 1" ) );
            Elems.Add( Signal2 = new XEDigitalOutput( this, "Signal Out 2" ) );
            AssignElementIndexes();
        }
    }

    public class XCProxy_SerialRS232DCE_DB9Female_M0101_v0100 : XCProxy {
        public readonly XESerialRS232DCE SerialRS232DCE;
        public XCProxy_SerialRS232DCE_DB9Female_M0101_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.SerialRS232DCE_DB9Female_M0101_v0100 ) {
            ModelName = "XPAV RIO Serial-RS232 DCE DB9-Female, Model 0101, Version 01.00";
            Elems.Add( SerialRS232DCE = new XESerialRS232DCE( this ) );
            AssignElementIndexes();
        }
    }

    public class XCProxy_SerialRS232DTE_DB9Male_M0102_v0100 : XCProxy {
        public XESerialRS232DTE SerialRS232DTE;
        public XCProxy_SerialRS232DTE_DB9Male_M0102_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.SerialRS232DCE_DB9Female_M0101_v0100 ) {
            ModelName = "XPAV RIO Serial-RS232 DTE DB9-Male, Model 0102, Version 01.00";
            Elems.Add( SerialRS232DTE = new XESerialRS232DTE( this ) );
            AssignElementIndexes();
        }
    }

    // proxy for Modbus serial card hardware, either RS232, RS485 or RS422 enabled, one at a time.
    public class XCProxy_SerialMultiPhy_M0105_v0100 : XCProxy {
        XESerialMultiPhy multiPhyElement;
        public XCProxy_SerialMultiPhy_M0105_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.SerialMultiPhy_M0105_v0100 ) {
            ModelName = "XPAV RIO Serial (RS232, RS422, RS485), Model 0105, Version 01.00";
            Elems.Add( multiPhyElement = new XESerialMultiPhy( this ) );
            AssignElementIndexes();
        }
    }

    // proxy for standard 2-relay-out, 4-digital-in GPIO card.
    public class XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100 : XCProxy {
        public XERelayOutput Relay_1, Relay_2;
        public XEDigitalInput Input_1, Input_2, Input_3, Input_4;
        public XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.GPIO_2RelayOut_4DigitalIn_M0201_v0100 ) {
            ModelName = "XPAV RIO GPIO (2 relay out, 4 digital in), Model 0201, Version 01.00";
            Relay_1 = new XERelayOutput( this, "Relay Out 1" );
            Relay_2 = new XERelayOutput( this, "Relay Out 2" );
            Elems.AddRange( new List<XEProxy>() { Relay_1, Relay_2 } );
            Input_1 = new XEDigitalInput( this, "Input 1" );
            Input_2 = new XEDigitalInput( this, "Input 2" );
            Input_3 = new XEDigitalInput( this, "Input 3" );
            Input_4 = new XEDigitalInput( this, "Input 4" );
            Elems.AddRange( new List<XEProxy>() { Input_1, Input_2, Input_3, Input_4 } );
            AssignElementIndexes();
        }
    }

    // proxy for standard 8-analog-in, GPIO card, voltage range and ADC accuracy unspecified.
    public class XCProxy_GPIO_8AnalogIn_Base : XCProxy {
        public XEAnalogInput Input_1, Input_2, Input_3, Input_4, Input_5, Input_6, Input_7, Input_8;
        public XCProxy_GPIO_8AnalogIn_Base ( XHProxy aParent, XCardType aCardType, double aRange, int aCalibration )
            : base( aParent, aCardType ) {
            Input_1 = new XEAnalogInput( this, aRange, aCalibration );
            Input_2 = new XEAnalogInput( this, aRange, aCalibration );
            Input_3 = new XEAnalogInput( this, aRange, aCalibration );
            Input_4 = new XEAnalogInput( this, aRange, aCalibration );
            Input_5 = new XEAnalogInput( this, aRange, aCalibration );
            Input_6 = new XEAnalogInput( this, aRange, aCalibration );
            Input_7 = new XEAnalogInput( this, aRange, aCalibration );
            Input_8 = new XEAnalogInput( this, aRange, aCalibration );
            Elems.AddRange( new List<XEProxy>() { Input_1, Input_2, Input_3, Input_4, Input_5, Input_6, Input_7, Input_8 } );
            AssignElementIndexes();
        }
    }
    // DEPRECATED! 24V card has sufficient range to cover this.
    // proxy for standard 8-analog-in, 5V range, 10-bit-accuracy GPIO card.
    public class XCProxy_GPIO_8AnalogIn_5V_ADC10_M0202_v0100 : XCProxy_GPIO_8AnalogIn_Base {
        public XCProxy_GPIO_8AnalogIn_5V_ADC10_M0202_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.GPIO_8AnalogIn_5V_ADC10_M0202_v0100, 5.0, 16689 ) {
            ModelName = "XPAV RIO GPIO (8 analog in, 5Vdc, 10-bit), Model 0202, Version 01.00";
        }
    }
    // proxy for standard 8-analog-in, 24V DC range, 10-bit-accuracy GPIO card.
    public class XCProxy_GPIO_8AnalogIn_24Vdc_ADC10_M0202_v0100 : XCProxy_GPIO_8AnalogIn_Base {
        public XCProxy_GPIO_8AnalogIn_24Vdc_ADC10_M0202_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.GPIO_8AnalogIn_24Vdc_ADC10_M0203_v0100, 12.0, 40085 ) {
            ModelName = "XPAV RIO GPIO (8 analog in, 24Vdc, 10-bit), Model 0203, Version 01.00";
        }
    }
    // proxy for standard 8-digital-out GPIO card.
    public class XCProxy_GPIO_8DigitalOut_M0204_v0100 : XCProxy {
        public XEDigitalOutput Output_1, Output_2, Output_3, Output_4, Output_5, Output_6, Output_7, Output_8;
        public XCProxy_GPIO_8DigitalOut_M0204_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.GPIO_8DigitalOut_M0204_v0100 ) {
            ModelName = "XPAV RIO GPIO (8 digital out), Model 0204, Version 01.00";
            Output_1 = new XEDigitalOutput( this );
            Output_2 = new XEDigitalOutput( this );
            Output_3 = new XEDigitalOutput( this );
            Output_4 = new XEDigitalOutput( this );
            Output_5 = new XEDigitalOutput( this );
            Output_6 = new XEDigitalOutput( this );
            Output_7 = new XEDigitalOutput( this );
            Output_8 = new XEDigitalOutput( this );
            Elems.AddRange( new List<XEProxy>() { Output_1, Output_2, Output_3, Output_4, Output_5, Output_6, Output_7, Output_8 } );
            AssignElementIndexes();
        }
    }

    // __TODO__ 5. this is incomplete!
    // proxy for standard Wiegand-input-device access control card (no local database)
    public class XCProxy_AC_2RelayOut_2DigitalOut_1WiegandIn_M0301_v0100 : XCProxy {
        public XERelayOutput Relay_1, Relay_2;
        public XEDigitalOutput Output_1, Output_2;
        public XCProxy_AC_2RelayOut_2DigitalOut_1WiegandIn_M0301_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.AC_2RelayOut_2DigitalOut_1WiegandIn_M0301_v0100 ) {
            ModelName = "XPAV RIO AC (2 relay out, 2 digital out, 1 Wiegand in), Model 0301, Version 01.00";
            Relay_1 = new XERelayOutput( this );
            Relay_2 = new XERelayOutput( this );
            Elems.AddRange( new List<XEProxy>() { Relay_1, Relay_2 } );
            Output_1 = new XEDigitalOutput( this );
            Output_2 = new XEDigitalOutput( this );
            Elems.AddRange( new List<XEProxy>() { Output_1, Output_2 } );
            AssignElementIndexes();
        }
    }

    // DEPRECATED!
    // proxy for standard 4-sensor fibre disturbance monitor card.
    public class XCProxy_ES_Fibre_Disturbance_4Sensor_M0401_v0100 : XCProxy {
        public XEAnalogMonitor Monitor_1, Monitor_2, Monitor_3, Monitor_4;
        public XCProxy_ES_Fibre_Disturbance_4Sensor_M0401_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.ES_Fibre_Disturbance_4Sensor_M0401_v0100 ) {
            ModelName = "XPAV RIO ES4 (4 optical analog mon), Model 0401, Version 01.00";
            Monitor_1 = new XEAnalogMonitor( this );
            Monitor_2 = new XEAnalogMonitor( this );
            Monitor_3 = new XEAnalogMonitor( this );
            Monitor_4 = new XEAnalogMonitor( this );
            Elems.AddRange( new List<XEProxy>() { Monitor_1, Monitor_2, Monitor_3, Monitor_4 } );
            AssignElementIndexes();
        }
    }

    // proxy for standard 2-sensor fibre disturbance monitor card as simply looped in/outs.
    // this card does *not* support communications over the fibre, it only senses disturbance.
    public class XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100 : XCProxy {
        public XEFibreSensor Sensor_1, Sensor_2;
        public XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.FS_Fibre_Sensor_2Zone_Looped_M0402_v0100 ) {
            ModelName = "XPAV RIO FS2 (2 optical fibre) looped, Model 0402, Version 01.00";
            Sensor_1 = new XEFibreSensor( this, "Fibre 1" );
            Sensor_2 = new XEFibreSensor( this, "Fibre 2" );
            Elems.AddRange( new List<XEProxy>() { Sensor_1, Sensor_2 } );
            AssignElementIndexes();
        }
    }

    // proxy for standard 2-sensor fibre disturbance monitor card as ring node.
    public class XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100 : XCProxy {
        public XEFibreSensor Sensor_1, Sensor_2;
        public XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.FS_Fibre_Sensor_2Zone_Node_M0403_v0100 ) {
            ModelName = "XPAV RIO FS2 (2 optical fibre) node, Model 0403, Version 01.00";
            Sensor_1 = new XEFibreSensor( this, "Fibre 1" );
            Sensor_2 = new XEFibreSensor( this, "Fibre 2" );
            Elems.AddRange( new List<XEProxy>() { Sensor_1, Sensor_2 } );
            AssignElementIndexes();
        }
    }

    // proxy for standard 2-sensor fibre disturbance monitor card as ring base.
    public class XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100 : XCProxy {
        public XEFibreSensor Sensor_1, Sensor_2;
        public XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.FS_Fibre_Sensor_2Zone_Base_M0404_v0100 ) {
            ModelName = "XPAV RIO FS2 (2 optical fibre) base, Model 0404, Version 01.00";
            Sensor_1 = new XEFibreSensor( this, "Fibre 1" );
            Sensor_2 = new XEFibreSensor( this, "Fibre 2" );
            Elems.AddRange( new List<XEProxy>() { Sensor_1, Sensor_2 } );
            AssignElementIndexes();
        }
    }

    // proxy for standard 2 x 1-sensor fibre disturbance monitor card as ring node.
    public class XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100 : XCProxy {
        public XEFibreSensor Sensor_1;
        public XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.FS_Fibre_Sensor_1Zone_Node_M0405_v0100 ) {
            ModelName = "XPAV RIO FS1 (1 optical fibre) node, Model 0405, Version 02.00";
            Sensor_1 = new XEFibreSensor( this, "Fibre 1" );
            Elems.AddRange( new List<XEProxy>() { Sensor_1 } );
            AssignElementIndexes();
        }
    }

    // proxy for standard 8 x 1-wire extension card.
    public class XCProxy_GPIO_8OnWire_M0501_v0100 : XCProxy {
        public XCProxy_GPIO_8OnWire_M0501_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.GPIO_8OnWire_M0501_v0100, false ) {
            ModelName = "XPAV RIO GPIO (8 OnWire ports), Model 0501, Version 01.00";
            for ( int i = 0; i < 32; ++i )
                Elems.Add( new XEOnWireDevice( this ) );
            AssignElementIndexes();
            // Elems[0] is the Admin element !
            // DEBUG CODE!
            ( (XEOnWireDevice)( Elems[ 1 ] ) ).Present = true;
            ( (XEOnWireDevice)( Elems[ 3 ] ) ).Present = true;
            ( (XEOnWireDevice)( Elems[ 11 ] ) ).Present = true;
        }
    }



    // proxy for CES-1 serial input brick (standalone)
    public class XCProxy_CES1_XP3101_v0100 : XCProxy {
        public XEEthernetPoENetworkIntf Ethernet;
        public XESerialRS232DTE SerialRS232DTE;
        public XCProxy_CES1_XP3101_v0100 ( XHProxy aParent )
            : base( aParent, XCardType.Ether_POE_001_100, true ) {
            ModelName = "XPAV RIO CES-1 (PoE + RS232 DTE), Model XP3101, Version 01.00";
            Elems.Add( Ethernet = new XEEthernetPoENetworkIntf( this ) );
            Elems.Add( SerialRS232DTE = new XESerialRS232DTE( this ) );
            AssignElementIndexes();
        }
    }


    public static class XCardTypeLookup {
        public static XCProxy Get ( XHProxy aParent, XCardType aCardType ) {
            switch ( aCardType ) {
                case XCardType.EmptySlot: return new XCProxy_Empty_Slot( aParent );
                case XCardType.Ether_POE_001_100: return new XCProxy_Hub_Master_M0003_v0100( aParent );
                case XCardType.Ether_POE_M0003_v0100: return new XCProxy_Hub_Master_M0003_v0100( aParent );
                case XCardType.Ether_PEX_011_100: return new XCProxy_Hub_Master_M0011_v0100( aParent );
                case XCardType.FSS_Controller_M0601_v0200: return new XCProxy_FSS_Hub_Master_M0601_v0200( aParent );
                case XCardType.SerialRS232DCE_DB9Female_M0101_v0100: return new XCProxy_SerialRS232DCE_DB9Female_M0101_v0100( aParent );
                case XCardType.SerialRS232DTE_DB9Male_M0102_v0100: return new XCProxy_SerialRS232DTE_DB9Male_M0102_v0100( aParent );
                case XCardType.SerialMultiPhy_M0105_v0100: return new XCProxy_SerialMultiPhy_M0105_v0100( aParent );
                case XCardType.GPIO_2RelayOut_4DigitalIn_M0201_v0100: return new XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100( aParent );
                case XCardType.GPIO_8AnalogIn_5V_ADC10_M0202_v0100: return new XCProxy_GPIO_8AnalogIn_5V_ADC10_M0202_v0100( aParent );
                case XCardType.GPIO_8AnalogIn_24Vdc_ADC10_M0203_v0100: return new XCProxy_GPIO_8AnalogIn_24Vdc_ADC10_M0202_v0100( aParent );
                case XCardType.GPIO_8DigitalOut_M0204_v0100: return new XCProxy_GPIO_8DigitalOut_M0204_v0100( aParent );
                case XCardType.AC_2RelayOut_2DigitalOut_1WiegandIn_M0301_v0100: return new XCProxy_AC_2RelayOut_2DigitalOut_1WiegandIn_M0301_v0100( aParent );
                case XCardType.ES_Fibre_Disturbance_4Sensor_M0401_v0100: return new XCProxy_ES_Fibre_Disturbance_4Sensor_M0401_v0100( aParent );
                case XCardType.FS_Fibre_Sensor_2Zone_Looped_M0402_v0100: return new XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100( aParent );
                case XCardType.FS_Fibre_Sensor_2Zone_Node_M0403_v0100: return new XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100( aParent );
                case XCardType.FS_Fibre_Sensor_2Zone_Base_M0404_v0100: return new XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100( aParent );
                case XCardType.FS_Fibre_Sensor_1Zone_Node_M0405_v0100: return new XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100( aParent );
                case XCardType.GPIO_8OnWire_M0501_v0100: return new XCProxy_GPIO_8OnWire_M0501_v0100( aParent );
                // __WARN__ something wrong here!
                // __TODO__ 0. fix it.
                //case XCardType.CES1_Ether_POE_RS232DTE_MXP3101_v0100: return new XCProxy_CES1_XP3101_v0100( aParent );
                case XCardType.CES1_Ether_POE_RS232DTE_MXP3101_v0100: return new XCProxy_Hub_Master_M0003_v0100( aParent );
            }
            return null;
        }
    }
}
