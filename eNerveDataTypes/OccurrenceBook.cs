﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace eNerve.DataTypes
{
    [Serializable]
    public class OccurrenceBook : OBEntry, IDisposable, IEquatable<OccurrenceBook>
    {
        protected int idNumber;

        [field: NonSerialized]
        public event PropertyUpdateEventHandler PropertyUpdated;

        protected ObservableCollection<OBSubEntry> subEntries;

        [NonSerialized]
        private bool expand = false;

        [NonSerialized]
        private string newDescription = "";
        [NonSerialized]
        private string newTypeDescription = "";

        protected ObservableCollection<OBType> subTypes;

        public OccurrenceBook() 
            : base() 
        {
            SubEntries = new ObservableCollection<OBSubEntry>();
        }

        public OccurrenceBook(string _occurrenceType, string _description)
            : base(_occurrenceType, _description)
        {
            SubEntries = new ObservableCollection<OBSubEntry>();
        }

        public OccurrenceBook(Guid _id)
            : base(_id)
        {
            SubEntries = new ObservableCollection<OBSubEntry>();
        }

        public void Dispose()
        {
            if (SubEntries != null)
            {
                SubEntries.Clear();
                SubEntries = null;
            }
        }

        public int IDNumber
        {
            get { return idNumber; }
            set 
            { 
                idNumber = value; 
                PropertyChangedHandler("IDNumber");

                if (PropertyUpdated != null)
                    PropertyUpdated(this, new PropertyUpdate(this.ID, "IDNumber", value));
            }
        }

        public ObservableCollection<OBSubEntry> SubEntries
        {
            get
            {
                if (subEntries == null)
                    subEntries = new ObservableCollection<OBSubEntry>();

                return subEntries;
            }
            set
            {
                subEntries = value; PropertyChangedHandler("SubEntries"); ;
            }
        }

        public bool Expand
        {
            get { return expand; }
            set { expand = value; PropertyChangedHandler("Expand"); }
        }

        public string NewDescription
        {
            get { return newDescription; }
            set { newDescription = value; PropertyChangedHandler("NewDescription"); }
        }

        public string NewTypeDescription
        {
            get { return newTypeDescription; }
            set { newTypeDescription = value; PropertyChangedHandler("NewTypeDescription"); }
        }

        public ObservableCollection<OBType> SubTypes
        {
            get { return subTypes; }
            set { subTypes = value; PropertyChangedHandler("SubTypes"); }
        }

        public bool Equals(OccurrenceBook other)
        {
            return this.ID.Equals(other.ID);
        }
    }

    [Serializable]
    public class OBSubEntry : OBEntry, IEquatable<OBSubEntry>
    {
        protected Guid obID;

        public OBSubEntry() : base() { }

        public OBSubEntry(Guid _OBID, string _occurrenceType, string _description) 
            : base(_occurrenceType, _description)
        {
            OBID = _OBID;
        }

        public OBSubEntry(Guid _id) : base(_id) { }

        public Guid OBID
        {
            get { return obID; }
            set { obID = value; PropertyChangedHandler("OBID"); }
        }

        public bool Equals(OBSubEntry other)
        {
            return this.ID.Equals(other.ID);
        }
    }

    [Serializable]
    public class OBEntry : System.ComponentModel.INotifyPropertyChanged, IEquatable<OBEntry>
    {
        protected Guid id;
        protected string occurrenceType;
        protected string description;
        protected DateTime occurrenceDT;
        protected User user;

        public OBEntry()
        {
            ID = Guid.NewGuid();
            OccurrenceDT = DateTime.Now;
        }

        public OBEntry(string _occurrenceType, string _description)
        {
            OccurrenceType = _occurrenceType;
            Description = _description;
            ID = Guid.NewGuid();
            OccurrenceDT = DateTime.Now;
        }

        public OBEntry(Guid _id)
        {
            ID = _id;
        }

        public Guid ID
        {
            get { return id; }
            set { id = value; PropertyChangedHandler("ID"); }
        }

        public string OccurrenceType
        {
            get { return occurrenceType; }
            set { occurrenceType = value; PropertyChangedHandler("OccurrenceType"); }
        }

        public string Description
        {
            get { return description; }
            set { description = value; PropertyChangedHandler("Description"); }
        }

        public DateTime OccurrenceDT
        {
            get { return occurrenceDT; }
            set { occurrenceDT = value; PropertyChangedHandler("OccurrenceDT"); }
        }

        public User User
        {
            get { return user; }
            set { user = value; PropertyChangedHandler("User"); }
        }

        public string UserName
        {
            get
            {
                return user != null ? user.UserName : "";
            }
        }

        public bool Equals(OBEntry other)
        {
            return this.ID.Equals(other.ID);
        }

        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }

        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    }

    [Serializable]
    public class OccurrenceBookHistoryFilter
    {
        public string TypeDescription { get; set; }
        public string Description { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string UserName { get; set; }

        public OccurrenceBookHistoryFilter() 
        {
            StartDate = null;
            EndDate = null;
            TypeDescription = "";
            Description = "";
            UserName = "";
        }

        public OccurrenceBookHistoryFilter(DateTime? _StartDate, DateTime? _EndDate, string _TypeDescription, string _Description, string _UserName)
        {
            StartDate = _StartDate;
            EndDate = _EndDate;
            TypeDescription = _TypeDescription;
            Description = _Description;
            UserName = _UserName;
        }

        /// <summary>
        /// Test if Occurrence Book passes through the filter
        /// </summary>
        /// <param name="_obEntry"></param>
        /// <param name="_filter"></param>
        /// <returns></returns>
        public static bool Test(OBEntry _obEntry, OccurrenceBookHistoryFilter _filter)
        {
            bool result = true;

            if (_filter != null && _obEntry != null)
            {
                if (_filter.StartDate != null)
                    if (_obEntry.OccurrenceDT < _filter.StartDate)
                        result = false;

                if (_filter.EndDate != null)
                    if (_obEntry.OccurrenceDT > _filter.EndDate)
                        result = false;

                if (!string.IsNullOrWhiteSpace(_filter.TypeDescription))
                    if (!_obEntry.OccurrenceType.Contains(_filter.TypeDescription))
                        result = false;

                if (!string.IsNullOrWhiteSpace(_filter.Description))
                    if (!_obEntry.Description.Contains(_filter.Description))
                        result = false;

                if (!string.IsNullOrWhiteSpace(_filter.UserName))
                    if (!_obEntry.User.Equals(_filter.UserName))
                        result = false;
            }

            return result;
        }
    }

    [Serializable]
    public class OBType : System.ComponentModel.INotifyPropertyChanged, IDisposable
    {
        protected string typeDescription;

        protected ObservableCollection<OBType> subTypes;

        public OBType() 
        {
            subTypes = new ObservableCollection<OBType>();
        }

        public OBType(string _typeDescription)
        {
            typeDescription = _typeDescription;

            subTypes = new ObservableCollection<OBType>();
        }

        ~OBType()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            subTypes.Clear();
        }

        public static ObservableCollection<OBType> FindSubTypes(IList<OBType> _types, string _typeDesc)
        {
            ObservableCollection<OBType> result = null;

            for (int i = 0; i < _types.Count; i++)
            {
                if (_types[i].TypeDescription == _typeDesc)
                {
                    result = _types[i].SubTypes;
                    break;
                }
            }

            return result;
        }

        public string TypeDescription
        {
            get { return typeDescription; }
            set { typeDescription = value; PropertyChangedHandler("TypeDescription"); }
        }

        public ObservableCollection<OBType> SubTypes
        {
            get { return subTypes; }
            set { subTypes = value; }
        }

        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }

        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    }
}
