﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    public class StatusMessage : IDisposable, IEquatable<StatusMessage>, IEquatable<Guid>
    {
        public StatusMessage() { }

        #region Cleanup
        ~StatusMessage()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (autoRemoveTimer != null)
            {
                autoRemoveTimer.Stop();
                autoRemoveTimer.Elapsed -= autoRemoveTimer_Elapsed;
                autoRemoveTimer.Dispose();
                autoRemoveTimer = null;
            }
        }
        #endregion Cleanup

        #region Properties
        public Guid StatusID { get; set; }

        public string Message { get; set; }
        public DateTime MessageTimeStamp { get; set; }

        /// <summary>
        /// Only display message for specified time. Zero to display indefinitely.
        /// </summary>
        public TimeSpan AutoRemove { get; set; }

        /// <summary>
        /// Use this to indicate client should display some sort of indication that the server is busy processing it's request.
        /// </summary>
        public bool Animation { get; set; }
        #endregion Properties

        #region Auto Remove
        [NonSerialized]
        private System.Timers.Timer autoRemoveTimer;

        public void StartAutoRemoveReminder()
        {
            if (AutoRemove != null)
            {
                if (AutoRemove.TotalMilliseconds >= 1)
                {
                    if (autoRemoveTimer == null)
                    {
                        autoRemoveTimer = new System.Timers.Timer();
                        autoRemoveTimer.Elapsed += autoRemoveTimer_Elapsed;
                    }

                    autoRemoveTimer.Interval = AutoRemove.TotalMilliseconds;
                    autoRemoveTimer.Start();
                }
            }
        }

        void autoRemoveTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            autoRemoveTimer.Stop();

            if (OnRemoveNotification != null)
                OnRemoveNotification(this, new StatusMessageEventArgs(this, StatusMessageEventArgs.StatusMessageAction.Remove));
        }

        public event EventHandler<StatusMessageEventArgs> OnRemoveNotification;
        #endregion Auto Remove

        public bool Equals(StatusMessage other)
        {
            return this.StatusID.Equals(other.StatusID);
        }

        public bool Equals(Guid other)
        {
            return this.StatusID.Equals(other);
        }
    }

    public class StatusMessageEventArgs : EventArgs
    {
        public StatusMessageEventArgs(StatusMessage _StatusMessage, StatusMessageAction _action = StatusMessageAction.Display)
        {           
            Message = _StatusMessage;
            Action = _action;
        }
        
        public StatusMessage Message { get; set; }

        public enum StatusMessageAction { Display, Remove };
        public StatusMessageAction Action { get; set; }
    }
}
