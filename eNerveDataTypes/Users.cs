﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Windows;
using eThele.Essentials;

namespace eNerve.DataTypes
{
    [Serializable]
    public class User : Base, IComparable<User>, IComparer<User>, IEquatable<User>
    {
        private Guid userID;
        private string userName;
        private string password;
        private string newPassword;
        private bool authenticated;
        private UserRights rights;
        
        [NonSerialized]
        private TcpClient client;

        public UserRights Rights
        {
            get { return rights; }
            set { rights = value; PropertyChangedHandler("Rights"); }
        }

        /// <summary>
        /// Do not remove - has to be present for deserialization
        /// </summary>
        public User() { }

        public User(XmlNode _userNode)
        {
            if (_userNode != null)
            {
                try
                {
                    rights = new UserRights(_userNode.Attribute("rights").Value.ToString());
                    if (_userNode.Attribute("guid") == null)
                    {
                        userID = Guid.NewGuid();
                    }
                    else
                    {
                        userID = Guid.Parse(_userNode.Attribute("guid").Value);
                    }
                    userName = _userNode.Name;
                    password = _userNode.Attribute("password").Value.ToString();
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add("User Contructor", "Error reading user from settings.xml", ex);
                }
                authenticated = false;
            }
        }

        public User(string _userName, string _password = "") : this(Guid.NewGuid(), _userName, _password)
        {
            /*userID = Guid.NewGuid();
            rights = new UserRights("512");
            userName = _userName;
            password = _password;
            authenticated = false;*/
        }

        public User(Guid _userID, string _userName, string _password = "")
        {
            //TODO get correct user object from config.
            userID = _userID;
            rights = new UserRights("512");
            userName = _userName;
            password = _password;
            authenticated = false;
        }

        private static User systemUser;
        public static User SystemUser
        {
            get
            {
                if (systemUser == null)
                    systemUser = new User("System");

                return systemUser;
            }
        }

        public Guid UserID
        {
            get { return userID; }
            set { userID = value; PropertyChangedHandler("UserID"); }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; PropertyChangedHandler("UserName"); }
        }

        public string Password
        {
            get { return password; }
            set { password = value; PropertyChangedHandler("Password"); }
        }

        public string NewPassword
        {
            get { return newPassword; }
            set { newPassword = value; PropertyChangedHandler("NewPassword"); }
        }

        /// <summary>
        /// Authenticates user and populates relevant rights to user object
        /// </summary>
        /// <param name="_user"></param>
        /// <param name="_message"></param>
        /// <returns></returns>
        public bool Verify(User _user, out string _message)
        {
            if (_user != null)
            {
                if (_user.UserName == "etheleAdmin")
                {
                    if (_user.Password == "訚靸ன쾰龍揓鵽Ζ릿뚲暟�⧀홯Ⴔ췹剷哺頀㲞")
                    {
                        _user.rights = new UserRights("64");
                        _message = "Authenticated.";
                        _user.Authenticated = true;
                        return true;
                    }
                    else
                    {
                        _message = "Invalid password.";
                        _user.Authenticated = false;
                        return false;
                    }
                }
                else if (Settings.TheeSettings["users"] != null)
                {
                    XmlNode userNode = Settings.TheeSettings["users"][_user.userName];
                    if (userNode != null)
                    {
                        _message = "Invalid password.";
                        try
                        {
                            if (userNode.Attribute("guid") == null)
                            {
                                userNode.AddAttribute("guid", _user.UserID.ToString());
                                Settings.TheeSettings.Save();
                            }
                            else
                            {
                                _user.userID = Guid.Parse(userNode.Attribute("guid").Value);
                            }
                            if (userNode.Attribute("password") != null)
                            {
                                _user.rights = new UserRights(userNode.Attribute("rights").Value);
                                if (_user.rights.Invalid)
                                {//Invalid user rights, or invalid XML file - deny login
                                    authenticated = false;
                                    _message = "Rights error.";
                                }
                                else if (_user.Rights.UserType == UserType.Deleted)
                                {
                                    authenticated = false;
                                    _message = "User deleted.";
                                }
                                else if ((_user.Password.Length > 1) && 
                                    (userNode.Attribute("password").Value == _user.Password.Substring(0, _user.Password.Length - 2)))
                                {
                                    _user.Name = userNode.Name;
                                    authenticated = true;
                                    _message = "Authenticated.";
                                    return true;
                                }
                            }
                            else
                            {
                                _message = string.Format("No password saved for {0}.", _user.UserName);
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.ExceptionsManager.Add("Users.Verify", "Error reading settings.xml", ex);
                            _message = "Server error.";
                        }
                    }
                    else
                    {
                        Exceptions.ExceptionsManager.Add("Users.Verify", string.Format("User {0}attempted to log in but was not found.", _user.UserName));
                        _message = string.Format("User {0} not found.", _user.UserName);
                    }
                    return false;
                }
                else
                {
                    Exceptions.ExceptionsManager.Add("Users.Verify", "Could not read user node from settings.xml");
                    _message = "Server error.";
                    return false;
                }
            }
            else
            {
                _message = "User is null.";
                return false;
            }
        }

        public bool Authenticated
        {
            get { return authenticated; }
            set { authenticated = value; }
        }
        
        /// <summary>
        /// Use this to keep track of what client the user logged in from
        /// </summary>
        public TcpClient Client
        {
            get { return client; }
            set { client = value; }
        }

        public override string ToString()
        {
            return UserName;
        }

        public int CompareTo(User other)
        {
            return UserID.CompareTo(other.UserID);
        }

        public int Compare(User x, User y)
        {
            return x.UserID.CompareTo(y.UserID);
        }

        public bool Equals(User other)
        {
            if ((other != null) && (other.UserID != null) && (UserID != null))
                return UserID.Equals(other.UserID);
            else
                return false;
        }

        public override bool Equals(object obj)
        {
            var userObject = obj as User;
            if ((userObject != null) && (userObject.UserID != null) && (UserID != null))
                return UserID.Equals(userObject.UserID);
            else return false;
        }

        public static bool operator ==(User user1, User user2)
        {
            if (ReferenceEquals(user1, null))
            {
                return ReferenceEquals(user2, null);
            }
            return user1.Equals(user2);
        }

        public static bool operator != (User user1, User user2)
        {
            return !(user1 == user2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public UserRights GetRights()
        {
            string message = "This will be ignored.";
            Verify(this, out message);
            return rights;
        }
    }

    [Serializable]
    public class UserRights : Base
    {
        public UserType UserType
        {
            get { return _type; }
            set { SetType(value); PropertyChangedHandler("UserType"); }
        }

        public bool CanEdit
        {
            get { return _type == UserType.Custom; }
        }

        public bool ChangePassword
        {
            get { return changePassword; }
            set { changePassword = value; PropertyChangedHandler("ChangePassword"); }
        }

        public bool ChangeRights
        {
            get { return changeRights; }
            set { changeRights = value; PropertyChangedHandler("ChangeRights"); }
        }

        public bool CreateUser
        {
            get { return createUser; }
            set { createUser = value; PropertyChangedHandler("CreateUser"); }
        }

        public bool DeleteUser
        {
            get { return deleteUser; }
            set { deleteUser = value; PropertyChangedHandler("DeleteUser"); }
        }

        public bool Invalid
        {
            get { return invalid; }
        }

        public bool Login
        {
            get { return login; }
            set { login = value; PropertyChangedHandler("Login"); }
        }

        private UserType _type;

        private bool changePassword = false;
        private bool changeRights = false;
        private bool createUser = false;
        private bool deleteUser = false;
        private bool invalid = true;
        private bool login = false;

        public UserRights() { }
        public UserRights(UserType type)
        {
            SetType(type);
        }
        public UserRights(string rightsString)
        {// Arbitrary STO system
            invalid = true;
            int rightsCode = 0;
            if (int.TryParse(rightsString, out rightsCode))
            {
                if (rightsCode == 0)
                {// Deleted user - valid, but can't log in.
                    SetType(UserType.Deleted);
                    invalid = false;
                    return;
                }
                if (rightsCode >= 1024)
                {
                    rightsCode -= 1024;
                    _type = UserType.Custom;
                    if (rightsCode >= 32)
                    {
                        rightsCode -= 32;
                        deleteUser = true;
                    }
                    if (rightsCode >= 16)
                    {
                        rightsCode -= 16;
                        changeRights = true;
                    }
                    if (rightsCode >= 8)
                    {
                        rightsCode -= 8;
                        createUser = true;
                    }
                    if (rightsCode >= 2)
                    {
                        rightsCode -= 2;
                        changePassword = true;
                    }
                    if (rightsCode == 1)
                    {
                        rightsCode = 0;
                        login = true;
                    }
                }
                else
                {
                    login = true;
                    if (rightsCode >= 512)
                    {
                        rightsCode -= 512;
                        _type = UserType.Limited;
                    }
                    else
                    {
                        changePassword = true;
                        if (rightsCode >= 256)
                        {
                            rightsCode -= 256;

                            _type = UserType.Normal;
                        }
                        else
                        {
                            createUser = true;
                            changeRights = true;
                            if (rightsCode >= 128)
                            {
                                rightsCode -= 128;
                                _type = UserType.Admin;
                            }
                            else
                            {
                                deleteUser = true;
                                if (rightsCode >= 64)
                                {
                                    rightsCode -= 64;
                                    _type = UserType.Super;
                                }
                            }
                        }
                    }
                }
                if (rightsCode == 0) invalid = false;
            }
        }
        public override string ToString()
        {
            int rightsCode = 0;
            if (_type == UserType.Deleted)
                return rightsCode.ToString();
            if (invalid) return "4";
            if (login)
            {
                _type = UserType.Limited;
                if (changePassword)
                {
                    _type = UserType.Normal;
                    if (createUser && changeRights)
                    {
                        _type = UserType.Admin;
                        if (deleteUser)
                        {
                            _type = UserType.Super;
                        }
                    }
                    else
                    {
                        if (createUser || deleteUser || changeRights)
                        {
                            _type = UserType.Custom;
                        }
                    }
                }
                else
                {
                    if (createUser || deleteUser || changeRights)
                    {
                        _type = UserType.Custom;
                    }
                }
            }
            if (_type == UserType.Custom)
            {
                rightsCode += 1024;
                if (deleteUser) rightsCode += 32;
                if (changeRights) rightsCode += 16;
                if (createUser) rightsCode += 8;
                if (changePassword) rightsCode += 2;
                if (login) rightsCode += 1;
            }
            else
                switch (_type)
                {
                    case UserType.Limited:
                        rightsCode += 512;
                        break;
                    case UserType.Normal:
                        rightsCode += 256;
                        break;
                    case UserType.Admin:
                        rightsCode += 128;
                        break;
                    case UserType.Super:
                        rightsCode += 64;
                        break;
                }
            return rightsCode.ToString();
        }

        private void SetType(UserType type)
        {
            _type = type;
            switch (type)
            {
                case UserType.Super:
                    DeleteUser = true; CreateUser = true; ChangeRights = true; ChangePassword = true; Login = true;
                    break;
                case UserType.Admin:
                    DeleteUser = false; CreateUser = true; ChangeRights = true; ChangePassword = true; Login = true;
                    break;
                case UserType.Normal:
                    DeleteUser = false; CreateUser = false; ChangeRights = false; ChangePassword = true; Login = true;
                    break;
                case UserType.Limited:
                    DeleteUser = false; CreateUser = false; ChangeRights = false; ChangePassword = false; Login = true;
                    break;
                case UserType.Custom:
                    DeleteUser = false; CreateUser = false; ChangeRights = false; ChangePassword = false; Login = false;
                    break;
                default:
                    break;
            }
            PropertyChangedHandler("CanEdit");
        }
    }

    public enum UserType
    {
        Super = 0,
        Admin = 1,
        Normal = 2,
        Limited = 3,
        Custom = 4,
        Deleted = 5
    }

    [Serializable]
    public class LoginResult
    {
        private string message;

        public LoginResult() { }

        public LoginResult(User _user, string _message = "")
        {
            User = _user;
            message = _message;
        }

        public User User
        {
            get; set;
        }

        public string Message
        {
            get { return message; }
            set { message = value; }
        }
    }

    public class Users : IDisposable
    {
        private List<User> userList;

        public Users()
        {
            userList = new List<User>();
        }

        ~Users()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (userList != null)
            {
                userList.Clear();
            }
        }

        public bool Login(User _user, out string _message)
        {
            bool result = true;
            _message = "";

            try
            {
                bool userVerified = true;
                
                userVerified = _user.Verify(_user, out _message);

                if (userVerified)
                {
                    userList.Add(_user);
                }
                else
                    result = false;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        /// <summary>
        /// Saves updated password to file, or creates a new user.
        /// </summary>
        /// <param name="_user"></param>
        /// <returns></returns>
        public bool Update(User _user)
        {
            if (Settings.TheeSettings["users"] != null)
            {
                XmlNode userNode = Settings.TheeSettings["users"][_user.UserName];
                if (userNode != null)
                {
                    if (!string.IsNullOrWhiteSpace(_user.NewPassword) && (userNode.Attribute("password") != null))
                    {
                        userNode.Attribute("password").Value = _user.NewPassword.Substring(0, _user.NewPassword.Length - 2);
                    }
                    if (_user.Rights != null)
                    {
                        userNode.Attribute("rights").Value = _user.Rights.ToString();
                    }
                    if (_user.UserID == null)
                        _user.UserID = new Guid();
                    if (userNode.Attribute("guid") == null)
                        userNode.AddAttribute("guid", _user.UserID.ToString());
                    else
                        userNode.Attribute("guid").Value = _user.UserID.ToString();
                    Settings.TheeSettings.Save();
                }
                else
                {// New user - save
                    userNode = Settings.TheeSettings["users"].Add(_user.UserName, "");
                    //userNode = Settings.TheeSettings["users"][_user.UserName];
                    if (userNode != null)
                    {// Created user entry - now populate password
                        userNode.AddAttribute("password", _user.Password.Substring(0, _user.Password.Length - 2));
                        userNode.AddAttribute("rights", _user.Rights.ToString());
                        Settings.TheeSettings.Save();
                    }
                    else
	                {// Failed to create user - panic.
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Unexpected exception: Cannot update password.");
                        return false;
                    }
                }
                return true;
            }
            else
            {// Users node broken- log 
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Unexpected exception: Cannot update password.");
                return false;
            }
        }

        /// <summary>
        /// User has logged out or lost connection.
        /// </summary>
        /// <param name="_user"></param>
        public void Logout(User _user)
        {
            if (userList != null)
            {
                _user.Authenticated = false;
                userList.Remove(_user);
            }
        }

        /// <summary>
        /// Network connection lost - remove user from list.
        /// </summary>
        /// <param name="client"></param>
        public void Disconnect(TcpClient client)
        {
            foreach (var user in userList)
            {
                if (user.Client == client)
                {
                    Logout(user);
                    return;
                }
            }
        }

        public User UserAtClient(TcpClient _client)
        {
            User result = null;

            foreach (var user in userList)
            {
                if (user.Client == _client)
                {
                    result = user;
                    break;
                }
            }

            return result;
        }

        public List<User> UserList
        {
            get { return userList; }
            set { userList = value; }
        }

        /// <summary>
        /// Removes user from settings file provided it doesn't violate a constraint
        /// </summary>
        /// <param name="deletingUser"></param>
        public string Delete(User deletingUser)
        {
            if (Settings.TheeSettings["users"] != null)
            {
                try
                {
                    XmlNode usersNode = Settings.TheeSettings["users"];
                    XmlNode userNode = usersNode[deletingUser.UserName];
                    if (userNode != null)
                    {
                        if ((userNode.Attribute("rights").Value == "64") || (userNode.Attribute("rights").Value == "128"))
                        {// If admin or super user, ensure it is not the last such user
                            int numberOfAdmins = 0;
                            foreach (XmlNode user in usersNode.Children)
                            {
                                if ((user.Attribute("rights").Value == "64") || (user.Attribute("rights").Value == "128"))
                                {
                                    numberOfAdmins++;
                                }
                            }
                            if (numberOfAdmins > 1)
                            {
                                userNode.Attribute("rights").Value = "0";
                                deletingUser.Rights = new UserRights("0");
                                //usersNode.RemoveChildNode(userNode);// Do not delete users - just deactivate them.
                                Settings.TheeSettings.Save();
                            }
                            else
                            {
                                return "Cannot delete last administrator.";
                            }
                        }
                        else
                        {
                            userNode.Attribute("rights").Value = "0";
                            deletingUser.Rights = new UserRights("0");
                            Settings.TheeSettings.Save();
                        }
                    }
                    else
                    {
                        return "User " + deletingUser.UserName + " not found.";
                    }

                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to load Settings.xml doc", ex);
                    return "Exception while attempting to delete user.";
                }
            }

            return "Successfully deleted " + deletingUser.UserName + ".";
        }
    }
}
