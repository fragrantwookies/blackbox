﻿using System;
using System.ComponentModel;

namespace eNerve.DataTypes
{
    [Serializable]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Fields should be disposed")]
    public abstract class Base : IDisposable, IExpandable, INotifyPropertyChanged, IEquatable<Base>
    {
        #region Properties
        protected string name = string.Empty;
        public string Name
        {
            get { return name; }
            set { name = value; PropertyChangedHandler("Name"); }
        }

        protected bool expand = false;
        public bool Expand
        {
            get { return expand; }
            set { expand = value; PropertyChangedHandler("Expand"); }
        }
        #endregion Properties

        #region Cleanup
        ~Base()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing) { }
        #endregion Cleanup

        #region Parent
        [NonSerialized]
        private Base parent = null;
        public Base Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        public delegate void RemoveChildDelegate(Base _child);
        public virtual void RemoveChild(Base _child) { }
        #endregion Parent

        #region PropertyChanged
        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }
        #endregion PropertyChanged
        
        public virtual void ExpandAll()
        {
            Expand = true;
        }

        public virtual void CollapseAll()
        {
            Expand = false;
        }

        public bool Equals(Base other)
        {
            if (other == null)
                return false;
            else if (Name == other.Name)
                if (Parent == other.Parent)
                    return true;
            return false;
        }
    }
}