using System;
using System.Collections.Generic;
using System.Linq;

namespace eNerve.DataTypes.Gauges
{
    [Serializable]
    public abstract class ArcScaledGauge : ScaledGauge
    {
        private double startAngle;
        public double StartAngle
        {
            get { return startAngle; }
            set { startAngle = value; PropertyChangedHandler("StartAngle"); }
        }

        private double endAngle;
        public double EndAngle
        {
            get { return endAngle; }
            set { endAngle = value; PropertyChangedHandler("EndAngle"); }
        }
    }
}
