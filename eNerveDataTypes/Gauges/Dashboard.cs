﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes.Gauges
{
    [Serializable]
    public class Dashboard : Base
    {
        [NonSerialized]
        public eThele.Essentials.XmlNode Node;
        public int Rows { get; set; }
        public int Columns { get; set; }
        public string Title { get; set; }
        public string User { get; set; }
        public List<string> EventNames { get; set; }
        public List<Gauge> Gauges { get; set; }
    }
}
