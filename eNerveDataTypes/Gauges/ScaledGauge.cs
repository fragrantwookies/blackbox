using System;
using System.Collections.Generic;
using System.Linq;

namespace eNerve.DataTypes.Gauges
{
    [Serializable]
    public abstract class ScaledGauge : Gauge
    {
        private double startValue;
        public double StartValue
        {
            get { return startValue; }
            set { startValue = value; PropertyChangedHandler("StartValue"); }
        }

        private double endValue;
        public double EndValue
        {
            get { return endValue; }
            set { endValue = value; PropertyChangedHandler("EndValue"); }
        }

        private int majorIntervalCount;
        public int MajorIntervalCount
        {
            get { return majorIntervalCount; }
            set { majorIntervalCount = value; PropertyChangedHandler("MajorIntervalCount"); }
        }

        private int minorIntervalCount;
        public int MinorIntervalCount
        {
            get { return minorIntervalCount; }
            set { minorIntervalCount = value; PropertyChangedHandler("MinorIntervalCount"); }
        }

        private double val = 0;
        public double Value
        {
            get { return val; }
            set { val = value; PropertyChangedHandler("Value"); }
        }

        public override void SetValue(object _value)
        {
            Value = Convert.ToDouble(_value);
        }
    }
}