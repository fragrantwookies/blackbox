﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes.Gauges
{
    [Serializable]
    public abstract class Gauge : INotifyPropertyChanged
    {
        public int Row { get; set; }
        public int Column { get; set; }

        private string eventName = "";
        public string EventName
        {
            get { return eventName; }
            set { eventName = value; PropertyChangedHandler("EventName"); }
        }

        public abstract void SetValue(object _value);

        #region PropertyChanged
        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }
        #endregion PropertyChanged
    }
}