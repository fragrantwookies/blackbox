using System;
using System.Collections.Generic;
using System.Linq;

namespace eNerve.DataTypes.Gauges
{
    [Serializable]
    public class StatusGauge : Gauge
    {
        private bool val = false;
        public bool Value
        {
            get { return val; }
            set { val = value; PropertyChangedHandler("Value"); }
        }

        public override void SetValue(object _value)
        {
            if(_value is bool)
                Value = Convert.ToBoolean(_value);
        }
    }
}
