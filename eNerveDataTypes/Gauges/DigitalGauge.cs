using System;
using System.Collections.Generic;
using System.Linq;

namespace eNerve.DataTypes.Gauges
{
    [Serializable]
    public class DigitalGauge : Gauge
    {
        private string val = "";
        public string Value
        {
            get { return val; }
            set { val = value; PropertyChangedHandler("Value"); }
        }

        public override void SetValue(object _value)
        {
            if(_value.GetType() == typeof(bool))
            {
                int tmpVal;
                if (!int.TryParse(val, out tmpVal))
                    tmpVal = 0;
                Value = (tmpVal + 1).ToString();
            }
            else
                Value = Convert.ToString(_value);
        }
    }
}
