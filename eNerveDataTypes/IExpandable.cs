using System;

namespace eNerve.DataTypes
{
    public interface IExpandable
    {
        bool Expand { get; set; }

        void ExpandAll();
        void CollapseAll();
    }
}
