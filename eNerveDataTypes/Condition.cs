﻿using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    public class Condition : IDisposable
    {
        /// <summary>
        /// Time frame (in seconds) in which all of the conditions must be met.
        /// </summary>
        public double TimeFrame { get; set; }
        
        /// <summary>
        /// Time to cool down (in seconds) before next event can be raised but this condition.
        /// </summary>
        public double Cooldown { get; set; }

        public string Name { get; set; }

        public bool CauseAlarm { get; set; }

        public double PercentageTriggersNeeded { get; set; }

        private DateTime lastEvent = DateTime.MinValue;

        private List<ConditionTrigger> conditionTriggers;

        public Condition()
        {
            TimeFrame = TimeSpan.FromHours(24).TotalSeconds;
            Cooldown = 0;
            PercentageTriggersNeeded = 100;

            conditionTriggers = new List<ConditionTrigger>();
        }

        #region Cleanup
        ~Condition()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            GC.SuppressFinalize(this);

            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            ClearConditionTriggers();
            ClearLimboTriggers();
        }

        public void ClearConditionTriggers()
        {
            if (conditionTriggers != null)
            {
                for(int i = conditionTriggers.Count - 1; i >= 0; i--)
                {
                    ConditionTrigger ct = conditionTriggers[i];
                    conditionTriggers.RemoveAt(i);
                    ct.Dispose();
                }
            }
        }
        #endregion

        public int AddConditionTrigger(ConditionTrigger _trigger)
        {
            int resultIndex = -1;

            try
            {
                resultIndex = conditionTriggers.BinarySearch(_trigger);
                if(resultIndex <= 0)
                {
                    resultIndex = ~resultIndex;
                    conditionTriggers.Insert(resultIndex, _trigger);
                }
                else
                    resultIndex = -1;
            }
            catch(Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return resultIndex;
        }

        public ConditionTrigger FindConditionTrigger(string _deviceName, string _detectorName)
        {
            ConditionTrigger resultTrigger = null;

            try
            {
                int sresult = conditionTriggers.BinarySearch(new ConditionTrigger() { DeviceName = _deviceName, DetectorName = _detectorName });

                if(sresult >= 0)
                    resultTrigger = conditionTriggers[sresult];
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return resultTrigger;
        }

        public void ClearLimboTriggers()
        {
            foreach (ConditionTrigger ct in conditionTriggers)
                ct.ClearLimboTriggers();
        }

        public Event ProcessTrigger(IEventTrigger _trigger)
        {
            Event resultEvent = null;

            try
            {
                ConditionTrigger conditionT = FindConditionTrigger(_trigger.DeviceName, _trigger.DetectorName);

                if (_trigger.DetectorType == DetectorType.Analog && conditionT != null && conditionT.DetectorType == DetectorType.Digital)
                {
                    _trigger.DetectorType = DetectorType.Digital;
                    _trigger.State = _trigger.EventValue == 0 ? false : true;
                }

                if (conditionT != null //No condition for this trigger, so no alarm
                    && (conditionT.DetectorType == DetectorType.Analog || _trigger.State == conditionT.TrueState || _trigger.State != conditionT.FalseState)
                    && Math.Abs((_trigger.TriggerDT - this.lastEvent).TotalSeconds) >= this.Cooldown //don't consider any triggers while in cooldown time period
                    && (conditionT.DetectorType != DetectorType.Analog || (_trigger.EventValue <= conditionT.MinThreshold || _trigger.EventValue >= conditionT.MaxThreshold))) //must be between thresholds
                {   
                    conditionT.PurgeLimboTriggers(_trigger.TriggerDT);

                    List<IEventTrigger> limboTriggers = conditionT.RetrieveLimboTriggers(TimeFrame, _trigger.TriggerDT);
                    List<IEventTrigger> otherLimboTriggers;

                    if ((_trigger.DetectorType == DetectorType.Analog
                        || limboTriggers.Count + 1 >= conditionT.TriggersToRaiseEvent) //This condition has enough triggers
                        && CheckOtherConditions(conditionT, _trigger, out otherLimboTriggers)) //the other conditions also checks out
                    {
                        resultEvent = this.CauseAlarm ? new Alarm(this.Name) : new Event(this.Name);

                        resultEvent.AlarmDT = limboTriggers.Count > 0 ? limboTriggers[limboTriggers.Count - 1].TriggerDT : _trigger.TriggerDT;

                        resultEvent.AddTrigger(_trigger);

                        conditionT.RemoveTriggers(limboTriggers);

                        foreach (ConditionTrigger ct in conditionTriggers)
                            ct.RemoveTriggers(otherLimboTriggers);

                        foreach (IEventTrigger t in limboTriggers)
                            resultEvent.AddTrigger(t);

                        foreach (IEventTrigger t in otherLimboTriggers)
                            resultEvent.AddTrigger(t);

                        this.lastEvent = resultEvent.AlarmDT;
                    }
                    else //Add trigger to list of triggers in limbo
                        conditionT.AddLimboTrigger(_trigger);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return resultEvent;
        }

        /// <summary>
        /// Check if the other parts of the condition meets the requirement to raise an event.
        /// </summary>
        /// <param name="_conditionTrigger">Exclude this part of the condition</param>
        /// <returns>True if all other requirements are met to raise an alarm</returns>
        private bool CheckOtherConditions(ConditionTrigger _conditionTrigger, IEventTrigger _trigger, out List<IEventTrigger> retrievedLimboTriggers)
        {
            bool result = true;

            int checkedConditions = 1; //first condition was already met by this stage
            bool allMandatoryConditionsMet = true;

            retrievedLimboTriggers = new List<IEventTrigger>();

            foreach(ConditionTrigger ct in conditionTriggers)
            {
                if(ct != _conditionTrigger)
                {
                    //List<IEventTrigger> tempRetrievedTriggers;
                    //result = CheckThisCondition(ct, _trigger, out tempRetrievedTriggers);

                    //if (result)
                    //    retrievedLimboTriggers.AddRange(tempRetrievedTriggers);
                    //else
                    //    break; //One of the conditions don't check out, so no alarm.

                    List<IEventTrigger> tempRetrievedTriggers;
                    bool currentResult = CheckThisCondition(ct, _trigger, out tempRetrievedTriggers);
                    if (currentResult)
                    {
                        checkedConditions++;
                        retrievedLimboTriggers.AddRange(tempRetrievedTriggers);
                    }
                    else if (this.PercentageTriggersNeeded < 100 && ct.Mandatory)
                    {
                        allMandatoryConditionsMet = false;
                        break;
                    }
                }
            }

            if (((double)checkedConditions / (double)conditionTriggers.Count * 100 >= this.PercentageTriggersNeeded) && allMandatoryConditionsMet)
                result = true;
            else
                result = false;

            return result;
        }

        private bool CheckThisCondition(ConditionTrigger _conditionTrigger, IEventTrigger _trigger, out List<IEventTrigger> retrievedLimboTriggers)
        {
            _conditionTrigger.PurgeLimboTriggers(_trigger.TriggerDT);
            retrievedLimboTriggers = _conditionTrigger.RetrieveLimboTriggers(TimeFrame, _trigger.TriggerDT);
            return _conditionTrigger.DetectorType == DetectorType.Analog || (retrievedLimboTriggers.Count >= _conditionTrigger.TriggersToRaiseEvent);
        }
    }
}
