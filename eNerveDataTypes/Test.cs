﻿using System;
using System.Collections.Generic;

namespace eNerve.DataTypes
{
    public class Test
    {
        public string Field1 { get; set; }

        public int Field2 { get; set; }

        public enum TestEnum { TesVal1, TestVal2, TestVal3, TestVal4}
        public TestEnum Field3 { get; set; }

        private List<Test2> field4;
        public List<Test2> Field4
        {
            get 
            { 
                if (field4 == null)
                    field4 = new List<Test2>();
                return field4;
            } 
        }

        private Queue<Test2> field5;
        public Queue<Test2> Field5
        {
            get
            {
                if (field5 == null)
                    field5 = new Queue<Test2>();
                return field5;
            }
        }

        public Test2 Field6 { get; set; }

        public DevicesDoc MyDevices { get; set; }
    }

    public class Test2
    {
        private Guid id;
        public Guid ID { get { return id; } set { id = value; } }

        private int field1 = -3123;

        protected byte field2 = 233;

        internal double field3 = 232.234;

        public string field4 = "sdfdsfdsf";

        public DateTime DT { get; set; }

        private byte[] bytes = new byte[] { 0, 1, 2, 4, 8, 16 };
        public byte[] Bytes { get { return bytes; } }
    }

    public class Test3 : Test2
    {
        private uint field5 = 34324;  
    }

    public class Test4 : Test3
    {
        private byte field6 = 33;

        public Test4(byte _field6)
        {
            field6 = _field6;
        }
    }
}
