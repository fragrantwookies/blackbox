﻿using eThele.Essentials;
using System;

namespace eNerve.DataTypes
{
    public class RelayNodeWrapper
    {
        private string name;
        private string description;
        private string location;
        protected string deviceMac;
        protected string deviceName;

        public MapImage Image { get; set; }

        private XmlNode relayNode;

        public RelayNodeWrapper() { }

        public RelayNodeWrapper(XmlNode _relayNode)
        {
            RelayNode = _relayNode;
        }

        public string Name
        {
            get { return name = RelayNode != null ? XmlNode.ParseString(RelayNode["Name"], "") : name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description = RelayNode != null ? XmlNode.ParseString(RelayNode["Description"], "") : description; }
            set { description = value; }
        }

        public string Location
        {
            get { return location = RelayNode != null ? XmlNode.ParseString(RelayNode["Location"], "") : location; }
            set { location = value; }
        }

        public string DeviceMac
        {
            get { return deviceMac; }
            set { deviceMac = value; }
        }

        public string DeviceName
        {
            get { return deviceName; }
            set { deviceName = value; }
        }

        public XmlNode RelayNode 
        {
            get { return relayNode; } 
            set 
            {
                relayNode = value; 
                if (value["Image"] != null) 
                    Image = new MapImage(value["Image"]);
            } 
        }
    }

    [Serializable]
    public class RelaySwitchMessage
    {
        public string DeviceMac {get; set;}
        public string DeviceName { get; set; }
        public string RelayName { get; set; }
        public bool State { get; set; }
        public int Duration { get; set; }

        public RelaySwitchMessage() { }

        public RelaySwitchMessage(string _deviceMac, string _deviceName, string _relayName, bool _state, int _duration = 0)
        {
            DeviceMac = _deviceMac;
            DeviceName = _deviceName;
            RelayName = _relayName;
            State = _state;
            Duration = _duration;
        }
    }
}
