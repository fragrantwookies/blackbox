﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    public class ScheduleDoc : XMLDoc
    {
        private static ScheduleDoc theeSchedule;

        private List<Schedule> schedule;

        public List<Schedule> Schedule
        {
            get 
            {
                if (schedule == null)
                    schedule = GetScheduleList();
                return schedule; 
            }
            set 
            {
                ClearScheduleList();
                schedule = value;
                SetScheduleList(schedule);
            }
        }

        public ScheduleDoc(string _name = "")
            : base(_name)
        {}

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            ClearScheduleList();
        }

        public static ScheduleDoc TheeSchedule
        {
            get
            {
                if (theeSchedule == null)
                {
                    theeSchedule = new ScheduleDoc("Schedule");

                    theeSchedule.Load();
                }

                return theeSchedule;
            }
        }

        public static void Selfdesctruct()
        {
            if (theeSchedule != null)
            {
                theeSchedule.Dispose();
                theeSchedule = null;
            }
        }

        private void ClearScheduleList()
        {
            if (schedule != null)
            {
                lock (schedule)
                {
                    int scheduleCount = schedule.Count;
                    for (int i = scheduleCount - 1; i >= 0; i--)
                    {
                        Schedule item = schedule[i];
                        schedule.Remove(item);
                        item.Dispose();
                    }
                }

                schedule = null; 
            }
        }

        public override bool Load(string _fileName = "")
        {
            bool result = base.Load(_fileName);

            ClearScheduleList();
                
            schedule = GetScheduleList();

            return result;
        }

        /// <summary>
        /// Test if we should raise the alarm. By default yes, unless the particular alarm is scheduled not to be raised or if all alarms are off or scheduled to not be raised.
        /// </summary>
        /// <param name="alarmName">Name of the Alarm</param>
        /// <returns>True if it should be raised, false if it shouldn't</returns>
        public bool Test(string alarmName)
        {
            bool result = true;

            try
            {
                XmlNode alarmNode = this[alarmName];

                if (alarmNode != null)
                {
                    if (alarmName != "Everything")
                    {
                        if (!Test("Everything"))
                            result = false;
                    }

                    if (XmlNode.ParseBool(alarmNode, false, "AllOff"))
                        result = false;                       

                    if (result && alarmNode != null) //if we already know the alarm should not be raised we can just exit
                    {
                        foreach (XmlNode zoneNode in alarmNode.Children)
                        {
                            string dayOfWeek = XmlNode.ParseString(zoneNode, "", "DayOfWeek");
                            TimeSpan offTime = XmlNode.ParseTimeSpan(zoneNode, TimeSpan.Zero, "OffTime");
                            TimeSpan onTime = XmlNode.ParseTimeSpan(zoneNode, TimeSpan.Zero, "OnTime");
                            DateTime now = DateTime.Now;

                            if ((dayOfWeek == "" || now.DayOfWeek.ToString().ToLower().StartsWith(dayOfWeek.ToLower())) && (now.TimeOfDay > offTime && now.TimeOfDay < onTime))
                                result = false;
                        }
                    }
                }
                else
                {
                    if (alarmName != "Everything" && !Test("Everything"))
                        result = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private List<Schedule> GetScheduleList()
        {
            List<Schedule> result = null;
            try
            {
                result = new List<Schedule>();

                foreach (XmlNode scheduleNode in this.Children)
                {
                    Schedule scheduleItem = new Schedule();
                    scheduleItem.Name = scheduleNode.Name;
                    scheduleItem.AllOff = XmlNode.ParseBool(scheduleNode, false, "AllOff");

                    foreach (XmlNode zoneNode in scheduleNode.Children)
                    {
                        ScheduleZone zone = new ScheduleZone();
                        zone.Name = zoneNode.Name;
                        zone.DayOfTheWeek = ParseDayOfTheWeek(XmlNode.ParseString(zoneNode, "", "DayOfWeek"));
                        zone.OffTime = XmlNode.ParseTimeSpan(zoneNode, TimeSpan.Zero, "OffTime");
                        zone.OnTime = XmlNode.ParseTimeSpan(zoneNode, TimeSpan.Zero, "OnTime");

                        scheduleItem.Zones.Add(zone);
                        //zone.Parent = scheduleItem;
                    }

                    if(!string.IsNullOrWhiteSpace(scheduleItem.Name))
                        result.Add(scheduleItem);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private void SetScheduleList(List<Schedule> _schedule)
        {
            try
            {
                this.ClearNodes();

                foreach (Schedule scheduleItem in _schedule)
                {
                    XmlNode scheduleNode = this.Add(scheduleItem.Name, "");

                    scheduleNode.AddAttribute("AllOff", scheduleItem.AllOff.ToString());

                    //foreach (ScheduleZone zone in scheduleItem.Zones)
                    for (int i = 0; i < scheduleItem.Zones.Count; i++ )
                    {
                        ScheduleZone zone = scheduleItem.Zones[i];
                        XmlNode zoneNode = scheduleNode.Add(string.Format("Zone{0}", i.ToString()), "");
                        zoneNode.AddAttribute("DayOfWeek", zone.DayOfTheWeek.ToString());
                        zoneNode.AddAttribute("OffTime", zone.OffTime.ToString());
                        zoneNode.AddAttribute("OnTime", zone.OnTime.ToString());
                    }
                }

                Save();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private DayOfWeek ParseDayOfTheWeek(string _val)
        {
            DayOfWeek result;

            switch (_val.Substring(0,2).ToLower())
            {
                case "mo":
                    result = DayOfWeek.Monday;
                    break;
                case "tu":
                    result = DayOfWeek.Tuesday;
                    break;
                case "we":
                    result = DayOfWeek.Wednesday;
                    break;
                case "th":
                    result = DayOfWeek.Thursday;
                    break;
                case "fr":
                    result = DayOfWeek.Friday;
                    break;
                case "sa":
                    result = DayOfWeek.Saturday;
                    break;
                case "su":
                    result = DayOfWeek.Sunday;
                    break;
                default:
                    result = DayOfWeek.Monday;
                    break;
            }

            return result;
        }
    }

    [Serializable]
    public class Schedule : System.ComponentModel.INotifyPropertyChanged, IEquatable<Schedule>, IComparable<Schedule>, IComparer<Schedule>, IDisposable
    {
        private string name;
        private bool allOff;
        private ObservableCollection<ScheduleZone> zones;

        private DayOfWeek newZoneDayOfTheWeek;
        private TimeSpan newZoneOffTime;
        private TimeSpan newZoneOnTime;

        [NonSerialized]
        private bool expand = false;

        public void Dispose()
        {
            if (zones != null)
                zones.Clear();
        }

        public string Name
        {
            get { return name; }
            set { name = value; PropertyChangedHandler("Name"); }
        }

        public bool AllOff
        {
            get { return allOff; }
            set 
            { 
                allOff = value; 
                PropertyChangedHandler("AllOff"); 
            }
        }

        public bool Expand
        {
            get { return expand; }
            set { expand = value; PropertyChangedHandler("Expand"); }
        }

        public ObservableCollection<ScheduleZone> Zones
        {
            get 
            {
                if (zones == null)
                    zones = new ObservableCollection<ScheduleZone>();

                return zones; 
            }
            set { zones = value; PropertyChangedHandler("Zones"); }
        }

        public DayOfWeek NewZoneDayOfTheWeek
        {
            get { return newZoneDayOfTheWeek; }
            set { newZoneDayOfTheWeek = value; PropertyChangedHandler("NewZoneDayOfTheWeek"); }
        }

        public TimeSpan NewZoneOffTime
        {
            get { return newZoneOffTime; }
            set { newZoneOffTime = value; PropertyChangedHandler("NewZoneOffTime"); }
        }

        public TimeSpan NewZoneOnTime
        {
            get { return newZoneOnTime; }
            set { newZoneOnTime = value; PropertyChangedHandler("NewZoneOnTime"); }
        }

        public bool Equals(Schedule other)
        {
            return this.Name.Equals(other.Name);
        }

        public int CompareTo(Schedule other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public int Compare(Schedule x, Schedule y)
        {
            return x.Name.CompareTo(y.Name);
        }

        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }
    }

    [Serializable]
    public class ScheduleZone : System.ComponentModel.INotifyPropertyChanged
    {
        private string name;
        private DayOfWeek dayOfTheWeek;
        private TimeSpan offTime;
        private TimeSpan onTime;

        //[NonSerialized]
        //private Schedule parent;

        //public Schedule Parent
        //{
        //    get { return parent; }
        //    set { parent = value; }
        //}

        public string Name
        {
            get { return name; }
            set { name = value; PropertyChangedHandler("Name"); }
        }

        public DayOfWeek DayOfTheWeek
        {
            get { return dayOfTheWeek; }
            set { dayOfTheWeek = value; PropertyChangedHandler("DayOfTheWeek"); }
        }

        public TimeSpan OffTime
        {
            get { return offTime; }
            set
            {
                offTime = value; 
                PropertyChangedHandler("OffTime"); 
                PropertyChangedHandler("OffTimeHours");
                PropertyChangedHandler("OffTimeMinutes");
            }
        }

        public int OffTimeHours
        {
            get
            {
                return OffTime.Hours;
            }
            set
            {
                int minutes = OffTime.Minutes;
                OffTime = TimeSpan.FromHours(value) + TimeSpan.FromMinutes(minutes);
                PropertyChangedHandler("OffTime");
                PropertyChangedHandler("OffTimeHours");
            }
        }

        public int OffTimeMinutes
        {
            get
            {
                return OffTime.Minutes;
            }
            set
            {
                int hours = OffTime.Hours;
                OffTime = TimeSpan.FromHours(hours) + TimeSpan.FromMinutes(value);
                PropertyChangedHandler("OffTime");
                PropertyChangedHandler("OffTimeMinutes");
            }
        }

        public TimeSpan OnTime
        {
            get { return onTime; }
            set
            {
                onTime = value; 
                PropertyChangedHandler("OnTime");
                PropertyChangedHandler("OnTimeHours");
                PropertyChangedHandler("OnTimeMinutes");
            }
        }

        public int OnTimeHours
        {
            get
            {
                return OnTime.Hours;
            }
            set
            {
                int minutes = OnTime.Minutes;
                OnTime = TimeSpan.FromHours(value) + TimeSpan.FromMinutes(minutes);
                PropertyChangedHandler("OnTime");
                PropertyChangedHandler("OnTimeHours");
            }
        }

        public int OnTimeMinutes
        {
            get
            {
                return OnTime.Minutes;
            }
            set
            {
                int hours = OnTime.Hours;
                OnTime = TimeSpan.FromHours(hours) + TimeSpan.FromMinutes(value);
                PropertyChangedHandler("OnTime");
                PropertyChangedHandler("OnTimeMinutes");
            }
        }

        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }
    }
}
