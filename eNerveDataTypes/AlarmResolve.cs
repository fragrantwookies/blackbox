﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    [Serializable]
    public class AlarmResolve : Base, IEquatable<AlarmResolve>, IEquatable<Guid>
    {
        #region Properties
        private Guid resolveID;
        public virtual Guid ResolveID
        {
            get { return resolveID; }
            set { resolveID = value; PropertyChangedHandler("ResolveID"); }
        }

        private Guid alarmID;

        public Guid AlarmID
        {
            get { return alarmID; }
            set { alarmID = value; PropertyChangedHandler("AlarmID"); }
        }

        private bool groupResolve = true;

        public bool GroupResolve
        {
            get { return groupResolve; }
            set { groupResolve = value; PropertyChangedHandler("GroupResolve"); }
        }

        private DateTime resolveDT;
        public virtual DateTime ResolveDT
        {
            get { return resolveDT; }
            set { resolveDT = value; PropertyChangedHandler("ResolveDT"); }
        }

        private AlarmType alarm_Type;
        public AlarmType Alarm_Type
        {
            get { return alarm_Type; }
            set { alarm_Type = value; PropertyChangedHandler("Alarm_Type"); }
        }

        private string description;
        public virtual string Description
        {
            get { return description; }
            set { description = value; PropertyChangedHandler("Description"); }
        }

        private User user;
        public virtual User User
        {
            get { return user; }
            set { user = value; PropertyChangedHandler("User"); }
        }
        public string UserName
        {
            get
            {
                return user != null ? user.UserName : "";
            }
        }
        #endregion Properties

        #region Equatable implementations
        public bool Equals(AlarmResolve other)
        {
            return this.ResolveID.Equals(other.ResolveID);
        }

        public bool Equals(Guid other)
        {
            return this.ResolveID.Equals(other);
        }
        #endregion Equatable implementations
    }

    public class AlarmResolvedEventArgs : EventArgs
    {
        public AlarmResolvedEventArgs(AlarmResolve _alarmResolveItem)
            : base()
        {
            AlarmResolveItem = _alarmResolveItem;
        }
        public AlarmResolve AlarmResolveItem { get; set; }
    }

    [Serializable]
    public class ResolveDetailItem : IEquatable<ResolveDetailItem>, System.ComponentModel.INotifyPropertyChanged
    {
        private Guid alarmID;
        private Guid resolveID;
        private DateTime resolveDT;
        private AlarmType alarm_Type;
        private string description;
        private User user;

        public ResolveDetailItem() { }

        public ResolveDetailItem(Guid _alarmID, Guid _resolveID)
        {
            alarmID = _alarmID;
            resolveID = _resolveID;
        }

        public ResolveDetailItem(Guid _alarmID, Guid _resolveID, DateTime _resolveDT, AlarmType _alarm_Type, string _description, User _user)
        {
            alarmID = _alarmID;
            resolveID = _resolveID;
            resolveDT = _resolveDT;
            alarm_Type = _alarm_Type;
            description = _description;
            user = _user;
        }

        public Guid AlarmID
        {
            get { return alarmID; }
            set { alarmID = value; PropertyChangedHandler("AlarmID"); }
        }

        public Guid ResolveID
        {
            get { return resolveID; }
            set { resolveID = value; PropertyChangedHandler("ResolveID"); }
        }

        public DateTime ResolveDT
        {
            get { return resolveDT; }
            set { resolveDT = value; PropertyChangedHandler("ResolveDT"); }
        }

        public AlarmType Alarm_Type
        {
            get { return alarm_Type; }
            set { alarm_Type = value; PropertyChangedHandler("Alarm_Type"); }
        }

        public string Description
        {
            get { return description; }
            set { description = value; PropertyChangedHandler("Description"); }
        }

        public User User
        {
            get { return user; }
            set { user = value; PropertyChangedHandler("User"); }
        }

        public string UserName
        {
            get
            {
                return user != null ? user.UserName : "";
            }
        }

        public bool Equals(ResolveDetailItem other)
        {
            return this.ResolveID.Equals(other.ResolveID);
        }

        private void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    }    
}
