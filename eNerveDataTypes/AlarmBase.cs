using System;
using System.Collections.Generic;
using System.Threading;
using eThele.Essentials;
using System.Collections.ObjectModel;

namespace eNerve.DataTypes
{
    [Serializable]
    public abstract class AlarmBase : Event, IComparable<AlarmBase>, IComparer<AlarmBase>
    {
        #region Constructors
        public AlarmBase() : base() { }

        /// <summary>
        /// Use this constructor to create search items only.
        /// </summary>
        public AlarmBase(Guid _id) : base(_id) { }

        public AlarmBase(string _name) : base(_name) { }
        #endregion Constructors

        #region Cleanup
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            try
            {
                if (timerEscalation != null)
                {
                    timerEscalation.Stop();
                    timerEscalation.Elapsed -= TimerEscalation_Elapsed;
                    timerEscalation.Dispose();
                    timerEscalation = null;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error disposing escalation timer", ex);
            }
        }
        #endregion Cleanup

        #region Properties
        protected AlarmType alarm_Type = AlarmType.Unresolved;
        public virtual AlarmType Alarm_Type
        {
            get { return alarm_Type; }
            set { alarm_Type = value; PropertyChangedHandler("Alarm_Type"); }
        }

        private int alarmNumber;
        public int AlarmNumber
        {
            get { return alarmNumber; }
            set { alarmNumber = value; PropertyChangedHandler("AlarmNumber"); }
        }

        public override Actions Actions
        {
            get
            {
                return base.Actions;
            }
            set
            {
                base.Actions = value;

                SetSiteInstructionsAlarmID();
            }
        }
        #endregion Properties

        #region Escalation
        private EscalationLevel escalation = EscalationLevel.None;
        public virtual EscalationLevel EscalationLevel
        {
            get { return escalation; }
            set { escalation = value; PropertyChangedHandler("EscalationLevel"); }
        }

        public bool CanEscalate
        {
            get
            {
                return actions != null ? actions["Escalations"] != null : false;
            }
        }

        public int GreenDelay
        {
            get
            {
                int result = 60;
                try
                {
                    EscalationsAction escalationsAction = actions["Escalations"] as EscalationsAction;
                    if (escalationsAction != null)
                        result = escalationsAction.GreenDelay;
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                }
                return result <= 0 ? 60 : result;
            }
        }
        public int YellowDelay
        {
            get
            {
                int result = 60;
                try
                {
                    EscalationsAction escalationsAction = actions["Escalations"] as EscalationsAction;
                    if (escalationsAction != null)
                        result = escalationsAction.YellowDelay;
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                }
                return result <= 0 ? 60 : result;
            }
        }
        public int RedDelay
        {
            get
            {
                int result = 60;
                try
                {
                    EscalationsAction escalationsAction = actions["Escalations"] as EscalationsAction;
                    if (escalationsAction != null)
                        result = escalationsAction.RedDelay;
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                }
                return result <= 0 ? 60 : result;
            }
        }

        [NonSerialized]
        private System.Timers.Timer timerEscalation;

        public class EscalateEventArgs : EventArgs
        {
            public EscalateEventArgs(EscalationLevel _escalationLevel)
                : base()
            {
                EscalationLevel = _escalationLevel;
            }

            public EscalationLevel EscalationLevel { get; private set; }
        }
        [field: NonSerialized]
        public event EventHandler<EscalateEventArgs> Escalated;

        /// <summary>
        /// Escalate the alarm to the next level
        /// </summary>
        /// <returns>True, unless the alarm was already on Red, then it will stay Red and the function will return false.</returns>
        public bool Escalate()
        {
            bool result = false;

            try
            {
                if (escalation != EscalationLevel.Red)
                {
                    switch (escalation)
                    {
                        case EscalationLevel.None:
                            escalation = EscalationLevel.Green;
                            break;
                        case EscalationLevel.Green:
                            escalation = EscalationLevel.Yellow;
                            break;
                        case EscalationLevel.Yellow:
                            escalation = EscalationLevel.Red;
                            break;
                        default:
                            escalation = EscalationLevel.Red;
                            break;
                    }

                    try
                    {
                        Thread thread = new Thread(delegate() { Escalate(escalation); });
                        thread.Start();
                    }
                    catch (Exception ex)
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to start thread", ex);
                    }

                    result = true;
                }
                else
                {
                    escalation = EscalationLevel.Red;
                    result = false;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public void Escalate(EscalationLevel _escalation)
        {
            try
            {
                if (Escalated != null)
                    Escalated(this, new EscalateEventArgs(_escalation));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void StartEscalation()
        {
            try
            {
                if (!IsResolved && CanEscalate)
                {
                    int interval = CalcEscalationInterval();
                    if (interval > 0)
                    {
                        if (timerEscalation == null)
                        {
                            timerEscalation = new System.Timers.Timer();

                            timerEscalation.Elapsed += TimerEscalation_Elapsed;
                        }

                        timerEscalation.Interval = interval;
                        timerEscalation.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        /// <summary>
        /// Calculates the interval until the next escalation. Returns 0 to indicate it should not be escalated any further. 
        /// </summary>
        private int CalcEscalationInterval()
        {
            int result = 0;
            int timeSinceAlarm = (int)(DateTime.Now - AlarmDT).TotalMilliseconds;

            switch (EscalationLevel)
            {
                case DataTypes.EscalationLevel.None:
                    {
                        result = GreenDelay * 1000 - timeSinceAlarm;
                        if (result <= 100)
                            result = 100;
                        break;
                    }
                case DataTypes.EscalationLevel.Green:
                    {
                        result = YellowDelay * 1000 - (timeSinceAlarm - GreenDelay * 1000);
                        if (result <= 100)
                            result = 100;
                        break;
                    }
                case DataTypes.EscalationLevel.Yellow:
                    {
                        result = RedDelay * 1000 - (timeSinceAlarm - (GreenDelay + YellowDelay) * 1000);
                        if (result <= 100)
                            result = 100;
                        break;
                    }
                default:
                    result = 0;
                    break; //Don't start the timer, it's already red.
            }

            return result;
        }

        public void StopEscalation()
        {
            if (timerEscalation != null)
            {
                timerEscalation.Stop();
            }
        }

        void TimerEscalation_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timerEscalation.Stop();

            if (Escalate())
            {
                int interval = CalcEscalationInterval();
                if (interval > 0 && !IsResolved && EscalationLevel != EscalationLevel.Red)
                {
                    timerEscalation.Interval = interval;
                    timerEscalation.Start();
                }
            }

            if (IsResolved || escalation == EscalationLevel.Red)
            {
                if (timerEscalation != null)
                {
                    timerEscalation.Stop();
                    timerEscalation.Elapsed -= TimerEscalation_Elapsed;
                    timerEscalation.Dispose();
                    timerEscalation = null;
                }
            }
        }
        #endregion Escalation

        #region Resolve
        [field: NonSerialized]
        public event EventHandler<AlarmResolvedEventArgs> Resolved;

        private ObservableCollection<AlarmResolve> resolveItems;
        public ObservableCollection<AlarmResolve> ResolveItems
        {
            get
            {
                if (resolveItems == null)
                    resolveItems = new ObservableCollection<AlarmResolve>();

                return resolveItems;
            }
        }
        public bool IsResolved
        {
            get
            {
                return ResolveItems != null ? (ResolveItems.Count > 0) : false;
            }
        }

        public void Resolve(AlarmResolve _resolveItem)
        {
            if (!ResolveItems.Contains(_resolveItem))
                ResolveItems.Add(_resolveItem);

            Alarm_Type = _resolveItem.Alarm_Type;

            StopEscalation();

            if (Resolved != null)
                Resolved(this, new AlarmResolvedEventArgs(_resolveItem));

            PropertyChangedHandler("IsResolved");
        }

        public void Resolve(IEnumerable<AlarmResolve> _resolveItems)
        {
            foreach (AlarmResolve resolveItem in _resolveItems)
                Resolve(resolveItem);
        }

        public AlarmResolve LastResolveItem
        {
            get
            {
                AlarmResolve result = null;

                if (ResolveItems != null)
                {
                    if (ResolveItems.Count > 0)
                        result = ResolveItems[ResolveItems.Count - 1];
                }

                return result;
            }
        }
        #endregion Resolve

        #region Comments
        private ObservableCollection<AlarmComment> comments;
        public ObservableCollection<AlarmComment> Comments
        {
            get
            {
                if (comments == null)
                    comments = new ObservableCollection<AlarmComment>();

                return comments;
            }
        }

        public void AddComment(AlarmComment _comment)
        {
            if (!Comments.Contains(_comment))
                Comments.Add(_comment);
        }

        public void AddComments(IEnumerable<AlarmComment> _comments)
        {
            if (_comments != null)
                foreach (AlarmComment comment in _comments)
                    AddComment(comment);
        }
        #endregion

        #region SiteInstruction shortcuts
        public void SetSiteInstructionsAlarmID()
        {
            if (actions != null)
            {
                for (int i = 0; i < actions.Count; i++)
                {
                    if (actions[i] is SiteInstructionAction)
                    {
                        ObservableCollection<SiteInstructionStep> steps = ((SiteInstructionAction)actions[i]).SiteInstructionSteps;
                        if (steps != null)
                        {
                            foreach (SiteInstructionStep step in steps)
                                step.AlarmID = ID;
                        }
                    }
                }
            }
        }

        public int SiteInstructionCount
        {
            get
            {
                int result = 0;

                if (actions != null)
                {
                    for (int i = 0; i < actions.Count; i++)
                    {
                        if (actions[i] is SiteInstructionAction)
                        {
                            result = ((SiteInstructionAction)actions[i]).SiteInstructionSteps.Count;
                            break;
                        }
                    }
                }

                return result;
            }
        }

        public SiteInstructionStep GetSiteInstructionStep(int index)
        {
            SiteInstructionStep result = null;

            if (actions != null)
            {
                for (int i = 0; i < actions.Count; i++)
                {
                    if (actions[i] is SiteInstructionAction)
                    {
                        result = ((SiteInstructionAction)actions[i]).SiteInstructionSteps[index];
                        break;
                    }
                }
            }

            return result;
        }

        public SiteInstructionStep GetSiteInstructionStep(Guid stepID)
        {
            SiteInstructionStep result = null;

            if (actions != null)
            {
                for (int i = 0; i < actions.Count; i++)
                {
                    if (actions[i] is SiteInstructionAction)
                    {
                        ObservableCollection<SiteInstructionStep> steps = ((SiteInstructionAction)actions[i]).SiteInstructionSteps;

                        if (steps != null)
                            foreach (SiteInstructionStep step in steps)
                                if (step.SiteInstructionStepID == stepID)
                                {
                                    result = step;
                                    break;
                                }
                    }
                }
            }

            return result;
        }

        public ObservableCollection<SiteInstructionStep> GetSiteInstructionItems
        {
            get
            {
                ObservableCollection<SiteInstructionStep> result = null;

                if (actions != null)
                {
                    for (int i = 0; i < actions.Count; i++)
                    {
                        if (actions[i] is SiteInstructionAction)
                            result = ((SiteInstructionAction)actions[i]).SiteInstructionSteps;
                    }
                }

                return result;
            }
        }

        public void ClearSiteInstructionSteps()
        {
            if (actions != null)
            {
                for (int i = 0; i < actions.Count; i++)
                {
                    if (actions[i] is SiteInstructionAction)
                    {
                        ((SiteInstructionAction)actions[i]).SiteInstructionSteps.Clear();
                        break;
                    }
                }
            }
        }

        public void AddSiteInstructionStep(IEnumerable<SiteInstructionStep> steps)
        {
            if (steps != null)
            {
                foreach (SiteInstructionStep step in steps)
                {
                    AddSiteInstructionStep(step);
                }
            }
        }

        public void AddSiteInstructionStep(SiteInstructionStep newStep)
        {
            if (actions == null)
            {
                actions = new Actions(this.Name);
            }

            bool found = false;
            for (int i = 0; i < actions.Count; i++)
            {
                if (actions[i] is SiteInstructionAction)
                {
                    SiteInstructionAction action = ((SiteInstructionAction)actions[i]);
                    if (action.SiteInstructionSteps.Contains(newStep))
                    {
                        SiteInstructionStep existingStep = action.SiteInstructionSteps[action.SiteInstructionSteps.IndexOf(newStep)];
                        existingStep.SiteInstructionStepID = newStep.SiteInstructionStepID;
                        existingStep.Value = newStep.Value;
                        existingStep.Completed = newStep.Completed;
                        existingStep.CompleteToResolve = newStep.CompleteToResolve;
                        existingStep.User = newStep.User;
                        existingStep.Order = newStep.Order;
                    }
                    else
                    {
                        action.SiteInstructionSteps.Add(newStep);
                    }
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                SiteInstructionAction wfAction = new SiteInstructionAction("SiteInstruction", true);
                wfAction.SiteInstructionSteps.Add(newStep);
                actions.Add(wfAction);
            }
        }

        /// <summary>
        /// You can't resolve an alarm if all SiteInstruction steps marked for CompleteToResolve is not completed
        /// </summary>
        public bool CanResolve
        {
            get
            {
                bool result = true;

                ObservableCollection<SiteInstructionStep> siteInstructionItems = GetSiteInstructionItems;

                if (siteInstructionItems != null)
                {
                    foreach (SiteInstructionStep step in siteInstructionItems)
                    {
                        if (step.CompleteToResolve && !step.Completed)
                        {
                            result = false;
                            break;
                        }
                    }
                }

                return result;
            }
        }
        #endregion SiteInstruction shortcuts

        #region Comparers
        public int CompareTo(AlarmBase other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public int Compare(AlarmBase x, AlarmBase y)
        {
            return x.Name.CompareTo(y.Name);
        }
        #endregion Comparers
    }
}