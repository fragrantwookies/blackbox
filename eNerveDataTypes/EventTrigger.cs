﻿using eNervePluginInterface;
using System;

namespace eNerve.DataTypes
{
    [Serializable]
    public class EventTrigger : Base, IEventTrigger
    {
        protected Guid eventID;
        protected Guid triggerID;
        protected int detectorGroupIndex = -1;
        protected int detectorIndex = -1;
        protected string detectorName;
        protected string deviceName;
        protected DetectorType detectorType = DetectorType.Unknown;
        protected string location = "";
        protected string mapName = "";
        protected bool state = true;
        protected double eventValue = 0;
        protected int eventCount;
        //protected double[] eventValues;
        protected DateTime firstTriggerDT;
        protected DateTime triggerDT;
        protected string data = "";

        #region Constructors
        public EventTrigger() { }

        /// <summary>
        /// This constructor will not create a new guid for TriggerID. Use it for binary searches only.
        /// </summary>
        public EventTrigger(string _detectorName, string _deviceName)
        {
            detectorName = _detectorName;
            deviceName = _deviceName;
        }

        public EventTrigger(Guid _triggerID, string _detectorName, string _deviceName)
        {
            triggerID = _triggerID;
            detectorName = _detectorName;
            deviceName = _deviceName;
        }

        public EventTrigger(string _detectorName, string _deviceName, DetectorType _detectorType, int _eventCount, DateTime _firstTriggerDT, double _eventValue = 0)
        {
            triggerID = Guid.NewGuid();
            detectorName = _detectorName;
            deviceName = _deviceName;
            detectorType = _detectorType;
            eventCount = _eventCount;
            eventValue = _eventValue;
            firstTriggerDT = _firstTriggerDT;
            triggerDT = DateTime.Now;
        }
        #endregion Constructors

        #region Properties
        public Guid EventID
        {
            get { return eventID; }
            set { eventID = value; PropertyChangedHandler("EventID"); }
        }

        public Guid TriggerID
        {
            get { return triggerID; }
            set { triggerID = value; }
        }

        public int DetectorGroupIndex
        {
            get { return detectorGroupIndex; }
            set { detectorGroupIndex = value; PropertyChangedHandler("DetectorGroupIndex"); }
        }

        public int DetectorIndex
        {
            get { return detectorIndex; }
            set { detectorIndex = value; PropertyChangedHandler("DetectorIndex"); }
        }

        public string DetectorName
        {
            get { return detectorName; }
            set { detectorName = value; PropertyChangedHandler("DetectorName"); }
        }

        public string DeviceName
        {
            get { return deviceName; }
            set { deviceName = value; PropertyChangedHandler("DeviceName"); }
        }

        public DetectorType DetectorType
        {
            get { return detectorType; }
            set { detectorType = value; PropertyChangedHandler("DetectorType"); }
        }

        public string EventName
        {
            get { return Name; }
            set { Name = value; PropertyChangedHandler("EventName"); }
        }

        public string Location
        {
            get { return location; }
            set { location = value; PropertyChangedHandler("Location"); }
        }

        public string MapName
        {
            get { return mapName; }
            set { mapName = value; PropertyChangedHandler("MapName"); }
        }

        public bool State
        {
            get { return state; }
            set { state = value; PropertyChangedHandler("State"); }
        }

        public double EventValue
        {
            get { return eventValue; }
            set
            {
                eventValue = value;
                //EventValues = new double[] { value };
                PropertyChangedHandler("EventValue");
            }
        }

        //public int EventCount
        //{
        //    get { return eventValues != null ? eventValues.Length : 0; }
        //}

        //public double[] EventValues
        //{
        //    get { return eventValues; }
        //    set { eventValues = value; PropertyChangedHandler("EventValues"); PropertyChangedHandler("EventCount"); PropertyChangedHandler("EventValueCSV"); }
        //}

        //public string EventValueCSV
        //{
        //    get
        //    {
        //        string result = "";

        //        if (eventValues != null)
        //            foreach (float val in eventValues)
        //            {
        //                result += (result != "" ? ", " : "") + val.ToString("0.000");
        //            }

        //        return result;
        //    }
        //}

        public DateTime TriggerDT
        {
            get { return triggerDT; }
            set { triggerDT = value; PropertyChangedHandler("TriggerDT"); }
        }

        public string Data
        {
            get { return data; }
            set { data = value; ; PropertyChangedHandler("Data"); }
        }
        #endregion Properties

        #region Compare
        public int CompareTo(IEventTrigger other)
        {
            if (this.DeviceName == other.DeviceName)
                return this.DetectorName.CompareTo(other.DetectorName);
            else
                return this.DeviceName.CompareTo(other.DeviceName);
        }

        public int Compare(IEventTrigger x, IEventTrigger y)
        {
            if (x.DeviceName == y.DeviceName)
                return x.DetectorName.CompareTo(y.DetectorName);
            else
                return x.DeviceName.CompareTo(y.DeviceName);
        }

        public bool Equals(IEventTrigger other)
        {
            return this.TriggerID.Equals(other.TriggerID);
        }

        public bool Equals(Guid other)
        {
            return this.TriggerID.Equals(other);
        }
        #endregion Compare
    }   
}
