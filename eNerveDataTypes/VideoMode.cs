﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    public enum VideoMode
    {
        None,
        Live,
        Playback
    }
}
