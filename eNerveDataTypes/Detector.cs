﻿using System;
using System.Collections.Generic;
using eNervePluginInterface;
using eThele.Essentials;
using eNerve.DataTypes;

namespace eNerve.DataTypes
{

    public class Detector : IDetector, IDisposable, IComparable<Detector>, IComparer<Detector>
    {
        //the idea is to give this class 2 modes, if you use the xmlNode, this becomes just a wrapper for it.
        private XmlNode detectorNode = null;
        public XmlNode DetectorNode { get { return detectorNode; } }

        protected string name;
        protected string eventName;
        protected string location;
        protected string deviceAddress;
        protected string deviceIP;
        protected string deviceName;
        protected int majorIndex;
        protected int minorIndex;

        protected DetectorType detectorType = DetectorType.Unknown;

        //protected double eventValueMinThreshold = 0;
        //protected double eventValueMaxThreshold = 0;
        //protected int eventsToRaiseAlarm = 1;
        //protected int eventsToRaiseAlarmInterval = 0;
        //protected int coolDown = 0;

        private string imagePath;
        private string imageMap;
        private string imageX;
        private string imageY;
        private string imageWidth;
        private string imageHeight;
        private string imageRotation;

        public Detector() { }

        public Detector(string _name, string _deviceAddress, DetectorType _detectorType = DetectorType.Unknown)
        {
            name = _name;
            deviceAddress = _deviceAddress;
            detectorType = _detectorType;
        }

        public Detector(XmlNode _detectorNode)
        {
            detectorNode = _detectorNode;
        }

        public Detector(string _deviceName, string _deviceAddress, XmlNode _detectorNode)
        {
            deviceName = _deviceName;
            deviceAddress = _deviceAddress;
            detectorNode = _detectorNode;
        }

        ~Detector()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);
        }

        public string Name
        {
            get 
            { 
                return detectorNode != null ? (detectorNode["Name"] != null ? detectorNode["Name"].Value : name) : name; 
            }
            set 
            { 
                name = value;

                if (detectorNode != null)
                    if (detectorNode["Name"] != null)
                        detectorNode["Name"].Value = value;
                    else
                        detectorNode.Add("Name", value);
            }
        }

        public string EventName
        {
            get
            {
                return detectorNode != null ? (detectorNode["EventName"] != null ? detectorNode["EventName"].Value : eventName) : eventName;
            }
            set
            {
                eventName = value;

                if (detectorNode != null)
                    if (detectorNode["EventName"] != null)
                        detectorNode["EventName"].Value = value;
                    else
                        detectorNode.Add("EventName", value);
            }
        }

        public string Location
        {
            get
            {
                return detectorNode != null ? (detectorNode["Location"] != null ? detectorNode["Location"].Value : location) : location;
            }
            set
            {
                location = value;

                if (detectorNode != null)
                    if (detectorNode["Location"] != null)
                        detectorNode["Location"].Value = value;
                    else
                        detectorNode.Add("Location", value);
            }
        }

        public string DeviceAddress
        {
            get { return deviceAddress; }
            set { deviceAddress = value; }
        }
        public string DeviceIP
        {
            get { return deviceIP; }
            set { deviceIP = value; }
        }

        public string DeviceName
        {
            get { return deviceName; }
            set { deviceName = value; }
        }

        //public int CardIndex
        //{
        //    get 
        //    {
        //        if (this.detectorNode != null && this.detectorNode.Parent != null)
        //            cardIndex = XmlNode.ParseInteger(this.detectorNode.Parent.Parent, 0, "CardIndex");
        //        return cardIndex; 
        //    }
        //    set { cardIndex = value; }
        //}

        //public int PinIndex
        //{
        //    get 
        //    { 
        //        if(detectorNode != null)
        //        {
        //            try
        //            {
        //                pinIndex = int.Parse(detectorNode.Name.Remove(0, 3));
        //            }
        //            catch (Exception ex)
        //            {
        //                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error Parsing PinIndex from Detector Node", ex);
        //            }
        //        }
        //        return pinIndex; 
        //    }
        //    set { pinIndex = value; }
        //}

        public int MajorIndex
        {
            get
            {
                if (this.detectorNode != null && this.detectorNode.Parent != null)
                    majorIndex = XmlNode.ParseInteger(this.detectorNode.Parent, 0, "Index");
                return majorIndex;
            }
            set { majorIndex = value; }
        }

        public int MinorIndex
        {
            get
            {
                if (this.detectorNode != null && this.detectorNode.Parent != null)
                    minorIndex = XmlNode.ParseInteger(this.detectorNode, 0, "Index");
                return minorIndex;
            }
            set { minorIndex = value; }
        }

        public DetectorType DetectorType
        {
            get 
            {
                DetectorType result = detectorType;
                if (detectorNode != null)
                    if(detectorNode.Parent != null)
                        if (detectorNode.Parent.Parent != null)
                        {
                            XmlNode parentNode = detectorNode.Parent.Parent;
                            if (parentNode.Name.ToLower().StartsWith("digital"))
                                result = detectorType = DetectorType.Digital;
                            else if (parentNode.Name.ToLower().StartsWith("analog"))
                                result = detectorType = DetectorType.Analog;
                            else if (parentNode.Name.ToLower().StartsWith("fibre"))
                                result = detectorType = DetectorType.Fibre;
                        }
                return result; 
            }
            set { detectorType = value; }
        }

        public string DetectorTypeDisplayName
        {
            get
            {
                return DetectorType.ToString();
            }
        }

        #region Old Properties
        //public double EventValueMinThreshold
        //{
        //    get
        //    {
        //        double result = eventValueMinThreshold;
        //        if (detectorNode != null)
        //            if (detectorNode["MinThreshold"] != null)
        //                if (!double.TryParse(detectorNode["MinThreshold"].Value, out result))
        //                    result = eventValueMinThreshold;

        //        return result;
        //    }
        //    set
        //    {
        //        eventValueMinThreshold = value;

        //        if (detectorNode != null)
        //            if (detectorNode["MinThreshold"] != null)
        //                detectorNode["MinThreshold"].Value = value.ToString();
        //            else
        //                detectorNode.Add("MinThreshold", value.ToString());
        //    }
        //}

        //public double EventValueMaxThreshold
        //{
        //    get
        //    {
        //        double result = eventValueMaxThreshold;
        //        if (detectorNode != null)
        //            if (detectorNode["MaxThreshold"] != null)
        //                if (!double.TryParse(detectorNode["MaxThreshold"].Value, out result))
        //                    result = eventValueMaxThreshold;

        //        return result;
        //    }
        //    set
        //    {
        //        eventValueMaxThreshold = value;

        //        if (detectorNode != null)
        //            if (detectorNode["MaxThreshold"] != null)
        //                detectorNode["MaxThreshold"].Value = value.ToString();
        //            else
        //                detectorNode.Add("MaxThreshold", value.ToString());
        //    }
        //}

        ///// <summary>
        ///// You can set this property if you only want to raise an alarm after this specified rumber of events occured.
        ///// </summary>
        //public int EventsToRaiseAlarm
        //{
        //    get
        //    {
        //        int result = eventsToRaiseAlarm;
        //        if (detectorNode != null)
        //            if (detectorNode["EventsToRaiseAlarm"] != null)
        //                if (!int.TryParse(detectorNode["EventsToRaiseAlarm"].Value, out result))
        //                    result = eventsToRaiseAlarm;

        //        return result;
        //    }
        //    set
        //    {
        //        eventsToRaiseAlarm = value;

        //        if (detectorNode != null)
        //            if (detectorNode["EventsToRaiseAlarm"] != null)
        //                detectorNode["EventsToRaiseAlarm"].Value = value.ToString();
        //            else
        //                detectorNode.Add("EventsToRaiseAlarm", value.ToString());
        //    }
        //}

        ///// <summary>
        ///// Set this if you only want to raise the alarm if a number of events occured in this specified time frame (seconds).
        ///// </summary>
        //public int EventsToRaiseAlarmInterval
        //{
        //    get
        //    {
        //        int result = eventsToRaiseAlarmInterval;
        //        if (detectorNode != null)
        //            if (detectorNode["EventsToRaiseAlarmInterval"] != null)
        //                if (!int.TryParse(detectorNode["EventsToRaiseAlarmInterval"].Value, out result))
        //                    result = eventsToRaiseAlarmInterval;

        //        return result;
        //    }
        //    set
        //    {
        //        eventsToRaiseAlarmInterval = value;

        //        if (detectorNode != null)
        //            if (detectorNode["EventsToRaiseAlarmInterval"] != null)
        //                detectorNode["EventsToRaiseAlarmInterval"].Value = value.ToString();
        //            else
        //                detectorNode.Add("EventsToRaiseAlarmInterval", value.ToString());
        //    }
        //}

        ///// <summary>
        ///// Set this if you want to have a cooldown period before rising the same alarm.
        ///// </summary>
        //public int CoolDown
        //{
        //    get
        //    {
        //        int result = coolDown;
        //        if (detectorNode != null)
        //            if (detectorNode["CoolDown"] != null)
        //                if (!int.TryParse(detectorNode["CoolDown"].Value, out result))
        //                    result = coolDown;

        //        return result;
        //    }
        //    set
        //    {
        //        coolDown = value;

        //        if (detectorNode != null)
        //            if (detectorNode["CoolDown"] != null)
        //                detectorNode["CoolDown"].Value = value.ToString();
        //            else
        //                detectorNode.Add("CoolDown", value.ToString());
        //    }
        //}
        #endregion

        #region Image properties
        public string ImagePath
        {
            get
            {
                return detectorNode != null ? (detectorNode["Image"] != null ? detectorNode["Image"].Value : imagePath) : imagePath;
            }
            set
            {
                imagePath = value;

                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        detectorNode["Image"].Value = value;
                    else
                        detectorNode.Add("Image", value);
            }
        }

        public string ImageMap
        {
            get
            {
                string result = imageMap;
                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Map") != null)
                            result = detectorNode["Image"].Attribute("Map").Value;

                return result;
            }
            set
            {
                imageMap = value;

                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Map") != null)
                            detectorNode["Image"].Attribute("Map").Value = imageMap;
                        else
                            detectorNode["Image"].AddAttribute("Map", value);
            }
        }

        public string ImageX
        {
            get
            {
                string result = imageX;
                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Left") != null)
                            result = detectorNode["Image"].Attribute("Left").Value;

                return result;
            }
            set
            {
                imageX = value;

                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Left") != null)
                            detectorNode["Image"].Attribute("Left").Value = imageMap;
                        else
                            detectorNode["Image"].AddAttribute("Left", value);
            }
        }

        public string ImageY
        {
            get
            {
                string result = imageY;
                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Top") != null)
                            result = detectorNode["Image"].Attribute("Top").Value;

                return result;
            }
            set
            {
                imageY = value;

                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Top") != null)
                            detectorNode["Image"].Attribute("Top").Value = imageMap;
                        else
                            detectorNode["Image"].AddAttribute("Top", value);
            }
        }

        public string ImageWidth
        {
            get
            {
                string result = imageWidth;
                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Width") != null)
                            result = detectorNode["Image"].Attribute("Width").Value;

                return result;
            }
            set
            {
                imageWidth = value;

                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Width") != null)
                            detectorNode["Image"].Attribute("Width").Value = imageWidth;
                        else
                            detectorNode["Image"].AddAttribute("Width", value);
            }
        }

        public string ImageHeight
        {
            get
            {
                string result = imageHeight;
                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Height") != null)
                            result = detectorNode["Image"].Attribute("Height").Value;

                return result;
            }
            set
            {
                imageHeight = value;

                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Height") != null)
                            detectorNode["Image"].Attribute("Height").Value = imageHeight;
                        else
                            detectorNode["Image"].AddAttribute("Height", value);
            }
        }

        public string ImageRotation
        {
            get
            {
                string result = imageRotation;
                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Rotation") != null)
                            result = detectorNode["Image"].Attribute("Rotation").Value;

                return result;
            }
            set
            {
                imageRotation = value;

                if (detectorNode != null)
                    if (detectorNode["Image"] != null)
                        if (detectorNode["Image"].Attribute("Rotation") != null)
                            detectorNode["Image"].Attribute("Rotation").Value = imageRotation;
                        else
                            detectorNode["Image"].AddAttribute("Rotation", value);
            }
        }
        #endregion        
                    
        public int CompareTo(Detector other)
        {
            if (this.DeviceName == "" || other.DeviceName == "" || this.DeviceName == other.DeviceName)
            {
                if (this.DeviceAddress == other.DeviceAddress)
                {
                    if (this.MajorIndex == other.MajorIndex)
                        return this.MinorIndex.CompareTo(other.MinorIndex);
                    else
                        return this.MajorIndex.CompareTo(other.MajorIndex);
                }
                else
                    return this.DeviceAddress.CompareTo(other.DeviceAddress);
            }
            else
                return this.DeviceName.CompareTo(other.DeviceName);
        }

        public int Compare(Detector x, Detector y)
        {
            if (x.DeviceName == "" || y.DeviceName == "" || x.DeviceName == y.DeviceName)
            {
                if (x.DeviceAddress == y.DeviceAddress)
                {
                    if (x.MajorIndex == y.MajorIndex)
                        return x.MinorIndex.CompareTo(y.MinorIndex);
                    else
                        return x.MajorIndex.CompareTo(y.MajorIndex);
                }
                else
                    return x.DeviceAddress.CompareTo(y.DeviceAddress);
            }
            else
                return x.DeviceName.CompareTo(y.DeviceName);
        }
    }

    /// <summary>
    /// This obviously serves to describe detectors (passives, door contacts ect), but it will also help deciding when to raise alarms by keeping count of events, time frames and linked detector events.
    /// </summary>
    //public class DetectorWithEventTracking : Detector, IDisposable, IComparable<DetectorWithEventTracking>, IComparer<DetectorWithEventTracking>
    //{
    //    protected int eventCount = 0;

    //    protected Queue<float> eventValues;
    //    protected Queue<DateTime> eventTimes;

    //    protected DateTime lastTrigger = DateTime.MinValue;

    //    public DetectorWithEventTracking() : base() { }

    //    public DetectorWithEventTracking(string _name, string _deviceMac, int _cardIndex, int _pinIndex, DetectorType _detectorType = DetectorType.Unknown)
    //        : base(_name, _deviceMac, _cardIndex, _pinIndex, _detectorType)
    //    {
    //        eventValues = new Queue<float>();
    //        eventTimes = new Queue<DateTime>();
    //    }

    //    protected override void Dispose(bool disposing)
    //    {
    //        base.Dispose(disposing);

    //        if (eventValues != null)
    //            eventValues.Clear();

    //        if (eventTimes != null)
    //            eventTimes.Clear();
    //    }

    //    public EventTrigger AddEvent()
    //    {
    //        return AddEvent(false, 0);
    //    }

    //    public EventTrigger AddEvent(float _eventValue)
    //    {
    //        return AddEvent(true, _eventValue);
    //    }

    //    protected EventTrigger AddEvent(bool _hasEventValue, float _eventValue)
    //    {
    //        EventTrigger result = null;

    //        eventCount++;

    //        if (eventsToRaiseAlarmInterval > 0)
    //        {
    //            eventTimes.Enqueue(DateTime.Now);
    //        }

    //        if (_hasEventValue)
    //        {
    //            if (eventCount > eventsToRaiseAlarm)
    //                eventValues.Dequeue();

    //            eventValues.Enqueue(_eventValue);
    //        }

    //        if ((eventCount >= eventsToRaiseAlarm 
    //            && ((eventTimes.Count > 0 ? ((DateTime.Now - eventTimes.Peek()).TotalSeconds <= eventsToRaiseAlarmInterval) : true) || eventsToRaiseAlarmInterval == 0)
    //            && (DateTime.Now - lastTrigger).TotalSeconds > coolDown)
    //            || XmlNode.ParseBool(Settings.TheeSettings["NoCooldowns"], false))
    //        {
    //            result = new EventTrigger(name, deviceName, (DetectorType)detectorType, eventCount, DateTime.Now, eventValues.Count > 0 ? eventValues.ToArray() : null);
    //            result.AlarmName = description;
    //            result.Location = location;
    //            result.MapName = ImageMap;

    //            lastTrigger = DateTime.Now;

    //            eventCount = 0;
    //            eventTimes.Clear();
    //        }
    //        else if(eventTimes.Count > 0)
    //        {
    //            while ((DateTime.Now - eventTimes.Peek()).TotalSeconds > eventsToRaiseAlarmInterval)
    //            {
    //                eventTimes.Dequeue();
    //                eventCount--;

    //                if (eventTimes.Count <= 0)
    //                    break;
    //            }
    //        }

    //        return result;
    //    }
        
    //    public int CompareTo(DetectorWithEventTracking other)
    //    {
    //        if (this.DeviceMac == other.DeviceMac)
    //        {
    //            if (this.cardIndex == other.cardIndex)
    //                return this.pinIndex.CompareTo(other.pinIndex);
    //            else
    //                return this.cardIndex.CompareTo(other.cardIndex);
    //        }
    //        else
    //            return this.DeviceMac.CompareTo(other.DeviceMac);            
    //    }

    //    public int Compare(DetectorWithEventTracking x, DetectorWithEventTracking y)
    //    {
    //        if (x.DeviceMac == y.DeviceMac)
    //        {
    //            if (x.cardIndex == y.cardIndex)
    //                return x.pinIndex.CompareTo(y.pinIndex);
    //            else
    //                return x.cardIndex.CompareTo(y.cardIndex);
    //        }
    //        else
    //            return x.DeviceMac.CompareTo(y.DeviceMac);  
    //    }
    //}
}
