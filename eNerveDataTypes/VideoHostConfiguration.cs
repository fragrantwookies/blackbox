﻿using System.Collections.Generic;

namespace eNerve.DataTypes
{
    public class VideoHostConfiguration
    {
        VideoHostConfiguration()
        {
            Name = "Default";
            CameraLayout = CameraLayouts.None;
            CameraPositions = new Dictionary<string, string>();
        }
        public CameraLayouts CameraLayout { get; }
        public Dictionary<string, string> CameraPositions { get; }
        public string Name { get; set; }
        public User User { get; private set; }

        public VideoHostConfiguration(CameraLayouts _cameraLayout, Dictionary<string, string> _cameraPositions, string _name, User _user = null)
        {
            Name = _name;
            CameraLayout = _cameraLayout;
            CameraPositions = _cameraPositions;
            User = _user;
        }
    }
}
