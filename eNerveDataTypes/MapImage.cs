﻿using eThele.Essentials;

namespace eNerve.DataTypes
{
    public class MapImage
    {
        private string imagePath;
        private string imageMap;
        private string imageX;
        private string imageY;
        private string imageWidth;
        private string imageHeight;
        private string imageRotation;

        public XmlNode ImageNode { get; set; }

        public MapImage() { }

        public MapImage(XmlNode _imageNode)
        {
            ImageNode = _imageNode;
        }

        public string ImagePath
        {
            get { return imagePath = ImageNode != null ? XmlNode.ParseString(ImageNode, "") : imagePath; }
            set 
            { 
                imagePath = value; 
                XmlNode.SetVal(ImageNode, value); 
            }
        }

        public string ImageMap
        {
            get { return imageMap = ImageNode != null ? XmlNode.ParseString(ImageNode, "", "Map") : imageMap; }
            set 
            { 
                imageMap = value;
                XmlNode.SetVal(ImageNode, value, "Map"); 
            }
        }

        public string ImageX
        {
            get { return imageX = ImageNode != null ? XmlNode.ParseString(ImageNode, "", "Left") : imageX; }
            set 
            { 
                imageX = value;
                XmlNode.SetVal(ImageNode, value, "Left");
            }
        }

        public string ImageY
        {
            get { return imageY = ImageNode != null ? XmlNode.ParseString(ImageNode, "", "Top") : imageY; }
            set
            {
                imageY = value;
                XmlNode.SetVal(ImageNode, value, "Top");
            }
        }

        public string ImageWidth
        {
            get { return imageWidth = ImageNode != null ? XmlNode.ParseString(ImageNode, "", "Width") : imageWidth; }
            set
            {
                imageWidth = value;
                XmlNode.SetVal(ImageNode, value, "Width");
            }
        }

        public string ImageHeight
        {
            get { return imageHeight = ImageNode != null ? XmlNode.ParseString(ImageNode, "", "Height") : imageHeight; }
            set
            {
                imageHeight = value;
                XmlNode.SetVal(ImageNode, value, "Height");
            }
        }

        public string ImageRotation
        {
            get { return imageRotation = ImageNode != null ? XmlNode.ParseString(ImageNode, "", "Rotation") : imageRotation; }
            set
            {
                imageRotation = value;
                XmlNode.SetVal(ImageNode, value, "Rotation");
            }
        }
    }
}
