﻿using System.Collections.Generic;

namespace eNerve.DataTypes
{
    public class LicensePlateResults
    {
        public int Version { get; set; }
        public string Data_type { get; set; }
        public long Epoch_time { get; set; }
        public int Img_width { get; set; }
        public int Img_height { get; set; }
        public double Processing_time_ms { get; set; }
        public List<object> Regions_of_interest { get; set; }
        public List<Result> Results { get; set; }
    }
    public class Result
    {
        public string Plate { get; set; }
        public double Confidence { get; set; }
        public int Matches_template { get; set; }
        public int Plate_index { get; set; }
        public string Region { get; set; }
        public int Region_confidence { get; set; }
        public double Processing_time_ms { get; set; }
        public int Requested_topn { get; set; }
        public List<Coordinate> Coordinates { get; set; }
        public List<Candidate> Candidates { get; set; }
    }
    public class Candidate
    {
        public string Plate { get; set; }
        public double Confidence { get; set; }
        public int Matches_template { get; set; }
    }
}
