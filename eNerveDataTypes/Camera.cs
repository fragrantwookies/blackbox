﻿using eThele.Essentials;

namespace eNerve.DataTypes
{
    public class Camera
    {
        private string name;
        private string ipAddress;
        private string description;
        private string location;

        public MapImage Image { get; set; }

        private XmlNode cameraNode;

        public Camera() { }

        public Camera(XmlNode _cameraNode)
        {
            CameraNode = _cameraNode;
        }

        public string Name
        {
            get { return name = CameraNode != null ? XmlNode.ParseString(CameraNode["Name"], "") : name; }
            set { name = value; }
        }

        public string IPAddress
        {
            get { return ipAddress = CameraNode != null ? XmlNode.ParseString(CameraNode["IPAddress"], "") : ipAddress; }
            set { ipAddress = value; }
        }

        public string Description
        {
            get { return description = CameraNode != null ? XmlNode.ParseString(CameraNode["Description"], "") : description; }
            set { description = value; }
        }

        public string Location
        {
            get { return location = CameraNode != null ? XmlNode.ParseString(CameraNode["Location"], "") : location; }
            set { location = value; }
        }

        public XmlNode CameraNode 
        { 
            get { return cameraNode; } 
            set 
            { 
                cameraNode = value; 
                if (value["Image"] != null) 
                    Image = new MapImage(value["Image"]);
            } 
        }
    }
}
