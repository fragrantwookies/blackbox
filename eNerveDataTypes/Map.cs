﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    [Serializable]
    public class MapDoc : XMLDoc
    {
        public MapDoc() : base() { }
        public MapDoc(string _name = "") : base(_name) { }

        /// <summary>
        /// Find a map node.
        /// </summary>
        /// <param name="_mapName">Point seperated path of map names (Overview.Location.Detail)</param>   
        /// <returns>Null if not found</returns>
        public XmlNode GetMap(string _mapName)
        {
            XmlNode result = null;

            try
            {
                if(!string.IsNullOrWhiteSpace(_mapName))
                {                    
                    string[] mapNames = _mapName.Split('.');

                    if (mapNames != null)
                    {
                        XmlNode currentLevel = null;
                        foreach (string mapName in mapNames)
                        {
                            if (mapName.ToLower() != "maps")
                            {
                                if (currentLevel == null)
                                {
                                    currentLevel = this[mapName];

                                    if (currentLevel == null && this.Count == 1)
                                    {
                                        if(this[0]["Maps"] != null)
                                            currentLevel = this[0]["Maps"][mapName];
                                    }
                                }
                                else
                                {
                                    if (currentLevel["Maps"] != null)
                                        currentLevel = currentLevel["Maps"][mapName];
                                    else //can't find it
                                    {
                                        currentLevel = null;
                                    }
                                }

                                if (currentLevel == null)
                                    break;
                            }
                        }

                        result = currentLevel;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public XmlNode GetHomeMap()
        {
            XmlNode result = null;

            if (this.Count > 0)
                return this[0];

            return result;
        }
    }
}
