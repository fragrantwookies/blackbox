using System;
using System.Collections.Generic;

namespace eNerve.DataTypes
{
    [Serializable]
    public class EventImageUpdate
    {
        public Guid ImageID { get; set; }

        public Guid EventID { get; set; }

        public byte[] ImageData { get; set; }
    }
}
