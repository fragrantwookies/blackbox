using System;
using System.Collections.Generic;
using eNervePluginInterface;

namespace eNerve.DataTypes
{
    [Serializable]
    public class GroupAlarmUpdate : IDisposable
    {
        private Guid id;
        public Guid ID
        {
            get { return id; }
            set { id = value; }
        }

        private List<IEventTrigger> triggers = null;
        public List<IEventTrigger> Triggers
        {
            get
            {
                if (triggers == null)
                    triggers = new List<IEventTrigger>();

                return triggers;
            }
            set { triggers = value; }
        }

        ~GroupAlarmUpdate()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            triggers.Clear();
        }
    }
}
