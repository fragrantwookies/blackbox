﻿using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    public class ConditionTrigger : IDisposable, IComparable<ConditionTrigger>, IComparer<ConditionTrigger>
    {
        #region Fields
        public string DeviceName { get; set; }
        public string DetectorName { get; set; }
        public DetectorType DetectorType { get; set; }
        public int TriggersToRaiseEvent { get; set; }
        public double TriggersToRaiseEventTimeFrame { get; set; }
        public bool TrueState { get; set; }
        public bool FalseState { get; set; }
        public double MinThreshold { get; set; }
        public double MaxThreshold { get; set; }

        /// <summary>
        /// Only applicable when the condition requires less than 100% of the triggers. Set this to true if it is mandatory that this trigger must be part of the triggers necessary for the condition.
        /// </summary>
        public bool Mandatory { get; set; }

        //private readonly Queue<IEventTrigger> limboTriggers;
        private List<IEventTrigger> limboTriggers;
        #endregion

        #region Constructors
        public ConditionTrigger()
        {
            DeviceName = "";
            DetectorName = "";
            DetectorType = DetectorType.Unknown;
            TriggersToRaiseEvent = 1;
            TriggersToRaiseEventTimeFrame = 0;
            TrueState = true;
            FalseState = true;
            MinThreshold = double.MinValue;
            MaxThreshold = double.MaxValue;
            Mandatory = false;

            limboTriggers = new List<IEventTrigger>();
        }
        #endregion

        #region Cleanup
        ~ConditionTrigger()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            ClearLimboTriggers();
        }
        #endregion

        #region LimboTriggers
        public void ClearLimboTriggers()
        {
            if (limboTriggers != null)
                limboTriggers.Clear();
        }

        public void PurgeLimboTriggers(DateTime _latestTriggerDT)
        {
            //DateTime startDT = DateTime.Now;

            lock (limboTriggers)
            {
                if (limboTriggers.Count > 0)
                {
                    DateTime cutoff = DateTime.MinValue;

                    if (limboTriggers.Count > 0 && _latestTriggerDT < limboTriggers[limboTriggers.Count - 1].TriggerDT)
                        cutoff = limboTriggers[limboTriggers.Count - 1].TriggerDT.AddHours(-1);
                    else
                        cutoff = _latestTriggerDT.AddHours(-1);

                    int timelineIndex = FindTimelineIndex(_latestTriggerDT, true);

                    if (timelineIndex < 0)
                        timelineIndex = ~timelineIndex;

                    for (int i = timelineIndex; i >= 0; i--)
                        if(limboTriggers[i].TriggerDT < cutoff)
                            limboTriggers.RemoveAt(i);
                }

                //while (limboTriggers.Count > 0 && ((_latestTriggerDT - limboTriggers.Peek().TriggerDT).TotalSeconds > _timeFrame))
                //    limboTriggers.Dequeue();
            }
        }

        public void RemoveRangeOfTriggers(DateTime _fromTime, DateTime _toTime)
        {
            int searchStartIndex = FindTimelineIndex(_fromTime, false);
            int searchEndIndex = FindTimelineIndex(_toTime, true);

            for(int i = searchEndIndex; i >= searchEndIndex; i--)
                limboTriggers.RemoveAt(i);
        }

        public void RemoveTriggers(IList<IEventTrigger> _triggersToRemove)
        {
            for(int i = limboTriggers.Count - 1; i >= 0; i--)
            {
                if (_triggersToRemove.FirstOrDefault(f => f.TriggerID == limboTriggers[i].TriggerID) != null)
                    limboTriggers.RemoveAt(i);
            }
        }

        public void AddLimboTrigger(IEventTrigger _trigger)
        {
            int timelineIndex = limboTriggers.BinarySearch(_trigger, new EventTriggerDateComparer());

            if (timelineIndex < 0) // ignore matches, we don't want more than one trigger of the same type at the exact same time
            {
                timelineIndex = ~timelineIndex;

                limboTriggers.Insert(timelineIndex, _trigger);
            }

            //DetectorType = _trigger.DetectorType;
            //limboTriggers.Enqueue(_trigger);
        }

        public List<IEventTrigger> RetrieveLimboTriggers(double Timeframe, DateTime _latestTriggerDT)
        {
            List<IEventTrigger> resultList = new List<IEventTrigger>();

            if (Timeframe < TriggersToRaiseEventTimeFrame)
                Timeframe = TriggersToRaiseEventTimeFrame;

            DateTime smallCutoff = _latestTriggerDT.AddSeconds(Timeframe * -1);
            DateTime bigCutoff = _latestTriggerDT.AddSeconds(Timeframe);

            int searchStartIndex = FindTimelineIndex(smallCutoff, false);
            int searchEndIndex = FindTimelineIndex(bigCutoff, true);

            if (searchStartIndex >= 0 && searchEndIndex >= 0)
            {
                int highCount = 0;
                int highStartIndex = 0;
                int highEndIndex = 0;

                for (int i = searchStartIndex; i <= searchEndIndex; i++)
                {
                    if (i < limboTriggers.Count)
                    {
                        int tempEnd = FindTimelineIndex(limboTriggers[i].TriggerDT.AddSeconds(TriggersToRaiseEventTimeFrame), true);

                        int triggerCount = tempEnd - i + 1;

                        if (triggerCount > highCount)
                        {
                            highCount = triggerCount;
                            highStartIndex = i;
                            highEndIndex = tempEnd;
                        }
                    }
                }

                if (highCount > 0)
                {
                    for (int i = highStartIndex; i <= highEndIndex; i++)
                        resultList.Add(limboTriggers[i]);
                }
            }

            return resultList;
        }

        private int FindTimelineIndex(DateTime _dateTime, bool _getOlderIfNoMatch = false)
        {
            int result = -1;

            if (limboTriggers != null && limboTriggers.Count > 0)
            {
                IEventTrigger searchTrigger = new EventTrigger() { TriggerDT = _dateTime };

                int searchIndex = limboTriggers.BinarySearch(searchTrigger, new EventTriggerDateComparer());

                if (searchIndex < 0)
                    searchIndex = ~searchIndex - (_getOlderIfNoMatch ? 1 : 0);

                result = searchIndex;
            }

            return result;
        }
        #endregion

        #region Compare
        public int CompareTo(ConditionTrigger other)
        {
            int result = this.DeviceName.CompareTo(other.DeviceName);

            if (result == 0)
                result = this.DetectorName.CompareTo(other.DetectorName);

            return result;
        }

        public int Compare(ConditionTrigger x, ConditionTrigger y)
        {
            int result = x.DeviceName.CompareTo(y.DeviceName);

            if (result == 0)
                result = x.DetectorName.CompareTo(y.DetectorName);

            return result;
        }
        #endregion
    }
}