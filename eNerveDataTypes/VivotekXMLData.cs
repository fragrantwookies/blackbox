﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackBoxEngine
{
    public class VivotekXMLPreCurrPosSnapshots : XMLDoc
    {
        public List<string> GetImagePaths()
        {
            List<string> result = null;

            try
            {
                foreach (XmlNode node in this.Children)
                {
                    if (node["destPath"] != null)
                    {
                        string imgPath = node["destPath"].Value;

                        if (!string.IsNullOrWhiteSpace(imgPath))
                        {
                            if (result == null)
                                result = new List<string>();

                            result.Add(imgPath);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public List<XmlNode> GetImageNodes()
        {
            List<XmlNode> result = null;

            try
            {
                foreach (XmlNode node in this.Children)
                {
                    if (node["destPath"] != null)
                    {
                        string imgPath = node["destPath"].Value;

                        if (!string.IsNullOrWhiteSpace(imgPath))
                        {
                            if (result == null)
                                result = new List<XmlNode>();

                            result.Add(node);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public string GetImagePath(XmlNode imageNode)
        {
            string result = "";

            if (imageNode != null)
            {
                if (imageNode["destPath"] != null)
                {
                    result = imageNode["destPath"].Value;
                }
            }

            return result;
        }

        public DateTime GetImageTimeStamp(XmlNode imageNode)
        {
            DateTime result = DateTime.Now;

            if (imageNode != null)
            {
                if (imageNode["destPath"] != null)
                {
                    string path = imageNode["destPath"].Value;
                    string[] parts = path.Split('/');
                    if (parts.Length > 0)
                    {
                        string dt = parts[parts.Length - 1];
                        //string
                        result = DateTime.Parse(string.Format("{0}-{1}-{2} {3}:{4}:{5}", dt.Substring(0, 4), dt.Substring(4, 2), dt.Substring(6, 2), dt.Substring(9, 2), dt.Substring(11, 2), dt.Substring(13, 2)));
                    }
                }
            }

            return result;
        }
    }
}
