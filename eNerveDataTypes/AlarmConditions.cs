﻿using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    public class AlarmConditions : IDisposable
    {
        private AlarmConditionsDoc alarmConditionsDoc = null;
        public AlarmConditionsDoc AlarmConditionsDoc
        {
            get { return alarmConditionsDoc; }
            set { alarmConditionsDoc = value; }
        }

        private List<IEventTrigger> triggers = null; // This list is needed so we can test conditions; i.e. did 2 or more triggers fire within specified time of each other.

        public AlarmConditions(string alarmConditionsDocPath = "")
        {
            alarmConditionsDoc = new AlarmConditionsDoc("AlarmConditions");

            try
            {
                alarmConditionsDoc.Load(alarmConditionsDocPath);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to load AlarmConditions doc", ex);
            }

            triggers = new List<IEventTrigger>();
        }

        ~AlarmConditions()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (alarmConditionsDoc != null)
            {
                alarmConditionsDoc.Dispose();
                alarmConditionsDoc = null;
            }

            if (triggers != null)
            {
                for (int i = triggers.Count - 1; i >= 0; i--)
                {
                    if (triggers[i] is IDisposable)
                        ((IDisposable)triggers[i]).Dispose();

                    triggers.RemoveAt(i);
                }
            }
        }
        
        /// <summary>
        /// The trigger will be tested to alarm conditions and an alarm will be raised if it meets all conditions or if the trigger is not tied to any condition.
        /// </summary>
        /// <returns>An Alarm object (if any was raised, otherwise null), that can be processed by the EventActions</returns>
        public Alarm ProcessTrigger(IEventTrigger _trigger)
        {
            Alarm resultAlarm = null;

            try
            {
                List<XmlNode> triggerConditions = alarmConditionsDoc.TriggerConditions(_trigger.DeviceName, _trigger.DetectorName); //get a list of all the conditions containing this trigger

                if (!XmlNode.ParseBool(Settings.TheeSettings["NoConditions"], false) && triggerConditions != null && triggerConditions.Count > 0)
                {
                    foreach (XmlNode conditionNode in triggerConditions) //run through all the conditions. A trigger can be part of multiple conditions
                    {
                        int triggersTimeFrame = XmlNode.ParseInteger(conditionNode["TriggersTimeFrame"], 0);
                        int coolDown = XmlNode.ParseInteger(conditionNode["CoolDown"], 0);

                        XmlNode lastAlarmNode = conditionNode["LastAlarm"]; //temporary node to keep track of the last alarm that was raised for this condition
                        DateTime lastAlarm = DateTime.MinValue;
                        if (lastAlarmNode != null)
                            lastAlarm = new DateTime(long.Parse(lastAlarmNode.Value));

                        bool raiseAlarm = true; // By default we want to raise the alarm. This is to make sure we raise the alarm in case some fool only assigned 1 trigger for a condition. In such a case it is an alarm.

                        List<IEventTrigger> candidateTriggers = new List<IEventTrigger>();

                        foreach (XmlNode triggerNode in conditionNode["Triggers"]) // Find all the other triggers in this condition
                        {
                            string nodeDeviceName = XmlNode.ParseString(triggerNode, "", "DeviceName");
                            string nodeDetectorName = XmlNode.ParseString(triggerNode, "", "DetectorName");
                            int nodeEventsToRaiseAlarm = XmlNode.ParseInteger(triggerNode, 1, "EventsToRaiseAlarm");
                            bool trueStateOnly = XmlNode.ParseBool(triggerNode, true, "TrueStateOnly");

                            //if (triggerNode.Attribute("DeviceName").Value != _trigger.DeviceName || triggerNode.Attribute("DetectorName").Value != _trigger.DetectorName) // we don't want to test against this trigger
                            //{
                                //IEventTrigger currentTrigger = FindTrigger(triggerNode.Attribute("DeviceName").Value, triggerNode.Attribute("DetectorName").Value);

                            List<IEventTrigger> currentTriggers = FindTriggers(nodeDeviceName, nodeDetectorName, _trigger.TriggerDT, triggersTimeFrame, trueStateOnly);

                            if (nodeDeviceName == _trigger.DeviceName && nodeDetectorName == _trigger.DetectorName)
                                currentTriggers.Add(_trigger);

                            if(currentTriggers != null && (currentTriggers.Count()) >= nodeEventsToRaiseAlarm)
                            {
                                foreach(IEventTrigger t in currentTriggers)
                                    candidateTriggers.Add(t);
                            }
                            else //there is another trigger defined in the condition, but we haven't received enough events from it yet, so no alarm.
                                raiseAlarm = false;

                                //if (currentTrigger != null)
                                //{                                    
                                //    if (((_trigger.TriggerDT - currentTrigger.TriggerDT).TotalSeconds <= triggersTimeFrame && (DateTime.Now - lastAlarm).TotalSeconds > coolDown)
                                //        || XmlNode.ParseBool(Settings.TheeSettings["NoCooldowns"], false))
                                //    {
                                //        raiseAlarm = true;
                                //        candidateTriggers.Add(currentTrigger);
                                //    }
                                //    else
                                //        raiseAlarm = false; //all triggers in condition must have been inside the timeframe;
                                //}
                                //else //there is another trigger defined in the condition, but we haven't received an event from it yet, so no alarm.
                                //    raiseAlarm = false;
                            //}
                        }

                        if (raiseAlarm && ((_trigger.TriggerDT - lastAlarm).TotalSeconds > coolDown || XmlNode.ParseBool(Settings.TheeSettings["NoCooldowns"], false)))
                        {
                            resultAlarm = new Alarm(conditionNode.Name);
                            resultAlarm.Add(_trigger);
                            foreach (IEventTrigger currentTrigger in candidateTriggers)
                            {
                                IEventTrigger newTrigger = ObjectCopier.CloneXML<IEventTrigger>(currentTrigger);
                                newTrigger.TriggerID = Guid.NewGuid();
                                resultAlarm.Add(newTrigger);
                            }

                            if (lastAlarmNode == null)
                                conditionNode.Add("LastAlarm", resultAlarm.AlarmDT.Ticks.ToString());
                            else
                                lastAlarmNode.Value = resultAlarm.AlarmDT.Ticks.ToString();
                        }

                        candidateTriggers.Clear();
                    }

                    AddTrigger(_trigger);
                }
                else // If there is no condition for this trigger it's an automatic alarm.
                {
                    resultAlarm = new Alarm(_trigger.EventName);
                    resultAlarm.Add(_trigger);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return resultAlarm;
        }

        protected List<IEventTrigger> FindTriggers(string _deviceName, string _detectorName, DateTime _lastTriggerDT, int _timeFrame, bool _trueStateOnly)
        {
            List<IEventTrigger> result = new List<IEventTrigger>();

            try
            {
                foreach(IEventTrigger trigger in triggers)
                {
                    if (trigger.DeviceName == _deviceName && trigger.DetectorName == _detectorName && (_lastTriggerDT - trigger.TriggerDT).TotalSeconds <= _timeFrame 
                        && (!_trueStateOnly || (_trueStateOnly && trigger.State)))
                    {
                        result.Add(trigger);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        /// <summary>
        /// Add the trigger to the list. If a trigger for the detector already exists, it will be replaced.
        /// </summary>
        /// <returns>The index of the trigger in the list. -1 if process failed.</returns>
        protected void AddTrigger(IEventTrigger _trigger)
        {
            try
            {
                triggers.Add(_trigger);

                int maxTimeFrame = alarmConditionsDoc.FindLargestTimeFrame(_trigger.DeviceName, _trigger.DetectorName);

                for(int i = triggers.Count - 1; i >= 0; i--)
                {
                    IEventTrigger currentTrigger = triggers[i];

                    if ((_trigger.TriggerDT - currentTrigger.TriggerDT).TotalSeconds > maxTimeFrame)
                    {
                        if (currentTrigger is IDisposable)
                            ((IDisposable)currentTrigger).Dispose();

                        triggers.RemoveAt(i);
                    }
                }                
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
    }
}
