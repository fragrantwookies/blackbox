﻿using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    [Serializable]
    public class Event : Base, IComparable<Event>, IComparer<Event>
    {
        #region Constructors
        public Event() { }

        /// <summary>
        /// Use this constructor to create search items only.
        /// </summary>
        public Event(Guid _id)
        {
            id = _id;
        }

        public Event(string _name)
        {
            id = Guid.NewGuid();
            name = _name;
            alarmDT = DateTime.Now;
            priority = int.MaxValue;

            triggers = new ObservableCollection<IEventTrigger>();
        }
        #endregion Constructors

        #region Cleanup
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            try
            {
                if (triggers != null)
                {
                    for (int i = triggers.Count - 1; i >= 0; i--)
                    {
                        object obj = triggers[i];
                        triggers.RemoveAt(i);

                        if (obj is IDisposable)
                            ((IDisposable)obj).Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error disposing triggers", ex);
            }

            try
            {
                if (actions != null)
                {
                    actions.Dispose();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error disposing actions", ex);
            }
        }
        #endregion

        #region Properties
        private Guid id;
        public Guid ID
        {
            get { return id; }
            set { id = value; PropertyChangedHandler("ID"); }
        }

        private Guid serverID;
        public Guid ServerID
        {
            get { return serverID; }
            set { serverID = value; }
        }

        protected DateTime alarmDT;
        public DateTime AlarmDT
        {
            get { return alarmDT; }
            set { alarmDT = value; PropertyChangedHandler("AlarmDT"); }
        }

        protected int priority;
        public virtual int Priority
        {
            get { return priority; }
            set { priority = value; PropertyChangedHandler("Priority"); }
        }

        private bool rootCause = false;
        public virtual bool RootCause
        {
            get
            {
                return rootCause;
            }
            set
            {
                rootCause = value;
            }
        }

        private ObservableCollection<IEventTrigger> triggers = null;
        public ObservableCollection<IEventTrigger> Triggers
        {
            get
            {
                if (triggers == null)
                    triggers = new ObservableCollection<IEventTrigger>();

                return triggers;
            }
        }

        protected Actions actions;
        public virtual Actions Actions
        {
            get { return actions; }
            set
            {
                actions = value;
                if (actions != null)
                    Priority = actions.Priority;
            }
        }        
        #endregion

        #region Map
        public bool HasMap
        {
            get
            {
                bool result = false;

                if (this.Triggers != null)
                    if (this.Triggers.Count > 0)
                        foreach (IEventTrigger trigger in this.Triggers)
                            if (!string.IsNullOrWhiteSpace(trigger.MapName))
                            {
                                result = true;
                                break;
                            }

                return result;
            }
        }

        public string GetMapName
        {
            get
            {
                string result = "";

                if (this.Triggers != null)
                    if (this.Triggers.Count > 0)
                        foreach (IEventTrigger trigger in this.Triggers)
                            if (!string.IsNullOrWhiteSpace(trigger.MapName))
                            {
                                result = trigger.MapName;
                                break;
                            }

                return result;
            }
        }
        #endregion Map

        #region Misc
        public List<string> DeviceNames
        {
            get
            {
                List<string> result = new List<string>();

                foreach (IEventTrigger trigger in Triggers)
                {
                    if (!result.Contains(trigger.DeviceName))
                        result.Add(trigger.EventName);
                }

                return result;
            }
        }

        public IEventTrigger AddTrigger(IEventTrigger _trigger)
        {
            IEventTrigger trigger = ObjectCopier.CloneXML<IEventTrigger>(_trigger);
            trigger.EventID = this.ID;
            
            Triggers.Add(trigger);

            return trigger;
        }

        #endregion Misc

        #region Images
        private List<EventImage> images;
        /// <summary>
        /// A new list will be created when you use Get while the list is null.
        /// </summary>
        public List<EventImage> Images { get { return images == null ? images = new List<EventImage>() : images; } set { images = value; } }

        public bool HasImages
        {
            get
            {
                return images != null ? images.Count > 0 : false;
            }
        }

        public bool AddImage(EventImage _image)
        {
            bool result = false;

            if (images == null)
                images = new List<EventImage>();

            int searchResult = images.BinarySearch(_image);
            if (searchResult < 0)
            {
                images.Insert(~searchResult, _image);
                result = true;
                PropertyChangedHandler("HasImages");
            }

            return result;
        }

        public void UpdateImageData(EventImageUpdate _update)
        {
            if(images != null)
            {
                for (int i = 0; i < images.Count; i++)
                {
                    if(images[i].ID == _update.ImageID)
                    {
                        images[i].ImageData = _update.ImageData;
                    }
                }    
            }
        }
        #endregion Images

        #region Comparers
        public int CompareTo(Event other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public int Compare(Event x, Event y)
        {
            return x.Name.CompareTo(y.Name);
        }
        #endregion
    }
}
