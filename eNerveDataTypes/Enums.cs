﻿using System;

namespace eNerve.DataTypes
{
    /// TODO investigate the possibility of moving alarm types (or at least extending resolution types) to XML
    [Serializable]
    public enum AlarmType : byte { Unresolved = 0, Valid = 1, Nuisance = 2, Test = 3, Recalibration = 4, AutoResolve = 5 };

    [Serializable]
    public enum EscalationLevel : byte { None = 0, Green = 1, Yellow = 2, Red = 3 };
}
