using System;
using System.Collections.Generic;

namespace eNerve.DataTypes
{
    public class GroupAlarmUpdateEventArgs : EventArgs
    {
        public GroupAlarmUpdateEventArgs(GroupAlarmUpdate update)
            : base()
        {
            Update = update;
        }

        public GroupAlarmUpdate Update { get; set; }
    }
}
