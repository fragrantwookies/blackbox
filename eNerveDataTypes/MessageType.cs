﻿namespace eNerve.DataTypes
{
    /// <summary>
    /// Capped at 32767 or 0x7FFF. When the message is packed we will only use 15 (least significant) bits, not that we are likely to ever need anywhere near that many messages, but still something to keep in mind.
    /// </summary>
    public enum MessageType : ushort
    {
        Echo = 1,
        EchoReturn = 2,
        RespondMessageTypeNotSupported = 3,
        RequestCompressionSupport = 4,
        RespondCompressionSupportTrue = 5,
        RespondCompressionSupportFalse = 6,
        StatusMessage = 7,
        StatusMessageRemove = 8,
        LogMessage = 9,

        Binary = 10,

        String = 11,

        RequestServerVersion = 12,
        ServerVersion = 13,
        RequestServerID = 14,
        ServerID = 15,
        RequestServerName = 16,
        ServerName = 17,

        Event = 20,

        Alarm = 22,
        AlarmPropertyUpdate = 23,
        AlarmResolve = 24,
        AlarmComment = 25,

        /// <summary>A new group alarm was created</summary>
        GroupAlarm = 40,
        /// <summary>A new alarm was added to the group</summary>
        GroupAlarmUpdate = 41,
        GroupAlarmResolve = 42,
        GroupAlarmPropertyUpdate = 43,

        Trigger = 50,

        Siren = 60,

        AlarmImages = 70,
        AlarmImageUpdate = 71,
        AlarmImage = 72,

        RequestUnresolvedAlarms = 80,
        UnresolvedAlarms = 81,
        RequestAlarmHistory = 82,
        AlarmHistory = 83,
        RequestAlarmNames = 84,
        AlarmNames = 85,

        //DeviceOnline = 101,
        //DeviceOffline = 102,
        RequestDevicesOnline = 103,
        OnlineDeviceList = 104,
        DeviceStatus = 105,

        RequestDeviceDoc = 201,
        DeviceDoc = 202,
        RequestMapDoc = 203,
        MapDoc = 204,
        RequestConditionsDoc = 205,
        ConditionsDoc = 206,
        RequestActionsDoc = 207,
        ActionsDoc = 208,
        RequestAlarmSchedule = 209,
        AlarmSchedule = 210,
        RequestPublicDashboard = 211,
        PublicDashboard = 212,
        RequestPrivateDashboard = 213,
        PrivateDashboard = 214,
        GuiDashboard = 215,

        UserLogin = 301,
        UserLogout = 302,
        UserLoginResult = 303,
        RequestUsersOnline = 304,
        UsersOnline = 305,
        UserLoggedOn = 306,
        UserLoggedOff = 307,
        UserList = 308,
        RequestUserRights = 309,
        UserRights = 310,
        ChangePassword = 311,
        RequestUserList = 312,
        DeleteUser = 313,
        /// <summary> Server is closing</summary>
        ForceLogoff = 314,
        CreateUser = 315,

        DatabaseConnectionState = 401,
        RequestDBConnectionState = 402,
        RequestDBSettings = 403,
        /// <summary>Only sent from Server to Client in reponse to RequestDBSettings.</summary>
        SendDBSettings = 404,
        /// <summary>Sent from Client to Server with new settings to test. Only returned if good.</summary>
        TestDBSettings = 405,
        /// <summary>Sent from server to client to show if TestDBSettings passed or not.</summary>
        DBTestResult = 406,
        ReportAddress = 407,
        ReportList = 408,

        SwitchRelay = 501,
        CameraSnapshot = 502,
        CameraLayouts = 503,
        DeleteCameraLayout = 504,
        RemoveCamera = 505,

        /// <summary>Client lets server know about a SiteInstruction it completed and the server in turn lets everyone else know.</summary>
        SiteInstructionStep = 601,

        /// <summary>Can be a master entry or a sub item. The receiver must check which it is and update/add as appropriate.</summary>
        OccurenceBook = 651,
        OccurenceBookPropertyUpdate = 652,
        OccurrenceBookHistory = 653,
        RequestOccurrenceBookHistory = 654,
        OBTypes = 655,
        RequestOBTypes = 656,

        /// <summary>
        /// Plugin messages - request and return types, request and return instances
        /// </summary>
        PluginTypes = 700,
        PluginList = 701,

        RootCauseConditionsDoc = 800,
        RequestRootCauseConditionsDoc = 801,
        RootCauseServerAddress = 802,
        RequestRootCauseServerAddress = 803,
        RootCauseAlarm = 804,
        /// <summary>
        /// The trigger didn't cause a root cause alarm nor add to an existing root cause alarm
        /// </summary>
        RootCauseReturnTrigger = 805,
        /// <summary>
        /// The trigger was added to an already existing root cause alarm
        /// </summary>
        RootCauseAlarmTrigger = 806,
        VideoServerOnline,
        VideoCameraAdded,
        VideoCamerasRequest
    }
}
