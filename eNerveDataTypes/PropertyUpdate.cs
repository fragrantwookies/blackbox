﻿using System;

namespace eNerve.DataTypes
{
    public delegate void PropertyUpdateEventHandler(object sender, PropertyUpdate propertyToUpdate);

    public class PropertyUpdateEventArgs : EventArgs
    {
        public PropertyUpdateEventArgs(PropertyUpdate propertyToUpdate) : base()
        {
            PropertyToUpdate = propertyToUpdate;
        }
        public PropertyUpdate PropertyToUpdate { get; set; }
    }

    [Serializable]
    public class PropertyUpdate
    {
        protected Guid id;
        protected string name;
        protected string propertyName;
        protected object value;

        public PropertyUpdate() { }

        public PropertyUpdate(Guid _id, string _propertyName, object _value)
        {
            id = _id;
            propertyName = _propertyName;
            value = _value;
        }

        public PropertyUpdate(string _name, string _propertyName, object _value)
        {
            name = _name;
            propertyName = _propertyName;
            value = _value;
        }

        public Guid Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string PropertyName
        {
            get { return propertyName; }
            set { propertyName = value; }
        }

        public object Value
        {
            get { return this.value; }
            set { this.value = value; }
        }
    }

    /// <summary>
    /// This class is just a wrapper to identify updates on Alarm objects.
    /// </summary>
    [Serializable]
    public class AlarmPropertyUpdate : PropertyUpdate
    {
        public AlarmPropertyUpdate() : base() { }

        public AlarmPropertyUpdate(Guid _id, string _propertyName, object _value)
            : base(_id, _propertyName, _value)
        { }
    }

    /// <summary>
    /// This class is just a wrapper to identify updates on Alarm objects.
    /// </summary>
    [Serializable]
    public class GroupAlarmPropertyUpdate : PropertyUpdate
    {
        public GroupAlarmPropertyUpdate() : base() { }

        public GroupAlarmPropertyUpdate(Guid _groupID, string _propertyName, object _value)
            : base(_groupID, _propertyName, _value)
        { }
    }       
}
