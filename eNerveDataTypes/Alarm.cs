﻿using System;
using System.Collections.Generic;
using System.Threading;
using eNervePluginInterface;
using eThele.Essentials;
using System.Collections.ObjectModel;

namespace eNerve.DataTypes
{   
    [Serializable]
    public class Alarm : AlarmBase, IComparable<Alarm>, IComparer<Alarm>, IEquatable<Alarm>, IEquatable<Guid>
    {
        #region Constructors
        public Alarm() : base() { }

        public Alarm(Guid _id) : base(_id) { }

        public Alarm(string _name) : base(_name) { }
        #endregion Constructors

        #region Cleanup
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        #endregion Cleanup

        #region Properties
        private Guid groupAlarmID = Guid.Empty;
        public Guid GroupAlarmID
        {
            get { return groupAlarmID; }
            set
            {
                groupAlarmID = value;
                PropertyChangedHandler("GroupAlarmID");
            }
        }
        #endregion Properties

        #region Triggers
        public int Count
        {
            get
            {
                return Triggers.Count;
            }
        }

        public IEventTrigger this[int index]
        {
            get
            {
                return Triggers[index];
            }
        }

        public void Add(IEventTrigger _trigger)
        {
            Triggers.Add(_trigger);
        }

        public static bool MatchAlarms(Alarm alarm1, Alarm alarm2)
        {
            bool result = false;

            if (alarm1.Name == alarm2.Name)
            {
                if ((alarm1.Triggers.Count > 0) && (alarm1.Triggers.Count == alarm2.Triggers.Count))
                {
                    result = true;

                    for (int i = 0; i < alarm1.Triggers.Count; i++)
                    {
                        if ((alarm1.Triggers[i].DeviceName != alarm2.Triggers[i].DeviceName) || (alarm1.Triggers[i].DetectorName != alarm2.Triggers[i].DetectorName))
                        {
                            result = false;
                            break;
                        }
                    }
                }
            }

            return result;
        }
        #endregion Triggers
                 
        #region Misc
        public override string ToString()
        {
            return string.Format("Alarm: {0}, {1}, Triggers: {2}, Actions: {3}", Name, AlarmDT, Triggers.Count.ToString(), Actions.Count.ToString());
        }
        #endregion Misc

        #region Comparers
        public int CompareTo(Alarm other)
        {
            return this.ID.CompareTo(other.ID);
        }

        public int Compare(Alarm x, Alarm y)
        {
            return x.ID.CompareTo(y.ID);
        }

        public bool Equals(Alarm other)
        {
            return this.ID.Equals(other.ID);
        }

        public bool Equals(Guid other)
        {
            return this.ID.Equals(other);
        }
        #endregion Comparers
    }
}