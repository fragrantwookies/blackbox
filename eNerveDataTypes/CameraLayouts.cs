﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    /// <summary>
    /// Okay so technically not a UIObject but vaguely relevant.
    /// </summary>
    public enum CameraLayouts
    {
        One,
        TwoByTwo,
        FivePlusOne,
        ThreeByThree,
        TwelvePlusOne,
        FourByFour,
        None
    }
}
