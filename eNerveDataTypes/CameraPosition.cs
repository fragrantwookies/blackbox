﻿using System;

namespace eNerve.DataTypes
{
    public class CameraPosition
    {
        public string CameraWindowName { get; set; }
        public string PositionName { get; set; }
        public string UserName { get; set; }
        public CameraPosition(string window, string position, string userName)
        {
            CameraWindowName = window;
            PositionName = position;
            UserName = userName;
        }
    }
}
