﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    public class Zone : IDisposable
    {
        public string Name { get; set; }

        public List<Coordinate> Coordinates { get; set; }

        public List<Detector> Detectors { get; set; }

        ~Zone()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (Coordinates != null)
                Coordinates.Clear();

            if (Detectors != null)
                Detectors.Clear();
        }
    }
}