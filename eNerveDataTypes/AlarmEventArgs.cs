using System;
using System.Collections.Generic;

namespace eNerve.DataTypes
{
    public class AlarmEventArgs : EventArgs
    {
        public AlarmEventArgs(AlarmBase alarm)
            : base()
        {
            Alarm = alarm;
        }

        public AlarmBase Alarm { get; set; }
    }
}
