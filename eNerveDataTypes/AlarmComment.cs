﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    [Serializable]
    public class AlarmComment : Base, IEquatable<AlarmComment>
    {
        private Guid alarmID;
        private Guid commentID;
        private DateTime commentDT;
        private string comment;
        private User user;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public Guid AlarmID
        {
            get { return alarmID; }
            set { alarmID = value; PropertyChangedHandler("AlarmID"); }
        }

        public Guid CommentID
        {
            get { return commentID; }
            set { commentID = value; PropertyChangedHandler("CommentID"); }
        }

        public DateTime CommentDT
        {
            get { return commentDT; }
            set { commentDT = value; PropertyChangedHandler("CommentDT"); }
        }

        public string Comment
        {
            get { return comment; }
            set { comment = value; PropertyChangedHandler("Comment"); }
        }

        public User User
        {
            get { return user; }
            set { user = value; PropertyChangedHandler("User"); }
        }

        public string UserName
        {
            get { return user.UserName; }
        }

        public bool Equals(AlarmComment other)
        {
            return this.CommentID.Equals(other.CommentID);
        }
    }
}
