﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    public class ReportFile
    {
        public ReportFile(string name, byte[] data)
        {
            Name = name;
            Data = data;
        }
        public byte[] Data { get; set; }

        public string Name { get; set; }
    }
}
