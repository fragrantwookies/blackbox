using System;
using System.Collections.Generic;
using System.Linq;

namespace eNerve.DataTypes
{
    public class Coordinate
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Coordinate(double _x, double _y)
        {
            X = _x;
            Y = _y;
        }
        public Coordinate()
        { }
    }
}
