using System;
using System.Collections.Generic;
using eNervePluginInterface;

namespace eNerve.DataTypes
{
    /// <summary>
    /// A group alarm groups all alarms with the same name together. This will allow for a much smaller alarm stack, that will be easier to monitor and maintain on the userinterface.
    /// The group will hold the first and last timestamp and an alarm counter. When an alarm is raised it will be added to its group. If the group doesn't exist a new one will be created.
    /// A group can be resolved. When it is resolved, all alarms in the group will be marked as resolved and the group will be removed from the stack.
    /// </summary>
    [Serializable]
    public class GroupAlarm : AlarmBase, IComparable<GroupAlarm>, IComparable<string>, IComparer<GroupAlarm>, IEquatable<GroupAlarm>, IEquatable<string>, IEquatable<Guid>
    {
        #region Constructors
        public GroupAlarm()
            : base()
        {
            this.ID = Guid.NewGuid();
        }

        public GroupAlarm(Alarm _alarm)
            : base()
        {
            if (_alarm.GroupAlarmID != Guid.Empty)
                this.ID = _alarm.GroupAlarmID;
            else
                this.ID = Guid.NewGuid();
            this.Name = _alarm.Name;
            this.AlarmNumber = _alarm.AlarmNumber;
            this.ServerID = _alarm.ServerID;
            this.alarmDT = _alarm.AlarmDT;
            this.lastAlarmDT = _alarm.AlarmDT;
            this.alarm_Type = _alarm.Alarm_Type;
            this.priority = _alarm.Priority;
            this.EscalationLevel = _alarm.EscalationLevel;
            if (_alarm.Actions != null)
                this.Actions = ObjectCopier.CloneXML<Actions>(_alarm.Actions);
            this.CopyTriggers(_alarm.Triggers);
        }
        #endregion Constructors

        #region Cleanup
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        #endregion Cleanup

        #region Properties
        private DateTime lastAlarmDT;
        public virtual DateTime LastAlarmDT
        {
            get { return lastAlarmDT; }
            set { lastAlarmDT = value; PropertyChangedHandler("LastAlarmDT"); }
        }

        private int alarmCount = 1;
        public virtual int AlarmCount
        {
            get { return alarmCount; }
            set { alarmCount = value; PropertyChangedHandler("AlarmCount"); }
        }
        #endregion Properties

        #region Update alarm
        /// <summary>
        /// Call update for each alarm added to the group.
        /// </summary>
        /// <param name="_update">List of all the triggers that resulted in this alarm</param>
        public GroupAlarmUpdate Update(IEnumerable<IEventTrigger> _update)
        {
            GroupAlarmUpdate result = new GroupAlarmUpdate() { ID = this.ID };

            if (_update != null)
            {
                result.Triggers = CopyTriggers(_update);
                if (result.Triggers.Count > 0)
                    this.AlarmCount++;
            }

            return result;
        }

        public void Update(GroupAlarmUpdate _update)
        {
            if (_update != null)
            {
                if (CopyTriggers(_update.Triggers).Count > 0)
                    this.AlarmCount++;
            }
        }

        private List<IEventTrigger> CopyTriggers(IEnumerable<IEventTrigger> _triggers)
        {
            List<IEventTrigger> result = new List<IEventTrigger>();

            if (_triggers != null)
            {
                foreach (IEventTrigger trigger in _triggers)
                    if (trigger != null && !Triggers.Contains(trigger))
                    {
                        IEventTrigger triggerCopy = ObjectCopier.CloneXML<IEventTrigger>(trigger);
                        Triggers.Add(triggerCopy);
                        result.Add(triggerCopy);

                        if (triggerCopy.TriggerDT < this.AlarmDT)
                            this.AlarmDT = triggerCopy.TriggerDT;

                        if (triggerCopy.TriggerDT > this.LastAlarmDT)
                            this.LastAlarmDT = triggerCopy.TriggerDT;
                    }
            }

            return result;
        }
        #endregion Update alarm

        #region Compare
        public int CompareTo(GroupAlarm other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public int CompareTo(string other)
        {
            return this.Name.CompareTo(other);
        }

        public int Compare(GroupAlarm x, GroupAlarm y)
        {
            return x.Name.CompareTo(y.Name);
        }

        public bool Equals(GroupAlarm other)
        {
            return this.Name.Equals(other.Name);
        }

        public bool Equals(string other)
        {
            return this.Name.Equals(other);
        }

        public bool Equals(Guid other)
        {
            return this.ID.Equals(other);
        }
        #endregion Compare
    }
}
