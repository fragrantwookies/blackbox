using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eNerve.DataTypes
{
    public class EventTriggerDateComparer : IComparer<IEventTrigger>
    {
        public int Compare(IEventTrigger x, IEventTrigger y)
        {
            return x.TriggerDT.CompareTo(y.TriggerDT);
        }
    }
}
