using System;
using System.Collections.Generic;

namespace eNerve.DataTypes
{
    [Serializable]
    public class EventImage : CameraImage, IComparable<EventImage>, IComparer<EventImage>, IEquatable<EventImage>, IEquatable<Guid>
    {
        public Guid EventID { get; set; }
        public Guid GroupID { get; set; }

        public EventImage()
        {
            ID = Guid.NewGuid();
        }

        public EventImage(Guid _id)
        {
            ID = _id;
        }

        public int CompareTo(EventImage other)
        {
            int result = this.TimeStamp.CompareTo(other.TimeStamp);
            if (result == 0)
                return this.ID.CompareTo(other.ID);
            else
                return result * -1;
        }

        public int Compare(EventImage x, EventImage y)
        {
            int result = x.TimeStamp.CompareTo(y.TimeStamp);
            if (result == 0)
                return x.ID.CompareTo(y.ID);
            else
                return result * -1;
        }

        public bool Equals(EventImage other)
        {
            return this.ID.Equals(other.ID);
        }

        public bool Equals(Guid other)
        {
            return this.ID.Equals(other);
        }
    }
}
