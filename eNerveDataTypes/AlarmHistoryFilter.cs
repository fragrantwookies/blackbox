using System;
using System.Collections.Generic;

namespace eNerve.DataTypes
{
    [Serializable]
    public class AlarmHistoryFilter
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public AlarmType? AlarmType { get; set; }
        public string AlarmName { get; set; }
        public EscalationLevel? EscalationLevel { get; set; }
        public int? Priority { get; set; }
        public bool? Resolved { get; set; }

        public AlarmHistoryFilter() { }

        public AlarmHistoryFilter(DateTime? _startDate, DateTime? _endDate, AlarmType? _alarmType, string _alarmName, EscalationLevel? _escalationLevel, int? _priority, bool? _resolved)
        {
            StartDate = _startDate;
            EndDate = _endDate;
            AlarmType = _alarmType;
            AlarmName = _alarmName;
            EscalationLevel = _escalationLevel;
            Priority = _priority;
            Resolved = _resolved;
        }

        /// <summary>
        /// Test if alarm passes through the filter
        /// </summary>
        public static bool Test(Alarm _alarm, AlarmHistoryFilter _filter)
        {
            bool result = true;

            if (_filter != null && _alarm != null)
            {
                if (_filter.StartDate != null)
                    if (_alarm.AlarmDT < _filter.StartDate)
                        result = false;

                if (_filter.EndDate != null)
                    if (_alarm.AlarmDT > _filter.EndDate)
                        result = false;

                if (_filter.AlarmType != null)
                    if (_alarm.Alarm_Type != _filter.AlarmType)
                        result = false;

                if (_filter.EscalationLevel != null)
                    if (_alarm.EscalationLevel != _filter.EscalationLevel)
                        result = false;
                if (!string.IsNullOrWhiteSpace(_filter.AlarmName))
                    if (!_alarm.Name.Contains(_filter.AlarmName))
                        result = false;

                if (_filter.Priority != null)
                    if (_alarm.Priority > _filter.Priority)
                        result = false;

                if (_filter.Resolved != null)
                    if (!_alarm.IsResolved && _filter.Resolved.Value)
                        result = false;
            }

            return result;
        }
    }
}
