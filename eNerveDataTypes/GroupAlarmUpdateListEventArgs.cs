using System;
using System.Collections.Generic;

namespace eNerve.DataTypes
{
    public class GroupAlarmUpdateListEventArgs : EventArgs
    {
        public GroupAlarmUpdateListEventArgs(List<GroupAlarmUpdate> update)
            : base()
        {
            Update = update;
        }

        public List<GroupAlarmUpdate> Update { get; set; }
    }
}
