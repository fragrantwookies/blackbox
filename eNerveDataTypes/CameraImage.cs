﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace eNerve.DataTypes
{
    [Serializable]
    public class CameraImage : IEquatable<CameraImage>, INotifyPropertyChanged
    {
        public Guid ID { get; set; }
        public DateTime TimeStamp { get; set; }
        public string CameraAddress { get; set; }
        public string CameraID { get; set; }
        public string SnapshotCommand { get; set; }
        public string PreCurrPostCommand { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Dewarp { get; set; }
        
        public byte[] imageData;
        public byte[] ImageData 
        { 
            get { return imageData; } 
            set { imageData = value; PropertyChangedHandler("ImageData"); } 
        }

        public static CameraImage FromNode(XmlNode _cameraNode)
        {
            CameraImage result = null;

            if (_cameraNode != null)
            {
                result = new CameraImage();
                result.CameraAddress = XmlNode.ParseString(_cameraNode["IPAddress"], "");
                result.CameraID = XmlNode.ParseString(_cameraNode["Name"], "");
                result.SnapshotCommand = XmlNode.ParseString(_cameraNode["Snapshot"], "");
                result.PreCurrPostCommand = XmlNode.ParseString(_cameraNode["PreCurrPost"], "");
                result.UserName = XmlNode.ParseString(_cameraNode["UserName"], "");
                result.Password = XmlNode.ParseString(_cameraNode["Password"], "");
                result.Dewarp = XmlNode.ParseBool(_cameraNode["Dewarp"], false);
            }

            return result;
        }

        public bool Equals(CameraImage other)
        {
            return this.ID.Equals(other.ID);
        }

        #region PropertyChanged
        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }
        #endregion PropertyChanged
    }
}
