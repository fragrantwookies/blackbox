﻿using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eNerve.DataTypes
{
    public class RioDevice : INotifyPropertyChanged, IEquatable<RioDevice>, IComparable<RioDevice>, IComparer<RioDevice>
    {
        private bool online;
        private PluginStatus status;

        public string Address { get; set; }

        public string Name { get; set; }

        public bool Online
        {
            get { return online; }
            set { online = value; PropertyChangedHandler("Online"); }
        }

        public PluginStatus Status
        {
            get { return status; }
            set { status = value; PropertyChangedHandler("Status"); }
        }

        private void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(_propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool Equals(RioDevice other)
        {
            if (!string.IsNullOrWhiteSpace(this.Name) && !string.IsNullOrWhiteSpace(other.Name))
                return this.Name.Equals(other.Name);
            else
                return this.Address.Equals(other.Address);
        }

        public int CompareTo(RioDevice other)
        {
            if (!string.IsNullOrWhiteSpace(this.Name) && !string.IsNullOrWhiteSpace(other.Name))
                return this.Name.CompareTo(other.Name);
            else
                return this.Address.CompareTo(other.Address); 
        }

        public int Compare(RioDevice x, RioDevice y)
        {
            if (!string.IsNullOrWhiteSpace(x.Name) && !string.IsNullOrWhiteSpace(y.Name))
                return x.Name.CompareTo(y.Name);
            else
                return x.Address.CompareTo(y.Address);
        }
    }
}
