using System;

namespace eNerve.DataTypes
{
    [Serializable]
    public enum EscalationLevel : byte { None = 0, Green = 1, Yellow = 2, Red = 3 };
}
