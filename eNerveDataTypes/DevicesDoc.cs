using System;
using System.Collections.Generic;
using eNervePluginInterface;
using eThele.Essentials;

namespace eNerve.DataTypes
{
    [Serializable]
    public class DevicesDoc : XMLDoc
    {
        public DevicesDoc() : base() { }

        public DevicesDoc(string _name = "")
            : base(_name)
        { }

        public string LookupDeviceName(string deviceAddress)
        {
            string result = "";

            foreach (XmlNode deviceNode in Children)
            {
                if (deviceNode.Name != "Cameras")
                {
                    XmlNode deviceAddressNode = deviceNode["DeviceAddress"];
                    if (deviceAddressNode != null)
                    {
                        if (deviceAddressNode.Value == deviceAddress)
                        {
                            result = deviceNode.Name;
                            break;
                        }
                    }
                }
            }

            return result;
        }

        public string LookupDeviceAddress(string deviceName)
        {
            string result = "";

            foreach (XmlNode deviceNode in Children)
            {
                if (deviceNode.Name == deviceName)
                {
                    XmlNode deviceAddressNode = deviceNode["DeviceAddress"];
                    if (deviceAddressNode != null)
                    {
                        result = deviceAddressNode.Value;
                        break;
                    }
                }
            }

            return result;
        }

        public string LookupPluginType(string PluginName)
        {
            string result = "";

            foreach (XmlNode deviceNode in Children)
            {
                if (deviceNode.Name == PluginName)
                {
                    XmlNode propertiesNode = deviceNode["Properties"];
                    if (propertiesNode != null)
                        result = XmlNode.ParseString(propertiesNode["PluginType"], "").Replace(" ", "");

                    break;
                }
            }

            return result;
        }

        public List<Tuple<string, string>> DeviceList(bool pluginsOnly = false)
        {
            List<Tuple<string, string>> devices = new List<Tuple<string, string>>();
            foreach (XmlNode deviceNode in Children)
            {
                if (!string.IsNullOrWhiteSpace(deviceNode.Name) && (deviceNode.Name != "Cameras"))
                {
                    bool isPlugin = false;

                    XmlNode propertiesNode = deviceNode["Properties"];
                    if (propertiesNode != null)
                        isPlugin = XmlNode.ParseBool(propertiesNode["IsPlugin"], false); 

                    XmlNode deviceAddressNode = deviceNode["DeviceAddress"];
                    if (deviceAddressNode != null && (!pluginsOnly || (pluginsOnly && isPlugin)))
                    {
                        Tuple<string, string> item = new Tuple<string, string>(deviceNode.Name, deviceAddressNode.Value);
                        devices.Add(item);
                    }
                }
            }

            devices.Sort((device1, device2) => device1.Item1.CompareTo(device2.Item1));

            return devices;
        }

        public XmlNode LookupDetector(string deviceAddress, int majorIndex, int minorIndex, DetectorType detectorType)
        {
            XmlNode result = null;

            foreach (XmlNode deviceNode in Children)
            {
                if (deviceNode.Name != "Cameras")
                {                    
                    XmlNode deviceAddressNode = deviceNode["DeviceAddress"];
                    if (deviceAddressNode != null)
                    {
                        if (deviceAddressNode.Value == deviceAddress)
                        {
                            result = LookupDetector(deviceNode, majorIndex, minorIndex, detectorType);

                            break;
                        }
                    }
                }
            }

            return result;
        }

        public XmlNode LookupDetector(XmlNode deviceNode, int majorIndex, int minorIndex, DetectorType detectorType)
        {
            XmlNode resultDetectorNode = null;
            try
            {
                XmlNode groupNode = null;

                if (deviceNode != null && deviceNode["Groups"] != null)
                    groupNode = deviceNode["Groups"]["Index", majorIndex.ToString()];

                if (groupNode != null)
                {
                    string ioType = "";
                    switch (detectorType)
                    {
                        case DetectorType.Analog:
                            ioType = "AnalogIn";
                            break;
                        case DetectorType.Digital:
                            ioType = "DigitalIn";
                            break;
                        case DetectorType.Fibre:
                            ioType = "FibreIn";
                            break;
                        default:
                            ioType = "";
                            break;
                    }

                    Tuple<string, string>[] searchattribs = new Tuple<string, string>[] 
                                            { 
                                                new Tuple<string, string>("Index", minorIndex.ToString()), 
                                                new Tuple<string, string>("Type", ioType) 
                                            };

                    resultDetectorNode = groupNode[searchattribs];
                }
            }
            catch (Exception ex)
            {
                resultDetectorNode = null;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return resultDetectorNode;
        }
        public XmlNode LookupRelay(string deviceName, string relayName)
        {
            XmlNode result = null;
            bool found = false;

            XmlNode deviceNode = this[deviceName];
            if (this[deviceName] != null)
            {
                if (deviceNode["Groups"] != null)
                {
                    foreach (XmlNode cardNode in deviceNode["Groups"])
                    {
                        foreach (XmlNode ioNode in cardNode)
                        {
                            if (DevicesDoc.ParseIOType(ioNode) == DeviceIOType.RelayOut)
                            {
                                if (ioNode.Name.ToLower() == relayName.ToLower())
                                {
                                    result = ioNode;
                                    found = true;
                                }
                            }

                            if (found)
                                break;
                        }

                        if (found)
                            break;
                    }
                }
            }

            return result;
        }

        public XmlNode LookupCamera(string deviceName)
        {
            return this["Cameras"] != null ? this["Cameras"][deviceName] : null;
        }

        public IEnumerable<string> FindEventNames()
        {
            List<string> eventNames = null;

            try
            {
                eventNames = new List<string>();

                foreach (XmlNode deviceNode in Children)
                {
                    if (deviceNode.Name != "Cameras")
                    {
                        if (deviceNode["Groups"] != null)
                        {
                            foreach (XmlNode groupNode in deviceNode["Groups"])
                            {
                                foreach (XmlNode ioNode in groupNode)
                                {
                                    DeviceIOType ioType = DevicesDoc.ParseIOType(ioNode);

                                    if (ioType == DeviceIOType.AnalogInput || ioType == DeviceIOType.DigitalInput || ioType == DeviceIOType.FibreInput)
                                    {
                                        string eventName = XmlNode.ParseString(ioNode["EventName"], "");
                                        if (!string.IsNullOrWhiteSpace(eventName))
                                        {
                                            int sResult = eventNames.BinarySearch(eventName);
                                            if (sResult < 0)
                                                eventNames.Insert(~sResult, eventName);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return eventNames;
        }

        public List<IEventTrigger> CreatEventTriggers()
        {
            List<IEventTrigger> resultTriggers = null;

            try
            {
                resultTriggers = new List<IEventTrigger>();

                foreach (XmlNode deviceNode in Children)
                {
                    if (deviceNode.Name != "Cameras")
                    {
                        if (deviceNode["Groups"] != null)
                        {
                            foreach (XmlNode groupNode in deviceNode["Groups"])
                            {
                                foreach (XmlNode ioNode in groupNode)
                                {
                                    DeviceIOType ioType = DevicesDoc.ParseIOType(ioNode);

                                    if (ioType == DeviceIOType.AnalogInput || ioType == DeviceIOType.DigitalInput || ioType == DeviceIOType.FibreInput)
                                    {
                                        string eventName = XmlNode.ParseString(ioNode["EventName"], "");
                                        if (!string.IsNullOrWhiteSpace(eventName))
                                        {
                                            EventTrigger trigger = new EventTrigger() { EventName = eventName, DeviceName = deviceNode.Name, DetectorName = XmlNode.ParseString(ioNode["Name"], "") };
                                            resultTriggers.Add(trigger);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return resultTriggers;
        }

        public enum DeviceIOType { AnalogInput, DigitalInput, FibreInput, RelayOut, Unknown };
        public static DeviceIOType ParseIOType(XmlNode _ioNode)
        {
            DeviceIOType result = DeviceIOType.Unknown;

            if (_ioNode != null)
            {
                string ioTypeStr = XmlNode.ParseString(_ioNode, "", "Type");

                switch (ioTypeStr.ToLower().Substring(0, 1))
                {
                    case "a":
                        result = DeviceIOType.AnalogInput;
                        break;
                    case "d":
                        result = DeviceIOType.DigitalInput;
                        break;
                    case "f":
                        result = DeviceIOType.FibreInput;
                        break;
                    case "r":
                        result = DeviceIOType.RelayOut;
                        break;
                    default:
                        switch (_ioNode.Name.ToLower().Substring(0, 1))
                        {
                            case "a":
                                result = DeviceIOType.AnalogInput;
                                break;
                            case "d":
                                result = DeviceIOType.DigitalInput;
                                break;
                            case "f":
                                result = DeviceIOType.FibreInput;
                                break;
                            case "r":
                                result = DeviceIOType.RelayOut;
                                break;
                            default:
                                result = DeviceIOType.Unknown;
                                break;
                        }
                        break;
                }
            }

            return result;
        }
    }
}
