using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eNerve.DataTypes
{
    [Serializable]
    public class AlarmConditionsDoc : XMLDoc
    {
        public AlarmConditionsDoc() : base() { }

        public AlarmConditionsDoc(string _name = "") : base(_name) { }

        /// <summary>
        /// Find all the Conditions for the Trigger
        /// </summary>
        /// <param name="_deviceName"></param>
        /// <param name="_detectorName"></param>
        /// <returns>A list of all the conditions containing the trigger. You can clear the list when you are done.</returns>
        public List<XmlNode> TriggerConditions(string _deviceName, string _detectorName)
        {
            List<XmlNode> result = null;

            foreach (XmlNode node in this.Children)
            {
                XmlNode triggersNode = node["Triggers"];
                foreach (XmlNode trigger in triggersNode)
                {
                    if (trigger.Attribute("DeviceName").Value == _deviceName && trigger.Attribute("DetectorName").Value == _detectorName)
                    {
                        if (result == null)
                            result = new List<XmlNode>();

                        result.Add(node);
                    }
                }
            }

            return result;
        }

        public int FindLargestTimeFrame(string _deviceName, string _detectorName)
        {
            int result = 0;

            foreach (XmlNode node in this.Children)
            {
                XmlNode triggersNode = node["Triggers"];
                if (triggersNode != null)
                {
                    foreach (XmlNode trigger in triggersNode)
                    {
                        if (trigger.Attribute("DeviceName").Value == _deviceName && trigger.Attribute("DetectorName").Value == _detectorName)
                        {
                            int timeFrame = XmlNode.ParseInteger(node, 0, "TriggersTimeFrame");
                            if (timeFrame > result)
                                result = timeFrame;
                        }
                    }
                }
            }

            return result;
        }

        public List<string> FindAlarmNames(List<string> _includedAlarmNames = null)
        {
            List<string> alarmNames = null;

            try
            {
                alarmNames = _includedAlarmNames != null ? _includedAlarmNames : new List<string>();
                int sResult = -1;

                for (int i = 0; i < this.Count; i++)
                {
                    string alarmName = this[i].Name;
                    sResult = alarmNames.BinarySearch(alarmName);
                    if (sResult < 0)
                        alarmNames.Insert(~sResult, alarmName);
                }

                //sResult = alarmNames.BinarySearch("Everything");
                //if (sResult < 0)
                //    alarmNames.Insert(~sResult, "Everything");
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return alarmNames;
        }
    }
}
