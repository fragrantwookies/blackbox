﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace eNerve.DataTypes
{
    [Serializable]
    public class DashboardImage : Gauges.Gauge
    {
        private byte[] data;
        public string Name { get; set; }
        public string Path { get; set; }
        
        [NonSerialized]
        private BitmapSource val;
        public BitmapSource Value
        {
            get {
                if (val == null)
                    LoadValue();

                return val; }
            set { val = value; //SaveImageData(val);
                PropertyChangedHandler("Value"); }
        }
        public override void SetValue(object _value)
        {
            val = _value as BitmapSource;
        }

        public void LoadValue()
        {
            if (data != null && data.Length > 0)
            {
                //val = (BitmapSource)new ImageSourceConverter().ConvertFrom(data);
                BitmapImage img = new BitmapImage();
                img.BeginInit();
                img.StreamSource = new MemoryStream(data);
                img.EndInit();
                val = img;

                PropertyChangedHandler("Value");
            }
        }

        private void SaveImageData(BitmapSource image)
        {
            MemoryStream ms = new MemoryStream();
            GifBitmapEncoder encoder = new GifBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(image));
            encoder.Save(ms);
            data = ms.ToArray();
        }

        public void SaveImageFile(string path = "")
        {
            if (string.IsNullOrEmpty(path))
                path = Path;
            data = File.ReadAllBytes(path);
        }
    }
}
