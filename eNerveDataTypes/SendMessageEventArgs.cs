using System;

namespace eNerve.DataTypes
{
    public class SendMessageEventArgs : EventArgs
    {
        public MessageType MessageType { get; private set; }
        public object Message { get; private set; }
        public SendMessageEventArgs(MessageType type, object contents)
        {
            MessageType = type;
            Message = contents;
        }
    }
}
