﻿using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace eNerve.DataTypes
{
    [Serializable]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Fields should be disposed")]
    public class EventActions : XMLDoc
    {
        private List<Actions> eventActions;

        [NonSerialized]
        private PluginManager pluginManager;
        public PluginManager PluginManager { get { return pluginManager; } set { pluginManager = value; } }

        public EventActions() : base() { }

        public EventActions(string _name = "")
            : base(_name)
        {
            eventActions = new List<Actions>();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (eventActions != null)
            {
                ClearActions();
            }
        }

        protected void ClearActions()
        {
            for (int i = eventActions.Count - 1; i >= 0; i--)
            {
                Actions actions = eventActions[i];
                eventActions.RemoveAt(i);
                if (actions != null)
                    actions.Dispose();
            }

            eventActions.Clear();
        }

        public override bool Load(string _fileName = "")
        {
            if (base.Load(_fileName))
            {
                return ProcessData();
            }
            else
                return false;
        }

        public bool ProcessData(string thisPluginTypeOnly = "")
        {
            bool result = true;

            try
            {
                if (string.IsNullOrWhiteSpace(thisPluginTypeOnly))
                    ClearActions();

                foreach (XmlNode eventNode in Children)
                {
                    AddActions(ProcessActions(eventNode, thisPluginTypeOnly));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Converts action nodes into actions.
        /// </summary>
        /// <param name="ActionsNode">a node containing 1 or more action nodes</param>
        /// <returns>a list of actions</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Complex member")]
        protected Actions ProcessActions(XmlNode actionsNode, string thisPluginTypeOnly = "")
        {
            Actions result = null;

            try
            {
                Actions currentActions = new Actions(actionsNode.Name) { Priority = ParseInteger(actionsNode, int.MaxValue, "Priority") };

                foreach (XmlNode actionNode in actionsNode)
                {
                    string actionName = actionNode.Name;

                    if (actionName.ToLower() == "plugins")
                    {
                        if (this.PluginManager != null)
                        {
                            foreach (XmlNode actionChildNode in actionNode)
                            {
                                string pluginName = ParseString(actionChildNode, "", "PluginName");
                                string pluginActionName = ParseString(actionChildNode, "", "ActionName");

                                if (string.IsNullOrWhiteSpace(thisPluginTypeOnly) || pluginName == thisPluginTypeOnly)
                                {
                                    var action = PluginManager.CreateAction(pluginName, pluginActionName);
                                    if (action != null)
                                    {
                                        action.Name = actionChildNode.Name;
                                        action.Value = ParseBool(actionChildNode, false);
                                        action.ActionOrder = ParseInteger(actionChildNode, int.MaxValue, "ActionOrder");
                                        action.ActionType = EventActionType.Plugin;

                                        foreach (XmlItem attribute in actionChildNode.Attributes)
                                            if (attribute.Name != "ActionName" && attribute.Name != "ActionOrder")
                                                PluginManager.SetActionProperty(action, attribute.Name, attribute.Value);

                                        currentActions.Add(action);
                                    }
                                }
                            }
                        }
                    }
                    else if(actionName.ToLower() == "escalations")
                    {
                        int delayGreen = actionNode["Blue"] != null ? ParseInteger(actionNode["Blue"], 0, "Delay") : 60; //Blue was replaced by Green, this is for compatibility's sake
                        delayGreen = actionNode["Green"] != null ? ParseInteger(actionNode["Green"], 0, "Delay") : 60;
                        int delayYellow = actionNode["Yellow"] != null ? ParseInteger(actionNode["Yellow"], 0, "Delay") : 60;
                        int delayRed = actionNode["Red"] != null ? ParseInteger(actionNode["Red"], 0, "Delay") : 60;

                        EscalationsAction escalation = new EscalationsAction(actionName, true, delayGreen, delayYellow, delayRed, ParseInteger(actionNode, int.MaxValue, "ActionOrder"));

                        if (actionNode["Blue"] != null) //Blue was replaced by Green, this is for compatibility's sake
                            if (actionNode["Blue"]["Actions"] != null)
                                escalation.GreenActions = ProcessActions(actionNode["Blue"]["Actions"], thisPluginTypeOnly);
                        if (actionNode["Green"] != null)
                            if (actionNode["Green"]["Actions"] != null)
                                escalation.GreenActions = ProcessActions(actionNode["Green"]["Actions"], thisPluginTypeOnly);
                        if (actionNode["Yellow"] != null)
                            if (actionNode["Yellow"]["Actions"] != null)
                                escalation.YellowActions = ProcessActions(actionNode["Yellow"]["Actions"], thisPluginTypeOnly);
                        if (actionNode["Red"] != null)
                            if (actionNode["Red"]["Actions"] != null)
                                escalation.RedActions = ProcessActions(actionNode["Red"]["Actions"], thisPluginTypeOnly);

                        currentActions.Add(escalation);

                        break;
                    }
                    else if (string.IsNullOrWhiteSpace(thisPluginTypeOnly))
                    {
                        switch (actionName.ToLower())
                        {
                            case "db":
                                currentActions.Add(new Action(actionName, ParseBool(actionNode, false), EventActionType.DB, ParseInteger(actionNode, int.MaxValue, "ActionOrder")));
                                break;
                            case "log":
                                currentActions.Add(new Action(actionName, ParseBool(actionNode, false), EventActionType.Log, ParseInteger(actionNode, int.MaxValue, "ActionOrder")));
                                break;
                            case "siren":
                                currentActions.Add(new SirenAction(actionName, ParseBool(actionNode, false), ParseString(actionNode, "", "SoundFile"), ParseInteger(actionNode, 0, "Duration"), ParseInteger(actionNode, int.MaxValue, "ActionOrder")));
                                break;
                            case "relays":
                                {
                                    foreach (XmlNode actionChildNode in actionNode)
                                        currentActions.Add(new RelayAction(ParseString(actionChildNode, actionChildNode.Name, "RelayName"), ParseBool(actionChildNode, false), ParseInteger(actionChildNode, 0, "Duration"), ParseString(actionChildNode, "", "DeviceName"), ParseInteger(actionChildNode, int.MaxValue, "ActionOrder")));

                                    break;
                                }
                            case "sirens":
                                {
                                    foreach (XmlNode actionChildNode in actionNode)
                                        currentActions.Add(new SirenAction(actionChildNode.Name, ParseBool(actionChildNode, false), ParseString(actionChildNode, "", "SoundFile"), ParseDouble(actionChildNode, 0, "Duration"), ParseInteger(actionChildNode, int.MaxValue, "ActionOrder")));

                                    break;
                                }
                            case "cameras":
                                {
                                    foreach (XmlNode actionChildNode in actionNode)
                                        currentActions.Add(new CameraAction(actionChildNode.Name, ParseBool(actionChildNode, false), ParseInteger(actionChildNode, int.MaxValue, "ActionOrder")));

                                    break;
                                }
                            case "email":
                                {
                                    foreach (XmlNode actionChildNode in actionNode)
                                        currentActions.Add(new EmailAction(actionChildNode.Name, ParseBool(actionChildNode, false), ParseString(actionChildNode, "", "Address"), ParseInteger(actionChildNode, int.MaxValue, "ActionOrder")));

                                    break;
                                }
                            case "autoresolves":
                                {
                                    foreach (XmlNode actionChildNode in actionNode)
                                        currentActions.Add(new AutoResolveAction(actionChildNode.Name, ParseBool(actionChildNode, false), ParseString(actionChildNode, "", "AlarmToResolve"), ParseInteger(actionChildNode, 0, "TimeDelay"), ParseInteger(actionChildNode, int.MaxValue, "ActionOrder")));

                                    break;
                                }
                            case "siteinstructions":
                                {
                                    SiteInstructionAction siteInstructionAction = new SiteInstructionAction(actionName, true, ParseInteger(actionNode, int.MaxValue, "ActionOrder"));

                                    for (int i = 0; i < actionNode.Count; i++)
                                    {
                                        XmlNode stepNode = actionNode[i];
                                        siteInstructionAction.SiteInstructionSteps.Add(new SiteInstructionStep(stepNode.Name, stepNode.Value, ParseBool(stepNode, false, "CompleteToResolve"), i));
                                    }

                                    currentActions.Add(siteInstructionAction);

                                    break;
                                }
                            default:
                                break;
                        }
                    }
                }

                result = currentActions;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                result = null;
            }

            return result;
        } 

        public int CountEventActions
        {
            get
            {
                return eventActions.Count;
            }
        }

        public List<string> FindAlarmNames(List<string> _alarmNames = null)
        {
            List<string> alarmNames = null;

            try
            {
                alarmNames = _alarmNames != null ? _alarmNames : new List<string>();

                for (int i = 0; i < eventActions.Count; i++)
                {
                    string alarmName = eventActions[i].Name;
                    if (alarmName != "Default")
                    {
                        int sResult = alarmNames.BinarySearch(alarmName);
                        if (sResult < 0)
                            alarmNames.Insert(~sResult, alarmName);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return alarmNames;
        }

        /// <param name="_eventActionName">Alarm Condition Name or DeviceInput Description of the detector that triggered</param>
        public Actions GetActions(string _eventActionName)
        {
            Actions result = null;

            try
            {
                if (eventActions != null && eventActions.Count > 0)
                {
                    Actions tmpActions = new Actions(_eventActionName);
                    int searchResult = eventActions.BinarySearch(tmpActions);
                    if (searchResult >= 0)
                    {
                        result = eventActions[searchResult];
                    }
                    else if (_eventActionName != "Default") //prevent circular reference
                    {
                        result = GetActions("Default");
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public Actions GetActionsDeepCopy(string _eventActionName)
        {
            Actions result = null;
            try
            {
                Actions actions = GetActions(_eventActionName);
                if (actions != null)
                {
                    //result = ObjectCopier.Clone<Actions>(GetActions(_eventActionName));
                    result = ObjectCopier.CloneXML<Actions>(GetActions(_eventActionName));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            return result;
        }

        public ObservableCollection<SiteInstructionStep> GetSiteInstructionsDeepCopy(string _eventActionName)
        {
            ObservableCollection<SiteInstructionStep> result = null;

            try
            {
                Actions tmpActions = new Actions(_eventActionName);
                Actions resultActions = null;
                int searchResult = eventActions.BinarySearch(tmpActions);
                if (searchResult >= 0)
                {
                    resultActions = eventActions[searchResult];
                }
                else if (_eventActionName != "Default") //prevent circular reference
                {
                    resultActions = GetActions("Default");
                }

                if (resultActions != null)
                {
                    SiteInstructionAction siAction = resultActions["SiteInstructions"] as SiteInstructionAction;
                    if (siAction != null)
                    {
                        if (siAction.SiteInstructionSteps.Count > 0)
                        {
                            result = ObjectCopier.Clone<SiteInstructionAction>(siAction).SiteInstructionSteps;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public int AddActions(Actions _actions)
        {
            int result = -1;

            try
            {
                int searchResult = eventActions.BinarySearch(_actions);
                if (searchResult < 0)
                {
                    eventActions.Insert(~searchResult, _actions);
                    result = ~searchResult;
                }
                else
                {
                    Actions actions = eventActions[searchResult];
                    if (_actions.ActionList != null && _actions.ActionList.Count > 0)
                    {
                        foreach (IAction a in _actions.ActionList)
                            actions.Add(a);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }
    }

    [Serializable]
    public class Actions : IDisposable, IComparable<Actions>, IComparer<Actions>
    {
        private string name;
        private int priority = int.MaxValue;

        private List<IAction> actions;
        public List<IAction> ActionList { get { return actions; } set { actions = value; } }

        public bool SortedByActionOrder { get; set; }

        public Actions() { }

        public Actions(string _name)
        {
            name = _name;
        }

        #region Cleanup
        ~Actions()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            try
            {
                if (actions != null)
                {
                    for (int i = actions.Count - 1; i >= 0; i--)
                    {
                        if (actions[i] != null)
                        {
                            IAction action = actions[i];
                            actions.RemoveAt(i);
                            if (action != null && action is IDisposable)
                                ((IDisposable)action).Dispose();
                            action = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Cleanup

        public void Add(IAction _action)
        {
            if (_action != null)
            {
                if (actions == null)
                    actions = new List<IAction>();

                if (SortedByActionOrder)
                {
                    actions.Sort(Actions.CompareByName);
                    SortedByActionOrder = false;
                }

                int searchResult = actions.BinarySearch(_action);

                if (searchResult < 0)
                {
                    actions.Insert(~searchResult, _action);
                }
                else
                {
                    if(_action.Name != "SiteInstructions" && _action.Name != "Escalations")
                        actions.Insert(searchResult, _action);
                    //IDisposable tempAction = actions[searchResult] as IDisposable;
                    //actions[searchResult] = _action;
                    //if (tempAction != null)
                    //    tempAction.Dispose();
                }
            }
        }

        public int Count
        {
            get
            {
                if (actions != null)
                    return actions.Count;
                else
                    return 0;
            }
        }

        public IAction this[int index]
        {
            get
            {
                if (!SortedByActionOrder)
                {
                    actions.Sort(Actions.CompareByActionOrder);
                    SortedByActionOrder = true;
                }

                if (actions != null)
                    return actions[index];
                else
                    return null;
            }
        }

        public IAction this[string _name]
        {
            get
            {
                if (actions != null)
                {
                    if (SortedByActionOrder)
                    {
                        actions.Sort(Actions.CompareByName);
                        SortedByActionOrder = false;
                    }

                    IAction searchAction = new Action(_name);
                    int searchResult = actions.BinarySearch(searchAction);

                    if (searchResult >= 0)
                    {
                        return actions[searchResult];
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
        }

        public void Clear()
        {
            if(actions != null)
                actions.Clear();
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int Priority
        {
            get { return priority; }
            set { priority = value; }
        }

        #region Comparers
        public int CompareTo(Actions other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public int Compare(Actions x, Actions y)
        {
            return x.Name.CompareTo(y.Name);
        }

        public static int CompareByActionOrder(IAction x, IAction y)
        {
            if (x.ActionOrder == y.ActionOrder)
            {
                return x.Name.CompareTo(y.Name);
            }
            else
                return x.ActionOrder.CompareTo(y.ActionOrder);
        }

        public static int CompareByName(IAction x, IAction y)
        {
            return x.Name.CompareTo(y.Name);
        }
        #endregion Comparers
    }

    [Serializable]
    public class Action : eNervePluginInterface.IAction, IDisposable
    {
        public Action() { }
        
        /// <summary>
        /// Use this constructor only for searching.
        /// </summary>
        public Action(string _name)
        {
            Name = _name;
            ActionType = EventActionType.Unknown;
        }

        public Action(string _name, bool _value, EventActionType _actionType, int _actionOrder = 0)
        {
            Name = _name;
            Value = _value;
            ActionType = _actionType;
            ActionOrder = _actionOrder;
        }

        ~Action()
        {
            this.Dispose(false);
        }

        public void Dispose()
        {
            this.Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);
        }

        public string Name { get; set; }

        public bool Value { get; set; }

        public int ActionOrder { get; set; }

        public EventActionType ActionType { get; set; }

        public int CompareByActionOrder(IAction x, IAction y)
        {
            if (x.ActionOrder == y.ActionOrder)
            {
                return x.Name.CompareTo(y.Name);
            }
            else
                return x.ActionOrder.CompareTo(y.ActionOrder);
        }

        public int CompareByName(IAction x, IAction y)
        {
            return x.Name.CompareTo(y.Name);
        }

        public int CompareTo(IAction other)
        {
            return this.Name.CompareTo(other.Name);
        }

        public int Compare(IAction x, IAction y)
        {
            return x.Name.CompareTo(y.Name);
        }
    }

    [Serializable]
    public class RelayAction : Action
    {
        protected int duration;
        protected string deviceName = "";

        public RelayAction() : base() { }

        public RelayAction(string _name, bool _value, int _duration, string _deviceName, int _actionOrder = 0)
            : base(_name, _value, EventActionType.Relay, _actionOrder)
        {
            duration = _duration;
            deviceName = _deviceName;
        }

        public int Duration
        {
            get { return duration; }
            set { duration = value; }
        }

        public string DeviceName
        {
            get { return deviceName; }
            set { deviceName = value; }
        }
    }

    [Serializable]
    public class SirenAction : Action
    {
        protected string soundFile;
        protected double duration;

        public SirenAction() : base() { }

        public SirenAction(string _name, bool _value, string _soundFile, double _duration, int _actionOrder = 0)
            : base(_name, _value, EventActionType.Siren, _actionOrder)
        {
            soundFile = _soundFile;
            duration = _duration;
        }

        public string SoundFile
        {
            get { return soundFile; }
            set { soundFile = value; }
        }

        public double Duration
        {
            get { return duration; }
            set { duration = value; }
        }
    }

    [Serializable]
    public class CameraAction : Action
    {
        public CameraAction() : base() { }

        public CameraAction(string _name, bool _value, int _actionOrder = 0)
            : base(_name, _value, EventActionType.Camera, _actionOrder)
        {}
    }

    [Serializable]
    public class EmailAction : Action
    {
        private string addresses;

        public EmailAction() : base() { }

        public EmailAction(string _name, bool _value, string _addresses, int _actionOrder = 0)
            : base(_name, _value, EventActionType.Email, _actionOrder)
        {
            addresses = _addresses;
        }

        public string Addresses
        {
          get { return addresses; }
          set { addresses = value; }
        }
    }

    [Serializable]
    public class SiteInstructionAction : Action
    {        
        protected ObservableCollection<SiteInstructionStep> siteInstructionSteps;

        public SiteInstructionAction() : base() { }

        public SiteInstructionAction(string _name, bool _value, int _actionOrder = 0)
            : base(_name, _value, EventActionType.SiteInstruction, _actionOrder)
        {
            siteInstructionSteps = new ObservableCollection<SiteInstructionStep>();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if(siteInstructionSteps != null)
                siteInstructionSteps.Clear();
        }

        public ObservableCollection<SiteInstructionStep> SiteInstructionSteps
        {
            get { return siteInstructionSteps; }
            set { siteInstructionSteps = value; }
        }

        public void AssignNewStepIDs()
        {
            foreach (SiteInstructionStep step in siteInstructionSteps)
            {
                step.SiteInstructionStepID = Guid.NewGuid();
            }
        }
    }

    [Serializable]
    public class SiteInstructionStep : System.ComponentModel.INotifyPropertyChanged, IEquatable<SiteInstructionStep>
    {
        public Guid SiteInstructionStepID { get; set; }
        public Guid AlarmID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int Order { get; set; }

        private bool completed;
        public bool Completed { get { return completed; } set { completed = value; PropertyChangedHandler("Completed"); } }
        public bool CompleteToResolve { get; set; }

        private DateTime completedDT;
        public DateTime CompletedDT { get { return completedDT; } set { completedDT = value; PropertyChangedHandler("CompletedDT"); PropertyChangedHandler("CompletedByDisplay"); } }

        private User user;

        public User User
        {
            get { return user; }
            set { user = value; PropertyChangedHandler("User"); PropertyChangedHandler("CompletedByDisplay"); }
        }

        public SiteInstructionStep() { }

        public SiteInstructionStep(string _name, string _value, bool _completeToResolve, int _order = 0)
        {
            SiteInstructionStepID = Guid.NewGuid();
            Name = _name;
            Value = _value;
            Completed = false;
            CompleteToResolve = _completeToResolve;
            Order = _order;
        }

        public SiteInstructionStep(Guid _siteInstructionStepID, Guid _alarmID, string _name, string _value, bool _completed, User _user, bool _completeToResolve, DateTime _completedDT, int _order = 0)
        {
            SiteInstructionStepID = _siteInstructionStepID;
            AlarmID = _alarmID;
            Name = _name;
            Value = _value;
            Completed = _completed;
            User = _user;
            CompleteToResolve = _completeToResolve;
            Order = _order;
            CompletedDT = _completedDT;
        }

        public string DisplayValue
        {
            get
            {
                return Name + ": " + Value;
            }
        }

        public string CompletedByDisplay
        {
            get
            {
                return string.Format("Completed by {0} on {1}", User != null ? User.Name : "Unknown", completedDT.ToString("yyyy/MM/dd HH:mm:ss"));
            }
        }

        private void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }

        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public bool Equals(SiteInstructionStep other)
        {
            return this.Name.Equals(other.Name);
        }
    }
    
    [Serializable]
    public class EscalationsAction : Action
    {
        private int greenDelay;
        private int yellowDelay;
        private int redDelay;

        private Actions greenActions;
        private Actions yellowActions;
        private Actions redActions;

        public EscalationsAction() : base() { }

        public EscalationsAction(string _name, bool _value, int _greenDelay, int _yellowDelay, int _redDelay, int _actionOrder = 0)
            : base(_name, _value, EventActionType.Escalation, _actionOrder)
        {
            greenDelay = _greenDelay;
            yellowDelay = _yellowDelay;
            redDelay = _redDelay;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (greenActions != null)
                greenActions.Dispose();

            if (yellowActions != null)
                yellowActions.Dispose();

            if (redActions != null)
                redActions.Dispose();
        }

        public int GreenDelay
        {
            get { return greenDelay; }
            set { greenDelay = value; }
        }

        public int YellowDelay
        {
            get { return yellowDelay; }
            set { yellowDelay = value; }
        }

        public int RedDelay
        {
            get { return redDelay; }
            set { redDelay = value; }
        }

        public Actions GreenActions
        {
            get { return greenActions; }
            set { greenActions = value; }
        }

        public Actions YellowActions
        {
            get { return yellowActions; }
            set { yellowActions = value; }
        }

        public Actions RedActions
        {
            get { return redActions; }
            set { redActions = value; }
        }
    }

    [Serializable]
    public class AutoResolveAction : Action
    {
        public AutoResolveAction() : base() { }

        public AutoResolveAction(string _name, bool _value, string _alarmToResolve, int _timeDelay, int _actionOrder = 0)
            : base(_name, _value, EventActionType.AutoResolve, _actionOrder)
        {
            TimeDelay = _timeDelay;
            AlarmToResolve = _alarmToResolve;
        }

        public int TimeDelay { get; set; }

        public string AlarmToResolve { get; set; }
    }
}
