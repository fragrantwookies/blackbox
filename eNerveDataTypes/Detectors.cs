using System;
using System.Collections.Generic;
using eNervePluginInterface;
using eThele.Essentials;

namespace eNerve.DataTypes
{
    public class Detectors : IDisposable, IEnumerable<Detector>
    {
        protected List<Detector> detectors;

        protected DevicesDoc devicesDoc = null;

        public Detectors(string devicesDocPath = "")
        {
            detectors = new List<Detector>();

            devicesDoc = new DevicesDoc("Devices");
            try
            {
                devicesDoc.Load(devicesDocPath);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to load device doc", ex);
            }
        }

        ~Detectors()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (detectors != null)
            {
                for (int i = detectors.Count - 1; i >= 0; i--)
                    detectors[i].Dispose();

                detectors.Clear();
                detectors = null;
            }

            if (devicesDoc != null)
            {
                devicesDoc.Dispose();
                devicesDoc = null;
            }
        }

        public void Reload()
        {
            if (detectors != null)
            {
                for (int i = detectors.Count - 1; i >= 0; i--)
                    detectors[i].Dispose();

                detectors.Clear();
            }
        }

        public int Count
        {
            get
            {
                return detectors.Count;
            }
        }

        public Detector this[int i]
        {
            get
            {
                return detectors[i];
            }
        }        

        public Detector this[string deviceAddress, int majorIndex, int minorIndex, DetectorType detectorType]
        {
            get
            {
                Detector result = null;

                result = new Detector("", deviceAddress) { MajorIndex = majorIndex, MinorIndex = minorIndex };

                int searchResult = detectors.BinarySearch(result);
                if (searchResult >= 0)
                    result = detectors[searchResult];
                else
                {
                    XmlNode detectorNode = devicesDoc.LookupDetector(deviceAddress, majorIndex, minorIndex, detectorType);

                    if (detectorNode != null)
                    {
                        result = new Detector(detectorNode["Name"].Value, deviceAddress, detectorType) { MajorIndex = majorIndex, MinorIndex = minorIndex };

                        result.DeviceName = devicesDoc.LookupDeviceName(deviceAddress);
                        result.EventName = XmlNode.ParseString(detectorNode["EventName"], "");
                        result.Location = XmlNode.ParseString(detectorNode["Location"], "");
                        result.ImageMap = XmlNode.ParseString(detectorNode["Image"], "", "Map");
                        result.ImagePath = XmlNode.ParseString(detectorNode["Image"], "");
                        result.ImageX = XmlNode.ParseString(detectorNode["Image"], "", "Left");
                        result.ImageY = XmlNode.ParseString(detectorNode["Image"], "", "Top");
                        result.ImageWidth = XmlNode.ParseString(detectorNode["Image"], "", "Width");
                        result.ImageHeight = XmlNode.ParseString(detectorNode["Image"], "", "Height");
                        result.ImageRotation = XmlNode.ParseString(detectorNode["Image"], "", "Rotation");

                        //result.EventsToRaiseAlarm = XmlNode.ParseInteger(detectorNode["EventsToRaiseAlarm"], 1);
                        //result.EventsToRaiseAlarmInterval = XmlNode.ParseInteger(detectorNode["EventsToRaiseAlarmInterval"], 0);
                        //result.CoolDown = XmlNode.ParseInteger(detectorNode["CoolDown"], 0);
                        //if (detectorType == DetectorType.Analog)
                        //{
                        //    result.EventValueMinThreshold = XmlNode.ParseDouble(detectorNode["MinThreshold"], 0);
                        //    result.EventValueMaxThreshold = XmlNode.ParseDouble(detectorNode["MaxThreshold"], 0);
                        //}                        

                        detectors.Add(result);
                    }
                    else
                        result = null;
                }

                return result;
            }
        }

        public Detector DetectorByIndex(string deviceName, int majorIndex, int minorIndex, DetectorType detectorType = DetectorType.Unknown)
        {
            Detector result = null;

            result = new Detector("", "") { DeviceName = deviceName, MajorIndex = majorIndex, MinorIndex = minorIndex };

                int searchResult = detectors.BinarySearch(result);
                if (searchResult >= 0)
                    result = detectors[searchResult];
                else
                {
                    XmlNode deviceNode = devicesDoc[deviceName];

                    XmlNode detectorNode = devicesDoc.LookupDetector(deviceNode, majorIndex, minorIndex, detectorType);

                    if (detectorNode != null)
                    {
                        result = new Detector(detectorNode["Name"].Value, "", detectorType) { MajorIndex = majorIndex, MinorIndex = minorIndex };

                        result.DeviceName = deviceName;
                        result.EventName = XmlNode.ParseString(detectorNode["EventName"], "");
                        result.Location = XmlNode.ParseString(detectorNode["Location"], "");
                        result.ImageMap = XmlNode.ParseString(detectorNode["Image"], "", "Map");
                        result.ImagePath = XmlNode.ParseString(detectorNode["Image"], "");
                        result.ImageX = XmlNode.ParseString(detectorNode["Image"], "", "Left");
                        result.ImageY = XmlNode.ParseString(detectorNode["Image"], "", "Top");
                        result.ImageWidth = XmlNode.ParseString(detectorNode["Image"], "", "Width");
                        result.ImageHeight = XmlNode.ParseString(detectorNode["Image"], "", "Height");
                        result.ImageRotation = XmlNode.ParseString(detectorNode["Image"], "", "Rotation");

                        detectors.Add(result);
                    }
                    else
                        result = null;
                }

                return result;
        }

        public bool Add(Detector detector)
        {
            bool result = false;

            int searchResult = detectors.BinarySearch(detector);

            if (searchResult < 0)
            {
                detectors.Insert(~searchResult, detector);

                result = true;
            }

            return result;
        }

        public DevicesDoc DevicesDoc
        {
            get { return devicesDoc; }
            set { devicesDoc = value; }
        }

        public IEnumerator<Detector> GetEnumerator()
        {
            foreach (Detector detector in detectors)
                yield return detector;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (Detector detector in detectors)
                yield return detector;
        }
    }
}
