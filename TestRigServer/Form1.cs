﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlackBoxEngine;
using eNerve.DataTypes;
using eNervePluginInterface;
using System.Threading;

namespace TestRigServer
{
    public partial class frmMain : Form
    {
        Service service = null;

        VideoServer.Service videoService = null;

        ZCP2Client firepanelClient;

        public frmMain()
        {
            InitializeComponent();             
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (service == null)
                service = new Service();

            if (!service.Start())
                MessageBox.Show("Invalid install, could not start");

            //if (firepanelClient == null)
            //    firepanelClient = new ZCP2Client("192.168.1.181", 4001);
            //else
            //    firepanelClient.Connect("192.168.1.181", 4001);            
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (service != null)
            {
                service.Stop();
                service.Dispose();
                service = null;
            }

            if (firepanelClient != null)
            {
                firepanelClient.Disconnect();
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            running = false;

            if (service != null)
            {
                service.Stop();
                service.Dispose();
                service = null;
            }

            if (firepanelClient != null)
            {
                firepanelClient.Disconnect();
                firepanelClient.Dispose();
            }
        }

        private void btnInjectTestAlarm_Click(object sender, EventArgs e)
        {
            if (service != null)
            {
                if (!string.IsNullOrWhiteSpace(txtAlarmName.Text))
                {
                    Alarm testAlarm = new Alarm(txtAlarmName.Text);
                    testAlarm.Alarm_Type = AlarmType.Test;
                    service.InjectAlarm(testAlarm);
                }
            }
        }

        private void btnMoxaSend_Click(object sender, EventArgs e)
        {
            if (firepanelClient != null)
            {
                firepanelClient.SendMessage(FirePanelZ3.EncodeInstruction(1, 1, "$"));
                //firepanelClient.SendMessage(new byte[] {(byte)ZCP2BlockMarkers.SOH, 49, 50, 51, (byte)ZCP2BlockMarkers.EOT});
            }
        }

        #region Test Alarms
        private List<IEventTrigger> triggers = new List<IEventTrigger> 
        { 
            new EventTrigger() { EventName = "Test1", DeviceName = "TestDevice1", DetectorName = "Detector1" }, 
            new EventTrigger() { EventName = "Test2", DeviceName = "TestDevice1", DetectorName = "Detector2" }, 
            new EventTrigger() { EventName = "Test3", DeviceName = "TestDevice2", DetectorName = "Detector1" }, 
            new EventTrigger() { EventName = "Test4", DeviceName = "TestDevice2", DetectorName = "Detector2" }, 
            new EventTrigger() { EventName = "Test5", DeviceName = "TestDevice3", DetectorName = "Detector1" }, 
            new EventTrigger() { EventName = "Test6", DeviceName = "TestDevice3", DetectorName = "Detector2" }, 
            new EventTrigger() { EventName = "Test7", DeviceName = "TestDevice4", DetectorName = "Detector1" }, 
            new EventTrigger() { EventName = "Test8", DeviceName = "TestDevice4", DetectorName = "Detector2" },
            new EventTrigger() { EventName = "Test1", DeviceName = "TestDevice11", DetectorName = "Detector1" }, 
            new EventTrigger() { EventName = "Test2", DeviceName = "TestDevice11", DetectorName = "Detector2" }, 
            new EventTrigger() { EventName = "Test3", DeviceName = "TestDevice12", DetectorName = "Detector1" }, 
            new EventTrigger() { EventName = "Test4", DeviceName = "TestDevice12", DetectorName = "Detector2" }, 
            new EventTrigger() { EventName = "Test5", DeviceName = "TestDevice13", DetectorName = "Detector1" }, 
            new EventTrigger() { EventName = "Test6", DeviceName = "TestDevice13", DetectorName = "Detector2" }, 
            new EventTrigger() { EventName = "Test7", DeviceName = "TestDevice14", DetectorName = "Detector1" }, 
            new EventTrigger() { EventName = "Test8", DeviceName = "TestDevice14", DetectorName = "Detector2" }
        };

        private bool running = false;
        private Random random = new Random();

        private void LetRip()
        {
            running = true;
            while(running)
            {
                if (triggers != null && triggers.Count > 0)
                {
                    IEventTrigger trigger = ObjectCopier.CloneXML<IEventTrigger>(triggers[random.Next(triggers.Count)]);
                    trigger.TriggerID = Guid.NewGuid();
                    trigger.TriggerDT = DateTime.Now;
                    service.InjectTrigger(trigger);

                    Thread.Sleep((int)(((double)60 / (double)(arpm * 40)) * 1000));
                }
                else
                    running = false;
            }
        }

        private int arpm;

        private void btnStartStopTestAlarms_Click(object sender, EventArgs e)
        {
            if(btnStartStopTestAlarms.Text == "Start")
            {
                using (DevicesDoc devicesDoc = new DevicesDoc("Devices"))
                {
                    devicesDoc.Load();
                    ClearTriggers();
                    triggers = devicesDoc.CreatEventTriggers();

                    arpm = trackBarARPM.Value;
                    Thread ripThread = new Thread(new ThreadStart(LetRip));
                    ripThread.Start();
                    btnStartStopTestAlarms.Text = "Stop";
                }
            }
            else
            {
                running = false;
                btnStartStopTestAlarms.Text = "Start";
                ClearTriggers();
            }
        }

        private void ClearTriggers()
        {
            if (triggers != null && triggers.Count > 0)
                triggers.Clear();
        }
        #endregion Test Alarms

        private void trackBarARPM_Scroll(object sender, EventArgs e)
        {
            arpm = trackBarARPM.Value;
        }

        private void btnTestStatusMessage_Click(object sender, EventArgs e)
        {
            service.InjectStatusMessage(new StatusMessage() { StatusID = Guid.NewGuid(), Message = "Show me something cool", Animation = true, AutoRemove = TimeSpan.FromSeconds(5), MessageTimeStamp = DateTime.Now });
        }

        private void btnInjectTrigger1_Click(object sender, EventArgs e)
        {
            if (service != null)
            {
                string[] timeStrings = txtTriggerTimes.Text.Split(new char[] {',', ';'});

                List<DateTime> times = new List<DateTime>();

                foreach (string s in timeStrings)
                {
                    DateTime tempDT;
                    if (DateTime.TryParse(s, out tempDT))
                        times.Add(tempDT);
                }

                foreach (DateTime t in times)
                {
                    service.InjectTrigger(new EventTrigger()
                        {
                            TriggerID = Guid.NewGuid(),
                            DeviceName = "Adam1",
                            DetectorName = "Input4",
                            DetectorType = eNervePluginInterface.DetectorType.Digital,
                            State = true,
                            TriggerDT = t
                        });
                }
            }
        }

        private void btnInjectTrigger2_Click(object sender, EventArgs e)
        {
            if (service != null)
            {
                service.InjectTrigger(new EventTrigger()
                    {
                        TriggerID = Guid.NewGuid(),
                        DeviceName = "Adam1",
                        DetectorName = "Input7",
                        DetectorType = eNervePluginInterface.DetectorType.Digital,
                        State = true,
                        TriggerDT = DateTime.Parse("2017/04/20 12:00:01")
                    });
            }
        }

        private void btnTimeSpan_Click(object sender, EventArgs e)
        {
            try
            {
                MessageBox.Show(string.Format("{0} ms", eThele.Essentials.TimeSpanParser.Parse(txtTriggerTimes.Text).ToString()));
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void VideServerStart(object sender, EventArgs e)
        {
            videoService = new VideoServer.Service();
            videoService.Start();
        }

        private void VideoServerStop(object sender, EventArgs e)
        {
            if (videoService != null)
            {
                videoService.Stop();
                videoService.Dispose();
                videoService = null;
            }
        }
    }
}
