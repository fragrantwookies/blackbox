﻿using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestRigServer
{
    class testplugin : INerveEventPlugin
    {
        public event EventHandler<PluginTriggerEventArgs> Trigger;

        INerveHostPlugin host;
        public INerveHostPlugin Host
        {
            get
            {
                return host;
            }
            set
            {
                host = value;
            }
        }

        public string Name
        {
            get
            {
                return "Lombaard se fokken plugin";
            }
            set
            {
            }
        }

        public string Description
        {
            get { return "Some sort of description"; }
        }

        public string Author
        {
            get { throw new NotImplementedException(); }
        }

        public string Version
        {
            get { throw new NotImplementedException(); }
        }
        public bool Equals(INervePlugin other)
        {
            return (Name == other.Name && FileName == other.FileName);
        }

        public string FileName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        public CustomProperties Properties
        {
            get { throw new NotImplementedException(); }
            set { }
        }
        
        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                return null;
            }
        }

        public void Start()
        {
            
        }

        private void DoYerThing()
        {
            while (true)
            {
                if (Trigger != null)
                    Trigger(this, new eNervePluginInterface.PluginTriggerEventArgs(new LolliTrigger() { EventName="Whatsup" }));

                Thread.Sleep(1000);
            }
        }

        event EventHandler<PluginErrorEventArgs> INervePlugin.Error
        {
            add { throw new NotImplementedException(); }
            remove { throw new NotImplementedException(); }
        }


        public string[] EventNames
        {
            get { return new string[0]; }
        }

        event EventHandler<PluginTriggerEventArgs> INerveEventPlugin.Trigger
        {
            add { throw new NotImplementedException(); }
            remove { throw new NotImplementedException(); }
        }

        string[] INerveEventPlugin.EventNames
        {
            get { throw new NotImplementedException(); }
        }

        INerveHostPlugin INervePlugin.Host
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string INervePlugin.Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        string INervePlugin.Description
        {
            get { throw new NotImplementedException(); }
        }

        string INervePlugin.Author
        {
            get { throw new NotImplementedException(); }
        }

        string INervePlugin.Version
        {
            get { throw new NotImplementedException(); }
        }

        string INervePlugin.FileName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        CustomProperties INervePlugin.Properties
        {
            get { throw new NotImplementedException(); }
            //set { }
        }

        List<CustomPropertyDefinition> INervePlugin.CustomPropertyDefinitions
        {
            get
            {
                return null;
            }
        }

        void INervePlugin.Start()
        {
            throw new NotImplementedException();
        }

        public bool Online
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool Enabled
        {
            get;

            set;
        }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }

        public event EventHandler DeviceOnline;

        public event EventHandler DeviceOffline;
    }

    public class LolliTrigger : IEventTrigger
    {
        public Guid EventID
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Guid TriggerID
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string DeviceName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string DetectorName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string EventName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string Location
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string MapName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DetectorType DetectorType
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int EventCount
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public double[] EventValues
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string EventValueCSV
        {
            get { throw new NotImplementedException(); }
        }

        public string Data
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public DateTime TriggerDT
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int Compare(IEventTrigger x, IEventTrigger y)
        {
            throw new NotImplementedException();
        }

        public int CompareTo(IEventTrigger other)
        {
            throw new NotImplementedException();
        }

        public bool Equals(IEventTrigger other)
        {
            throw new NotImplementedException();
        }

        public bool Equals(Guid other)
        {
            throw new NotImplementedException();
        }


        public bool State
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public int DetectorGroupIndex
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public int DetectorIndex
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public double EventValue
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
