﻿namespace TestRigServer
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            if (service != null)
                service.Dispose();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabMain = new System.Windows.Forms.TabPage();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.tabTestAlarms = new System.Windows.Forms.TabPage();
            this.btnTimeSpan = new System.Windows.Forms.Button();
            this.txtTriggerTimes = new System.Windows.Forms.TextBox();
            this.btnInjectTrigger2 = new System.Windows.Forms.Button();
            this.btnInjectTrigger1 = new System.Windows.Forms.Button();
            this.btnTestStatusMessage = new System.Windows.Forms.Button();
            this.trackBarARPM = new System.Windows.Forms.TrackBar();
            this.btnStartStopTestAlarms = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAlarmName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnInjectTestAlarm = new System.Windows.Forms.Button();
            this.tabFirePanel = new System.Windows.Forms.TabPage();
            this.btnMoxaSend = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabTestAlarms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarARPM)).BeginInit();
            this.tabFirePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabMain);
            this.tabControl1.Controls.Add(this.tabTestAlarms);
            this.tabControl1.Controls.Add(this.tabFirePanel);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(593, 411);
            this.tabControl1.TabIndex = 6;
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.button1);
            this.tabMain.Controls.Add(this.button2);
            this.tabMain.Controls.Add(this.label3);
            this.tabMain.Controls.Add(this.btnStop);
            this.tabMain.Controls.Add(this.btnStart);
            this.tabMain.Location = new System.Drawing.Point(4, 22);
            this.tabMain.Name = "tabMain";
            this.tabMain.Padding = new System.Windows.Forms.Padding(3);
            this.tabMain.Size = new System.Drawing.Size(585, 385);
            this.tabMain.TabIndex = 0;
            this.tabMain.Text = "Main";
            this.tabMain.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(102, 19);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 3;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(21, 19);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tabTestAlarms
            // 
            this.tabTestAlarms.Controls.Add(this.btnTimeSpan);
            this.tabTestAlarms.Controls.Add(this.txtTriggerTimes);
            this.tabTestAlarms.Controls.Add(this.btnInjectTrigger2);
            this.tabTestAlarms.Controls.Add(this.btnInjectTrigger1);
            this.tabTestAlarms.Controls.Add(this.btnTestStatusMessage);
            this.tabTestAlarms.Controls.Add(this.trackBarARPM);
            this.tabTestAlarms.Controls.Add(this.btnStartStopTestAlarms);
            this.tabTestAlarms.Controls.Add(this.label2);
            this.tabTestAlarms.Controls.Add(this.txtAlarmName);
            this.tabTestAlarms.Controls.Add(this.label1);
            this.tabTestAlarms.Controls.Add(this.btnInjectTestAlarm);
            this.tabTestAlarms.Location = new System.Drawing.Point(4, 22);
            this.tabTestAlarms.Name = "tabTestAlarms";
            this.tabTestAlarms.Padding = new System.Windows.Forms.Padding(3);
            this.tabTestAlarms.Size = new System.Drawing.Size(585, 385);
            this.tabTestAlarms.TabIndex = 1;
            this.tabTestAlarms.Text = "Test alarms";
            this.tabTestAlarms.UseVisualStyleBackColor = true;
            // 
            // btnTimeSpan
            // 
            this.btnTimeSpan.Location = new System.Drawing.Point(504, 335);
            this.btnTimeSpan.Name = "btnTimeSpan";
            this.btnTimeSpan.Size = new System.Drawing.Size(75, 23);
            this.btnTimeSpan.TabIndex = 16;
            this.btnTimeSpan.Text = "Timespan";
            this.btnTimeSpan.UseVisualStyleBackColor = true;
            this.btnTimeSpan.Click += new System.EventHandler(this.btnTimeSpan_Click);
            // 
            // txtTriggerTimes
            // 
            this.txtTriggerTimes.Location = new System.Drawing.Point(9, 300);
            this.txtTriggerTimes.Name = "txtTriggerTimes";
            this.txtTriggerTimes.Size = new System.Drawing.Size(570, 20);
            this.txtTriggerTimes.TabIndex = 15;
            // 
            // btnInjectTrigger2
            // 
            this.btnInjectTrigger2.Location = new System.Drawing.Point(280, 335);
            this.btnInjectTrigger2.Name = "btnInjectTrigger2";
            this.btnInjectTrigger2.Size = new System.Drawing.Size(75, 23);
            this.btnInjectTrigger2.TabIndex = 14;
            this.btnInjectTrigger2.Text = "Trigger 2";
            this.btnInjectTrigger2.UseVisualStyleBackColor = true;
            this.btnInjectTrigger2.Click += new System.EventHandler(this.btnInjectTrigger2_Click);
            // 
            // btnInjectTrigger1
            // 
            this.btnInjectTrigger1.Location = new System.Drawing.Point(50, 335);
            this.btnInjectTrigger1.Name = "btnInjectTrigger1";
            this.btnInjectTrigger1.Size = new System.Drawing.Size(75, 23);
            this.btnInjectTrigger1.TabIndex = 13;
            this.btnInjectTrigger1.Text = "Trigger 1";
            this.btnInjectTrigger1.UseVisualStyleBackColor = true;
            this.btnInjectTrigger1.Click += new System.EventHandler(this.btnInjectTrigger1_Click);
            // 
            // btnTestStatusMessage
            // 
            this.btnTestStatusMessage.Location = new System.Drawing.Point(361, 8);
            this.btnTestStatusMessage.Name = "btnTestStatusMessage";
            this.btnTestStatusMessage.Size = new System.Drawing.Size(75, 23);
            this.btnTestStatusMessage.TabIndex = 12;
            this.btnTestStatusMessage.Text = "Status Msg";
            this.btnTestStatusMessage.UseVisualStyleBackColor = true;
            this.btnTestStatusMessage.Click += new System.EventHandler(this.btnTestStatusMessage_Click);
            // 
            // trackBarARPM
            // 
            this.trackBarARPM.Location = new System.Drawing.Point(50, 77);
            this.trackBarARPM.Maximum = 12;
            this.trackBarARPM.Minimum = 1;
            this.trackBarARPM.Name = "trackBarARPM";
            this.trackBarARPM.Size = new System.Drawing.Size(224, 45);
            this.trackBarARPM.TabIndex = 11;
            this.trackBarARPM.Value = 1;
            this.trackBarARPM.Scroll += new System.EventHandler(this.trackBarARPM_Scroll);
            // 
            // btnStartStopTestAlarms
            // 
            this.btnStartStopTestAlarms.Location = new System.Drawing.Point(280, 77);
            this.btnStartStopTestAlarms.Name = "btnStartStopTestAlarms";
            this.btnStartStopTestAlarms.Size = new System.Drawing.Size(75, 23);
            this.btnStartStopTestAlarms.TabIndex = 10;
            this.btnStartStopTestAlarms.Text = "Start";
            this.btnStartStopTestAlarms.UseVisualStyleBackColor = true;
            this.btnStartStopTestAlarms.Click += new System.EventHandler(this.btnStartStopTestAlarms_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "ARPM";
            // 
            // txtAlarmName
            // 
            this.txtAlarmName.Location = new System.Drawing.Point(100, 10);
            this.txtAlarmName.Name = "txtAlarmName";
            this.txtAlarmName.Size = new System.Drawing.Size(147, 20);
            this.txtAlarmName.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Test Alarm Name";
            // 
            // btnInjectTestAlarm
            // 
            this.btnInjectTestAlarm.Location = new System.Drawing.Point(253, 8);
            this.btnInjectTestAlarm.Name = "btnInjectTestAlarm";
            this.btnInjectTestAlarm.Size = new System.Drawing.Size(102, 23);
            this.btnInjectTestAlarm.TabIndex = 5;
            this.btnInjectTestAlarm.Text = "Inject Test Alarm";
            this.btnInjectTestAlarm.UseVisualStyleBackColor = true;
            this.btnInjectTestAlarm.Click += new System.EventHandler(this.btnInjectTestAlarm_Click);
            // 
            // tabFirePanel
            // 
            this.tabFirePanel.Controls.Add(this.btnMoxaSend);
            this.tabFirePanel.Location = new System.Drawing.Point(4, 22);
            this.tabFirePanel.Name = "tabFirePanel";
            this.tabFirePanel.Padding = new System.Windows.Forms.Padding(3);
            this.tabFirePanel.Size = new System.Drawing.Size(585, 385);
            this.tabFirePanel.TabIndex = 2;
            this.tabFirePanel.Text = "Fire Panel";
            this.tabFirePanel.UseVisualStyleBackColor = true;
            // 
            // btnMoxaSend
            // 
            this.btnMoxaSend.Location = new System.Drawing.Point(17, 15);
            this.btnMoxaSend.Name = "btnMoxaSend";
            this.btnMoxaSend.Size = new System.Drawing.Size(99, 23);
            this.btnMoxaSend.TabIndex = 6;
            this.btnMoxaSend.Text = "Send Moxa Msg";
            this.btnMoxaSend.UseVisualStyleBackColor = true;
            this.btnMoxaSend.Click += new System.EventHandler(this.btnMoxaSend_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Video Server";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(102, 118);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Stop";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.VideoServerStop);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(21, 118);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "Start";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.VideServerStart);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 435);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmMain";
            this.Text = "Black Box Server Simulator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.tabMain.PerformLayout();
            this.tabTestAlarms.ResumeLayout(false);
            this.tabTestAlarms.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarARPM)).EndInit();
            this.tabFirePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabMain;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TabPage tabTestAlarms;
        private System.Windows.Forms.TextBox txtAlarmName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnInjectTestAlarm;
        private System.Windows.Forms.TabPage tabFirePanel;
        private System.Windows.Forms.Button btnMoxaSend;
        private System.Windows.Forms.Button btnStartStopTestAlarms;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TrackBar trackBarARPM;
        private System.Windows.Forms.Button btnTestStatusMessage;
        private System.Windows.Forms.Button btnInjectTrigger2;
        private System.Windows.Forms.Button btnInjectTrigger1;
        private System.Windows.Forms.TextBox txtTriggerTimes;
        private System.Windows.Forms.Button btnTimeSpan;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
    }
}

