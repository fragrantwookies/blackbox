﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CubePlugin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubePlugin.Tests
{
    [TestClass()]
    public class CubeWebServiceTests
    {
        [TestMethod()]
        public void EchoWithGetTest()
        {
            CubeWebService service = new CubeWebService();
            try
            {
                service.EchoWithGet("#$%^#$%N BAD STRING");
                Assert.IsNotNull(service);
            }
            catch (Exception ex)
            {
                Assert.Fail("EchoWithGet failed with error: " + ex);
            }

        }

        [TestMethod()]
        public void EchoWithPostTest()
        {
            CubeWebService service = new CubeWebService();
            Assert.IsNotNull(service);
        }

        [TestMethod()]
        public void AlarmMessageEventArgsTest()
        {
            var alarmMessageEventArgs = new AlarmMessageEventArgs("72595,862877033382936ALM,1,0,17/05/09-18:28:26");
            Assert.AreEqual(new DateTime(2017, 5, 9, 20, 28, 26, DateTimeKind.Local), alarmMessageEventArgs.TimeStamp);
            try
            {
                alarmMessageEventArgs = new AlarmMessageEventArgs("54,862877033382936LOC,-25.872843,28.183784,");
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(FormatException));
            }
        }

        [TestMethod()]
        public void LocationMessageEventArgsTest()
        {
            var LocationMessageEventArgs = new LocationMessageEventArgs("54,862877033382936LOC,-25.872843,28.183784,");
            Assert.AreEqual(-25.872843, LocationMessageEventArgs.Location.Lattitude);
            try
            {
                LocationMessageEventArgs = new LocationMessageEventArgs("72595,862877033382936ALM,1,0,17/05/09-08:28:26");
                Assert.Fail();
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(FormatException));
            }
        }
    }
}