﻿using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EmailPlugin
{
    public class EmailPlugin : INerveActionPlugin, IDisposable
    {     
        public event EventHandler<PluginErrorEventArgs> Error;     
        public event EventHandler DeviceOnline;     
        public event EventHandler DeviceOffline;

        #region Dispose
        ~EmailPlugin()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {

        }
        #endregion
        
        public Type GetActionType(string _actionName = "")
        {
            return typeof(EmailAction);
        }

        public IEnumerable<Type> GetActionTypes()
        {
            return new Type[] { typeof(EmailAction) };
        }

        public bool Enabled { get; set; }

        public bool Online { get; set; }

        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public string Description
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description; }
        }

        public string Author
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company; }
        }

        public string Version
        {
            get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
        }

        private string fileName;

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        private CustomProperties properties;
        public CustomProperties Properties
        {
            get 
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
        }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get 
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                result.Add(new CustomPropertyDefinition("FromAddress", "From Address", "From Address", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("SmtpServer", "SMTP Server", "SMTP Server (smtp.gmail.com for gmail)", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("SmtpPort", "SMTP Port", "SMTP Port (587 for gmail)", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("UserName", "User Name", "User Name", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Password", "Password", "Password", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("EnableSsl", "Enable SSL", "Enable SSL", typeof(CustomProperty)));
                return result;
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        public void Start()
        {
            Online = true;// PingHost(this.Properties["FromAddress"]);

            if (Online && DeviceOnline != null)
                DeviceOnline(this, new EventArgs());
        }

        public bool PerformAction(string eventName, IEnumerable<IEventTrigger> triggers, IAction action)
        {
            bool result = false;

            System.Net.Mail.MailMessage msg = null;
            System.Net.Mail.SmtpClient smtp = null;

            try
            {
                string fromAddress = this.Properties["FromAddress"];
                string smtpServer = this.Properties["SmtpServer"];
                int smtpPort;
                if (!int.TryParse(this.Properties["SmtpPort"], out smtpPort))
                    smtpPort = 587;
                string userName = this.Properties["UserName"];
                string Password = this.Properties["Password"];
                bool enableSsl;
                if (!bool.TryParse(this.Properties["EnableSsl"], out enableSsl))
                    enableSsl = true;

                msg = new System.Net.Mail.MailMessage();

                EmailAction emailAction = action as EmailAction;

                string[] toAddresses = emailAction.ToAddress.Split(new char[] { ',', ';' });

                foreach (string address in toAddresses)
                    msg.To.Add(address.Trim());

                msg.Subject = string.IsNullOrWhiteSpace(emailAction.Subject) ? "eNerve alarm" : emailAction.Subject;
                msg.From = new System.Net.Mail.MailAddress(fromAddress);
                string plainBody = string.Format("Alarm: {0}\n", eventName);
                msg.Body = plainBody;

                string triggermsgs = "";
                foreach (IEventTrigger trigger in triggers)
                    triggermsgs += string.Format("Device: <i>{0}</i><br>Detector: <i>{1}</i><br>Date: <i>{2}</i><br>Value: <i>{3}</i>", trigger.DeviceName, trigger.DetectorName, trigger.TriggerDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), trigger.DetectorType == DetectorType.Analog ? trigger.EventValue.ToString() : trigger.State.ToString());
                string niceBody = string.Format("<h1>{0}</h1>{1}", eventName, triggermsgs);

                ContentType mimeType = new System.Net.Mime.ContentType("text/html");
                AlternateView alternate = AlternateView.CreateAlternateViewFromString(niceBody, mimeType);
                msg.AlternateViews.Add(alternate);

                smtp = new System.Net.Mail.SmtpClient(smtpServer, smtpPort);
                smtp.EnableSsl = enableSsl;
                smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new NetworkCredential(userName, Password);
                smtp.Send(msg);

                result = true;
            }
            catch(Exception ex)
            {
                if (Error != null)
                    Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), ex.Message, ex));

                result = false;
            }
            finally
            {
                if (msg != null)
                    msg.Dispose();

                if (smtp != null)
                    smtp.Dispose();
            }

            return result;
        }

        public static bool PingHost(string nameOrAddress)
        {
            bool pingable = false;
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(nameOrAddress);
                pingable = reply.Status == IPStatus.Success;
            }
            catch (PingException)
            {
                // Discard PingExceptions and return false;
            }
            return pingable;
        }
    }
}
