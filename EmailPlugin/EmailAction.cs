﻿using eNervePluginInterface;
using eNervePluginInterface.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailPlugin
{
    public class EmailAction : PluginAction
    {
        public EmailAction() : base() { }

        public EmailAction(string _name, bool _value, string _pluginName, int _actionOrder = 0)
            : base(_name, _value, _pluginName, _actionOrder) {}

        [DisplayInPluginActionPropertiesList]
        public string ToAddress { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string Subject { get; set; }
    }
}
