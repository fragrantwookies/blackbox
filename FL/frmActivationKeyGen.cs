﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FL
{
    public partial class frmActivationKeyGen : Form
    {
        public frmActivationKeyGen()
        {
            InitializeComponent();
        }

        private void btnGenerateKey_Click(object sender, EventArgs e)
        {
            if (txtProductKey.Text != "")
            {
                txtActivationKey.Text = Encrypt(txtProductKey.Text);
            }
        }

        private static string Encrypt(string val)
        {
            string result = "";

            try
            {
                byte[] key;
                byte[] iv;
                byte[] salt = new byte[8] { 0x23, 0x43, 0x29, 0x3F, 0xDA, 0xBD, 0x5B, 0xED };

                //using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
                //{
                //    // Fill the array with a random value.
                //    rngCsp.GetBytes(salt);
                //}

                using (Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes("Live Long", salt))
                {
                    key = pdb.GetBytes(16);
                }

                using (Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes("And Prosper", salt))
                {
                    iv = pdb.GetBytes(16);
                }

                byte[] code = EncryptStringToBytes_Aes(val, key, iv);

                byte[] resultarray = new byte[code.Length + salt.Length];
                code.CopyTo(resultarray, 0);
                salt.CopyTo(resultarray, code.Length);

                string fullCode = BitConverter.ToString(resultarray).Replace("-", "");
                
                for(int i = 0; i < fullCode.Length; i++)
                    if(i % 5 == 0)
                        result += fullCode[i];
            }
            catch
            {
                result = "";
            }

            return result;
        }

        private static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an AesCryptoServiceProvider object 
            // with the specified key and IV. 
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);


                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        }
    }
}
