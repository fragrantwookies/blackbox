﻿using System;
using System.Runtime.InteropServices;
using System.Timers;
using System.Windows;
using System.Windows.Interop;


namespace VideoHost
{
    public class CameraWindow : HwndHost
    {
        public bool Fullscreen { get; set; }
        public EventHandler MouseActivate;
        public EventHandler MouseDoubleClick;
        private byte clicks = 0;
        public new EventHandler MouseUp;
        private bool mouseDoubleClicking = false;
        private HandleRef parentHandle;
        private IntPtr hwndControl;
        public IntPtr hwndHost;
        Procedure procedure;

        private Timer doubleClickTimer;

        int hostHeight, hostWidth;

        const int WM_SIZE = 0x0005, CS_DBLCLKS = 0x0008, WM_PAINT = 0x000F, WM_LBUTTONDOWN = 0x0201, WM_LBUTTONUP = 0x0202, WS_VSCROLL = 0x00200000, WS_BORDER = 0x00800000, LBS_NOTIFY = 1,
            WM_DROPFILES = 0x0233, DL_DROPPED = 0x0487, WM_NCHITTEST = 0x84, WM_LBUTTONDBLCLK = 0x0203, WM_NCLBUTTONDBLCLK = 0x00A3, WS_EX_ACCEPTFILES = 0x00000010, WM_COMMAND = 0x00000111,
            WM_MOUSEWHEEL = 0x020a;


        internal const int
          WS_CHILD = 0x40000000,
          HOST_ID = 0x00000002,
          WS_VISIBLE = 0x10000000;

        delegate IntPtr Procedure(IntPtr handle, uint message, IntPtr wparam, IntPtr lparam);

        [StructLayout(LayoutKind.Sequential)]
        struct Paint
        {
            public IntPtr Context;
            public bool Erase;
            public Rect Area;
            public bool Restore;
            public bool Update;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] Reserved;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct WindowClass
        {
            public uint Style;
            public IntPtr Callback;
            public int ClassExtra;
            public int WindowExtra;
            public IntPtr Instance;
            public IntPtr Icon;
            public IntPtr Cursor;
            public IntPtr Background;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string Menu;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string Class;
        }

        public CameraWindow() {
            hostHeight = (int)Height;
            hostWidth = (int)Width;
        }

        public CameraWindow(Size size)
        {
            hostHeight = (int)size.Height;
            hostWidth = (int)size.Width;
            MessageHook += ControlMsgFilter;
            
            //Child = new CameraControl(size);
            //Child.DragDrop += OnMouseUp;
            //Child = new System.Windows.Forms.Control(Name, 0, 0, hostWidth, hostHeight);
            //Child.AllowDrop = true;

            /*doubleClickTimer = new Timer(300);
            doubleClickTimer.Enabled = false;
            doubleClickTimer.Elapsed += DoubleClickTimer_Elapsed;*/
            
        }

        private void DoubleClickTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            doubleClickTimer.Stop();
            mouseDoubleClicking = false;
            if (clicks == 1)
                OnMouseActivate();
            else if (clicks == 2)
                OnMouseDoubleClick();
        }

        protected override HandleRef BuildWindowCore(HandleRef parent)
        {
            hwndHost = IntPtr.Zero;

            var callback = Marshal.GetFunctionPointerForDelegate(procedure = WndProc);
            var width = Convert.ToInt32(ActualWidth);
            var height = Convert.ToInt32(ActualHeight);
            var cursor = LoadCursor(IntPtr.Zero, 32512);
            var menu = string.Empty;
            var background = new IntPtr(1);
            var zero = IntPtr.Zero;
            var extra = 0;
            //var extended = 0u;
            //var window = 0x50000000u;
            //var point = 0;

            parentHandle = parent;

            var wnd = new WindowClass
            {
                Style = 3u,
                Callback = callback,
                ClassExtra = extra,
                WindowExtra = extra,
                Instance = zero,
                Icon = zero,
                Cursor = cursor,
                Background = background,
                Menu = menu,
                Class = Name
            };

            hwndHost = CreateWindowEx(0, "static", "",
                                      WS_CHILD | LBS_NOTIFY,// | WS_VISIBLE,
                                      0, 0,
                                      hostWidth, hostHeight,
                                      parent.Handle,
                                      (IntPtr)HOST_ID,
                                      zero,
                                      zero);

            hwndControl = CreateWindowEx(0, "listbox", Name,
                WS_CHILD | WS_VISIBLE,// | WS_BORDER,
                0, 0,
                hostWidth * 4, hostHeight * 4, hwndHost,
                (IntPtr)1, zero, zero);
            
            return new HandleRef(this, hwndHost);
        }

        public void Clear()
        {
            if ((hwndHost != IntPtr.Zero) && (parentHandle.Handle != IntPtr.Zero))
            {
                hwndHost = CreateWindowEx(0, "static", "",
                                          WS_CHILD | LBS_NOTIFY,// | WS_VISIBLE,
                                          0, 0,
                                          hostWidth, hostHeight,
                                          parentHandle.Handle,
                                          (IntPtr)HOST_ID,
                                          IntPtr.Zero,
                                          IntPtr.Zero);

                hwndControl = CreateWindowEx(0, "listbox", Name,
                    WS_CHILD | WS_VISIBLE,// | WS_BORDER,
                    0, 0,
                    hostWidth * 4, hostHeight * 4, hwndHost,
                    (IntPtr)1, IntPtr.Zero, IntPtr.Zero);
            }
        }
        private IntPtr ControlMsgFilter(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            handled = false;
            if (msg == WM_COMMAND)
            {
                switch ((uint)wParam.ToInt32() >> 16 & 0xFFFF) //extract the HIWORD
                {
                    case 4:// Button Down.
                        //Get the item text and display it
                        //selectedItem = SendMessage(listControl.hwndListBox, LB_GETCURSEL, IntPtr.Zero, IntPtr.Zero);
                        //textLength = SendMessage(listControl.hwndListBox, LB_GETTEXTLEN, IntPtr.Zero, IntPtr.Zero);
                        //StringBuilder itemText = new StringBuilder();
                        //SendMessage(hwndListBox, LB_GETTEXT, selectedItem, itemText);
                        //selectedText.Text = itemText.ToString();
                        handled = true;
                        break;
                    case 5://Button up
                        //handled = true;
                        break;
                    default:
                        break;
                }
            }
            else if (msg == 0x21)// Mouse Activate as per https://www.winehq.org/pipermail/wine-patches/2011-November/108980.html
            {
                //System.Diagnostics.Debug.Write("Mouse Activate");
                if (mouseDoubleClicking)
                {
                    //OnMouseDoubleClick();
                    //handled = false;
                    //clicks = 2;
                }
                else
                {
                    //OnMouseActivate();
                    //clicks = 1;
                    //handled = false;
                    //mouseDoubleClicking = true;
                    //doubleClickTimer.Start();
                }
            }
            else if (msg == 0x022c)
            {
                handled = true;
            }
            return IntPtr.Zero;
        }

        private void OnMouseActivate()
        {
            if (MouseActivate != null)
            {
                MouseActivate(this, new EventArgs());
            }
        }

        private void OnMouseDoubleClick()
        {
            mouseDoubleClicking = false;
            doubleClickTimer.Stop();
            if (MouseDoubleClick != null)
            {
                MouseDoubleClick(this, new EventArgs());
            }
        }

        private IntPtr CreateWindow()
        {
            return CreateWindowEx(0, "listbox", Name,
                                      WS_CHILD | WS_VISIBLE | WS_BORDER,
                                      0, 0,
                                      0, 0,
                                      parentHandle.Handle,
                                      (IntPtr)HOST_ID,
                                      IntPtr.Zero,
                                      IntPtr.Zero);
        }

        protected override void DestroyWindowCore(HandleRef handle)
        {
            if (doubleClickTimer != null)
            {
                doubleClickTimer.Elapsed -= DoubleClickTimer_Elapsed;
                doubleClickTimer.Dispose();
                doubleClickTimer = null;
            }
            DestroyWindow(handle.Handle);
        }
        protected override IntPtr WndProc(IntPtr handle, int message, IntPtr wparam, IntPtr lparam, ref bool handled)
        {
            handled = false;
            {
                try
                {
                    switch (message)
                    {
                        case WM_NCHITTEST:
                        //    handled = true;
                            break;
                        case WM_PAINT:
                            Paint paint;
                            BeginPaint(handle, out paint);
                            paint.Area = new Rect(new Size(ActualWidth, ActualHeight));
                            EndPaint(handle, ref paint);
                            handled = true;
                            return IntPtr.Zero;
                        case WM_SIZE:
                            handled = true;
                            break;
                        case WM_MOUSEWHEEL:
                            break;
                        case WM_LBUTTONDOWN:
                            handled = true;
                            break;
                        case WM_LBUTTONUP:
                            handled = true;
                            break;
                        case WM_LBUTTONDBLCLK:
                            case WM_NCLBUTTONDBLCLK:
                            handled = true;
                            break;
                        case 32://SETCURSOR //TODO fix bug where dropping outside of camerawindows still populates camera.
                            //SetCursor((int)LoadCursor(IntPtr.Zero, 32649));
                            OnMouseUp();
                            handled = true;
                            break;
                        case 512: case 160://MOUSEMOVE
                            handled = true;
                            break;
                        case WM_COMMAND:
                            System.Diagnostics.Debug.Write(string.Format("Message: {0}. WPARAM: {1}. LPARAM: {2}", message, wparam, lparam));
                            break;
                        default:
                            //System.Diagnostics.Debug.Write(message);
                            System.Diagnostics.Debug.Write("Message: " + message);
                            handled = false;
                            return IntPtr.Zero;
                            //break;

                    }
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.Write(e.Message);
                }
            }

            return base.WndProc(handle, message, wparam, lparam, ref handled);
        }

        private void OnMouseUp()
        {
            if (MouseUp != null)
            {
                MouseUp(this, null);
            }
        }

        static IntPtr WndProc(IntPtr handle, uint message, IntPtr wparam, IntPtr lparam)
        {
            return DefWindowProc(handle, message, wparam, lparam);
        }


        #region imported methods
        [DllImport("user32.dll")]
        static extern IntPtr BeginPaint
            (IntPtr handle,
            out Paint paint);

        [DllImport("user32.dll", EntryPoint = "CreateWindowEx", CharSet = CharSet.Unicode)]
        internal static extern IntPtr CreateWindowEx(int dwExStyle, string lpszClassName, string lpszWindowName, int style,
                                              int x, int y, int width, int height, IntPtr hwndParent, IntPtr hMenu, IntPtr hInst,
                                              [MarshalAs(UnmanagedType.AsAny)] object pvParam);


        [DllImport("user32.dll")]
        static extern IntPtr DefWindowProc(IntPtr handle, uint message, IntPtr wparam, IntPtr lparam);

        [DllImport("user32.dll")]
        static extern bool DestroyWindow
            (IntPtr handle);

        [DllImport("user32.dll")]
        static extern bool EndPaint
            (IntPtr handle,
            [In] ref Paint paint);

        [DllImport("user32.dll")]
        static extern IntPtr LoadCursor
            (IntPtr instance,
            int name);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        [DllImport("user32.dll")]
        public static extern int SetCursor(int hCursor);

        [DllImport("user32.dll")]
        static extern ushort RegisterClass
            ([In]
            ref WindowClass register);

        #endregion
    }
}
