﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace VideoHost
{
    public class PreviewToolTip : ToolTip
    {
        public bool Enabled { get; set; }
        protected override void OnTemplateChanged(ControlTemplate oldTemplate, ControlTemplate newTemplate)
        {
            if (newTemplate != null)
            {
                Visibility = Visibility.Collapsed;
                IsOpen = true;
                Popup popup = GetPopupFromVisualChild(this);
                if (popup != null) popup.AllowsTransparency = false;
                IsOpen = false;
                Visibility = Visibility.Visible;
            }
        }

        private static Popup GetPopupFromVisualChild(Visual child)
        {
            Visual parent = child;
            FrameworkElement visualRoot = null;
            while (parent != null)
            {
                visualRoot = parent as FrameworkElement;
                parent = VisualTreeHelper.GetParent(parent) as Visual;
            }

            Popup popup = null;
            if (visualRoot != null)
            {
                popup = visualRoot.Parent as Popup;
            }

            return popup;
        }
    }
}