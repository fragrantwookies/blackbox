﻿using System;

namespace VideoHost
{
    public class CameraClosedEventArgs : EventArgs
    {
        public string Position { get; set; }
        public CameraClosedEventArgs(string videoControlName)
        {
            if (videoControlName.Contains("VideoControl"))
                Position = videoControlName.Substring(0, videoControlName.IndexOf("VideoControl"));
            else Position = videoControlName;
        }
    }
}
