﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;

using BlackBoxEngine;
using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;

namespace VideoHost
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable
    {
        internal const int
          LBN_SELCHANGE = 0x00000001,
          WM_COMMAND = 0x00000111,
          LB_GETCURSEL = 0x00000188,
          LB_GETTEXTLEN = 0x0000018A,
          LB_ADDSTRING = 0x00000180,
          LB_GETTEXT = 0x00000189,
          LB_DELETESTRING = 0x00000182,
          LB_GETCOUNT = 0x0000018B;


        //int selectedItem;
        //IntPtr hwndViewGrid;
        //System.Windows.Application app;
        //Window myWindow;
        //int itemCount;

        private CameraLayouts cameraLayout;
        private GridLength cameraListWidth;
        private Dictionary<string,string> cameraPositions;
        private bool cameraPositionsPopulated = false;
        private Point clickPoint;
        private bool displaysReady = false;
        //private bool doubleClicking = false;
        private System.Timers.Timer doubleClickTimer;

        private GridLength gridControlHeight = new GridLength(20);
        private GridLength gridLengthStar = new GridLength(1, GridUnitType.Star);
        private GridLength gridLengthZero = new GridLength(0);
        private GridLength gridPlaybackHeight = new GridLength(40);

        private ObservableCollection<DisplayCamera> cameras;
        private byte clicks = 0;
        private Client client;
        private DisplayCamera dragCamera;
        private bool dragging;
        private Point dragStartPoint;
        private FrameworkElement dragged;
        //private WindowHost windowHost;
        //private ImageServerConnection[] imageConnections;
        //private Dictionary<string, Thread> imageThreads;

        private System.Timers.Timer populateCameraTimer;

        private Client.ServerDetails serverDetails = new Client.ServerDetails();
        private List<ICameraServerPlugin> servers;
        private Dictionary<string, Thread> serverThreads;


        /// <summary>
        /// The last area where the user clicked.
        /// </summary>
        private RowDefinition activeControl = null;
        /// <summary>
        /// The current area where the mouse is.
        /// </summary>
        private RowDefinition currentControl = null;

        private User user;

        /// <summary>
        /// Default constructor is never used in RIO, so is hard coded for testing. If you're reading this, use the other constructor.
        /// </summary>
        public MainWindow()
        {
            serverDetails.IpAddress = "192.168.1.230";
            serverDetails.PortNo = 4050;
            Initialize("RIO test video", new User("Hendrik", ""), CameraLayouts.TwoByTwo);
            Connect();
            lvCameras.DataContext = cameras;
            cameraPositionsPopulated = true;
        }

        ~MainWindow()
        {
            Dispose(false);
        }

        public MainWindow(string title, User _user, CameraLayouts _cameraLayout, System.Net.IPEndPoint _serverDetails, Dictionary<string, string> _cameraPositions = null)
        {
            serverDetails.IpAddress = _serverDetails.Address.MapToIPv4().ToString();
            serverDetails.PortNo = _serverDetails.Port;
            Initialize(title, _user, _cameraLayout);
            Connect();
            if ((_cameraPositions == null) || (_cameraPositions.Count < 1))
            {
                cameraPositionsPopulated = true;
            }
            else
            {
                cameraPositions = _cameraPositions;

                if (populateCameraTimer == null)
                {
                    populateCameraTimer = new System.Timers.Timer(1000);
                    populateCameraTimer.Elapsed += PopulateCameraTimer_Elapsed;
                }
                populateCameraTimer.AutoReset = false;
                populateCameraTimer.Start();
            }
        }

        private void Initialize(string title, User _user, CameraLayouts _cameraLayout)
        {
            InitializeComponent();
            Title = title;
            user = _user;
            cameraLayout = _cameraLayout;
            cameras = new ObservableCollection<DisplayCamera>();
            viewGrid.Children.Clear();// For Justin.
            doubleClickTimer = new System.Timers.Timer(300);
            doubleClickTimer.Enabled = false;
            doubleClickTimer.Elapsed += DoubleClickTimer_Elapsed;
            SubscribeMouseEvents();
        }

        private void cameraDrop(object sender, System.Windows.DragEventArgs e, bool preview = false)
        {
            if (dragCamera != null)
            {
                if (CheckAccess())
                {
                    lock (dragCamera)
                    {
                        try
                        {
                            CameraWindow screenImage = (sender as CameraWindow);
                            if (screenImage != null)
                            {
                                if (dragCamera.Displays == null)
                                {
                                    dragCamera.Displays = new List<CameraWindow>();
                                }
                                else if (dragCamera.Displays.Count > 0)
                                {
                                    dragCamera.Displays.Remove(screenImage);
                                }
                                if ((dragCamera.Parent != null) && (dragCamera.Parent.Online))
                                {
                                    if ((cameraPositionsPopulated) && (!preview))
                                    {
                                        dragCamera.StopLive(screenImage.Handle);
                                        screenImage.Clear();
                                    }
                                    dragCamera.GoToLive(screenImage.Handle);
                                }
                                if (!dragCamera.Displays.Contains(screenImage))
                                    dragCamera.Displays.Add(screenImage);
                                if (!preview)
                                {
                                    var videoControl = (screenImage.Parent as Grid).Children[1] as VideoControl;
                                    if (videoControl != null)
                                    {
                                        videoControl.Camera = dragCamera;
                                        videoControl.DisplayHandle = screenImage.Handle;
                                        videoControl.VideoMode = VideoMode.Live;
                                        videoControl.PlaybackVisibility = Visibility.Collapsed;
                                        videoControl.ControlVisibility = Visibility.Collapsed;
                                        videoControl.btnSwitch.ToolTip = VideoMode.Playback.ToString();
                                        dragCamera.PreviewEnabled = false;
                                        // TODO update user's layouts on the server.
                                        if ((cameraPositionsPopulated) && !preview)// Avoid updating the server with the settings you just received from the server, and preview caemras.
                                        {
                                            if (cameraPositions == null)
                                            {
                                                cameraPositions = new Dictionary<string, string>();
                                                cameraPositions.Add(screenImage.Name, dragCamera.Name);
                                            }
                                            else if (cameraPositions.ContainsKey(screenImage.Name))
                                            {
                                                cameraPositions[screenImage.Name] = dragCamera.Name;
                                            }
                                            else
                                            {
                                                cameraPositions.Add(screenImage.Name, dragCamera.Name);
                                            }
                                            client.SendMessage(MessageType.CameraLayouts, new VideoHostConfiguration(cameraLayout, cameraPositions, Title, user));
                                        }
                                    }

                                }
                            }
                            dragCamera = null;
                        }
                        catch (Exception ex)
                        {
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                                "Error dropping camera", ex);
                        }
                    }
                }
                else
                {
                    Dispatcher.Invoke(() => { cameraDrop(sender, e); });
                }
            }
        }

        private void client_ConnectionStatusChanged(object sender, Client.ConnectState connectionState)
        {
            switch (connectionState)
            {
                case Client.ConnectState.Connected:
                    client.SendMessage(MessageType.CameraLayouts);
                    break;
                case Client.ConnectState.Disconnected:
                    CloseMe();
                    break;
            }
        }

        private void client_OnObjectMessageReceived(object sender, CommsNode.ObjectMessageEventArgs e)
        {
            if (CheckAccess())
            {
                switch (e.Message_Type)
                {
                    case MessageType.CameraLayouts: // Load servers, cameras, and layouts for this user, including cameras.
                        var cameraList = (e.Message as ObservableCollection<ICameraPlugin>);
                        if (cameraList != null)
                        {
                            cameras = new ObservableCollection<DisplayCamera>(); // Replace any existing (stale?) cameras.
                            lvCameras.DataContext = cameras;
                            foreach (var cameraPlugin in cameraList)
                            {
                                if (cameraPlugin != null)
                                {
                                    if (servers != null)
                                    {
                                        try
                                        {
                                            cameraPlugin.Parent = servers.FirstOrDefault(x => x.Name == cameraPlugin.Properties["Parent"]);
                                        }
                                        catch (Exception ex)
                                        {
                                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error setting camera's parent in camera list.", ex);
                                        }
                                    }
                                    cameras.Add(new DisplayCamera(cameraPlugin));
                                }
                            }
                            if ((cameraPositions != null) && (cameraPositions.Count > 0) && (servers != null) && (servers.Count > 0))
                            {
                                populateCameraTimer = new System.Timers.Timer(1000);
                                populateCameraTimer.AutoReset = false;
                                populateCameraTimer.Elapsed += PopulateCameraTimer_Elapsed;
                                populateCameraTimer.Start();
                                //PopulateCameraPositions();
                            }
                        }
                        else
                        {
                            servers = e.Message as List<ICameraServerPlugin>;
                            if (servers != null)
                            {
                                serverThreads = new Dictionary<string, Thread>(servers.Count);
                                //foreach (var camera in cameras)
                                //{
                                //    try
                                //    {
                                //        camera.Parent = servers.FirstOrDefault(x => x.Name == camera.Properties["Parent"]);
                                //    }
                                //    catch (Exception ex)
                                //    {
                                //        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error setting camera's parent from server list.", ex);
                                //    }
                                //}
                                foreach (var server in servers)
                                {
                                    if (server.Enabled)
                                    {
                                        Thread thread = new Thread(new ThreadStart(server.Start)) { Name = server.Name };

                                        thread.Start();
                                        serverThreads.Add(server.Name, thread);
                                        Thread.Sleep(100);// Allow server to start up
                                    }
                                }
                                client.SendMessage(MessageType.CameraLayouts, user);
                            }
                        }
                        break;
                    case MessageType.ForceLogoff:
                    case MessageType.UserLoggedOff:
                        var logOffUser = (e.Message as User);
                        if ((logOffUser != null) && (logOffUser.UserName == user.UserName))
                        {
                            CloseMe();
                        }
                        break;
                }
            }
            else
            {
                Dispatcher.Invoke(() => { client_OnObjectMessageReceived(sender, e); });
            }
        }

        private void DoubleClickTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (CheckAccess())
            {
                doubleClickTimer.Stop();
                //doubleClicking = false;
                currentControl = GetCurrentControl(clickPoint);
                if (currentControl != null)
                {
                    var cameraWindow = GetCameraWindow(currentControl);
                    if (cameraWindow != null)
                    {
                        MouseCommand(cameraWindow.Name, clicks);
                        /// Events can't be raised here.
                        /*if (clicks == 1)
                            cameraWindow.MouseActivate(this, new EventArgs());
                        else if (clicks == 2)
                            cameraWindow.MouseDoubleClick(this, new EventArgs());*/
                    }
                }
            }
            else
            {
                Dispatcher.Invoke(() => DoubleClickTimer_Elapsed(sender, e));
            }
        }

        private void MouseCommand(string windowName, byte clicks)
        {
            switch (windowName)
            {
                case "TopLeft":
                    if (clicks == 1) mouseActivateTopLeft();
                    else if (clicks == 2) mouseDoubleClickTopLeft();
                    break;
                case "TopSecond":
                    if (clicks == 1) mouseActivateTopSecond();
                    else if (clicks == 2) mouseDoubleClickTopSecond();
                    break;
                case "TopThird":
                    if (clicks == 1) mouseActivateTopThird();
                    else if (clicks == 2) mouseDoubleClickTopThird();
                    break;
                case "TopRight":
                    if (clicks == 1) mouseActivateTopRight();
                    else if (clicks == 2) mouseDoubleClickTopRight();
                    break;
                case "SecondLeft":
                    if (clicks == 1) mouseActivateSecondLeft();
                    else if (clicks == 2) mouseDoubleClickSecondLeft();
                    break;
                case "SecondSecond":
                    if (clicks == 1) mouseActivateSecondSecond();
                    else if (clicks == 2) mouseDoubleClickSecondSecond();
                    break;
                case "SecondThird":
                    if (clicks == 1) mouseActivateSecondThird();
                    else if (clicks == 2) mouseDoubleClickSecondThird();
                    break;
                case "SecondRight":
                    if (clicks == 1) mouseActivateSecondRight();
                    else if (clicks == 2) mouseDoubleClickSecondRight();
                    break;
                case "ThirdLeft":
                    if (clicks == 1) mouseActivateThirdLeft();
                    else if (clicks == 2) mouseDoubleClickThirdLeft();
                    break;
                case "ThirdSecond":
                    if (clicks == 1) mouseActivateThirdSecond();
                    else if (clicks == 2) mouseDoubleClickThirdSecond();
                    break;
                case "ThirdThird":
                    if (clicks == 1) mouseActivateThirdThird();
                    else if (clicks == 2) mouseDoubleClickThirdThird();
                    break;
                case "ThirdRight":
                    if (clicks == 1) mouseActivateThirdRight();
                    else if (clicks == 2) mouseDoubleClickThirdRight();
                    break;
                case "BottomLeft":
                    if (clicks == 1) mouseActivateBottomLeft();
                    else if (clicks == 2) mouseDoubleClickBottomLeft();
                    break;
                case "BottomSecond":
                    if (clicks == 1) mouseActivateBottomSecond();
                    else if (clicks == 2) mouseDoubleClickBottomSecond();
                    break;
                case "BottomThird":
                    if (clicks == 1) mouseActivateBottomThird();
                    else if (clicks == 2) mouseDoubleClickBottomThird();
                    break;
                case "BottomRight":
                    if (clicks == 1) mouseActivateBottomRight();
                    else if (clicks == 2) mouseDoubleClickBottomRight();
                    break;
            }
            clicks = 0;
        }

        private void PopulateCameraTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            populateCameraTimer.Stop();
            populateCameraTimer.Elapsed -= PopulateCameraTimer_Elapsed;
            populateCameraTimer.Dispose();
            populateCameraTimer = null;
            try
            {
                PopulateCameraPositions();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error populating cameras.", ex);
            }
        }

        private void PopulateCameraPositions()
        {
            while (!displaysReady)
                Thread.Sleep(1000);
            if (cameraPositionsPopulated == false)
            {
                lock (this)
                {
                    if (cameraLayout == CameraLayouts.None)
                    {
                        cameraPositionsPopulated = true;
                        return; // No cameras to populate
                    }
                    if (cameras != null && cameras.Count > 0)
                    {
                        if (servers != null && servers.Count > 0)
                        {
                            if (cameraPositions != null && cameraPositions.Count > 0)
                            {
                                if (cameraPositions.ContainsKey("TopLeft"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["TopLeft"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        //Thread thread = new Thread(new ThreadStart(() => cameraDrop(TopLeft, null)));
                                        //thread.Start();
                                        cameraDrop(TopLeft, null);
                                        TopLeftVideoControl.VideoMode = VideoMode.Live;
                                    }
                                    if (cameraLayout == CameraLayouts.One)
                                    {
                                        cameraPositionsPopulated = true;
                                        return; // All displays populated.
                                    }
                                }
                                if (cameraPositions.ContainsKey("TopSecond"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["TopSecond"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(TopSecond, null);
                                        TopSecondVideoControl.VideoMode = VideoMode.Live;
                                    }
                                }
                                if (cameraPositions.ContainsKey("SecondLeft"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["SecondLeft"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(SecondLeft, null);
                                        SecondLeftVideoControl.VideoMode = VideoMode.Live;
                                    }
                                }
                                if (cameraPositions.ContainsKey("SecondSecond"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["SecondSecond"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(SecondSecond, null);
                                        SecondSecondVideoControl.VideoMode = VideoMode.Live;
                                    }
                                }
                                if (cameraLayout == CameraLayouts.TwoByTwo)
                                {
                                    cameraPositionsPopulated = true;
                                    return;
                                }
                                if (cameraPositions.ContainsKey("ThirdLeft"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["ThirdLeft"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(ThirdLeft, null);
                                    }
                                }
                                if (cameraPositions.ContainsKey("ThirdSecond"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["ThirdSecond"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(ThirdSecond, null);
                                    }
                                }
                                if (cameraPositions.ContainsKey("ThirdThird"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["ThirdThird"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(ThirdThird, null);
                                    }
                                }
                                if (cameraLayout == CameraLayouts.FivePlusOne)
                                {
                                    cameraPositionsPopulated = true;
                                    return;
                                }
                                if (cameraPositions.ContainsKey("TopThird"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["TopThird"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(TopThird, null);
                                    }
                                }
                                if (cameraPositions.ContainsKey("SecondThird"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["SecondThird"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(SecondThird, null);
                                    }
                                }
                                if (cameraLayout == CameraLayouts.ThreeByThree)
                                {
                                    cameraPositionsPopulated = true;
                                    return;
                                }
                                if (cameraPositions.ContainsKey("TopRight"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["TopRight"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(TopRight, null);
                                    }
                                }
                                if (cameraPositions.ContainsKey("SecondRight"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["SecondRight"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(SecondRight, null);
                                    }
                                }
                                if (cameraPositions.ContainsKey("ThirdRight"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["ThirdRight"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(ThirdRight, null);
                                    }
                                }
                                if (cameraPositions.ContainsKey("BottomLeft"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["BottomLeft"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(BottomLeft, null);
                                    }
                                }
                                if (cameraPositions.ContainsKey("BottomSecond"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["BottomSecond"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(BottomSecond, null);
                                    }
                                }
                                if (cameraPositions.ContainsKey("BottomThird"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["BottomThird"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(BottomThird, null);
                                    }
                                }
                                if (cameraPositions.ContainsKey("BottomRight"))
                                {
                                    var currentCamera = cameras.FirstOrDefault(x => x.Name == cameraPositions["BottomRight"]);
                                    if (currentCamera != null)
                                    {
                                        dragCamera = currentCamera;
                                        cameraDrop(BottomRight, null);
                                    }
                                }
                            }
                            cameraPositionsPopulated = true;
                        }
                    }
                    else
                    {
                        populateCameraTimer.Start();
                    }
                }
            }
        }

        private void SubscribeMouseEvents()
        {
            /*mouseEvents = Hook.AppEvents();
            mouseEvents.MouseClick += MouseEvents_MouseClick;
            mouseEvents.MouseDoubleClick += MouseEvents_MouseDoubleClick;

            mouseEvents.MouseWheel += MouseEvents_MouseWheel;

            mouseEvents.MouseDragStarted += MouseEvents_MouseDragStarted;
            mouseEvents.MouseDragFinished += MouseEvents_MouseDragFinished;

            mouseEvents.MouseMove += MouseEvents_MouseMove;*/
        }

        private void MouseEvents_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (dragCamera == null)
            {
                clickPoint = new Point(e.X, e.Y);
                clicks = 2;
                DoubleClickTimer_Elapsed(doubleClickTimer, null);
            }
        }

        private void MouseEvents_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (dragCamera == null)
            {
                clickPoint = new Point(e.X, e.Y);
                clicks = 1;
                //doubleClicking = true;
                doubleClickTimer.Start();
            }
        }

        private void UnsubscribeMouseEvents()
        {
            /*if (mouseEvents != null)
            {
                mouseEvents.MouseClick -= MouseEvents_MouseClick;
                mouseEvents.MouseDoubleClick -= MouseEvents_MouseDoubleClick;

                mouseEvents.MouseWheel -= MouseEvents_MouseWheel;

                mouseEvents.MouseDragStarted -= MouseEvents_MouseDragStarted;
                mouseEvents.MouseDragFinished -= MouseEvents_MouseDragFinished;

                mouseEvents.MouseMove -= MouseEvents_MouseMove;
            }*/
        }

        private CameraWindow GetCameraWindow(RowDefinition row)
        {
            if (CheckAccess())
            {
                if (row == null)
                    return null;
                else
                {
                    var grid = row.Parent as Grid;
                    if (grid == null)
                        return null;
                    else
                    {
                        if (grid.Children != null && grid.Children.Count >= 1)
                        {
                            var videoControl = grid.Children[0] as CameraWindow;
                            return videoControl == null ? null : videoControl;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            else
            {
                CameraWindow result = null;
                Dispatcher.Invoke(() => { result = GetCameraWindow(row); });
                return result;
            }
        }


        private DisplayCamera GetCamera(RowDefinition row)
        {
            if (CheckAccess())
            {
                if (row == null)
                    return null;
                else
                {
                    var grid = row.Parent as Grid;
                    if (grid == null)
                        return null;
                    else
                    {
                        if (grid.Children != null && grid.Children.Count >= 2)
                        {
                            var videoControl = grid.Children[1] as VideoControl;
                            if (videoControl == null)
                                return null;
                            else
                            {
                                return videoControl.Camera == null ? null : videoControl.Camera;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            else
            {
                DisplayCamera result = null;
                Dispatcher.Invoke(() => { result = GetCamera(row); });
                return result;
            }
        }

        private void MouseEvents_MouseDragFinished(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (!dragging || currentControl == null)//Drag started outside of camera controls
            {
                dragging = false;
                return;
            }
            else
            {
                DisplayCamera camera = GetCamera(currentControl);
                if (camera != null)
                {
                    if (!camera.StopPanTilt())
                        Exceptions.ExceptionsManager.Add("StopPtz", "error while attempting to stop PTZ on camera " + camera.DisplayName + " with ID " + camera.Name);
                }
                dragging = false;
                return;
            }
        }

        private void MouseEvents_MouseDragStarted(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            dragStartPoint = new Point(e.X, e.Y);
            currentControl = GetCurrentControl(dragStartPoint);
            if (currentControl == null)
                dragging = false;
            else
                dragging = true;
        }

        private void MouseEvents_MouseMove(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (dragging)
            {
                var newPoint = new Point(e.X, e.Y);
                if (currentControl == GetCurrentControl(newPoint))
                {
                    DisplayCamera camera = GetCamera(currentControl);
                    if (camera != null)
                    {
                        if (!camera.PanTilt((int)(dragStartPoint.X - newPoint.X), (int)(dragStartPoint.Y - newPoint.Y)))
                            Exceptions.ExceptionsManager.Add("PanTilt", "error while sending pan/tilt command to camera " + camera.DisplayName + " with ID " + camera.Name);
                    }
                }
                else
                {
                    dragging = false;
                    return;
                }
            }
        }

        private void MouseEvents_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            currentControl = GetCurrentControl(new Point(e.X, e.Y));
            if (currentControl == null)
                return;
            DisplayCamera camera = GetCamera(currentControl);
            if (camera != null)
            {
                if (camera.Zoom(e.Delta))
                {
                    Thread.Sleep(500);
                    if (!camera.StopZoom())
                        Exceptions.ExceptionsManager.Add("StopPtz", "error while attempting to stop zoom on camera " + camera.DisplayName + " with ID " + camera.Name);
                }
                else
                    Exceptions.ExceptionsManager.Add("StopPtz", "error while attempting to zoom on camera " + camera.DisplayName + " with ID " + camera.Name);
            }
            return;
        }

        /// <summary>
        /// This returns the RowDefinition where the mouse cursor currently resides.
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private RowDefinition GetCurrentControl(Point point)
        {
            if (CheckAccess())
            {
                Point viewGridPoint = viewGrid.PointFromScreen(point);
                if ((viewGridPoint.X < 0) || (viewGridPoint.Y < 0)) /// Point isn't on the viewgrid.
                    return null;

                Point cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, TopLeftGrid);
                if (cameraWindowPoint.X < 0 || cameraWindowPoint.Y < 0) // Zooming above or to the left of the topleft window has no effect, as the mouse is not over a camera.
                    return null;

                switch (cameraLayout)
                {
                    case CameraLayouts.One:
                        return TopLeftControl;
                    case CameraLayouts.TwoByTwo:
                        if (TopLeft.Fullscreen)
                            return TopLeftControl;
                        else if (TopSecond.Fullscreen)
                            return TopSecondControl;
                        else if (SecondLeft.Fullscreen)
                            return SecondLeftControl;
                        else if (SecondSecond.Fullscreen)
                            return SecondSecondControl;

                        cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, SecondSecondGrid);
                        if (cameraWindowPoint.X < 0) // Mouse is to the left of the bottom right camera window
                        {
                            if (cameraWindowPoint.Y < 0)// Mouse is also above the bottom right camera window
                                return TopLeftControl;
                            else return SecondLeftControl;
                        }
                        else
                        {
                            if (cameraWindowPoint.Y < 0)
                                return TopSecondControl;
                            else return SecondSecondControl;
                        }
                    case CameraLayouts.FivePlusOne:
                        if (TopLeft.Fullscreen)
                            return TopLeftControl;
                        else if (TopSecond.Fullscreen)
                            return TopSecondControl;
                        else if (SecondLeft.Fullscreen)
                            return SecondLeftControl;
                        else if (SecondSecond.Fullscreen)
                            return SecondSecondControl;
                        else if (ThirdLeft.Fullscreen)
                            return ThirdLeftControl;
                        else if (ThirdSecond.Fullscreen)
                            return ThirdSecondControl;
                        else if (ThirdThird.Fullscreen)
                            return ThirdThirdControl;

                        cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, ThirdThirdGrid);
                        if (cameraWindowPoint.X < 0) // Mouse is to the left of the bottom right camera window
                        {
                            if (cameraWindowPoint.Y < 0)// Mouse is also above the bottom right camera window
                                return TopLeftControl;
                            else
                            {
                                cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, ThirdSecondGrid);
                                if (cameraWindowPoint.X < 0)
                                    return ThirdLeftControl;
                                else return ThirdSecondControl;
                            }
                        }
                        else
                        {
                            if (cameraWindowPoint.Y < 0)
                            {
                                cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, SecondSecondGrid);
                                if (cameraWindowPoint.Y < 0)
                                    return TopSecondControl;
                                else return SecondSecondControl;
                            }
                            else return ThirdThirdControl;
                        }
                    case CameraLayouts.ThreeByThree:
                        if (TopLeft.Fullscreen)
                            return TopLeftControl;
                        else if (TopSecond.Fullscreen)
                            return TopSecondControl;
                        else if (TopThird.Fullscreen)
                            return TopThirdControl;
                        else if (SecondLeft.Fullscreen)
                            return SecondLeftControl;
                        else if (SecondSecond.Fullscreen)
                            return SecondSecondControl;
                        else if (SecondThird.Fullscreen)
                            return SecondThirdControl;
                        else if (ThirdLeft.Fullscreen)
                            return ThirdLeftControl;
                        else if (ThirdSecond.Fullscreen)
                            return ThirdSecondControl;
                        else if (ThirdThird.Fullscreen)
                            return ThirdThirdControl;

                        cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, SecondSecondGrid);
                        if (cameraWindowPoint.X < 0) // Mouse is to the left of the middle camera window
                        {
                            if (cameraWindowPoint.Y < 0)// Mouse is above the middle camera window
                                return TopLeftControl;
                            else
                            {
                                if (cameraWindowPoint.Y > SecondSecondControl.ActualHeight)
                                    return ThirdLeftControl;
                                else
                                    return SecondLeftControl;
                            }
                        }
                        else
                        {
                            if (cameraWindowPoint.Y < 0)
                            {
                                if (cameraWindowPoint.X > SecondSecondGrid.ActualWidth)
                                    return TopThirdControl;
                                else return TopSecondControl;
                            }
                            else
                            {
                                if (cameraWindowPoint.Y > SecondSecondGrid.ActualHeight)
                                {
                                    if (cameraWindowPoint.X > SecondSecondGrid.ActualWidth)
                                        return ThirdThirdControl;
                                    else return ThirdSecondControl;
                                }
                                else
                                {
                                    if (cameraWindowPoint.X > SecondSecondGrid.ActualWidth)
                                        return SecondThirdControl;
                                    else return SecondSecondControl;
                                }
                            }
                        }
                    case CameraLayouts.TwelvePlusOne:
                        if (TopLeft.Fullscreen)
                            return TopLeftControl;
                        else if (TopSecond.Fullscreen)
                            return TopSecondControl;
                        else if (TopThird.Fullscreen)
                            return TopThirdControl;
                        else if (TopRight.Fullscreen)
                            return TopRightControl;
                        else if (SecondLeft.Fullscreen)
                            return SecondLeftControl;
                        else if (SecondSecond.Fullscreen)
                            return SecondSecondControl;
                        else if (SecondRight.Fullscreen)
                            return SecondRightControl;
                        else if (ThirdLeft.Fullscreen)
                            return ThirdLeftControl;
                        else if (ThirdRight.Fullscreen)
                            return ThirdRightControl;
                        else if (BottomLeft.Fullscreen)
                            return BottomLeftControl;
                        else if (BottomSecond.Fullscreen)
                            return BottomSecondControl;
                        else if (BottomThird.Fullscreen)
                            return BottomThirdControl;
                        else if (BottomRight.Fullscreen)
                            return BottomRightControl;

                        cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, SecondSecondGrid);
                        if (cameraWindowPoint.X < 0) // Mouse is to the left of the middle camera window
                        {
                            if (cameraWindowPoint.Y < 0)// Mouse is above the middle camera window
                                return TopLeftControl;
                            else
                            {
                                cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, ThirdLeftGrid);
                                if (cameraWindowPoint.Y < 0)
                                    return SecondLeftControl;
                                else
                                {
                                    if (cameraWindowPoint.Y > ThirdLeftGrid.ActualHeight)
                                        return BottomLeftControl;
                                    else
                                        return ThirdLeftControl;
                                }
                            }
                        }
                        else
                        {
                            if (cameraWindowPoint.Y < 0)
                            {
                                cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, TopThirdGrid);
                                if (cameraWindowPoint.X < 0)
                                    return TopSecondControl;
                                else
                                {
                                    if (cameraWindowPoint.X > TopThirdGrid.ActualWidth)
                                        return TopRightControl;
                                    else
                                        return TopThirdControl;
                                }
                            }
                            else
                            {
                                if (cameraWindowPoint.X > SecondSecondGrid.ActualWidth)
                                {
                                    if (cameraWindowPoint.Y > SecondRightGrid.ActualHeight)
                                    {
                                        if (cameraWindowPoint.Y > SecondSecondGrid.ActualHeight)
                                            return BottomRightControl;
                                        else return ThirdRightControl;
                                    }
                                    else
                                        return SecondRightControl;
                                }
                                else
                                {
                                    if (cameraWindowPoint.Y > SecondSecondGrid.ActualHeight)
                                    {
                                        if (cameraWindowPoint.X > BottomSecondGrid.ActualWidth)
                                            return BottomThirdControl;
                                        else
                                            return BottomSecondControl;
                                    }
                                    else
                                        return SecondSecondControl;
                                }
                            }
                        }
                    case CameraLayouts.FourByFour:
                        if (TopLeft.Fullscreen)
                            return TopLeftControl;
                        else if (TopSecond.Fullscreen)
                            return TopSecondControl;
                        else if (TopThird.Fullscreen)
                            return TopThirdControl;
                        else if (TopRight.Fullscreen)
                            return TopRightControl;
                        else if (SecondLeft.Fullscreen)
                            return SecondLeftControl;
                        else if (SecondSecond.Fullscreen)
                            return SecondSecondControl;
                        else if (SecondThird.Fullscreen)
                            return SecondThirdControl;
                        else if (SecondRight.Fullscreen)
                            return SecondRightControl;
                        else if (ThirdLeft.Fullscreen)
                            return ThirdLeftControl;
                        else if (ThirdSecond.Fullscreen)
                            return ThirdSecondControl;
                        else if (ThirdThird.Fullscreen)
                            return ThirdThirdControl;
                        else if (ThirdRight.Fullscreen)
                            return ThirdRightControl;
                        else if (BottomLeft.Fullscreen)
                            return BottomLeftControl;
                        else if (BottomSecond.Fullscreen)
                            return BottomSecondControl;
                        else if (BottomThird.Fullscreen)
                            return BottomThirdControl;
                        else if (BottomRight.Fullscreen)
                            return BottomRightControl;

                        cameraWindowPoint = viewGrid.TranslatePoint(viewGridPoint, ThirdThirdGrid);
                        if (cameraWindowPoint.X < 0)
                        {// Mouse is in the leftmost two columns
                            if (cameraWindowPoint.Y < 0)
                            {//Mouse is in the topmost two rows
                                if ((cameraWindowPoint.Y + ThirdThirdGrid.ActualHeight) > 0)
                                {// Mouse is only one row above thirdthird
                                    if ((cameraWindowPoint.X + ThirdThirdGrid.ActualWidth) > 0)
                                    {// Mouse is only one column to the left of thirdthird
                                        return SecondSecondControl;
                                    }
                                    else
                                    {// Mouse is more than one column to the left
                                        return SecondLeftControl;
                                    }
                                }
                                else
                                {// More than one row above
                                    if ((cameraWindowPoint.X + ThirdThirdGrid.ActualWidth) > 0)
                                    {// Mouse is only one column to the left of thirdthird
                                        return TopSecondControl;
                                    }
                                    else
                                    {// Mouse is more than one column to the left
                                        return TopLeftControl;
                                    }
                                }
                            }
                            else
                            {// Mouse is in bottom two rows
                                if (cameraWindowPoint.Y > ThirdThirdGrid.ActualHeight)
                                {// Mouse is in bottom row
                                    if ((cameraWindowPoint.X + ThirdThirdGrid.ActualWidth) < 0)
                                        return BottomLeftControl;
                                    else return BottomSecondControl;
                                }
                                else
                                {// Mouse is in third row
                                    if ((cameraWindowPoint.X + ThirdThirdGrid.ActualWidth) < 0)
                                        return ThirdLeftControl;
                                    else return ThirdSecondControl;
                                }
                            }
                        }
                        else
                        {// Mouse is in rightmost two columns
                            if (cameraWindowPoint.Y < 0)
                            {//Mouse is in the topmost two rows
                                if ((cameraWindowPoint.Y + ThirdThirdGrid.ActualHeight) > 0)
                                {// Mouse is only one row above thirdthird
                                    if (cameraWindowPoint.X < ThirdThirdGrid.ActualWidth)
                                    {// Mouse is in the same column as thirdthird
                                        return SecondThirdControl;
                                    }
                                    else
                                    {// Mouse is one column to the right
                                        return SecondRightControl;
                                    }
                                }
                                else
                                {// More than one row above
                                    if (cameraWindowPoint.X < ThirdThirdGrid.ActualWidth)
                                    {// Mouse is only one row to the left of thirdthird
                                        return TopThirdControl;
                                    }
                                    else
                                    {// Mouse is more than one row to the left
                                        return TopRightControl;
                                    }
                                }
                            }
                            else
                            {// Mouse is in bottom two rows
                                if (cameraWindowPoint.Y > ThirdThirdGrid.ActualHeight)
                                {// Mouse is in bottom row
                                    if (cameraWindowPoint.X < ThirdThirdGrid.ActualWidth)
                                        return BottomThirdControl;
                                    else return BottomRightControl;
                                }
                                else
                                {// Mouse is in third row
                                    if (cameraWindowPoint.X < ThirdThirdGrid.ActualWidth)
                                        return ThirdThirdControl;
                                    else return ThirdRightControl;
                                }
                            }

                        }
                    default: return null;
                }
            }
            else
            {
                RowDefinition result = null;
                Dispatcher.Invoke(() => { result = GetCurrentControl(point); });
                return result;
            }
        }

        public void CloseMe()
        {
            try
            {
                if (Dispatcher.CheckAccess())
                    Close();
                else
                    Dispatcher.Invoke(Close);
            }
            catch (TaskCanceledException)
            {
                //bury it
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Unexpected error while closing.", ex);
            }
        }

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (lvCameras.Visibility == Visibility.Visible)
                {
                    lvCameras.Visibility = Visibility.Collapsed;
                    gridSplitter.Visibility = Visibility.Collapsed;
                    btnExpand.Text = "▲";
                    cameraListWidth = cameraListGrid.Width;
                    cameraListGrid.Width = new GridLength(30);
                }
                else
                {
                    lvCameras.Visibility = Visibility.Visible;
                    gridSplitter.Visibility = Visibility.Visible;
                    btnExpand.Text = "▼";
                    cameraListGrid.Width = cameraListWidth;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error changing camera list visibility.", ex);
            }
        }

        private void GenerateLayout()
        {
            switch (cameraLayout)
            {
                #region One
                case CameraLayouts.One:
                    
                    //TopLeft = new CameraWindow(new Size(viewGrid.ColumnDefinitions[0].ActualWidth * 4, viewGrid.RowDefinitions[0].ActualHeight * 4));
                    //TopLeft.Name = "TopLeft";
                    //TopLeft.MouseActivate += mouseActivateTopLeft;
                    //TopLeft.MouseUp += mouseUp;
                    Grid.SetColumnSpan(TopLeftGrid, 4);
                    Grid.SetRowSpan(TopLeftGrid, 4);
                    //viewGrid.Children.Add(TopLeft);
                    
                    break;
                #endregion
                #region TwoByTwo
                case CameraLayouts.TwoByTwo:
                    viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                    viewGrid.RowDefinitions[2].Height = gridLengthZero;
                    viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                    viewGrid.RowDefinitions[3].Height = gridLengthZero;
                    break;
                #endregion
                #region FivePlusOne
                case CameraLayouts.FivePlusOne:
                    viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                    viewGrid.RowDefinitions[3].Height = gridLengthZero;
                    //TopLeft = new CameraWindow(new Size(viewGrid.ColumnDefinitions[0].ActualWidth * 2, viewGrid.RowDefinitions[0].ActualHeight * 2)); //Hotspot is unique size - use same size for all others.
                    //TopLeft.Name = "TopLeft";
                    //TopLeft.MouseActivate += mouseActivateTopLeft;
                    //TopLeft.MouseUp += mouseUp;
                    Grid.SetColumnSpan(TopLeftGrid, 2);
                    Grid.SetRowSpan(TopLeftGrid, 2);
                    //viewGrid.Children.Add(TopLeft);

                    //var windowSize = new Size(viewGrid.ColumnDefinitions[0].ActualWidth, viewGrid.RowDefinitions[0].ActualHeight);

                    Grid.SetColumn(TopSecondGrid, 2);

                    Grid.SetColumn(SecondSecondGrid, 2);
                    //Grid.SetRow(SecondSecondGrid, 1);

                    Grid.SetRow(ThirdLeftGrid, 2);

                    //Grid.SetColumn(ThirdSecondGrid, 1);
                    //Grid.SetRow(ThirdSecondGrid, 2);

                    //Grid.SetColumn(ThirdThirdGrid, 2);
                    //Grid.SetRow(ThirdThirdGrid, 2);
                    
                    break;
                #endregion
                #region ThreeeByThreee
                case CameraLayouts.ThreeByThree:
                    viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                    viewGrid.RowDefinitions[3].Height = gridLengthZero;
                    break;
#endregion
                #region TwelvePlusOne
                case CameraLayouts.TwelvePlusOne:

                    Grid.SetColumnSpan(SecondSecondGrid, 2);
                    Grid.SetRowSpan(SecondSecondGrid, 2);

                    Grid.SetColumn(SecondThirdGrid, 3);

                    Grid.SetColumn(ThirdThirdGrid, 3);

                    break;
                #endregion
            }
        }
        private void mouseActivateTopLeft()
        {
            if (activeControl == null)
            {
                activeControl = TopLeftControl;

            }
            else if (activeControl != TopLeftControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = TopLeftControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (TopLeftVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                TopLeftVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (TopLeftVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                TopLeftVideoControl.ControlVisibility = Visibility.Visible;
                TopLeftVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                TopLeftVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateTopLeft(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateTopLeft();
            else
                Dispatcher.Invoke(mouseActivateTopLeft);
        }

        private void mouseDoubleClickTopLeft()
        {
            if (TopLeft.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwoByTwo:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        break;
                    case CameraLayouts.FivePlusOne:
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                    default: break;
                }
                TopLeft.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwoByTwo:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        break;
                    case CameraLayouts.FivePlusOne:
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                    default: break;
                }

                TopLeft.Fullscreen = true;
            }
        }

        private void mouseDoubleClickTopLeft(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickTopLeft();
            else
                Dispatcher.Invoke(mouseDoubleClickTopLeft);
        }

        private void mouseActivateTopSecond()
        {
            if (activeControl == null)
            {
                activeControl = TopSecondControl;

            }
            else if (activeControl != TopSecondControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = TopSecondControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (TopSecondVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                TopSecondVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (TopSecondVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                TopSecondVideoControl.ControlVisibility = Visibility.Visible;
                TopSecondVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                TopSecondVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateTopSecond(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateTopSecond();
            else
                Dispatcher.Invoke(mouseActivateTopSecond);
        }

        private void mouseDoubleClickTopSecond()
        {
            if (TopSecond.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwoByTwo:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        break;
                    case CameraLayouts.FivePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                TopSecond.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwoByTwo:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        break;
                    case CameraLayouts.FivePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                    default: break;
                }
                TopSecond.Fullscreen = true;
            }
        }

        private void mouseDoubleClickTopSecond(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickTopSecond();
            else
                Dispatcher.Invoke(mouseDoubleClickTopSecond);
        }

        private void mouseActivateTopThird()
        {
            if (activeControl == null)
            {
                activeControl = TopThirdControl;

            }
            else if (activeControl != TopThirdControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = TopThirdControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (TopThirdVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                TopThirdVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (TopThirdVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                TopThirdVideoControl.ControlVisibility = Visibility.Visible;
                TopThirdVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                TopThirdVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateTopThird(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateTopThird();
            else
                Dispatcher.Invoke(mouseActivateTopThird);
        }

        private void mouseDoubleClickTopThird()
        {
            if (TopThird.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                TopThird.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                }
                TopThird.Fullscreen = true;
            }
        }

        private void mouseDoubleClickTopThird(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickTopThird();
            else
                Dispatcher.Invoke(mouseDoubleClickTopThird);
        }

        private void mouseActivateTopRight()
        {
            if (activeControl == null)
            {
                activeControl = TopRightControl;

            }
            else if (activeControl != TopRightControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = TopRightControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (TopRightVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                TopRightVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (TopRightVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                TopRightVideoControl.ControlVisibility = Visibility.Visible;
                TopRightVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                TopRightVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateTopRight(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateTopRight();
            else
                Dispatcher.Invoke(mouseActivateTopRight);
        }

        private void mouseDoubleClickTopRight()
        {
            if (TopRight.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                    default: break;
                }
                TopRight.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                    default: break;
                }
                TopRight.Fullscreen = true;
            }
        }

        private void mouseDoubleClickTopRight(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickTopRight();
            else
                Dispatcher.Invoke(mouseDoubleClickTopRight);
        }

        private void mouseActivateSecondLeft()
        {
            if (activeControl == null)
            {
                activeControl = SecondLeftControl;

            }
            else if (activeControl != SecondLeftControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = SecondLeftControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (SecondLeftVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                SecondLeftVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (SecondLeftVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                SecondLeftVideoControl.ControlVisibility = Visibility.Visible;
                SecondLeftVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                SecondLeftVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateSecondLeft(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateSecondLeft();
            else
                Dispatcher.Invoke(mouseActivateSecondLeft);
        }

        private void mouseDoubleClickSecondLeft()
        {
            if (SecondLeft.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwoByTwo:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        break;
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                SecondLeft.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwoByTwo:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        break;
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                }
                SecondLeft.Fullscreen = true;
            }
        }

        private void mouseDoubleClickSecondLeft(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickSecondLeft();
            else
                Dispatcher.Invoke(mouseDoubleClickSecondLeft);
        }

        private void mouseActivateSecondSecond()
        {
            if (activeControl == null)
            {
                activeControl = SecondSecondControl;

            }
            else if (activeControl != SecondSecondControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = SecondSecondControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (SecondSecondVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                SecondSecondVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (SecondSecondVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                SecondSecondVideoControl.ControlVisibility = Visibility.Visible;
                SecondSecondVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                SecondSecondVideoControl.PlaybackVisibility = Visibility.Collapsed;

        }

        private void mouseActivateSecondSecond(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateSecondSecond();
            else
                Dispatcher.Invoke(mouseActivateSecondSecond);
        }

        private void mouseDoubleClickSecondSecond()
        {
            if (SecondSecond.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwoByTwo:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        break;
                    case CameraLayouts.FivePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                    default: break;
                }
                SecondSecond.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwoByTwo:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        break;
                    case CameraLayouts.FivePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                    default: break;
                }
                SecondSecond.Fullscreen = true;
            }
        }

        private void mouseDoubleClickSecondSecond(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickSecondSecond();
            else
                Dispatcher.Invoke(mouseDoubleClickSecondSecond);
        }

        private void mouseActivateSecondThird()
        {
            if (activeControl == null)
            {
                activeControl = SecondThirdControl;

            }
            else if (activeControl != SecondThirdControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = SecondThirdControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (SecondThirdVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                SecondThirdVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (SecondThirdVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                SecondThirdVideoControl.ControlVisibility = Visibility.Visible;
                SecondThirdVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                SecondThirdVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateSecondThird(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateSecondThird();
            else
                Dispatcher.Invoke(mouseActivateSecondThird);
        }

        private void mouseDoubleClickSecondThird()
        {
            if (SecondThird.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                SecondThird.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                }
                SecondThird.Fullscreen = true;
            }
        }

        private void mouseDoubleClickSecondThird(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickSecondThird();
            else
                Dispatcher.Invoke(mouseDoubleClickSecondThird);
        }

        private void mouseActivateSecondRight()
        {
            if (activeControl == null)
            {
                activeControl = SecondRightControl;

            }
            else if (activeControl != SecondRightControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = SecondRightControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (SecondRightVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                SecondRightVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (SecondRightVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                SecondRightVideoControl.ControlVisibility = Visibility.Visible;
                SecondRightVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                SecondRightVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateSecondRight(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateSecondRight();
            else
                Dispatcher.Invoke(mouseActivateSecondRight);
        }

        private void mouseDoubleClickSecondRight()
        {
            if (SecondRight.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                SecondRight.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                SecondRight.Fullscreen = true;
            }
        }

        private void mouseDoubleClickSecondRight(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickSecondRight();
            else
                Dispatcher.Invoke(mouseDoubleClickSecondRight);
        }

        private void mouseActivateThirdLeft()
        {
            if (activeControl == null)
            {
                activeControl = ThirdLeftControl;

            }
            else if (activeControl != ThirdLeftControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = ThirdLeftControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (ThirdLeftVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                ThirdLeftVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (ThirdLeftVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                ThirdLeftVideoControl.ControlVisibility = Visibility.Visible;
                ThirdLeftVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                ThirdLeftVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateThirdLeft(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateThirdLeft();
            else
                Dispatcher.Invoke(mouseActivateThirdLeft);
        }

        private void mouseDoubleClickThirdLeft()
        {
            if (ThirdLeft.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.FivePlusOne:
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                ThirdLeft.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.FivePlusOne:
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                }
                ThirdLeft.Fullscreen = true;
            }
        }

        private void mouseDoubleClickThirdLeft(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickThirdLeft();
            else
                Dispatcher.Invoke(mouseDoubleClickThirdLeft);
        }

        private void mouseActivateThirdSecond()
        {
            if (activeControl == null)
            {
                activeControl = ThirdSecondControl;

            }
            else if (activeControl != ThirdSecondControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = ThirdSecondControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (ThirdSecondVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                ThirdSecondVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (ThirdSecondVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                ThirdSecondVideoControl.ControlVisibility = Visibility.Visible;
                ThirdSecondVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                ThirdSecondVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateThirdSecond(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateThirdSecond();
            else
                Dispatcher.Invoke(mouseActivateThirdSecond);
        }

        private void mouseDoubleClickThirdSecond()
        {
            if (ThirdSecond.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.FivePlusOne:
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                ThirdSecond.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.FivePlusOne:
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        break;
                    case CameraLayouts.TwelvePlusOne:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                }
                ThirdSecond.Fullscreen = true;
            }
        }

        private void mouseDoubleClickThirdSecond(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickThirdSecond();
            else
                Dispatcher.Invoke(mouseDoubleClickThirdSecond);
        }

        private void mouseActivateThirdThird()
        {
            if (activeControl == null)
            {
                activeControl = ThirdThirdControl;

            }
            else if (activeControl != ThirdThirdControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = ThirdThirdControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (ThirdThirdVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                ThirdThirdVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (ThirdThirdVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                ThirdThirdVideoControl.ControlVisibility = Visibility.Visible;
                ThirdThirdVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                ThirdThirdVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateThirdThird(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateThirdThird();
            else
                Dispatcher.Invoke(mouseActivateThirdThird);
        }

        private void mouseDoubleClickThirdThird()
        {
            if (ThirdThird.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.FivePlusOne:
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                ThirdThird.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.FivePlusOne:
                    case CameraLayouts.ThreeByThree:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        break;
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                }
                ThirdThird.Fullscreen = true;
            }
        }

        private void mouseDoubleClickThirdThird(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickThirdThird();
            else
                Dispatcher.Invoke(mouseDoubleClickThirdThird);
        }

        private void mouseActivateThirdRight()
        {
            if (activeControl == null)
            {
                activeControl = ThirdRightControl;

            }
            else if (activeControl != ThirdRightControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = ThirdRightControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (ThirdRightVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                ThirdRightVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (ThirdRightVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                ThirdRightVideoControl.ControlVisibility = Visibility.Visible;
                ThirdRightVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                ThirdRightVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateThirdRight(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateThirdRight();
            else
                Dispatcher.Invoke(mouseActivateThirdRight);
        }

        private void mouseDoubleClickThirdRight()
        {
            if (ThirdRight.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[3].Height = gridLengthStar;
                        break;
                }
                ThirdRight.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[3].Height = gridLengthZero;
                        break;
                }
                ThirdRight.Fullscreen = true;
            }
        }

        private void mouseDoubleClickThirdRight(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickThirdRight();
            else
                Dispatcher.Invoke(mouseDoubleClickThirdRight);
        }

        private void mouseActivateBottomLeft()
        {
            if (activeControl == null)
            {
                activeControl = BottomLeftControl;

            }
            else if (activeControl != BottomLeftControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = BottomLeftControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (BottomLeftVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                BottomLeftVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (BottomLeftVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                BottomLeftVideoControl.ControlVisibility = Visibility.Visible;
                BottomLeftVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                BottomLeftVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateBottomLeft(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateBottomLeft();
            else
                Dispatcher.Invoke(mouseActivateBottomLeft);
        }

        private void mouseDoubleClickBottomLeft()
        {
            if (BottomLeft.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                }
                BottomLeft.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                }
                BottomLeft.Fullscreen = true;
            }
        }

        private void mouseDoubleClickBottomLeft(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickBottomLeft();
            else
                Dispatcher.Invoke(mouseDoubleClickBottomLeft);
        }

        private void mouseActivateBottomSecond()
        {
            if (activeControl == null)
            {
                activeControl = BottomSecondControl;

            }
            else if (activeControl != BottomSecondControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = BottomSecondControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (BottomSecondVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                BottomSecondVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (BottomSecondVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                BottomSecondVideoControl.ControlVisibility = Visibility.Visible;
                BottomSecondVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                BottomSecondVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateBottomSecond(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateBottomSecond();
            else
                Dispatcher.Invoke(mouseActivateBottomSecond);
        }

        private void mouseDoubleClickBottomSecond()
        {
            if (BottomSecond.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                }
                BottomSecond.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                }
                BottomSecond.Fullscreen = true;
            }
        }

        private void mouseDoubleClickBottomSecond(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickBottomSecond();
            else
                Dispatcher.Invoke(mouseDoubleClickBottomSecond);
        }

        private void mouseActivateBottomThird()
        {
            if (activeControl == null)
            {
                activeControl = BottomThirdControl;

            }
            else if (activeControl != BottomThirdControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = BottomThirdControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (BottomThirdVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                BottomThirdVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (BottomThirdVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                BottomThirdVideoControl.ControlVisibility = Visibility.Visible;
                BottomThirdVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                BottomThirdVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateBottomThird(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateBottomThird();
            else
                Dispatcher.Invoke(mouseActivateBottomThird);
        }

        private void mouseDoubleClickBottomThird()
        {
            if (BottomThird.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                }
                BottomThird.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[3].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                }
                BottomThird.Fullscreen = true;
            }
        }

        private void mouseDoubleClickBottomThird(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickBottomThird();
            else
                Dispatcher.Invoke(mouseDoubleClickBottomThird);
        }

        private void mouseActivateBottomRight()
        {
            if (activeControl == null)
            {
                activeControl = BottomRightControl;

            }
            else if (activeControl != BottomRightControl)
            {
                activeControl.Height = gridLengthZero;
                activeControl = BottomRightControl;
            }
            else
            {
                if (activeControl.ActualHeight > 0)
                {
                    activeControl.Height = gridLengthZero;
                    return;
                }
            }
            if (BottomRightVideoControl.VideoMode == VideoMode.Live)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                BottomRightVideoControl.ControlVisibility = Visibility.Visible;
            }
            else if (BottomRightVideoControl.VideoMode == VideoMode.Playback)
            {
                activeControl.Height = new GridLength(1, GridUnitType.Auto);
                BottomRightVideoControl.ControlVisibility = Visibility.Visible;
                BottomRightVideoControl.PlaybackVisibility = Visibility.Visible;
            }
            else
                BottomRightVideoControl.PlaybackVisibility = Visibility.Collapsed;
        }

        private void mouseActivateBottomRight(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseActivateBottomRight();
            else
                Dispatcher.Invoke(mouseActivateBottomRight);
        }

        private void mouseDoubleClickBottomRight()
        {
            if (BottomRight.Fullscreen)
            {// Resize down.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthStar;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthStar;
                        viewGrid.RowDefinitions[0].Height = gridLengthStar;
                        viewGrid.RowDefinitions[1].Height = gridLengthStar;
                        viewGrid.RowDefinitions[2].Height = gridLengthStar;
                        break;
                }
                BottomRight.Fullscreen = false;
            }
            else
            {// Size up.
                switch (cameraLayout)
                {
                    case CameraLayouts.TwelvePlusOne:
                    case CameraLayouts.FourByFour:
                        viewGrid.ColumnDefinitions[0].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[1].Width = gridLengthZero;
                        viewGrid.ColumnDefinitions[2].Width = gridLengthZero;
                        viewGrid.RowDefinitions[0].Height = gridLengthZero;
                        viewGrid.RowDefinitions[1].Height = gridLengthZero;
                        viewGrid.RowDefinitions[2].Height = gridLengthZero;
                        break;
                }
                BottomRight.Fullscreen = true;
            }
        }

        private void mouseDoubleClickBottomRight(object sender, EventArgs e)
        {
            if (CheckAccess())
                mouseDoubleClickBottomRight();
            else
                Dispatcher.Invoke(mouseDoubleClickBottomRight);
        }

        private void mouseUp(object sender, EventArgs e)
        {
            if (dragCamera == null)
            {
                Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
            }
            else
            {
                //CameraWindow currentWindow = sender as CameraWindow;
                //if (currentWindow != null)
                //{
                cameraDrop(sender, null);
                //}
            }
        }
                
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TopLeft.MouseActivate -= mouseActivateTopLeft;
            TopLeft.MouseDoubleClick -= mouseDoubleClickTopLeft;
            TopLeft.MouseUp -= mouseUp;
            TopSecond.MouseActivate -= mouseActivateTopSecond;
            TopSecond.MouseDoubleClick -= mouseDoubleClickTopSecond;
            TopSecond.MouseUp -= mouseUp;
            TopThird.MouseActivate -= mouseActivateTopThird;
            TopThird.MouseDoubleClick -= mouseDoubleClickTopThird;
            TopThird.MouseUp -= mouseUp;
            TopRight.MouseActivate -= mouseActivateTopRight;
            TopRight.MouseDoubleClick -= mouseDoubleClickTopRight;
            TopRight.MouseUp -= mouseUp;
            SecondLeft.MouseActivate -= mouseActivateSecondLeft;
            SecondLeft.MouseDoubleClick -= mouseDoubleClickSecondLeft;
            SecondLeft.MouseUp -= mouseUp;
            SecondSecond.MouseActivate -= mouseActivateSecondSecond;
            SecondSecond.MouseDoubleClick -= mouseDoubleClickSecondSecond;
            SecondSecond.MouseUp -= mouseUp;
            SecondThird.MouseActivate -= mouseActivateSecondThird;
            SecondThird.MouseDoubleClick -= mouseDoubleClickSecondThird;
            SecondThird.MouseUp -= mouseUp;
            SecondRight.MouseActivate -= mouseActivateSecondRight;
            SecondRight.MouseDoubleClick -= mouseDoubleClickSecondRight;
            SecondRight.MouseUp -= mouseUp;
            ThirdLeft.MouseActivate -= mouseActivateThirdLeft;
            ThirdLeft.MouseDoubleClick -= mouseDoubleClickThirdLeft;
            ThirdLeft.MouseUp -= mouseUp;
            ThirdSecond.MouseActivate -= mouseActivateThirdSecond;
            ThirdSecond.MouseDoubleClick -= mouseDoubleClickThirdSecond;
            ThirdSecond.MouseUp -= mouseUp;
            ThirdThird.MouseActivate -= mouseActivateThirdThird;
            ThirdThird.MouseDoubleClick -= mouseDoubleClickThirdThird;
            ThirdThird.MouseUp -= mouseUp;
            ThirdRight.MouseActivate -= mouseActivateThirdRight;
            ThirdRight.MouseDoubleClick -= mouseDoubleClickThirdRight;
            ThirdRight.MouseUp -= mouseUp;
            BottomLeft.MouseActivate -= mouseActivateBottomLeft;
            BottomLeft.MouseDoubleClick -= mouseDoubleClickBottomLeft;
            BottomLeft.MouseUp -= mouseUp;
            BottomSecond.MouseActivate -= mouseActivateBottomSecond;
            BottomSecond.MouseDoubleClick -= mouseDoubleClickBottomSecond;
            BottomSecond.MouseUp -= mouseUp;
            BottomThird.MouseActivate -= mouseActivateBottomThird;
            BottomThird.MouseDoubleClick -= mouseDoubleClickBottomThird;
            BottomThird.MouseUp -= mouseUp;
            BottomRight.MouseActivate -= mouseActivateBottomRight;
            BottomRight.MouseDoubleClick -= mouseDoubleClickBottomRight;
            BottomRight.MouseUp -= mouseUp;
            Dispose(true);
        }

        private void gridSplitter_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.SizeWE;
        }

        private void gridSplitter_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            Mouse.OverrideCursor = System.Windows.Input.Cursors.Arrow;
        }
        
        private void TopLeftGrid_Loaded(object sender, RoutedEventArgs e)
        {
            TopLeft = new CameraWindow(new Size(TopLeftGrid.ActualWidth, TopLeftGrid.RowDefinitions[0].ActualHeight));
            TopLeft.Name = "TopLeft";
            TopLeft.MouseActivate += mouseActivateTopLeft;
            TopLeft.MouseDoubleClick += mouseDoubleClickTopLeft;
            TopLeft.MouseUp += mouseUp;
            TopLeftGrid.Children.Add(TopLeft);
            
            if (cameraLayout == CameraLayouts.One)
                displaysReady = true;
        }

        private void TopSecondGrid_Loaded(object sender, RoutedEventArgs e)
        {
            TopSecond = new CameraWindow(new Size(TopSecondGrid.ActualWidth, TopSecondGrid.RowDefinitions[0].ActualHeight));
            TopSecond.Name = "TopSecond";
            TopSecond.MouseActivate += mouseActivateTopSecond;
            TopSecond.MouseDoubleClick += mouseDoubleClickTopSecond;
            TopSecond.MouseUp += mouseUp;
            TopSecondGrid.Children.Add(TopSecond);
        }

        private void TopThirdGrid_Loaded(object sender, RoutedEventArgs e)
        {
            TopThird = new CameraWindow(new Size(TopThirdGrid.ActualWidth, TopThirdGrid.RowDefinitions[0].ActualHeight));
            TopThird.Name = "TopThird";
            TopThird.MouseActivate += mouseActivateTopThird;
            TopThird.MouseDoubleClick += mouseDoubleClickTopThird;
            TopThird.MouseUp += mouseUp;
            TopThirdGrid.Children.Add(TopThird);
        }

        private void TopRightGrid_Loaded(object sender, RoutedEventArgs e)
        {
            TopRight = new CameraWindow(new Size(TopRightGrid.ActualWidth, TopRightGrid.RowDefinitions[0].ActualHeight));
            TopRight.Name = "TopRight";
            TopRight.MouseActivate += mouseActivateTopRight;
            TopRight.MouseDoubleClick += mouseDoubleClickTopRight;
            TopRight.MouseUp += mouseUp;
            TopRightGrid.Children.Add(TopRight);
        }

        private void SecondLeftGrid_Loaded(object sender, RoutedEventArgs e)
        {
            SecondLeft = new CameraWindow(new Size(SecondLeftGrid.ActualWidth, SecondLeftGrid.RowDefinitions[0].ActualHeight));
            SecondLeft.Name = "SecondLeft";
            SecondLeft.MouseActivate += mouseActivateSecondLeft;
            SecondLeft.MouseDoubleClick += mouseDoubleClickSecondLeft;
            SecondLeft.MouseUp += mouseUp;
            SecondLeftGrid.Children.Add(SecondLeft);
        }

        private void SecondSecondGrid_Loaded(object sender, RoutedEventArgs e)
        {
            SecondSecond = new CameraWindow(new Size(SecondSecondGrid.ActualWidth, SecondSecondGrid.RowDefinitions[0].ActualHeight));
            SecondSecond.Name = "SecondSecond";
            SecondSecond.MouseActivate += mouseActivateSecondSecond;
            SecondSecond.MouseDoubleClick += mouseDoubleClickSecondSecond;
            SecondSecond.MouseUp += mouseUp;
            SecondSecondGrid.Children.Add(SecondSecond);
            if (cameraLayout == CameraLayouts.TwoByTwo)
                displaysReady = true;
        }

        private void SecondThirdGrid_Loaded(object sender, RoutedEventArgs e)
        {
            SecondThird = new CameraWindow(new Size(SecondThirdGrid.ActualWidth, SecondThirdGrid.RowDefinitions[0].ActualHeight));
            SecondThird.Name = "SecondThird";
            SecondThird.MouseActivate += mouseActivateSecondThird;
            SecondThird.MouseDoubleClick += mouseDoubleClickSecondThird;
            SecondThird.MouseUp += mouseUp;
            SecondThirdGrid.Children.Add(SecondThird);
        }

        private void SecondRightGrid_Loaded(object sender, RoutedEventArgs e)
        {
            SecondRight = new CameraWindow(new Size(SecondRightGrid.ActualWidth, SecondRightGrid.RowDefinitions[0].ActualHeight));
            SecondRight.Name = "SecondRight";
            SecondRight.MouseActivate += mouseActivateSecondRight;
            SecondRight.MouseDoubleClick += mouseDoubleClickSecondRight;
            SecondRight.MouseUp += mouseUp;
            SecondRightGrid.Children.Add(SecondRight);
        }

        private void ThirdLeftGrid_Loaded(object sender, RoutedEventArgs e)
        {
            ThirdLeft = new CameraWindow(new Size(ThirdLeftGrid.ActualWidth, ThirdLeftGrid.RowDefinitions[0].ActualHeight));
            ThirdLeft.Name = "ThirdLeft";
            ThirdLeft.MouseActivate += mouseActivateThirdLeft;
            ThirdLeft.MouseDoubleClick += mouseDoubleClickThirdLeft;
            ThirdLeft.MouseUp += mouseUp;
            ThirdLeftGrid.Children.Add(ThirdLeft);
        }

        private void ThirdSecondGrid_Loaded(object sender, RoutedEventArgs e)
        {
            ThirdSecond = new CameraWindow(new Size(ThirdSecondGrid.ActualWidth, ThirdSecondGrid.RowDefinitions[0].ActualHeight));
            ThirdSecond.Name = "ThirdSecond";
            ThirdSecond.MouseActivate += mouseActivateThirdSecond;
            ThirdSecond.MouseDoubleClick += mouseDoubleClickThirdSecond;
            ThirdSecond.MouseUp += mouseUp;
            ThirdSecondGrid.Children.Add(ThirdSecond);
        }

        private void ThirdThirdGrid_Loaded(object sender, RoutedEventArgs e)
        {
            ThirdThird = new CameraWindow(new Size(ThirdThirdGrid.ActualWidth, ThirdThirdGrid.RowDefinitions[0].ActualHeight));
            ThirdThird.Name = "ThirdThird";
            ThirdThird.MouseActivate += mouseActivateThirdThird;
            ThirdThird.MouseDoubleClick += mouseDoubleClickThirdThird;
            ThirdThird.MouseUp += mouseUp;
            ThirdThirdGrid.Children.Add(ThirdThird);
            if (cameraLayout == CameraLayouts.FivePlusOne || cameraLayout == CameraLayouts.ThreeByThree)
                displaysReady = true;
        }

        private void ThirdRightGrid_Loaded(object sender, RoutedEventArgs e)
        {
            ThirdRight = new CameraWindow(new Size(ThirdRightGrid.ActualWidth, ThirdRightGrid.RowDefinitions[0].ActualHeight));
            ThirdRight.Name = "ThirdRight";
            ThirdRight.MouseActivate += mouseActivateThirdRight;
            ThirdRight.MouseDoubleClick += mouseDoubleClickThirdRight;
            ThirdRight.MouseUp += mouseUp;
            ThirdRightGrid.Children.Add(ThirdRight);
        }

        private void BottomLeftGrid_Loaded(object sender, RoutedEventArgs e)
        {
            BottomLeft = new CameraWindow(new Size(BottomLeftGrid.ActualWidth, BottomLeftGrid.RowDefinitions[0].ActualHeight));
            BottomLeft.Name = "BottomLeft";
            BottomLeft.MouseActivate += mouseActivateBottomLeft;
            BottomLeft.MouseDoubleClick += mouseDoubleClickBottomLeft;
            BottomLeft.MouseUp += mouseUp;
            BottomLeftGrid.Children.Add(BottomLeft);
        }

        private void BottomSecondGrid_Loaded(object sender, RoutedEventArgs e)
        {
            BottomSecond = new CameraWindow(new Size(BottomSecondGrid.ActualWidth, BottomSecondGrid.RowDefinitions[0].ActualHeight));
            BottomSecond.Name = "BottomSecond";
            BottomSecond.MouseActivate += mouseActivateBottomSecond;
            BottomSecond.MouseDoubleClick += mouseDoubleClickBottomSecond;
            BottomSecond.MouseUp += mouseUp;
            BottomSecondGrid.Children.Add(BottomSecond);
        }

        private void BottomThirdGrid_Loaded(object sender, RoutedEventArgs e)
        {
            BottomThird = new CameraWindow(new Size(BottomThirdGrid.ActualWidth, BottomThirdGrid.RowDefinitions[0].ActualHeight));
            BottomThird.Name = "BottomThird";
            BottomThird.MouseActivate += mouseActivateBottomThird;
            BottomThird.MouseDoubleClick += mouseDoubleClickBottomThird;
            BottomThird.MouseUp += mouseUp;
            BottomThirdGrid.Children.Add(BottomThird);
        }

        private void BottomRightGrid_Loaded(object sender, RoutedEventArgs e)
        {
            BottomRight = new CameraWindow(new Size(BottomRightGrid.ActualWidth, BottomRightGrid.RowDefinitions[0].ActualHeight));
            BottomRight.Name = "BottomRight";
            BottomRight.MouseActivate += mouseActivateBottomRight;
            BottomRight.MouseDoubleClick += mouseDoubleClickBottomRight;
            BottomRight.MouseUp += mouseUp;
            BottomRightGrid.Children.Add(BottomRight);
            displaysReady = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var screenCount = Screen.AllScreens.Count();
            if (screenCount > 1)
            {
                var i = 0;
                Screen videoScreen = Screen.AllScreens[i];
                while ((videoScreen == Screen.PrimaryScreen) && (i < screenCount))
                {
                    videoScreen = Screen.AllScreens[++i];
                }
                System.Drawing.Rectangle r1 = videoScreen.WorkingArea;
                Top = r1.Top;
                Left = r1.Left;
                Show();
            }
            WindowState = WindowState.Maximized;
        }

        private void StackPanel_ToolTipClosing(object sender, ToolTipEventArgs e)
        {
            var previewCamera = (sender as StackPanel).DataContext as DisplayCamera;
            if (previewCamera != null)
            {
                var toolTipWindow = ((sender as StackPanel).ToolTip as PreviewToolTip).Content as CameraWindow;
                if (toolTipWindow != null)
                {
                    if (previewCamera.Displays != null)
                        previewCamera.Displays.Remove(toolTipWindow);
                    previewCamera.StopLive(toolTipWindow.Handle);
                    toolTipWindow.Clear();
                }
            }
        }

        private void PreviewToolTip_ToolTipOpening(object sender, ToolTipEventArgs e)
        {
            var previewCamera = (sender as StackPanel).DataContext as DisplayCamera;
            if ((previewCamera != null) && (dragCamera == null))
            {
                var toolTipWindow = ((sender as StackPanel).ToolTip as PreviewToolTip).Content as CameraWindow;
                if (toolTipWindow != null)
                {
                    dragCamera = previewCamera;
                    cameraDrop(toolTipWindow, null, true);
                }
            }
        }

        //private void StackPanel_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        //{
        //    if (dragCamera == null)
        //        previewCamera = ((sender as StackPanel).DataContext as DisplayCamera);
        //}

        //private void StackPanel_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        //{
        //    previewCamera = null;
        //}

        private void Connect()
        {
            client = new Client();
            client.ConnectionStatusChanged += client_ConnectionStatusChanged;
            client.OnObjectMessageReceived += client_OnObjectMessageReceived;
            client.Connect(serverDetails.IpAddress, serverDetails.PortNo);
        }

        private void DoDragDrop()
        {
            if ((dragCamera != null) && (dragged != null))
            {
                System.Windows.Forms.DataObject data = new System.Windows.Forms.DataObject(dragCamera);
                DragDrop.DoDragDrop(dragged, data, System.Windows.DragDropEffects.All);
                dragged = null;
            }
        }

        private void DragDrop_StartDragging(object sender, MouseButtonEventArgs e)
        {
            if (dragged != null)
                return;

            UIElement lb = (UIElement)sender;
            if (lb != null)
            {
                UIElement element = lb.InputHitTest(e.GetPosition(lb)) as UIElement;
                while (element != null)
                {
                    dragged = (FrameworkElement)element;
                    if (dragged != null)
                        break;
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }
            }
        }

        private void DragDrop_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (dragged == null)
            {
                //activeControl = null; // Remove active control so that PTZ commands aren't routed incorrectly.
                return;
            }
            try
            {
                if (e.LeftButton == MouseButtonState.Released)
                {
                    dragged = null;
                    return;
                }
                else 
                {
                    dragCamera = dragged.DataContext as DisplayCamera;
                    if (dragCamera != null)
                    {
                        Dispatcher.Invoke(DoDragDrop);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error while dragging camera.", ex);
            }
        }

        private void On_UIReady(object sender, RoutedEventArgs e)
        {
            if (cameraPositions != null && cameraPositions.Count > 0)
                ExpandMouseDown(null, null);
            GenerateLayout();
            viewGrid.Children.Add(TopLeftGrid);
            switch (cameraLayout)
            {
                case CameraLayouts.TwoByTwo:
                    viewGrid.Children.Add(TopSecondGrid);
                    viewGrid.Children.Add(SecondLeftGrid);
                    viewGrid.Children.Add(SecondSecondGrid);
                    break;
                case CameraLayouts.FivePlusOne:
                    viewGrid.Children.Add(TopSecondGrid);
                    viewGrid.Children.Add(SecondSecondGrid);
                    viewGrid.Children.Add(ThirdLeftGrid);
                    viewGrid.Children.Add(ThirdSecondGrid);
                    viewGrid.Children.Add(ThirdThirdGrid);
                    break;
                case CameraLayouts.ThreeByThree:
                    viewGrid.Children.Add(TopSecondGrid);
                    viewGrid.Children.Add(TopThirdGrid);
                    viewGrid.Children.Add(SecondLeftGrid);
                    viewGrid.Children.Add(SecondSecondGrid);
                    viewGrid.Children.Add(SecondThirdGrid);
                    viewGrid.Children.Add(ThirdLeftGrid);
                    viewGrid.Children.Add(ThirdSecondGrid);
                    viewGrid.Children.Add(ThirdThirdGrid);
                    break;
                case CameraLayouts.TwelvePlusOne:
                    viewGrid.Children.Add(TopSecondGrid);
                    viewGrid.Children.Add(TopThirdGrid);
                    viewGrid.Children.Add(TopRightGrid);
                    viewGrid.Children.Add(SecondLeftGrid);
                    viewGrid.Children.Add(SecondSecondGrid);
                    viewGrid.Children.Add(SecondThirdGrid);
                    viewGrid.Children.Add(ThirdLeftGrid);
                    viewGrid.Children.Add(ThirdThirdGrid);
                    viewGrid.Children.Add(BottomLeftGrid);
                    viewGrid.Children.Add(BottomSecondGrid);
                    viewGrid.Children.Add(BottomThirdGrid);
                    viewGrid.Children.Add(BottomRightGrid);
                    break;
                case CameraLayouts.FourByFour:
                    viewGrid.Children.Add(TopSecondGrid);
                    viewGrid.Children.Add(TopThirdGrid);
                    viewGrid.Children.Add(TopRightGrid);
                    viewGrid.Children.Add(SecondLeftGrid);
                    viewGrid.Children.Add(SecondSecondGrid);
                    viewGrid.Children.Add(SecondThirdGrid);
                    viewGrid.Children.Add(SecondRightGrid);
                    viewGrid.Children.Add(ThirdLeftGrid);
                    viewGrid.Children.Add(ThirdSecondGrid);
                    viewGrid.Children.Add(ThirdThirdGrid);
                    viewGrid.Children.Add(ThirdRightGrid);
                    viewGrid.Children.Add(BottomLeftGrid);
                    viewGrid.Children.Add(BottomSecondGrid);
                    viewGrid.Children.Add(BottomThirdGrid);
                    viewGrid.Children.Add(BottomRightGrid);
                    break;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            UnsubscribeMouseEvents();
            if (disposing)
            {
                GC.SuppressFinalize(this);
                if (dragCamera != null)
                {
                    dragCamera.Dispose();
                    dragCamera = null;
                }
                if (populateCameraTimer != null)
                {
                    populateCameraTimer.Dispose();
                    populateCameraTimer = null;
                }
                if (user != null)
                {
                    user.Dispose();
                    user = null;
                }
            }
            try
            {
                if (client != null)
                {
                    client.Disconnect();
                    client.ConnectionStatusChanged -= client_ConnectionStatusChanged;
                    client.OnObjectMessageReceived -= client_OnObjectMessageReceived;
                    client.Dispose();

                    client = null;
                }
                if (serverThreads != null)
                {
                    foreach (var serverThread in serverThreads)
                    {
                        serverThread.Value.Abort();
                    }
                    serverThreads.Clear();
                    serverThreads = null;
                }
                //if (TopLeft != null) { TopLeft.Dispose(); TopLeft = null; }
                //if (TopSecond != null) { TopSecond.Dispose(); TopSecond = null; }
                //if (TopThird != null) { TopThird.Dispose(); TopThird = null; }
                //if (TopRight != null) { TopRight.Dispose(); TopRight = null; }
                //if (SecondLeft != null) { SecondLeft.Dispose(); SecondLeft = null; }
                //if (SecondSecond != null) { SecondSecond.Dispose(); SecondSecond = null; }
                //if (SecondThird != null) { SecondThird.Dispose(); SecondThird = null; }
                //if (SecondRight != null) { SecondRight.Dispose(); SecondRight = null; }
                //if (ThirdLeft != null) { ThirdLeft.Dispose(); ThirdLeft = null; }
                //if (ThirdSecond != null) { ThirdSecond.Dispose(); ThirdSecond = null; }
                //if (ThirdThird != null) { ThirdThird.Dispose(); ThirdThird = null; }
                //if (ThirdRight != null) { ThirdRight.Dispose(); ThirdRight = null; }
                //if (BottomLeft != null) { BottomLeft.Dispose(); BottomLeft = null; }
                //if (BottomSecond != null) { BottomSecond.Dispose(); BottomSecond = null; }
                //if (BottomThird != null) { BottomThird.Dispose(); BottomThird = null; }
                //if (BottomRight != null) { BottomRight.Dispose(); BottomRight = null; }

                if (cameras != null && cameras.Count > 0)
                {
                    foreach (var camera in cameras)
                    {
                        if ((camera != null) && (camera.Displays != null))
                        {
                            foreach (var display in camera.Displays)
                            {
                                camera.StopLive(display.Handle);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write("Unexpected error " + ex);
                //throw;
            }
            CloseMe();
        }


        private void videoControl_CameraClosedEvent(object sender, CameraClosedEventArgs e)
        {
            if (e != null)
            {
                if (cameraPositions == null)
                    cameraPositions = new Dictionary<string, string>();
                else
                {
                    if (cameraPositions.ContainsKey(e.Position))
                        cameraPositions.Remove(e.Position);
                }
                client.SendMessage(MessageType.RemoveCamera, new CameraPosition(Title, e.Position, user.Name));
            }
        }
    }
}
