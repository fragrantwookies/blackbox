﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Reflection;

using eNerve.DataTypes;

namespace VideoHost
{
    /// <summary>
    /// Interaction logic for VideoControl.xaml
    /// </summary>
    public partial class VideoControl : UserControl, INotifyPropertyChanged
    {
        #region PropertyChanged
        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(_propertyName));
        }
        #endregion PropertyChanged

        public event EventHandler<CameraClosedEventArgs> CameraClosedEvent;
        
        public Visibility ControlVisibility
        { get { return controlVisibility; } set { controlVisibility = value; PropertyChangedHandler("ControlVisibility"); } }
        private Visibility controlVisibility = Visibility.Collapsed;

        public Visibility PlaybackVisibility
        { get { return playbackVisibility; } set { playbackVisibility = value; PropertyChangedHandler("PlaybackVisibility"); } }
        private Visibility playbackVisibility = Visibility.Collapsed;

        public string ToolTipContent { get { return seekTime.AddSeconds((int)timeSlider.Value).ToString("hh:mm:ss"); } }

        public string TimeLabel { get { return timeLabel; } set { timeLabel = value; PropertyChangedHandler("TimeLabel"); } }
        private string timeLabel;

        public string DateLabel { get { return dateLabel; } set { dateLabel = value; PropertyChangedHandler("DateLabel"); } }
        private string dateLabel;

        public DisplayCamera Camera { get; set; }
        public IntPtr DisplayHandle { get; set; }

        private readonly System.Timers.Timer displayTimer;
        private DateTime seekTime;
        
        public VideoMode VideoMode = VideoMode.None;
        public VideoControl()
        {
            InitializeComponent();
            controlGrid.DataContext = this;
            lblDate.DataContext = this;
            lblTime.DataContext = this;
            displayTimer = new System.Timers.Timer(1000);
            displayTimer.Elapsed += DisplayTimer_Elapsed;
            displayTimer.Enabled = false;
        }

        private void btnSwitch_Click(object sender, RoutedEventArgs e)
        {
            if (VideoMode == VideoMode.Playback)
            {
                PlaybackVisibility = Visibility.Collapsed;
                btnSwitch.ToolTip = VideoMode.ToString();
                Camera.GoToLive(DisplayHandle);
                VideoMode = VideoMode.Live;

            }
            else if (VideoMode == VideoMode.Live)
            {
                btnSwitch.ToolTip = VideoMode.ToString();
                if (Camera.GoToPlayback(DisplayHandle))
                {
                    seekTime = DateTime.Now.AddSeconds(-60);
                    TimeLabel = seekTime.ToString("hh:mm:ss");
                    DateLabel = seekTime.ToString("d");
                    displayTimer.Start();
                    VideoMode = VideoMode.Playback;
                    PlaybackVisibility = Visibility.Visible;
                }
            }
            else
            {
                PlaybackVisibility = Visibility.Collapsed;
                Height = 0;
            }
        }

        private void DisplayTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (CheckAccess())
            {
                DisplayTimer_Elapsed();
            }
            else
            {
                Dispatcher.Invoke(DisplayTimer_Elapsed);
            }
        }

        private void DisplayTimer_Elapsed()
        {
            timeSlider.Value += 1;
            TimeLabel = seekTime.ToString("hh:mm:ss");
            DateLabel = seekTime.ToString("d");
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            if (Camera != null)
            {
                Camera.StopLive(DisplayHandle);                
            }
            PlaybackVisibility = Visibility.Collapsed;
            ControlVisibility = Visibility.Collapsed;
            VideoMode = VideoMode.None;
            OnCameraClosedEvent(new CameraClosedEventArgs(Name));
            // TODO notyify MainWindow that camera has been removed, so that server can be udpated.
        }

        private void OnCameraClosedEvent(CameraClosedEventArgs e)
        {
            if ((CameraClosedEvent != null) && (e != null))
            {
                CameraClosedEvent(this, e);
            }
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (e.NewValue != 1800)
            {
                var slider = sender as Slider;
                if (slider != null)
                {
                    seekTime = seekTime.AddSeconds((int)(e.NewValue - e.OldValue));
                    timeSlider.AutoToolTipFormat = seekTime.ToString("hh:mm:ss");
                }
            }
        }

        private void Slider_DragStarted(object sender, RoutedEventArgs e)
        {
            displayTimer.Stop();
        }

        private void Slider_DragCompleted(object sender, RoutedEventArgs e)
        {
            if (Camera != null)
            {
                if (Camera.GoToTime(seekTime))
                    TimeLabel = seekTime.ToString("hh:mm:ss");
                timeSlider.Value = 1800;
            }
            displayTimer.Start();
        }

        private void btnPause_Click(object sender, RoutedEventArgs e)
        {
            if (Camera != null)
            {
                if (Camera.Pause())
                    displayTimer.Stop();
            }
        }

        private void btnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (Camera != null)
            {
                if (Camera.Play())
                    displayTimer.Start();
            }

        }

        /// <summary>
        /// TODO Ensure file name is a configurable field with variables (camera name, datetime, etc.)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSnapshot_Click(object sender, RoutedEventArgs e)
        {
            if (Camera != null)
            {
                string fileName = string.Format("C:\\NetDEVSDK\\Pic\\{0}-", Camera.DisplayName.Substring(Camera.DisplayName.LastIndexOf('.') + 1));
                if (VideoMode == VideoMode.Playback)
                    fileName += seekTime.ToShortTimeString().Replace(":", "");
                else
                    fileName += DateTime.Now.ToShortTimeString().Replace(":", "");
                fileName = fileName.Replace(" ", "");
                Camera.SnapShot(DisplayHandle, fileName);
            }
        }

        private void changeDate(object sender, MouseButtonEventArgs e)
        {
            displayTimer.Stop();
            datePicker.ShowDropDownButton = true;
        }

        private void datePicker_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            datePicker.Visibility = Visibility.Collapsed;
            if (datePicker.Value.HasValue)
            {
                seekTime = datePicker.Value.Value;
                if (Camera.GoToTime(seekTime))
                {
                    TimeLabel = seekTime.ToString("hh:mm:ss");
                    DateLabel = seekTime.ToString("d");
                }
            }
            displayTimer.Start();

        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            ExportWindow exportWindow = new ExportWindow(Camera.DisplayName);
            if (exportWindow.ShowDialog() == true)
            {
                string result;
                if (exportWindow.Start > exportWindow.End)
                {
                    result = Camera.Export(exportWindow.End, exportWindow.Start, string.Format("{0}\\{1}", exportWindow.Target, Camera.DisplayName.Substring(Camera.DisplayName.LastIndexOf('.') + 1)),
                        DisplayHandle);
                }
                else
                {
                    result = Camera.Export(exportWindow.Start, exportWindow.End, string.Format("{0}\\{1}", exportWindow.Target, Camera.DisplayName.Substring(Camera.DisplayName.LastIndexOf('.') + 1)),
                        DisplayHandle);
                }
                if (string.IsNullOrEmpty(result) || result.StartsWith("Error", StringComparison.OrdinalIgnoreCase))
                {
                    // Sum ting wong
                    MessageBox.Show("Error - could not perform export.");
                }
                else
                {
                    // Report success.
                    MessageBox.Show("Success - downloading exported video file to " + result);
                }
            }
        }
    }
}
