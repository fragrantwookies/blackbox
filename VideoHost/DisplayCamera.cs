﻿using System;
using System.Collections.Generic;

using eNervePluginInterface;
using eNervePluginInterface.CameraInterfaces;

namespace VideoHost
{
    public class DisplayCamera : ICameraPlugin, IDisposable
    {
        readonly ICameraPlugin cameraPlugin;
        
        public DisplayCamera(ICameraPlugin plugin)
        {
            cameraPlugin = plugin;
        }
        public bool Admin
        {
            get { return cameraPlugin.Admin; }
            set { cameraPlugin.Admin = value; }
        }

        public string Author
        { get { return cameraPlugin.Author; } }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                return cameraPlugin.CustomPropertyDefinitions;
            }
        }

        public string Description
        {
            get
            {
                return cameraPlugin.Description;
            }
        }

        public string DisplayName
        {
            get
            {
                var displayName = Properties.FindProperty("DisplayName").PropertyValue;
                if (string.IsNullOrEmpty(displayName))
                    return Name;
                else return displayName;
            }
        }

        public bool Enabled
        {
            get
            {
                return cameraPlugin.Enabled;
            }

            set
            {
                cameraPlugin.Enabled = value;
            }
        }

        public string FileName
        {
            get
            {
                return cameraPlugin.FileName;
            }

            set
            {
                cameraPlugin.FileName = value;
            }
        }

        public INerveHostPlugin Host
        {
            get
            {
                return cameraPlugin.Host;
            }

            set
            {
                cameraPlugin.Host = value;
            }
        }

        public string Name
        {
            get
            {
                return cameraPlugin.Name;
            }

            set
            {
                cameraPlugin.Name = value;
            }
        }

        public bool Online
        {
            get
            {
                return cameraPlugin.Online;
            }

            set
            {
                cameraPlugin.Online = value;
            }
        }

        public ICameraServerPlugin Parent
        {
            get
            {
                return cameraPlugin.Parent;
            }

            set
            {
                cameraPlugin.Parent = value;
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        public CustomProperties Properties
        {
            get
            {
                return cameraPlugin.Properties;
            }
        }

        public bool PreviewEnabled { get; set; }

        public string Version
        {
            get
            {
                return cameraPlugin.Version;
            }
        }

        public List<CameraWindow> Displays { get; set; }

        public event EventHandler DeviceOffline;
        public event EventHandler DeviceOnline;
        public event EventHandler<PluginErrorEventArgs> Error;

        public bool Equals(INervePlugin other)
        {
            return (Name == other.Name && FileName == other.FileName);
        }

        public string Export(DateTime start, DateTime end, string fileName, IntPtr displayHandle)
        {
            IExportCamera exportCamera = cameraPlugin as IExportCamera;
            if (exportCamera == null)
                return string.Empty;
            else
                return exportCamera.Export(start, end, fileName, displayHandle);
        }

        /// <summary>
        /// Display live video feed from this camera. TODO if not available, display backround with info.w
        /// </summary>
        /// <param name="displayHandle"></param>
        public void GoToLive(IntPtr displayHandle)
        {
            ILiveCamera liveCamera = cameraPlugin as ILiveCamera;
            if (liveCamera != null)
                liveCamera.GoToLive(displayHandle);
        }

        public bool GoToPlayback(IntPtr displayHandle)
        {
            IPlaybackCamera playbackCamera = cameraPlugin as IPlaybackCamera;
            if (playbackCamera == null)
                return false;
            else
                return playbackCamera.GoToPlayback(displayHandle);
        }

        public bool GoToTime(DateTime localTime)
        {
            if (localTime > (DateTime.Now.AddSeconds(-30)))
                return false;
            IPlaybackCamera playbackCamera = cameraPlugin as IPlaybackCamera;
            if (playbackCamera == null)
                return false;
            else
                return playbackCamera.GoToTime(localTime);
        }

        public void SnapShot(IntPtr handle, string fileName)
        {
            ISnapshotCamera snapshotCamera = cameraPlugin as ISnapshotCamera;
            if (snapshotCamera != null)
                snapshotCamera.SnapShot(handle, fileName);
        }

        public void Start()
        {
            cameraPlugin.Start();
        }

        public void StopLive(IntPtr displayHandle)
        {
            ILiveCamera liveCamera = cameraPlugin as ILiveCamera;
            if (liveCamera != null)
                liveCamera.StopLive(displayHandle);
        }

        public override string ToString()
        {
            try
            {
                string displayName = Properties["DisplayName"];
                if (!string.IsNullOrEmpty(displayName))
                    return displayName;
                else return Name;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);
            try
            {
                Displays.Clear();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.Write("Unexpected error " + ex);
            }
        }

        public bool Pause()
        {
            IPlaybackCamera playbackCamera = cameraPlugin as IPlaybackCamera;
            if (playbackCamera == null)
                return false;
            else
                return playbackCamera.Pause();
        }

        public bool Play()
        {
            IPlaybackCamera playbackCamera = cameraPlugin as IPlaybackCamera;
            if (playbackCamera == null)
                return false;
            else
                return playbackCamera.Play();
        }

        public bool PanTilt(int x, int y)
        {
            return false;
        }

        public bool StopPanTilt()
        {
            return false;
        }

        public bool Zoom(int factor)
        {
            IPtzCamera ptzCamera = cameraPlugin as IPtzCamera;
            if (ptzCamera == null)
                return false;
            else
                return ptzCamera.Zoom(factor);
        }

        public bool StopZoom()
        {
            IPtzCamera ptzCamera = cameraPlugin as IPtzCamera;
            if (ptzCamera == null)
                return false;
            else
                return ptzCamera.StopZoom();
        }
    }
}
