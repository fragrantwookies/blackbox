﻿namespace TestRig
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            if(blackBoxManager != null)
                blackBoxManager.Dispose();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.lbDevices = new System.Windows.Forms.ListBox();
            this.contextMenuStripRelays = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemRelay1 = new System.Windows.Forms.ToolStripMenuItem();
            this.trueToolStripMenuIem = new System.Windows.Forms.ToolStripMenuItem();
            this.falseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemRelay2 = new System.Windows.Forms.ToolStripMenuItem();
            this.trueToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.falseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemRelay3 = new System.Windows.Forms.ToolStripMenuItem();
            this.trueToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.falseToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemRelay4 = new System.Windows.Forms.ToolStripMenuItem();
            this.trueToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.falseToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.serialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lbMessages = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTestXML = new System.Windows.Forms.Button();
            this.btnSerialize = new System.Windows.Forms.Button();
            this.btnRecursiveTest = new System.Windows.Forms.Button();
            this.btnTestServer = new System.Windows.Forms.Button();
            this.btnTestVivotek = new System.Windows.Forms.Button();
            this.btnAnimate = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnTestRandom = new System.Windows.Forms.Button();
            this.btnXMLSerialize = new System.Windows.Forms.Button();
            this.btnXmlSerialize2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnReversebits = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.contextMenuStripRelays.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(21, 22);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(102, 22);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(183, 22);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 2;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // lbDevices
            // 
            this.lbDevices.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbDevices.ContextMenuStrip = this.contextMenuStripRelays;
            this.lbDevices.FormattingEnabled = true;
            this.lbDevices.Location = new System.Drawing.Point(12, 95);
            this.lbDevices.Name = "lbDevices";
            this.lbDevices.Size = new System.Drawing.Size(367, 563);
            this.lbDevices.TabIndex = 3;
            // 
            // contextMenuStripRelays
            // 
            this.contextMenuStripRelays.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemRelay1,
            this.toolStripMenuItemRelay2,
            this.toolStripMenuItemRelay3,
            this.toolStripMenuItemRelay4,
            this.serialToolStripMenuItem});
            this.contextMenuStripRelays.Name = "contextMenuStripRelays";
            this.contextMenuStripRelays.Size = new System.Drawing.Size(112, 114);
            // 
            // toolStripMenuItemRelay1
            // 
            this.toolStripMenuItemRelay1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trueToolStripMenuIem,
            this.falseToolStripMenuItem});
            this.toolStripMenuItemRelay1.Name = "toolStripMenuItemRelay1";
            this.toolStripMenuItemRelay1.Size = new System.Drawing.Size(111, 22);
            this.toolStripMenuItemRelay1.Text = "Relay 1";
            // 
            // trueToolStripMenuIem
            // 
            this.trueToolStripMenuIem.Name = "trueToolStripMenuIem";
            this.trueToolStripMenuIem.Size = new System.Drawing.Size(100, 22);
            this.trueToolStripMenuIem.Tag = "0";
            this.trueToolStripMenuIem.Text = "True";
            this.trueToolStripMenuIem.Click += new System.EventHandler(this.trueToolStripMenuIem_Click);
            // 
            // falseToolStripMenuItem
            // 
            this.falseToolStripMenuItem.Name = "falseToolStripMenuItem";
            this.falseToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.falseToolStripMenuItem.Tag = "0";
            this.falseToolStripMenuItem.Text = "False";
            this.falseToolStripMenuItem.Click += new System.EventHandler(this.trueToolStripMenuIem_Click);
            // 
            // toolStripMenuItemRelay2
            // 
            this.toolStripMenuItemRelay2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trueToolStripMenuItem1,
            this.falseToolStripMenuItem1});
            this.toolStripMenuItemRelay2.Name = "toolStripMenuItemRelay2";
            this.toolStripMenuItemRelay2.Size = new System.Drawing.Size(111, 22);
            this.toolStripMenuItemRelay2.Text = "Relay 2";
            // 
            // trueToolStripMenuItem1
            // 
            this.trueToolStripMenuItem1.Name = "trueToolStripMenuItem1";
            this.trueToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.trueToolStripMenuItem1.Tag = "1";
            this.trueToolStripMenuItem1.Text = "True";
            this.trueToolStripMenuItem1.Click += new System.EventHandler(this.trueToolStripMenuIem_Click);
            // 
            // falseToolStripMenuItem1
            // 
            this.falseToolStripMenuItem1.Name = "falseToolStripMenuItem1";
            this.falseToolStripMenuItem1.Size = new System.Drawing.Size(100, 22);
            this.falseToolStripMenuItem1.Tag = "1";
            this.falseToolStripMenuItem1.Text = "False";
            this.falseToolStripMenuItem1.Click += new System.EventHandler(this.trueToolStripMenuIem_Click);
            // 
            // toolStripMenuItemRelay3
            // 
            this.toolStripMenuItemRelay3.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trueToolStripMenuItem3,
            this.falseToolStripMenuItem3});
            this.toolStripMenuItemRelay3.Name = "toolStripMenuItemRelay3";
            this.toolStripMenuItemRelay3.Size = new System.Drawing.Size(111, 22);
            this.toolStripMenuItemRelay3.Text = "Relay 3";
            // 
            // trueToolStripMenuItem3
            // 
            this.trueToolStripMenuItem3.Name = "trueToolStripMenuItem3";
            this.trueToolStripMenuItem3.Size = new System.Drawing.Size(100, 22);
            this.trueToolStripMenuItem3.Tag = "2";
            this.trueToolStripMenuItem3.Text = "True";
            this.trueToolStripMenuItem3.Click += new System.EventHandler(this.trueToolStripMenuIem_Click);
            // 
            // falseToolStripMenuItem3
            // 
            this.falseToolStripMenuItem3.Name = "falseToolStripMenuItem3";
            this.falseToolStripMenuItem3.Size = new System.Drawing.Size(100, 22);
            this.falseToolStripMenuItem3.Tag = "2";
            this.falseToolStripMenuItem3.Text = "False";
            this.falseToolStripMenuItem3.Click += new System.EventHandler(this.trueToolStripMenuIem_Click);
            // 
            // toolStripMenuItemRelay4
            // 
            this.toolStripMenuItemRelay4.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.trueToolStripMenuItem2,
            this.falseToolStripMenuItem2});
            this.toolStripMenuItemRelay4.Name = "toolStripMenuItemRelay4";
            this.toolStripMenuItemRelay4.Size = new System.Drawing.Size(111, 22);
            this.toolStripMenuItemRelay4.Text = "Relay 4";
            // 
            // trueToolStripMenuItem2
            // 
            this.trueToolStripMenuItem2.Name = "trueToolStripMenuItem2";
            this.trueToolStripMenuItem2.Size = new System.Drawing.Size(100, 22);
            this.trueToolStripMenuItem2.Tag = "3";
            this.trueToolStripMenuItem2.Text = "True";
            this.trueToolStripMenuItem2.Click += new System.EventHandler(this.trueToolStripMenuIem_Click);
            // 
            // falseToolStripMenuItem2
            // 
            this.falseToolStripMenuItem2.Name = "falseToolStripMenuItem2";
            this.falseToolStripMenuItem2.Size = new System.Drawing.Size(100, 22);
            this.falseToolStripMenuItem2.Tag = "3";
            this.falseToolStripMenuItem2.Text = "False";
            this.falseToolStripMenuItem2.Click += new System.EventHandler(this.trueToolStripMenuIem_Click);
            // 
            // serialToolStripMenuItem
            // 
            this.serialToolStripMenuItem.Name = "serialToolStripMenuItem";
            this.serialToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.serialToolStripMenuItem.Text = "Serial";
            this.serialToolStripMenuItem.Click += new System.EventHandler(this.serialToolStripMenuItem_Click);
            // 
            // lbMessages
            // 
            this.lbMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbMessages.FormattingEnabled = true;
            this.lbMessages.Location = new System.Drawing.Point(385, 95);
            this.lbMessages.Name = "lbMessages";
            this.lbMessages.Size = new System.Drawing.Size(587, 563);
            this.lbMessages.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Devices";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(382, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Messages";
            // 
            // btnTestXML
            // 
            this.btnTestXML.Location = new System.Drawing.Point(385, 22);
            this.btnTestXML.Name = "btnTestXML";
            this.btnTestXML.Size = new System.Drawing.Size(75, 23);
            this.btnTestXML.TabIndex = 7;
            this.btnTestXML.Text = "Test XML";
            this.btnTestXML.UseVisualStyleBackColor = true;
            this.btnTestXML.Click += new System.EventHandler(this.btnTestXML_Click);
            // 
            // btnSerialize
            // 
            this.btnSerialize.Location = new System.Drawing.Point(466, 22);
            this.btnSerialize.Name = "btnSerialize";
            this.btnSerialize.Size = new System.Drawing.Size(75, 23);
            this.btnSerialize.TabIndex = 8;
            this.btnSerialize.Text = "Serialize";
            this.btnSerialize.UseVisualStyleBackColor = true;
            this.btnSerialize.Click += new System.EventHandler(this.btnSerialize_Click);
            // 
            // btnRecursiveTest
            // 
            this.btnRecursiveTest.Location = new System.Drawing.Point(547, 22);
            this.btnRecursiveTest.Name = "btnRecursiveTest";
            this.btnRecursiveTest.Size = new System.Drawing.Size(75, 23);
            this.btnRecursiveTest.TabIndex = 9;
            this.btnRecursiveTest.Text = "RecursiveTest";
            this.btnRecursiveTest.UseVisualStyleBackColor = true;
            this.btnRecursiveTest.Click += new System.EventHandler(this.btnRecursiveTest_Click);
            // 
            // btnTestServer
            // 
            this.btnTestServer.Location = new System.Drawing.Point(628, 22);
            this.btnTestServer.Name = "btnTestServer";
            this.btnTestServer.Size = new System.Drawing.Size(75, 23);
            this.btnTestServer.TabIndex = 10;
            this.btnTestServer.Text = "Server Test";
            this.btnTestServer.UseVisualStyleBackColor = true;
            this.btnTestServer.Click += new System.EventHandler(this.btnTestServer_Click);
            // 
            // btnTestVivotek
            // 
            this.btnTestVivotek.Location = new System.Drawing.Point(709, 22);
            this.btnTestVivotek.Name = "btnTestVivotek";
            this.btnTestVivotek.Size = new System.Drawing.Size(75, 23);
            this.btnTestVivotek.TabIndex = 11;
            this.btnTestVivotek.Text = "Vivotek";
            this.btnTestVivotek.UseVisualStyleBackColor = true;
            this.btnTestVivotek.Click += new System.EventHandler(this.btnTestVivotek_Click);
            // 
            // btnAnimate
            // 
            this.btnAnimate.Location = new System.Drawing.Point(790, 22);
            this.btnAnimate.Name = "btnAnimate";
            this.btnAnimate.Size = new System.Drawing.Size(75, 23);
            this.btnAnimate.TabIndex = 12;
            this.btnAnimate.Text = "Animate";
            this.btnAnimate.UseVisualStyleBackColor = true;
            this.btnAnimate.Click += new System.EventHandler(this.btnAnimate_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(905, 22);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(52, 50);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // btnTestRandom
            // 
            this.btnTestRandom.Location = new System.Drawing.Point(264, 22);
            this.btnTestRandom.Name = "btnTestRandom";
            this.btnTestRandom.Size = new System.Drawing.Size(75, 23);
            this.btnTestRandom.TabIndex = 14;
            this.btnTestRandom.Text = "Test Rand";
            this.btnTestRandom.UseVisualStyleBackColor = true;
            this.btnTestRandom.Click += new System.EventHandler(this.btnTestRandom_Click);
            // 
            // btnXMLSerialize
            // 
            this.btnXMLSerialize.Location = new System.Drawing.Point(466, 51);
            this.btnXMLSerialize.Name = "btnXMLSerialize";
            this.btnXMLSerialize.Size = new System.Drawing.Size(75, 23);
            this.btnXMLSerialize.TabIndex = 15;
            this.btnXMLSerialize.Text = "Serialize 2";
            this.btnXMLSerialize.UseVisualStyleBackColor = true;
            this.btnXMLSerialize.Click += new System.EventHandler(this.btnXMLSerialize_Click);
            // 
            // btnXmlSerialize2
            // 
            this.btnXmlSerialize2.Location = new System.Drawing.Point(547, 51);
            this.btnXmlSerialize2.Name = "btnXmlSerialize2";
            this.btnXmlSerialize2.Size = new System.Drawing.Size(75, 23);
            this.btnXmlSerialize2.TabIndex = 16;
            this.btnXmlSerialize2.Text = "Serialize 3";
            this.btnXmlSerialize2.UseVisualStyleBackColor = true;
            this.btnXmlSerialize2.Click += new System.EventHandler(this.btnXmlSerialize2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(628, 51);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(709, 51);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnReversebits
            // 
            this.btnReversebits.Location = new System.Drawing.Point(791, 52);
            this.btnReversebits.Name = "btnReversebits";
            this.btnReversebits.Size = new System.Drawing.Size(75, 23);
            this.btnReversebits.TabIndex = 19;
            this.btnReversebits.Text = "Reverse bits";
            this.btnReversebits.UseVisualStyleBackColor = true;
            this.btnReversebits.Click += new System.EventHandler(this.btnReversebits_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(385, 51);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 20;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 662);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnReversebits);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnXmlSerialize2);
            this.Controls.Add(this.btnXMLSerialize);
            this.Controls.Add(this.btnTestRandom);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnAnimate);
            this.Controls.Add(this.btnTestVivotek);
            this.Controls.Add(this.btnTestServer);
            this.Controls.Add(this.btnRecursiveTest);
            this.Controls.Add(this.btnSerialize);
            this.Controls.Add(this.btnTestXML);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbMessages);
            this.Controls.Add(this.lbDevices);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.contextMenuStripRelays.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.ListBox lbDevices;
        private System.Windows.Forms.ListBox lbMessages;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripRelays;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRelay1;
        private System.Windows.Forms.ToolStripMenuItem trueToolStripMenuIem;
        private System.Windows.Forms.ToolStripMenuItem falseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRelay2;
        private System.Windows.Forms.ToolStripMenuItem trueToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem falseToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRelay3;
        private System.Windows.Forms.ToolStripMenuItem trueToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem falseToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemRelay4;
        private System.Windows.Forms.ToolStripMenuItem trueToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem falseToolStripMenuItem2;
        private System.Windows.Forms.Button btnTestXML;
        private System.Windows.Forms.Button btnSerialize;
        private System.Windows.Forms.Button btnRecursiveTest;
        private System.Windows.Forms.Button btnTestServer;
        private System.Windows.Forms.ToolStripMenuItem serialToolStripMenuItem;
        private System.Windows.Forms.Button btnTestVivotek;
        private System.Windows.Forms.Button btnAnimate;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnTestRandom;
        private System.Windows.Forms.Button btnXMLSerialize;
        private System.Windows.Forms.Button btnXmlSerialize2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnReversebits;
        private System.Windows.Forms.Button button3;
    }
}

