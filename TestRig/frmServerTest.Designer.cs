﻿namespace TestRig
{
    partial class frmServerTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            if (server != null)
                server.Dispose();

            if (client != null)
                client.Dispose();
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbOutbox = new System.Windows.Forms.ListBox();
            this.lbInbox = new System.Windows.Forms.ListBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.nudItems = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTransfer = new System.Windows.Forms.Button();
            this.btnTCPTransfer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudItems)).BeginInit();
            this.SuspendLayout();
            // 
            // lbOutbox
            // 
            this.lbOutbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbOutbox.FormattingEnabled = true;
            this.lbOutbox.Location = new System.Drawing.Point(12, 60);
            this.lbOutbox.Name = "lbOutbox";
            this.lbOutbox.Size = new System.Drawing.Size(470, 433);
            this.lbOutbox.TabIndex = 0;
            // 
            // lbInbox
            // 
            this.lbInbox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInbox.FormattingEnabled = true;
            this.lbInbox.Location = new System.Drawing.Point(498, 60);
            this.lbInbox.Name = "lbInbox";
            this.lbInbox.Size = new System.Drawing.Size(470, 433);
            this.lbInbox.TabIndex = 1;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(138, 28);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 2;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // nudItems
            // 
            this.nudItems.Location = new System.Drawing.Point(65, 31);
            this.nudItems.Name = "nudItems";
            this.nudItems.Size = new System.Drawing.Size(67, 20);
            this.nudItems.TabIndex = 3;
            this.nudItems.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Items";
            // 
            // btnTransfer
            // 
            this.btnTransfer.Location = new System.Drawing.Point(659, 28);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(75, 23);
            this.btnTransfer.TabIndex = 5;
            this.btnTransfer.Text = "Transfer";
            this.btnTransfer.UseVisualStyleBackColor = true;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // btnTCPTransfer
            // 
            this.btnTCPTransfer.Location = new System.Drawing.Point(740, 28);
            this.btnTCPTransfer.Name = "btnTCPTransfer";
            this.btnTCPTransfer.Size = new System.Drawing.Size(84, 23);
            this.btnTCPTransfer.TabIndex = 6;
            this.btnTCPTransfer.Text = "TCP Transfer";
            this.btnTCPTransfer.UseVisualStyleBackColor = true;
            this.btnTCPTransfer.Click += new System.EventHandler(this.btnTCPTransfer_Click);
            // 
            // frmServerTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 499);
            this.Controls.Add(this.btnTCPTransfer);
            this.Controls.Add(this.btnTransfer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nudItems);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.lbInbox);
            this.Controls.Add(this.lbOutbox);
            this.Name = "frmServerTest";
            this.Text = "frmServerTest";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmServerTest_FormClosing);
            this.Load += new System.EventHandler(this.frmServerTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudItems)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbOutbox;
        private System.Windows.Forms.ListBox lbInbox;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.NumericUpDown nudItems;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnTransfer;
        private System.Windows.Forms.Button btnTCPTransfer;
    }
}