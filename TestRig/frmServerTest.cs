﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlackBoxEngine;
using eNervePluginInterface;
using eNerve.DataTypes;
using eThele.Essentials;

namespace TestRig
{
    public partial class frmServerTest : Form
    {
        private Server server;
        private Client client;

        public frmServerTest()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            lbOutbox.Items.Clear();

            for (int i = 0; i < (int)nudItems.Value; i++)
            {
                if (i % 7 == 0)
                {
                    Alarm alarm = new Alarm(string.Format("TestAlarm{0}", i.ToString()));
                    alarm.Add(new EventTrigger("Detector1", "TestDevice1", DetectorType.Analog, 3, DateTime.Now.AddMinutes(-5), 100));
                    alarm.Add(new EventTrigger("Detector2", "TestDevice1", DetectorType.Digital, 5, DateTime.Now.AddMinutes(-4)));
                    alarm.Add(new EventTrigger("Detector1", "TestDevice2", DetectorType.Digital, 1, DateTime.Now.AddMinutes(-3)));
                    alarm.Actions = new Actions("Condition1");
                    alarm.Actions.Add(new eNerve.DataTypes.Action("DB", true, EventActionType.DB));
                    alarm.Actions.Add(new eNerve.DataTypes.Action("Camera1", true, EventActionType.Camera));
                    lbOutbox.Items.Add(alarm);
                }
                else
                    lbOutbox.Items.Add(new Item(i));
            }
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            List<byte[]> messages = new List<byte[]>();

            int totalBufferSize = 0;

            for(int i = 0; i < lbOutbox.Items.Count; i++)
            {
                MemoryStream stream = new MemoryStream();

                BinaryFormatter binaryFormatter = new BinaryFormatter();
                binaryFormatter.Serialize(stream, lbOutbox.Items[i]);

                byte[] tmpdata = stream.ToArray();

                byte[] msg = new byte[tmpdata.Length + headerSize];
                msg[0] = 0x02;
                byte[] msgID = BitConverter.GetBytes((ushort)1);
                byte[] msgLength = BitConverter.GetBytes(tmpdata.Length);
                Array.Copy(msgID, 0, msg, 1, 2);
                Array.Copy(msgLength, 0, msg, 3, 4);
                Array.Copy(tmpdata, 0, msg, headerSize - 1, tmpdata.Length);
                msg[msg.Length - 1] = 0x03;

                messages.Add(msg);

                totalBufferSize += msg.Length;
            }


            byte[] buffer = new byte[totalBufferSize];
            int bufferIndex = 0;
            for (int i = 0; i < messages.Count; i++)
            {
                Array.Copy(messages[i], 0, buffer, bufferIndex, messages[i].Length);
                bufferIndex += messages[i].Length;
            }

            lbInbox.Items.Clear();

            CheckForMessage(ref buffer);
        }

        /// <summary>
        /// STX + Header + ETX
        /// </summary>
        private const int headerSize = 8;

        /// <summary>
        /// Check if there is a full message available in the buffer. If there is, the message will be removed from the buffer and processed.
        /// </summary>
        /// <param name="_buffer"></param>
        /// <returns>The number of complete messages found and processed</returns>
        private int CheckForMessage(ref byte[] _buffer)
        {
            int result = 0;
            byte[] newBuffer = null;

            if (_buffer.Length > 0)
            {
                if (_buffer[0] == 0x02) // if our data doesn't start with STX, we know something went wrong, ignore the data.
                {
                    int bytesToRemove = 0;

                    if (_buffer.Length >= headerSize)
                    {
                        ushort msgTypeID = BitConverter.ToUInt16(_buffer, 1);
                        int msgLength = BitConverter.ToInt32(_buffer, 3);

                        if (_buffer.Length >= msgLength + headerSize) //we have all the data.
                        {
                            bytesToRemove = msgLength + headerSize;

                            if (_buffer[msgLength + headerSize - 1] == 0x03) //if the last byte is not ETX we know something went wrong, so we should discard the data.
                            {
                                byte[] msg = new byte[msgLength + headerSize];
                                Array.Copy(_buffer, msg, msgLength + headerSize);

                                //Thread processMsgThread = new Thread(new ParameterizedThreadStart(ProcessMessage));
                                //processMsgThread.Start(msg);

                                ProcessMessage(msg);

                                result++;
                            }
                        }
                    }

                    //create a buffer for the left over data.
                    newBuffer = new byte[_buffer.Length - bytesToRemove];
                    Array.Copy(_buffer, bytesToRemove, newBuffer, 0, _buffer.Length - bytesToRemove);

                    if (result > 0) // we will only check for another message if we managed to get 1.
                        result += CheckForMessage(ref newBuffer);
                }
            }

            _buffer = newBuffer;

            return result;
        }

        private void ProcessMessage(object _message)
        {
            try
            {
                if (_message is byte[])
                {
                    byte[] msg = (byte[])_message;

                    if (msg.Length > headerSize)
                    {
                        MemoryStream stream = new MemoryStream(msg, headerSize - 1, msg.Length - headerSize);

                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        Item newItem = (Item)binaryFormatter.Deserialize(stream);

                        lbInbox.Items.Add(newItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void frmServerTest_Load(object sender, EventArgs e)
        {
            server = new Server(4050);
            client = new Client();
            client.Connect("127.0.0.1", 4050);
            client.OnObjectMessageReceived += client_OnObjectMessageReceived;
        }

        private void frmServerTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (client != null)
                client.Dispose();

            if (server != null)
                server.Dispose();
        }

        private void ObjectMessageReceived(object sender, TcpClient client, MessageType messageType, object message)
        {
            if (sender is ListBox)
            {
                ((ListBox)sender).Items.Add(message);
            }
        }

        private void client_OnObjectMessageReceived(object sender, CommsNode.ObjectMessageEventArgs e)
        {
            if (lbInbox.InvokeRequired)
            {
                this.Invoke((MethodInvoker)delegate() { client_OnObjectMessageReceived(sender, e); });

                //if(objMessageEH == null)
                //    objMessageEH = new CommsNode.ObjectMessageEventHandler(ObjectMessageReceived);

                //lbInbox.Invoke(objMessageEH, new object[] { lbInbox, messageType, message });
            }
            else
                lbInbox.Items.Add((Item)e.Message);
        }

        private void btnTCPTransfer_Click(object sender, EventArgs e)
        {
            lbInbox.Items.Clear();

            if (server != null)
            {
                for (int i = 0; i < lbOutbox.Items.Count; i++)
                {
                    server.SendMessage(MessageType.Alarm, lbOutbox.Items[i]);
                }
            }
        }
    }

    [Serializable]
    public class Item
    {
        private int id;
        private string name;
        private DateTime timeCreated;

        public Item(int _id)
        {
            id = _id;
            name = string.Format("Item - {0}", _id.ToString());
            timeCreated = DateTime.Now;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public DateTime TimeCreated
        {
            get { return timeCreated; }
            set { timeCreated = value; }
        }

        public override string ToString()
        {
            return string.Format("Item: {0}, {1}, {2}", id.ToString(), name, timeCreated.ToString());
        }
    }
}
