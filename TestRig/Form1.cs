﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Windows.Forms;
using BlackBoxAPI;
using BlackBoxEngine;
using eNervePluginInterface;
using eThele.Essentials;
using eNerve.DataTypes;
using System.Diagnostics;
using System.Threading;
using eTheleSoundPlayer;

namespace TestRig
{
    public partial class Form1 : Form
    {
        BlackBoxManager blackBoxManager = null;
        SoundPlayer soundPlayer;
                
        public Form1()
        {
            InitializeComponent();

            //blackBoxManager = new BlackBoxManager();
            //blackBoxManager.OnDeviceAdded += new DeviceEventHandler(xTendManager_OnDeviceAdded);
            //blackBoxManager.OnRemovingDevice += new DeviceEventHandler(xTendManager_OnRemovingDevice);
            //blackBoxManager.OnCardInputEvent += new CardInputEvent(xTendManager_OnCardInputEvent);
            //blackBoxManager.OnCardInputChange += new CardInputChange(xTendManager_OnCardInputChange);
            //blackBoxManager.OnCardSerialDataReceive += new CardSerialDataReceive(xTendManager_OnCardSerialDataReceive);
            //blackBoxManager.OnCardSerialEvent += new CardSerialEvent(xTendManager_OnCardSerialEvent);
            //blackBoxManager.OnCardSerialError += new CardSerialError(xTendManager_OnCardSerialError);

            soundPlayer = new SoundPlayer();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (blackBoxManager != null)
            {
                blackBoxManager.Stop();
                blackBoxManager.Dispose();
            }
            blackBoxManager = null;

            if (soundPlayer != null) 
            {
                soundPlayer.Dispose();
                soundPlayer = null;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (blackBoxManager == null)
            {
                blackBoxManager = new BlackBoxManager();
                blackBoxManager.OnDeviceAdded += new DeviceEventHandler(xTendManager_OnDeviceAdded);
                blackBoxManager.OnRemovingDevice += new DeviceEventHandler(xTendManager_OnRemovingDevice);
                blackBoxManager.OnCardInputEvent += new CardInputEvent(xTendManager_OnCardInputEvent);
                blackBoxManager.OnCardInputChange += new CardInputChange(xTendManager_OnCardInputChange);
                blackBoxManager.OnCardSerialDataReceive += new CardSerialDataReceive(xTendManager_OnCardSerialDataReceive);
                blackBoxManager.OnCardSerialEvent += new CardSerialEvent(xTendManager_OnCardSerialEvent);
                blackBoxManager.OnCardSerialError += new CardSerialError(xTendManager_OnCardSerialError);
            }

            blackBoxManager.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (blackBoxManager != null)
                blackBoxManager.Stop();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            blackBoxManager.Reset();
        }

        private void xTendManager_OnDeviceAdded(Device _device)
        {
            if (lbDevices.InvokeRequired)
                lbDevices.Invoke(new DeviceEventHandler(AddDeviceNode), new object[] { _device });
            else
                AddDeviceNode(_device);
        }

        public void AddDeviceNode(Device _device)
        {
            lbDevices.Items.Add(_device);
        }

        void xTendManager_OnRemovingDevice(Device _device)
        {
            
            if (lbDevices.InvokeRequired)
                lbDevices.Invoke(new DeviceEventHandler(RemoveDeviceNode), new object[] { _device });
            else
                RemoveDeviceNode(_device);
        }

        public void RemoveDeviceNode(Device _device)
        {
            lbDevices.Items.Remove(_device);
        }

        void xTendManager_OnCardInputEvent(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, InputConditions _eventKind, bool _status)
        {
            string msg = string.Format("Device: {0}, Card: {1}, Card Type: {2}, Pin: {3}, Event: {4}, State: {5}", _device.ToString(), _slotIndex, _elementKind.ToString(), _elementIndex, _eventKind.ToString(), _status.ToString());

            if (lbMessages.InvokeRequired)
                lbMessages.Invoke(new MessageEventHandler(InsertMessageNode), new object[] { msg });
            else
                InsertMessageNode(msg);
        }

        void xTendManager_OnCardInputChange(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, float _value)
        {
            string msg = string.Format("Device: {0}, Card: {1}, Card Type: {2}, Pin: {3}, Value: {4}", _device.ToString(), _slotIndex, _elementKind.ToString(), _elementIndex, _value.ToString());

            if (lbMessages.InvokeRequired)
                lbMessages.Invoke(new MessageEventHandler(InsertMessageNode), new object[] { msg });
            else
                InsertMessageNode(msg);
        }

        void xTendManager_OnCardSerialDataReceive(Device _device, int _slotIndex, byte[] _dataArray)
        {
            string msg = string.Format("Device: {0}, Card: {1}, Data: {2}", _device.ToString(), _slotIndex, _dataArray.ToString());

            if (lbMessages.InvokeRequired)
                lbMessages.Invoke(new MessageEventHandler(InsertMessageNode), new object[] { msg });
            else
                InsertMessageNode(msg);
        }

        void xTendManager_OnCardSerialEvent(Device _device, int _slotIndex, SerialPortConditions _event, bool _status)
        {
            string msg = string.Format("Device: {0}, Card: {1}, Event: {2}, Status: {3}", _device.ToString(), _slotIndex, _event.ToString(), _status.ToString());

            if (lbMessages.InvokeRequired)
                lbMessages.Invoke(new MessageEventHandler(InsertMessageNode), new object[] { msg });
            else
                InsertMessageNode(msg);
        }

        void xTendManager_OnCardSerialError(Device _device, int _slotIndex, string _error)
        {
            string msg = string.Format("Device: {0}, Card: {1}, Error: {2}", _device.ToString(), _slotIndex, _error);

            if (lbMessages.InvokeRequired)
                lbMessages.Invoke(new MessageEventHandler(InsertMessageNode), new object[] { msg });
            else
                InsertMessageNode(msg);
        }

        public delegate void MessageEventHandler(string _message);
        public void InsertMessageNode(string _message)
        {
            lbMessages.Items.Insert(0, _message);
        }

        private void trueToolStripMenuIem_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbDevices.SelectedItem is Device)
                {
                    Device selectedDevice = (Device)lbDevices.SelectedItem;

                    if (sender is ToolStripMenuItem)
                    {
                        selectedDevice.SetRelayState(int.Parse(((ToolStripMenuItem)sender).Tag.ToString()), bool.Parse(((ToolStripMenuItem)sender).Text.ToLower()));
                    }
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        
        private void btnTestXML_Click(object sender, EventArgs e)
        {
            XMLDoc xmlDoc = null;
            OpenFileDialog ofd = null;
            SaveFileDialog sfd = null;
            try
            {
                xmlDoc = new XMLDoc();

                ofd = new OpenFileDialog();
                ofd.InitialDirectory = @"C:\Users\Herman\Dev\BlackBox\XML";
                if(ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    xmlDoc.Load(ofd.FileName);

                    sfd = new SaveFileDialog();
                    sfd.InitialDirectory = @"C:\Users\Herman\Dev\BlackBox\XML";
                    if(sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        xmlDoc.Save(sfd.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                if (xmlDoc != null)
                    xmlDoc.Dispose();

                if (ofd != null)
                    ofd.Dispose();

                if (sfd != null)
                    sfd.Dispose();
            }            
        }

        private void btnSerialize_Click(object sender, EventArgs e)
        {
            Alarm alarm = new Alarm("TestAlarm");
            alarm.Add(new EventTrigger("Detector1", "TestDevice1", DetectorType.Analog, 3, DateTime.Now.AddMinutes(-5), 100F));
            alarm.Add(new EventTrigger("Detector2", "TestDevice1", DetectorType.Digital, 5, DateTime.Now.AddMinutes(-4)));
            alarm.Add(new EventTrigger("Detector1", "TestDevice2", DetectorType.Digital, 1, DateTime.Now.AddMinutes(-3)));
            alarm.Actions = new Actions("Condition1");
            alarm.Actions.Add(new eNerve.DataTypes.Action("DB", true, EventActionType.DB));
            alarm.Actions.Add(new eNerve.DataTypes.Action("Camera1", true, EventActionType.Camera));

            MemoryStream stream = new MemoryStream();

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, alarm);

            stream.Position = 0;

            Alarm alarm2 = (Alarm)binaryFormatter.Deserialize(stream);
        }

        private void btnRecursiveTest_Click(object sender, EventArgs e)
        {
            int val = 100;
            int result = recursiveTest(ref val);
        }

        private int recursiveTest(ref int val)
        {
            int result = 0;

            if (val > 23)
            {
                val -= 23;
                result++;
            }

            if (result > 0)
                result += recursiveTest(ref val);

            return result;
        }

        private void btnTestServer_Click(object sender, EventArgs e)
        {
            using (frmServerTest testServerForm = new frmServerTest())
            {
                testServerForm.ShowDialog();
            }
        }

        private void serialToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                if (lbDevices.SelectedItem is Device)
                {
                    Device selectedDevice = (Device)lbDevices.SelectedItem;

                    //selectedDevice.TransmitSerialData("test");
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
        }

        private void btnTestVivotek_Click(object sender, EventArgs e)
        {
            WebClient webClient = null;
            VivotekXMLPreCurrPosSnapshots vivotekXMLPreCurrPosSnapshotsDoc = null;

            try
            {
                webClient = new WebClient();
                string data = webClient.DownloadString(@"http://192.168.1.170/cgi-bin/admin/lsctrl.cgi?cmd=search&triggerType='di'&triggerTime='2013-07-16%2010:37:00'+TO+'2013-07-16%2011:00:00'");

                vivotekXMLPreCurrPosSnapshotsDoc = new VivotekXMLPreCurrPosSnapshots();
                if (vivotekXMLPreCurrPosSnapshotsDoc.LoadFromString(data))
                {
                    List<string> paths = vivotekXMLPreCurrPosSnapshotsDoc.GetImagePaths();

                    foreach (string path in paths)
                    {
                        string a = path.Remove(0, "/mnt/auto/CF/".Length);

                        EventImage img = new EventImage(Guid.NewGuid());
                        img.ImageData = webClient.DownloadData(@"http://192.168.1.170/" + a);
                        img.TimeStamp = DateTime.Now;
                    }

                    paths.Clear();
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
            }
            finally
            {
                if(webClient != null)
                    webClient.Dispose();

                if (vivotekXMLPreCurrPosSnapshotsDoc != null)
                    vivotekXMLPreCurrPosSnapshotsDoc.Dispose();
            }
        }

        private void btnAnimate_Click(object sender, EventArgs e)
        {
            
        }

        private void btnTestRandom_Click(object sender, EventArgs e)
        {
            MessageBox.Show(RandomInterval(20,10).ToString());
        }

        private int RandomInterval(int _maximum, int _minimum = 0)
        {
            int result = _maximum;
            Random random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            result = random.Next(Math.Abs(_maximum - _minimum)) + (_minimum < _maximum ? _minimum : _maximum);
            return result;
        }

        private void btnXMLSerialize_Click(object sender, EventArgs e)
        {
            GroupAlarm groupAlarm = new GroupAlarm() { Name = "TestGroup", Alarm_Type = AlarmType.Test, EscalationLevel = EscalationLevel.Yellow, AlarmCount = 4, AlarmDT = DateTime.Now.AddMinutes(-90), LastAlarmDT = DateTime.Now, Priority = 2 };

            groupAlarm.Resolve(new AlarmResolve() { AlarmID = groupAlarm.ID, Description = "Resolved", Alarm_Type = AlarmType.Nuisance, Name = "TestGroup", ResolveDT = DateTime.Now, ResolveID = Guid.NewGuid(), User = new User("Herman") });
            groupAlarm.Resolve(new AlarmResolve() { AlarmID = groupAlarm.ID, Description = "ResolvedAgain", Alarm_Type = AlarmType.Nuisance, Name = "TestGroup", ResolveDT = DateTime.Now, ResolveID = Guid.NewGuid(), User = new User("SomeOtherGuy") });
            
            string serializedGroupAlarm = XMLSerializer.Serialize(groupAlarm);

            //Test t = new Test() { Field1 = "Test1", Field2 = 1, Field3 = Test.TestEnum.TesVal1, Field6 = new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now } };
            //t.Field4.Add(new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now });
            //t.Field4.Add(new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now });

            //t.Field5.Enqueue(new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now });
            //t.Field5.Enqueue(new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now });
            //t.Field5.Enqueue(new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now });

            //string serializedGroupAlarm = XMLSerializer.Serialize(t);

            //MemoryStream stream = new MemoryStream();
            //BinaryFormatter binaryFormatter = new BinaryFormatter();
            //binaryFormatter.Serialize(stream, t);

            //byte[] objbuffer = stream.ToArray();

            //int l1 = objbuffer.Length;
            int l2 = serializedGroupAlarm.Length;

            int l3 = Encoding.UTF8.GetBytes(serializedGroupAlarm).Length;

            object deserializedObj = XMLSerializer.Deserialize(serializedGroupAlarm);
            string serializedGroupAlarm2 = XMLSerializer.Serialize(deserializedObj);

            int l4 = serializedGroupAlarm2.Length;


            byte[] objbuffer = Encoding.UTF8.GetBytes(serializedGroupAlarm);
            byte[] objbufferCompressed = Compression.Compress(objbuffer);

            byte[] objbufferDecompressed = Compression.Decompress(objbufferCompressed);
                       

            string serializedGroupAlarmDecompressed = Encoding.UTF8.GetString(objbufferDecompressed);
            deserializedObj = XMLSerializer.Deserialize(serializedGroupAlarmDecompressed);

            l4 = serializedGroupAlarm2.Length;

            
        }

        private void btnXmlSerialize2_Click(object sender, EventArgs e)
        {
            Test t = new Test() { Field1 = "Test1", Field2 = 1, Field3 = Test.TestEnum.TesVal1, Field6 = new Test4(34) { ID = Guid.NewGuid(), DT = DateTime.Now } };
            t.Field4.Add(new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now });
            t.Field4.Add(new Test3() { ID = Guid.NewGuid(), DT = DateTime.Now });

            t.Field5.Enqueue(new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now });
            t.Field5.Enqueue(new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now });
            t.Field5.Enqueue(new Test2() { ID = Guid.NewGuid(), DT = DateTime.Now });

            string s1 = XMLSerializer.Serialize(t);
            object o1 = XMLSerializer.Deserialize(s1);
            string s2 = XMLSerializer.Serialize(o1);

            //DevicesDoc deviceDoc = new DevicesDoc();
            //deviceDoc.Load(@"C:\Users\Herman\Dev\BlackBox\TestRigServer\bin\Debug\Devices.xml");

            //t.MyDevices = deviceDoc;

            //string serializedGroupAlarm = XMLSerializer.Serialize(t);

            //string testList = XMLSerializer.Serialize(t.Field4);

            //object obj = XMLSerializer.Deserialise(serializedGroupAlarm);
            //object obj2 = XMLSerializer.Deserialise(testList);

            //string serializedGroupAlarm2 = XMLSerializer.Serialize(obj);

            //List<Schedule> schedule = ScheduleDoc.TheeSchedule.Schedule;
            //string ss = XMLSerializer.Serialize(schedule);
            //object obj = XMLSerializer.Deserialise(ss);
            //string ss2 = XMLSerializer.Serialize(obj);

            //MapDoc map = new MapDoc("Maps");
            //map.Load();
            //XMLDoc doc = new XMLDoc("AlarmConditions");
            //doc.Load();

            //string ss = XMLSerializer.Serialize(doc);
            //object obj = XMLSerializer.Deserialise(ss);
            //string ss2 = XMLSerializer.Serialize(obj);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TimeSpan maxRebootFreq = TimeSpan.FromMinutes(20);
            DateTime lastReboot = DateTime.MinValue;
            bool bla = false;
            if(DateTime.Now - lastReboot > maxRebootFreq)
                bla = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string data = @"AAEAAAD/////AQAAAAAAAAAMAgAAAF5DTkwuSVBTZWN1cml0eUNlbnRlci5Eb21haW4sIFZlcnNpb249NC45LjMuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1jYzM2ZjgzMjQ3ZWMwMmE3BQEAAAA9Q05MLklQU2VjdXJpdHlDZW50ZXIuRG9tYWluLlJlc3BvbnNlUGxhbi5TdGVwcy5HZW5lcmFsLlNjcmlwdAMAAAAsQmFzZVNjcmlwdFN0ZXArPEluc3RydWN0aW9ucz5rX19CYWNraW5nRmllbGQmQmFzZVNjcmlwdFN0ZXArPFNjcmlwdD5rX19CYWNraW5nRmllbGQgQmFzZVN0ZXArPFJvdXRlcz5rX19CYWNraW5nRmllbGQDAQPDAVN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLkxpc3RgMVtbQ05MLklQU2VjdXJpdHlDZW50ZXIuRG9tYWluLlJlc3BvbnNlUGxhbi5TY3JpcHRpbmcuSW5zdHJ1Y3Rpb24sIENOTC5JUFNlY3VyaXR5Q2VudGVyLkRvbWFpbiwgVmVyc2lvbj00LjkuMy4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWNjMzZmODMyNDdlYzAyYTddXX5TeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYy5MaXN0YDFbW1N5c3RlbS5JbnQzMiwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0CAAAACQMAAAAGBAAAAF1NeS5QYWdlVmFyaWFibGVzLk1hcF9HVUkuc2hhcnBNYXBHSVNNYXAxLkdvdG9Mb2NhdGlvbihNeS5QYWdlVmFyaWFibGVzLlNlbGVjdGVkX0xvY2F0aW9uKQAAAAAJBQAAAAQDAAAAwwFTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYy5MaXN0YDFbW0NOTC5JUFNlY3VyaXR5Q2VudGVyLkRvbWFpbi5SZXNwb25zZVBsYW4uU2NyaXB0aW5nLkluc3RydWN0aW9uLCBDTkwuSVBTZWN1cml0eUNlbnRlci5Eb21haW4sIFZlcnNpb249NC45LjMuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1jYzM2ZjgzMjQ3ZWMwMmE3XV0DAAAABl9pdGVtcwVfc2l6ZQhfdmVyc2lvbgQAAEBDTkwuSVBTZWN1cml0eUNlbnRlci5Eb21haW4uUmVzcG9uc2VQbGFuLlNjcmlwdGluZy5JbnN0cnVjdGlvbltdAgAAAAgICQYAAAAEAAAABAAAAAQFAAAAflN5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLkxpc3RgMVtbU3lzdGVtLkludDMyLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldXQMAAAAGX2l0ZW1zBV9zaXplCF92ZXJzaW9uBwAACAgICQcAAAABAAAAAQAAAAcGAAAAAAEAAAAEAAAABD5DTkwuSVBTZWN1cml0eUNlbnRlci5Eb21haW4uUmVzcG9uc2VQbGFuLlNjcmlwdGluZy5JbnN0cnVjdGlvbgIAAAAJCAAAAAkJAAAACQoAAAAJCwAAAA8HAAAABAAAAAgDAAAAAAAAAAAAAAAAAAAABQgAAAA+Q05MLklQU2VjdXJpdHlDZW50ZXIuRG9tYWluLlJlc3BvbnNlUGxhbi5TY3JpcHRpbmcuSW5zdHJ1Y3Rpb24CAAAAHjxPcGVyYXRpb25Db2RlPmtfX0JhY2tpbmdGaWVsZBg8T3BlcmFuZD5rX19CYWNraW5nRmllbGQEAkBDTkwuSVBTZWN1cml0eUNlbnRlci5Eb21haW4uUmVzcG9uc2VQbGFuLlNjcmlwdGluZy5PcGVyYXRpb25Db2RlAgAAAAIAAAAF9P///0BDTkwuSVBTZWN1cml0eUNlbnRlci5Eb21haW4uUmVzcG9uc2VQbGFuLlNjcmlwdGluZy5PcGVyYXRpb25Db2RlAQAAAAd2YWx1ZV9fAAgCAAAAAAAAAAkNAAAAAQkAAAAIAAAAAfL////0////AAAAAAkPAAAADBAAAABYQ05MLklQU2VjdXJpdHlDZW50ZXIuU2NyaXB0RW5naW5lLCBWZXJzaW9uPTQuOS4zLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49bnVsbAEKAAAACAAAAAHv////9P///y8AAAAJEgAAAAELAAAACAAAAAHt////9P///x4AAAAKBA0AAAALU3lzdGVtLkd1aWQLAAAAAl9hAl9iAl9jAl9kAl9lAl9mAl9nAl9oAl9pAl9qAl9rAAAAAAAAAAAAAAAIBwcCAgICAgICAvxdAQs52mlVkFjS2QxnbkIBDwAAAA0AAACclnGp78WQVLN6vsA2xmo9BRIAAAAyQ05MLklQU2VjdXJpdHlDZW50ZXIuU2NyaXB0RW5naW5lLk1ldGhvZENhbGxIZWxwZXIFAAAAD19PYmplY3RUeXBlTmFtZQtfTWV0aG9kTmFtZRNfTnVtYmVyT2ZQYXJhbWV0ZXJzC19WYXJpYWJsZUlkDF9Db250cm9sTmFtZQEBAAMBCAtTeXN0ZW0uR3VpZBAAAAAGFAAAADBDTkwuSVBTZWN1cml0eUNlbnRlci5HSVMuU2hhcnBNYXAuU2hhcnBNYXBHSVNNYXAGFQAAAAxHb3RvTG9jYXRpb24BAAAAAer///8NAAAA/F0BCznaaVWQWNLZDGduQgYXAAAAD3NoYXJwTWFwR0lTTWFwMQs=";

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            byte[] databuffer = Convert.FromBase64String(data);
            MemoryStream dataStream = new MemoryStream(databuffer);
            object obj = binaryFormatter.Deserialize(dataStream);
            obj = obj;
        }

        private void btnReversebits_Click(object sender, EventArgs e)
        {
            byte b = 30; // reverse this byte

            Stopwatch sw = Stopwatch.StartNew();

            Debug.WriteLine(string.Format("Initialise: {0} - {1}ticks", b, sw.Elapsed.Ticks));

            sw.Restart();

            for (int i = 0; i < 501; i++ )
                b = (byte)(((b * 0x80200802ul) & 0x0884422110ul) * 0x0101010101ul >> 32);

            Debug.WriteLine(string.Format("4 operations: {0} - {1}ticks", b, sw.Elapsed.Ticks));

            sw.Restart();

            for (int i = 0; i < 501; i++)
                b = (byte)((b * 0x0202020202ul & 0x010884422010ul) % 1023);

            Debug.WriteLine(string.Format("3 operations: {0} - {1}ticks", b, sw.Elapsed.Ticks));

            ushort regVal = 30;
            Debug.WriteLine(string.Format("4 operations: {0}", regVal));

            regVal = (ushort)((((((ulong)((regVal & 0xFF00) >> 8) * 0x80200802ul) & 0x0884422110ul) * 0x0101010101ul >> 32) & 0x00FF)
                                            + ((((((ulong)(regVal & 0x00FF) * 0x80200802ul) & 0x0884422110ul) * 0x0101010101ul >> 32) << 8) & 0xFF00));

            Debug.WriteLine(string.Format("4 operations: {0}", regVal));

            regVal = (ushort)((((((ulong)((regVal & 0xFF00) >> 8) * 0x80200802ul) & 0x0884422110ul) * 0x0101010101ul >> 32) & 0x00FF)
                                            + ((((((ulong)(regVal & 0x00FF) * 0x80200802ul) & 0x0884422110ul) * 0x0101010101ul >> 32) << 8) & 0xFF00));

            Debug.WriteLine(string.Format("4 operations: {0}", regVal));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.PlaySound(@"C:\Git\BlackBox\WPF_UI\bin\Debug\Sounds\Drum.mp3", 1.8);
            

            //using (SoundPlayer player = new SoundPlayer(@"C:\Git\BlackBox\WPF_UI\bin\Debug\Sounds\Red Alert-SoundBible.com-108009997.mp3"))
            //{
            //    player.Play();
            //    Thread.Sleep(1000);
            //    player.Stop();
            //}
        }
    }
}
