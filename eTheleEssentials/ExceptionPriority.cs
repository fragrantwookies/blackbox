using System;
using System.Collections.Generic;

namespace eThele.Essentials
{
    public enum ExceptionPriority
    {
        Low,
        Medium,
        High
    }
}
