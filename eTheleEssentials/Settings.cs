﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace eThele.Essentials
{
    public class Settings : XMLDoc
    {
        private static Settings theeSettings;

        public Settings(string _name = "")
            : base(_name)
        {}

        public static Settings TheeSettings
        {
            get
            {
                if (theeSettings == null)
                {
                    theeSettings = new Settings("Settings");

                    string filename = string.Format("{0}\\Ethele\\RioSoft\\{1}\\Config\\Settings.xml", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), Assembly.GetEntryAssembly().GetName().Name);

                    if (System.IO.File.Exists(filename))
                        theeSettings.Load(filename);
                    else
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Can't find settings file: {0}", filename));

                    //if ((!System.IO.File.Exists(theeSettings.FileName)) || (!theeSettings.Load()))
                    //{
                    //    theeSettings.Load(string.Format("{0}\\Ethele\\{1}\\Settings.xml", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), 
                    //        Assembly.GetEntryAssembly().GetName().Name));
                    //}
                }

                return theeSettings;
            }
        }

        public static void Selfdesctruct()
        {
            if (theeSettings != null)
            {
                theeSettings.Dispose();
                theeSettings = null;
            }
        }
    }
}
