﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;

namespace eThele.Essentials
{
    public class XMLSerializer
    {
        public static string Serialize(object obj)
        {
            return Serialize(obj, false);
        }

        public static void SerializeToFile(object obj, string filePath = "")
        {
            Serialize(obj, true, filePath);
        }

        private static string Serialize(object obj, bool saveToFile, string filePath = "")
        {
            string result = "";

            try
            {
                if (obj != null)
                {                    
                    using (XMLDoc doc = new XMLDoc("Object") { UniqueChildNodes = false })
                    {
                        XmlNode headerNode = doc.Add("InfoHeader", "");
                        headerNode.Add("Serialized", DateTime.Now);
                        XmlNode aqnNode = headerNode.Add("AQN", "");

                        Dictionary<string, string> aqnDictionary = new Dictionary<string, string>();
                        XmlNode dataNode = doc.Add("Object", "");
                        Serialize(obj, ref dataNode, aqnDictionary);

                        foreach (KeyValuePair<string, string> item in aqnDictionary)
                        {
                            XmlNode itemNode = aqnNode.Add("Type", "");
                            itemNode.AddAttribute("Name", item.Key);
                            itemNode.AddAttribute("AQN", item.Value);
                        }

                        if (saveToFile)
                            doc.Save(filePath);
                        else
                            result = doc.SaveToString();
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private static void Serialize(object obj, ref XmlNode node, Dictionary<string, string> aqnDictionary)
        {
            try
            {
                if (obj != null)
                {
                    Type objType = obj.GetType();
                    node.AddAttribute("Type", AddTypeToDictionary(objType, aqnDictionary));                   
                    
                    if(objType.ImplementsIEnumerable() && objType != typeof(string) && !objType.IsXMLNode())
                    {   
                        IEnumerable list = (IEnumerable)obj;
                        if (list != null)
                        {
                            lock (list)
                            {
                                foreach (object listItem in list)
                                {
                                    XmlNode itemNode = node.Add("Item", "");
                                    if (itemNode != null)
                                    {
                                        Serialize(listItem, ref itemNode, aqnDictionary);
                                    }
                                }
                            }
                        }
                    }
                    else if (objType.IsClass && !(obj is string))
                    {
                        foreach (FieldInfo fieldInfo in objType.GetAllFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
                        {
                            if (!fieldInfo.IsStatic && !fieldInfo.IsNotSerialized && !fieldInfo.IsLiteral && !fieldInfo.IsNotSerializedXML())
                            {
                                XmlNode fieldNode = node.Add("Field", "");
                                fieldNode.AddAttribute("Name", fieldInfo.Name);
                                Serialize(fieldInfo.GetValue(obj), ref fieldNode, aqnDictionary);
                            }
                        }

                        if (objType.IsXMLDoc())
                        {
                            node.Add("XML", string.Format("<!--{0}-->", ((XMLDoc)obj).SaveToString()));
                            //node.Add("XML", "").Add(ObjectCopier.Clone<XmlNode>((XmlNode)obj));
                        }
                    }
                    else if (objType == typeof(DateTime))
                    {
                        DateTime val = (DateTime)obj;
                        node.SetValueFromDate(val);
                        node.AddAttribute("Type", AddTypeToDictionary(objType, aqnDictionary));
                    }
                    else if(objType.IsGenericType && objType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>))
                    {
                        Type keyType = objType.GetProperty("Key").PropertyType;
                        Type valType = objType.GetProperty("Value").PropertyType;

                        object key = objType.GetProperty("Key").GetValue(obj);
                        object val = objType.GetProperty("Value").GetValue(obj);

                        node.Value = string.Format("[{0}, {1}]", key is DateTime ? ((DateTime)key).ToUniversalTime().ToString("o", DateTimeFormatInfo.InvariantInfo) : key.ToString(), val is DateTime ? ((DateTime)val).ToUniversalTime().ToString("o", DateTimeFormatInfo.InvariantInfo) : val.ToString());
                        node.AddAttribute("Type", AddTypeToDictionary(objType, aqnDictionary));
                    }
                    else
                    {
                        node.Value = obj.ToString();
                        node.AddAttribute("Type", AddTypeToDictionary(objType, aqnDictionary));
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }        

        /// <summary>
        /// Checks if type is in dictionary, if not, it will be added.
        /// </summary>
        /// <returns>Type's full name</returns>
        private static string AddTypeToDictionary(Type type, Dictionary<string, string> aqnDictionary)
        {
            if (!aqnDictionary.ContainsKey(type.FullName))
                aqnDictionary.Add(type.FullName, type.AssemblyQualifiedName);

            return type.FullName;
        }

        private static string LookupAQN(string typeName, Dictionary<string, string> aqnDictionary)
        {
            if (aqnDictionary.ContainsKey(typeName))
                return aqnDictionary[typeName];
            else
                return "";
        }

        /// <summary>
        /// Reconstruct object from xml data buffer
        /// </summary>
        /// <param name="buffer">serialized data</param>
        /// <param name="typePath">In thee case of types loaded in runtime (plugins for example) type info won't be found unless you specify where the data types are defined.
        /// Use this parameter to specify the file name where the type is defined, or just a directory that contains the dll (plugins directory for example).
        /// Defaults to Plugins sub directory of the executing assembly path</param>
        /// <returns>Deserialized object</returns>
        public static object Deserialize(string buffer, string typePath = "")
        {
            object result = null;

            try
            {
                using (XMLDoc doc = new XMLDoc() { UniqueChildNodes = false })
                {
                    doc.LoadFromString(buffer);

                    Dictionary<string, string> aqnDictionary = new Dictionary<string, string>();

                    foreach (XmlNode typeNode in doc["InfoHeader"]["AQN"])
                        aqnDictionary.Add(typeNode.Attribute("Name").Value, typeNode.Attribute("AQN").Value);

                    result = Deserialize(doc[doc.Name], aqnDictionary, typePath);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private static Type FindType(XmlNode _node, Dictionary<string, string> aqnDictionary, string typePath)
        {
            Type objType = null;
            string aqn = "";
            objType = Type.GetType(aqn = LookupAQN(_node.Attribute("Type").Value, aqnDictionary));

            if (objType == null)
            {
                if (string.IsNullOrEmpty(typePath))
                    typePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Ethele\\RioSoft\\Plugins"); //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).TrimEnd('\\') + @"\Plugins\";

                if (File.Exists(typePath))
                {
                    objType = Type.GetType(aqn,
                           (aName) => Assembly.LoadFrom(typePath),
                           (assem, name, ignore) => assem == null ? Type.GetType(name, false, ignore) : assem.GetType(name, false, ignore),
                           true);
                }
                else if (Directory.Exists(typePath))
                {
                    bool found = false;
                    foreach (string fileName in Directory.GetFiles(typePath))
                    {
                        try
                        {
                            Assembly assembly = Assembly.LoadFrom(fileName);

                            foreach (Type type in assembly.GetTypes())
                            {
                                if (aqn.Contains(type.Name))
                                {
                                    try
                                    {
                                        objType = Type.GetType(aqn,
                                            (aName) => aName.Name == assembly.GetName().Name ? Assembly.LoadFrom(fileName) : Assembly.Load(aName),
                                           (assem, name, ignore) => assem == null ? Type.GetType(name, false, ignore) : assem.GetType(name, false, ignore),
                                           true);
                                    }
                                    catch (Exception)
                                    {
                                        /// Buried this exception as it causes a lot of false alarms.
                                        //Exceptions.ExceptionsManager.Add(string.Format("{0}, {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Error loading type", ex);
                                        objType = null;
                                    }

                                    if (objType != null)
                                    {
                                        found = true;
                                        break;
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.ExceptionsManager.Add(string.Format("{0}, {1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                                "Error loading assembly", ex);
                        }

                        if (found)
                            break;
                    }
                }
            }

            return objType;
        }

        private static object Deserialize(XmlNode _node, Dictionary<string, string> aqnDictionary, string typePath, Type _type = null)
        {
            object result = null;

            try
            {
                if (_node != null)
                {
                    if (_node.Attribute("Type") != null || _type != null)
                    {
                        Type objType = _type ?? FindType(_node, aqnDictionary, typePath);

                        if (objType == null)
                        {
                            string aqn = LookupAQN(_node.Attribute("Type").Value, aqnDictionary);
                            throw new ArgumentNullException("objType", string.Format("Could not recreate type: {0}", aqn));
                        }

                        if (!(Nullable.GetUnderlyingType(objType) != null && _node.Value == "" && _node.Count == 0))
                        {
                            if (objType.IsEnum)
                            {
                                string enumVal = XmlNode.ParseString(_node, "");

                                if (!string.IsNullOrWhiteSpace(enumVal))
                                    result = Enum.Parse(objType, enumVal);
                            }
                            else if (objType == typeof(bool))
                                result = XmlNode.ParseBool(_node, false);
                            else if (objType == typeof(byte))
                                result = byte.Parse(XmlNode.ParseString(_node, ""));
                            else if (objType == typeof(sbyte))
                                result = sbyte.Parse(XmlNode.ParseString(_node, ""));
                            else if (objType == typeof(Int16))
                                result = Int16.Parse(XmlNode.ParseString(_node, ""));
                            else if (objType == typeof(UInt16))
                                result = UInt16.Parse(XmlNode.ParseString(_node, ""));
                            else if (objType == typeof(int) || objType == typeof(Int32))
                                result = XmlNode.ParseInteger(_node, 0);
                            else if (objType == typeof(uint) || objType == typeof(UInt32))
                                result = uint.Parse(XmlNode.ParseString(_node, ""));
                            else if (objType == typeof(Int64) || objType == typeof(long))
                                result = Int64.Parse(XmlNode.ParseString(_node, ""));
                            else if (objType == typeof(UInt64) || objType == typeof(ulong))
                                result = UInt64.Parse(XmlNode.ParseString(_node, ""));
                            else if (objType == typeof(double))
                                result = XmlNode.ParseDouble(_node, 0);
                            else if (objType == typeof(float) || objType == typeof(Single))
                                result = float.Parse(XmlNode.ParseString(_node, ""));
                            else if (objType == typeof(string))
                                result = XmlNode.ParseString(_node, "");
                            else if (objType == typeof(DateTime))
                                result = XmlNode.ParseDateTime(_node, DateTime.MinValue, true);
                            else if (objType == typeof(TimeSpan))
                                result = XmlNode.ParseTimeSpan(_node, TimeSpan.Zero);
                            else if (objType == typeof(Guid))
                                result = XmlNode.ParseGuid(_node, Guid.Empty);
                            else if (objType.IsGenericType && objType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>))
                            {
                                Type keyType = objType.GetProperty("Key").PropertyType;
                                Type valType = objType.GetProperty("Value").PropertyType;

                                string[] keyValuePairString = _node.Value.Trim(new[] { '[', ']' }).Split(new[] { ',' });

                                if (keyValuePairString.Length >= 2)
                                    result = Activator.CreateInstance(objType, new[] { Deserialize(new XmlNode("tempKeynode", keyValuePairString[0].Trim()), aqnDictionary, typePath, keyType), Deserialize(new XmlNode("tempValnode", keyValuePairString[1].Trim()), aqnDictionary, typePath, valType) });
                                else
                                    result = null;
                            }
                            else if (objType.IsArray)
                            {
                                Array arr = Array.CreateInstance(objType.GetElementType(), _node.Count);

                                int index = 0;
                                foreach (XmlNode itemNode in _node.Children)
                                {
                                    object listItem = Deserialize(itemNode, aqnDictionary, typePath);
                                    if (listItem != null)
                                        arr.SetValue(listItem, index);

                                    index++;
                                }

                                result = arr;
                            }
                            else if (objType.ImplementsIEnumerable() && !objType.IsXMLNode())
                            {
                                IEnumerable list = null;
                                if (objType.GetConstructor(Type.EmptyTypes) != null) //check if the class has a default constructor
                                    list = (IEnumerable)Activator.CreateInstance(objType); //if it does have one, create the class and call the constructor
                                else
                                    list = (IEnumerable)FormatterServices.GetUninitializedObject(objType); //if it doesn't have one, this way creates an instance without calling any constructor.

                                if (list != null)
                                {
                                    foreach (XmlNode itemNode in _node.Children)
                                    {
                                        object listItem = Deserialize(itemNode, aqnDictionary, typePath);
                                        if (list is IList)
                                            ((IList)list).Add(listItem);
                                        else if (objType.IsGenericType && objType.GetGenericTypeDefinition() == typeof(Queue<>) || list is Queue)
                                            objType.GetMethod("Enqueue").Invoke(list, new object[] { listItem });
                                        else if (objType.IsGenericType && objType.GetGenericTypeDefinition() == typeof(Stack<>) || list is Stack)
                                            objType.GetMethod("Push").Invoke(list, new object[] { listItem });
                                        else if(objType.IsGenericType && objType.GetGenericTypeDefinition() == typeof(IDictionary<,>) || list is IDictionary)
                                        {
                                            Type listItemType = listItem.GetType();
                                            if(listItemType.IsGenericType && listItemType.GetGenericTypeDefinition() == typeof(KeyValuePair<,>))
                                                objType.GetMethod("Add").Invoke(list, new object[] { listItemType.GetProperty("Key").GetValue(listItem), listItemType.GetProperty("Value").GetValue(listItem) });
                                        }
                                    }
                                }

                                result = list;
                            }
                            else if (objType.IsClass)
                            {
                                if (!string.IsNullOrWhiteSpace(_node.Value) || _node.Count > 0)
                                {
                                    if (objType.GetConstructor(Type.EmptyTypes) != null) //check if the class has a default constructor
                                        result = Activator.CreateInstance(objType); //if it does have one, create the class and call the constructor
                                    else
                                        result = FormatterServices.GetUninitializedObject(objType); //if it doesn't have one, this way creates an instance without calling any constructor.

                                    foreach (FieldInfo fieldInfo in result.GetType().GetAllFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
                                    {
                                        if (!fieldInfo.IsStatic && !fieldInfo.IsNotSerialized && !fieldInfo.IsLiteral && !fieldInfo.IsNotSerializedXML())
                                        {
                                            XmlNode fieldNode = _node["Name", fieldInfo.Name];
                                            if (fieldNode != null)
                                            {
                                                object propVal = fieldInfo.GetValue(result);

                                                object newVal = Deserialize(fieldNode, aqnDictionary, typePath /*, fieldInfo.FieldType*/);
                                                if (newVal != null)
                                                {
                                                    fieldInfo.SetValue(result, newVal);
                                                }
                                            }
                                        }
                                    }

                                    if (objType.IsXMLDoc())
                                    {
                                        XmlNode xmlnode = _node["XML"];

                                        if(xmlnode.Value.Length > 7 && xmlnode.Value.StartsWith("<!--") && xmlnode.Value.EndsWith("-->"))
                                        {
                                            string docData = xmlnode.Value.Substring(4, xmlnode.Value.Length - 7);

                                            ((XMLDoc)result).LoadFromString(docData);
                                        }
                                    }

                                    //if (objType.IsXMLNode())
                                    //{
                                    //    XmlNode xmlnode = _node["XML"];

                                    //    if (xmlnode.Count > 0)
                                    //    {
                                    //        XmlNode doc = xmlnode[0];

                                    //        ((XmlNode)result).Name = doc.Name;

                                    //        foreach (XmlNode child in doc)
                                    //            ((XmlNode)result).Add(ObjectCopier.Clone<XmlNode>(child)); //Make copies!

                                    //        foreach (XmlItem attr in doc.Attributes)
                                    //            ((XmlNode)result).AddAttribute(ObjectCopier.Clone<XmlItem>(attr)); //Make copies!
                                    //    }
                                    //}
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }
    }
}
