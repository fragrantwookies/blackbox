﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eThele.Essentials
{
    /// <summary>
    /// Class to simplify timespan user inputs.
    /// </summary>
    public static class TimeSpanParser
    {
        private const string formatExceptionMessage = "Time span string must be in the following format: value[.fraction][:][s]|[m]|[h]|[d]|[w]. {0}";

        /// <summary>
        /// Parse a timespan from user input. Supports miliseconds, seconds, minutes, hours, days and weeks. 
        /// Format: value[.fraction][:][s]|[m]|[h]|[d]|[w]
        /// Example: 2m => 120000
        ///         1h => 3600000
        /// </summary>
        /// <param name="val">string value to parse</param>
        /// <returns>Time span in miliseconds.</returns>
        public static int Parse(string val)
        {
            double result = 0;

            if (!string.IsNullOrWhiteSpace(val))
            {
                string[] parts = val.Split(':');
                string unit = "";

                if (parts.Length >= 1)
                {
                    string firstPart = parts[0].Trim();

                    if (!double.TryParse(firstPart, out result))
                    {
                        if (double.TryParse(firstPart.Substring(0, firstPart.Length - 1).Trim(), out result))
                        {
                            unit = firstPart.Substring(firstPart.Length - 1);
                        }
                        else
                            throw new FormatException(string.Format(formatExceptionMessage, val));
                    }
                }
                else
                    throw new FormatException(string.Format(formatExceptionMessage, val));

                if (parts.Length == 2)
                {
                    unit = parts[1].Trim();
                }

                if (!string.IsNullOrWhiteSpace(unit))
                {
                    switch (unit.ToLower())
                    {
                        case "s":
                            result *= 1000;
                            break;
                        case "m":
                            result *= 60000;
                            break;
                        case "h":
                            result *= 3600000;
                            break;
                        case "d":
                            result *= 86400000;
                            break;
                        case "w":
                            result *= 604800000;
                            break;
                        default:
                            break;
                    }
                }
            }

            return (int)result;
        }

        public static bool TryParse(string val, out int result, out string error)
        {
            bool returnval = true;

            try
            {
                result = Parse(val);
                error = "";
            }
            catch(Exception ex)
            {
                returnval = false;
                result = 0;
                error = ex.Message;
            }

            return returnval;
        }
    }
}
