﻿using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace eThele.Essentials
{
    public class NetworkTools
    {
        public static IPAddress MyIPAddress()
        {
            IPAddress ip = IPAddress.Parse("127.0.0.1");// This is exactly the return value you want to avoid.
            using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, 0))
            {
                socket.Connect("8.8.8.8", 65530);
                IPEndPoint endPoint = socket.LocalEndPoint as IPEndPoint;
                ip = endPoint.Address;
            }
            return ip;
        }
    }
}
