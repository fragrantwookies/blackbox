﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Text;
using System.Globalization;

namespace eThele.Essentials
{
    /// <summary>
    /// Rules: Names must be unique for the group they reside in; i.e. child nodes with the same parent must have unique names. The node name may obviously be used under a different parent node.
    /// </summary>
    [Serializable]
    public class XMLDoc : XmlNode
    {
        [NonSerialized]
        private string defaultPath = string.Format("{0}\\Ethele\\RioSoft\\{1}\\Config", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), Assembly.GetEntryAssembly().GetName().Name);
        protected string fileName = "";

        public XMLDoc() : base() { }

        public XMLDoc(string _name = "")
            : base(_name, "", 0) //The main node won't have a value and it's index will obviously be 0.
        { }

        /// <summary>
        /// Load from file.
        /// </summary>
        /// <param name="_fileName">Path and File name of xml file</param>
        public virtual bool Load(string _fileName = "")
        {
            bool result = false;

            if (string.IsNullOrWhiteSpace(_fileName))
                _fileName = DefaultFilePath;

            XmlReader reader = null;

            try
            {
                Clear();

                reader = XmlReader.Create(_fileName);

                result = Read(reader);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }
            }

            return result;
        }

        /// <summary>
        /// Load from xml string
        /// </summary>
        /// <param name="_data">Xml data string</param>
        public virtual bool LoadFromString(string _data)
        {
            bool result = false;

            Clear();

            //XmlReader reader = null;

            using (StringReader sr = new StringReader(_data))
            {
                using (XmlReader reader = XmlReader.Create(sr))
                {
                    result = Read(reader);
                }
            }

            //try
            //{
            //    reader = XmlReader.Create(new StringReader(_data));

            //    result = Read(reader);
            //}
            //finally
            //{
            //    if (reader != null)
            //    {
            //        reader.Close();
            //        reader.Dispose();
            //    }
            //}

            return result;
        }

        protected virtual bool Read(XmlReader reader)
        {
            XmlNode lastNode = null;
            int depth = 0;
            bool mayHaveValue = false; //just to make sure we don't put random text into the last node's value field

            while (reader.Read())
            {
                XmlNode currentNode = null;

                if (reader.IsStartElement() && reader.NodeType == XmlNodeType.Element/* && !reader.IsEmptyElement*/)
                {
                    string currentName = reader.Name;
                    mayHaveValue = !reader.IsEmptyElement;

                    #region Find parent
                    //This is a new node. If this is not the first node, the parent must have been created already, so we must find it.
                    XmlNode parentNode = null;
                    if (reader.Depth > depth)
                    {
                        parentNode = lastNode;
                    }
                    else if (reader.Depth == depth)
                    {
                        parentNode = lastNode != null ? lastNode.Parent : null;
                    }
                    else if (reader.Depth < depth)
                    {
                        int depthDiff = depth - reader.Depth;

                        parentNode = lastNode != null ? lastNode.Parent : null;

                        while (depthDiff > 0)
                        {
                            parentNode = parentNode != null ? parentNode.Parent : null;

                            depthDiff--;
                        }
                    }
                    #endregion Find parent

                    if (parentNode != null)
                        currentNode = new XmlNode(currentName, "", parentNode.MaxChildIndex++);
                        //currentNode = parentNode.Add(currentName, "");
                    else
                    {
                        currentNode = this;
                        currentNode.Name = currentName;
                    }

                    //Load this node's attributes
                    if (reader.HasAttributes && currentNode != null)
                    {
                        while (reader.MoveToNextAttribute())
                        {
                            string attributeName = reader.Name;
                            if (attributeName == "NodeName")
                                currentNode.Name = reader.Value;
                            else
                                currentNode.AddAttribute(attributeName, reader.Value);
                        }

                        reader.MoveToElement();
                    }

                    if (parentNode != null && currentNode != null)
                        currentNode = parentNode.Add(currentNode);

                    lastNode = currentNode;
                    depth = reader.Depth;
                }
                else if (reader.NodeType == XmlNodeType.Text) //read text value of node
                {
                    if (reader.HasValue && lastNode != null && mayHaveValue)
                        lastNode.Value = reader.Value;
                }
                else if (reader.NodeType == XmlNodeType.EndElement)
                {
                    mayHaveValue = false;
                }
            }

            return true;
        }

        public virtual bool Save(string _fileName = "")
        {
            bool result = false;

            if (_fileName == "")
                _fileName = DefaultFilePath;

            XmlWriter writer = null;

            try
            {
                string directory = Path.GetDirectoryName(_fileName);
                if (!Directory.Exists(directory))
                    Directory.CreateDirectory(directory);

                writer = XmlWriter.Create(_fileName);

                WriteNode(writer, this);

                result = true;
            }
            finally
            {
                if (writer != null)
                {
                    writer.Flush();
                    writer.Close();
                    writer.Dispose();
                }
            }

            return result;
        }

        public class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }

        public virtual string SaveToString()
        {
            string result = "";

            //XmlWriter writer = null;

            try
            {
                using (Utf8StringWriter sw = new Utf8StringWriter())
                {
                    using (XmlWriter writer = XmlWriter.Create(sw, new XmlWriterSettings() { OmitXmlDeclaration = true, ConformanceLevel = ConformanceLevel.Fragment }))
                    {
                        WriteNode(writer, this, false);
                    }

                    result = sw.ToString();
                }
            }
            finally
            {
                //if (writer != null)
                //{
                //    writer.Flush();
                //    writer.Close();
                //    writer.Dispose();
                //}
            }

            return result;
        }

        /// <summary>
        /// This is only to help us to keep track of what node level we are, so that we can create nice indentations.
        /// </summary>
        [NonSerialized] private int nodeLevel = 0;

        /// <summary>
        /// This funtion is called recursively. The parent will call it for all its children and they for theirs.
        /// </summary>
        /// <param name="writer">The xml Writer</param>
        /// <param name="node">The node to write</param>
        /// <param name="format">Format with line breaks and indentations</param>
        private void WriteNode(XmlWriter writer, XmlNode node, bool format = true)
        {
            if(format)
            { 
                writer.WriteWhitespace("\n");

                for (int i = 0; i < nodeLevel; i++)
                    writer.WriteWhitespace("\t");
            }

            string nodeName = string.Format("Node{0}", node.Index);
            writer.WriteStartElement(nodeName);
            writer.WriteAttributeString("NodeName", node.Name);

            if (node.AttributeCount > 0)
            {
                node.SortAttributesByIndex();

                for (int i = 0; i < node.AttributeCount; i++)
                    writer.WriteAttributeString(node.Attribute(i).Name, node.Attribute(i).Value);

                node.SortAttributes();
            }

            if (node.Count > 0)
            {
                node.SortByIndex();

                for (int i = 0; i < node.Count; i++)
                {
                    nodeLevel++;
                    WriteNode(writer, node[i], format);
                    nodeLevel--;
                }

                node.Sort();

                if (format)
                {
                    writer.WriteWhitespace("\n");

                    for (int i = 0; i < nodeLevel; i++)
                        writer.WriteWhitespace("\t");
                }
            }
            else
                writer.WriteString(node.Value);

            writer.WriteEndElement();
        }

        public string FileName
        {
            get { return !string.IsNullOrWhiteSpace(fileName) ? fileName : (name + ".xml"); }
            set { fileName = value; }
        }

        /// <summary>
        /// Combines the default path with the Name property and '.xml'
        /// </summary>
        public string DefaultFilePath
        {
            get
            {
                string filePath = Path.Combine(defaultPath, Path.GetFileName(FileName));
                return filePath;
            }
        }

        public void ReassignChildrenParents(XmlNode _node = null)
        {
            _node = _node ?? this;

            foreach(XmlNode childNode in _node)
            {
                if (childNode.Parent == null)
                    childNode.Parent = _node;

                ReassignChildrenParents(childNode);
            }
        }
    }

    [Serializable]
    public class XmlNode : XmlItem, IEnumerable<XmlNode>, IEquatable<XmlNode>
    {
        #region Constructors
        public XmlNode()
            : base()
        {
            children = new List<XmlNode>();
            attributes = new List<XmlItem>();
        }
        public XmlNode(string _name, string _value, int _index = -1)
            : base(_name, _value, _index)
        {
            children = new List<XmlNode>();
            attributes = new List<XmlItem>();
        }

        public XmlNode(string _name, DateTime _value, int _index = -1)
            : base(_name, _value, _index)
        {
            children = new List<XmlNode>();
            attributes = new List<XmlItem>();
        }
        #endregion Constructors

        #region Cleanup
        protected override void Dispose(bool disposing)
        {
            Clear();

            base.Dispose(disposing);
        }

        public void Clear()
        {
            ClearNodes();
            ClearAttributes();
        }
        #endregion Cleanup

        #region Parent
        [NonSerialized] 
        [NonSerializedXML]
        protected XmlNode parent = null;
        public XmlNode Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        public XmlNode Root
        {
            get
            {
                XmlNode root = this;

                while (root.Parent != null)
                {
                    root = root.Parent;
                }

                return root;
            }
        }
        #endregion Parent

        #region Child nodes
        public bool HasChildren
        {
            get
            {
                return children != null && children.Count > 0;
            }
        }
        
        [NonSerializedXML]
        protected List<XmlNode> children;
        public List<XmlNode> Children { get { return children; } set { children = value; } }

        //protected bool uniqueChildNodes = true;
        //protected bool uniqueChildNodesExplicitlySet = false;

        public bool UniqueChildNodes
        {
            get
            {
                //return XmlNode.ParseBool(Root, true, "UCN");
                return false;
            }
            set
            {
                //XmlItem attribute = Root.Attribute("UCN");

                //if (attribute != null)
                //    attribute.Value = value.ToString();
                //else
                //    this.AddAttribute("UCN", value.ToString());
            }
            //get 
            //{
            //    if (parent != null)
            //        return uniqueChildNodesExplicitlySet ? UniqueChildNodesAttribute : Root.UniqueChildNodes;
            //    else
            //        return UniqueChildNodesAttribute; 
            //}
            //set 
            //{ 
            //    uniqueChildNodes = value;
            //    uniqueChildNodesExplicitlySet = true;
            //    UniqueChildNodesAttribute = value;
            //}
        }

        //public bool UniqueChildNodesAttribute
        //{
        //    get
        //    {                
        //        return XmlNode.ParseBool(this, uniqueChildNodes, "UCN"); 
        //    }
        //    set
        //    {
        //        XmlItem attribute = this.Attribute("UCN");

        //        if (attribute != null)
        //            attribute.Value = value.ToString();
        //        else
        //            this.AddAttribute("UCN", value.ToString());
        //    }
        //}

        [NonSerialized]
        [NonSerializedXML]
        private int maxChildIndex = 0;

        public int MaxChildIndex
        {
            get { return maxChildIndex; }
            set { maxChildIndex = value; }
        }

        public XmlNode this[int i]
        {
            get
            {
                return children[i];
            }
        }

        /// <summary>
        /// If Child Nodes are unique, you'll get the right one, otherwise you'll get the first match
        /// </summary>
        public XmlNode this[string _name]
        {
            get
            {
                XmlNode result = null;
                if (children != null)
                {
                    XmlNode tempNode = new XmlNode(_name, "");
                    if(UniqueChildNodes)
                    { 
                        int searchResult = children.BinarySearch(tempNode);
                        if (searchResult >= 0)
                            result = children[searchResult];
                    }
                    else
                    {
                        int searchResult = children.IndexOf(tempNode);
                        if(searchResult >= 0)
                            result = children[searchResult];
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Use this to find a child node multiple levels down
        /// </summary>
        /// <param name="_nodeNamesCSV">Comma seperated list of child node names</param>
        public XmlNode FindChildNode(string _nodeNamesCSV, char _delimiter = ',')
        {
            XmlNode result = this;

            foreach (string nodeName in _nodeNamesCSV.Split(_delimiter))
            {
                if (result != null)
                    result = result[nodeName.Trim()];
                else
                    break;
            }

            return result != this ? result : null;
        }

        /// <summary>
        /// Search for the first child node with a specified attribute value.
        /// </summary>
        /// <param name="_attributeName">Name of the attribute</param>
        /// <param name="_attributeValue">Value of the attribute</param>
        public XmlNode this[string _attributeName, string _attributeValue]
        {
            get
            {
                XmlNode result = null;
                if (children != null)
                {
                    foreach (XmlNode node in children)
                    {
                        if (node.Attribute(_attributeName) != null)
                        {
                            if (node.Attribute(_attributeName).Value.ToLower().Equals(_attributeValue.ToLower()))
                            {
                                result = node;
                                break;
                            }
                        }
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Search for the first child node matching all specified attribute values.
        /// </summary>
        /// <param name="_attributes">Array of attribute names and values</param>
        public XmlNode this[Tuple<string, string>[] _attributes]
        {
            get
            {
                XmlNode result = null;
                if (children != null)
                {
                    foreach (XmlNode node in children)
                    {
                        bool match = true;

                        foreach (var attribute in _attributes)
                        {
                            if (node.Attribute(attribute.Item1) != null)
                            {
                                if (!node.Attribute(attribute.Item1).Value.ToLower().Equals(attribute.Item2.ToLower()))
                                {
                                    match = false;
                                    break;
                                }
                            }
                        }

                        if(match)
                        {
                            result = node;
                            break;
                        }
                    }
                }

                return result;
            }
        }

        /// <summary>
        /// Use this to compare based on an attribute.
        /// </summary>
        /// <param name="x">The attribute you want to compare by</param>
        /// <param name="y">The node to compare to</param>
        /// <returns></returns>
        public static int CompareByAttribute(XmlItem x, XmlNode y)
        {
            return x.Value.CompareTo(y.Attribute(x.Name).Value);
        }

        public int Count
        {
            get
            {
                return children.Count;
            }
        }

        public XmlNode Add(string _name, string _value, int _index = -1)
        {
            if (_index < 0)
                _index = maxChildIndex++;

            XmlNode newChild = new XmlNode(_name, _value, _index);

            return Add(newChild);
        }

        public XmlNode Add(string _name, DateTime _value, int _index = -1)
        {
            return Add(_name, _value.ToUniversalTime().ToString("o", DateTimeFormatInfo.InvariantInfo), _index);
        }

        /// <summary>
        /// If you have Unique children and you try to add one that allready exists it will be ignored and null returned
        /// </summary>
        public XmlNode Add(XmlNode newChild)
        {
            XmlNode result = null;

            if (UniqueChildNodes)
            {
                int searchResult = children.BinarySearch(newChild);
                if (searchResult < 0)
                {
                    children.Insert(~searchResult, newChild);
                    newChild.Parent = this;

                    if (newChild.Index > maxChildIndex)
                        maxChildIndex = newChild.Index;

                    result = newChild;
                }
            }
            else
            {
                children.Add(newChild);
                newChild.Parent = this;

                if (newChild.Index > maxChildIndex)
                    maxChildIndex = newChild.Index;

                result = newChild;
            }

            return result;
        }

        public XmlNode Update(string _name, string _value)
        {
            XmlNode newChild = new XmlNode(_name, _value, maxChildIndex++);

            return Update(newChild);
        }

        public XmlNode Update(string _name, DateTime _value)
        {
            return Update(_name, _value.ToUniversalTime().ToString("o", DateTimeFormatInfo.InvariantInfo));
        }

        public XmlNode Update(XmlNode newChild)
        {
            XmlNode result = null;

            result = this[newChild.Name];

            if (result != null)
                result.Value = newChild.Value;
            else
                children.Add(newChild);

            return result;
        }

        public bool RemoveChildNode(XmlNode _child)
        {
            bool result = false;

            if (children != null)
            {
                result = children.Remove(_child);
                _child.Dispose();
            }

            return result;
        }

        public bool RemoveChildNodeAt(int _childIndex)
        {
            if (_childIndex >= 0 && _childIndex < children.Count)
            {
                XmlNode child = children[_childIndex];
                children.RemoveAt(_childIndex);
                child.Dispose();
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Clear and dispose the child nodes
        /// </summary>
        public void ClearNodes()
        {
            if (children != null)
            {
                for (int i = children.Count - 1; i >= 0; i--)
                {
                    XmlNode child = children[i];
                    if (child != null)
                        child.Dispose();

                    children.RemoveAt(i);
                }
            }
        }

        public void Sort()
        {
            children.Sort();
        }

        public void SortByIndex()
        {
            children.Sort(CompareSettingsByIndex);
        }
        #endregion Child nodes

        #region Attributes
        [NonSerializedXML]
        protected List<XmlItem> attributes;
        public List<XmlItem> Attributes { get { return attributes; } set { attributes = value; } }

        [NonSerialized]
        [NonSerializedXML]
        int maxAttributeIndex = 0;
        public XmlItem Attribute(int index)
        {
            return attributes[index];
        }

        public XmlItem Attribute(string _name)
        {
            XmlItem result = null;
            if (attributes != null)
            {
                XmlItem tempAttribute = new XmlItem(_name, "", -1);
                //int searchResult = attributes.BinarySearch(tempAttribute);
                int searchResult = attributes.IndexOf(tempAttribute);
                if (searchResult >= 0)
                    result = attributes[searchResult];
            }

            return result;
        }

        public int AttributeCount
        {
            get
            {
                return attributes.Count;
            }
        }

        public bool AddAttribute(string _name, string _value, int _index = -1)
        {
            if (_index < 0)
                _index = maxAttributeIndex++;

            XmlItem attribute = new XmlItem(_name, _value, _index);

            return AddAttribute(attribute);
        }

        public bool AddAttribute(XmlItem attribute, string chilNodeName = "")
        {
            bool result = false;

            XmlNode node = this;

            if (!string.IsNullOrEmpty(chilNodeName))
            {
                node = this[chilNodeName];

                if (node == null)
                    node = new XmlNode() { Name = chilNodeName };
            }

            //int searchResult = attributes.BinarySearch(attribute);
            int searchResult = node.attributes.IndexOf(attribute);
            if (searchResult < 0)
            {
                //attributes.Insert(~searchResult, attribute);
                attributes.Add(attribute);

                if (attribute.Index > maxAttributeIndex)
                    maxAttributeIndex = attribute.Index;

                result = true;
            }
            else
                attributes[searchResult].Value = attribute.Value;

            return result;
        }

        public bool RemoveAttribute(XmlItem _attribute)
        {
            bool result = false;

            if (children != null)
            {
                result = attributes.Remove(_attribute);
                _attribute.Dispose();
            }

            return result;
        }

        public bool RemoveAttributeAt(int _attributeIndex)
        {
            if (_attributeIndex >= 0 && _attributeIndex < children.Count)
            {
                XmlItem attribute = attributes[_attributeIndex];
                attributes.RemoveAt(_attributeIndex);
                attribute.Dispose();
                return true;
            }
            else
                return false;
        }

        public void ClearAttributes()
        {
            if (attributes != null)
                attributes.Clear();
        }

        public void SortAttributes()
        {
            attributes.Sort();
        }

        public void SortAttributesByIndex()
        {
            attributes.Sort(CompareSettingsByIndex);
        }

        public static int CompareSettingsByIndex(XmlItem x, XmlItem y)
        {
            int result = 0;

            if (x != null && y != null)
                result = x.Index.CompareTo(y.Index);

            return result;
        }
        #endregion Attributes

        #region Parsers
        /// <param name="attributeName">Ignore to parse Value</param>
        public static bool ParseBool(XmlNode node, bool defaultValue, string attributeName = "")
        {
            bool result = defaultValue;

            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                        result = node.Attribute(attributeName) != null ? bool.Parse(node.Attribute(attributeName).Value) : defaultValue;
                    else
                        result = bool.Parse(node.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return result;
        }

        /// <param name="attributeName">Ignore to parse Value</param>
        public static int ParseInteger(XmlNode node, int defaultValue, string attributeName = "")
        {
            int result = defaultValue;

            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                        result = node.Attribute(attributeName) != null ? int.Parse(node.Attribute(attributeName).Value) : defaultValue;
                    else
                        result = int.Parse(node.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return result;
        }        

        /// <param name="attributeName">Ignore to parse Value</param>
        public static double ParseDouble(XmlNode node, double defaultValue, string attributeName = "")
        {
            double result = defaultValue;

            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                        result = node.Attribute(attributeName) != null ? double.Parse(node.Attribute(attributeName).Value, CultureInfo.InvariantCulture) : defaultValue;
                    else
                        result = double.Parse(node.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return result;
        }

        /// <param name="convertToLocalTime">Value is UCT and you want to convert it to local time</param>
        /// <param name="attributeName">Ignore to parse Value</param>
        public static DateTime ParseDateTime(XmlNode node, DateTime defaultValue, bool convertToLocalTime, string attributeName = "")
        {
            DateTime result = defaultValue;

            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                        result = node.Attribute(attributeName) != null ? DateTime.ParseExact(node.Attribute(attributeName).Value, "o", DateTimeFormatInfo.InvariantInfo) : defaultValue;
                    else
                        result = DateTime.ParseExact(node.Value, "o", DateTimeFormatInfo.InvariantInfo);
                }

                if (convertToLocalTime)
                    result = result.ToLocalTime();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return result;
        }

        /// <param name="attributeName">Ignore to parse Value</param>
        public static TimeSpan ParseTimeSpan(XmlNode node, TimeSpan defaultValue, string attributeName = "")
        {
            TimeSpan result = defaultValue;

            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                        result = node.Attribute(attributeName) != null ? TimeSpan.Parse(node.Attribute(attributeName).Value) : defaultValue;
                    else
                        result = TimeSpan.Parse(node.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return result;
        }

        /// <param name="attributeName">Ignore to parse Value</param>
        public static string ParseString(XmlNode node, string defaultValue, string attributeName = "")
        {
            string result = defaultValue;

            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                        result = node.Attribute(attributeName) != null ? node.Attribute(attributeName).Value : defaultValue;
                    else
                        result = node.Value;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return result;
        }

        /// <param name="attributeName">Ignore to parse Value</param>
        public static Guid ParseGuid(XmlNode node, Guid defaultValue, string attributeName = "")
        {
            Guid result = defaultValue;

            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                        result = node.Attribute(attributeName) != null ? (node.Attribute(attributeName).Value == "" ? defaultValue : Guid.Parse(node.Attribute(attributeName).Value)) : defaultValue;
                    else
                        result = node.Value == "" ? defaultValue : Guid.Parse(node.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return result;
        }

        /// <param name="attributeName">Ignore to parse Value</param>
        public static TEnum ParseEnum<TEnum>(XmlNode node, TEnum defaultValue, string attributeName = "") where TEnum : struct
        {
            TEnum result = defaultValue;

            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                    {
                        if (node.Attribute(attributeName) != null)
                            result = (TEnum)Enum.Parse(typeof(TEnum), node.Attribute(attributeName).Value);
                    }
                    else
                        result = (TEnum)Enum.Parse(typeof(TEnum), node.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return result;
        }

        public static TEnum ParseEnum<TEnum>(XmlNode node, string attributeName = "") where TEnum : struct
        {
            TEnum result = default (TEnum);//(TEnum)(Enum.GetValues(typeof(TEnum)).GetValue(0));

            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                    {
                        if (node.Attribute(attributeName) != null)
                            result = (TEnum)Enum.Parse(typeof(TEnum), node.Attribute(attributeName).Value);
                    }
                    else
                        result = (TEnum)Enum.Parse(typeof(TEnum), node.Value);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return result;
        }

        /// <param name="attributeName">Ignore to parse Value</param>
        public static bool TryParseString(XmlNode node, out string result, string defaultValue, string attributeName = "")
        {
            result = defaultValue;

            bool success = false;
            try
            {
                if (node != null)
                {
                    if (!string.IsNullOrWhiteSpace(attributeName))
                    {
                        if (node.Attribute(attributeName) == null)
                        {
                            result = defaultValue;
                        }
                        else
                        {
                            result = node.Attribute(attributeName).Value;
                            success = true;
                        }

                    }
                    else
                    {
                        result = node.Value;
                        success = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), string.Format("Couldn't parse {0}", (attributeName == "" ? "Value" : attributeName + " Attribute")), ex);
            }

            return success;
        }
        #endregion Parsers

        #region Misc
        public static void SetVal(XmlNode node, string val, string attributeName = "")
        {
            if (node != null)
            {
                if (!string.IsNullOrWhiteSpace(attributeName))
                    if (node.Attribute(attributeName) != null)
                        node.Attribute(attributeName).Value = val;
                    else
                        node.Attributes.Add(new XmlItem(attributeName, val, node.Attributes.Count));
                else
                    node.Value = val;
            }
        }
        #endregion Misc

        #region Interface Implementations
        public IEnumerator<XmlNode> GetEnumerator()
        {
            foreach (XmlNode xmlNode in children)
                yield return xmlNode;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (XmlNode xmlNode in children)
                yield return xmlNode;
        }

        public bool Equals(XmlNode other)
        {
            return this.Name.ToLower().Equals(other.Name.ToLower());
        }
        #endregion Interface Implementations
    }

    [Serializable]
    public class XmlItem : IDisposable, IComparable<XmlItem>, IComparer<XmlItem>, IEquatable<XmlItem>
    {
        [NonSerializedXML]
        protected string name;
        [NonSerializedXML]
        protected string value;
        [NonSerializedXML]
        protected int index;

        #region Constructors
        public XmlItem() { }

        public XmlItem(string _name, string _value, int _index)
        {
            name = _name;
            value = _value;
            index = _index;
        }

        public XmlItem(string _name, DateTime _value, int _index)
        {
            name = _name;
            SetValueFromDate(_value);
            index = _index;
        }
        #endregion Constructors

        #region Cleanup
        ~XmlItem()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);
        }
        #endregion Cleanup

        #region Properties
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public void SetValueFromDate(DateTime _dateVal)
        {
            this.value = _dateVal.ToUniversalTime().ToString("o", DateTimeFormatInfo.InvariantInfo);
        }

        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        #endregion Properties

        #region Compare
        public int CompareTo(XmlItem other)
        {
            return this.Name.ToLower().CompareTo(other.Name.ToLower());
        }

        public int Compare(XmlItem x, XmlItem y)
        {
            return x.Name.ToLower().CompareTo(y.Name.ToLower());
        }

        public bool Equals(XmlItem other)
        {
            return this.Name.ToLower().Equals(other.Name.ToLower());
        }
        #endregion Compare
    }
}
