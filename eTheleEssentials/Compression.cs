﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eThele.Essentials
{
    /// <summary>
    /// GZip compression and decompression
    /// </summary>
    public class Compression
    {        
        public static byte[] Compress(byte[] _data)
        {
            return Compress(_data, 0, _data.Length);
        }

        public static byte[] Compress(byte[] _data, int _offset, int _length)
        {
            byte[] result = null;

            using (MemoryStream compressedStream = new MemoryStream())
            {
                using (GZipStream zip = new GZipStream(compressedStream, CompressionMode.Compress))
                {
                    zip.Write(_data, _offset, _length);
                }

                result = compressedStream.ToArray();
            }

            return result;
        }

        public static byte[] Decompress(byte[] _data)
        {
            return Decompress(_data, 0, _data.Length);
        }

        public static byte[] Decompress(byte[] _data, int _offset, int _length)
        {
            byte[] result = null;

            using (MemoryStream compressedStream2 = new MemoryStream(_data, _offset, _length))
            {
                using (GZipStream zip2 = new GZipStream(compressedStream2, CompressionMode.Decompress))
                {
                    MemoryStream output = new MemoryStream();
                    byte[] buffer = new byte[4096];
                    int bytesRead;
                    while ((bytesRead = zip2.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        output.Write(buffer, 0, bytesRead);
                    }

                    result = output.ToArray();
                }
            }

            return result;
        }
    }
}
