﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eThele.Essentials
{
    public class CRC
    {
        #region CRC16
        private static ushort crc16_P = 0x8408;
        public static ushort CRC16_Polynomial
        {
            get { return CRC.crc16_P; }
            set { CRC.crc16_P = value; crc16tabInitialized = false; }
        }

        private static bool crc16tabInitialized = false;

        private readonly static ushort[] crc16tab = new ushort[256];

        private static void InitializeCRC16Tab()
        {
            ushort crc;

            for (ushort i = 0; i < 256; i++)
            {
                crc = i;

                for (uint j = 0; j < 8; j++)
                {
                    if ((crc & 0x0001) > 0)
                        crc = (ushort)((crc >> 1) ^ crc16_P);
                    else
                        crc = (ushort)(crc >> 1);
                }

                crc16tab[i] = crc;
            }

            crc16tabInitialized = true;
        }

        public static ushort CalcCRC16(byte[] data)
        {
            return CalcCRC16(data, 0, data.Length);
        }

        public static ushort CalcCRC16(byte[] data, int index, int length)
        {
            ushort crc = 0xFFFF;

            if (!crc16tabInitialized)
                InitializeCRC16Tab();

            for (int i = index; i < length; i++)
            {
                ushort tmp = (ushort)(crc ^ data[i]);
                crc = (ushort)((crc >> 8) ^ crc16tab[tmp & 0xFF]);
            }

            return (ushort)(crc ^ 0xFFFF);
        } 
        #endregion CRC16

        #region CRC32
        private static uint crc32_P = 0xEDB88320;

        public static uint CRC32_Polynomial
        {
            get { return CRC.crc32_P; }
            set { CRC.crc32_P = value; crc32tabInitialized = false; }
        }

        private static bool crc32tabInitialized = false;
        private readonly static uint[] crc32tab = new uint[256];

        private static void InitializeCRC32Tab()
        {
            uint crc;

            for (uint i = 0; i < 256; i++)
            {
                crc = i;

                for (uint j = 0; j < 8; j++)
                {
                    if ((crc & 0x00000001) > 0)
                        crc = (crc >> 1) ^ crc32_P;
                    else
                        crc = crc >> 1;
                }

                crc32tab[i] = crc;
            }

            crc32tabInitialized = true;
        }

        public static uint CalcCRC32(byte[] data)
        {
            return CalcCRC32(data, 0, data.Length);
        }

        public static uint CalcCRC32(byte[] data, int index, int length)
        {
            uint crc = 0xFFFFFFFF;

            if (!crc32tabInitialized)
                InitializeCRC32Tab();

            for (int i = index; i < length; i++)
            {
                uint tmp = crc ^ data[i];
                crc = (crc >> 8) ^ crc32tab[tmp & 0xFF];
            }

            return crc ^ 0xFFFFFFFF;
        }
        #endregion
    }
}
