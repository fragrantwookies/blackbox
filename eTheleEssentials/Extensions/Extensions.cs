﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace eThele.Essentials
{
    public static class Extensions
    {
        public static IEnumerable<FieldInfo> GetAllFields(this Type type, BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
        {
            if (type == typeof(Object)) 
                return Enumerable.Empty<FieldInfo>();
            else
                return type.BaseType.GetAllFields(flags).Concat(type.GetFields(flags | BindingFlags.DeclaredOnly));
        }

        public static bool ImplementsIEnumerable(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }
            
            foreach (Type curInterface in type.GetInterfaces())
            {
                if (curInterface.IsGenericType)
                {
                    if (curInterface.GetGenericTypeDefinition() == typeof(IEnumerable<>))
                    {
                        // if needed, you can also return the type used as generic argument
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Use this to get the value as underlying type of an enumeration, int, short or byte for example.
        /// </summary>
        public static object GetAsUnderlyingType(this Enum enval)
        {
            Type entype = enval.GetType();

            Type undertype = Enum.GetUnderlyingType(entype);

            return Convert.ChangeType(enval, undertype);
        }

        /// <summary>
        /// Tests wether type is XmlNode or derives from XmlNode
        /// </summary>
        public static bool IsXMLNode(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            return type.IsSubclassOf(typeof(XmlNode)) || type == typeof(XmlNode);
        }
        
        /// <summary>
        /// Tests wether type is XmlNode or derives from XmlNode
        /// </summary>
        public static bool IsXMLDoc(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            return type.IsSubclassOf(typeof(XMLDoc)) || type == typeof(XMLDoc);
        }

        /// <summary>
        /// Checks if field was marked with the custom NonSerializedXML attribute
        /// </summary>
        public static bool IsNotSerializedXML(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException("type");
            }

            object[] attributes = type.GetCustomAttributes(typeof(NonSerializedXMLAttribute), false);
            return attributes != null ? attributes.Length > 0 : false;
        }

        /// <summary>
        /// Checks if field was marked with the custom NonSerializedXML attribute
        /// </summary>
        public static bool IsNotSerializedXML(this FieldInfo fieldInfo)
        {
            if (fieldInfo == null)
            {
                throw new ArgumentNullException("fieldInfo");
            }

            object[] attributes = fieldInfo.GetCustomAttributes(typeof(NonSerializedXMLAttribute), false);
            return attributes != null ? attributes.Length > 0 : false;
        }

        public static string GetUniqueName<T>(this IEnumerable<T> _list, string _baseName)
        {
            if(_baseName == "")
                _baseName = "Object";

            int number = 1;

            if (_list != null)
            {
                lock (_list)
                {
                    bool found = true;
                    while (found) //if we found a match, it means we need to keep searching.
                    {
                        found = false; //we'll asume false until found

                        foreach (object obj in _list)
                        {
                            string currObjName = "Object";
                            try
                            {
                                currObjName = obj.GetType().GetProperty("Name").GetValue(obj).ToString();
                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Something went wrong getting the Name property of the list item.", ex);
                            }

                            if (currObjName == _baseName + number.ToString())
                            {
                                number++;
                                found = true;
                                break;
                            }
                        }
                    }
                }
            }

            return _baseName + number.ToString();
        }
    }
}
