﻿using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Timers;

namespace eThele.Essentials
{
    public class Exceptions : IDisposable
    {
        private Queue<ExceptionItem> exceptions;

        public delegate void ExceptionEventHandler(object sender, ExceptionItem exception);
        public event ExceptionEventHandler OnException;

        private const int LowPriorityTimeoutMs      = 300000;
        private const int MediumPriorityTimeoutMs   = 60000;
        private const int HighPriorityTimeoutMs     = 10000;

        private bool disposed = false;
        private bool loggingEnabled;
        private ExceptionPriority priority;
        private Timer saveTimer;

        /// <summary>
        /// Sets the global default priority of exceptions. Low means exceptions are written every 5 minutes,
        /// medium every 1 minute, and high means realtime. Priorities for exceptions take precedence to global priority.
        /// </summary>
        public ExceptionPriority Priority
        {
            get { return priority; }
            set
            {
                priority = value;
                if (saveTimer == null)
                    saveTimer = new Timer();
                switch (priority)
                {
                    case ExceptionPriority.Low:
                        saveTimer.Interval = LowPriorityTimeoutMs;
                        break;
                    case ExceptionPriority.Medium:
                        saveTimer.Interval = MediumPriorityTimeoutMs;
                        break;
                    case ExceptionPriority.High:
                        saveTimer.Interval = HighPriorityTimeoutMs;
                        break;
                }
            }
        }

        private XMLDoc logFile;
        private XmlNode logDetails;

        public Exceptions()
        {
            if (!disposed)
            {
                exceptions = new Queue<ExceptionItem>();
                loggingEnabled = false;
                bool nodeModified = false;
                try
                {
                    XmlNode loggingNode = Settings.TheeSettings["logging"];
                    if (loggingNode == null)
                        Settings.TheeSettings.Load();
                    loggingNode = Settings.TheeSettings["logging"];

                    if (loggingNode == null)
                    {
                        loggingNode = Settings.TheeSettings.Add("logging", "");
                        loggingNode.AddAttribute("enabled", true.ToString());
                        loggingNode.AddAttribute("level", Priority.ToString());
                        nodeModified = true;
                    }
                    else
                    {
                        if (loggingNode.Attributes.Contains(new XmlItem("enabled", "", 0)))
                        {
                            if (XmlNode.ParseBool(loggingNode, false, "enabled"))
                            {
                                loggingEnabled = true;
                            }
                        }
                        else
                        {
                            nodeModified = true;
                            loggingNode.AddAttribute("enabled", loggingEnabled.ToString());
                        }

                        if (loggingNode.Attributes.Contains(new XmlItem("level", "", 0)))
                        {
                            switch (XmlNode.ParseString(loggingNode, "low", "level"))
                            {
                                case "low":
                                    Priority = ExceptionPriority.Low;
                                    break;
                                case "medium":
                                    Priority = ExceptionPriority.Medium;
                                    break;
                                case "high":
                                    Priority = ExceptionPriority.High;
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            nodeModified = true;
                            loggingNode.AddAttribute("level", priority.ToString().ToLower());
                        }
                    }/* Avoid overwriting the settings file. The application won't function without a settings.xml file.
                else
                {// Setting doesn't exist in file - create it
                    loggingNode = Settings.TheeSettings.Add("Logging", "");
                    loggingNode.AddAttribute("enabled", loggingEnabled.ToString());
                    loggingNode.AddAttribute("level", priority.ToString().ToLower());
                    Settings.TheeSettings.Save();
                }*/
                    if (nodeModified)
                    {
                        Settings.TheeSettings.Save();
                    }
                }
                catch (Exception ex)
                {
                    EventLog.WriteEntry("Exceptions.Add", "Could not read settings file: " + ex, EventLogEntryType.Error);
                }
                if (saveTimer == null)
                    saveTimer = new Timer();
                //saveTimer.AutoReset = false;
                saveTimer.Elapsed += SaveTimerCallback;
            }
        }

        ~Exceptions()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (saveTimer != null)
            {
                saveTimer.Elapsed -= SaveTimerCallback;
                saveTimer.Stop();
                saveTimer.Dispose();
            }

            if (exceptions != null)
            {
                try
                {
                    exceptions.Clear();
                }
                catch (IndexOutOfRangeException ex)
                {
                    // Could not open log file - write to app event logs.
                    EventLog.WriteEntry(MethodBase.GetCurrentMethod().Name, "Could not clear exceptions: " + ex, EventLogEntryType.Error);
                }
            }
            loggingEnabled = false;
            exceptions = null;
            disposed = true;
        }

        public void Save()
        {
            bool loggingStatus = loggingEnabled;
            loggingEnabled = false;
            try
            {
                if (logFile != null)
                {
                    lock (logFile)
                    {
                        if (logFile != null)
                        {
                            DirectoryInfo logPath = new DirectoryInfo(Path.GetPathRoot(Environment.SystemDirectory) + "ProgramData\\Ethele\\RioSoft\\" 
                                + Assembly.GetEntryAssembly().GetName().Name + "\\Logs");
                            if (!logPath.Exists)
                                logPath.Create();
                            try
                            {
                                using (FileStream zipStream = new FileStream(logPath + "\\Archive.zip", FileMode.OpenOrCreate))
                                {
                                    using (ZipArchive zipArchive = new ZipArchive(zipStream, ZipArchiveMode.Update, true))
                                    {
                                        foreach (FileInfo oldLog in logPath.GetFiles("Incidents*.*"))
                                        {
                                            if ((oldLog.Extension != ".zip") && !(oldLog.Attributes.HasFlag(FileAttributes.Hidden)))
                                            {
                                                var entry = zipArchive.CreateEntryFromFile(oldLog.FullName, oldLog.Name, CompressionLevel.Optimal);
                                                using (BinaryWriter bw = new BinaryWriter(entry.Open()))
                                                {
                                                    bw.Write(File.ReadAllBytes(oldLog.FullName));
                                                    bw.Close();
                                                }
                                                File.Delete(oldLog.FullName);
                                            }
                                        }
                                        zipArchive.Dispose();
                                        zipStream.Close();
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
                            }

                            string fileName = string.Format("{0}\\Incidents_{1:yyyy_MM_dd_HH_mm_ss}.xml", logPath, DateTime.Now);
                            logFile.Save(fileName);
                            logFile.Dispose();
                            logFile = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Could not save log file, hence there is nowhere else to log to.
                if (!EventLog.SourceExists("RIO Server"))
                    EventLog.CreateEventSource("RIO Server", "Application");

                EventLog.WriteEntry("RIO Server", "Could not save log file: " + ex, EventLogEntryType.Error);
            }
            loggingEnabled = loggingStatus;
        }

        private static Exceptions exceptionsManager;
        /// <summary>
        /// This is handy if you don't want to create and manage your own object.
        /// </summary>
        public static Exceptions ExceptionsManager
        {
            get 
            {
                if (exceptionsManager == null)
                {
                    exceptionsManager = new Exceptions();
                }

                return exceptionsManager; 
            }
        }

        /// <summary>
        /// Not really necessary, but it does no harm calling it if you don't need the Exception manager anymore (when you close your application). 
        /// If you do call this and then use the ExceptionManager again, it will create a new object. You will have to register for the OnException event again.
        /// </summary>
        public static void DisposeManager()
        {
            if (exceptionsManager != null)
            {
                exceptionsManager.Dispose();
                exceptionsManager = null;
            }
        }

        public void Add(string _functionName, string _description, Exception _exception = null, ExceptionPriority _priority = ExceptionPriority.Low)
        {
            Add(new ExceptionItem(_functionName, _description, _exception, _priority));
        }

        public void Add(ExceptionItem _exceptionItem)
        {
            if (disposed)
            {
                EventLog.WriteEntry("Exceptions already disposed. Exception detail: ", _exceptionItem.Exception.Message, EventLogEntryType.Error);
            }
            else
            {
                if (exceptions == null)
                    exceptions = new Queue<ExceptionItem>();

                if (loggingEnabled)
                {
                    Trace.Write(_exceptionItem);
                    LogToFile(_exceptionItem);
                    if (saveTimer == null)
                        saveTimer = new Timer();
                    if (!(priority == ExceptionPriority.High)) // Global priority is not high.
                    {
                        if (priority != _exceptionItem.Priority) // Global and exception priority differ.
                        {
                            if (_exceptionItem.Priority == ExceptionPriority.High)
                                saveTimer.Interval = HighPriorityTimeoutMs; // Resets the timer to run only 10 seconds before saving to file.
                            else
                            {// Priorities differ, and exception is not high. Low makes no difference, but medium decrease interval.
                                if (_exceptionItem.Priority == ExceptionPriority.Medium)
                                    saveTimer.Interval = MediumPriorityTimeoutMs;
                            }
                        }// Else priorities match, do nothing - exception already added to queue.
                    }

                    if ((exceptions != null) && (exceptions.Count > 0) && (!saveTimer.Enabled))
                    {
                        saveTimer.Start();
                        saveTimer.Enabled = true;
                    }
                }
                else
                {
                    exceptions.Enqueue(_exceptionItem);
                }

                if (OnException != null)
                    OnException(this, _exceptionItem);
            }
        }

        private void LogToFile(ExceptionItem _exceptionItem)
        {
            try
            {
                if (logFile == null)
                    logFile = new XMLDoc("IncidentLog");
                string incidentName = "Incident" + _exceptionItem.TimeOfException.ToString("yyyyMMddHHmmssfff");
                logDetails = logFile.Add(incidentName, "");
                int i = 0;
                while ((logDetails == null) && (i < 42))
                {
                    logDetails = logFile.Add(incidentName + i++, "");
                }
                logDetails.AddAttribute("TimeStamp", _exceptionItem.TimeOfException.ToString("u"));
                logDetails.AddAttribute("FunctionName", _exceptionItem.FunctionName);
                logDetails.AddAttribute("IncidentDescription", _exceptionItem.Description);
                logDetails.AddAttribute("IncidentDetail", _exceptionItem.Exception.ToString());
            }
            catch (Exception ex)
            {
                // Could not open log file - write to app event logs.
                EventLog.WriteEntry(MethodBase.GetCurrentMethod().Name, "Could not save log file: " + ex, EventLogEntryType.Error);
            }
        }

        public Queue<ExceptionItem> ListOfExceptions
        {
            get
            {
                return exceptions;
            }
        }

        private void SaveTimerCallback(object sender, ElapsedEventArgs e)
        {
            SaveAndKillTimer();
        }
        private void SaveAndKillTimer()
        {
            if (saveTimer != null)
            {
                if (saveTimer.Enabled)
                {
                    saveTimer.Stop();
                    saveTimer.Enabled = false;
                }
            }
            Save();
        }
    }
}