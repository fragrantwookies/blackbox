﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eThele.Essentials
{
    public class ExceptionItem : INotifyPropertyChanged
    {
        private readonly string functionName;
        private readonly string description;
        private readonly DateTime timeOfException;

        private readonly Exception exception;
        private readonly ExceptionPriority priority;

        public ExceptionItem(string _functionName, string _description, Exception _exception, ExceptionPriority _priority)
        {
            timeOfException = DateTime.Now;
            functionName = _functionName;
            description = _description;
            exception = _exception ?? new Exception();
            priority = _priority;
        }

        public string FunctionName
        {
            get { return functionName; }
        }

        public string Description
        {
            get { return description; }
        }

        public DateTime TimeOfException
        {
            get { return timeOfException; }
        }

        public Exception Exception
        {
            get { return exception; }
        }

        public string ExceptionMessage
        {
            get { return exception != null ? exception.Message : ""; }
        }

        public ExceptionPriority Priority
        {
            get { return priority; }
        }
        
        protected bool expand = false;
        public bool Expand
        {
            get { return expand; }
            set { expand = value; PropertyChangedHandler("Expand"); }
        }

        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}", timeOfException, functionName, description, exception != null ? exception.Message : "");
        }

        #region PropertyChanged
        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }
        #endregion PropertyChanged
    }
}
