﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Management;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace Activator
{
    public partial class frmActivator : Form
    {
        public frmActivator()
        {
            InitializeComponent();

            try
            { 
            tsslMsg.Text = "Loading product key";
            Application.DoEvents();
            txtProductKey.Text = LoadProductKeyV2();
            txtActivationKey.Text = LoadActivationKey();
            tsslMsg.Text = "";
            Application.DoEvents();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unexpected exception: " + ex.Message);
            }
        }

        private string LoadProductKey()
        {
            string processorid = "FreePass";

            try
            {
                using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_Processor"))
                {
                    foreach (ManagementObject mobject in searcher.Get())
                    {
                        processorid = (string)mobject.Properties["ProcessorID"].Value;

                        if (processorid != "FreePass")
                            break;
                    }
                }
            }
            catch
            {
                MessageBox.Show("Unable to retrieve product key, please make sure you have Admin rights on this machine.");
            }

            return processorid;
        }

        private string LoadProductKeyV2()
        {
            string processorid = "FreePass";

            try
            {
                System.Diagnostics.Process proc = System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo() { FileName = "wmic", Arguments = "cpu get ProcessorId", UseShellExecute = false, RedirectStandardOutput = true, CreateNoWindow = true });
                while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine().Trim();
                    if (!string.IsNullOrWhiteSpace(line) && line != "ProcessorId")
                    {
                        processorid = line;
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Unable to retrieve product key V2: {0}", ex.Message));
            }

            return processorid;
        }

        private string LoadActivationKey()
        {
            string activationKey = "";

            using (RegistryKey servicekey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Ethele\\eNerve"))
            {
                if (servicekey != null)
                {
                    activationKey = (string)servicekey.GetValue("ActivationCode", "");

                    servicekey.Close();
                }
            }

            return activationKey;
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {
            ServiceController eNerveService = null;

            try
            {
                tsslMsg.Text = "Stopping RioSoftServer service";
                Application.DoEvents();

                eNerveService = new ServiceController("RioSoftServer");
                if (eNerveService != null)
                {
                    if (eNerveService.Status != ServiceControllerStatus.Stopped)
                    {
                        eNerveService.Stop();
                        eNerveService.WaitForStatus(ServiceControllerStatus.Stopped, TimeSpan.FromSeconds(5));

                        if (eNerveService.Status != ServiceControllerStatus.Stopped)
                            tsslMsg.Text = "Couldn't stop the RioSoftServer service";
                        else
                            tsslMsg.Text = "";
                        Application.DoEvents();
                    }
                }
            }
            catch
            {
                tsslMsg.Text = "Couldn't stop the RioSoftServer service";
                Application.DoEvents();
            }

            try
            {
                tsslMsg.Text = "Saving activation code";
                Application.DoEvents();

                using (RegistryKey servicekey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Ethele\\eNerve"))
                {

                    servicekey.SetValue("ActivationCode", txtActivationKey.Text);

                    servicekey.Close();
                }

                tsslMsg.Text = "";
                Application.DoEvents();
            }
            catch
            {
                tsslMsg.Text = "Unable to activate, please make sure you have Admin rights on this machine";
                Application.DoEvents();
            }

            try
            {
                tsslMsg.Text = "Starting RioSoftServer service";
                Application.DoEvents();

                if (eNerveService != null)
                {
                    eNerveService.Start();
                    eNerveService.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(5));

                    if (eNerveService.Status != ServiceControllerStatus.Running)
                        tsslMsg.Text = "Couldn't start the RioSoftServer service";
                    else
                        tsslMsg.Text = "Activation successful";
                    Application.DoEvents();
                }
            }
            catch
            {
                tsslMsg.Text = "Failed to start service.";
                Application.DoEvents();
            }
        }
    }
}
