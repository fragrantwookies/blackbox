﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using eNervePluginInterface;
using eThele.Essentials;

namespace DeviceOnlinePlugin
{
    public class DeviceOnlinePlugin : IDisposable, INerveEventPlugin
    {
        private Dictionary<string, DeviceOnlineTarget> Devices;
        private System.Timers.Timer timer;

        public string Author
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company;
            }
        }

        private List<CustomPropertyDefinition> customPropertyDefinitions = null;

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                if (customPropertyDefinitions == null)
                {
                    customPropertyDefinitions = new List<CustomPropertyDefinition>();
                    CustomPropertyDefinition Device = new CustomPropertyDefinition("Device", "Device", "Device", typeof(CustomPropertyList));
                    Device.ChildDefinitions.Add(new CustomPropertyDefinition("IPAddress", "IP Address", "IP Address", typeof(CustomProperty)));
                    Device.ChildDefinitions.Add(new CustomPropertyDefinition("Port", "Port", "Port", typeof(CustomProperty)));
                    Device.ChildDefinitions.Add(new CustomPropertyDefinition("Interval", "Interval (seconds)", "Interval", typeof(CustomProperty)));

                        CustomPropertyDefinition Children = new CustomPropertyDefinition("Device", "Device", "Device", typeof(CustomPropertyList));
                        Children.ChildDefinitions.Add(new CustomPropertyDefinition("IPAddress", "IP Address", "IP Address", typeof(CustomProperty)));
                        Children.ChildDefinitions.Add(new CustomPropertyDefinition("Port", "Port", "Port", typeof(CustomProperty)));
                        Children.ChildDefinitions.Add(new CustomPropertyDefinition("Interval", "Interval (seconds)", "Interval", typeof(CustomProperty)));

                            CustomPropertyDefinition GrandChildren = new CustomPropertyDefinition("Device", "Device", "Device", typeof(CustomPropertyList));
                            GrandChildren.ChildDefinitions.Add(new CustomPropertyDefinition("IPAddress", "IP Address", "IP Address", typeof(CustomProperty)));
                            GrandChildren.ChildDefinitions.Add(new CustomPropertyDefinition("Port", "Port", "Port", typeof(CustomProperty)));
                            GrandChildren.ChildDefinitions.Add(new CustomPropertyDefinition("Interval", "Interval (seconds)", "Interval", typeof(CustomProperty)));

                            Children.ChildDefinitions.Add(GrandChildren);

                        Device.ChildDefinitions.Add(Children);

                    customPropertyDefinitions.Add(Device);
                }

                return customPropertyDefinitions;
            }
        }

        public string Description
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
            }
        }

        private bool enabled;
        public bool Enabled { get { return enabled; } set
            {
                enabled = value;
                timer.Enabled = enabled;
            } }

        public string[] EventNames
        {
            get
            {
                //return new string[] { "DeviceOnline" };
                return new string[] { "DevicePingFailed", "DevicePortFailed", "DeviceRecovered" };
            }
        }

        private string fileName;

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public bool Online { get; set; }

        private CustomProperties properties;
        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
        }

        public string Version
        {
            get
            {
                return Assembly.GetCallingAssembly().GetName().Version.ToString();
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Populate events directly on the UI to simplify conditions.
        /// </summary>
        public string[] MiscellaneousEventNames
        {
            get
            {
                return new string[] { "DevicePingFailed", "DevicePortFailed", "DeviceRecovered" };
            }
        }

        public event EventHandler DeviceOffline;
        public event EventHandler DeviceOnline;
        public event EventHandler<PluginErrorEventArgs> Error;
        public event EventHandler<PluginTriggerEventArgs> Trigger;

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        public void Start()
        {
            if (timer == null)
            {
                timer = new System.Timers.Timer(1000);
                timer.AutoReset = false;
                timer.Elapsed += Timer_Elapsed;
            }
            timer.Start();

            Online = true;
            if (DeviceOnline != null)
                DeviceOnline(this, new EventArgs());
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (timer == null)
                timer = new System.Timers.Timer(1000);

            if (Enabled)
            {
                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    timer.Stop();
                    if (Devices == null)
                        Devices = new Dictionary<string, DeviceOnlineTarget>();
                    foreach (ICustomProperty property in Properties.GetProperties())
                    {
                        if (property.PropertyName == "Device")
                        {
                            CustomPropertyList attributes = property as CustomPropertyList;
                            if (attributes != null)
                            {
                                CheckTarget(attributes, Devices);
                            }
                        }
                    }
                    timer.Start();
                }
                else
                {
                    if (DeviceOffline != null)
                        DeviceOffline(this, new PluginErrorEventArgs(MethodBase.GetCurrentMethod().Name, "Network unavailable.", new Exception()));
                }
            }
            else
            {
                timer.Enabled = false;
            }
        }

        private void CheckTarget(CustomPropertyList attributes, Dictionary<string, DeviceOnlineTarget> targets)
        {
            bool result = false;
            DeviceOnlineTarget target;
            if (targets.ContainsKey(attributes["IPAddress"]))
            {
                target = targets[attributes["IPAddress"]];
            }
            else
            {
                target = new DeviceOnlineTarget()
                {
                    Children = new Dictionary<string, DeviceOnlineTarget>(),
                    LastCheck = DateTime.Now,
                    Online = true
                };
                targets.Add(attributes["IPAddress"], target);
            }
            if (string.IsNullOrEmpty(attributes["IPAddress"]))
            {
                if (Error != null)
                    Error(this, new PluginErrorEventArgs(MethodBase.GetCurrentMethod().Name, "Invalid IP address: " + attributes["IPAddress"], new Exception()));
            }
            else
            {
                int interval;
                string err;
                if (!TimeSpanParser.TryParse(attributes["Interval"], out interval, out err))
                {
                    /*if (Error != null) /// Interval is optional, so not an error if missing.
                        Error(this, new PluginErrorEventArgs(MethodBase.GetCurrentMethod().Name, "Invalid interval for address: " + attributes["IPAddress"] + 
                            ". Assuming 60.", new Exception()));*/
                    interval = 60000;
                }
                if ((DateTime.Now - target.LastCheck) < TimeSpan.FromMilliseconds(interval))
                    result = true; // Parent interval hasn't timed out, check children.
                else
                {
                    int port;
                    //target.LastCheck = DateTime.Now;
                    if (int.TryParse(attributes["Port"], out port))
                    {// test port
                        using (TcpClient tcp = new TcpClient())
                        {
                            try
                            {
                                tcp.Connect(attributes["IPAddress"], port);
                                result = true;
                                target.LastCheck = DateTime.Now;
                                if (!target.Online)
                                    if (Trigger != null)
                                        Trigger(this, new PluginTriggerEventArgs(new DeviceOnlineTrigger()
                                        {
                                            TriggerID = Guid.NewGuid(),
                                            DeviceName = Name,
                                            DetectorName = EventNames[2],
                                            TriggerDT = DateTime.Now,
                                            EventName = attributes["IPAddress"],
                                            DetectorType = DetectorType.Digital,
                                            State = result
                                        }));
                                target.Online = true;
                            }
                            catch (SocketException)
                            {
                                result = false;
                                if (target.Online)
                                    if (Trigger != null)
                                        Trigger(this, new PluginTriggerEventArgs(new DeviceOnlineTrigger()
                                        {
                                            TriggerID = Guid.NewGuid(),
                                            DeviceName = Name,
                                            DetectorName = EventNames[1],
                                            TriggerDT = DateTime.Now,
                                            EventName = attributes["IPAddress"],
                                            DetectorType = DetectorType.Digital,
                                            State = false
                                        }));
                                target.Online = false;
                            }
                            catch (Exception ex)
                            {
                                result = false;
                                if (Error != null)
                                    Error(this, new PluginErrorEventArgs(MethodBase.GetCurrentMethod().Name, "Failed to check port " + port + " on address: " 
                                        + attributes["IPAddress"] + ". Assuming 60.", ex));
                            }
                        }
                    }
                    else
                    {//test PING
                        using (Ping ping = new Ping())
                        {
                            try
                            {
                                switch (ping.Send(attributes["IPAddress"]).Status)
                                {
                                    case IPStatus.Success:
                                        {
                                            result = true;
                                            target.LastCheck = DateTime.Now;
                                            if (!target.Online)
                                            {
                                                if (Trigger != null)
                                                    Trigger(this, new PluginTriggerEventArgs(new DeviceOnlineTrigger()
                                                    {
                                                        TriggerID = Guid.NewGuid(),
                                                        DeviceName = Name,
                                                        DetectorName = EventNames[2],
                                                        TriggerDT = DateTime.Now,
                                                        EventName = attributes["IPAddress"],
                                                        DetectorType = DetectorType.Digital,
                                                        State = result
                                                    }));
                                                target.Online = true;
                                            }
                                        }
                                        break;
                                    default:
                                        {
                                            result = false;
                                            if (target.Online)
                                            {
                                                if (Trigger != null)
                                                    Trigger(this, new PluginTriggerEventArgs(new DeviceOnlineTrigger()
                                                    {
                                                        TriggerID = Guid.NewGuid(),
                                                        DeviceName = Name,
                                                        DetectorName = EventNames[0],
                                                        TriggerDT = DateTime.Now,
                                                        EventName = attributes["IPAddress"],
                                                        DetectorType = DetectorType.Digital,
                                                        State = false
                                                    }));
                                                target.Online = false;
                                            }
                                        }
                                        break;
                                }
                            }
                            catch (Exception ex)
                            {
                                result = false;
                                if (Error != null)
                                    Error(this, new PluginErrorEventArgs(MethodBase.GetCurrentMethod().Name, "Failed to ping address: " + attributes["IPAddress"], ex));
                            }
                        }
                    }
                }
            }
            if (result)
            {
                foreach (ICustomProperty child in attributes.Properties)
                {
                    if (child.PropertyName == "Device")
                    {
                        CustomPropertyList eienskappe = child as CustomPropertyList;
                        if (eienskappe != null)
                        {
                            CheckTarget(eienskappe, target.Children);
                        }
                    }
                }
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            Online = false;
            if (timer != null)
            {
                timer.Elapsed -= Timer_Elapsed;
                timer.Dispose();
            }
        }
    }
}
