﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeviceOnlinePlugin
{
    public class DeviceOnlineTarget
    {
        public Dictionary<string, DeviceOnlineTarget> Children;
        public DateTime LastCheck;
        public bool Online;
    }
}
