﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Linq;

using eNerve.DataTypes;

namespace BlackBoxEngine
{   
    /// <summary>
    /// Serves as base for Server and Client.
    /// Message Protocol: 
    /// Header - STX (1 byte) + MessageType (2 bytes: most significant bit indicates if data is compressed, rest is MessageType) + MessageLength (4 bytes)
    /// Msg block (MessageLength in bytes). Data is any serialised object in xml format.
    /// Footer - CRC (4 bytes) + ETX (1 byte)
    /// Numbers are encoded to little endian
    /// </summary>
    public abstract class CommsNode : IDisposable
    {
        protected bool listen = false;
        private const int keepAlive = 10000;

        private bool supportsCompression = true;
        public bool SupportCompression { get { return supportsCompression; } set { supportsCompression = value; } }

        protected Dictionary<TcpClient, bool> sendCompressedData = new Dictionary<TcpClient, bool>();
        
        /// <summary>STX + Header(msg type + msg length</summary>
        protected const int headerSize = 7;

        /// <summary>CRC + ETX</summary>
        protected const int footerSize = 5;

        #region Event Args
        public class MessageEventArgs : EventArgs
        {
            public MessageEventArgs(TcpClient client)
                : base()
            {
                Client = client;
            }

            public TcpClient Client { get; set; }
        }

        public class BinaryMessageEventArgs : MessageEventArgs
        {
            public BinaryMessageEventArgs(TcpClient client, byte[] message)
                : base(client)
            {
                BinaryData = message;
            }

            public byte[] BinaryData { get; set; }
        }

        public class StringMessageEventArgs : MessageEventArgs
        {
            public StringMessageEventArgs(TcpClient client, string message)
                : base(client)
            {
                Message = message;
            }

            public string Message { get; set; }
        }

        public class ObjectMessageEventArgs : MessageEventArgs
        {
            public ObjectMessageEventArgs(TcpClient client, MessageType messageType, object message)
                : base(client)
            {
                Message_Type = messageType;
                Message = message;
            }

            public MessageType Message_Type { get; set; }
            public object Message { get; set; }
        }
        #endregion Event Args

        public event EventHandler<BinaryMessageEventArgs> OnBinaryMessageReceived;
        public event EventHandler<StringMessageEventArgs> OnStringMessageReceived;
        public event EventHandler<ObjectMessageEventArgs> OnObjectMessageReceived;

        #region Cleanup
        ~CommsNode()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);            

            listen = false;

            if (sendCompressedData != null)
                sendCompressedData.Clear();
            
            if (outBuffer != null && outBuffer.Count > 0)
            {
                for (int index = outBuffer.Count - 1; index >= 0; index--)
                {
                    var item = outBuffer.ElementAt(index);
                    item.Value.Dispose();
                }

                outBuffer.Clear();
            }
        }

        protected virtual void DisconnectClient(TcpClient _client)
        {
            if ((_client != null) && (_client.Connected))
            {
                if (outBuffer != null)
                {
                    int clientHash = _client.GetHashCode();
                    if (outBuffer.ContainsKey(clientHash))
                    {
                        outBuffer[clientHash].Dispose();
                        outBuffer.Remove(clientHash);
                    }
                }

                _client.Close();
            }

            _client = null;
        }
        #endregion Cleanup

        #region Receiving
        protected virtual void Read(object _client)
        {
            TcpClient client = null;
            NetworkStream clientStream = null;

            try
            {
                byte[] buffer = null;
                
                client = (TcpClient)_client;
                clientStream = client.GetStream();

                DateTime lastMsg = DateTime.Now;
                DateTime lastEchoSent = DateTime.Now;

                while (listen /*&& (DateTime.Now - lastMsg).TotalMilliseconds < keepAlive*/)
                {
                    int bytesRead;

                    try
                    {
                        //if (clientStream.DataAvailable)
                        {
                            bytesRead = 0;
                            byte[] packet = new byte[8192];
                            if (client.Connected && clientStream.CanRead)
                            {
                                bytesRead = clientStream.Read(packet, 0, packet.Length);
                            }

                            if (bytesRead == 0)
                            {
                                //the client has disconnected from the server
                                break;
                            }
                            else
                            {
                                lastMsg = DateTime.Now;

                                byte[] data = null;

                                if (buffer == null)
                                {
                                    data = new byte[bytesRead];
                                    Array.Copy(packet, data, bytesRead);
                                }
                                else
                                {
                                    data = new byte[buffer.Length + bytesRead];
                                    Array.Copy(buffer, data, buffer.Length);
                                    Array.Copy(packet, 0, data, buffer.Length, bytesRead);
                                }

                                if (data.Length > 0)
                                {
                                    CheckForMessage(ref data, client);
                                }

                                buffer = data;
                            }
                        }
                        //else if ((DateTime.Now - lastMsg).TotalMilliseconds * 2 > keepAlive //If we get to half the keepalive time, we should start sending echo msgs
                        //    && (DateTime.Now - lastEchoSent).TotalMilliseconds * 5 > keepAlive) //but don't overdo it
                        //{
                        //    SendMessage(client, MessageType.Echo);
                        //    lastEchoSent = DateTime.Now;

                        //    Thread.Sleep(50);
                        //}
                    }
                    catch (IOException ex)
                    {
                        //Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Client disconnected", ex);
                        //listen = false;
                        break;
                    }
                    catch (Exception ex)
                    {
                        listen = false;
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Unexpected disconnection", ex);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (clientStream != null)
                {
                    clientStream.Close();
                }

                if (client != null)
                {
                    DisconnectClient(client);
                }
            }
        }

        /// <summary>
        /// Check if there is a full message available in the buffer. If there is, the message will be removed from the buffer and processed.
        /// </summary>
        /// <param name="_buffer"></param>
        /// <returns>The number of complete messages found and processed</returns>
        protected virtual int CheckForMessage(ref byte[] _buffer, TcpClient _client, int initialSTXOffset = 0)
        {
            int result = 0;
            byte[] newBuffer = null;

            if (_buffer.Length > 0)
            {
                int stxIndex = initialSTXOffset;
                while (_buffer[stxIndex] != 0x02 && stxIndex < _buffer.Length)
                    stxIndex++;

                if (stxIndex < _buffer.Length)
                {
                    int bytesToRemove = stxIndex;

                    if (_buffer.Length - stxIndex >= headerSize)
                    {
                        int msgLength = 0;
                        if (!BitConverter.IsLittleEndian)
                        {
                            byte[] msgLenthData = new byte[4];
                            Array.Copy(_buffer, stxIndex + 3, msgLenthData, 0, 4);
                            Array.Reverse(msgLenthData);
                            msgLength = BitConverter.ToInt32(msgLenthData, 0);
                        }
                        else
                            msgLength = BitConverter.ToInt32(_buffer, stxIndex + 3);

                        if (_buffer.Length - stxIndex >= msgLength + headerSize + footerSize) //we have all the data.
                        {
                            bytesToRemove = stxIndex + msgLength + headerSize + footerSize;

                            if (_buffer[bytesToRemove - 1] == 0x03) //if the last byte is not ETX we know something went wrong, so we should find the next STX
                            {
                                byte[] msg = new byte[msgLength + headerSize + footerSize];
                                Array.Copy(_buffer, stxIndex, msg, 0, msgLength + headerSize + footerSize);

                                Thread processMsgThread = new Thread(delegate() { ProcessMessage(new ClientMessage(_client, msg)); });
                                processMsgThread.Start();

                                result++;
                            }
                            else
                                bytesToRemove = stxIndex + 1; //don't discard the entire message, just discard everything up to and including the STX, next iteration will try to find the next STX.
                        }
                    }

                    //create a buffer for the left over data.
                    newBuffer = new byte[_buffer.Length - bytesToRemove];
                    Array.Copy(_buffer, bytesToRemove, newBuffer, 0, _buffer.Length - bytesToRemove);

                    if (result > 0 || bytesToRemove == stxIndex + 1) // we will only check for another message if we managed to get 1 or if we didn't get an ETX where expected.
                        result += CheckForMessage(ref newBuffer, _client);
                }
            }

            _buffer = newBuffer;

            return result;
        }

        protected class ClientMessage
        {
            private TcpClient client;
            private byte[] data;

            public ClientMessage(TcpClient _client, byte[] _data)
            {
                client = _client;
                data = _data;
            }

            public TcpClient Client
            {
                get { return client; }
                set { client = value; }
            }

            public byte[] Data
            {
                get { return data; }
                set { data = value; }
            }
        }

        protected virtual void ProcessMessage(ClientMessage _clientMessage)
        {
            try
            {
                byte[] msg = _clientMessage.Data;

                if (msg.Length >= headerSize)
                {
                    ushort msgTypeID = 0; //msb = compression, rest = message type
                    if (!BitConverter.IsLittleEndian)
                    {
                        byte[] msgTypeData = new byte[2];
                        Array.Copy(msg, 1, msgTypeData, 0, 2);
                        Array.Reverse(msgTypeData);
                        msgTypeID = BitConverter.ToUInt16(msgTypeData, 0);
                    }
                    else
                        msgTypeID = BitConverter.ToUInt16(msg, 1);

                    int msgLength = 0;
                    if (!BitConverter.IsLittleEndian)
                    {
                        byte[] msgLenthData = new byte[4];
                        Array.Copy(msg, 3, msgLenthData, 0, 4);
                        Array.Reverse(msgLenthData);
                        msgLength = BitConverter.ToInt32(msgLenthData, 0);
                    }
                    else
                        msgLength = BitConverter.ToInt32(msg, 3);

                    if (msg.Length >= msgLength + headerSize + footerSize)
                    {
                        bool compressedData = ((msgTypeID & 0x8000) >> 15) == 1; //Get the compression flag
                        msgTypeID = (ushort)(msgTypeID & 0x7FFF); //Get rid of the compression flag
                        if(Enum.IsDefined(typeof(MessageType), msgTypeID))
                        {
                            MessageType msgType = (MessageType)msgTypeID;

                            uint crc = CRC.CalcCRC32(msg, 1, msg.Length - footerSize - 1); //Data block + message type + length

                            if (!BitConverter.IsLittleEndian) //no need to worry about reversing the msg here, this part of the data doesn't get used to calc the CRC
                                Array.Reverse(msg, msg.Length - footerSize, 4);
                            uint crcField = BitConverter.ToUInt32(msg, msg.Length - footerSize);

                            if (crc != crcField)
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "CRC mismatch");
                            else
                            {
                                object objMsg = null;

                                if (msgLength > 0)
                                {
                                    if (msgType == MessageType.Binary)
                                    {
                                        if(OnBinaryMessageReceived != null)
                                        {
                                            byte[] msgDataPortion = new byte[msgLength];
                                            Array.Copy(msg, headerSize, msgDataPortion, 0, msgLength);
                                            OnBinaryMessageReceived(this, new BinaryMessageEventArgs(_clientMessage.Client, compressedData ? Compression.Decompress(msgDataPortion) : msgDataPortion));
                                        }

                                        return;
                                    }
                                    else
                                    {
                                        string strMsg;
                                        if (compressedData)
                                            strMsg = Encoding.UTF8.GetString(Compression.Decompress(msg, headerSize, msgLength));
                                        else
                                            strMsg = Encoding.UTF8.GetString(msg, headerSize, msgLength);

                                        if (msgType == MessageType.String)
                                        {
                                            if (OnStringMessageReceived != null)
                                                OnStringMessageReceived(this, new StringMessageEventArgs(_clientMessage.Client, strMsg));

                                            return;
                                        }
                                        else
                                        {
                                            objMsg = XMLSerializer.Deserialize(strMsg);
                                        }
                                    }
                                }

                                if (msgType == MessageType.Echo)
                                    SendMessage(_clientMessage.Client, MessageType.EchoReturn);
                                else if (msgType == MessageType.RespondCompressionSupportTrue || msgType == MessageType.RespondCompressionSupportFalse)
                                {
                                    if (!sendCompressedData.ContainsKey(_clientMessage.Client))
                                        sendCompressedData.Add(_clientMessage.Client, (msgType == MessageType.RespondCompressionSupportTrue));

                                    sendCompressedData[_clientMessage.Client] = (msgType == MessageType.RespondCompressionSupportTrue);
                                }
                                else if (msgType == MessageType.RequestCompressionSupport)
                                    SendMessage(_clientMessage.Client, MessageType.RespondCompressionSupportTrue);
                                else if (OnObjectMessageReceived != null && msgType != MessageType.EchoReturn)
                                    OnObjectMessageReceived(this, new ObjectMessageEventArgs(_clientMessage.Client, msgType, objMsg));
                            }
                        }
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Receiving

        #region Send message
        public abstract bool SendMessage(MessageType _messageType, object _message = null);

        protected virtual bool SendMessage(TcpClient _client, byte[] _message)
        {
            bool result = false;
            //NetworkStream clientStream = null;

            try
            {
                if (_client.Connected)
                {
                    int clientHashCode = _client.GetHashCode();

                    if (outBuffer == null)
                        outBuffer = new Dictionary<int, OutBuffer>();

                    if (!outBuffer.ContainsKey(clientHashCode))
                        outBuffer.Add(clientHashCode, new OutBuffer(_client));

                    outBuffer[clientHashCode].BufferQ.Enqueue(_message);

                    if (!outBuffer[clientHashCode].Busy)
                    {
                        Thread processOutThread = new Thread(delegate() { ProcessOutBuffer(clientHashCode); });
                        processOutThread.Start();
                    }

                    //clientStream = _client.GetStream();

                    //if (_message != null && clientStream != null)
                    //{
                        
                    //    clientStream.Write(_message, 0, _message.Length);

                    //    result = true;
                    //}
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error while sending message", ex);
                result = false;
            }
            //finally
            //{
            //    if (clientStream != null)
            //        clientStream.Flush();
            //}

            return result;
        }

        private class OutBuffer : IDisposable
        {
            public bool Busy { get; set; }

            public TcpClient Client { get; set; }

            private Queue<byte[]> bufferQ;
            public Queue<byte[]> BufferQ { get{return bufferQ;} }

            public OutBuffer(TcpClient _client)
            {
                Busy = false;
                Client = _client;
                bufferQ = new Queue<byte[]>();
            }

            ~OutBuffer()
            {
                Dispose(false);
            }

            public void Dispose()
            {
                Dispose(true);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (disposing)
                    GC.SuppressFinalize(this);

                if(bufferQ != null && bufferQ.Count > 0)
                {
                    bufferQ.Clear();
                }
            }
        }

        private Dictionary<int, OutBuffer> outBuffer;

        /// <summary>
        /// We want to make sure we write to the network stream synchronously and preferably in chuncks of 8kB.
        /// </summary>
        /// <param name="_clientHashCode"></param>
        private void ProcessOutBuffer(int _clientHashCode)
        {
            if (outBuffer.ContainsKey(_clientHashCode))
            {
                OutBuffer buffer = outBuffer[_clientHashCode];
                NetworkStream clientStream = null;

                if (!buffer.Busy) //Get out if it is already busy
                {   
                    buffer.Busy = true;

                    try
                    {
                        clientStream = buffer.Client.GetStream();

                        int byteCount = 0;
                        int leftoverBytes = 0;
                        byte[] packet = new byte[8192];
                        byte[] msg = null;

                        while(leftoverBytes > 0 || buffer.BufferQ.Count > 0)
                        {
                            if (leftoverBytes > 0)
                            {
                                int lengthToCopy = leftoverBytes > packet.Length - byteCount ? packet.Length - byteCount : leftoverBytes;
                                Array.Copy(msg, msg.Length - leftoverBytes, packet, byteCount, lengthToCopy);
                                byteCount += lengthToCopy;
                                leftoverBytes -= lengthToCopy;
                            }

                            while(byteCount < packet.Length && buffer.BufferQ.Count > 0)
                            {
                                msg = buffer.BufferQ.Dequeue();
                                if (msg != null) 
                                {
                                    int lengthToCopy = msg.Length > packet.Length - byteCount ? packet.Length - byteCount : msg.Length;
                                    Array.Copy(msg, 0, packet, byteCount, lengthToCopy);
                                    byteCount += lengthToCopy;
                                    leftoverBytes = msg.Length - lengthToCopy;
                                }
                            }

                            if (byteCount > 0 && clientStream != null && clientStream.CanWrite)
                            {
                                clientStream.Write(packet, 0, byteCount);
                                byteCount = 0;
                                //Thread.Sleep(100);
                            }

                            if (leftoverBytes <= 0 && buffer.BufferQ.Count <= 0)
                                Thread.Sleep(500);
                        }
                    }
                    catch(Exception ex)
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error while sending message", ex);                
                    }
                    finally
                    {
                        //clientStream.Flush();
                        buffer.Busy = false;
                    }
                }
            }
        }

        public virtual bool SendMessage(TcpClient _client, MessageType _messageType, bool _dontCompress, object _message = null)
        {
            bool result = false;

            try
            {
                if ((_client != null) && (_client.Connected))
                {
                    bool compress = false;

                    if (!_dontCompress)
                    {
                        if (sendCompressedData.ContainsKey(_client))
                            compress = sendCompressedData[_client];
                        else if (_messageType != MessageType.RequestCompressionSupport)
                        {
                            if (sendCompressedData != null)
                                sendCompressedData.Add(_client, false);
                            SendMessage(_client, MessageType.RequestCompressionSupport); //We'll send this message without compression, but at least we'll be able to use compression in future if this client supports it.
                        }
                    }

                    byte[] msg = null;
                    byte[] objbuffer = null;

                    if (_message != null)
                    {
                        if (_messageType == MessageType.Binary)
                        {
                            objbuffer = (byte[])_message;
                        }
                        else if (_messageType == MessageType.String)
                        {
                            objbuffer = Encoding.UTF8.GetBytes((string)_message);
                        }
                        else
                        {
                            objbuffer = Encoding.UTF8.GetBytes(XMLSerializer.Serialize(_message));

                            //MemoryStream stream = new MemoryStream();
                            //BinaryFormatter binaryFormatter = new BinaryFormatter();
                            //binaryFormatter.Serialize(stream, _message);                        

                            //objbuffer = stream.ToArray();
                        }

                        if (compress && objbuffer != null)
                            objbuffer = Compression.Compress(objbuffer);
                    }

                    int objBufferLength = 0;
                    if (objbuffer != null)
                        objBufferLength = objbuffer.Length;

                    msg = new byte[objBufferLength + headerSize + footerSize];

                    //Header
                    msg[0] = 0x02;
                    byte[] msgTypeID = BitConverter.GetBytes((ushort)(((ushort)_messageType) | (compress ? 0x8000 : 0x0))); //Add compression flag
                    if (!BitConverter.IsLittleEndian)
                        Array.Reverse(msgTypeID);
                    byte[] msgLength = BitConverter.GetBytes(objBufferLength);
                    if (!BitConverter.IsLittleEndian)
                        Array.Reverse(msgLength);
                    Array.Copy(msgTypeID, 0, msg, 1, 2);
                    Array.Copy(msgLength, 0, msg, 3, 4);

                    //Data block
                    if (objbuffer != null)
                        Array.Copy(objbuffer, 0, msg, headerSize, objBufferLength);

                    //Footer
                    uint crc = CRC.CalcCRC32(msg, 1, msg.Length - footerSize - 1); //Data block + Type + Length
                    byte[] crcBytes = BitConverter.GetBytes(crc);
                    if (!BitConverter.IsLittleEndian)
                        Array.Reverse(crcBytes);
                    Array.Copy(crcBytes, 0, msg, msg.Length - footerSize, 4);
                    msg[msg.Length - 1] = 0x03;

                    result = SendMessage(_client, msg);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                result = false;
            }

            return result;
        }
        public virtual bool SendMessage(TcpClient _client, MessageType _messageType, object _message = null)
        {
            return SendMessage(_client, _messageType, false, _message);
        }

        public virtual bool SendBinaryMessage(TcpClient _client, byte[] _message, bool _dontCompress = false)
        {
            return SendMessage(_client, MessageType.Binary, _dontCompress, _message);
        }

        public abstract bool SendBinaryMessage(byte[] _message, bool _dontCompress = false);        
        #endregion Send message
    }
}
