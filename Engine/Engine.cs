﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using BlackBoxAPI;
using eNervePluginInterface;
using eThele.Essentials;
using eNerve.DataTypes;
using System.Diagnostics;

namespace BlackBoxEngine
{
    /// <summary>
    /// Primarily giving meaning to the events from devices by looking at the configurations and thresholds.
    /// It will make decisions on when to raise alarms.
    /// It will then act upon it as specified in EventActions file.
    /// </summary>
    public class Engine : IDisposable
    {
        private bool running = false;

        private Guid serverID = Guid.Empty;
        private string serverName = "";

        protected BlackBoxManager blackBoxManager;
        protected Bandaid bandaid;
        protected Detectors detectors = null;
        protected ConditionsEngine conditionsEngine = null; //This will make the decision to raise an alarm or not
        protected EventActions eventActions = null; //When an alarm was raised, this will tell us what to do about it.
        protected MapDoc map_Doc = null;

        protected List<GroupAlarm> unresolvedAlarms;
        public List<GroupAlarm> UnresolvedAlarms { get { return unresolvedAlarms; } }

        public event EventHandler<AlarmEventArgs> OnAlarm;
        public event EventHandler<GroupAlarmUpdateEventArgs> OnGroupAlarmUpdate;
        public event EventHandler<GroupAlarmUpdateListEventArgs> OnGroupAlarmUpdateList;
        public event EventHandler<PropertyUpdateEventArgs> AlarmPropertyUpdated;
        //public event PropertyUpdateEventHandler AlarmPropertyUpdated;
        public event EventHandler<AlarmResolvedEventArgs> AlarmResolveItemAdded;

        public event EventHandler<PropertyUpdateEventArgs> GroupAlarmPropertyUpdated;
        public event EventHandler<AlarmResolvedEventArgs> GroupAlarmResolveItemAdded;

        public delegate void SiteInstructionEventHandler(object sender, SiteInstructionStep siteInstructionStep);
        public event SiteInstructionEventHandler SiteInstructionUpdated;
        public delegate void AlarmImageEventHandler(object sender, List<EventImage> alarmImages);
        public event AlarmImageEventHandler OnAlarmImages;

        //public delegate void DeviceMacEventHandler(object sender, string _deviceMac);
        //public event DeviceMacEventHandler DeviceOnline;
        //public event DeviceMacEventHandler DeviceOffline;

        public event EventHandler<StatusMessageEventArgs> OnStatusMessage;

        private List<RioDevice> onlineDevices = new List<RioDevice>();
        public List<RioDevice> OnlineDevices { get { return onlineDevices; } }

        private List<ICameraServerPlugin> cameraServers;

        private Users users = null;
        public Users Users { get { return users; } }

        protected DBInterface db;
        public DBInterface DB { get { return db; } }
        public ConnectionDetails dbDetails = new ConnectionDetails();
        public event System.Data.StateChangeEventHandler DBConnectionStateChanged;

        public event PropertyUpdateEventHandler OccurrenceBookPropertyUpdated;

        protected PluginManager pluginManager;
        protected ReportManager reportManager;

        public Engine()
        {       
            users = new Users();

            unresolvedAlarms = new List<GroupAlarm>();
        }

        #region Cleanup
        ~Engine()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (blackBoxManager != null)
            {
                blackBoxManager.Stop();
                DeregisterBlackBoxEventHandlers();
                blackBoxManager.Dispose();
                blackBoxManager = null;
            }

            if (pluginManager != null)
            {
                pluginManager.OnAlarmTrigger -= PluginManager_AlarmTrigger;
                pluginManager.ImageCaptured -= pluginManager_ImageCaptured;
                pluginManager.PluginStatus -= pluginManager_PluginStatus;
                //pluginManager.PluginOnline -= pluginManager_PluginOnline;
                //pluginManager.PluginOffline -= pluginManager_PluginOffline;
                pluginManager.Dispose();
                pluginManager = null;
            }

            if (reportManager != null)
            {
                reportManager.Dispose();
                reportManager = null;
            }

            if (detectors != null)
            {
                detectors.Dispose();
                detectors = null;
            }

            if (conditionsEngine != null)
            {
                conditionsEngine.Dispose();
                conditionsEngine = null;
            }

            if (eventActions != null)
            {
                eventActions.Dispose();
                eventActions = null;
            }

            if (unresolvedAlarms != null)
            {
                for (int i = unresolvedAlarms.Count - 1; i >= 0; i--)
                {
                    GroupAlarm alarm = unresolvedAlarms[i];

                    unresolvedAlarms.RemoveAt(i);

                    if (alarm != null)
                    {
                        alarm.Escalated -= GroupAlarm_Escalated;

                        alarm.Dispose();
                        alarm = null;
                    }
                }
            }

            if (map_Doc != null)
                map_Doc.Dispose();

            if(users != null)
                users.Dispose();

            if (db != null)
            {
                db.ConnectionStateChanged -= db_ConnectionStateChanged;
                db.Dispose();
                db = null;
            }
        }
        #endregion Cleanup

        #region Start & Stop
        public void Start()
        {
            try
            {
                running = true;

                lastAlarmNumber = XmlNode.ParseInteger(Settings.TheeSettings["LastAlarmNumber"], 0);

                serverID = ServerID;
                serverName = ServerName;

                if (pluginManager != null)
                {
                    pluginManager.OnAlarmTrigger -= PluginManager_AlarmTrigger;
                    pluginManager.ImageCaptured -= pluginManager_ImageCaptured;
                    pluginManager.PluginStatus -= pluginManager_PluginStatus;
                    pluginManager.Dispose();
                    pluginManager = null;
                }

                if (detectors != null)
                    detectors.Dispose();
                detectors = new Detectors();

                pluginManager = new PluginManager(Devices_Doc);
                pluginManager.OnAlarmTrigger += PluginManager_AlarmTrigger;
                pluginManager.ImageCaptured += pluginManager_ImageCaptured;
                pluginManager.PluginStatus += pluginManager_PluginStatus;
                pluginManager.Start();
                
                if (reportManager != null)
                {
                    reportManager.Dispose();
                }
                reportManager = new ReportManager();

                RestartBlackBoxManager();

                Thread.Sleep(1000);

                if (conditionsEngine != null)
                    conditionsEngine.Dispose();
                conditionsEngine = new ConditionsEngine();
                conditionsEngine.LoadConditions();

                if (eventActions != null)
                    eventActions.Dispose();
                eventActions = new EventActions("EventActions") { PluginManager = this.pluginManager };
                eventActions.Load();

                ConnectDB();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void RestartPluginManager()
        {
            pluginManager.Stop();
            PluginManager.PluginsDoc = this.Devices_Doc;
            pluginManager.Start();
        }

        public void RestartBlackBoxManager()
        {
            if (XmlNode.ParseBool(Settings.TheeSettings["UseBlackBoxManager"], false))
            {
                if (blackBoxManager != null)
                {
                    blackBoxManager.Stop();
                    DeregisterBlackBoxEventHandlers();
                    blackBoxManager.Dispose();
                    blackBoxManager = null;
                }
                blackBoxManager = new BlackBoxManager();

                RegisterBlackBoxEventHandlers();
                blackBoxManager.Start();

                if (bandaid == null)
                    bandaid = new Bandaid();

                bandaid.Bbm = blackBoxManager;

                SetSensitivity();
            }
        }

        public void RestartConditionsEngine()
        {
            if (conditionsEngine != null)
                conditionsEngine.LoadConditions();
        }

        private bool SetDeviceStatus(RioDevice tempRioDevice)
        {
            bool result = false;

            if (string.IsNullOrWhiteSpace(tempRioDevice.Name) && !string.IsNullOrWhiteSpace(tempRioDevice.Address) && Devices_Doc != null)
            {
                string deviceNickName = Devices_Doc.LookupDeviceName(tempRioDevice.Address);
                if (!string.IsNullOrWhiteSpace(deviceNickName))
                    tempRioDevice.Name = deviceNickName;
                else
                    tempRioDevice.Name = tempRioDevice.Address;
            }

            lock (onlineDevices)
            {
                int resultIndex = ~0;

                if (onlineDevices.Count > 0)
                {
                    resultIndex = onlineDevices.BinarySearch(tempRioDevice);
                }

                if (resultIndex < 0)
                {
                    //see if it is listed under it's address, and remove it
                    if (!string.IsNullOrWhiteSpace(tempRioDevice.Address) && onlineDevices.Count > 0)
                    {
                        int resultIndex2 = onlineDevices.BinarySearch(new RioDevice() { Name = tempRioDevice.Address, Address = tempRioDevice.Address });
                        if (resultIndex >= 0)
                            onlineDevices.RemoveAt(resultIndex2);
                    }

                    onlineDevices.Insert(~resultIndex, tempRioDevice);
                }
                else if (onlineDevices[resultIndex].Status != tempRioDevice.Status)
                {
                    onlineDevices[resultIndex].Status = tempRioDevice.Status;
                    result = true;
                }
            }

            Service.SendMessage(MessageType.DeviceStatus, tempRioDevice);

            return result;
        }

        void pluginManager_PluginStatus(object sender, PluginStatusEventArgs e)
        {
            RioDevice tempDevice = new RioDevice() { Name = e.Plugin.Name, Online = e.Status == PluginStatus.Online, Status = e.Status };
            bool changed = SetDeviceStatus(tempDevice);
            //Service.SendMessage(MessageType.DeviceStatus, tempDevice);

            if(changed && e.Status == PluginStatus.Offline)
                Trigger(new EventTrigger(Guid.NewGuid(), "Offline", e.Plugin.Name) { EventName = string.Format("{0} Offline", e.Plugin.Name), TriggerDT = DateTime.Now, State = true });
        }

        void pluginManager_PluginOnline(object sender, PluginStatusEventArgs e)
        {
            if (onlineDevices != null)
            {
                //if (!onlineDevices.Contains(e.Plugin.Name))
                //    onlineDevices.Add(e.Plugin.Name);

                SetDeviceStatus(new RioDevice() { Name = e.Plugin.Name, Online = true, Status = PluginStatus.Online });
                
                if (eventActions != null)
                    eventActions.ProcessData(e.Plugin.Name);
            }

            //if (DeviceOnline != null)
            //    DeviceOnline(this, e.Plugin.Name);
        }

        void pluginManager_PluginOffline(object sender, PluginStatusEventArgs e)
        {
            Trigger(new EventTrigger(Guid.NewGuid(), "Offline", e.Plugin.Name) { EventName = string.Format("{0} Offline", e.Plugin.Name), TriggerDT = DateTime.Now, State = true });

            SetDeviceStatus(new RioDevice() { Name = e.Plugin.Name, Online = false, Status = PluginStatus.Offline });

            //if (DeviceOffline != null)
            //    DeviceOffline(this, e.Plugin.Name);
        }

        public void Stop()
        {
            try
            {
                running = false;

                if (db != null)
                {
                    db.Disconnect();
                    db.Dispose();
                    db = null;
                }

                if (blackBoxManager != null)
                {
                    blackBoxManager.Stop();
                    DeregisterBlackBoxEventHandlers();
                    blackBoxManager.Dispose();
                    blackBoxManager = null;
                }

                if(bandaid != null)
                    bandaid.Dispose();
                bandaid = null;

                if (detectors != null)
                    detectors.Dispose();
                detectors = null;

                if (conditionsEngine != null)
                    conditionsEngine.Dispose();
                conditionsEngine = null;

                if (eventActions != null)
                    eventActions.Dispose();
                eventActions = null;

                if (pluginManager != null)
                {
                    pluginManager.Stop();
                    pluginManager.OnAlarmTrigger -= PluginManager_AlarmTrigger;
                    pluginManager.ImageCaptured -= pluginManager_ImageCaptured;
                    pluginManager.PluginStatus -= pluginManager_PluginStatus;
                    //pluginManager.PluginOnline -= pluginManager_PluginOnline;
                    //pluginManager.PluginOffline -= pluginManager_PluginOffline;
                    pluginManager.Dispose();
                    pluginManager = null;
                }

                Settings.TheeSettings.Update("LastAlarmNumber", lastAlarmNumber.ToString());
                Settings.TheeSettings.Save();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Start & Stop

        #region Server name and ID
        public Guid ServerID
        {
            get
            {
                Guid serverID;

                serverID = XmlNode.ParseGuid(Settings.TheeSettings["ServerID"], Guid.Empty);

                if (serverID == Guid.Empty)
                {
                    serverID = Guid.NewGuid();
                    Settings.TheeSettings.Update("ServerID", serverID.ToString());
                    Settings.TheeSettings.Save();
                }

                return serverID;
            }
        }

        public string ServerName
        {
            get
            {
                string serverName = "Unknown Server";

                try
                {
                    serverName = XmlNode.ParseString(Settings.TheeSettings["ServerName"], "");

                    if (string.IsNullOrWhiteSpace(serverName))
                    {
                        serverName = Environment.MachineName;
                        Settings.TheeSettings.Update("ServerName", serverName);
                        Settings.TheeSettings.Save();
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                }

                return serverName;
            }
        }
        #endregion

        #region DB
        protected async void ConnectDB()
        {
            try
            {
                if (db != null)
                    if (db.ConnectionOpen)
                        db.Disconnect();

                if (db == null)
                {
                    dbDetails.Load();

                    if (!EventLog.SourceExists("RIO Server"))
                        EventLog.CreateEventSource("RIO Server", "Application");

                    EventLog.WriteEntry("RIO Server", "Connection string: " + dbDetails.ConnectionString(), EventLogEntryType.Information);

                    db = new SqlDBInterface(dbDetails.ConnectionString());
                    db.ConnectionStateChanged += db_ConnectionStateChanged;
                }
                else
                    db.ConnectionString = dbDetails.ConnectionString();

                await db.ConnectAsync();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        protected void DisconnectConnectDB()
        {
            if (db != null)
                db.Disconnect();
        }

        private void db_ConnectionStateChanged(object sender, System.Data.StateChangeEventArgs e)
        {
            if (db.ConnectionOpen && running)
            {
                Thread loadUnresolvedAlarmsThread = new Thread(new ThreadStart(LoadUnresolvedAlarms));
                loadUnresolvedAlarmsThread.Start();
                dbDetails.Valid = true;
            }

            if (DBConnectionStateChanged != null)
                DBConnectionStateChanged(this, e);
        }

        public System.Data.ConnectionState DBConnectionState
        {
            get
            {
                return db != null ? db.ConnectionState : System.Data.ConnectionState.Closed;
            }
        }

	    #endregion DB

        #region BlackBox EventHandlers
        private DeviceEventHandler onDeviceAddedEH;
        private DeviceEventHandler onRemovingDeviceEH;
        private CardInputEvent onCardInputEventEH;
        private CardInputChange onCardInputChangeEH;
        private CardSerialDataReceive onCardSerialDataReceiveEH;
        private CardSerialEvent onCardSerialEventEH;
        private CardSerialError onCardSerialErrorEH;
        private APIExceptionEventHandler onBBExceptionEH;

        protected void RegisterBlackBoxEventHandlers()
        {
            #region EventHandlers
            onDeviceAddedEH = new DeviceEventHandler(blackBoxManager_OnDeviceAdded);
            onRemovingDeviceEH = new DeviceEventHandler(blackBoxManager_OnRemovingDevice);
            onCardInputEventEH = new CardInputEvent(blackBoxManager_OnCardInputEvent);
            onCardInputChangeEH = new CardInputChange(blackBoxManager_OnCardInputChange);
            onCardSerialDataReceiveEH = new CardSerialDataReceive(blackBoxManager_OnCardSerialDataReceive);
            onCardSerialEventEH = new CardSerialEvent(blackBoxManager_OnCardSerialEvent);
            onCardSerialErrorEH = new CardSerialError(blackBoxManager_OnCardSerialError);
            onBBExceptionEH = new APIExceptionEventHandler(blackBoxManager_OnException);
            #endregion EventHandlers

            if (blackBoxManager != null)
            {
                if (onDeviceAddedEH != null)
                    blackBoxManager.OnDeviceAdded += onDeviceAddedEH;

                if (onRemovingDeviceEH != null)
                    blackBoxManager.OnRemovingDevice += onRemovingDeviceEH;

                if (onCardInputEventEH != null)
                    blackBoxManager.OnCardInputEvent += onCardInputEventEH;

                if (onCardInputChangeEH != null)
                    blackBoxManager.OnCardInputChange += onCardInputChangeEH;

                if (onCardSerialDataReceiveEH != null)
                    blackBoxManager.OnCardSerialDataReceive += onCardSerialDataReceiveEH;

                if (onCardSerialEventEH != null)
                    blackBoxManager.OnCardSerialEvent += onCardSerialEventEH;

                if (onCardSerialErrorEH != null)
                    blackBoxManager.OnCardSerialError += onCardSerialErrorEH;

                if (onBBExceptionEH != null)
                    blackBoxManager.OnException += onBBExceptionEH;
            }
        }

        protected void DeregisterBlackBoxEventHandlers()
        {
            if (blackBoxManager != null)
            {
                if (onDeviceAddedEH != null)
                    blackBoxManager.OnDeviceAdded -= onDeviceAddedEH;

                if (onRemovingDeviceEH != null)
                    blackBoxManager.OnRemovingDevice -= onRemovingDeviceEH;

                if (onCardInputEventEH != null)
                    blackBoxManager.OnCardInputEvent -= onCardInputEventEH;

                if (onCardInputChangeEH != null)
                    blackBoxManager.OnCardInputChange -= onCardInputChangeEH;

                if (onCardSerialDataReceiveEH != null)
                    blackBoxManager.OnCardSerialDataReceive -= onCardSerialDataReceiveEH;

                if (onCardSerialEventEH != null)
                    blackBoxManager.OnCardSerialEvent -= onCardSerialEventEH;

                if (onCardSerialErrorEH != null)
                    blackBoxManager.OnCardSerialError -= onCardSerialErrorEH;

                if (onBBExceptionEH != null)
                    blackBoxManager.OnException -= onBBExceptionEH;
            }
        }        

        private void blackBoxManager_OnDeviceAdded(Device _device)
        {
            try
            {
                SetDeviceStatus(new RioDevice() { Address = _device.MacAddress, Online = true, Status = PluginStatus.Online });

                //if (onlineDevices != null)
                //    if (!onlineDevices.Contains(_device.MacAddress))
                //        onlineDevices.Add(_device.MacAddress);

                //if (DeviceOnline != null)
                //    DeviceOnline(this, _device.MacAddress);

                SetSensitivity(_device);

                #region Resolve Offline Alarm
                try
                {
                    string deviceName = detectors.DevicesDoc.LookupDeviceName(_device.MacAddress);

                    AlarmBase alarm = FindLastAlarmByName(string.Format("{0} Offline", !string.IsNullOrWhiteSpace(deviceName) ? deviceName : _device.DeviceName));

                    if(alarm != null)
                    {
                        AlarmResolve resolveItem = new AlarmResolve() 
                        {
                            Alarm_Type = AlarmType.Recalibration,
                            AlarmID = alarm.ID,
                            Description = "Device came back online after recalibrating lasers.",
                            GroupResolve = alarm is GroupAlarm,
                            ResolveDT = DateTime.Now,
                            ResolveID = Guid.NewGuid(),
                            User = User.SystemUser
                        };

                        this.ResolveAlarm(resolveItem);
                    }
                    //else
                    //{
                    //    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Couldn't find offline alarm to classify as Recalibration");
                    //}
                }
                catch(Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error resolving device offline alarm", ex);
                }
                #endregion Resolve Offline Alarm
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private AlarmBase FindLastAlarmByName(string _alarmName)
        {
            AlarmBase result = null;

            try
            {
                for (int i = 0; i < unresolvedAlarms.Count; i++)
                {
                    if (unresolvedAlarms[i].Name == _alarmName && (result == null || unresolvedAlarms[i].AlarmDT > result.AlarmDT))
                    {
                        result = unresolvedAlarms[i];
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "FindLastAlarmByName failed", ex);
            }

            return result;
        }

        private void blackBoxManager_OnRemovingDevice(Device _device)
        {
            try
            {
                SetDeviceStatus(new RioDevice() { Address = _device.MacAddress, Online = false, Status = PluginStatus.Offline });

                //if (onlineDevices != null)
                //    if (onlineDevices.Contains(_device.MacAddress))
                //        onlineDevices.Remove(_device.MacAddress);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error removing device", ex);
            }

            #region device offline alarm
            try
            {
                string deviceName = detectors.DevicesDoc.LookupDeviceName(_device.MacAddress);
                EventTrigger deviceOfflineTrigger = new EventTrigger(Guid.NewGuid(), "", !string.IsNullOrWhiteSpace(deviceName) ? deviceName : _device.DeviceName);
                deviceOfflineTrigger.EventName = string.Format("{0} Offline", !string.IsNullOrWhiteSpace(deviceName) ? deviceName : _device.DeviceName);
                deviceOfflineTrigger.TriggerDT = DateTime.Now;

                IEnumerable<Event> events = conditionsEngine.ProcessTrigger(deviceOfflineTrigger);
                if (events != null)
                    foreach (Event e in events)
                    {
                        e.ServerID = ServerID;
                        ProcessAlarmEventActions(e);
                    }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error creating device offline alarm", ex);
            }
            #endregion device offline alarm

            try
            {
                //if (DeviceOffline != null)
                //    DeviceOffline(this, _device.MacAddress);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error firing DeviceOffline event", ex);
            }
        }

        private void blackBoxManager_OnCardInputEvent(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, InputConditions _eventKind, bool _status)
        {
            try
            {
                if (_status)
                {
                    Detector detector = detectors[_device.MacAddress, _slotIndex, _elementIndex, _elementKind == ElementKind.AnalogInput ? DetectorType.Analog : DetectorType.Digital];
                    if (detector != null) //This shouldn't ever be null, just a safety check
                    {
                        IEventTrigger trigger = new EventTrigger() 
                        {
                            TriggerID = Guid.NewGuid(),
                            DeviceName = detector.DeviceName, 
                            Name = detector.Name, 
                            DetectorName = detector.Name,
                            DetectorGroupIndex = _slotIndex,
                            DetectorIndex = _elementIndex,
                            EventName = detector.EventName, 
                            DetectorType = detector.DetectorType, 
                            TriggerDT = DateTime.Now, 
                            State = _status, 
                            Location = detector.Location, 
                            MapName = detector.ImageMap
                        };

                        if (trigger != null)
                        {
                            Trigger(trigger);
                            //IEnumerable<Event> events = conditionsEngine.ProcessTrigger(trigger);
                            //if (events != null)
                            //    foreach (Event e in events)
                            //        ProcessAlarmEventActions(e);
                        }
                    }
                    else
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Couldn't find the detector: MAC {0}, Card {1}, Element {2}, Type {3}", _device.MacAddress, _slotIndex, _elementIndex, _elementKind), new Exception());
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void blackBoxManager_OnCardInputChange(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, float _value)
        {
            try
            {
                Detector detector = detectors[_device.MacAddress, _slotIndex, _elementIndex, _elementKind == ElementKind.AnalogInput ? DetectorType.Analog : DetectorType.Digital];
                if (detector != null) //This shouldn't ever be null, just a safety check
                {
                    IEventTrigger trigger = new EventTrigger()
                    {
                        TriggerID = Guid.NewGuid(),
                        DeviceName = detector.DeviceName,
                        Name = detector.Name,
                        DetectorName = detector.Name,
                        DetectorGroupIndex = _slotIndex,
                        DetectorIndex = _elementIndex,
                        EventName = detector.EventName,
                        DetectorType = detector.DetectorType,
                        TriggerDT = DateTime.Now,
                        EventValue = _value,
                        Location = detector.Location,
                        MapName = detector.ImageMap
                    };

                    if (trigger != null)
                    {
                        Trigger(trigger);
                        //IEnumerable<Event> events = conditionsEngine.ProcessTrigger(trigger);
                        //if (events != null)
                        //    foreach (Event e in events)
                        //        ProcessAlarmEventActions(e);
                    }
                }
                else
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Couldn't find the detector: MAC {0}, Card {1}, Element {2}, Type {3}", _device.MacAddress, _slotIndex, _elementIndex, _elementKind), new Exception());
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void blackBoxManager_OnCardSerialDataReceive(Device _device, int _slotIndex, byte[] _dataArray)
        {            
        }

        private void blackBoxManager_OnCardSerialEvent(Device _device, int _slotIndex, SerialPortConditions _event, bool _status)
        {            
        }

        private void blackBoxManager_OnCardSerialError(Device _device, int _slotIndex, string _error)
        {            
        }

        void blackBoxManager_OnException(string _functionName, string _description, Exception _exception)
        {
            Exceptions.ExceptionsManager.Add(_functionName, _description, _exception);
        }
        #endregion BlackBox EventHandlers

        #region Alarm Processing
        private void PluginManager_AlarmTrigger(object sender, PluginTriggerEventArgs e)
        {
            Trigger(e.Trigger);
        }

        public void Trigger(IEventTrigger trigger)
        {
            try
            {
                if (trigger.DetectorGroupIndex >= 0 || trigger.DetectorIndex >= 0)
                {
                    Detector detector = Detectors.DetectorByIndex(trigger.DeviceName, trigger.DetectorGroupIndex, trigger.DetectorIndex, trigger.DetectorType);

                    if (detector != null && !string.IsNullOrWhiteSpace(detector.Name))
                        trigger.DetectorName = detector.Name;

                    if (detector != null && !string.IsNullOrWhiteSpace(detector.EventName))
                        trigger.EventName = detector.EventName;
                }

                if (conditionsEngine != null)
                {
                    if(!Service.SendRootCauseMessage(MessageType.Trigger, trigger)) //Send it off to the Root Cause server first for processing, unless there isn't one.
                        ProcessTrigger(trigger);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void ProcessTrigger(IEventTrigger trigger)
        {
            IEnumerable<Event> events = conditionsEngine.ProcessTrigger(trigger);
            if (events != null)
            {
                foreach (Event e in events)
                {
                    e.ServerID = ServerID;
                    ProcessAlarmEventActions(e);
                }
            }
        }

        /// <summary>
        /// When an alarm is raised we need to go through a process. Show alarm on the UI, log in DB, sound an audible alarm, switch a relay on the black box ect.
        /// </summary>
        protected void ProcessAlarmEventActions(Event _event)
        {
            try
            {
                if (_event != null)
                {
                    Thread raiseEventThread = new Thread(() => RaiseEvent(_event));
                    raiseEventThread.Start();

                    if(ScheduleDoc.TheeSchedule.Test(_event.Name)) //Check the schedule first
                    {   
                        if (eventActions == null)
                        {
                            lock (eventActions)
                            {
                                eventActions = new eNerve.DataTypes.EventActions("EventActions");
                                eventActions.PluginManager = this.pluginManager;
                                eventActions.Load();
                            }
                        }

                        Actions actions = eventActions.GetActionsDeepCopy(_event.Name);
                        _event.Actions = actions ?? new Actions(_event.Name);

                        if (actions != null)
                        {
                            for (int i = 0; i < actions.Count; i++)
                            {
                                IAction currentAction = actions[i];

                                ThreadPool.QueueUserWorkItem(state => { ProcessAction(_event, currentAction); });                                
                            }
                        }

                        Alarm alarm = _event as Alarm;
                        if (alarm != null)
                        {
                            AssignAlarmNumber(alarm);

                            Thread raiseAlarmThread = new Thread(() => RaiseAlarm(alarm));
                            raiseAlarmThread.Start();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        protected int lastAlarmNumber = 0;

        protected void AssignAlarmNumber(AlarmBase _alarm)
        {
            _alarm.AlarmNumber = ++lastAlarmNumber;
        }

        public Queue<ProcessedAction> actionsQueue = new Queue<ProcessedAction>();

        private bool busyProcessingActions = false;

        private void ProcessAction(Event _event, IAction _action)
        {
            ProcessedAction action = new ProcessedAction(_event, _action);

            if (!ProcessAction(action))
            {
                action.Retries++;
                lock (actionsQueue)
                {
                    actionsQueue.Enqueue(action);
                }
            }

            if (!busyProcessingActions && actionsQueue.Count > 0)
            {
                busyProcessingActions = true;

                int counter = 0;

                try
                {
                    int sleepTime = 1000;

                    while(actionsQueue.Count > 0)
                    {
                        Debug.WriteLine(string.Format("Process actions Queue: {0}", ++counter));
                        ProcessedAction retryAction = actionsQueue.Dequeue();

                        if (!ProcessAction(retryAction))
                        {
                            retryAction.Retries++;

                            if (retryAction.Retries < 30)
                                actionsQueue.Enqueue(retryAction);
                        }

                        if (actionsQueue.Count > 0)
                            sleepTime = actionsQueue.Peek().Retries * 1000;
                        else
                            sleepTime = 0;

                        if(sleepTime > 0)
                            Thread.Sleep(sleepTime);
                    }
                }
                finally
                {
                    busyProcessingActions = false;
                }
            }
        }

        private bool ProcessAction(ProcessedAction _action)
        {
            bool success = true;

            try
            {
                switch (_action.Action.ActionType)
                {
                    case EventActionType.DB:
                        break;
                    case EventActionType.Log:
                        break;
                    case EventActionType.Siren:
                        Service.SendMessage(MessageType.Siren, _action.Action);
                        break;
                    case EventActionType.Relay:
                        SwitchRelay(_action.Event, (RelayAction)_action.Action);
                        break;
                    case EventActionType.Camera:
                        ProcessCameraEventAction(_action.Event.ID, _action.Event.Name, _action.Event.AlarmDT, _action.Action);
                        //Thread cameraEventActionThread = new Thread(() => ProcessCameraEventAction(_action.Event.ID, _action.Event.Name, _action.Event.AlarmDT, _action.Action));
                        //cameraEventActionThread.Start();
                        break;
                    case EventActionType.Email:
                        ProcessEmailEventAction(_action.Event.Name, (EmailAction)_action.Action);
                        //Thread emailEventActionThread = new Thread(() => ProcessEmailEventAction(_action.Event.Name, (EmailAction)_action.Action));
                        //emailEventActionThread.Start();
                        break;
                    case EventActionType.SiteInstruction:
                        ((SiteInstructionAction)_action.Action).AssignNewStepIDs();
                        break;
                    case EventActionType.Plugin:
                        if (_action.Event.Triggers.Count > 0)
                        {
                            success = pluginManager.PerformAction(_action.Event.Name, _action.Event.Triggers.ToArray(), (PluginAction)_action.Action);
                            //Thread pluginEventActionThread = new Thread(() => pluginManager.PerformAction(_action.Event.Triggers.ToArray(), (PluginAction)_action.Action));
                            //pluginEventActionThread.Start();
                        }
                        break;
                    case EventActionType.AutoResolve:
                        {
                            ProcessAutoResolveEventAction(_action.Event, (AutoResolveAction)_action.Action);

                                //Thread autoResolveEventActionThread = new Thread(() => ProcessAutoResolveEventAction(a, (AutoResolveAction)_action.Action));
                                //autoResolveEventActionThread.Start();

                            break;
                        }
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return success;
        }

        private void ProcessEmailEventAction(string _alarmName, EmailAction _emailAction, EscalationLevel _escalationLevel = EscalationLevel.None)
        {   
            System.Net.Mail.MailMessage msg = null;
            System.Net.Mail.SmtpClient smtp = null;

            try
            {
                XmlNode emailSettings = Settings.TheeSettings["email"];
                if (emailSettings != null)
                {
                    string fromAddress = XmlNode.ParseString(emailSettings, "ethelenerve@gmail.com", "fromAddress");
                    string smtpServer = XmlNode.ParseString(emailSettings, "smtp.gmail.com", "smtpServer");
                    int smtpPort = XmlNode.ParseInteger(emailSettings, 587, "smtpPort");
                    string userName = XmlNode.ParseString(emailSettings, "ethelenerve@gmail.com", "UserName");
                    string Password = XmlNode.ParseString(emailSettings, "", "Password");
                    bool enableSsl = XmlNode.ParseBool(emailSettings, true, "EnableSsl");

                    msg = new System.Net.Mail.MailMessage();

                    string[] toAddresses = _emailAction.Addresses.Split(new char[] { ',', ';' });

                    foreach (string address in toAddresses)
                        msg.To.Add(address);

                    msg.Subject = "eNerve alarm";
                    msg.From = new System.Net.Mail.MailAddress(fromAddress);
                    string body = string.Format("Alarm: {0}\n", _alarmName);                    
                    msg.Body = body;

                    smtp = new System.Net.Mail.SmtpClient(smtpServer, smtpPort);
                    smtp.EnableSsl = enableSsl;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(userName, Password);
                    smtp.Send(msg);
                }
                else
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "No email settings in setup.");
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if(msg != null)
                    msg.Dispose();

                if (smtp != null)
                    smtp.Dispose();
            }
        }

        void pluginManager_ImageCaptured(object sender, ImageCapturedEventArgs e)
        {
            using (GroupAlarm tempGroupAlarm = new GroupAlarm())
            {
                tempGroupAlarm.Name = e.EventName;
                int searchResult = unresolvedAlarms.BinarySearch(tempGroupAlarm);
                if (searchResult >= 0)
                {
                    GroupAlarm resultAlarm = unresolvedAlarms[searchResult];
                    EventImage eventImage = new EventImage() { EventID = resultAlarm.ID, TimeStamp = e.TimeStamp, ImageData = e.ImageData, GroupID = resultAlarm.ID };
                    resultAlarm.AddImage(eventImage);

                    Service.SendMessage(MessageType.AlarmImage, eventImage); 
                    
                    if (db != null)
                        db.QueueAlarmImage(eventImage); 
                }
            }   
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Complex member")]
        private void ProcessCameraEventAction(Guid _eventID, string _eventName, DateTime _eventDT, IAction _action)
        {
            WebClient webClient = null;
            List<EventImage> alarmImages = null;

            try
            {
                Thread.Sleep(3000);

                alarmImages = new List<EventImage>();

                if (_action is CameraAction)
                {
                    XmlNode cameraNode = Devices_Doc.LookupCamera(_action.Name);

                    if (cameraNode != null)
                    {
                        string snapshotCommand = cameraNode["Snapshot"] != null ? cameraNode["Snapshot"].Value : "";
                        string preCurrPostCommand = cameraNode["PreCurrPost"] != null ? cameraNode["PreCurrPost"].Value : "";

                        bool dewarp = false;
                        if (cameraNode["Dewarp"] != null)
                            if (!bool.TryParse(cameraNode["Dewarp"].Value, out dewarp))
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Unable to parse Dewarp value", null);

                        webClient = new WebClient();

                        if (cameraNode["UserName"] != null && cameraNode["Password"] != null)
                        {
                            webClient.Credentials = new NetworkCredential(cameraNode["UserName"].Value, cameraNode["Password"].Value);
                        }

                        if (!string.IsNullOrWhiteSpace(snapshotCommand))
                        {
                            string ipaddress = cameraNode["IPAddress"] != null ? cameraNode["IPAddress"].Value : "";
                            string url = @"http://" + ipaddress.TrimEnd(new char[] { '/', '\\' }) + @"/" + snapshotCommand.TrimStart(new char[] { '/', '\\' });

                            EventImage img = new EventImage();
                            img.EventID = _eventID;
                            img.TimeStamp = DateTime.Now;
                            img.CameraAddress = cameraNode["IPAddress"] != null ? cameraNode["IPAddress"].Value : "";
                            img.CameraID = cameraNode["Name"] != null ? cameraNode["Name"].Value : "";
                            img.Dewarp = dewarp;
                            img.ImageData = webClient.DownloadData(url);

                            alarmImages.Add(img);
                        }

                        if (!string.IsNullOrWhiteSpace(preCurrPostCommand))
                        {
                            VivotekXMLPreCurrPosSnapshots vivotekXMLPreCurrPosSnapshotsDoc = null;

                            try
                            {
                                string ipaddress = cameraNode["IPAddress"] != null ? cameraNode["IPAddress"].Value : "";
                                string fromTime = _eventDT.AddSeconds(-20).ToString("yyyy-MM-dd HH:mm:ss");
                                string toTime = _eventDT.AddHours(1).ToString("yyyy-MM-dd HH:mm:ss");
                                string url = @"http://" + ipaddress.TrimEnd(new char[] { '/', '\\' }) + @"/" + string.Format(preCurrPostCommand, fromTime, toTime).TrimStart(new char[] { '/', '\\' });

                                string xmlData = webClient.DownloadString(url);
                                if (!string.IsNullOrWhiteSpace(xmlData))
                                {
                                    vivotekXMLPreCurrPosSnapshotsDoc = new VivotekXMLPreCurrPosSnapshots();
                                    if (vivotekXMLPreCurrPosSnapshotsDoc.LoadFromString(xmlData))
                                    {
                                        List<XmlNode> nodes = vivotekXMLPreCurrPosSnapshotsDoc.GetImageNodes();

                                        if (nodes != null)
                                        {
                                            if (nodes.Count > 0)
                                                alarmImages.Clear();

                                            foreach (XmlNode node in nodes)
                                            {
                                                string imgPath = vivotekXMLPreCurrPosSnapshotsDoc.GetImagePath(node).Remove(0, "/mnt/auto/CF/".Length);
                                                DateTime imgDT = vivotekXMLPreCurrPosSnapshotsDoc.GetImageTimeStamp(node);

                                                EventImage img = new EventImage(Guid.NewGuid());
                                                img.EventID = _eventID;
                                                img.TimeStamp = DateTime.Now;
                                                img.CameraAddress = cameraNode["IPAddress"] != null ? cameraNode["IPAddress"].Value : "";
                                                img.CameraID = cameraNode["Name"] != null ? cameraNode["Name"].Value : "";
                                                img.Dewarp = dewarp;
                                                img.ImageData = webClient.DownloadData(@"http://" + img.CameraAddress.TrimEnd(new char[] { '/', '\\' }) + @"/" + imgPath.TrimStart(new char[] { '/', '\\' }));
                                                img.TimeStamp = imgDT;

                                                alarmImages.Add(img);
                                            }
                                            nodes.Clear();
                                        }
                                    }
                                }
                            }
                            finally
                            {
                                if (vivotekXMLPreCurrPosSnapshotsDoc != null)
                                    vivotekXMLPreCurrPosSnapshotsDoc.Dispose();
                            }
                        }
                    }
                }

                //Add to alarm and save to DB
                try
                {
                    foreach(EventImage img in alarmImages)
                    {
                        //Alarm searchAlarm = new Alarm(img.AlarmID);
                        //int searchResult = unresolvedAlarms.BinarySearch(searchAlarm);
                        //if (searchResult >= 0)
                        //    unresolvedAlarms[searchResult].AddImage(img);

                        using (GroupAlarm tempGroupAlarm = new GroupAlarm())
                        {
                            tempGroupAlarm.Name = _eventName;
                            int searchResult = unresolvedAlarms.BinarySearch(tempGroupAlarm);
                            if (searchResult >= 0)
                            {
                                GroupAlarm resultAlarm = unresolvedAlarms[searchResult];
                                resultAlarm.AddImage(img);
                                img.GroupID = resultAlarm.ID;
                            }
                        }                        

                        if (db != null)
                            db.QueueAlarmImage(img);
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error adding images to alarm", ex);
                }

                if (OnAlarmImages != null)
                    OnAlarmImages(this, alarmImages);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (webClient != null)
                    webClient.Dispose();

                if (alarmImages != null)
                    alarmImages.Clear();
            }
        }

        private void ProcessAutoResolveEventAction(Event _event, AutoResolveAction _autoResolveAction)
        {
            if (_autoResolveAction.TimeDelay > 0)
                Thread.Sleep(_autoResolveAction.TimeDelay * 1000);

            if(_event != null)
            {
                AlarmBase alarm = null;

                if (string.IsNullOrWhiteSpace(_autoResolveAction.AlarmToResolve))
                {
                    alarm = _event as AlarmBase;
                    if (alarm != null && !(alarm is GroupAlarm))
                    {
                        alarm = new GroupAlarm((Alarm)_event);

                        lock (unresolvedAlarms)
                        {
                            int searchResult = unresolvedAlarms.BinarySearch((GroupAlarm)alarm);
                            if (searchResult >= 0)
                            {
                                alarm = unresolvedAlarms[searchResult];
                            }
                        }
                    }
                }
                else
                {
                    foreach(AlarmBase a in unresolvedAlarms)
                    {
                        if(a.Name == _autoResolveAction.AlarmToResolve)
                        {
                            alarm = a;
                            break;
                        }
                    }
                }

                if(alarm != null)
                    ResolveAlarm(new AlarmResolve() { ResolveID = Guid.NewGuid(), AlarmID = alarm.ID, Alarm_Type = AlarmType.AutoResolve, Description = "Auto Resolved", GroupResolve = true, ResolveDT = DateTime.Now, User = User.SystemUser });
            }
        }

        private void RaiseAlarm(Alarm _alarm)
        {
            if (_alarm != null)
            {
                GroupAlarm groupAlarm = new GroupAlarm(_alarm);
                if (_alarm.HasImages)
                {
                    foreach (EventImage image in _alarm.Images)
                    {
                        image.GroupID = groupAlarm.ID;
                        groupAlarm.AddImage(image);
                    }
                }

                lock (unresolvedAlarms)
                {
                    int searchResult = unresolvedAlarms.BinarySearch(groupAlarm);
                    if (searchResult < 0)
                    {
                        unresolvedAlarms.Insert(~searchResult, groupAlarm);
                        groupAlarm.Escalated += GroupAlarm_Escalated;
                        groupAlarm.StartEscalation();

                        if (OnAlarm != null)
                            OnAlarm(this, new AlarmEventArgs(groupAlarm));
                    }
                    else
                    {
                        groupAlarm.Dispose();

                        groupAlarm = unresolvedAlarms[searchResult];
                        GroupAlarmUpdate update = groupAlarm.Update(_alarm.Triggers);
                        _alarm.EscalationLevel = unresolvedAlarms[searchResult].EscalationLevel;

                        if (OnGroupAlarmUpdate != null)
                            OnGroupAlarmUpdate(this, new GroupAlarmUpdateEventArgs(update));
                    }
                    if (groupAlarm.HasImages)
                        Service.SendMessage(MessageType.AlarmImages, groupAlarm.Images);
                }
                _alarm.GroupAlarmID = groupAlarm.ID;                

                if (db != null)
                    db.QueueAlarm(_alarm);
            }
        }

        private void RaiseEvent(Event _event)
        {
            try
            {
                if (_event != null)
                {
                    Service.SendMessage(MessageType.Event, _event);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void LoadUnresolvedAlarms()
        {
            List<Alarm> dbAlarms = null;
            List<GroupAlarmUpdate> groupAlarmUpdateList = null;

            StatusMessage statusMsg = null;

            try
            {
                statusMsg = new StatusMessage() { StatusID = Guid.NewGuid(), Message = "Loading unresolved alarms", MessageTimeStamp = DateTime.Now, Animation = true, AutoRemove = TimeSpan.FromMinutes(5) };

                if (OnStatusMessage != null)
                    OnStatusMessage(this, new StatusMessageEventArgs(statusMsg));

                dbAlarms = db.GetUnresolvedAlarms();

                if (dbAlarms != null)
                {
                    if (unresolvedAlarms == null)
                        unresolvedAlarms = new List<GroupAlarm>();

                    foreach (Alarm dbAlarm in dbAlarms)
                    {
                        if (dbAlarm != null)
                        {
                            GroupAlarm tmpGroupAlarm = new GroupAlarm(dbAlarm);

                            int searchResult = unresolvedAlarms.BinarySearch(tmpGroupAlarm);

                            if (searchResult >= 0)
                            {
                                tmpGroupAlarm = unresolvedAlarms[searchResult];
                                GroupAlarmUpdate update = tmpGroupAlarm.Update(dbAlarm.Triggers);
                            }
                            else
                            {
                                Actions actions = eventActions.GetActionsDeepCopy(dbAlarm.Name);
                                tmpGroupAlarm.Actions = actions;
                                tmpGroupAlarm.Escalated += GroupAlarm_Escalated;
                                tmpGroupAlarm.StartEscalation();

                                unresolvedAlarms.Insert(~searchResult, tmpGroupAlarm);
                            }
                        }
                    }

                    foreach (GroupAlarm groupAlarm in unresolvedAlarms)
                    {
                        groupAlarm.AddSiteInstructionStep(db.GetAlarmInstructionSteps(groupAlarm.ID));
                        groupAlarm.AddComments(db.GetAlarmComments(groupAlarm.ID));

                        Service.SendMessage(MessageType.GroupAlarm, groupAlarm);

                        Thread imagesThread = new Thread(() => { LoadAlarmImages(groupAlarm); });
                        imagesThread.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if(dbAlarms != null)
                {
                    for (int i = dbAlarms.Count - 1; i >= 0; i--)
                    {
                        Alarm alarm = dbAlarms[i];
                        dbAlarms.RemoveAt(i);
                        alarm.Dispose();
                    }
                }

                if (OnStatusMessage != null && statusMsg != null)
                    OnStatusMessage(this, new StatusMessageEventArgs(statusMsg, StatusMessageEventArgs.StatusMessageAction.Remove));
            }
        }

        private void LoadAlarmImages(Event _event)
        {
            List<EventImage> images = null;

            try
            {
                images = db.GetAlarmImages(_event.ID, 10);

                Service.SendMessage(MessageType.AlarmImages, images);

                foreach (EventImage img in images)
                {
                    _event.AddImage(img);

                    Service.SendMessage(MessageType.AlarmImage, img);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if(images != null)
                    images.Clear();
            }
        }

        public List<Alarm> GetAlarmHistory(AlarmHistoryFilter filter)
        {
            List<Alarm> result = null;

            try
            {
                result = db.GetAlarmHistory(filter.StartDate, filter.EndDate, filter.AlarmType, filter.AlarmName, filter.EscalationLevel, filter.Resolved);

                if (result != null)
                {
                    foreach (Alarm alarm in result)
                    {
                        alarm.AddSiteInstructionStep(eventActions.GetSiteInstructionsDeepCopy(alarm.Name));
                        alarm.AddSiteInstructionStep(db.GetAlarmInstructionSteps(alarm.GroupAlarmID));
                        alarm.AddComments(db.GetAlarmComments(alarm.GroupAlarmID));

                        alarm.Images = db.GetAlarmImages(alarm.ID);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        void GroupAlarm_Escalated(object sender, GroupAlarm.EscalateEventArgs e)
        {
            try
            {
                if (sender is GroupAlarm)
                {
                    //Todo: perform all the escalation actions
                    GroupAlarm groupAlarm = (GroupAlarm)sender;

                    if (groupAlarm.Actions != null)
                    {
                        for (int i1 = 0; i1 < groupAlarm.Actions.Count; i1++)
                        {
                            if (groupAlarm.Actions[i1] is EscalationsAction)
                            {
                                EscalationsAction eAction = (EscalationsAction)groupAlarm.Actions[i1];

                                Actions eActions = null;
                                if (e.EscalationLevel == EscalationLevel.Green)
                                    eActions = eAction.GreenActions;
                                else if (e.EscalationLevel == EscalationLevel.Yellow)
                                    eActions = eAction.YellowActions;
                                else if (e.EscalationLevel == EscalationLevel.Red)
                                    eActions = eAction.RedActions;

                                if (eActions != null)
                                {
                                    for (int i2 = 0; i2 < eActions.Count; i2++)
                                    {
                                        IAction currentAction = eActions[i2];

                                        ThreadPool.QueueUserWorkItem(state => { ProcessAction(groupAlarm, currentAction); });
                                    }
                                }
                            }
                        }
                    }
                    
                    GroupAlarmPropertyUpdate propertyUpdate = new GroupAlarmPropertyUpdate(groupAlarm.ID, groupAlarm.EscalationLevel.GetType().Name, e.EscalationLevel);

                    if (db != null)
                        db.QueueGroupAlarmPropertyUpdate(propertyUpdate);

                    if (GroupAlarmPropertyUpdated != null)
                        GroupAlarmPropertyUpdated(this, new PropertyUpdateEventArgs(propertyUpdate));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void AlarmEscalated(object _sender, EscalationLevel _escalationLevel)
        {
            try
            {
                if (_sender is Alarm)
                {
                    //Todo: perform all the escalation actions
                    Alarm alarm = (Alarm)_sender;

                    if (alarm.Actions != null)
                    {
                        for (int i1 = 0; i1 < alarm.Actions.Count; i1++)
                        {
                            if (alarm.Actions[i1] is EscalationsAction)
                            {
                                EscalationsAction eAction = (EscalationsAction)alarm.Actions[i1];

                                Actions eActions = null;
                                if (_escalationLevel == EscalationLevel.Green)
                                    eActions = eAction.GreenActions;
                                else if (_escalationLevel == EscalationLevel.Yellow)
                                    eActions = eAction.YellowActions;
                                else if (_escalationLevel == EscalationLevel.Red)
                                    eActions = eAction.RedActions;

                                if (eActions != null)
                                {
                                    for (int i2 = 0; i2 < eActions.Count; i2++)
                                    {
                                        IAction currentAction = eActions[i2];

                                        ThreadPool.QueueUserWorkItem(state => { ProcessAction(alarm, currentAction); });
                                    }
                                }
                            }
                        }
                    }


                    AlarmPropertyUpdate alarmUpdate = new AlarmPropertyUpdate(((Alarm)_sender).ID, "EscalationLevel", (byte)_escalationLevel);

                    if (db != null)
                        db.QueueAlarmPropertyUpdate(alarmUpdate);

                    if (AlarmPropertyUpdated != null)
                        AlarmPropertyUpdated(this, new PropertyUpdateEventArgs(alarmUpdate));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void ResolveAlarm(AlarmResolve _alarmResolve)
        {
            GroupAlarm tmpAlarm = null;

            try
            {
                if (_alarmResolve.GroupResolve)
                {
                    bool found = false;
                    lock (unresolvedAlarms)
                    {
                        if (unresolvedAlarms != null)
                        {
                            if (unresolvedAlarms.Count > 0)
                            {
                                //tmpAlarm = new GroupAlarm() { Name = _alarmResolve.Name };
                                //int searchResult = unresolvedAlarms.BinarySearch(tmpAlarm);

                                int i = 0;
                                while (i < unresolvedAlarms.Count)
                                {
                                    if (unresolvedAlarms[i].ID == _alarmResolve.AlarmID)
                                    {
                                        GroupAlarm resolvedGroupAlarm = unresolvedAlarms[i];
                                        resolvedGroupAlarm.Resolve(_alarmResolve);
                                        unresolvedAlarms.RemoveAt(i);
                                        resolvedGroupAlarm.Dispose();
                                        found = true;
                                        break;
                                    }

                                    i++;
                                }
                            }
                        }
                    }

                    if ((_alarmResolve.Alarm_Type == AlarmType.AutoResolve && found) || _alarmResolve.Alarm_Type != AlarmType.AutoResolve)
                    {
                        if (db != null)
                            db.QueueAlarmResolve(_alarmResolve);

                        if (GroupAlarmResolveItemAdded != null)
                            GroupAlarmResolveItemAdded(this, new AlarmResolvedEventArgs(_alarmResolve));
                    }
                }
                else
                {
                    if (db != null)
                        db.QueueAlarmResolve(_alarmResolve);

                    if (AlarmResolveItemAdded != null)
                        AlarmResolveItemAdded(this, new AlarmResolvedEventArgs(_alarmResolve));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (tmpAlarm != null)
                    tmpAlarm.Dispose();
            }
        }

        public void AlarmComment(AlarmComment _comment)
        {
            if (unresolvedAlarms != null && unresolvedAlarms.Count > 0)
            {
                GroupAlarm unresolvedAlarm = unresolvedAlarms.Find(x => x.ID.Equals(_comment.AlarmID));
                if (unresolvedAlarm != null)
                {
                    unresolvedAlarm.AddComment(_comment);
                }
            }

            if (db != null)
                db.QueueAlarmComment(_comment);
        }

        public void SiteInstructionStep(SiteInstructionStep _siteInstructionStep)
        {
            try
            {
                if (unresolvedAlarms != null)
                {
                    if (unresolvedAlarms.Count > 0)
                    {
                        GroupAlarm unresolvedAlarm = unresolvedAlarms.Find(x => x.ID.Equals(_siteInstructionStep.AlarmID));

                        if (unresolvedAlarm != null)
                        {
                            SiteInstructionStep step = unresolvedAlarm.GetSiteInstructionStep(_siteInstructionStep.SiteInstructionStepID);

                            if (step != null)
                            {
                                step.Completed = _siteInstructionStep.Completed;
                                step.User = _siteInstructionStep.User;
                                step.CompletedDT = _siteInstructionStep.CompletedDT;
                            }
                            else
                                unresolvedAlarm.AddSiteInstructionStep(_siteInstructionStep);
                        }
                    }
                }

                if (db != null)
                    db.QueueSiteInstruction(_siteInstructionStep);

                if (SiteInstructionUpdated != null)
                    SiteInstructionUpdated(this, _siteInstructionStep);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        internal void AddVideoCamera(CameraPlugin camera, IPAddress address)
        {
            pluginManager.AddVideoCamera(camera, address);
        }

        internal void AddVideoServer(IPEndPoint server)
        {
            pluginManager.AddCameraServer(server);
        }

        protected void SwitchRelay(Event _event, RelayAction _action)
        {
            try
            {
                if (_action.DeviceName == "")
                {
                    List<string> deviceNames = _event.DeviceNames;
                    if (deviceNames != null)
                        foreach (string name in deviceNames)
                            SwitchRelay(name, _action.Name, _action.Value, _action.Duration);
                }
                else
                    SwitchRelay(_action.DeviceName, _action.Name, _action.Value, _action.Duration);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        //protected void SwitchRelay(GroupAlarm _alarm, RelayAction _action)
        //{
        //    try
        //    {
        //        if (_action.DeviceName == "")
        //        {
        //            List<string> deviceNames = _alarm.DeviceNames;
        //            if(deviceNames != null)
        //                foreach(string name in deviceNames)
        //                    SwitchRelay(name, _action.Name, _action.Value, _action.Duration);
        //        }
        //        else
        //            SwitchRelay(_action.DeviceName, _action.Name, _action.Value, _action.Duration);
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
        //    }
        //}

        public void SwitchRelay(string _deviceName, string _relayName, bool _value, int _duration = 0)
        {
            try
            {
                XmlNode deviceNode = detectors.DevicesDoc[_deviceName];
                XmlNode relayNode = detectors.DevicesDoc.LookupRelay(_deviceName, _relayName);
                if (relayNode != null)
                {
                    int groupIndex = XmlNode.ParseInteger(relayNode.Parent, -1, "Index");
                    int relayIndex = XmlNode.ParseInteger(relayNode, -1, "Index");

                    INerveRelaySwitchPlugin plugin = PluginManager.PluginByName(_deviceName) as INerveRelaySwitchPlugin;
                    if (plugin != null)
                    {
                        plugin.SwitchRelay(relayIndex, _value, _duration);
                    }
                    else if (blackBoxManager != null && deviceNode != null && relayNode != null && groupIndex >= 0 && relayIndex >= 0)
                    {
                        Device device = blackBoxManager.GetDeviceByMacAddress(deviceNode["DeviceAddress"].Value);
                        if (device != null)
                            device.SetRelayState(groupIndex, relayIndex, _value, _duration);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        /// <summary>
        /// Sets the sensitivity for all the sensors that supports it.
        /// </summary>
        public void SetSensitivity(Device _device = null)
        {
            try
            {
                foreach (XmlNode deviceNode in detectors.DevicesDoc)
                {
                    string deviceAddress = XmlNode.ParseString(deviceNode["DeviceAddress"], "");

                    if (!string.IsNullOrWhiteSpace(deviceAddress))
                    {
                        if (deviceNode["Groups"] != null)
                        {
                            foreach (XmlNode groupNode in deviceNode["Groups"])
                            {
                                int groupIndex = XmlNode.ParseInteger(groupNode, -1, "Index");
                                foreach (XmlNode detectorNode in groupNode)
                                {
                                    DevicesDoc.DeviceIOType ioType = DevicesDoc.ParseIOType(detectorNode);
                                    if (ioType == DevicesDoc.DeviceIOType.DigitalInput || ioType == DevicesDoc.DeviceIOType.FibreInput || ioType == DevicesDoc.DeviceIOType.AnalogInput)
                                    {
                                        double sensitivity = XmlNode.ParseDouble(detectorNode["Sensitivity"], -1);
                                        if (sensitivity > 0)
                                        {
                                            if (blackBoxManager != null)
                                            {
                                                Device device = blackBoxManager.GetDeviceByMacAddress(deviceAddress);
                                                if (device != null)
                                                {
                                                    if ((_device == null && device.OnLine) || (_device != null && _device == device && device.OnLine))
                                                    {
                                                        device.SetFibreSensitivity(groupIndex, detectorNode.Name.EndsWith("1") ? Device.FibreSensorName.Sensor_1 : Device.FibreSensorName.Sensor_2, (float)sensitivity);
                                                        Thread.Sleep(50);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        /// <summary>
        /// Only to be used for testing purposes.
        /// </summary>
        /// <param name="_alarm">Create a test alarm and push it to the engine.</param>
        public void InjectAlarm(Event _alarm)
        {
            ProcessAlarmEventActions(_alarm);
        }
        #endregion Alarm Processing

        #region Root Cause
        public void ProcessRootCauseAlarm(Event _event)
        {
            ProcessAlarmEventActions(_event);
        }

        public void AddRouteCauseTrigger(IEventTrigger _rootCauseTrigger)
        {
            Event e = unresolvedAlarms.FirstOrDefault(x => x.ID == _rootCauseTrigger.EventID);

            if (e != null && e.RootCause)
            {
                IEventTrigger trigger = e.AddTrigger(_rootCauseTrigger);

                if (db != null)
                    db.QueueEventTrigger(trigger);

                Service.SendMessage(MessageType.RootCauseAlarmTrigger, trigger); // pass it on to the clients
            }         
        }

        public void ProcessRouteCauseReturnTrigger(IEventTrigger _trigger)
        {
            ProcessTrigger(_trigger);
        }

        #endregion

        #region OccurrenceBook
        public void AddOccurenceBookEntry(OBEntry _entry)
        {
            if (_entry is OccurrenceBook)
            {
                ((OccurrenceBook)_entry).PropertyUpdated += OccurrenceBook_PropertyUpdated;
            }

            if (DB != null)
                DB.QueueOccurrenceBook(_entry);
        }

        private void OccurrenceBook_PropertyUpdated(object sender, PropertyUpdate propertyToUpdate)
        {
            if (sender is OccurrenceBook)
            {
                ((OccurrenceBook)sender).PropertyUpdated -= OccurrenceBook_PropertyUpdated;
            }

            if (OccurrenceBookPropertyUpdated != null)
                OccurrenceBookPropertyUpdated(sender, propertyToUpdate);
        }

        public List<OBType> GetOBTypes()
        {
            List<OBType> obTypes = null;

            try
            {
                obTypes = new List<OBType>();

                XmlNode obTypesNode = Settings.TheeSettings["OBTypes"];
                if (obTypesNode != null)
                {
                    foreach (XmlNode obTypeN in obTypesNode.Children)
                    {
                        string desc = XmlNode.ParseString(obTypeN, obTypeN.Name, "DisplayValue");
                        if (desc != "")
                        {
                            OBType obTypeItem = new OBType(desc);
                            obTypes.Add(obTypeItem);

                            if (obTypeN.Children.Count > 0)
                            {
                                foreach (XmlNode obSubTypeN in obTypeN.Children)
                                {
                                    string subTypeDesc = XmlNode.ParseString(obSubTypeN, obSubTypeN.Name, "DisplayValue");

                                    if (subTypeDesc != "")
                                    {
                                        OBType obSubTypeItem = new OBType(subTypeDesc);
                                        obTypeItem.SubTypes.Add(obSubTypeItem);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return obTypes;
        }
        #endregion OccurrenceBook

        #region Cameras

        public ObservableCollection<ICameraPlugin> CameraList(User user)
        {
            if (cameraServers == null)
                cameraServers = new List<ICameraServerPlugin>();
            var userCameras = new ObservableCollection<ICameraPlugin>();

            // Load plugin cameras.
            var allCameras = pluginManager.ConfiguredCameras;

            bool adminUser = true;
            if ((user.Rights.UserType != UserType.Admin) && (user.Rights.UserType != UserType.Super))
            {
                adminUser = false;
            }

            foreach (var camera in allCameras)
            {
                if (!adminUser)
                {
                    if (camera.Admin)
                        continue;
                }
                camera.Host = null;
                if (camera.Parent != null)
                {
                    if (!cameraServers.Contains(camera.Parent))
                    {
                        camera.Parent.Host = null;
                        cameraServers.Add(camera.Parent);
                    }
                    camera.Parent = null;
                }
                userCameras.Add(camera);
            }
            return userCameras;
        }

        public List<ICameraServerPlugin> CameraServers()
        {
            if ((cameraServers != null) && (cameraServers.Count > 0))
                return cameraServers;
            else if (pluginManager != null && pluginManager.CameraServers != null && pluginManager.CameraServers.Count > 0)
            {
                cameraServers = new List<ICameraServerPlugin>();
                foreach (var server in pluginManager.CameraServers)
                {
                    server.Host = null;
                    cameraServers.Add(server);
                }
            }

            if (cameraServers == null)
                cameraServers = new List<ICameraServerPlugin>();
            return cameraServers;
        }

        public byte[] CameraSnapshot(string _ipAddress, string _snapshotCommand, string _username, string _password)
        {
            string url = @"http://" + _ipAddress.TrimEnd(new char[] { '/', '\\' }) + @"/" + _snapshotCommand.TrimStart(new char[] { '/', '\\' });
            return CameraSnapshot(url, _username, _password);
        }

        public byte[] CameraSnapshot(string _url, string _username, string _password)
        {
            WebClient webClient = null;
            byte[] result = null;

            try
            {
                webClient = new WebClient();
                if (!string.IsNullOrWhiteSpace(_username) && !string.IsNullOrWhiteSpace(_password))
                {
                    webClient.Credentials = new NetworkCredential(_username, _password);
                }

                result = webClient.DownloadData(_url);

            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (webClient != null)
                    webClient.Dispose();
            }

            return result;
        }
        #endregion Camera

        #region Users
        public bool Login(User _user, out string _message)
        {
            bool result = true;
            _message = "";
            
            result = users.Login(_user, out _message);

            return result;
        }

        public List<VideoHostConfiguration> GetVideoConfigurations(User user)
        {
            try
            {
                XmlNode usersNode = Settings.TheeSettings["users"];
                if (usersNode != null)
                {
                    XmlNode userNode = usersNode[user.UserName];
                    if (userNode != null)
                    {
                        XmlNode videoNode = userNode["VideoSettings"];
                        if (videoNode != null)
                        {
                            List<VideoHostConfiguration> configs = new List<VideoHostConfiguration>(videoNode.Children.Count);
                            foreach (XmlNode item in videoNode)
                            {
                                CameraLayouts layout = (CameraLayouts)Enum.Parse(typeof(CameraLayouts), item.Attribute("type").Value);
                                Dictionary<string, string> cameras = new Dictionary<string, string>(item.Children.Count);
                                foreach (XmlNode cameraNode in item)
                                {
                                    cameras.Add(cameraNode.Name, cameraNode.Attribute("camera").Value);
                                }
                                VideoHostConfiguration config = new VideoHostConfiguration(layout, cameras, item.Name);
                                configs.Add(config);
                            }
                            return configs;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                                   System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing video config from XML", ex);
            }
            return null;
        }

        public List<User> GetUsers()
        {
            List<User> users = new List<User>();
            XmlNode usersNode = Settings.TheeSettings["users"];
            if (usersNode != null)
            {
                foreach (XmlNode user in usersNode.Children)
                {
                    if (user != null)
                    {
                        var tempUser = new User(user);
                        if ((tempUser != null) && (!string.IsNullOrEmpty(tempUser.UserName)))
                            users.Add(tempUser);
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, 
                                System.Reflection.MethodBase.GetCurrentMethod().Name), "Invalid user node in settings doc!" + user + user.Value);
                    }
                }
            }
            else
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to load user node from Settings doc");
                return null;
            }

            if (users.Count > 0)
                return users;
            else
                return null;
            
        }

        public void Logout(User _user)
        {
            users.Logout(_user);
        }
        #endregion Users

        #region Documents
        public Detectors Detectors
        {
            get { return detectors; }
            set { detectors = value; }
        }

        public DevicesDoc Devices_Doc
        {
            get
            {
                return detectors != null ? detectors.DevicesDoc : null;
            }
            set
            {
                if (detectors != null)
                {
                    detectors.DevicesDoc = value;
                    SetSensitivity();
                }
            }
        }

        public AlarmConditionsDoc AlarmConditionsDoc
        {
            get { return conditionsEngine.AlarmConditionsDoc; }
            set 
            {
                conditionsEngine.LoadConditions(value);
            }
        }

        public EventActions EventActions
        {
            get
            {
                return eventActions;
            }
            set
            {
                eventActions = value;
            }
        }
        
        public MapDoc Map_Doc
        {
            get
            {
                if (map_Doc == null)
                {
                    map_Doc = new MapDoc("Maps");
                    map_Doc.Load();
                }
                return map_Doc;
            }
            set
            {
                map_Doc = value;
            }
        }

        public List<string> AlarmNames
        {
            get
            {
                List<string> alarmNames = null;

                //if (detectors != null)
                //{
                //    foreach (string s in detectors.DevicesDoc.FindEventNames())
                //        alarmNames.Add(s);
                //}

                if (conditionsEngine != null)
                {
                    alarmNames = conditionsEngine.AlarmConditionsDoc.FindAlarmNames(alarmNames);
                }

                return alarmNames ?? new List<string>();
            }
        }

        public List<ReportFile> Reports
        {
            get
            {
                if (reportManager == null)
                    return null;
                else
                {
                    reportManager.ClearReports();
                    reportManager.LoadReports();
                    return reportManager.Reports;
                }
            }
        }
        #endregion Documents
    }
}
