﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

using eNerve.DataTypes;

namespace BlackBoxEngine
{
    public class ReportManager : IDisposable
    {
        public List<ReportFile> Reports;

        public ReportManager()
        {
            LoadReports();
        }
        
        ~ReportManager()
        {
            Dispose(false);
        }
        public void Dispose()
        {
            Dispose(true);
        }

        public void ClearReports()
        {
            if (Reports != null)
            {
                Reports.Clear();
                Reports = null;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            ClearReports();
        }

        public void LoadReports()
        {
            if (Reports == null)
                Reports = new List<ReportFile>();
            string reportsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Ethele\\RioSoft\\Reports");
            if (Directory.Exists(reportsFolder))
            {
                foreach (var reportFile in Directory.GetFiles(reportsFolder, "*.rdl"))
                {
                    byte[] fileData = File.ReadAllBytes(reportFile);
                    Reports.Add(new ReportFile(reportFile.Substring(reportFile.LastIndexOf('\\') + 1), fileData));

                    /*using (FileStream stream = File.Open(reportFile, FileMode.Open, FileAccess.Read))
                    {
                        new byte[stream.Length];
                        stream.Read(fileData, 0, (int)stream.Length);
                    }*/
                }
            }
            else
            {// As hy nie bestaan nie, kan hy nie reports in hê nie.
                Directory.CreateDirectory(reportsFolder);
            }
        }
    }
}
