﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using eNervePluginInterface;
using eThele.Essentials;
using eNerve.DataTypes;
using System.Collections.ObjectModel;

namespace BlackBoxEngine
{
    public abstract class DBInterface : IDisposable
    {
        public string ConnectionString { get; set; }
        
        public abstract System.Data.ConnectionState ConnectionState { get; }
        protected System.Data.StateChangeEventHandler stateChangeEventHandler;
        public event System.Data.StateChangeEventHandler ConnectionStateChanged;

        protected Queue<object> dataToInsert; //buffer the objects so that we can insert them sequentially and make sure they are inserted into the DB.

        public DBInterface()
        {
            stateChangeEventHandler = new System.Data.StateChangeEventHandler(con_StateChange);
        }

        #region Dispose
        ~DBInterface()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (dataToInsert != null)
                dataToInsert.Clear();  
        }
        #endregion Dispose

        #region Connectivity
        private System.Timers.Timer connectivityTimer;
        private System.Timers.Timer connectivityTimer2;

        public virtual bool Connect()
        {
            bool result = true;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    if (connectivityTimer == null)
                    {
                        connectivityTimer = new System.Timers.Timer(10000);
                        connectivityTimer.Elapsed += connectivityTimer_Elapsed;
                    }

                    connectivityTimer.Start();
                }
                else
                    result = false;
            }
            catch(Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                result = false;
            }

            return result;
        }

        void connectivityTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                connectivityTimer.Stop();

                if (!ConnectionOpen)
                    Connect();

                connectivityTimer.Start();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public virtual async Task<bool> ConnectAsync()
        {
            bool result = true;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    if (connectivityTimer2 == null)
                    {
                        connectivityTimer2 = new System.Timers.Timer(10000);
                        connectivityTimer2.Elapsed += connectivityTimer2_Elapsed;
                    }

                    connectivityTimer2.Start();
                }
                else
                    result = false;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                result = false;
            }

            return result;
        }
        
        async void connectivityTimer2_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                connectivityTimer2.Stop();

                if (!ConnectionOpen)
                    await ConnectAsync();

                connectivityTimer2.Start();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public virtual void Disconnect()
        {
            if(connectivityTimer2 != null)
            {
                connectivityTimer2.Stop();
                connectivityTimer2.Elapsed -= connectivityTimer2_Elapsed;
                connectivityTimer2.Dispose();
                connectivityTimer2 = null;
            }
        }

        public bool ConnectionOpen
        {
            get
            {
                return ConnectionState == System.Data.ConnectionState.Open || ConnectionState == System.Data.ConnectionState.Executing || ConnectionState == System.Data.ConnectionState.Fetching;
            }
        }

        protected void con_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            try
            {
                if (ConnectionOpen)
                {
                    Thread thread = new Thread(new ThreadStart(InsertData));
                    insertingData = true;
                    thread.Start();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error starting insert data thread", ex);
            }

            try
            {
                if (ConnectionStateChanged != null)
                    ConnectionStateChanged(this, e);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error firing ConnectionStateChanged event", ex);
            }
        }
        #endregion Connectivity

        #region Inserting Data
        public bool QueueAlarm(Alarm _alarm)
        {
            return QueueData(_alarm);
        }

        public bool QueueAlarmPropertyUpdate(AlarmPropertyUpdate _alarmUpdate)
        {
            return QueueData(_alarmUpdate);
        }

        public bool QueueGroupAlarmPropertyUpdate(GroupAlarmPropertyUpdate _groupAlarmPropertyUpdate)
        {
            return QueueData(_groupAlarmPropertyUpdate);
        }

        public bool QueueSiteInstruction(SiteInstructionStep _siteInstructionStep)
        {
            return QueueData(_siteInstructionStep);
        }

        public bool QueueAlarmResolveItem(ResolveDetailItem _resolveDetailItem)
        {
            return QueueData(_resolveDetailItem);
        }

        public bool QueueAlarmResolve(AlarmResolve _alarmResolve)
        {
            return QueueData(_alarmResolve);
        }

        public bool QueueAlarmImage(EventImage _alarmImage)
        {
            return QueueData(_alarmImage);
        }

        public bool QueueOccurrenceBook(OBEntry _obEntry)
        {
            return QueueData(_obEntry);
        }

        public bool QueueAlarmComment(AlarmComment _comment)
        {
            return QueueData(_comment);
        }

        public bool QueueEventTrigger(IEventTrigger _trigger)
        {
            return QueueData(_trigger);
        }

        private bool QueueData(object _item)
        {
            bool result = true;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false) && _item != null)
                {
                    if (dataToInsert == null)
                        dataToInsert = new Queue<object>();

                    dataToInsert.Enqueue(_item);

                    if (!insertingData)
                    {
                        insertingData = true;

                        Thread thread = new Thread(InsertData);
                        thread.Start();
                    }
                }
                else
                    result = false;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                result = false;
            }

            return result;
        }

        private bool insertingData = false;

        private void InsertData()
        {
            try
            {
                if (dataToInsert != null)
                {
                    if (!ConnectionOpen)
                        Connect();

                    int attempts = 0;

                    while (dataToInsert.Count > 0 && ConnectionOpen)
                    {
                        object item = dataToInsert.Peek();
                        bool result = false;
                        if (item is Alarm)
                            result = InsertAlarm((Alarm)item);
                        else if (item is GroupAlarmPropertyUpdate)
                            result = UpdateAlarmProperty((GroupAlarmPropertyUpdate)item);
                        else if (item is AlarmPropertyUpdate)
                            result = UpdateAlarmProperty((AlarmPropertyUpdate)item);
                        else if (item is ResolveDetailItem)
                            result = ResolveAlarm((ResolveDetailItem)item);
                        else if (item is AlarmResolve)
                            result = ResolveAlarm((AlarmResolve)item);
                        else if (item is EventImage)
                            result = InsertAlarmImage((EventImage)item);
                        else if (item is SiteInstructionStep)
                            result = UpdateSiteInstruction((SiteInstructionStep)item);
                        else if (item is OBEntry)
                            result = InsertOccurrenceBookEntry((OBEntry)item);
                        else if (item is AlarmComment)
                            result = InsertAlarmComment((AlarmComment)item);
                        else if (item is IEventTrigger)
                            result = InsertTrigger((IEventTrigger)item);

                        if (result)
                        {
                            dataToInsert.Dequeue();
                            attempts = 0;
                        }
                        else
                        {
                            if (attempts < 10)
                                attempts++;
                            else
                            {
                                dataToInsert.Dequeue();
                                attempts = 0;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error firing ConnectionStateChanged event", ex);
            }

            insertingData = false;
        }

        protected abstract bool InsertAlarm(Alarm _alarm);
        protected abstract bool ResolveAlarm(ResolveDetailItem _resolveDetailItem);
        protected abstract bool ResolveAlarm(AlarmResolve _alarmResolve);
        protected abstract bool UpdateAlarmProperty(PropertyUpdate _alarmUpdate);
        protected abstract bool UpdateSiteInstruction(SiteInstructionStep _siteInstructionStep);
        protected abstract bool InsertAlarmImage(EventImage _alarmImage);
        protected abstract bool InsertOccurrenceBookEntry(OBEntry _obEntry);
        protected abstract bool InsertAlarmComment(AlarmComment _comment);
        protected abstract bool InsertTrigger(IEventTrigger _trigger);
        #endregion  Inserting Data

        #region Get Data
        public List<Alarm> GetUnresolvedAlarms()
        {
            return GetAlarmHistory(null, null, null, "", null, false);
        }

        /// <param name="_startDate">Default to DateTime.MinValue</param>
        /// <param name="_endDate">Default to DateTime.MaxValue</param>
        /// <param name="_alarmType">Null for all</param>
        /// <param name="_alarmName">Partial search, empty string for all</param>
        /// <param name="_escalationLevel">Including higher level. Null for all</param>
        /// <returns></returns>
        public abstract List<Alarm> GetAlarmHistory(DateTime? _startDate, DateTime? _endDate, eNerve.DataTypes.AlarmType? _alarmType, string _alarmName, EscalationLevel? _escalationLevel, bool? _resolved);

        public abstract List<IEventTrigger> GetAlarmTriggers(Guid _alarmID, object _con = null);
        protected abstract double[] GetAlarmTriggerValues(Guid _alarmTriggerID, object _con = null);
        public abstract List<EventImage> GetAlarmImages(Guid _alarmID, object _con = null);
        public abstract List<EventImage> GetAlarmImages(Guid _alarmID, int? _lastXImages, object _con = null);
        public abstract List<SiteInstructionStep> GetAlarmInstructionSteps(Guid _alarmID, object _con = null);
        public abstract List<AlarmResolve> GetAlarmResolveItems(Guid _alarmID, object _con = null);

        public abstract List<OccurrenceBook> GetOccurrenceBookHistory(DateTime? _startDate, DateTime? _endDate, string _typeDescription, string _description, string _userName);
        public abstract List<OBSubEntry> GetOBSubEntries(Guid _occurrenceBookID);

        public abstract List<AlarmComment> GetAlarmComments(Guid _alarmID, object _con = null);
        #endregion Get Data
    }
}
