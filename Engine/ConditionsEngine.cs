﻿using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackBoxEngine
{
    public class ConditionsEngine : IDisposable
    {
        private readonly List<Condition> conditions;
        private AlarmConditionsDoc alarmConditionsDoc = null;

        public AlarmConditionsDoc AlarmConditionsDoc
        {
            get { return alarmConditionsDoc; }
        }

        public ConditionsEngine()
        {
            conditions = new List<Condition>();
        }

        #region Cleanup
        ~ConditionsEngine()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            ClearConditions();

            if (alarmConditionsDoc != null)
            {
                alarmConditionsDoc.Dispose();
                alarmConditionsDoc = null;
            }
        }

        private void ClearConditions()
        {
            if (conditions != null)
            {
                for (int i = conditions.Count - 1; i >= 0; i--)
                {
                    Condition c = conditions[i];
                    conditions.RemoveAt(i);
                    c.Dispose();
                }
            }
        }
        #endregion

        public void LoadConditions(string alarmConditionsDocPath = "")
        {
            if (alarmConditionsDoc == null)
                alarmConditionsDoc = new AlarmConditionsDoc("AlarmConditions");

            alarmConditionsDoc.Load(alarmConditionsDocPath);

            LoadConditions();
        }

        public void LoadConditions(AlarmConditionsDoc _alarmConditionsDoc)
        {
            alarmConditionsDoc.Dispose();
            alarmConditionsDoc = _alarmConditionsDoc;

            LoadConditions();
        }

        private void LoadConditions()
        {
            try
            {
                ClearConditions();                

                lock (conditions)
                {
                    foreach (XmlNode conditionNode in alarmConditionsDoc)
                    {
                        Condition condition = new Condition()
                        {
                            Name = XmlNode.ParseString(conditionNode, conditionNode.Name, "Name"),
                            CauseAlarm = XmlNode.ParseBool(conditionNode["CauseAlarm"], true),
                            //Cooldown = XmlNode.ParseDouble(conditionNode["CoolDown"], 0),
                            //TimeFrame = XmlNode.ParseDouble(conditionNode["TriggersTimeFrame"], 0),
                            PercentageTriggersNeeded = XmlNode.ParseDouble(conditionNode["PercentageTriggersNeeded"], 100)
                        };

                        int cooldown;
                        int timeframe;
                        string err;

                        if (TimeSpanParser.TryParse(XmlNode.ParseString(conditionNode["CoolDown"], "0"), out cooldown, out err))
                            condition.Cooldown = cooldown;
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Error parsing CoolDown: {0}", err), null);

                        if (TimeSpanParser.TryParse(XmlNode.ParseString(conditionNode["TriggersTimeFrame"], "0"), out timeframe, out err))
                            condition.TimeFrame = timeframe;
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Error parsing TriggersTimeFrame: {0}", err), null);

                        XmlNode triggersNode = conditionNode["Triggers"];
                        if (triggersNode != null)
                        {
                            foreach (XmlNode triggerNode in triggersNode)
                            {
                                ConditionTrigger conditionTrigger = new ConditionTrigger()
                                {
                                    DeviceName = XmlNode.ParseString(triggerNode, "", "DeviceName"),
                                    DetectorName = XmlNode.ParseString(triggerNode, "", "DetectorName"),
                                    TrueState = XmlNode.ParseBool(triggerNode, true, "TrueState"),
                                    FalseState = XmlNode.ParseBool(triggerNode, false, "FalseState"),
                                    TriggersToRaiseEvent = XmlNode.ParseInteger(triggerNode, 1, "TriggersToRaiseEvent"),
                                    MinThreshold = XmlNode.ParseDouble(triggerNode, double.MinValue, "MinThreshold"),
                                    MaxThreshold = XmlNode.ParseDouble(triggerNode, double.MaxValue, "MaxThreshold"),
                                    DetectorType = XmlNode.ParseBool(triggerNode, false, "IsAnalog") ? DetectorType.Analog : DetectorType.Digital,
                                    Mandatory = XmlNode.ParseBool(triggerNode, false, "Mandatory")
                                };

                                if (TimeSpanParser.TryParse(XmlNode.ParseString(triggerNode["TriggersToRaiseEventTimeFrame"], "0"), out timeframe, out err))
                                    conditionTrigger.TriggersToRaiseEventTimeFrame = timeframe;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Error parsing TriggersToRaiseEventTimeFrame: {0}", err), null);
                                
                                condition.AddConditionTrigger(conditionTrigger);
                            }
                        }

                        conditions.Add(condition);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to load AlarmConditions doc", ex);
            }
        }

        public IEnumerable<Event> ProcessTrigger(IEventTrigger _trigger)
        {
            List<Event> resultEvents = null;

            lock (conditions)
            {
                foreach (Condition condition in conditions)
                {
                    Event e = condition.ProcessTrigger(_trigger);
                    if (e != null)
                    {
                        if (resultEvents == null)
                            resultEvents = new List<Event>();

                        resultEvents.Add(e);
                    }
                }
            }

            return resultEvents;
        }
    }
}
