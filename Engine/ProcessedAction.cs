﻿using eNerve.DataTypes;
using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackBoxEngine
{
    public class ProcessedAction
    {
        public Event Event {get; set;}
        public IAction Action { get; set; }
        public int Retries { get; set; }
        public DateTime FirstTry { get; set; }

        public ProcessedAction()
        {
            Retries = 0;
            FirstTry = DateTime.MinValue;
        }

        public ProcessedAction(Event _event, IAction _action)
        {
            Event = _event;
            Action = _action;
            Retries = 0;
            FirstTry = DateTime.MinValue;
        }
    }
}
