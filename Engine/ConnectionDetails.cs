using System;
using System.Reflection;
using eThele.Essentials;

namespace BlackBoxEngine
{
    /// <summary>
    /// Contains connection details and credentials for SQL database. Server must be populated with instance name.
    /// </summary>
    [Serializable]
    public class ConnectionDetails
    {
        public string Server { get; set; }
        public string Database { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string ReportServer { get; set; }

        public string FailureReason { get; set; }

        public bool Valid { get; set; }

        /// <summary>
        /// Load connection details from file
        /// </summary>
        public void Load()
        {
            try
            {
                // Uninitialised: Load from file.
                XmlNode connectionDetailsNode = Settings.TheeSettings["connectionDetails"];
                if ((connectionDetailsNode != null) && (connectionDetailsNode.AttributeCount >= 4))
                {
                    Database = XmlNode.ParseString(connectionDetailsNode, "BlackBox", "database");
                    Password = XmlNode.ParseString(connectionDetailsNode, "Bl@ckB0x", "password");
                    Server = XmlNode.ParseString(connectionDetailsNode, ".", "server");
                    UserId = XmlNode.ParseString(connectionDetailsNode, "BlackBox", "userid");
                    string reportServer = ".";
                    if (XmlNode.TryParseString(connectionDetailsNode, out reportServer, reportServer, "reportserver"))
                    {
                        ReportServer = reportServer;
                    }
                    else
                    {
                        Save(reportServer);
                    }
                }
                else
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                        MethodBase.GetCurrentMethod().Name), "Failed to load DB settings");
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void Save(string reportServer)
        {
            try
            {
                XmlNode connectionDetailsNode = Settings.TheeSettings["connectionDetails"];
                if (connectionDetailsNode == null) // Behoort nie te gebeur nie maar kom ons wees volledig.
                {
                    connectionDetailsNode.AddAttribute("reportserver", reportServer);
                }
                else
                {
                    if (connectionDetailsNode.Attribute("reportserver") == null)
                        connectionDetailsNode.AddAttribute("reportserver", reportServer);
                    else
                        connectionDetailsNode.Attribute("reportserver").Value = reportServer;
                }
                Settings.TheeSettings.Save();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void Save(ConnectionDetails NewCredentials)
        {
            Database = NewCredentials.Database;
            Server = NewCredentials.Server;
            UserId = NewCredentials.UserId;
            Password = NewCredentials.Password;
            ReportServer = NewCredentials.ReportServer;
            try
            {
                XmlNode connectionDetailsNode = Settings.TheeSettings["connectionDetails"];
                if (connectionDetailsNode == null)
                {// Node does not exist - create a new one
                    connectionDetailsNode = Settings.TheeSettings.Add("connectionDetails", "");
                    connectionDetailsNode.AddAttribute("server", NewCredentials.Server);
                    connectionDetailsNode.AddAttribute("database", NewCredentials.Database);
                    connectionDetailsNode.AddAttribute("userid", NewCredentials.UserId);
                    connectionDetailsNode.AddAttribute("password", NewCredentials.Password);
                    connectionDetailsNode.AddAttribute("reportserver", NewCredentials.ReportServer);
                }
                else
                {
                    connectionDetailsNode.Attribute("server").Value = NewCredentials.Server;
                    connectionDetailsNode.Attribute("database").Value = NewCredentials.Database;
                    connectionDetailsNode.Attribute("userid").Value = NewCredentials.UserId;
                    connectionDetailsNode.Attribute("password").Value = NewCredentials.Password;
                    connectionDetailsNode.Attribute("reportserver").Value = ReportServer;
                }
                Settings.TheeSettings.Save();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), "Failed to save DB settings", ex);
            }
        }
        public string ConnectionString()
        {
            return string.Format("Data Source={0};Initial Catalog={1};User ID={2};Password={3};MultipleActiveResultSets=True;", Server, Database, UserId, Password);
        }

        /// <summary>
        /// Compile connection string from provided credentials for testing
        /// </summary>
        /// <param name="TestCredentials">New credentials received from client</param>
        /// <returns>Formatted connection string for connection to SQL</returns>
        public static string ConnectionString(ConnectionDetails TestCredentials)
        {
            if (TestCredentials != null)
                return "Data Source=" + TestCredentials.Server
                + ";Initial Catalog=" + TestCredentials.Database
                + ";User ID=" + TestCredentials.UserId
                + ";Password=" + TestCredentials.Password
                + ";MultipleActiveResultSets=True;";
            else
                return "";
        }
    }
}
