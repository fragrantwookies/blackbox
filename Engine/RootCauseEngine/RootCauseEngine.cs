﻿using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackBoxEngine.RootCauseEngine
{
    public class RootCauseEngine : IDisposable
    {
        private List<Event> unresolvedAlarms;
        private readonly List<Condition> conditions;
        private AlarmConditionsDoc rootCauseConditionsDoc = null;

        public AlarmConditionsDoc RootCauseConditionsDoc { get { return rootCauseConditionsDoc; } }

        #region Cleanup
        ~RootCauseEngine()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            ClearConditions();

            if (rootCauseConditionsDoc != null)
            {
                rootCauseConditionsDoc.Dispose();
                rootCauseConditionsDoc = null;
            }
        }

        private void ClearConditions()
        {
            if (conditions != null)
            {
                for (int i = conditions.Count - 1; i >= 0; i--)
                {
                    Condition c = conditions[i];
                    conditions.RemoveAt(i);
                    c.Dispose();
                }
            }
        }
        #endregion        

        public void LoadConditions(string rootCauseConditionsDocPath = "")
        {
            if (rootCauseConditionsDoc == null)
                rootCauseConditionsDoc = new AlarmConditionsDoc("RootCauseConditions");

            rootCauseConditionsDoc.Load(rootCauseConditionsDocPath);

            LoadConditions();
        }

        public void LoadConditions(AlarmConditionsDoc _rootCauseConditionsDoc)
        {
            rootCauseConditionsDoc.Dispose();
            rootCauseConditionsDoc = _rootCauseConditionsDoc;

            rootCauseConditionsDoc.Save();

            LoadConditions();
        }

        private void LoadConditions()
        {
            try
            {
                ClearConditions();

                lock (conditions)
                {
                    foreach (XmlNode conditionNode in rootCauseConditionsDoc)
                    {
                        Condition condition = new Condition()
                        {
                            Name = XmlNode.ParseString(conditionNode, conditionNode.Name, "Name"),
                            CauseAlarm = XmlNode.ParseBool(conditionNode["CauseAlarm"], true),
                            Cooldown = XmlNode.ParseDouble(conditionNode["CoolDown"], 0),
                            TimeFrame = XmlNode.ParseDouble(conditionNode["TriggersTimeFrame"], 0),
                            PercentageTriggersNeeded = XmlNode.ParseDouble(conditionNode["PercentageTriggersNeeded"], 100)
                        };

                        int cooldown;
                        int timeframe;
                        string err;

                        if (TimeSpanParser.TryParse(XmlNode.ParseString(conditionNode["CoolDown"], "0"), out cooldown, out err))
                            condition.Cooldown = cooldown;
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Error parsing CoolDown: {0}", err), null);

                        if (TimeSpanParser.TryParse(XmlNode.ParseString(conditionNode["TriggersTimeFrame"], "0"), out timeframe, out err))
                            condition.TimeFrame = timeframe;
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Error parsing TriggersTimeFrame: {0}", err), null);

                        XmlNode triggersNode = conditionNode["Triggers"];
                        if (triggersNode != null)
                        {
                            foreach (XmlNode triggerNode in triggersNode)
                            {
                                ConditionTrigger conditionTrigger = new ConditionTrigger()
                                {
                                    DeviceName = XmlNode.ParseString(triggerNode, "", "DeviceName"),
                                    DetectorName = XmlNode.ParseString(triggerNode, "", "DetectorName"),
                                    TrueState = XmlNode.ParseBool(triggerNode, true, "TrueState"),
                                    FalseState = XmlNode.ParseBool(triggerNode, false, "FalseState"),
                                    TriggersToRaiseEvent = XmlNode.ParseInteger(triggerNode, 1, "TriggersToRaiseEvent"),
                                    MinThreshold = XmlNode.ParseDouble(triggerNode, double.MinValue, "MinThreshold"),
                                    MaxThreshold = XmlNode.ParseDouble(triggerNode, double.MaxValue, "MaxThreshold"),
                                    DetectorType = XmlNode.ParseBool(triggerNode, false, "IsAnalog") ? DetectorType.Analog : DetectorType.Digital,
                                    Mandatory = XmlNode.ParseBool(triggerNode, false, "Mandatory")
                                };

                                if (TimeSpanParser.TryParse(XmlNode.ParseString(triggerNode["TriggersToRaiseEventTimeFrame"], "0"), out timeframe, out err))
                                    conditionTrigger.TriggersToRaiseEventTimeFrame = timeframe;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Error parsing TriggersToRaiseEventTimeFrame: {0}", err), null);

                                condition.AddConditionTrigger(conditionTrigger);
                            }
                        }

                        conditions.Add(condition);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to load AlarmConditions doc", ex);
            }
        }

        public void Start()
        {
            LoadConditions("");

            unresolvedAlarms = new List<Event>();
        }

        public void Stop()
        {
            ClearConditions();

            if (rootCauseConditionsDoc != null)
            {
                rootCauseConditionsDoc.Dispose();
                rootCauseConditionsDoc = null;
            }

            if(unresolvedAlarms != null)
            {
                for (int i = unresolvedAlarms.Count - 1; i >= 0; i--)
                {
                    Event e = unresolvedAlarms[i];
                    e.Dispose();
                    unresolvedAlarms.RemoveAt(i);
                }

                unresolvedAlarms = null;
            }
        }

        public void ProcessTrigger(IEventTrigger _trigger)
        {
            List<Event> resultEvents = null;

            lock (conditions)
            {
                foreach (Condition condition in conditions)
                {
                    Event e = condition.ProcessTrigger(_trigger);
                    if (e != null)
                    {
                        if (resultEvents == null)
                            resultEvents = new List<Event>();

                        resultEvents.Add(e);
                    }
                }
            }

            if(resultEvents != null && resultEvents.Count > 0)
            {
                foreach(Event e in resultEvents)
                {
                    int searchResult = unresolvedAlarms.BinarySearch(e);
                    if (searchResult < 0)
                    {
                        unresolvedAlarms.Insert(~searchResult, e);
                        RootCauseService.RootCauseServer.SendMessage(MessageType.RootCauseAlarm, e);
                    }
                    else
                    {
                        Event resultEvent = unresolvedAlarms[searchResult];

                        foreach(IEventTrigger t in e.Triggers)
                        {
                            IEventTrigger newTrigger = resultEvent.AddTrigger(t);
                            RootCauseService.RootCauseServer.SendMessage(MessageType.RootCauseAlarmTrigger, newTrigger);
                        }
                    }
                }
            }
            else
            {
                RootCauseService.RootCauseServer.SendMessage(MessageType.RootCauseReturnTrigger, _trigger);
            }
        }

        public void ResolveAlarm(Guid _id)
        {
            if (unresolvedAlarms != null)
            {
                for (int i = unresolvedAlarms.Count - 1; i >= 0; i--)
                {
                    Event e = unresolvedAlarms[i];
                    if (e.ID == _id)
                    {
                        e.Dispose();
                        unresolvedAlarms.RemoveAt(i);
                    }
                }
            }
        }
    }
}
