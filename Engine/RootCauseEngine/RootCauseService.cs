﻿using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace BlackBoxEngine.RootCauseEngine
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Fields should be disposed")]
    public class RootCauseService : IDisposable
    {
        private int serverPort;
        private RootCauseEngine rootCauseEngine;
        private static Server server;

        public static Server RootCauseServer { get { return server; } }

        public RootCauseService(int _serverPort = 0xFFF)
        {    
            serverPort = _serverPort;            
        }

        #region Cleanup
        ~RootCauseService()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            Stop();
        }
        #endregion

        public bool Start()
        {
            bool result = false;

            try
            {
                if (server == null)
                {
                    server = new Server(serverPort);

                    server.OnObjectMessageReceived += server_OnObjectMessageReceived;
                    server.OnStringMessageReceived += server_OnStringMessageReceived;
                    server.OnBinaryMessageReceived += server_OnBinaryMessageReceived;
                    server.ClientDisconnected += server_ClientDisconnected;
                }

                result = true;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public bool Stop()
        {
            bool result = false;

            try
            {
                if (server != null)
                {
                    server.ClientDisconnected -= server_ClientDisconnected;
                    server.OnObjectMessageReceived -= server_OnObjectMessageReceived;
                    server.OnStringMessageReceived -= server_OnStringMessageReceived;
                    server.OnBinaryMessageReceived -= server_OnBinaryMessageReceived;
                    server.Dispose();
                    server = null;
                }

                if (rootCauseEngine != null)
                {
                    rootCauseEngine.Stop();                    

                    rootCauseEngine.Dispose();
                    rootCauseEngine = null;
                }

                Settings.Selfdesctruct();

                result = true;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        void server_ClientDisconnected(object sender, System.Net.Sockets.TcpClient e)
        {
            
        }

        void server_OnObjectMessageReceived(object sender, CommsNode.ObjectMessageEventArgs e)
        {
            try
            {
                switch (e.Message_Type)
                {
                    case MessageType.Trigger:
                        IEventTrigger trigger = e.Message as IEventTrigger;
                        if (trigger != null)
                            rootCauseEngine.ProcessTrigger(trigger);
                        break;
                    case MessageType.AlarmResolve://Intensional fall through
                    case MessageType.GroupAlarmResolve:                        
                        AlarmResolve alarmResolve = (AlarmResolve)e.Message;
                        rootCauseEngine.ResolveAlarm(alarmResolve.AlarmID);
                        break;
                    case MessageType.RequestRootCauseConditionsDoc:
                        server.SendMessage(e.Client, MessageType.RootCauseConditionsDoc, rootCauseEngine.RootCauseConditionsDoc);
                        break;
                    case MessageType.RootCauseConditionsDoc: //This will happen when a client saved a new config
                        AlarmConditionsDoc doc = e.Message as AlarmConditionsDoc;
                        if(doc!= null)
                            rootCauseEngine.LoadConditions(doc); //Load new doc and save it too
                        break;
                    default:
                        server.SendMessage(e.Client, MessageType.RespondMessageTypeNotSupported);
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Unknown message type received: " + e.Message_Type, null);
                        break;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        void server_OnStringMessageReceived(object sender, CommsNode.StringMessageEventArgs e)
        {
            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "String messages not supported");
        }

        void server_OnBinaryMessageReceived(object sender, CommsNode.BinaryMessageEventArgs e)
        {
            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Binary messages not supported");
        }
    }
}
