﻿using eThele.Essentials;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using eNerve.DataTypes;

namespace BlackBoxEngine
{
    public class Client : CommsNode
    {
        protected TcpClient client;
        int retries;

        public class ServerDetails
        {
            public string IpAddress {get; set;}
            public int PortNo { get; set; }
        }

        public enum ConnectState { Disconnected, Connecting, Connected };

        private ConnectState ConnectionState
        {
            get { return ConnectionStatus; }
            set 
            {
                if (ConnectionStatus != value)
                {
                    ConnectionStatus = value;

                    if (ConnectionStatusChanged != null)
                        ConnectionStatusChanged(this, ConnectionStatus);
                }
            }
        }

        public ConnectState ConnectionStatus { get; private set; }

        public delegate void ConnectionStatusEventHandler(object sender, ConnectState connectionState);
        public event ConnectionStatusEventHandler ConnectionStatusChanged;

        public Client()
        {
            client = null;
            ConnectionStatus = ConnectState.Disconnected;            
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (client != null)
            {
                client.Close();
                client = null;
            }
        }

        /// <summary>
        /// Negative retries (default) means infinite. 0 means try only once. Positive retries specifies the number of times to retry the connection.
        /// </summary>
        /// <param name="_ipAddress"></param>
        /// <param name="_port"></param>
        /// <param name="_retries"></param>
        public void Connect(string _ipAddress, int _port, int _retries = -1)
        {
            try
            {
                if (ConnectionState == ConnectState.Disconnected)
                {
                    ConnectionState = ConnectState.Connecting;

                    if (listen)
                        Disconnect();

                    retries = _retries;

                    ServerConfig = new IPEndPoint(IPAddress.Parse(_ipAddress), _port);

                    Thread connCheckThread = new Thread(CheckConnectivity);
                    connCheckThread.Start();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public IPEndPoint ServerConfig { get; private set; }

        public static ServerDetails GetServerDetails()
        {
            XmlNode serverDetailsNode = Settings.TheeSettings["ServerDetails"];
            ServerDetails serverDetails = new ServerDetails() { IpAddress = "127.0.0.1", PortNo = 4050 };
            if (serverDetailsNode != null)
            {// Details exist in file
                serverDetails.IpAddress = XmlNode.ParseString(serverDetailsNode, "127.0.0.1", "ipaddress");
                serverDetails.PortNo = XmlNode.ParseInteger(serverDetailsNode, 4050, "portno");
            }
            else
            {// Could not read details from file - default to loopback and log.
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to load ServerDetails from file");
                Settings.TheeSettings.Add("ServerDetails", "");
                serverDetailsNode = Settings.TheeSettings["ServerDetails"];
                if (serverDetailsNode != null)
                {
                    serverDetailsNode.AddAttribute("ipaddress", "127.0.0.1");
                    serverDetailsNode.AddAttribute("portno", "4050");
                }
                else
                {// Bad wrong - could not save server details to file
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to save ServerDetails to file ");
                }
                //Settings.TheeSettings.Save();
            }
            return serverDetails;
        }

        public void Disconnect()
        {
            listen = false;
            Thread.Sleep(200);
            DisconnectClient(client);
            client = null;
        }

        protected override void DisconnectClient(TcpClient _client)
        {
            base.DisconnectClient(_client);

            if (client != null)
            {
                client.Close();
                client = null;
            }
        }

        protected void CheckConnectivity()
        {
            listen = true;
            Thread readThread = new Thread(() => Read(client));

            do
            {
                try
                {
                    if (client == null)
                    {
                        client = new TcpClient();
                    }
                    if (!client.Connected)
                    {
                        client.Connect(ServerConfig);
                    }
                    if (client.Connected)
                    {
                        if (ConnectionState != ConnectState.Connected)
                        {
                            ConnectionState = ConnectState.Connected;
                        }
                        if ((readThread != null) && (!readThread.IsAlive))
                        {
                            try
                            {
                                readThread.Start();
                            }
                            catch (ThreadStateException)
                            { // Thread has dead and can't be revived - recreate it.
                                readThread = new Thread(() => Read(client));
                                readThread.Start();
                            }
                        }
                    }
                    else
                    {
                        listen = false;
                        ConnectionState = ConnectState.Disconnected;
                        break;
                    }
                }
                catch (SocketException)
                {// Server unavailable
                    //Disconnect();
                }
                catch (Exception ex)
                {
                    ConnectionState = ConnectState.Disconnected;
                    listen = false;
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                }
                if (retries > 0)
                    retries--;

                Thread.Sleep(3000);
            } while (listen && retries != 0);
            if (ConnectionState == ConnectState.Connecting)
                ConnectionState = ConnectState.Disconnected;
        }

        protected override void Read(object _client)
        {
            ConnectionState = ConnectState.Connected;
            base.Read(_client);
            ConnectionState = ConnectState.Disconnected;
        }

        public override bool SendMessage(MessageType _messageType, object _message = null)
        {
            return SendMessage(client, _messageType, _message);
        }

        public static void SaveServerDetails(ServerDetails serverDetails)
        {
            try
            {                
                XmlNode serverDetailsNode = Settings.TheeSettings["ServerDetails"];
                if (serverDetailsNode != null)
                {// Node exists - update values
                    serverDetailsNode.Attribute("ipaddress").Value = serverDetails.IpAddress;
                    serverDetailsNode.Attribute("portno").Value = serverDetails.PortNo.ToString();
                }
                else
                {// Settings don't exist - create.
                    serverDetailsNode = Settings.TheeSettings.Add("ServerDetails", "");
                    serverDetailsNode.AddAttribute("ipaddress", serverDetails.IpAddress);
                    serverDetailsNode.AddAttribute("portno", serverDetails.PortNo.ToString());
                }
                Settings.TheeSettings.Save();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Failed to save Server details doc", ex);
            }
        }

        public override bool SendBinaryMessage(byte[] _message, bool _dontCompress = false)
        {
            return SendMessage(client, MessageType.Binary, _dontCompress, _message);
        }
    }
}
