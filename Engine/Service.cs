﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Win32;
using eThele.Essentials;
using eNerve.DataTypes;
using eNervePluginInterface;
using eNerve.DataTypes.Gauges;
using System.Reflection;
using System.Net;

namespace BlackBoxEngine
{
    /// <summary>
    /// This brings the Engine and Server together and manages them.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Fields should be disposed")]
    public class Service : IDisposable
    {
        private bool running = false;

        private int serverPort;
        private Engine engine;
        private static Server server;

        private IPEndPoint rootCauseDetails;
        private static Client rootCauseClient;
        private const string defaultRootCauseIP = "127.0.0.1";
        private const int defaultRootCausePort = 0xFFF;

        private static bool routeCauseServerOperational = false;

        public Service(int _serverPort = 4050)
        {    
            serverPort = _serverPort;            
        }

        #region Cleanup
        ~Service()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            Stop();
        }
        #endregion Cleanup

        #region Stop & Start
        public bool Start()
        {
            bool result = false;

            try
            {
                if (AllowStart())
                {
                    if (engine == null)
                    {
                        engine = new Engine();

                        engine.OnAlarm += engine_OnAlarm;
                        engine.OnGroupAlarmUpdate += engine_OnGroupAlarmUpdate;
                        engine.OnGroupAlarmUpdateList += engine_OnGroupAlarmUpdateList;
                        engine.OnAlarmImages += Engine_OnAlarmImages;
                        engine.AlarmPropertyUpdated += Engine_OnAlarmPropertyUpdated;
                        engine.AlarmResolveItemAdded += Engine_OnAlarmResolved;
                        engine.GroupAlarmPropertyUpdated += Engine_GroupAlarmPropertyUpdated;
                        engine.GroupAlarmResolveItemAdded += Engine_GroupAlarmResolveItemAdded;
                        engine.SiteInstructionUpdated += Engine_OnSiteInstructionUpdated;
                        engine.OccurrenceBookPropertyUpdated += Engine_OccurrenceBookPropertyUpdated;

                        engine.DBConnectionStateChanged += Engine_DBConnectionStateChanged;

                        engine.OnStatusMessage += engine_OnStatusMessage;

                        engine.Start();
                    }

                    if (server == null)
                    {
                        server = new Server(serverPort);

                        server.OnObjectMessageReceived += Server_OnObjectMessageReceived;
                        server.OnStringMessageReceived += Server_OnStringMessageReceived;
                        server.OnBinaryMessageReceived += Server_OnBinaryMessageReceived;
                        server.ClientDisconnected += Server_ClientDisconnected;
                    }

                    if (rootCauseClient == null)
                    {
                        rootCauseClient = new Client();
                        
                        rootCauseDetails = new IPEndPoint(IPAddress.Parse(XmlNode.ParseString(Settings.TheeSettings["RootCauseServer"], defaultRootCauseIP, "IP")), XmlNode.ParseInteger(Settings.TheeSettings["RootCauseServer"], defaultRootCausePort, "Port"));

                        rootCauseClient.ConnectionStatusChanged += RootCauseClient_ConnectionStatusChanged;
                        rootCauseClient.OnObjectMessageReceived += RootCauseClient_OnObjectMessageReceived;
                        rootCauseClient.OnStringMessageReceived += RootCauseClient_OnStringMessageReceived;

                        ConnectRootCause(rootCauseDetails);
                    }

                    result = true;
                }
                else
                {
                    result = false;
                    Exceptions.ExceptionsManager.Save();
                    ScheduleDoc.Selfdesctruct();
                    Exceptions.DisposeManager();
                    Settings.Selfdesctruct();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            running = result;

            return result;
        }

        private void ConnectRootCause(IPEndPoint _rootCauseDetails)
        {
            if (_rootCauseDetails != null)
            {
                string ip = _rootCauseDetails.Address.ToString();

                if(!string.IsNullOrWhiteSpace(ip) && _rootCauseDetails.Port > 0)
                    rootCauseClient.Connect(ip, _rootCauseDetails.Port);
            }
        }


        private void Server_ClientDisconnected(object sender, TcpClient e)
        {
            engine.Users.Disconnect(e);
        }

        private void RootCauseClient_ConnectionStatusChanged(object sender, Client.ConnectState connectionState)
        {
            
        }

        private void RootCauseClient_OnObjectMessageReceived(object sender, CommsNode.ObjectMessageEventArgs e)
        {
            try
            {
                switch (e.Message_Type)
                {
                    case MessageType.RootCauseAlarm:
                        Event rootCauseAlarm = e.Message as Event;
                        if(rootCauseAlarm != null)
                            engine.ProcessRootCauseAlarm(rootCauseAlarm);
                        break;
                    case MessageType.RootCauseAlarmTrigger:
                        IEventTrigger rootCauseTrigger = e.Message as IEventTrigger;
                        if (rootCauseTrigger != null)
                            engine.AddRouteCauseTrigger(rootCauseTrigger);
                        break;
                    case MessageType.RootCauseReturnTrigger:
                        IEventTrigger _trigger = e.Message as IEventTrigger;
                        if (_trigger != null)
                            engine.ProcessRouteCauseReturnTrigger(_trigger);
                        break;
                    case MessageType.RootCauseConditionsDoc:
                        AlarmConditionsDoc doc = e.Message as AlarmConditionsDoc;
                        if(doc != null)
                            server.SendMessage(MessageType.RootCauseConditionsDoc, doc); //just sent it to all the clients, no way of knowing who asked for it, and it doesn't really matter too much. The easiest way is for the client to decide if he want's it or not.
                        break;
                    default:
                        rootCauseClient.SendMessage(e.Client, MessageType.RespondMessageTypeNotSupported);
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Unknown message type received: " + e.Message_Type, null);
                        break;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void RootCauseClient_OnStringMessageReceived(object sender, CommsNode.StringMessageEventArgs e)
        {
            
        }

        public void Stop()
        {
            if (running)
            {
                try
                {
                    if (server != null)
                    {
                        server.ClientDisconnected -= Server_ClientDisconnected;
                        server.OnObjectMessageReceived -= Server_OnObjectMessageReceived;
                        server.OnStringMessageReceived -= Server_OnStringMessageReceived;
                        server.OnBinaryMessageReceived -= Server_OnBinaryMessageReceived;
                        server.Dispose();
                        server = null;
                    }

                    if (engine != null)
                    {
                        engine.Stop();

                        engine.DBConnectionStateChanged -= Engine_DBConnectionStateChanged;

                        engine.OnAlarm -= engine_OnAlarm;
                        engine.OnGroupAlarmUpdate -= engine_OnGroupAlarmUpdate;
                        engine.OnGroupAlarmUpdateList -= engine_OnGroupAlarmUpdateList;
                        engine.OnAlarmImages -= Engine_OnAlarmImages;
                        engine.AlarmPropertyUpdated -= Engine_OnAlarmPropertyUpdated;
                        engine.AlarmResolveItemAdded -= Engine_OnAlarmResolved;
                        engine.SiteInstructionUpdated -= Engine_OnSiteInstructionUpdated;
                        engine.OccurrenceBookPropertyUpdated -= Engine_OccurrenceBookPropertyUpdated;

                        engine.Dispose();
                        engine = null;
                    }

                    if (rootCauseClient != null)
                    { 
                        rootCauseClient.ConnectionStatusChanged -= RootCauseClient_ConnectionStatusChanged;
                        rootCauseClient.OnObjectMessageReceived -= RootCauseClient_OnObjectMessageReceived;
                        rootCauseClient.OnStringMessageReceived -= RootCauseClient_OnStringMessageReceived;
                        rootCauseClient.Disconnect();
                    }

                    ScheduleDoc.Selfdesctruct();
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
                }

                Exceptions.ExceptionsManager.Save();
                Exceptions.DisposeManager();
                Settings.Selfdesctruct();
            }

            running = false;
        }

        #region FL
        protected bool AllowStart()
        {
            bool result = false;

            try
            {
                using (RegistryKey servicekey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\Ethele\\eNerve"))
                {
                    string activationKey = (string)servicekey.GetValue("ActivationCode", "");

                    servicekey.Close();

                    string processorid = "";

                    System.Diagnostics.Process proc = System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo() { FileName = @"wmic", Arguments = "cpu get ProcessorId", UseShellExecute = false, RedirectStandardOutput = true, CreateNoWindow = true });
                    while (!proc.StandardOutput.EndOfStream)
                    {
                        string line = proc.StandardOutput.ReadLine().Trim();

                        if (!string.IsNullOrWhiteSpace(line) && line != "ProcessorId")
                        {
                            processorid = line;
                            break;
                        }
                    }

                    result = activationKey == Encrypt(processorid);
                    

                        //using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_Processor"))
                        //{
                        //    foreach (ManagementObject mobject in searcher.Get())
                        //    {
                        //        string processorid = (string)mobject.Properties["ProcessorID"].Value;

                        //        if (activationKey == Encrypt(processorid))
                        //        {
                        //            result = true;
                        //            break;
                        //        }
                        //    }
                        //}
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private static string Encrypt(string val)
        {
            string result = "";

            try
            {
                byte[] key;
                byte[] iv;
                byte[] salt = new byte[8] { 0x23, 0x43, 0x29, 0x3F, 0xDA, 0xBD, 0x5B, 0xED };

                //using (RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider())
                //{
                //    // Fill the array with a random value.
                //    rngCsp.GetBytes(salt);
                //}

                using (Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes("Live Long", salt))
                {
                    key = pdb.GetBytes(16);
                }

                using (Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes("And Prosper", salt))
                {
                    iv = pdb.GetBytes(16);
                }

                byte[] code = EncryptStringToBytes_Aes(val, key, iv);

                byte[] resultarray = new byte[code.Length + salt.Length];
                code.CopyTo(resultarray, 0);
                salt.CopyTo(resultarray, code.Length);

                string fullCode = BitConverter.ToString(resultarray).Replace("-", "");

                for (int i = 0; i < fullCode.Length; i++)
                    if (i % 5 == 0)
                        result += fullCode[i];
            }
            catch
            {
                result = "";
            }

            return result;
        }

        private static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an AesCryptoServiceProvider object 
            // with the specified key and IV. 
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);


                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream. 
            return encrypted;
        } 
        #endregion FL
        #endregion Stop Start

        #region Alarm injection
        /// <summary>
        /// Just for testing purposes
        /// </summary>
        public void InjectAlarm(Alarm _alarm)
        {
            if (engine != null)
                engine.InjectAlarm(_alarm);
        }

        public void InjectTrigger(IEventTrigger _trigger)
        {
            if (engine != null)
                engine.Trigger(_trigger);
        } 
        #endregion Alarm injection

        #region Status Messages
        public void InjectStatusMessage(StatusMessage _statusMessage)
        {
            if (server != null)
                server.SendMessage(MessageType.StatusMessage, _statusMessage);
        }

        public StatusMessage SendStatusMessage(TcpClient _client, string _message, bool _animation = true)
        {
            return SendStatusMessage(_client, _message, TimeSpan.FromMinutes(5), _animation);
        }

        public StatusMessage SendStatusMessage(TcpClient _client, string _message, TimeSpan _autoRemove, bool _animation = true)
        {
            StatusMessage statusMsg = null;

            try
            {
                statusMsg = new StatusMessage() { StatusID = Guid.NewGuid(), Message = _message, MessageTimeStamp = DateTime.Now, Animation = _animation, AutoRemove = _autoRemove };
                server.SendMessage(_client, MessageType.StatusMessage, statusMsg);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return statusMsg;
        }

        public void RemoveStatusMessage(TcpClient _client, StatusMessage _statusMsg)
        {
            try
            {
                if (_client != null)
                    server.SendMessage(_client, MessageType.StatusMessageRemove, _statusMsg);
                else
                    server.SendMessage(MessageType.StatusMessageRemove, _statusMsg);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion  Status Messages

        #region SendMessages
        public static bool SendMessage(MessageType _messageType, object _message = null)
        {
            bool result;

            if (server != null)
                result = server.SendMessage(_messageType, _message);
            else
                result = false;

            return result;
        }

        public static bool SendMessage(TcpClient _client, MessageType _messageType, bool _dontCompress, object _message = null)
        {
            bool result;

            if (server != null)
                result = server.SendMessage(_client, _messageType, _dontCompress, _message);
            else
                result = false;

            return result;
        }

        public static bool SendMessage(TcpClient _client, MessageType _messageType, object _message = null)
        {
            bool result;

            if (server != null)
                result = server.SendMessage(_client, _messageType, _message);
            else
                result = false;

            return result;
        }

        public static bool SendRootCauseMessage(MessageType _messageType, object _message = null)
        {
            bool result;

            if (rootCauseClient != null)
                result = rootCauseClient.SendMessage(_messageType, _message);
            else
                result = false;

            return result;
        }
        #endregion
        
        #region Engine event handlers

        void engine_OnStatusMessage(object sender, StatusMessageEventArgs e)
        {
            try
            {
                if (server != null)
                {
                    server.SendMessage(e.Action == StatusMessageEventArgs.StatusMessageAction.Display ? MessageType.StatusMessage : MessageType.StatusMessageRemove, e.Message);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        
        void engine_OnAlarm(object sender, AlarmEventArgs e)
        {
            try
            {
                if (server != null)
                {
                    lock (e.Alarm)
                    {
                        server.SendMessage(e.Alarm is GroupAlarm ? MessageType.GroupAlarm : MessageType.Alarm, e.Alarm);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        void engine_OnGroupAlarmUpdate(object sender, GroupAlarmUpdateEventArgs e)
        {
            try
            {
                if (server != null)
                    lock (e.Update)
                    {
                        server.SendMessage(MessageType.GroupAlarmUpdate, e.Update);
                    }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        void engine_OnGroupAlarmUpdateList(object sender, GroupAlarmUpdateListEventArgs e)
        {
            try
            {
                if (server != null)
                    lock (e.Update)
                    {
                        server.SendMessage(MessageType.GroupAlarmUpdate, e.Update);
                    }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void Engine_OnAlarmImages(object sender, List<EventImage> alarmImages)
        {
            try
            {
                if (server != null)
                {
                    lock (alarmImages)
                    {
                        server.SendMessage(MessageType.AlarmImages, alarmImages);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void Engine_OnAlarmPropertyUpdated(object sender, PropertyUpdateEventArgs e)
        {
            try
            {
                if (server != null)
                {
                    lock (e.PropertyToUpdate)
                    {
                        server.SendMessage(e.PropertyToUpdate is GroupAlarmPropertyUpdate ? MessageType.GroupAlarmPropertyUpdate : MessageType.AlarmPropertyUpdate, e.PropertyToUpdate);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void Engine_OnAlarmResolved(object sender, AlarmResolvedEventArgs e)
        {
            try
            {
                if (server != null)
                {
                    lock (e.AlarmResolveItem)
                    {
                        server.SendMessage(MessageType.AlarmResolve, e.AlarmResolveItem);

                        rootCauseClient.SendMessage(MessageType.AlarmResolve, e.AlarmResolveItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void Engine_GroupAlarmResolveItemAdded(object sender, AlarmResolvedEventArgs e)
        {
            try
            {
                if (server != null)
                {
                    lock (e.AlarmResolveItem)
                    {
                        server.SendMessage(MessageType.GroupAlarmResolve, e.AlarmResolveItem);

                        rootCauseClient.SendMessage(MessageType.GroupAlarmResolve, e.AlarmResolveItem);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void Engine_GroupAlarmPropertyUpdated(object sender, PropertyUpdateEventArgs e)
        {
            try
            {
                if (server != null)
                {
                    lock (e.PropertyToUpdate)
                    {
                        server.SendMessage(MessageType.GroupAlarmPropertyUpdate, e.PropertyToUpdate);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void Engine_OnSiteInstructionUpdated(object sender, SiteInstructionStep siteInstructionStep)
        {
            try
            {
                if (server != null)
                {
                    lock (siteInstructionStep)
                    {
                        server.SendMessage(MessageType.SiteInstructionStep, siteInstructionStep);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }


        private void Engine_OccurrenceBookPropertyUpdated(object sender, PropertyUpdate propertyToUpdate)
        {
            try
            {
                if (server != null)
                {
                    lock (propertyToUpdate)
                    {
                        server.SendMessage(MessageType.OccurenceBookPropertyUpdate, propertyToUpdate);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        //private void Engine_DeviceOnline(object sender, string _deviceMac)
        //{
        //    try
        //    {
        //        if (server != null)
        //            server.SendMessage(MessageType.DeviceOnline, _deviceMac);
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
        //    }
        //}

        //private void Engine_DeviceOffline(object sender, string _deviceMac)
        //{
        //    try
        //    {
        //        if (server != null)
        //            server.SendMessage(MessageType.DeviceOffline, _deviceMac);
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
        //    }
        //}

        private void Engine_DBConnectionStateChanged(object sender, System.Data.StateChangeEventArgs e)
        {
            try
            {
                if (server != null)
                    server.SendMessage(MessageType.DatabaseConnectionState, e.CurrentState);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Engine event handlers

        #region Server event handlers
        void Server_OnObjectMessageReceived(object sender, CommsNode.ObjectMessageEventArgs e)
        {
            try
            {
                User senderUser = engine.Users.UserAtClient(e.Client);

                switch (e.Message_Type)
                {
                    #region Server Info
                    case MessageType.RequestServerID:
                        server.SendMessage(e.Client, MessageType.ServerID, engine.ServerID);
                        break;

                    case MessageType.RequestServerName:
                        server.SendMessage(e.Client, MessageType.ServerName, engine.ServerName);
                        break;

                    case MessageType.RequestServerVersion:
                        server.SendMessage(e.Client, MessageType.ServerVersion, Assembly.GetExecutingAssembly().GetName().Version);
                        break;                    
                    #endregion

                    #region Config docs related messages
                    case MessageType.RequestDeviceDoc:
                        server.SendMessage(e.Client, MessageType.DeviceDoc, engine.Devices_Doc);
                        break;

                    case MessageType.DeviceDoc:
                        {
                            lock (engine.Devices_Doc)
                            {
                                string backupfilepath = Path.Combine(string.Format("{0}\\Ethele\\RioSoft\\{1}\\Config\\Backup\\Devices", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), Assembly.GetEntryAssembly().GetName().Name), string.Format("Devices_{0:yyyy_MM_dd_HH_mm_ss}.xml", DateTime.Now));
                                engine.Devices_Doc.Save(backupfilepath);

                                engine.Devices_Doc = (DevicesDoc)e.Message;
                                engine.Devices_Doc.Save();
                                engine.Devices_Doc.Load();

                                engine.Detectors.Reload();

                                engine.RestartPluginManager();
                                engine.RestartBlackBoxManager();
                            }

                            foreach (TcpClient client in server.Clients)
                            {
                                if (client == e.Client)
                                    SendStatusMessage(client, "You successfully saved the Device document", TimeSpan.FromSeconds(5));
                                else
                                {
                                    SendStatusMessage(client, string.Format("{0} successfully saved the Device document", senderUser != null ? senderUser.UserName : "Someone"), TimeSpan.FromSeconds(5));
                                }
                                
                                server.SendMessage(client, e.Message_Type, e.Message);                                
                            }
                        }
                        break;

                    case MessageType.RequestConditionsDoc:
                        server.SendMessage(e.Client, MessageType.ConditionsDoc, engine.AlarmConditionsDoc);
                        break;

                    case MessageType.ConditionsDoc:
                        if (e.Message is XMLDoc)
                        {
                            XMLDoc conditionsDoc = (XMLDoc)e.Message;

                            lock (engine.AlarmConditionsDoc)
                            {
                                string backupfilepath = Path.Combine(string.Format("{0}\\Ethele\\RioSoft\\{1}\\Config\\Backup\\AlarmConditions", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), Assembly.GetEntryAssembly().GetName().Name), string.Format("AlarmConditions_{0:yyyy_MM_dd_HH_mm_ss}.xml", DateTime.Now));
                                engine.AlarmConditionsDoc.Save(backupfilepath);

                                conditionsDoc.Save();

                                engine.AlarmConditionsDoc.Load();

                                engine.RestartConditionsEngine();
                            }

                            foreach (TcpClient client in server.Clients)
                            {
                                if (client == e.Client)
                                {
                                    SendStatusMessage(client, "You successfully saved the Conditions document", TimeSpan.FromSeconds(6));
                                    server.SendMessage(client, e.Message_Type, e.Message);
                                    //don't send the update to the client who made the update - he already knows
                                }
                                else
                                {
                                    SendStatusMessage(client, string.Format("{0} successfully saved the Conditions document", senderUser != null ? senderUser.UserName : "Someone"), TimeSpan.FromSeconds(6));
                                    server.SendMessage(client, e.Message_Type, e.Message);
                                }

                                server.SendMessage(client, MessageType.AlarmNames, engine.AlarmNames);
                                SendStatusMessage(client, "Alarm names updated", TimeSpan.FromSeconds(3));
                            }
                        }
                        break;

                    case MessageType.RequestActionsDoc:
                        server.SendMessage(e.Client, MessageType.ActionsDoc, engine.EventActions);
                        break;

                    case MessageType.ActionsDoc:
                        if (e.Message is XMLDoc)
                        {
                            XMLDoc actionsDoc = (XMLDoc)e.Message;

                            lock (engine.EventActions)
                            {
                                string backupfilepath = Path.Combine(string.Format("{0}\\Ethele\\RioSoft\\{1}\\Config\\Backup\\EventActions", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), Assembly.GetEntryAssembly().GetName().Name), string.Format("EventActions_{0:yyyy_MM_dd_HH_mm_ss}.xml", DateTime.Now));
                                engine.EventActions.Save(backupfilepath);

                                engine.EventActions.ClearNodes();

                                foreach (XmlNode node in actionsDoc)
                                {
                                    engine.EventActions.Add(ObjectCopier.Clone<XmlNode>(node));
                                }

                                engine.EventActions.ProcessData();
                                
                                engine.EventActions.Save();
                            }

                            foreach (TcpClient client in server.Clients)
                            {
                                if (client == e.Client)
                                {
                                    SendStatusMessage(client, "You successfully saved the Actions document", TimeSpan.FromSeconds(5));
                                    server.SendMessage(client, e.Message_Type, engine.EventActions);
                                    //don't send the update to the client who made the update - he already knows
                                }
                                else
                                {
                                    SendStatusMessage(client, string.Format("{0} successfully saved the Actions document", senderUser != null ? senderUser.UserName : "Someone"), TimeSpan.FromSeconds(5));
                                    
                                    server.SendMessage(client, e.Message_Type, engine.EventActions);
                                }
                            }
                        }
                        break;

                    case MessageType.RequestMapDoc:
                        server.SendMessage(e.Client, MessageType.MapDoc, engine.Map_Doc);
                        break;

                    case MessageType.MapDoc:
                        {
                            lock (engine.Map_Doc)
                            {
                                string backupfilepath = Path.Combine(string.Format("{0}\\Ethele\\RioSoft\\{1}\\Config\\Backup\\Maps", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), Assembly.GetEntryAssembly().GetName().Name), string.Format("Maps_{0:yyyy_MM_dd_HH_mm_ss}.xml", DateTime.Now));
                                engine.Map_Doc.Save(backupfilepath);

                                engine.Map_Doc = (MapDoc)e.Message;
                                engine.Map_Doc.Save();
                            }

                            foreach (TcpClient client in server.Clients)
                            {
                                if (client == e.Client)
                                {
                                    SendStatusMessage(client, "You successfully saved the Maps document", TimeSpan.FromSeconds(5));
                                }
                                else
                                {
                                    SendStatusMessage(client, string.Format("{0} successfully saved the Maps document", senderUser != null ? senderUser.UserName : "Someone"), TimeSpan.FromSeconds(5));

                                    //don't send the update to the client who made the update - he already knows
                                    server.SendMessage(client, e.Message_Type, e.Message);
                                }
                            }
                        }
                        break;

                    case MessageType.RequestAlarmSchedule:
                        server.SendMessage(e.Client, MessageType.AlarmSchedule, ScheduleDoc.TheeSchedule.Schedule);
                        break;
                    case MessageType.AlarmSchedule:
                        {
                            if (e.Message is List<Schedule>)
                            {
                                lock (ScheduleDoc.TheeSchedule.Schedule)
                                {
                                    string backupfilepath = Path.Combine(string.Format("{0}\\Ethele\\RioSoft\\{1}\\Config\\Backup\\Schedule", Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), Assembly.GetEntryAssembly().GetName().Name), string.Format("Schedule_{0:yyyy_MM_dd_HH_mm_ss}.xml", DateTime.Now));
                                    ScheduleDoc.TheeSchedule.Save(backupfilepath);
                
                                    ScheduleDoc.TheeSchedule.Schedule = (List<Schedule>)e.Message;
                                }

                                foreach (TcpClient client in server.Clients)
                                {
                                    if (client == e.Client)
                                        SendStatusMessage(client, "You successfully saved the Schedule document", TimeSpan.FromSeconds(5));
                                    else
                                        SendStatusMessage(client, string.Format("{0} successfully saved the Schedule document", senderUser != null ? senderUser.UserName : "Someone"), TimeSpan.FromSeconds(5));
                                }

                                server.SendMessage(MessageType.AlarmSchedule, ScheduleDoc.TheeSchedule.Schedule);
                            }
                        }
                        break;

                    case MessageType.RequestPublicDashboard:
                        {
                            string dashboardString = XmlNode.ParseString(Settings.TheeSettings["Dashboard"], "").Trim();
                            Dashboard dashboard = null;
                            if (!string.IsNullOrWhiteSpace(dashboardString))
                            {
                                dashboard = XMLSerializer.Deserialize(dashboardString) as Dashboard;
                                if (dashboard != null)
                                    dashboard.EventNames = engine.AlarmNames;
                            }

                            if (dashboard == null)
                                dashboard = new Dashboard() { Rows = 3, Columns = 4, EventNames = engine.AlarmNames, Gauges = new List<Gauge>() };

                            server.SendMessage(e.Client, MessageType.PublicDashboard, dashboard);
                        }
                        break;
                        
                    case MessageType.RequestPrivateDashboard:
                        {
                            string username = e.Message as string;
                            string dashboardString = "";
                            if (!string.IsNullOrWhiteSpace(username) && Settings.TheeSettings["users"] != null && Settings.TheeSettings["users"][username] != null)
                                dashboardString = XmlNode.ParseString(Settings.TheeSettings["users"][username]["Dashboard"], "").Trim();                                
                           
                            Dashboard dashboard = null;
                            if (!string.IsNullOrWhiteSpace(dashboardString))
                            {
                                dashboard = XMLSerializer.Deserialize(dashboardString) as Dashboard;
                                if (dashboard != null)
                                    dashboard.EventNames = engine.AlarmNames;
                            }

                            if(dashboard == null)
                                dashboard = new Dashboard() { User = username, Rows = 3, Columns = 4, EventNames = engine.AlarmNames, Gauges = new List<Gauge>() };

                            server.SendMessage(e.Client, MessageType.PrivateDashboard, dashboard);
                        }
                        break;

                    case MessageType.PublicDashboard:
                        {
                            if (Settings.TheeSettings["Dashboard"] == null)
                                Settings.TheeSettings.Add(new XmlNode() { Name = "DashBoard" });

                            if (e.Message != null)
                            {
                                Settings.TheeSettings["Dashboard"].Value = XMLSerializer.Serialize(e.Message);
                                Settings.TheeSettings.Save();
                            }

                            foreach (TcpClient client in server.Clients)
                            {
                                if (client == e.Client)
                                {
                                    SendStatusMessage(client, "You successfully saved the Public Dashboard", TimeSpan.FromSeconds(5));
                                }
                                else
                                {
                                    SendStatusMessage(client, string.Format("{0} successfully saved the Public Dashboard", senderUser != null ? senderUser.UserName : "Someone"), TimeSpan.FromSeconds(5));

                                    //don't send the update to the client who made the update - he already knows
                                    server.SendMessage(client, e.Message_Type, e.Message);
                                }
                            }
                        }
                        break;

                    case MessageType.PrivateDashboard:
                        {
                            Dashboard dashboard = e.Message as Dashboard;
                            if (dashboard != null && !string.IsNullOrWhiteSpace(dashboard.User) && Settings.TheeSettings["users"] != null && Settings.TheeSettings["users"][dashboard.User] != null)
                            {
                                if(Settings.TheeSettings["users"][dashboard.User]["DashBoard"] == null)
                                    Settings.TheeSettings["users"][dashboard.User].Add(new XmlNode() { Name = "DashBoard" });

                                Settings.TheeSettings["users"][dashboard.User]["Dashboard"].Value = XMLSerializer.Serialize(e.Message);
                                Settings.TheeSettings.Save();

                                SendStatusMessage(e.Client, "You successfully saved your Private Dashboard", TimeSpan.FromSeconds(5));
                            }
                        }
                        break;
                    #endregion Config docs related messages

                    #region Alarm related messages
                    case MessageType.GroupAlarmResolve:
                        AlarmResolve groupAlarmResolve = (AlarmResolve)e.Message;
                        engine.ResolveAlarm(groupAlarmResolve);
                        break;

                    case MessageType.AlarmResolve:
                        AlarmResolve alarmResolve = (AlarmResolve)e.Message;
                        engine.ResolveAlarm(alarmResolve);
                        break;

                    case MessageType.RequestUnresolvedAlarms:
                        {
                            StatusMessage statusMsg = SendStatusMessage(e.Client, "Loading unresolved alarms");

                            List<GroupAlarm> unresolvedAlarms = ObjectCopier.CloneXML<List<GroupAlarm>>(engine.UnresolvedAlarms);
                            foreach (GroupAlarm groupAlarm in unresolvedAlarms)
                                groupAlarm.Images.Clear();

                            server.SendMessage(e.Client, MessageType.UnresolvedAlarms, unresolvedAlarms);

                            foreach (GroupAlarm groupAlarm in engine.UnresolvedAlarms)
                            {
                                foreach (EventImage img in groupAlarm.Images)
                                    Service.SendMessage(MessageType.AlarmImage, img);
                            }

                            RemoveStatusMessage(e.Client, statusMsg);
                        }
                        break;

                    case MessageType.RequestAlarmHistory:
                        if (engine.DB != null && e.Message is AlarmHistoryFilter)
                        {
                            StatusMessage statusMsg = SendStatusMessage(e.Client, "Loading alarm history from DB");

                            AlarmHistoryFilter alarmHistoryFilter = (AlarmHistoryFilter)e.Message;
                            List<Alarm> alarms = engine.GetAlarmHistory(alarmHistoryFilter);
                            if (alarms != null)
                            {
                                List<Alarm> alarmsClone = ObjectCopier.CloneXML<List<Alarm>>(alarms);
                                foreach (Alarm alarm in alarmsClone)
                                    alarm.Images.Clear();
                                server.SendMessage(e.Client, MessageType.AlarmHistory, alarmsClone);
                            }

                            foreach (Alarm alarm in alarms)
                            {
                                foreach (EventImage img in alarm.Images)
                                {
                                    Service.SendMessage(MessageType.AlarmImage, img);
                                }
                            }

                            RemoveStatusMessage(e.Client, statusMsg);
                        }
                        break;

                    case MessageType.RequestAlarmNames:
                        server.SendMessage(e.Client, MessageType.AlarmNames, engine.AlarmNames);
                        break;

                    case MessageType.AlarmComment:
                        {
                            AlarmComment comment = e.Message as AlarmComment;

                            if (comment != null)
                            {
                                engine.AlarmComment(comment);

                                Service.SendMessage(MessageType.AlarmComment, comment);
                            }
                        }
                        break;
                    case MessageType.Alarm:
                        {
                            if (e.Message is Alarm alarm)
                            {
                                engine.InjectAlarm(alarm);
                            }
                        }
                        break;
                    #endregion Alarm related messages

                    #region Device related messages
                    case MessageType.RequestDevicesOnline:
                        server.SendMessage(e.Client, MessageType.OnlineDeviceList, engine.OnlineDevices);
                        break;

                    case MessageType.SwitchRelay:
                        RelaySwitchMessage relaySwitchMsg = (RelaySwitchMessage)e.Message;
                        engine.SwitchRelay(relaySwitchMsg.DeviceName, relaySwitchMsg.RelayName, relaySwitchMsg.State, relaySwitchMsg.Duration);
                        break;

                    case MessageType.CameraSnapshot:
                        {
                            StatusMessage statusMsg = SendStatusMessage(e.Client, "Loading snapshot from camera");

                            CameraImage snapshot = (CameraImage)e.Message;
                            snapshot.ImageData = engine.CameraSnapshot(snapshot.CameraAddress, snapshot.SnapshotCommand, snapshot.UserName, snapshot.Password);
                            server.SendMessage(e.Client, MessageType.CameraSnapshot, snapshot);

                            RemoveStatusMessage(e.Client, statusMsg);
                        }
                        break;

                    case MessageType.PluginTypes:
                        server.SendMessage(e.Client, MessageType.PluginTypes, PluginManager.GetPluginTypes());
                        break;
                    #endregion Device related messages

                    #region OB Book and Site Instructions
                    case MessageType.SiteInstructionStep:
                        SiteInstructionStep siteInstructionStep = (SiteInstructionStep)e.Message;
                        engine.SiteInstructionStep(siteInstructionStep);
                        break;

                    case MessageType.OccurenceBook:
                        engine.AddOccurenceBookEntry((OBEntry)e.Message);
                        server.SendMessage(MessageType.OccurenceBook, e.Message); //pass it on to everyone.
                        break;

                    case MessageType.RequestOccurrenceBookHistory:
                        {
                            StatusMessage statusMsg = SendStatusMessage(e.Client, "Loading Occurence Book history from DB");

                            OccurrenceBookHistoryFilter filter = (OccurrenceBookHistoryFilter)e.Message;
                            List<OccurrenceBook> obItems = engine.DB.GetOccurrenceBookHistory(filter.StartDate, filter.EndDate, filter.TypeDescription, filter.Description, filter.UserName);
                            if (obItems != null)
                                server.SendMessage(e.Client, MessageType.OccurrenceBookHistory, obItems);

                            RemoveStatusMessage(e.Client, statusMsg);
                        }
                        break;

                    case MessageType.RequestOBTypes:
                        List<OBType> obTypes = engine.GetOBTypes();

                        if (obTypes != null)
                            server.SendMessage(e.Client, MessageType.OBTypes, obTypes);
                        break;
                    #endregion OB Book and Site Instructions

                    #region DB related messages
                    case MessageType.RequestDBConnectionState:
                        server.SendMessage(e.Client, MessageType.DatabaseConnectionState, engine.DBConnectionState);
                        break;

                    case MessageType.RequestDBSettings:
                        if ((engine.dbDetails.Server != null) && (engine.dbDetails.Server.Length > 0))
                            //DBdetails exists - send to client
                            server.SendMessage(e.Client, MessageType.SendDBSettings, engine.dbDetails);
                        else
                        {
                            //DBdetails didn't load - load and then send
                            engine.dbDetails.Load();
                            server.SendMessage(e.Client, MessageType.SendDBSettings, engine.dbDetails);
                        }
                        break;

                    case MessageType.ReportAddress:
                        if (e.Message == null) // Client is requesting address.
                        {
                            server.SendMessage(e.Client, MessageType.ReportAddress, engine.dbDetails.ReportServer);
                        }
                        else
                        {
                            string newAddresss = e.Message as string;
                            if (newAddresss != null && !string.IsNullOrEmpty(newAddresss))
                            {
                                engine.dbDetails.Save(newAddresss);
                            }
                        }
                        break;
                    case MessageType.ReportList:
                        server.SendMessage(e.Client, MessageType.ReportList, engine.Reports);
                        break;

                    case MessageType.TestDBSettings:
                        SqlConnection testDb = null;
                        ConnectionDetails testSettings = (ConnectionDetails)e.Message;
                        try
                        {
                            testDb = new SqlConnection(ConnectionDetails.ConnectionString(testSettings));
                            testDb.Open();
                            testSettings.Valid = true;
                            server.SendMessage(e.Client, MessageType.DBTestResult, testSettings);
                        }
                        catch (SqlException SqlEx)
                        {
                            testSettings.Valid = false;
                            if (SqlEx.Message.Contains("instance"))
                                testSettings.FailureReason = "Server not found.";
                            else if (SqlEx.Message.Contains("Cannot open"))
                                testSettings.FailureReason = "Invalid database.";
                            else if (SqlEx.Message.Contains("Login failed"))
                                testSettings.FailureReason = "Invalid login.";
                            server.SendMessage(e.Client, MessageType.DBTestResult, testSettings);
                        }
                        finally
                        {
                            if (testDb != null) testDb.Dispose();
                        }
                        break;

                    case MessageType.SendDBSettings:
                        // New settings tested and submitted, or request for report settings.
                        if (e.Message == null)
                        {// Reports

                            if ((engine.dbDetails.Server != null) && (engine.dbDetails.Server.Length > 0))
                                server.SendMessage(e.Client, MessageType.RequestDBSettings, engine.dbDetails);
                            else
                            {
                                //DBdetails didn't load - load and then send
                                engine.dbDetails.Load();
                                server.SendMessage(e.Client, MessageType.RequestDBSettings, engine.dbDetails);
                            }
                        }
                        else
                        {
                            engine.dbDetails.Save((ConnectionDetails)e.Message);
                            if (engine.DB.ConnectionOpen)
                                engine.DB.Disconnect();
                            engine.UnresolvedAlarms.Clear();
                            server.SendMessage(MessageType.ForceLogoff, null);
                            engine.DB.ConnectionString = engine.dbDetails.ConnectionString();
                            engine.DB.Connect();
                        }

                        break;
                    #endregion DB related messages

                    #region User related messages
                    case MessageType.UserLogin:
                        {
                            User loginUser = (User)e.Message;
                            loginUser.Client = e.Client;

                            string msg = "";
                            bool loginResult = engine.Login(loginUser, out msg);

                            server.SendMessage(e.Client, MessageType.UserLoginResult, new LoginResult(loginUser, msg));
                            if (loginResult)
                            {
                                server.SendMessage(MessageType.UserLoggedOn, loginUser);
                                List<VideoHostConfiguration> videoConfigurations = engine.GetVideoConfigurations(loginUser);
                                if (videoConfigurations != null)
                                {
                                    foreach (VideoHostConfiguration layout in videoConfigurations)
                                    {
                                        server.SendMessage(e.Client, MessageType.CameraLayouts, layout);
                                    }
                                }
                            }
                            break;
                        }

                    case MessageType.UserLogout:
                        {
                            User logoutUser = (User)e.Message;
                            if (logoutUser != null)
                            {
                                logoutUser.Client = e.Client;
                                engine.Logout(logoutUser);
                                server.SendMessage(MessageType.UserLoggedOff, logoutUser);
                            }
                            break;
                        }

                    case MessageType.RequestUsersOnline: // Send a list of currently logged in users.
                        server.SendMessage(e.Client, MessageType.UsersOnline, engine.Users.UserList);
                        break;

                    case MessageType.RequestUserList:
                        // Request to obtain all valid users in the system
                        server.SendMessage(e.Client, MessageType.UserList, engine.GetUsers());
                        break;

                    case MessageType.RequestUserRights: // Could possibly be phased out as user rights are included in user object.
                        User user = e.Message as User;
                        if (user != null)
                        {
                            user.Rights = user.GetRights();
                            server.SendMessage(e.Client, MessageType.UserRights, user);
                        }
                        break;

                    case MessageType.ChangePassword:
                        {
                            User modifiedUser = e.Message as User;

                            if (modifiedUser != null)
                            {
                                modifiedUser.Client = e.Client;
                                //if (user.Authenticated)
                                {// Logged in already, just update
                                    if (engine.Users.Update(modifiedUser))
                                    {// Successful update - notify
                                        server.SendMessage(e.Client, MessageType.UserRights, modifiedUser);
                                        server.SendMessage(e.Client, MessageType.ChangePassword, "Successfully updated user.");
                                    }
                                    else
                                    {// Sum ting wong - send message
                                        server.SendMessage(e.Client, MessageType.ChangePassword, "Failure - could not update user.");
                                    }
                                }
                                /*else
                                {// Not logged in - only update if login successful
                                    //bool loginResult = user.Verify(user, out msg);
                                    //if (loginResult)
                                    {
                                        engine.Users.Update(user);
                                    }
                                    //server.SendMessage(_client, MessageType.UserLoginResult, new LoginResult(loginResult, msg));
                                    //server.SendMessage(MessageType.UserLoggedOn, user);
                                } */

                            }
                        }
                        break;

                    case MessageType.DeleteUser:
                        {
                            User deletingUser = e.Message as User;
                            if (deletingUser != null)
                            {
                                string result = engine.Users.Delete(deletingUser);
                                server.SendMessage(e.Client, MessageType.DeleteUser, new LoginResult(deletingUser, result));

                                if (result.Contains("deleted"))
                                {
                                    engine.Logout(deletingUser);
                                    
                                    server.SendMessage(MessageType.UserLoggedOff, deletingUser);                                    
                                }
                            }
                        }
                        break;

                    case MessageType.CreateUser:
                        {
                            User newUser = e.Message as User;
                            if (newUser != null)
                            {// Return the result: True = success, false = failure.
                                server.SendMessage(MessageType.CreateUser, engine.Users.Update(newUser));
                            }
                        }
                        break;
                    #endregion User related messages

                    #region CameraLayouts

                    case MessageType.CameraLayouts:
                        if (e.Message == null)
                        {// Null means request for camera servers.
                            server.SendMessage(e.Client, MessageType.CameraLayouts, engine.CameraServers());
                        }
                        else
                        {// If user is specified, it is a request for cameras that this user is allowed to see.
                            var cameraUser = e.Message as User;
                            if (cameraUser != null)
                            {
                                cameraUser.Rights = cameraUser.GetRights();
                                server.SendMessage(e.Client, MessageType.CameraLayouts, engine.CameraList(cameraUser));
                            }
                            else
                            {// If it's a config, save the config to file.
                                if (e.Message is VideoHostConfiguration config)
                                {
                                    try
                                    {
                                        XmlNode usersNode = Settings.TheeSettings["users"];
                                        if (usersNode != null)
                                        {
                                            if (senderUser == null)
                                                senderUser = config.User;
                                            XmlNode userNode = usersNode[senderUser.UserName];
                                            if (userNode != null)
                                            {// Don't add user node, as the video shouldn't have such rights.
                                                XmlNode videoNode = userNode["VideoSettings"];
                                                if (videoNode == null)
                                                {//create video node
                                                    videoNode = userNode.Add("VideoSettings", "");
                                                }
                                                XmlNode screenNode = videoNode[config.Name];
                                                if (screenNode == null)
                                                {
                                                    screenNode = videoNode.Add(config.Name, "");
                                                    screenNode.AddAttribute("type", ((int)config.CameraLayout).ToString());
                                                }
                                                else if (screenNode.Attribute("type") != null)
                                                {
                                                    screenNode.Attribute("type").Value = ((int)config.CameraLayout).ToString();
                                                }
                                                else
                                                {
                                                    screenNode.AddAttribute("type", ((int)config.CameraLayout).ToString());
                                                }

                                                if (config.CameraPositions != null)
                                                {
                                                    screenNode.ClearNodes();
                                                    foreach (KeyValuePair<string, string> item in config.CameraPositions)
                                                    {
                                                        var cameraNode = screenNode[item.Key];
                                                        if (cameraNode == null)
                                                        {
                                                            cameraNode = screenNode.Add(item.Key, "");
                                                            cameraNode.AddAttribute("camera", item.Value);
                                                        }
                                                        else
                                                        {
                                                            cameraNode.ClearAttributes();
                                                            cameraNode.AddAttribute("camera", item.Value);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        Settings.TheeSettings.Save();
                                    }
                                    catch (Exception ex)
                                    {
                                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                                                           MethodBase.GetCurrentMethod().Name), "Error parsing video config from XML", ex);
                                    }
                                }
                            }
                        }
                        break; 
                    case MessageType.DeleteCameraLayout:
                        XmlNode usersNodus = Settings.TheeSettings["users"];
                        if ((usersNodus != null) && (e.Message != null))
                        {
                            XmlNode userNode = usersNodus[senderUser.UserName];
                            if (userNode != null)
                            {// Don't add the user node, as there will be nothing to delete if you do.
                                XmlNode videoNode = userNode["VideoSettings"];
                                if (videoNode == null)
                                    break;
                                else
                                {
                                    try
                                    {
                                        XmlNode layoutNode = videoNode[e.Message as string];
                                        if (layoutNode != null)
                                        {
                                            videoNode.RemoveChildNode(layoutNode);
                                            Settings.TheeSettings.Save();
                                        }
                                    }
                                    catch { } // Not a serious issue if you can't read the video configuration you're attempting to delete.
                                }
                            }
                        }
                        break;
                    case MessageType.RemoveCamera:
                        XmlNode usersNodule = Settings.TheeSettings["users"];
                        if ((usersNodule != null) && (e.Message != null))
                        {
                            var cameraPosition = e.Message as CameraPosition;
                            if (cameraPosition != null)
                            {
                                XmlNode userNode = usersNodule[cameraPosition.UserName];
                                if (userNode != null)
                                {
                                    XmlNode videoNode = userNode["VideoSettings"];
                                    if (videoNode == null)
                                        break; //Nothing to delete.
                                    else
                                    {
                                        try
                                        {
                                            XmlNode layoutNode = videoNode[cameraPosition.CameraWindowName];
                                            if (layoutNode != null)
                                            {
                                                XmlNode positionNode = layoutNode[cameraPosition.PositionName];
                                                if (positionNode != null)
                                                {
                                                    layoutNode.RemoveChildNode(positionNode);
                                                    Settings.TheeSettings.Save();
                                                }
                                            }
                                        }
                                        catch { } // Not a serious issue if you can't read the camera position you're attempting to delete.
                                    }
                                }
                            }
                        }
                        break;

                    #endregion Camera Layouts

                    #region VideoServer

                    case MessageType.VideoServerOnline:
                        {
                            if (e.Message is IPEndPoint videoServer)
                            {
                                engine.AddVideoServer(videoServer);
                                server.SendMessage(e.Client, MessageType.VideoCamerasRequest);
                            }
                        }
                        break;
                    case MessageType.VideoCameraAdded:
                        {
                            if (e.Message is CameraPlugin camera)
                            {
                                IPAddress address = ((IPEndPoint)e.Client.Client.RemoteEndPoint).Address;
                                if (address.ToString() == "127.0.0.1")
                                {
                                    address = NetworkTools.MyIPAddress();
                                }
                                engine.AddVideoCamera(camera, address);
                                server.SendMessage(MessageType.VideoCameraAdded);
                            }
                        }
                        break;

                    #endregion VideoServer

                    #region Root cause
                    case MessageType.RequestRootCauseServerAddress:
                        server.SendMessage(MessageType.RootCauseServerAddress, rootCauseDetails);
                        break;

                    case MessageType.RootCauseServerAddress:
                        IPEndPoint rootCauseAddress = e.Message as IPEndPoint;

                        if (rootCauseAddress != null)
                        {
                            ConnectRootCause(rootCauseAddress);

                            Settings.TheeSettings.AddAttribute(new XmlItem() { Name = "IP", Value = rootCauseAddress.Address.ToString() }, "RootCauseServer");
                            Settings.TheeSettings.AddAttribute(new XmlItem() { Name = "Port", Value = rootCauseAddress.Port.ToString() }, "RootCauseServer");
                            Settings.TheeSettings.Save();
                        }

                        break;

                    case MessageType.RequestRootCauseConditionsDoc:
                        if(rootCauseClient != null)
                            rootCauseClient.SendMessage(MessageType.RequestRootCauseConditionsDoc);
                        break;
                    #endregion

                    case MessageType.RespondMessageTypeNotSupported:
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Receiver does not support message type", null);
                        break;

                    default:
                        server.SendMessage(e.Client, MessageType.RespondMessageTypeNotSupported);
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "Unknown message type received: " + e.Message_Type, null);
                        break;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        void Server_OnStringMessageReceived(object sender, CommsNode.StringMessageEventArgs e)
        {
            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "String messages not supported");            
        }

        void Server_OnBinaryMessageReceived(object sender, CommsNode.BinaryMessageEventArgs e)
        {
            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Binary messages not supported");
        }
        #endregion Server event handlers
    }
}
