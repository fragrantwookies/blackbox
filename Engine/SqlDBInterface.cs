using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using eNervePluginInterface;
using eThele.Essentials;
using eNerve.DataTypes;
using System.Collections.ObjectModel;

namespace BlackBoxEngine
{
    public class SqlDBInterface : DBInterface
    {
        protected SqlConnection loggingCon = null; //use this connection for logging alarms.

        public SqlDBInterface(string _connectionString)
            : base()
        {
            ConnectionString = _connectionString;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (loggingCon != null)
            {
                loggingCon.Close();
                if (stateChangeEventHandler != null)
                    loggingCon.StateChange -= stateChangeEventHandler;
                loggingCon.Dispose();
                loggingCon = null;
            }
        }

        #region Connect and Disconnect
        public override bool Connect()
        {
            bool result = base.Connect();

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    if (loggingCon != null)
                        loggingCon.Close();

                    if (loggingCon == null)
                    {
                        loggingCon = new SqlConnection(ConnectionString);

                        loggingCon.StateChange += stateChangeEventHandler;
                    }
                    else
                        loggingCon.ConnectionString = ConnectionString;

                    loggingCon.Open();

                    result = loggingCon.State == System.Data.ConnectionState.Open;
                }
                else
                    result = false;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                result = false;
            }

            return result;
        }

        public override async Task<bool> ConnectAsync()
        {
            bool result = await base.ConnectAsync();

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    if (loggingCon != null)
                        loggingCon.Close();

                    if (loggingCon == null)
                    {
                        loggingCon = new SqlConnection(ConnectionString);
                        loggingCon.StateChange += stateChangeEventHandler;
                    }
                    else
                        loggingCon.ConnectionString = ConnectionString;

                    await loggingCon.OpenAsync();

                    result = loggingCon.State == System.Data.ConnectionState.Open;
                }
                else
                    result = false;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                result = false;
            }

            return result;
        }

        public override System.Data.ConnectionState ConnectionState
        {
            get
            {
                return loggingCon != null ? loggingCon.State : System.Data.ConnectionState.Closed;
            }
        }

        public override void Disconnect()
        {
            try
            {
                base.Disconnect();

                if (loggingCon != null)
                {
                    loggingCon.Close();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Connect and Disconnect

        #region Insert Alarm
        protected override bool InsertAlarm(Alarm _alarm)
        {
            bool result = false;

            SqlTransaction transaction = null;
            SqlCommand cmd = null;

            try
            {
                if (loggingCon != null && _alarm != null)
                {
                    if (ConnectionOpen)
                    {
                        transaction = loggingCon.BeginTransaction();

                        cmd = loggingCon.CreateCommand();
                        cmd.Transaction = transaction;

                        cmd.CommandText = string.Format(@"INSERT INTO [Alarms] ([AlarmID], [AlarmGroupID], [AlarmName], [AlarmDT], [AlarmType], [EscalationLevel], [Priority], [AlarmNumber], [ServerID], [RootCause])
                                        SELECT '{0}', '{1}', '{2}', '{3}', {4}, {5}, {6}, {7}, '{8}', {9}",
                                            _alarm.ID, _alarm.GroupAlarmID, _alarm.Name, _alarm.AlarmDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), (byte)_alarm.Alarm_Type, (byte)_alarm.EscalationLevel, _alarm.Priority, _alarm.AlarmNumber, _alarm.ServerID, (_alarm.RootCause ? 1 : 0));

                        cmd.ExecuteNonQuery();

                        #region Triggers
                        if (_alarm.Triggers != null)
                        {
                            if (_alarm.Triggers.Count > 0)
                            {
                                foreach (IEventTrigger trigger in _alarm.Triggers)
                                {
                                    InsertTrigger(trigger, cmd);

                                    //string insertTrigger = "";
                                    //string insertTriggerEventValues = "";

                                    //if (insertTrigger == "")
                                    //    insertTrigger = "INSERT INTO [AlarmTriggers] ([TriggerID], [AlarmID], [DetectorName], [DeviceName], [DetectorType], [Description], [Location], [MapName], [TriggerDT], [TriggerState], [TriggerValue], [CustomData])";
                                    //else
                                    //    insertTrigger += " UNION ALL ";

                                    //insertTrigger += string.Format(" SELECT '{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', @CustomData",
                                    //                trigger.TriggerID, _alarm.ID, trigger.DetectorName, trigger.DeviceName, (byte)trigger.DetectorType, trigger.EventName, trigger.Location, trigger.MapName,
                                    //                trigger.TriggerDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), (trigger.State ? 1 : 0), trigger.EventValue);

                                    ////if (trigger.EventValues != null)
                                    ////    if (trigger.EventValues.Length > 0)
                                    ////    {
                                    ////        foreach (float eventValue in trigger.EventValues)
                                    ////        {
                                    ////            if (insertTriggerEventValues == "")
                                    ////                insertTriggerEventValues = "INSERT INTO [AlarmTriggerEventValues] ([EventValueID], [AlarmTriggerID], [EventValue])";
                                    ////            else
                                    ////                insertTriggerEventValues += " UNION ALL ";

                                    ////            insertTriggerEventValues += string.Format(" SELECT '{0}', '{1}', {2}",
                                    ////                                        Guid.NewGuid(), trigger.TriggerID, eventValue);
                                    ////        }
                                    ////    }

                                    //if (!string.IsNullOrWhiteSpace(insertTrigger))
                                    //{
                                    //    cmd.CommandText = insertTrigger;

                                    //    if (!cmd.Parameters.Contains("@CustomData"))
                                    //        cmd.Parameters.Add("@CustomData", System.Data.SqlDbType.NVarChar, trigger.Data.Length).Value = trigger.Data;
                                    //    else
                                    //        cmd.Parameters["@CustomData"].Value = trigger.Data;

                                    //    cmd.ExecuteNonQuery();
                                    //}

                                    //if (!string.IsNullOrWhiteSpace(insertTriggerEventValues))
                                    //{
                                    //    cmd.CommandText = insertTriggerEventValues;
                                    //    cmd.ExecuteNonQuery();
                                    //}
                                }

                                cmd.Parameters.Clear();
                            }
                        }
                        #endregion Triggers

                        #region SiteInstructionSteps
                        try
                        {
                            Collection<SiteInstructionStep> steps = _alarm.GetSiteInstructionItems;
                            if (steps != null)
                            {
                                if (steps.Count > 0)
                                {
                                    string insertSiteInstructionSteps = "";

                                    foreach (SiteInstructionStep step in steps)
                                    {
                                        if (insertSiteInstructionSteps == "")
                                            insertSiteInstructionSteps = "INSERT INTO [SiteInstructions] ([InstructionID],[AlarmID],[InstructionName],[InstructionValue],[InstructionOrder],[Completed],[CompletedByUserName],[CompleteToResolve],[CompletedDT])";
                                        else
                                            insertSiteInstructionSteps += " UNION ALL ";

                                        insertSiteInstructionSteps += string.Format(" SELECT '{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', {8}",
                                            step.SiteInstructionStepID, step.AlarmID, step.Name, step.Value, step.Order, (step.Completed ? 1 : 0).ToString(), (step.User != null ? step.User.UserName : ""), (step.CompleteToResolve ? 1 : 0).ToString(), step.CompletedDT > DateTime.Parse("1753-01-01") ? string.Format("'{0}'", step.CompletedDT.ToString("yyyy-MM-dd HH:mm:ss.fff")) : "NULL");
                                    }

                                    if (!string.IsNullOrWhiteSpace(insertSiteInstructionSteps))
                                    {
                                        cmd.CommandText = insertSiteInstructionSteps;
                                        cmd.ExecuteNonQuery();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                        }
                        #endregion SiteInstructionSteps

                        #region Images
                        if (_alarm.Images != null)
                        {
                            if (_alarm.Images.Count > 0)
                            {
                                string insertImg = "";

                                foreach (EventImage img in _alarm.Images)
                                {
                                    insertImg = "INSERT INTO [BlackBox].[dbo].[AlarmImages] ([AlarmImageID], [AlarmID], [AlarmImageDT], [CameraAddress], [CameraID], [Dewarp], [ImageData])";
                                    insertImg += string.Format(" SELECT '{0}', '{1}', '{2}', '{3}', '{4}', {5}, @ImageData",
                                        img.ID, _alarm.ID, img.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss.fff"), img.CameraAddress, img.CameraID, (img.Dewarp ? 1 : 0).ToString());

                                    cmd.CommandText = insertImg;
                                    if (!cmd.Parameters.Contains("@ImageData"))
                                        cmd.Parameters.Add("@ImageData", System.Data.SqlDbType.VarBinary, img.ImageData.Length);
                                    cmd.Parameters["@ImageData"].Value = img.ImageData;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }
                        #endregion Images
                    }
                }

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (transaction != null)
                {
                    if (result)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    transaction.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();
            }

            return result;
        }

        protected override bool InsertTrigger(IEventTrigger _trigger)
        {
            bool result = true;

            SqlTransaction transaction = null;
            SqlCommand cmd = null;

            try
            {
                if (loggingCon != null && _trigger != null)
                {
                    if (ConnectionOpen)
                    {
                        transaction = loggingCon.BeginTransaction();

                        cmd = loggingCon.CreateCommand();
                        cmd.Transaction = transaction;

                        result = InsertTrigger(_trigger, cmd);
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (transaction != null)
                {
                    if (result)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    transaction.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();
            }
            return result;
        }

        private bool InsertTrigger(IEventTrigger _trigger, SqlCommand _cmd)
        {
            string insertTrigger = "";
            string insertTriggerEventValues = "";

            if (insertTrigger == "")
                insertTrigger = "INSERT INTO [AlarmTriggers] ([TriggerID], [AlarmID], [DetectorName], [DeviceName], [DetectorType], [Description], [Location], [MapName], [TriggerDT], [TriggerState], [TriggerValue], [CustomData])";
            else
                insertTrigger += " UNION ALL ";

            insertTrigger += string.Format(" SELECT '{0}', '{1}', '{2}', '{3}', {4}, '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', @CustomData",
                            _trigger.TriggerID, _trigger.EventID, _trigger.DetectorName, _trigger.DeviceName, (byte)_trigger.DetectorType, _trigger.EventName, _trigger.Location, _trigger.MapName,
                            _trigger.TriggerDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), (_trigger.State ? 1 : 0), _trigger.EventValue);

            if (!string.IsNullOrWhiteSpace(insertTrigger))
            {
                _cmd.CommandText = insertTrigger;

                if (!_cmd.Parameters.Contains("@CustomData"))
                    _cmd.Parameters.Add("@CustomData", System.Data.SqlDbType.NVarChar, _trigger.Data.Length).Value = _trigger.Data;
                else
                    _cmd.Parameters["@CustomData"].Value = _trigger.Data;

                _cmd.ExecuteNonQuery();
            }

            if (!string.IsNullOrWhiteSpace(insertTriggerEventValues))
            {
                _cmd.CommandText = insertTriggerEventValues;
                _cmd.ExecuteNonQuery();
            }          

            return true;
        }

        protected override bool InsertAlarmImage(EventImage _alarmImage)
        {
            bool result = false;

            SqlTransaction transaction = null;
            SqlCommand cmd = null;

            try
            {
                if (loggingCon != null && _alarmImage != null)
                {
                    if (ConnectionOpen)
                    {
                        transaction = loggingCon.BeginTransaction();

                        cmd = loggingCon.CreateCommand();
                        cmd.Transaction = transaction;

                        string insertImg = "INSERT INTO [BlackBox].[dbo].[AlarmImages] ([AlarmImageID], [AlarmID], [AlarmImageDT], [CameraAddress], [CameraID], [Dewarp], [ImageData])";
                        insertImg += string.Format(" SELECT '{0}', '{1}', '{2}', '{3}', '{4}', {5}, @ImageData",
                            _alarmImage.ID, _alarmImage.EventID, _alarmImage.TimeStamp.ToString("yyyy-MM-dd HH:mm:ss.fff"), _alarmImage.CameraAddress, _alarmImage.CameraID, (_alarmImage.Dewarp ? 1 : 0).ToString());

                        cmd.CommandText = insertImg;
                        if (!cmd.Parameters.Contains("@ImageData"))
                            cmd.Parameters.Add("@ImageData", System.Data.SqlDbType.VarBinary, _alarmImage.ImageData.Length);
                        cmd.Parameters["@ImageData"].Value = _alarmImage.ImageData;
                        cmd.ExecuteNonQuery();
                    }
                }

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (transaction != null)
                {
                    if (result)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    transaction.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();
            }

            return result;
        }
        #endregion Insert Alarm

        #region Update Alarm
        protected override bool UpdateAlarmProperty(PropertyUpdate _alarmUpdate)
        {
            bool result = false;

            SqlTransaction transaction = null;
            SqlCommand cmd = null;

            try
            {
                if (loggingCon != null && _alarmUpdate != null)
                {
                    if (ConnectionOpen)
                    {
                        transaction = loggingCon.BeginTransaction();

                        cmd = loggingCon.CreateCommand();
                        cmd.Transaction = transaction;

                        string alarmIDFieldName = _alarmUpdate is GroupAlarmPropertyUpdate ? "AlarmGroupID" : "AlarmID";

                        cmd.CommandText = string.Format("UPDATE Alarms SET {0} = '{1}' WHERE {2} = '{3}'", _alarmUpdate.PropertyName, _alarmUpdate.Value is Enum ? ((Enum)_alarmUpdate.Value).GetAsUnderlyingType() : _alarmUpdate.Value, alarmIDFieldName, _alarmUpdate.Id);

                        cmd.ExecuteNonQuery();
                    }
                }

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (transaction != null)
                {
                    if (result)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    transaction.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();
            }

            return result;
        }

        protected override bool ResolveAlarm(ResolveDetailItem _resolveDetailItem)
        {
            bool result = false;

            SqlTransaction transaction = null;
            SqlCommand cmd = null;

            try
            {
                if (loggingCon != null && _resolveDetailItem != null)
                {
                    if (ConnectionOpen)
                    {
                        transaction = loggingCon.BeginTransaction();

                        cmd = loggingCon.CreateCommand();
                        cmd.Transaction = transaction;

                        cmd.CommandText = string.Format("INSERT INTO AlarmResolveItems (ResolveItemID, AlarmID, ResolveDT, AlarmType, Description, UserName) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}')",
                            _resolveDetailItem.ResolveID, _resolveDetailItem.AlarmID, _resolveDetailItem.ResolveDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), (byte)_resolveDetailItem.Alarm_Type, _resolveDetailItem.Description, _resolveDetailItem.User.UserName);

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = string.Format("UPDATE Alarms SET AlarmType = '{0}' WHERE AlarmID = '{1}'", (byte)_resolveDetailItem.Alarm_Type, _resolveDetailItem.AlarmID);

                        cmd.ExecuteNonQuery();
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (transaction != null)
                {
                    if (result)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    transaction.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();
            }

            return result;
        }

        protected override bool ResolveAlarm(AlarmResolve _alarmResolve)
        {
            bool result = false;

            SqlTransaction transaction = null;
            SqlCommand cmd = null;

            try
            {
                if (loggingCon != null && _alarmResolve != null)
                {
                    if (ConnectionOpen)
                    {
                        transaction = loggingCon.BeginTransaction();

                        cmd = loggingCon.CreateCommand();
                        cmd.Transaction = transaction;

                        cmd.CommandText = string.Format("INSERT INTO AlarmResolveItems (ResolveItemID, AlarmID, ResolveDT, AlarmType, [Description], UserName) SELECT '{0}', '{1}', '{2}', '{3}', '{4}', '{5}'",
                            _alarmResolve.ResolveID.ToString(), _alarmResolve.AlarmID.ToString(), _alarmResolve.ResolveDT.ToString("yyyy-MM-dd HH:mm:ss.fff"),
                            (byte)_alarmResolve.Alarm_Type, _alarmResolve.Description, _alarmResolve.User != null ? _alarmResolve.User.UserName : "System");

                        cmd.ExecuteNonQuery();

                        //cmd.CommandText = string.Format("UPDATE Alarms SET AlarmType = '{0}' WHERE {1} = '{2}'", (byte)_alarmResolve.Alarm_Type, _alarmResolve.GroupResolve ? "AlarmGroupID" : "AlarmID", _alarmResolve.AlarmID.ToString());
                        cmd.CommandText = string.Format("UPDATE Alarms SET AlarmType = '{0}' WHERE {1}", (byte)_alarmResolve.Alarm_Type, _alarmResolve.GroupResolve ? string.Format("AlarmName = (SELECT TOP 1 AlarmName FROM Alarms WHERE AlarmGroupID = '{0}') AND AlarmType = 0", _alarmResolve.AlarmID.ToString()) : string.Format("AlarmID = '{0}'", _alarmResolve.AlarmID.ToString()));

                        cmd.ExecuteNonQuery();
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (transaction != null)
                {
                    if (result)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    transaction.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();
            }

            return result;
        }

        protected override bool UpdateSiteInstruction(SiteInstructionStep _siteInstructionStep)
        {
            bool result = false;

            SqlTransaction transaction = null;
            SqlCommand cmd = null;

            try
            {
                if (loggingCon != null && _siteInstructionStep != null)
                {
                    if (ConnectionOpen)
                    {
                        transaction = loggingCon.BeginTransaction();

                        cmd = loggingCon.CreateCommand();
                        cmd.Transaction = transaction;

                        cmd.CommandText = string.Format("UPDATE SiteInstructions SET Completed = {0}, CompletedByUserName = '{1}', CompletedDT = {2} WHERE InstructionID = '{3}'",
                            (_siteInstructionStep.Completed ? 1 : 0).ToString(), (_siteInstructionStep.User != null ? _siteInstructionStep.User.UserName : ""), _siteInstructionStep.CompletedDT != DateTime.MinValue ? string.Format("'{0}'", _siteInstructionStep.CompletedDT.ToString("yyyy-MM-dd HH:mm:ss.fff")) : "NULL", _siteInstructionStep.SiteInstructionStepID);

                        int recordsAffected = cmd.ExecuteNonQuery();

                        if (recordsAffected == 0)
                        {
                            cmd.CommandText = string.Format(@"INSERT INTO SiteInstructions (InstructionID, AlarmID, InstructionName, InstructionValue, InstructionOrder, Completed, CompletedByUserName, CompleteToResolve, CompletedDT) 
                                                                VALUES ('{0}','{1}','{2}','{3}',{4},'{5}','{6}','{7}', {8})",
                                _siteInstructionStep.SiteInstructionStepID, _siteInstructionStep.AlarmID, _siteInstructionStep.Name, _siteInstructionStep.Value, _siteInstructionStep.Order,
                                (_siteInstructionStep.Completed ? 1 : 0).ToString(), (_siteInstructionStep.User != null ? _siteInstructionStep.User.UserName : ""), (_siteInstructionStep.CompleteToResolve ? 1 : 0).ToString(), _siteInstructionStep.CompletedDT != DateTime.MinValue ? string.Format("'{0}'", _siteInstructionStep.CompletedDT.ToString("yyyy-MM-dd HH:mm:ss.fff")) : "NULL");

                            cmd.ExecuteNonQuery();
                        }
                    }
                }
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (transaction != null)
                {
                    if (result)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    transaction.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();
            }

            return result;
        }
        #endregion Update Alarm

        #region Load Alarm History
        public override List<Alarm> GetAlarmHistory(DateTime? _startDate, DateTime? _endDate, AlarmType? _alarmType, string _alarmName, EscalationLevel? _escalationLevel, bool? _resolved)
        {
            List<Alarm> result = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    result = new List<Alarm>();

                    con = new SqlConnection(ConnectionString);
                    con.Open();

                    cmd = con.CreateCommand();
                    string cmdText = "";

                    if (_startDate != null)
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" AlarmDT >= '{0}'", _startDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                    if (_endDate != null)
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" AlarmDT <= '{0}'", _endDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                    if (!string.IsNullOrWhiteSpace(_alarmName))
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" LOWER(AlarmName) LIKE LOWER('%{0}%')", _alarmName);

                    if (_alarmType != null)
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" AlarmType = {0}", (byte)_alarmType.Value);

                    if (_escalationLevel != null)
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" EscalationLevel = {0}", (byte)_escalationLevel.Value);

                    if (_resolved != null)
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" AlarmType {0} 0", _resolved.Value ? ">" : "=");
                    //cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" (select COUNT(*) from AlarmResolveItems where AlarmResolveItems.AlarmID = alarms.AlarmID) {0} 0", _resolved.Value ? ">" : "<=");

                    cmdText = string.Format("SELECT AlarmID, AlarmGroupID, AlarmName, AlarmDT, AlarmType, EscalationLevel, Priority, AlarmNumber, ServerID, RootCause FROM Alarms {0} ORDER BY AlarmDT ASC", cmdText);
                    //cmdText = string.Format("SELECT AlarmID, AlarmGroupID, AlarmName, AlarmDT, AlarmType, EscalationLevel, Priority FROM ViewUnresolvedAlarms {0}", cmdText);

                    cmd.CommandText = cmdText;

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Guid alarmID;
                        if (Guid.TryParse(reader["AlarmID"].ToString(), out alarmID))
                        {
                            try
                            {
                                Alarm alarm = new Alarm(alarmID);
                                alarm.Name = reader["AlarmName"].ToString();

                                Guid groupAlarmID = Guid.Empty; ;
                                if (Guid.TryParse(reader["AlarmGroupID"].ToString(), out groupAlarmID))
                                    alarm.GroupAlarmID = groupAlarmID;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing AlarmGroupID", null);
                                
                                DateTime alarmDT;
                                try
                                {
                                    alarmDT = reader.GetDateTime(reader.GetOrdinal("AlarmDT"));
                                    alarm.AlarmDT = alarmDT;
                                }
                                catch (Exception ex)
                                {
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing AlarmDT", ex);
                                }
                                
                                AlarmType alarmType;
                                try
                                {
                                    alarmType = (AlarmType)(byte.Parse(reader["AlarmType"].ToString()));
                                }
                                catch (Exception ex)
                                {
                                    alarmType = AlarmType.Unresolved;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing AlarmType", ex);
                                }
                                alarm.Alarm_Type = alarmType;

                                EscalationLevel escalationLevel;
                                try
                                {
                                    escalationLevel = (EscalationLevel)(byte.Parse(reader["EscalationLevel"].ToString()));
                                }
                                catch (Exception ex)
                                {
                                    escalationLevel = EscalationLevel.None;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing EscalationLevel", ex);
                                }
                                alarm.EscalationLevel = escalationLevel;

                                int alarmPriority;
                                if (int.TryParse(reader["Priority"].ToString(), out alarmPriority))
                                    alarm.Priority = alarmPriority;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing Priority", null);

                                int alarmNumber;
                                if (int.TryParse(reader["AlarmNumber"].ToString(), out alarmNumber))
                                    alarm.AlarmNumber = alarmNumber;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing AlarmNumber", null);

                                Guid serverID = Guid.Empty; ;
                                if (Guid.TryParse(reader["ServerID"].ToString(), out serverID))
                                    alarm.ServerID = serverID;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing ServerID", null);

                                bool rootCause = true;
                                try
                                {
                                    rootCause = reader.GetBoolean(reader.GetOrdinal("RootCause"));
                                }
                                catch (Exception ex)
                                {
                                    rootCause = true;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing RootCause", ex);
                                }
                                alarm.RootCause = rootCause;

                                result.Add(alarm);

                                foreach (IEventTrigger trigger in GetAlarmTriggers(alarm.ID, con))
                                    alarm.Triggers.Add(trigger);
                                foreach (AlarmResolve resolve in GetAlarmResolveItems(alarm.ID, con))
                                    alarm.ResolveItems.Add(resolve);
                                foreach (AlarmResolve resolve in GetAlarmResolveItems(alarm.GroupAlarmID, con)) //Load group resolves too.
                                    alarm.ResolveItems.Add(resolve);

                                //alarm.Images = GetAlarmImages(alarm.ID, _lastXImages, con);
                                //alarm.AddSiteInstructionStep(GetAlarmInstructionSteps(alarm.GroupAlarmID, con));
                                //alarm.AddComments(GetAlarmComments(alarm.GroupAlarmID, con));
                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error loading alarm", ex);
                            }
                        }
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing AlarmID", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return result;
        }

        public override List<IEventTrigger> GetAlarmTriggers(Guid _alarmID, object _con = null)
        {
            List<IEventTrigger> result = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    result = new List<IEventTrigger>();

                    if (_con is SqlConnection)
                        con = (SqlConnection)_con;
                    else
                        con = new SqlConnection(ConnectionString);

                    if (!(con.State == System.Data.ConnectionState.Open || con.State == System.Data.ConnectionState.Executing || con.State == System.Data.ConnectionState.Fetching))
                        con.Open();

                    cmd = con.CreateCommand();
                    cmd.CommandText = string.Format("SELECT TriggerID, DetectorName, DeviceName, DetectorType, Description, Location, MapName, TriggerDT, isnull(TriggerState, 1) as TriggerState, isnull(TriggerValue, 0) as TriggerValue, isnull(CustomData, '') as CustomData FROM AlarmTriggers WHERE AlarmID = '{0}' ORDER BY TriggerDT ASC", _alarmID.ToString());
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Guid triggerID;
                        if (Guid.TryParse(reader["TriggerID"].ToString(), out triggerID))
                        {
                            try
                            {
                                IEventTrigger trigger = new EventTrigger(triggerID, reader["DetectorName"].ToString(), reader["DeviceName"].ToString());

                                trigger.EventName = reader["Description"].ToString();
                                trigger.Location = reader["Location"].ToString();
                                trigger.MapName = reader["MapName"].ToString();

                                DetectorType detectorType;
                                try
                                {
                                    detectorType = (DetectorType)(byte.Parse(reader["DetectorType"].ToString()));
                                }
                                catch (Exception ex)
                                {
                                    detectorType = DetectorType.Unknown;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing DetectorType", ex);
                                }
                                trigger.DetectorType = detectorType;

                                DateTime triggerDT;
                                try
                                {
                                    triggerDT = reader.GetDateTime(reader.GetOrdinal("TriggerDT"));
                                    trigger.TriggerDT = triggerDT;
                                }
                                catch (Exception ex)
                                {
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing TriggerDT", ex);
                                }

                                bool triggerState = true;
                                try
                                {
                                    triggerState = reader.GetBoolean(reader.GetOrdinal("TriggerState"));
                                }
                                catch (Exception ex)
                                {
                                    triggerState = true;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing TriggerState", ex);
                                }
                                trigger.State = triggerState;

                                double triggerValue = 0;
                                try
                                {
                                    triggerValue = reader.GetDouble(reader.GetOrdinal("TriggerValue"));
                                }
                                catch (Exception ex)
                                {
                                    triggerValue = 0;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing TriggerValue", ex);
                                }
                                trigger.EventValue = triggerValue;

                                trigger.Data = reader["CustomData"].ToString();

                                result.Add(trigger);

                                //trigger.EventValues = GetAlarmTriggerValues(trigger.TriggerID);
                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error loading trigger", ex);
                            }
                        }
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing TriggerID", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                if (con != null && !(_con is SqlConnection)) //only dispose if we creted it.
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return result;
        }

        protected override double[] GetAlarmTriggerValues(Guid _alarmTriggerID, object _con = null)
        {
            double[] result = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            List<double> values = null;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    if (_con is SqlConnection)
                        con = (SqlConnection)_con;
                    else
                        con = new SqlConnection(ConnectionString);

                    if (!(con.State == System.Data.ConnectionState.Open || con.State == System.Data.ConnectionState.Executing || con.State == System.Data.ConnectionState.Fetching))
                        con.Open();

                    cmd = con.CreateCommand();
                    cmd.CommandText = string.Format("SELECT EventValue FROM AlarmTriggerEventValues WHERE AlarmTriggerID = '{0}'", _alarmTriggerID.ToString());
                    reader = cmd.ExecuteReader();

                    values = new List<double>();

                    while (reader.Read())
                    {
                        double val = 0;
                        if (double.TryParse(reader["EventValue"].ToString(), out val))
                            values.Add(val);
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing EventValue", null);
                    }

                    result = values.ToArray();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                if (con != null && !(_con is SqlConnection)) //only dispose if we creted it.
                {
                    con.Close();
                    con.Dispose();
                }

                if (values != null)
                    values.Clear();
            }

            return result;
        }

        public override List<EventImage> GetAlarmImages(Guid _alarmID, object _con = null)
        {
            return GetAlarmImages(_alarmID, null, _con);
        }

        public override List<EventImage> GetAlarmImages(Guid _alarmID, int? _lastXImages, object _con = null)
        {
            List<EventImage> result = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    result = new List<EventImage>();

                    if (_con is SqlConnection)
                        con = (SqlConnection)_con;
                    else
                        con = new SqlConnection(ConnectionString);

                    if (!(con.State == System.Data.ConnectionState.Open || con.State == System.Data.ConnectionState.Executing || con.State == System.Data.ConnectionState.Fetching))
                        con.Open();

                    cmd = con.CreateCommand();
                    cmd.CommandText = string.Format("SELECT {0} AlarmImageID, AlarmImageDT, CameraAddress, CameraID, Dewarp, ImageData FROM AlarmImages WHERE AlarmID = '{1}' OR AlarmID IN (SELECT AlarmID from Alarms WHERE AlarmGroupID = '{1}') ORDER BY AlarmImageDT DESC", _lastXImages != null ? "TOP " + _lastXImages : "", _alarmID.ToString());
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Guid imageID;
                        if (Guid.TryParse(reader["AlarmImageID"].ToString(), out imageID))
                        {
                            try
                            {
                                EventImage img = new EventImage(imageID);

                                img.EventID = _alarmID;

                                DateTime imgDT;
                                if (DateTime.TryParse(reader["AlarmImageDT"].ToString(), out imgDT))
                                    img.TimeStamp = imgDT;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing FirstTriggerDT", null);

                                img.CameraAddress = reader["CameraAddress"].ToString();
                                img.CameraID = reader["CameraID"].ToString();

                                bool dewarp = false;
                                try
                                {
                                    dewarp = reader.GetBoolean(reader.GetOrdinal("Dewarp"));
                                }
                                catch (Exception ex)
                                {
                                    dewarp = false;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing Dewarp", ex);
                                }
                                img.Dewarp = dewarp;

                                try
                                {
                                    img.ImageData = (byte[])reader["ImageData"];
                                }
                                catch (Exception ex)
                                {
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error loading ImageData", ex);
                                }

                                result.Add(img);
                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error loading AlarmImage", ex);
                            }
                        }
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing ImageID", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                if (con != null && !(_con is SqlConnection)) //only dispose if we created it.
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return result;
        }

        public override List<SiteInstructionStep> GetAlarmInstructionSteps(Guid _alarmID, object _con = null)
        {
            List<SiteInstructionStep> result = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    result = new List<SiteInstructionStep>();

                    if (_con is SqlConnection)
                        con = (SqlConnection)_con;
                    else
                        con = new SqlConnection(ConnectionString);

                    if (!(con.State == System.Data.ConnectionState.Open || con.State == System.Data.ConnectionState.Executing || con.State == System.Data.ConnectionState.Fetching))
                        con.Open();

                    cmd = con.CreateCommand();
                    cmd.CommandText = string.Format("SELECT InstructionID, AlarmID, InstructionName, InstructionValue, InstructionOrder, Completed, CompletedByUserName, CompleteToResolve, CompletedDT FROM SiteInstructions WHERE AlarmID = '{0}'", _alarmID.ToString());
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Guid instructionID;
                        if (Guid.TryParse(reader["InstructionID"].ToString(), out instructionID))
                        {
                            try
                            {
                                string instructionName = reader["InstructionName"].ToString();
                                string instructionValue = reader["InstructionValue"].ToString();
                                int instructionOrder = 0;
                                int.TryParse(reader["InstructionOrder"].ToString(), out instructionOrder);

                                bool instructionCompleted = false;
                                try
                                {
                                    instructionCompleted = reader.GetBoolean(reader.GetOrdinal("Completed"));
                                }
                                catch (Exception ex)
                                {
                                    instructionCompleted = false;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing Completed", ex);
                                }
                                bool completeToResolve = false;
                                try
                                {
                                    completeToResolve = reader.GetBoolean(reader.GetOrdinal("CompleteToResolve"));
                                }
                                catch (Exception ex)
                                {
                                    completeToResolve = false;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing CompleteToResolve", ex);
                                }

                                User completedByUser = null;
                                if (instructionCompleted)
                                {
                                    completedByUser = new User(reader["CompletedByUserName"].ToString());
                                }

                                DateTime completedDT = DateTime.MinValue;
                                if (!DateTime.TryParse(reader["CompletedDT"].ToString(), out completedDT))
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing CompletedDT", null);
                                
                                SiteInstructionStep siteInstructionStep = new SiteInstructionStep(instructionID, _alarmID, instructionName, instructionValue, instructionCompleted, completedByUser, completeToResolve, completedDT, instructionOrder);

                                result.Add(siteInstructionStep);
                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error loading Resolve Item", ex);
                            }
                        }
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing InstructionID", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                if (con != null && !(_con is SqlConnection)) //only dispose if we creted it.
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return result;
        }

        public override List<AlarmResolve> GetAlarmResolveItems(Guid _alarmID, object _con = null)
        {
            List<AlarmResolve> result = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    result = new List<AlarmResolve>();

                    if (_con is SqlConnection)
                        con = (SqlConnection)_con;
                    else
                        con = new SqlConnection(ConnectionString);

                    if (!(con.State == System.Data.ConnectionState.Open || con.State == System.Data.ConnectionState.Executing || con.State == System.Data.ConnectionState.Fetching))
                        con.Open();

                    cmd = con.CreateCommand();
                    cmd.CommandText = string.Format("SELECT ResolveItemID, ResolveDT, AlarmType, Description, UserName FROM AlarmResolveItems WHERE AlarmID = '{0}'", _alarmID.ToString());
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Guid resolveItemID;
                        if (Guid.TryParse(reader["ResolveItemID"].ToString(), out resolveItemID))
                        {
                            try
                            {
                                AlarmResolve resolveDetailItem = new AlarmResolve() { AlarmID = _alarmID, ResolveID = resolveItemID };

                                DateTime resolveDT;
                                if (DateTime.TryParse(reader["ResolveDT"].ToString(), out resolveDT))
                                    resolveDetailItem.ResolveDT = resolveDT;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing ResolveDT", null);

                                AlarmType alarmType;
                                try
                                {
                                    alarmType = (AlarmType)(byte.Parse(reader["AlarmType"].ToString()));
                                }
                                catch (Exception ex)
                                {
                                    alarmType = AlarmType.Unresolved;
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing AlarmType", ex);
                                }
                                resolveDetailItem.Alarm_Type = alarmType;

                                resolveDetailItem.Description = reader["Description"].ToString();
                                resolveDetailItem.User = new User(reader["UserName"].ToString());

                                result.Add(resolveDetailItem);
                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error loading Resolve Item", ex);
                            }
                        }
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing ResolveItemID", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                if (con != null && !(_con is SqlConnection)) //only dispose if we creted it.
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return result;
        }
        #endregion Load Alarm History

        #region OccurrenceBook
        protected override bool InsertOccurrenceBookEntry(OBEntry _obEntry)
        {
            bool result = false;

            SqlTransaction transaction = null;
            SqlCommand cmd = null;

            try
            {
                if (loggingCon != null && _obEntry != null)
                {
                    if (ConnectionOpen)
                    {
                        transaction = loggingCon.BeginTransaction();

                        cmd = loggingCon.CreateCommand();
                        cmd.Transaction = transaction;
                        string insertImg = "";

                        if (_obEntry is OccurrenceBook)
                        {
                            OccurrenceBook occurrenceBook = (OccurrenceBook)_obEntry;

                            insertImg = "INSERT INTO [OccurrenceBook] ([OccurrenceBookID], [OccurrenceType], [OccurrenceDescription], [OccurrenceDT], [UserName])";
                            insertImg += string.Format(" SELECT '{0}', '{1}', '{2}', '{3}', '{4}'",
                                occurrenceBook.ID, occurrenceBook.OccurrenceType, occurrenceBook.Description, occurrenceBook.OccurrenceDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), occurrenceBook.User);
                        }
                        else if (_obEntry is OBSubEntry)
                        {
                            OBSubEntry obSubEntry = (OBSubEntry)_obEntry;
                            insertImg = "INSERT INTO [OBSubEntry] ([OBSubEntryID], [OccurrenceBookID], [OccurrenceType], [OccurrenceDescription], [SubEntryDT], [UserName])";
                            insertImg += string.Format(" SELECT '{0}', '{1}', '{2}', '{3}', '{4}', '{5}'",
                                obSubEntry.ID, obSubEntry.OBID, obSubEntry.OccurrenceType, obSubEntry.Description, obSubEntry.OccurrenceDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), obSubEntry.User);
                        }

                        if (!string.IsNullOrWhiteSpace(insertImg))
                        {
                            cmd.CommandText = insertImg;

                            cmd.ExecuteNonQuery();
                        }

                        if (_obEntry is OccurrenceBook)
                        {
                            OccurrenceBook occurrenceBook = (OccurrenceBook)_obEntry;
                            SqlDataReader reader = null;

                            try
                            {
                                cmd.CommandText = string.Format("select IDNumber from OccurrenceBook where OccurrenceBookID = '{0}'", occurrenceBook.ID);
                                reader = cmd.ExecuteReader();
                                if (reader.Read())
                                {
                                    occurrenceBook.IDNumber = int.Parse(reader["IDNumber"].ToString());
                                }
                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error querying IDNumber", ex);
                            }
                            finally
                            {
                                if (reader != null)
                                {
                                    reader.Close();
                                    reader.Dispose();
                                }
                            }
                        }
                    }
                }

                result = true;
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (transaction != null)
                {
                    if (result)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    transaction.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();
            }

            return result;
        }

        public override List<OccurrenceBook> GetOccurrenceBookHistory(DateTime? _startDate, DateTime? _endDate, string _typeDescription, string _description, string _userName)
        {
            List<OccurrenceBook> result = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    result = new List<OccurrenceBook>();

                    con = new SqlConnection(ConnectionString);
                    con.Open();

                    cmd = con.CreateCommand();
                    string cmdText = "";

                    if (_startDate != null)
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" OccurrenceDT >= '{0}'", _startDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                    if (_endDate != null)
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" OccurrenceDT <= '{0}'", _endDate.Value.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                    if (!string.IsNullOrWhiteSpace(_typeDescription))
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" LOWER(OccurrenceType) LIKE LOWER('%{0}%')", _typeDescription);

                    if (_description != null)
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" LOWER(OccurrenceDescription) LIKE LOWER('%{0}%')", _description);

                    if (_userName != null)
                        cmdText += (cmdText == "" ? " WHERE" : " AND") + string.Format(" LOWER(UserName) LIKE LOWER('%{0}%')", _userName);

                    cmdText = "SELECT OccurrenceBookID, IDNumber, OccurrenceType, OccurrenceDescription, OccurrenceDT, UserName FROM OccurrenceBook" + cmdText;

                    cmd.CommandText = cmdText;

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Guid obID;
                        if (Guid.TryParse(reader["OccurrenceBookID"].ToString(), out obID))
                        {
                            try
                            {
                                OccurrenceBook ob = new OccurrenceBook(obID);
                                ob.OccurrenceType = reader["OccurrenceType"].ToString();
                                ob.Description = reader["OccurrenceDescription"].ToString();
                                ob.User = new User(reader["UserName"].ToString());

                                int idnumber = 0;
                                if (int.TryParse(reader["IDNumber"].ToString(), out idnumber))
                                    ob.IDNumber = idnumber;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing IDNumber", null);

                                DateTime obDT;
                                if (DateTime.TryParse(reader["OccurrenceDT"].ToString(), out obDT))
                                    ob.OccurrenceDT = obDT;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing OccurrenceDT", null);

                                List<OBSubEntry> subEntries = GetOBSubEntries(obID);
                                if (subEntries != null)
                                {
                                    foreach (OBSubEntry entry in subEntries)
                                    {
                                        ob.SubEntries.Add(entry);
                                    }
                                }

                                result.Add(ob);

                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error loading OccurrenceBook", ex);
                            }
                        }
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing OccurrenceBookID", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return result;
        }

        public override List<OBSubEntry> GetOBSubEntries(Guid _occurrenceBookID)
        {
            List<OBSubEntry> result = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    result = new List<OBSubEntry>();

                    con = new SqlConnection(ConnectionString);
                    con.Open();

                    cmd = con.CreateCommand();
                    string cmdText = string.Format("SELECT OBSubEntryID, OccurrenceType, OccurrenceDescription, SubEntryDT, UserName FROM OBSubEntry WHERE OccurrenceBookID = '{0}'", _occurrenceBookID);

                    cmd.CommandText = cmdText;

                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Guid obSubEntryID;
                        if (Guid.TryParse(reader["OBSubEntryID"].ToString(), out obSubEntryID))
                        {
                            try
                            {
                                OBSubEntry obSubEntry = new OBSubEntry(obSubEntryID);
                                obSubEntry.OBID = _occurrenceBookID;
                                obSubEntry.OccurrenceType = reader["OccurrenceType"].ToString();
                                obSubEntry.Description = reader["OccurrenceDescription"].ToString();
                                obSubEntry.User = new User(reader["UserName"].ToString());

                                DateTime obDT;
                                if (DateTime.TryParse(reader["SubEntryDT"].ToString(), out obDT))
                                    obSubEntry.OccurrenceDT = obDT;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing SubEntryDT", null);

                                result.Add(obSubEntry);

                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error loading OccurrenceBook", ex);
                            }
                        }
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing OBSubEntryID", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                if (con != null)
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return result;
        }
        #endregion OccurrenceBook

        #region Alarm Comments
        protected override bool InsertAlarmComment(AlarmComment _comment)
        {
            bool result = false;

            SqlTransaction transaction = null;
            SqlCommand cmd = null;

            try
            {
                if (loggingCon != null)
                {
                    if (ConnectionOpen)
                    {
                        transaction = loggingCon.BeginTransaction();

                        cmd = loggingCon.CreateCommand();
                        cmd.Transaction = transaction;
                        string cmdText = "";

                        cmdText = "INSERT INTO [AlarmComments] ([CommentID], [AlarmID], [CommentDT], [Comment], [UserName])";
                        cmdText += string.Format(" SELECT '{0}', '{1}', '{2}', '{3}', '{4}'",
                            _comment.CommentID, _comment.AlarmID, _comment.CommentDT.ToString("yyyy-MM-dd HH:mm:ss.fff"), _comment.Comment, _comment.User);

                        if (!string.IsNullOrWhiteSpace(cmdText))
                        {
                            cmd.CommandText = cmdText;

                            cmd.ExecuteNonQuery();
                        }

                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                result = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (transaction != null)
                {
                    if (result)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    transaction.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();
            }

            return result;
        }

        public override List<AlarmComment> GetAlarmComments(Guid _alarmID, object _con = null)
        {
            List<AlarmComment> result = null;

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader reader = null;

            try
            {
                if (!XmlNode.ParseBool(Settings.TheeSettings["NoDB"], false))
                {
                    result = new List<AlarmComment>();

                    if (_con is SqlConnection)
                        con = (SqlConnection)_con;
                    else
                        con = new SqlConnection(ConnectionString);

                    if (!(con.State == System.Data.ConnectionState.Open || con.State == System.Data.ConnectionState.Executing || con.State == System.Data.ConnectionState.Fetching))
                        con.Open();

                    cmd = con.CreateCommand();
                    cmd.CommandText = string.Format("SELECT CommentID, CommentDT, Comment, UserName FROM AlarmComments WHERE AlarmID = '{0}'", _alarmID.ToString());
                    reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        Guid commentID;
                        if (Guid.TryParse(reader["CommentID"].ToString(), out commentID))
                        {
                            try
                            {
                                AlarmComment comment = new AlarmComment() { CommentID = commentID, AlarmID = _alarmID, Comment = reader["Comment"].ToString(), User = new User(reader["UserName"].ToString()) };

                                DateTime commentDT;
                                if (DateTime.TryParse(reader["CommentDT"].ToString(), out commentDT))
                                    comment.CommentDT = commentDT;
                                else
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing CommentDT", null);



                                result.Add(comment);

                            }
                            catch (Exception ex)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error loading Comment", ex);
                            }
                        }
                        else
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "Error parsing CommentID", null);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                    reader.Dispose();
                }

                if (cmd != null)
                    cmd.Dispose();

                if (con != null && !(_con is SqlConnection)) //only dispose if we creted it.
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return result;
        }
        #endregion
    }
}
