﻿using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BlackBoxEngine
{
    public enum ZCP2BlockMarkers : byte { SOH = 1, STX = 2, ETX = 3, EOT = 4, ENQ = 5, ACK = 6, NAK = 21 };
        
    public class ZCP2Client : Client
    {
        public delegate void BinaryMessageEventHandler(object sender, System.Net.Sockets.TcpClient client, byte[] message);
        public event BinaryMessageEventHandler OnZCP2MessageReceived;

        protected override int CheckForMessage(ref byte[] _buffer, System.Net.Sockets.TcpClient _client, int initialSTXOffset = 0)
        {
            int result = 0;
            byte[] newBuffer = null;

            if (_buffer.Length > 0)
            {
                if (_buffer[0] == (byte)ZCP2BlockMarkers.SOH || _buffer[0] == (byte)ZCP2BlockMarkers.ACK || _buffer[0] == (byte)ZCP2BlockMarkers.NAK)
                {
                    int bytesToRemove = 0;

                    for (int i = 0; i < _buffer.Length; i++)
                    {
                        if (_buffer[i] == (byte)ZCP2BlockMarkers.EOT) //look for the end of transmission
                        {
                            bytesToRemove = _buffer.Length - (i + 1);

                            byte[] msg = new byte[i + 1];
                            Array.Copy(_buffer, msg, i + 1);

                            Thread processMsgThread = new Thread(delegate() { ProcessMessage(new ClientMessage(_client, msg)); });
                            processMsgThread.Start();
                            //Thread processMsgThread = new Thread(new ParameterizedThreadStart(ProcessMessage));
                            //processMsgThread.Start(new ClientMessage(_client, msg));

                            result++;
                        }
                    }

                    //create a buffer for the left over data.
                    newBuffer = new byte[bytesToRemove];
                    Array.Copy(_buffer, bytesToRemove, newBuffer, 0, _buffer.Length - bytesToRemove);

                    if (result > 0) // we will only check for another message if we managed to get 1.
                        result += CheckForMessage(ref newBuffer, _client);
                }
            }

            _buffer = newBuffer;

            return result;
        }

        protected override void ProcessMessage(ClientMessage _clientMessage)
        {
            try
            {
                if (OnZCP2MessageReceived != null)
                    OnZCP2MessageReceived(this, _clientMessage.Client, _clientMessage.Data);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public virtual bool SendMessage(byte[] _message)
        {
            return SendMessage(this.client, _message);
        }
    }

    public class FirePanelZ3
    {
        public static void DecodeData(byte[] _data)
        {
            if (_data != null)
            {
                if (_data.Length > 17)
                {
                    if (_data[0] == (byte)ZCP2BlockMarkers.SOH)
                    {
                        byte[] tmpBuffer = new byte[3];
                        Array.Copy(_data, 1, tmpBuffer, 0, 3);
                        string senderID = Encoding.ASCII.GetString(tmpBuffer);

                        Array.Copy(_data, 4, tmpBuffer, 0, 3);
                        string TargetID = Encoding.ASCII.GetString(tmpBuffer);
                    }
                }
            }
        }

        public static byte[] EncodeAcknowledge(int _senderID, int _panelID)
        {
            byte[] result = null;

            try
            {
                result = new byte[13];

                result[0] = (byte)ZCP2BlockMarkers.ACK;

                Encoding.ASCII.GetBytes(Convert.ToString(_senderID).PadLeft(3, '0').Substring(0, 3), 0, 3, result, 1);
                Encoding.ASCII.GetBytes(Convert.ToString(_panelID).PadLeft(3, '0').Substring(0, 3), 0, 3, result, 4);

                int checksum = 0;

                for (int i = 0; i < result.Length - 6; i++)
                {
                    checksum += result[i];
                }

                Encoding.ASCII.GetBytes(Convert.ToString(checksum).PadLeft(5, '0').Substring(0, 5), 0, 5, result, 7);

                result[12] = (byte)ZCP2BlockMarkers.EOT;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public static byte[] EncodeInstruction(int _senderID, int _panelID, string _instruction)
        {
            byte[] result = null;

            try
            {
                result = new byte[17 + _instruction.Length];

                result[0] = (byte)ZCP2BlockMarkers.SOH;

                Encoding.ASCII.GetBytes(Convert.ToString(_senderID).PadLeft(3, '0').Substring(0, 3), 0, 3, result, 1);
                Encoding.ASCII.GetBytes(Convert.ToString(_panelID).PadLeft(3, '0').Substring(0, 3), 0, 3, result, 4);

                result[7] = 49;
                result[8] = 49;
                result[9] = (byte)ZCP2BlockMarkers.STX;

                Encoding.ASCII.GetBytes(_instruction, 0, _instruction.Length, result, 10);

                result[10 + _instruction.Length] = (byte)ZCP2BlockMarkers.ETX;

                int checksum = 0;

                for (int i = 0; i < result.Length - 6; i++)
                {
                    checksum += result[i];
                }

                Encoding.ASCII.GetBytes(Convert.ToString(checksum).PadLeft(5, '0').Substring(0, 5), 0, 5, result, 11 + _instruction.Length);

                result[16 + _instruction.Length] = (byte)ZCP2BlockMarkers.EOT;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }
    }
}
