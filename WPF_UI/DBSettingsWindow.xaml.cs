﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for DBSettingsWindow.xaml
    /// </summary>
    public partial class DBSettingsWindow : Window
    {
        public bool NewCredentials = false;
        public DBSettingsWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Submit updated DB details to server for testing - TODO
        /// </summary>
        private void btnTest_Click(object sender, RoutedEventArgs e)
        {
            NewCredentials = true;
            btnTest.IsEnabled = false;
            this.DialogResult = true;
            this.Close();
        }

        /// <summary>
        /// Disable 'Submit' until new details are tested
        /// </summary>
        private void textChangedEventHandler(object sender, RoutedEventArgs e)
        {
            btnTest.IsEnabled = true;
            btnSubmit.IsEnabled = false;
        }

        /// <summary>
        /// Send new details to server - TODO
        /// </summary>
        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            btnTest.IsEnabled = false;
            btnSubmit.IsEnabled = false;
            NewCredentials = false;
            this.DialogResult = true;
            this.Close();
        }

        public string Database
        {
            get
            {
                return txtDatabase.Text;
            }
        }
        public string Server
        {
            get
            {
                return txtServername.Text;
            }
        }
        public string UserId
        {
            get
            {
                return txtUserId.Text;
            }
        }
        public string Password
        {
            get
            {
                return txtPassword.Password;
            }
        }        
    }
}
