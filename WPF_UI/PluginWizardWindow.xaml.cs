﻿using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for PluginWizardWindow.xaml
    /// </summary>
    public partial class PluginWizardWindow : Window
    {
        public PluginWizardWindow()
        {
            InitializeComponent();
        }

        UserControl wizardControl = null;

        public IPluginWizard WizardControl
        {
            get
            {
                return wizardControl as IPluginWizard;
            }
            set
            {
                wizardControl = value as UserControl;
                if (wizardControl != null)
                {
                    wizardGrid.Children.Add(wizardControl);
                    this.Height = wizardControl.Height;
                    this.Width = wizardControl.Width;
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            wizardGrid.Children.Clear();
        }
    }
}
