﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace WPF_UI
{
    [ValueConversion(typeof(string), typeof(String))]
    public class CustomImagePathConverter : IValueConverter
    {
        #region IValueConverter Members
        /// <summary>
        /// Meaningless comment to test SCM's notification system.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="targetType"></param>
        /// <param name="parameter"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            string baseDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string result = Path.Combine(baseDir, @"Resources/placeholder.png");

            if (value != null)
            {
                string imgPath = value.ToString();

                if (!string.IsNullOrWhiteSpace(imgPath))
                {   
                    string fullFilePath = Path.Combine(baseDir, imgPath);
                    
                    if(File.Exists(fullFilePath))
                        result = fullFilePath;
                    else
                        result = Path.Combine(baseDir, @"Resources/placeholder.png");
                }
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
                                        System.Globalization.CultureInfo culture)
        {
            return "";
        }

        #endregion
    }

    [ValueConversion(typeof(bool), typeof(int))]
    public class BoolToStateIndexConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int result = 0;

            if (value is bool)
                if ((bool)value)
                    result = 1;
                else
                    result = 0;

            return result;            
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool result = false;

            if (value is int)
                result = !(((int)value) == 0);

            return result;
        }
    }
}
