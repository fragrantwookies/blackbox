﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;

using BlackBoxEngine;
using eThele.Essentials;
using eNerve.DataTypes;
using eNerve.DataTypes.Gauges;
using eNervePluginInterface;
using eTheleSoundPlayer;
using System.Net;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable//, INotifyPropertyChanged
    {
        public static Guid ServerID;
        public static string ServerName;
        public static Version ServerVersion;

        public static Client Client;
        public static User DisplayUser;

        public static List<MainWindow> children;
        //private Client.ConnectState connectionStatus; Not used anymore

        private Client.ServerDetails serverDetails;

        private DevicesDoc devicesDoc = null;
        private MapDoc mapDoc = null;

        private ObservableCollection<GroupAlarm> alarmStack;

        private AlarmBase alarmToResolve = null;

        private ICollectionView alarmHistoryDataView;
        private ObservableCollection<AlarmBase> alarmHistoryItems;
        private AlarmHistoryFilter alarmHistoryFilter;        
        
        public static User CurrentUser;
        public static User NewUser;
        private static int numWindows;

        private List<VideoWindow.MainWindow> cameraWindows;

        //private string selectedUserName = "";
        //private Thread newWindowThread;

        private DBSettingsWindow dbWindow = null;
        
        private UIPluginManager pluginManager = null;

        SoundPlayer soundPlayer;

        public MainWindow()
        {
            InitializeComponent();

            tabSystemConfig.IsEnabled = false;
            tabSystemConfig.Visibility = Visibility.Collapsed;
            tabActions.IsEnabled = false;
            tabActions.Visibility = Visibility.Collapsed;
            tabConditions.IsEnabled = false;
            tabConditions.Visibility = Visibility.Collapsed;
            tabDeviceSetup.IsEnabled = false;
            tabDeviceSetup.Visibility = Visibility.Collapsed;
            tabOccurrenceBook.IsEnabled = false;
            tabOccurrenceBook.Visibility = Visibility.Collapsed;
            tabMapSetup.IsEnabled = false;
            tabMapSetup.Visibility = Visibility.Collapsed;

            if (Client == null)
            {
                numWindows = 1;
                Client = new Client();
                serverDetails = Client.GetServerDetails();

                Client.ConnectionStatusChanged += client_ConnectionStatusChanged;
                Client.OnObjectMessageReceived += client_OnObjectMessageReceived;
                Client.OnStringMessageReceived += client_OnStringMessageReceived;
            }
            else
            {
                numWindows++;
                if (children == null)
                    children = new List<MainWindow>();
                children.Add(this);
            }

            //listDevices.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("Content", System.ComponentModel.ListSortDirection.Ascending));

            //Authenticate();
        }

        #region Cleanup
        ~MainWindow()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (cameraWindows != null)
            {
                foreach (VideoWindow.MainWindow window in cameraWindows)
                {
                    window.Closed -= VideoHost_Closing;
                    if (Dispatcher.CheckAccess())
                        window.Close();
                }
            }

            if (Client != null)
            {
                Client.Dispose();
                Client = null;
            }

            if (mapDoc != null)
                mapDoc.Dispose();

            if (devicesDoc != null)
                devicesDoc.Dispose();
        }
        #endregion Cleanup

        #region Begin and End
        private void LoadInitialData()
        {
            try
            {
                statusMessageControl.AddMessage(new StatusMessage() { StatusID = Guid.NewGuid(), Message = "Loading initial data",
                    MessageTimeStamp = DateTime.Now, Animation = true, AutoRemove = TimeSpan.FromSeconds(5) });

                Client.SendMessage(MessageType.RequestServerID);
                Client.SendMessage(MessageType.RequestServerName);

                Client.SendMessage(MessageType.RequestServerVersion);
                Client.SendMessage(MessageType.RequestDBConnectionState);
                Client.SendMessage(MessageType.PluginTypes);
                //Client.SendMessage(MessageType.RequestDeviceDoc); //request this only once we have received plugin types
                Client.SendMessage(MessageType.RequestMapDoc);
                Client.SendMessage(MessageType.RequestUsersOnline);
                Client.SendMessage(MessageType.RequestUnresolvedAlarms);
                Client.SendMessage(MessageType.RequestOBTypes);
                //Client.SendMessage(MessageType.RequestActionsDoc); //request this only once we have received DeviceDoc
                Client.SendMessage(MessageType.RequestPublicDashboard);
                Client.SendMessage(MessageType.RequestPrivateDashboard, CurrentUser.UserName);
                /*Client.SendMessage(MessageType.RequestAlarmNames); Moved to after DeviceDoc was received.
                Client.SendMessage(MessageType.RequestAlarmSchedule);
                Client.SendMessage(MessageType.RequestConditionsDoc);
                Client.SendMessage(MessageType.CameraLayouts, CurrentUser);*/
                statusMessageControl.AddMessage(reportControl.ReportMessage);
                Client.SendMessage(MessageType.ReportList);
                
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (numWindows == 1)
            {
                try
                {
                    Exceptions.ExceptionsManager.OnException += ExceptionsManager_OnException;

                    alarmStack = new ObservableCollection<GroupAlarm>();
                    lbAlarmMaster.DataContext = alarmStack;

                    alarmHistoryItems = new ObservableCollection<AlarmBase>();
                    lbAlarmHistory.DataContext = alarmHistoryItems;

                    ICollectionView dataView = CollectionViewSource.GetDefaultView(lbAlarmMaster.DataContext);

                    if (dataView != null)
                    {
                        dataView.SortDescriptions.Clear();
                        dataView.SortDescriptions.Add(new SortDescription("Priority", ListSortDirection.Ascending));
                        dataView.SortDescriptions.Add(new SortDescription("AlarmDT", ListSortDirection.Ascending));
                    }

                    alarmHistoryDataView = CollectionViewSource.GetDefaultView(lbAlarmHistory.DataContext);
                    if (alarmHistoryDataView != null)
                    {
                        alarmHistoryDataView.SortDescriptions.Clear();
                        alarmHistoryDataView.SortDescriptions.Add(new SortDescription("AlarmDT", ListSortDirection.Descending));
                    }

                    panelAlarmResolver.Visibility = Visibility.Collapsed;

                    mapControl.OnMapLoaded += mapControl_OnMapLoaded;
                    mapControl.OnRequestSave += mapControl_OnRequestSave;
                    mapControl.RelaySwitchMessage += mapControl_RelaySwitchMessage;
                    mapControl.SnapshotRequest += mapControl_SnapshotRequest;

                    occurrenceBookControl.OBEntryAdded += occurrenceBookControl_OBEntryAdded;

                    occurrenceBookControl.OBHistorySearch += occurrenceBookControl_OBHistorySearch;

                    alarmScheduleControl.SaveSchedule += alarmScheduleControl_SaveSchedule;

                    conditionsControl.SaveConditions += conditionsControl_SaveConditions;

                    actionsControl.SaveActions += actionsControl_SaveActions;

                    deviceSetupControl.SaveDevices += deviceSetupControl_SaveDevices;
                    deviceSetupControl.SendMessageEvent += control_SendMessageEvent;

                    mapSetupControl.SaveMaps += mapSetupControl_SaveMaps;

                    //pluginsControl.PluginTypeMessage += pluginsControl_RequestPluginTypesEvent;

                    reportControl.SendMessageEvent += control_SendMessageEvent;

                    systemConfig.AuthenticateEvent += systemConfig_AuthenticateEvent;
                    systemConfig.ClosingTime += systemConfig_ClosingTime;
                    systemConfig.SendMessageEvent += control_SendMessageEvent;

                    pluginManager = new UIPluginManager();
                    pluginManager.ControlLoaded += pluginManager_ControlLoaded;
                    pluginManager.ControlRemoved += pluginManager_ControlRemoved;
                    pluginManager.FindPlugins();
                    
                    Title = string.Format("RioSoft Control Room - Version: {0}", Assembly.GetExecutingAssembly().GetName().Version.ToString(2));
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                        "", ex);
                }
            }
        }

        public void CloseMe()
        {
            try
            {
                if (Dispatcher.CheckAccess())
                {
                    if (cameraWindows != null && cameraWindows.Count > 0)
                    {
                        foreach (VideoWindow.MainWindow window in cameraWindows)
                        {
                            window.Close();
                        }
                    }
                    Close();
                }
                else
                    Dispatcher.Invoke(CloseMe);
            }
            catch (TaskCanceledException)
            {
                //bury it
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), 
                    "", ex);
            }
        }


        private void systemConfig_AuthenticateEvent(AuthenticateEventArgs e)
        {
            if (e != null)
            {
                Authenticate(e.Message);
            }
        }

        private void systemConfig_ClosingTime(object sender, EventArgs e)
        {
            CloseMe();
        }

        /// <summary>
        /// Confirmation dialogue could be displayed when the server goes down - to be tested.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (numWindows <= 1 || infoColumn.ActualWidth >= 100)
            {
                MessageBoxResult result;
                if (CurrentUser != null && CurrentUser.Authenticated)
                {
                    result = MessageBox.Show(
                                      "Are you sure you want to exit?",
                                      "Exit",
                                      MessageBoxButton.YesNo,
                                      MessageBoxImage.Question,
                                      MessageBoxResult.No);

                }
                else
                {
                    result = MessageBoxResult.Yes;
                }

                if (result != MessageBoxResult.Yes)
                    e.Cancel = true;
                else
                {

                    try
                    {
                        if (CurrentUser != null && Client != null)
                            if (CurrentUser.Authenticated)
                            {
                                Client.SendMessage(MessageType.UserLogout, CurrentUser);
                                CurrentUser.Authenticated = false;
                            }

                        if (soundPlayer != null)
                        {
                            soundPlayer.Dispose();
                            soundPlayer = null;
                        }

                        if (pluginManager != null)
                        {
                            pluginManager.ControlLoaded -= pluginManager_ControlLoaded;
                            pluginManager.ControlRemoved -= pluginManager_ControlRemoved;
                            pluginManager.ClearPlugins();
                        }

                        mapControl.SnapshotRequest -= mapControl_SnapshotRequest;
                        mapControl.RelaySwitchMessage -= mapControl_RelaySwitchMessage;
                        mapControl.OnRequestSave -= mapControl_OnRequestSave;
                        mapControl.OnMapLoaded -= mapControl_OnMapLoaded;

                        occurrenceBookControl.OBEntryAdded -= occurrenceBookControl_OBEntryAdded;
                        occurrenceBookControl.OBHistorySearch -= occurrenceBookControl_OBHistorySearch;
                        occurrenceBookControl.Dispose();

                        alarmScheduleControl.SaveSchedule -= alarmScheduleControl_SaveSchedule;

                        conditionsControl.SaveConditions -= conditionsControl_SaveConditions;
                        conditionsControl.Dispose();

                        actionsControl.SaveActions -= actionsControl_SaveActions;
                        actionsControl.Dispose();

                        deviceSetupControl.SaveDevices -= deviceSetupControl_SaveDevices;
                        deviceSetupControl.SendMessageEvent -= control_SendMessageEvent;
                        deviceSetupControl.Dispose();

                        mapSetupControl.SaveMaps -= mapSetupControl_SaveMaps;
                        mapSetupControl.Dispose();

                        reportControl.SendMessageEvent -= control_SendMessageEvent;

                        systemConfig.AuthenticateEvent -= systemConfig_AuthenticateEvent;
                        systemConfig.SendMessageEvent -= control_SendMessageEvent;
                        systemConfig.ClosingTime -= systemConfig_ClosingTime;
                        systemConfig.Dispose();

                        if (Client != null)
                        {
                            Client.ConnectionStatusChanged -= client_ConnectionStatusChanged;
                            Client.OnObjectMessageReceived -= client_OnObjectMessageReceived;
                            Client.OnStringMessageReceived -= client_OnStringMessageReceived;
                            Client.Dispose();
                            Client = null;
                        }

                        ClearAllAlarms();

                        
                    }
                    catch (Exception ex)
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                            MethodBase.GetCurrentMethod().Name), "", ex);
                    }
                }
                if (numWindows > 1)
                {
                    for (int i = children.Count - 1; i >= 0; i--)
                    {
                        children[i].CloseMe();
                    }
                }

                Exceptions.ExceptionsManager.OnException -= ExceptionsManager_OnException;
                Exceptions.ExceptionsManager.Save();
                Exceptions.DisposeManager();

                Settings.Selfdesctruct();
            }
            else
            {
                children.Remove(this);
                numWindows--; // TODO fix issue when main window is first to close and exceptions are raised.
                /*if (numWindows == 1)
                {
                    if (CheckAccess())
                        ShowInfoColumn();
                    else
                        Dispatcher.Invoke(ShowInfoColumn);
                }*/
            }
        }

        private void control_SendMessageEvent(SendMessageEventArgs e)
        {
            if (e != null)
            {
                if (e.MessageType == MessageType.CameraLayouts)
                {// Open a new video window, then send layout to server to save user's config.
                    VideoHostConfiguration config = e.Message as VideoHostConfiguration;
                    if (config != null)
                        AddVideoWindow(config);
                }

                if (e.MessageType == MessageType.StatusMessage)
                {
                    if (e.Message is StatusMessage)
                        statusMessageControl.AddMessage((StatusMessage)e.Message);
                }
                else if (e.MessageType == MessageType.StatusMessageRemove)
                {
                    statusMessageControl.RemoveMessage((StatusMessage)e.Message);
                }
                else
                {
                    Client.SendMessage(e.MessageType, e.Message);
                }
            }
        }

        void ExceptionsManager_OnException(object sender, ExceptionItem exception)
        {
            logControl.AddItem(exception);
        }
        #endregion Begin and End

        #region Plug-ins

        void pluginManager_ControlLoaded(object sender, UIPluginManager.UserControlPluginEventArgs e)
        {
            try
            {
                if (e.PluginControl != null && e.PluginControl is IUserInterfacePlugin)
                {
                    pluginManager_ControlRemoved(sender, e);

                    TabItem tab = new TabItem();
                    tab.Name = string.Format("tabPlugin{0}", ((IUserInterfacePlugin)e.PluginControl).PluginName.Replace('.', '_').Replace(' ', '_'));
                    //string[] ctrlNames = ((IUserInterfacePlugin)e.PluginControl).PluginName.Split('.');
                    //tab.Header = ctrlNames[ctrlNames.Length - 1];
                    tab.Header = ((IUserInterfacePlugin)e.PluginControl).PluginName;
                    tab.Content = e.PluginControl;
                    tabCtrlMain.Items.Add(tab);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        void pluginManager_ControlRemoved(object sender, UIPluginManager.UserControlPluginEventArgs e)
        {
            try
            {
                if (e.PluginControl != null && e.PluginControl is IUserInterfacePlugin)
                {
                    for (int i = tabCtrlMain.Items.Count - 1; i >= 0; i--)
                    {
                        if (tabCtrlMain.Items[i] is TabItem)
                        {
                            TabItem tab = (TabItem)tabCtrlMain.Items[i];
                            if (tab.Name == string.Format("tabPlugin{0}", ((IUserInterfacePlugin)e.PluginControl).PluginName.Replace('.', '_')))
                            {
                                tabCtrlMain.Items.RemoveAt(i);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void pluginsControl_RequestPluginTypesEvent(object sender, EventArgs e)
        {
            Client.SendMessage(MessageType.PluginTypes);
        }
        #endregion Plug-ins

        #region MapControl eventhandlers
        private void mapControl_OnMapLoaded(object sender, EventArgs e)
        {
            try
            {
                if (alarmStack != null && mapControl != null)
                {
                    for (int i = 0; i < alarmStack.Count; i++)
                    {
                        mapControl.FlashDetector(alarmStack[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void mapControl_OnRequestSave(object sender, EventArgs e)
        {
            try
            {
                if (Client != null)
                {
                    if (mapDoc != null)
                        Client.SendMessage(MessageType.MapDoc, mapDoc);

                    if (devicesDoc != null)
                        Client.SendMessage(MessageType.DeviceDoc, devicesDoc);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void mapControl_RelaySwitchMessage(object sender, WPFMapControl.MapControl2.RelaySwitchMessageEventArgs e)
        {
            try
            {
                if (Client != null && e != null && e.RelaySwitchMessage != null)
                    Client.SendMessage(MessageType.SwitchRelay, e.RelaySwitchMessage);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        void mapControl_SnapshotRequest(object sender, WPFMapControl.MapControl2.SnapshotRequestEventArgs e)
        //private void mapControl_SnapshotRequest(object sender, CameraImage cameraImg)
        {
            try
            {
                if (Client != null && e != null && e.CameraImage != null)
                    Client.SendMessage(MessageType.CameraSnapshot, e.CameraImage);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }
        #endregion MapControl eventhandlers              

        #region OccurrenceBook eventHandlers
        private void occurrenceBookControl_OBEntryAdded(object sender, OBEntry _obEntry)
        {
            try
            {
                if (Client != null && _obEntry != null)
                    Client.SendMessage(MessageType.OccurenceBook, _obEntry);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void occurrenceBookControl_OBHistorySearch(object sender, OccurrenceBookHistoryFilter _obHistoryFilter)
        {
            try
            {
                if (Client != null && _obHistoryFilter != null)
                    Client.SendMessage(MessageType.RequestOccurrenceBookHistory, _obHistoryFilter);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }
	    #endregion OccurrenceBook eventHandlers

        #region Save docs
        private void alarmScheduleControl_SaveSchedule(object sender, List<Schedule> _scheduleItems)
        {
            try
            {
                if (Client != null && _scheduleItems != null)
                    Client.SendMessage(MessageType.AlarmSchedule, _scheduleItems);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void conditionsControl_SaveConditions(object sender, XMLDoc _doc)
        {
            try
            {
                if (Client != null && _doc != null)
                    Client.SendMessage(MessageType.ConditionsDoc, _doc);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void actionsControl_SaveActions(object sender, XMLDoc _doc)
        {
            try
            {
                if (Client != null && _doc != null)
                    Client.SendMessage(MessageType.ActionsDoc, _doc);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void deviceSetupControl_SaveDevices(object sender, DevicesDoc _doc)
        {
            try
            {
                if (Client != null && _doc != null)
                    Client.SendMessage(MessageType.DeviceDoc, _doc);
                
                mapControl.Devices_Doc = _doc;
                mapControl.LoadMap(mapControl.CurrentMapName);

                LoadMapTree(mapDoc);
                LoadOfflineDevices();

                conditionsControl.LoadDevicesandTriggers(devicesDoc);

                if (Client != null)
                {
                    Client.SendMessage(MessageType.RequestDevicesOnline, null);
                    Client.SendMessage(MessageType.RequestPublicDashboard, null);
                    Client.SendMessage(MessageType.RequestPrivateDashboard, null);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void mapSetupControl_SaveMaps(object sender, MapDoc _doc)
        {
            try
            {
                if (_doc != null)
                {
                    if (Client != null)
                        Client.SendMessage(MessageType.MapDoc, _doc);

                    if (mapDoc != null)
                        mapDoc.Dispose();
                    mapDoc = _doc;
                    mapControl.Map_Doc = _doc;
                    mapControl.LoadMap("");

                    LoadMapTree(_doc);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }
        #endregion  Save docs

        #region Connection States
        private void client_ConnectionStatusChanged(object sender, Client.ConnectState connectionState)
        {
            try
            {
                if (connectionState == Client.ConnectState.Connected)
                {
                    if (CurrentUser == null)
                        Authenticate();
                    else if (!(CurrentUser.Authenticated))
                        Client.SendMessage(MessageType.UserLogin, CurrentUser);
                }
                else if (connectionState == Client.ConnectState.Disconnected)
                {
                    CurrentUser.Authenticated = false;
                    Authenticate("Server not available.");
                }

                SetConnectionStatus(statusConnected, connectionState);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void SetConnectionStatus(object sender, Client.ConnectState connectionState)
        {
            try
            {
                if (CheckAccess())
                {
                    statusBar.Visibility = Visibility.Visible;
                    if (sender is TextBlock)
                    {
                        ((TextBlock)sender).Text = Enum.GetName(typeof(Client.ConnectState), connectionState);

                        if (connectionState == Client.ConnectState.Disconnected)
                        {
                            statusDBConnected.Text = "Server disconnected";
                            statusServer.Text = "";
                        }
                    }
                }
                else
                    Dispatcher.Invoke(() => SetConnectionStatus(sender, connectionState));                
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        //private delegate void DBConnectionStatusDelegate(object sender, ConnectionState connectionState);
        private void SetDBConnectionStatus(object sender, ConnectionState connectionState)
        {
            try
            {
                if (CheckAccess())
                {
                    statusBar.Visibility = Visibility.Visible;
                    if (sender is TextBlock)
                    {
                        ((TextBlock)sender).Text = Enum.GetName(typeof(ConnectionState), connectionState);
                    }
                }
                else
                    Dispatcher.Invoke(() => SetDBConnectionStatus(sender, connectionState));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }
        #endregion Connection States

        #region Auth
        public void Authenticate(string message = "")
        {
            if (CheckAccess())
            {
                WindowState = WindowState.Minimized;
                try
                {
                    AuthenticateSP(message);
                    //if (newWindowThread != null)
                    //    newWindowThread.Abort();
                    //newWindowThread = new Thread(new ThreadStart(() => AuthenticateSP(message)));
                    //newWindowThread.SetApartmentState(ApartmentState.STA);
                    //newWindowThread.IsBackground = true;
                    //newWindowThread.Start();
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
                }
            }
            else
            {
                Dispatcher.Invoke(delegate () { Authenticate(message); });
            }
        }

        private void AuthenticateSP(string message)
        {
            AuthenticateWindow authW = null;
            try
            {
                authW = new AuthenticateWindow();
                if (!string.IsNullOrWhiteSpace(message))
                {
                    authW.Title = message;
                }
                authW.ServerIpAddress = serverDetails.IpAddress;
                authW.ServerPortNo = serverDetails.PortNo;
                authW.serverDetailsChanged = false;
                if (authW.ShowDialog() == true)
                {
                    if ((authW.ServerIpAddress != serverDetails.IpAddress) || (authW.ServerPortNo != serverDetails.PortNo))
                    {
                        serverDetails.IpAddress = authW.ServerIpAddress;
                        serverDetails.PortNo = authW.ServerPortNo;
                    }
                    CurrentUser = new User(authW.Username, authW.Password);
                    if (authW.serverDetailsChanged) Client.SaveServerDetails(serverDetails);
                    //if (authW.Username == "etheleAdmin")
                    //{
                    //    if (authW.Password[5] == '⼎')
                    //    {
                    //        CurrentUser.Authenticated = true;
                    //    }
                    //}
                    if (Client.ConnectionStatus == Client.ConnectState.Connected)
                    {
                        Client.SendMessage(MessageType.UserLogin, CurrentUser);
                    }
                    else if (Client.ConnectionStatus == Client.ConnectState.Disconnected)
                    {
                        Client.Connect(serverDetails.IpAddress, serverDetails.PortNo);
                    }
                }
                else
                {
                    CloseMe();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Error while authenticating", ex);
            }
            finally
            {
                if (authW != null)
                {
                    authW.Close();
                    authW = null;
                }
            }
            // System.Windows.Threading.Dispatcher.Run();
        }

        public static bool CurrentUserAuthenticated
        {
            get
            {
                return CurrentUser != null ? CurrentUser.Authenticated : false;
            }
        }
        #endregion Auth

        #region Messages
        private void client_OnStringMessageReceived(object sender, CommsNode.StringMessageEventArgs e)
        {
            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                MethodBase.GetCurrentMethod().Name), "String messages not supported.");
        }

        private void client_OnObjectMessageReceived(object sender, CommsNode.ObjectMessageEventArgs e)
        {
            try
            {
                switch (e.Message_Type)
                {
                    case MessageType.StatusMessage:
                       if (e.Message is StatusMessage)
                            statusMessageControl.AddMessage((StatusMessage)e.Message);
                        break;
                    case MessageType.StatusMessageRemove:
                        if (e.Message is StatusMessage)
                            statusMessageControl.RemoveMessage((StatusMessage)e.Message);
                        break;
                    case MessageType.ServerVersion:
                        Version serverVersion = e.Message as Version;
                        if (serverVersion != null)
                            SetServerVersion(serverVersion);
                        break;
                    case MessageType.ServerID:
                        if(e.Message is Guid)
                            SetServerID((Guid)e.Message);
                        break;
                    case MessageType.ServerName:
                        string serverName = e.Message as string;
                        if (serverName != null)
                            SetServerName(serverName);
                        break;
                    case MessageType.LogMessage:
                        logControl.AddItem(e.Message);
                        break;
                    /*else if ((e.Message_Type == MessageType.UserLoggedOn) && (!CurrentUserAuthenticated))
                    {
                        var loggedOnUser = (User)e.Message;
                        if (loggedOnUser != null)
                        {
                            if (loggedOnUser.UserName == CurrentUser.UserName)
                            {
                                CurrentUser = loggedOnUser;
                                //CurrentUser.Rights = loggedOnUser.Rights; // Could be redundant. Redundant.
                                if (CurrentUser.Authenticated)
                                {
                                    PopulateTabs();
                                }
                            }
                        }
                    }*/
                    case MessageType.UserLoginResult:
                        {
                            LoginResult loginResult = (LoginResult)e.Message;
                            if (loginResult == null)
                            {
                                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                                    MethodBase.GetCurrentMethod().Name), "Null login result - unexpected message from server.");
                            }
                            else if ((loginResult.User != null) && (loginResult.User.Authenticated))
                            {
                                CurrentUser = loginResult.User;
                                LoadInitialData();
                                PopulateTabs();
                            }
                            else if (loginResult.Message.Contains("deleted") || loginResult.Message == "Invalid password." || loginResult.Message == "Rights error.")
                            {
                                Authenticate(loginResult.Message);
                            }
                            else if (loginResult.Message.Contains("Authentic"))
                            {
                                // Prompt for all unsuccessful logins
                                systemConfig.ChangePassword(loginResult.Message);
                            }
                            else
                                Authenticate(loginResult.Message);
                                break;
                        }
                    case MessageType.CameraLayouts:
                        {
                            VideoHostConfiguration videoConfiguration = e.Message as VideoHostConfiguration;
                            if (videoConfiguration != null)
                            {
                                AddVideoWindow(videoConfiguration);
                            }

                            break;
                        }
                    default:
                        if (CurrentUserAuthenticated) //You can't receive any other data before you are authenticated.
                        {
                            switch (e.Message_Type)
                            {
                                #region Event
                                case MessageType.Event:
                                    {
                                        Event newEvent = (Event)e.Message;
                                        publicDashboardControl.ProcessEvent(newEvent);
                                        privateDashboardControl.ProcessEvent(newEvent);
                                    }
                                    break;
                                #endregion

                                #region Alarm
                                case MessageType.Alarm:
                                    {
                                        Alarm newAlarm = (Alarm)e.Message;

                                        //AddAlarm<Alarm>(alarmStack, Alarm);
                                    }
                                    break;

                                case MessageType.GroupAlarm:
                                    {
                                        GroupAlarm newAlarm = (GroupAlarm)e.Message;

                                        AddAlarm<GroupAlarm>(alarmStack, newAlarm);

                                        if (mapControl != null && newAlarm != null)
                                            mapControl.FlashDetector(newAlarm);
                                    }
                                    break;

                                case MessageType.GroupAlarmUpdate:
                                    {
                                        if (e.Message is GroupAlarmUpdate)
                                        {
                                            AlarmUpdate((GroupAlarmUpdate)e.Message);
                                        }
                                        else if (e.Message is List<GroupAlarmUpdate>)
                                        {
                                            foreach (GroupAlarmUpdate update in (List<GroupAlarmUpdate>)e.Message)
                                                AlarmUpdate(update);
                                        }
                                    }
                                    break;

                                case MessageType.AlarmPropertyUpdate:
                                case MessageType.GroupAlarmPropertyUpdate:
                                    {
                                        PropertyUpdate alarmUpdate = (PropertyUpdate)e.Message;

                                        foreach (AlarmBase alarm in alarmStack)
                                        {
                                            if (alarm.ID == alarmUpdate.Id)
                                            {
                                                try
                                                {
                                                    alarm.GetType().GetProperty(alarmUpdate.PropertyName).SetValue(alarm, alarmUpdate.Value);
                                                }
                                                catch (Exception ex)
                                                {
                                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                                                        MethodBase.GetCurrentMethod().Name), "Failed to update Alarm property.", ex);
                                                }

                                                break;
                                            }
                                        }
                                    }
                                    break;
                                #endregion Alarm

                                #region AlarmComment
                                case MessageType.AlarmComment:
                                    {
                                        AlarmComment comment = e.Message as AlarmComment;

                                        if (comment != null)
                                        {
                                            AddAlarmComment(comment);
                                        }
                                    }
                                    break;
                                #endregion

                                #region Alarm Siren
                                case MessageType.Siren:
                                    PlaySound((SirenAction)e.Message);
                                    break;
                                #endregion

                                #region AlarmImages
                                case MessageType.AlarmImages:
                                    {
                                        List<EventImage> alarmImages = (List<EventImage>)e.Message;
                                        foreach (EventImage img in alarmImages)
                                        {
                                            AddAlarmImage(alarmStack, img);
                                            AddAlarmImage(alarmHistoryItems, img);
                                        }
                                    }
                                    break;

                                case MessageType.AlarmImageUpdate:
                                    {
                                        EventImageUpdate img = e.Message as EventImageUpdate;
                                        if (img != null)
                                        {
                                            AlarmImageUpdate(img);
                                        }
                                    }
                                    break;

                                case MessageType.AlarmImage:
                                    {
                                        EventImage img = e.Message as EventImage;
                                        if (img != null)
                                        {
                                            AddAlarmImage(alarmStack, img);
                                        }
                                    }
                                    break;
                                #endregion AlarmImages

                                #region CameraSnapshot
                                case MessageType.CameraSnapshot:
                                    if (CheckAccess())
                                        ShowImage(this, (CameraImage)e.Message);
                                    else
                                        Dispatcher.Invoke(() => { ShowImage(this, (CameraImage)e.Message); });
                                    break;
                                #endregion CameraSnapshot

                                #region AlarmResolve
                                case MessageType.AlarmResolve:
                                    {
                                        AlarmResolve resolveDetailItem = (AlarmResolve)e.Message;

                                        AlarmBase alarm = FindAlarmInHistory(resolveDetailItem.AlarmID);
                                        if (alarm != null)
                                        {
                                            alarm.Resolve(resolveDetailItem);
                                        }
                                    }
                                    break;

                                case MessageType.GroupAlarmResolve:
                                    AlarmResolve groupAlarmResolve = (AlarmResolve)e.Message;
                                    GroupAlarm groupAlarm = FindAlarmInStack(groupAlarmResolve.AlarmID);
                                    if (groupAlarm != null)
                                    {
                                        groupAlarm.Resolve(groupAlarmResolve);
                                        RemoveAlarm<GroupAlarm>(alarmStack, groupAlarm, true);
                                    }
                                    break;
                                #endregion AlarmResolve

                                #region SiteInstructionStep
                                case MessageType.SiteInstructionStep:
                                    {
                                        SiteInstructionStep stepMsg = (SiteInstructionStep)e.Message;

                                        AlarmBase alarm = FindAlarmInStack(stepMsg.AlarmID);
                                        if (alarm != null)
                                        {
                                            SiteInstructionStep step = alarm.GetSiteInstructionStep(stepMsg.SiteInstructionStepID);

                                            if (step != null)
                                            {
                                                step.Completed = stepMsg.Completed;
                                                step.User = stepMsg.User;
                                                step.CompletedDT = stepMsg.CompletedDT;
                                            }
                                            else
                                                alarm.AddSiteInstructionStep(stepMsg);
                                        }

                                        alarm = FindAlarmInHistory(stepMsg.AlarmID);
                                        if (alarm != null)
                                        {
                                            SiteInstructionStep step = alarm.GetSiteInstructionStep(stepMsg.SiteInstructionStepID);

                                            if (step != null)
                                            {
                                                step.Completed = stepMsg.Completed;
                                                step.User = stepMsg.User;
                                                step.CompletedDT = stepMsg.CompletedDT;
                                            }
                                            else
                                                alarm.AddSiteInstructionStep(stepMsg);
                                        }
                                    }
                                    break;
                                #endregion SiteInstructionStep

                                #region UnresolvedAlarms
                                case MessageType.UnresolvedAlarms:
                                    {
                                        if (e.Message is List<GroupAlarm>)
                                        {
                                            List<GroupAlarm> unresolvedAlarms = (List<GroupAlarm>)e.Message;

                                            //clear list.
                                            //RemoveAlarm<GroupAlarm>(alarmStack, null);

                                            foreach (GroupAlarm alarm in unresolvedAlarms)
                                            {
                                                AddAlarm<GroupAlarm>(alarmStack, alarm);

                                                if (mapControl != null && alarm != null)
                                                    mapControl.FlashDetector(alarm);
                                            }
                                        }
                                    }
                                    break;
                                #endregion UnresolvedAlarms

                                #region AlarmHistory
                                case MessageType.AlarmHistory:
                                    {
                                        if (e.Message is List<Alarm>)
                                        {
                                            List<Alarm> historyAlarms = (List<Alarm>)e.Message;

                                            //clear list.
                                            RemoveAlarm(alarmHistoryItems, null);

                                            foreach (Alarm alarm in historyAlarms)
                                            {
                                                AddAlarm<AlarmBase>(alarmHistoryItems, alarm);
                                            }
                                        }
                                    }
                                    break;
                                #endregion AlarmHistory

                                #region Occurrence Book

                                case MessageType.OccurenceBook:
                                    if (e.Message is OBEntry)
                                        occurrenceBookControl.AddOBEntry((OBEntry)e.Message);
                                    break;

                                case MessageType.OccurenceBookPropertyUpdate:
                                    if (e.Message is PropertyUpdate)
                                        occurrenceBookControl.OBPropertyUpdate((PropertyUpdate)e.Message);
                                    break;

                                case MessageType.OccurrenceBookHistory:
                                    if (e.Message is List<OccurrenceBook>)
                                        occurrenceBookControl.LoadOBHistory((List<OccurrenceBook>)e.Message);
                                    break;

                                case MessageType.OBTypes:
                                    if (e.Message is List<OBType>)
                                    {
                                        List<OBType> obTypes = (List<OBType>)e.Message;
                                        occurrenceBookControl.SetTypes(obTypes);
                                    }
                                    break;
                                #endregion Occurrence Book

                                #region DeviceDoc
                                case MessageType.DeviceDoc:
                                    {
                                        devicesDoc = (DevicesDoc)e.Message;
                                        devicesDoc.ReassignChildrenParents();
                                        mapControl.Devices_Doc = devicesDoc;
                                        mapControl.LoadMap(mapControl.CurrentMapName);

                                        //LoadMapTree(mapDoc);
                                        LoadOfflineDevices();

                                        UIPluginManager.DeviceDoc = devicesDoc;

                                        if ((CurrentUser.Rights.UserType == UserType.Admin) || (CurrentUser.Rights.UserType == UserType.Super))
                                        {
                                            deviceSetupControl.SetDevices(devicesDoc);

                                            conditionsControl.LoadDevicesandTriggers(devicesDoc);

                                            List<string> deviceNames = new List<string>();

                                            if (devicesDoc != null)
                                            {
                                                var deviceList = devicesDoc.DeviceList(true);

                                                if (deviceList != null && deviceList.Count > 0)
                                                {
                                                    foreach (var device in deviceList)
                                                        deviceNames.Add(device.Item1);
                                                }
                                            }
                                            actionsControl.PopulatePlugins(deviceNames);
                                        }

                                        Client.SendMessage(MessageType.RequestDevicesOnline, null);
                                        Client.SendMessage(MessageType.RequestActionsDoc);
                                        Client.SendMessage(MessageType.RequestAlarmNames);
                                        Client.SendMessage(MessageType.RequestAlarmSchedule);
                                        Client.SendMessage(MessageType.RequestConditionsDoc);
                                        Client.SendMessage(MessageType.CameraLayouts, CurrentUser);
                                    }
                                    break;
                                #endregion DeviceDoc

                                #region ConditionsDoc
                                case MessageType.ConditionsDoc:
                                    if (e.Message is XMLDoc)
                                        if ((CurrentUser.Rights.UserType == UserType.Admin) || (CurrentUser.Rights.UserType == UserType.Super))
                                        {
                                            XMLDoc conditionsDoc = (XMLDoc)e.Message;
                                            conditionsControl.SetConditions(conditionsDoc);
                                            deviceSetupControl.SetConditions(conditionsDoc);
                                        }
                                    break;
                                #endregion ConditionsDoc

                                #region ActionsDoc
                                case MessageType.ActionsDoc:
                                    if (e.Message is XMLDoc)
                                        if ((CurrentUser.Rights.UserType == UserType.Admin) || (CurrentUser.Rights.UserType == UserType.Super))
                                        {
                                            XMLDoc actionsDoc = (XMLDoc)e.Message;
                                            actionsControl.SetActions(actionsDoc);
                                            deviceSetupControl.SetActions(actionsDoc);
                                        }
                                    break;
                                #endregion ActionsDoc

                                #region MapDoc
                                case MessageType.MapDoc:
                                    {
                                        if (mapDoc != null)
                                            mapDoc.Dispose();

                                        mapDoc = (MapDoc)e.Message;
                                        if (mapDoc != mapControl.Map_Doc)
                                        {
                                            mapControl.Map_Doc = mapDoc;
                                            mapControl.LoadMap("");

                                            if ((CurrentUser.Rights.UserType == UserType.Admin) || (CurrentUser.Rights.UserType == UserType.Super))
                                                mapSetupControl.SetMap(mapDoc);

                                            LoadMapTree(mapDoc);
                                        }
                                    }
                                    break;
                                #endregion MapDoc

                                #region Dashboards
                                case MessageType.PublicDashboard:
                                    {
                                        eNerve.DataTypes.Gauges.Dashboard dashboard = e.Message as eNerve.DataTypes.Gauges.Dashboard;
                                        if (dashboard != null)
                                            publicDashboardControl.SetDashboard(dashboard);
                                    }
                                    break;
                                case MessageType.PrivateDashboard:
                                    {
                                        eNerve.DataTypes.Gauges.Dashboard dashboard = e.Message as eNerve.DataTypes.Gauges.Dashboard;
                                        if (dashboard != null)
                                            privateDashboardControl.SetDashboard(dashboard);
                                    }
                                    break;
                                #endregion

                                #region AlarmNames
                                case MessageType.AlarmNames:
                                    if (e.Message is List<string>)
                                    {
                                        List<string> names = (List<string>)e.Message;
                                        alarmScheduleControl.SetAlarmNames(names);
                                        actionsControl.SetAlarmNames(names);
                                        actionsControl.SetActions();
                                        publicDashboardControl.SetEventNames(names);
                                        privateDashboardControl.SetEventNames(names);
                                        guiDashboardControl.SetEventNames(names);
                                    }
                                    break;
                                #endregion AlarmNames

                                #region Schedule
                                case MessageType.AlarmSchedule:
                                    if (e.Message is List<Schedule>)
                                        if ((CurrentUser.Rights.UserType == UserType.Admin) || (CurrentUser.Rights.UserType == UserType.Super))
                                            alarmScheduleControl.SetSchedule((List<Schedule>)e.Message);
                                    break;
                                #endregion Schedule

                                #region OnlineDeviceList
                                case MessageType.OnlineDeviceList:
                                    {
                                        List<RioDevice> devices = (List<RioDevice>)e.Message;
                                        LoadOnlineDeviceList(listDevices, devices);
                                    }
                                    break;
                                #endregion OnlineDeviceList

                                #region DeviceStatus
                                //case MessageType.DeviceOnline:
                                //    {
                                //        string device = (string)e.Message;
                                //        UpdateDeviceStatus(listDevices, new RioDevice() { Name = device, Status = PluginStatus.Online });
                                //    }
                                //    break;                        

                                //case MessageType.DeviceOffline:
                                //    {
                                //        string device = (string)e.Message;
                                //        UpdateDeviceStatus(listDevices, new RioDevice() { Name = device, Status = PluginStatus.Offline });
                                //    }
                                //    break;                        

                                case MessageType.DeviceStatus:
                                    {
                                        RioDevice deviceStatus = e.Message as RioDevice;
                                        if (deviceStatus != null)
                                            UpdateDeviceStatus(listDevices, deviceStatus);
                                    }
                                    break;
                                #endregion

                                #region Plugins
                                case MessageType.PluginTypes:
                                    var pluginTypeList = e.Message as ObservableCollection<INervePlugin>;
                                    if ((pluginTypeList != null) && (pluginTypeList.Count > 0))
                                    {// Ensure something exists before trying to do something with nothing.
                                        // Busy changing - don't panic if there is a null reference here.
                                        LoadPluginTypes(pluginTypeList);

                                        Client.SendMessage(MessageType.RequestDeviceDoc);
                                    }
                                    //Client.SendMessage(MessageType.PluginList);
                                    break;

                                //deprecated
                                case MessageType.PluginList:
                                    var pluginList = e.Message as ObservableCollection<INervePlugin>;
                                    if ((pluginList != null) && (pluginList.Count > 0))
                                    {
                                        deviceSetupControl.PopulatePlugins(pluginList);

                                        List<string> pluginNames = new List<string>();

                                        foreach (var plugin in pluginList)
                                            pluginNames.Add(plugin.Name);

                                        actionsControl.PopulatePlugins(pluginNames);
                                    }
                                    break;

                                #endregion

                                #region Users

                                #region UsersOnline
                                case MessageType.UsersOnline:
                                    {
                                        List<User> users = (List<User>)e.Message;
                                        if (CheckAccess())
                                        {
                                            LoadUsersList(listUsers, users);
                                        }
                                        else
                                        {
                                            Dispatcher.Invoke(new UserListDelegate(LoadUsersList), new object[] { listUsers, users });
                                        }
                                    }
                                    break;
                                #endregion UsersOnline

                                #region UserLoggedOn
                                case MessageType.UserLoggedOn:
                                    {// Notification to all clients that a user has logged on to the system.
                                        User user = (User)e.Message;
                                        if (user != null)
                                        {

                                            if (CheckAccess())
                                            {
                                                AddUser(listUsers, user);
                                            }
                                            else
                                            {
                                                Dispatcher.Invoke(new UserDelegate(AddUser), new object[] { listUsers, user });
                                            }
                                        }
                                    }
                                    break;
                                #endregion UserLoggedOn

                                #region UserLoggedOff
                                case MessageType.UserLoggedOff:
                                    {
                                        User user = (User)e.Message;
                                        if (user != null)
                                        {
                                            if (CheckAccess())
                                            {
                                                RemoveUser(listUsers, user);
                                            }
                                            else
                                            {
                                                Dispatcher.Invoke(new UserDelegate(RemoveUser), new object[] { listUsers, user });
                                            }
                                            if (CurrentUser.Equals(user))
                                            {
                                                CurrentUser.Authenticated = false;
                                                ClearAllAlarms();
                                                Authenticate("User has been logged off from the server.");
                                            }
                                        }
                                    }
                                    break;
                                #endregion UserLoggedOff

                                #region Create User
                                case MessageType.CreateUser:
                                    if ((bool)e.Message)
                                    {// Success - gather user rights.
                                        if (NewUser != null)
                                        {
                                            if (CheckAccess())
                                            {
                                                DisplayNewUser();
                                            }
                                            else
                                            {
                                                Dispatcher.Invoke(DisplayNewUser);
                                            }
                                        }
                                    }
                                    else
                                    {// Failed creating user - remove.
                                        if (NewUser != null)
                                        {
                                            if (CheckAccess())
                                            {
                                                systemConfig.ExistingUsers.Remove(NewUser);
                                            }
                                            else
                                            {
                                                Dispatcher.Invoke(delegate { systemConfig.ExistingUsers.Remove(NewUser); });
                                            }
                                            NewUser = null;
                                        }
                                    }
                                    break;
                                #endregion Create User

                                #region User Logout
                                case MessageType.UserLogout:
                                    // User has been deleted - if logged in, logout. In all cases remove from existing users.
                                    User deletedUser = e.Message as User;
                                    if (deletedUser != null)
                                    {
                                        if ((CurrentUser != null) && (CurrentUser.UserName == deletedUser.UserName))
                                        {// Deleted user is logged in on this machine - logout.
                                            if (CurrentUser.Authenticated)
                                                Client.SendMessage(MessageType.UserLogout, CurrentUser);
                                            CurrentUser.Authenticated = false;
                                            ClearAllAlarms();
                                            Authenticate();
                                        }
                                        if (systemConfig.ExistingUsers != null)
                                        {
                                            if (CheckAccess())
                                            {
                                                systemConfig.ExistingUsers.Remove(deletedUser);
                                            }
                                            else
                                            {
                                                Dispatcher.Invoke(delegate { systemConfig.ExistingUsers.Remove(deletedUser); });
                                            }
                                        }
                                    }
                                    break;
                                #endregion Users Logout

                                #region Delete user
                                case MessageType.DeleteUser:
                                    LoginResult deleteUserResult = e.Message as LoginResult;
                                    if (deleteUserResult != null)
                                    {
                                        statusMessageControl.AddMessage(new StatusMessage()
                                        {
                                            StatusID = Guid.NewGuid(),
                                            Message = "Successfully deleted user: " + deleteUserResult.User.UserName,
                                            MessageTimeStamp = DateTime.Now,
                                            Animation = false,
                                            AutoRemove = TimeSpan.FromSeconds(5)
                                        });
                                        /*MessageBox.Show();
                                
                                        else */
                                        if (deleteUserResult.User.Rights.ToString() == "0")
                                        {// Deleted user - show confirmation message
                                            if (CheckAccess())
                                            {
                                                systemConfig.DisplayUserRights(deleteUserResult.User);
                                            }
                                            else
                                            {
                                                Dispatcher.Invoke(new DisplayUserRightsDelegate(systemConfig.DisplayUserRights), deleteUserResult.User);
                                            }
                                        }
                                        else
                                        {
                                            statusMessageControl.AddMessage(new StatusMessage()
                                            {
                                                StatusID = Guid.NewGuid(),
                                                Message = "Error: Could not delete user: " + deleteUserResult.User.UserName,
                                                MessageTimeStamp = DateTime.Now,
                                                Animation = false,
                                                AutoRemove = TimeSpan.FromSeconds(5)
                                            });
                                            //MessageBox.Show();
                                        }
                                        if (deleteUserResult.User == CurrentUser)
                                        {
                                            CurrentUser.Authenticated = false;
                                            ClearAllAlarms();
                                            Authenticate(deleteUserResult.Message);
                                        }
                                    }
                                    break;

                                #endregion Delete user

                                #region User list

                                case MessageType.UserList:
                                    {
                                        if (CheckAccess())
                                        {
                                            DisplayUserList(e.Message as List<User>);
                                        }
                                        else
                                        {
                                            Dispatcher.Invoke(new DisplayUsersDelegate(DisplayUserList), e.Message as List<User>);
                                        }
                                    }
                                    break;

                                #endregion

                                #region User Rights
                                case MessageType.UserRights:
                                    {
                                        if (e.Message is User)
                                        {
                                            if ((NewUser != null) && (NewUser.UserName == (e.Message as User).UserName))
                                            {// Creating a new user
                                                DisplayUser = null;
                                                NewUser = (User)e.Message;
                                                if (CheckAccess())
                                                {
                                                    systemConfig.DisplayUserRights(NewUser);
                                                    //btnSaveUser.Visibility = Visibility.Visible;
                                                }
                                                else
                                                {
                                                    Dispatcher.Invoke(new DisplayUserRightsDelegate(systemConfig.DisplayUserRights), NewUser);
                                                }
                                            }
                                            else
                                            {// Displaying an existing user
                                                DisplayUser = (User)e.Message;
                                                if (CheckAccess())
                                                {
                                                    systemConfig.DisplayUserRights(DisplayUser);
                                                }
                                                else
                                                {
                                                    Dispatcher.Invoke(new DisplayUserRightsDelegate(systemConfig.DisplayUserRights), DisplayUser);
                                                }
                                            }
                                        }
                                    }
                                    break;

                                #endregion User Rights

                                #region Change Password
                                case MessageType.ChangePassword:
                                    if (e.Message is string)
                                    {
                                        statusMessageControl.AddMessage(new StatusMessage()
                                        {
                                            StatusID = Guid.NewGuid(),
                                            Message = (string)e.Message,
                                            MessageTimeStamp = DateTime.Now,
                                            Animation = false,
                                            AutoRemove = TimeSpan.FromSeconds(5)
                                        });
                                        //MessageBox.Show((string)e.Message);
                                    }
                                    break;
                                #endregion Change Password

                                #endregion Users

                                #region DatabaseConnectionState
                                case MessageType.DatabaseConnectionState:
                                    ConnectionState connectionState = (ConnectionState)e.Message;

                                    SetDBConnectionStatus(statusDBConnected, connectionState);

                                    if (connectionState == ConnectionState.Open && dbWindow != null)
                                    {
                                        dbWindow.btnSubmit.IsEnabled = true;
                                    }
                                    break;
                                #endregion DatabaseConnectionState

                                #region SendDBSettings
                                case MessageType.SendDBSettings:
                                    DisplayDBSettings(e.Message, false);
                                    break;
                                case MessageType.RequestDBSettings:// DB settings for generating reports.
                                    reportControl.DatabaseDetails = e.Message as ConnectionDetails;
                                    break;
                                #endregion SendDBSettings

                                #region DBTestResult
                                case MessageType.DBTestResult:
                                    ConnectionDetails testedCredentials = (ConnectionDetails)e.Message;
                                    if (testedCredentials.Valid)
                                    // Credentials connect successfully to database
                                    {
                                        DisplayDBSettings(e.Message, true);
                                    }
                                    else
                                    // These don't work
                                    {
                                        DisplayDBSettings(e.Message, false);
                                    }
                                    break;
                                #endregion DBTestResult

                                #region ForceLogoff
                                case MessageType.ForceLogoff:
                                    if (CurrentUser != null && e.Client != null)
                                        if (CurrentUser.Authenticated)
                                            Client.SendMessage(MessageType.UserLogout, CurrentUser);

                                    CurrentUser.Authenticated = false;
                                    ClearAllAlarms();
                                    Authenticate();
                                    break;
                                #endregion ForceLogoff

                                #region Root cause
                                case MessageType.RootCauseServerAddress:
                                    IPEndPoint rootCauseAddress = e.Message as IPEndPoint;
                                    if(rootCauseAddress != null)
                                    {
                                        rootCauseConfigControl.SetServerAddres(rootCauseAddress);
                                    }
                                    break;

                                case MessageType.RootCauseConditionsDoc:
                                    AlarmConditionsDoc doc = e.Message as AlarmConditionsDoc;
                                    if (doc != null)
                                        rootCauseConfigControl.SetConditions(doc);
                                    break;
                                #endregion

                                case MessageType.ReportList:
                                    if (e.Message != null)
                                    {
                                        var list = e.Message as List<ReportFile>;
                                        if (list != null)
                                        {
                                            reportControl.ShowReports(list);
                                        }// TODO cache 'reports loading' message so that it can be removed later. Generic.
                                    }
                                    break;

                                default:
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                                        MethodBase.GetCurrentMethod().Name), "Unknown message type received: " + e.Message_Type, null);
                                    break;
                            }
                        }

                        break;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void AddVideoWindow(VideoHostConfiguration videoConfiguration)
        {
            if (cameraWindows == null)
                cameraWindows = new List<VideoWindow.MainWindow>();
            else
            {// Test existing windows to avoid multiple windows with the same name.
                var existingWindow = cameraWindows.Find(w => w.Title == videoConfiguration.Name);
                if (existingWindow != null)
                {
                    MessageBox.Show("Please specify a unique name for the new camera window.");
                    return;
                }
            }
            Dispatcher.Invoke(() =>
            {
                VideoWindow.MainWindow newVideoWindow = new VideoWindow.MainWindow(videoConfiguration.Name, CurrentUser, videoConfiguration.CameraLayout, Client.ServerConfig,
                    videoConfiguration.CameraPositions);
                newVideoWindow.Show();
                newVideoWindow.Closed += VideoHost_Closing;
                cameraWindows.Add(newVideoWindow);
            });
        }

        private void DisplayNewUser()
        {
            DisplayUser = NewUser;
            NewUser = null;
            systemConfig.ExistingUsers.Add(DisplayUser);
            systemConfig.boxUserList.SelectedItem = DisplayUser;
        }
        #endregion Messages

        #region Sounds
        private void PlaySound(SirenAction _sirenAction = null)
        {
            if(_sirenAction != null)
            {
                try
                {
                    if (soundPlayer == null)
                        soundPlayer = new SoundPlayer();

                    soundPlayer.PlaySound(_sirenAction.SoundFile, _sirenAction.Duration);
                }
                catch(Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);

                    System.Media.SystemSounds.Asterisk.Play();
                }
            }
            else
                System.Media.SystemSounds.Asterisk.Play();
        }
        #endregion

        private void SetTitle()
        {
            if (Dispatcher.CheckAccess())
            {
                Title = string.Format("RioSoft Control Room - Version: {0} - Server version: {1}", Assembly.GetExecutingAssembly().GetName().Version.ToString(2), ServerVersion.ToString(2));
            }
            else
                Dispatcher.Invoke(() => SetTitle());
        }

        private void SetServerVersion(Version serverVersion)
        {
            ServerVersion = serverVersion;
            SetTitle();
        }

        private void SetServerID(Guid serverID)
        {
            ServerID = serverID;
        }

        private void SetServerName(string serverName)
        {
            if (Dispatcher.CheckAccess())
            {
                ServerName = serverName;

                statusServer.Text = string.Format("to {0}", serverName);
            }
            else
                Dispatcher.Invoke(() => SetServerName(serverName));
        }

        private void LoadPluginTypes(ObservableCollection<INervePlugin> pluginTypeList)
        {
            if (CheckAccess())
            {
                /*if (pluginsControl != null)
                {
                    pluginsControl.lbPluginTypes.DataContext = pluginTypeList;
                }*/
                if (deviceSetupControl != null)
                {
                    deviceSetupControl.PopulatePluginTypes(pluginTypeList);
                }                
            }
            else
            {
                Dispatcher.Invoke(() => LoadPluginTypes(pluginTypeList));
            }
        }

        private void PopulateTabs()
        {
            try
            {
                if (Dispatcher.CheckAccess())
                {
                    infoColumn.MinWidth = 100;
                    infoColumn.Width = new GridLength(250);
                    WindowState = WindowState.Maximized;
                    if ((CurrentUser.Rights.UserType == UserType.Admin) || (CurrentUser.Rights.UserType == UserType.Super))
                    {// Todo: Add other tabs that should be hidden from normal users
                        logControl.IsEnabled = true;
                        logControl.Visibility = Visibility.Visible;
                        tabActions.IsEnabled = true;
                        tabActions.Visibility = Visibility.Visible;
                        tabAlarmSchedule.IsEnabled = true;
                        tabAlarmSchedule.Visibility = Visibility.Visible;
                        tabConditions.IsEnabled = true;
                        tabConditions.Visibility = Visibility.Visible;
                        tabDeviceSetup.IsEnabled = true;
                        tabDeviceSetup.Visibility = Visibility.Visible;
                        tabMapSetup.IsEnabled = true;
                        tabMapSetup.Visibility = Visibility.Visible;
                        tabOccurrenceBook.IsEnabled = true;
                        tabOccurrenceBook.Visibility = Visibility.Visible;
                        tabSystemConfig.IsEnabled = true;
                        tabSystemConfig.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        logControl.IsEnabled = false;
                        logControl.Visibility = Visibility.Collapsed;
                        tabActions.IsEnabled = false;
                        tabActions.Visibility = Visibility.Collapsed;
                        tabAlarmSchedule.IsEnabled = false;
                        tabAlarmSchedule.Visibility = Visibility.Collapsed;
                        tabConditions.IsEnabled = false;
                        tabConditions.Visibility = Visibility.Collapsed;
                        tabDeviceSetup.IsEnabled = false;
                        tabDeviceSetup.Visibility = Visibility.Collapsed;
                        tabMapSetup.IsEnabled = false;
                        tabMapSetup.Visibility = Visibility.Collapsed;
                        tabOccurrenceBook.IsEnabled = true;
                        tabOccurrenceBook.Visibility = Visibility.Visible;
                        tabSystemConfig.IsEnabled = false;
                        tabSystemConfig.Visibility = Visibility.Collapsed;
                    }
                    Show();
                    Activate();
                    Focus();
                }
                else
                    Dispatcher.Invoke(PopulateTabs);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        
        #region Alarms
        private GroupAlarm FindAlarmInStack(Guid _alarmID)
        {
            GroupAlarm result = null;

            try
            {
                for (int i = alarmStack.Count - 1; i >= 0; i--)
                {
                    GroupAlarm alarm = alarmStack[i];

                    if (alarm.ID == _alarmID)
                    {
                        result = alarm;

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }

            return result;
        }

        private AlarmBase FindAlarmInHistory(Guid _alarmID)
        {
            AlarmBase result = null;

            try
            {
                for (int i = alarmHistoryItems.Count - 1; i >= 0; i--)
                {
                    AlarmBase alarm = alarmHistoryItems[i];

                    if (alarm.ID == _alarmID)
                    {
                        result = alarm;

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }

            return result;
        }

        private void ClearAllAlarms()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (alarmStack != null)
                    {
                        for (int i = alarmStack.Count - 1; i >= 0; i--)
                        {
                            GroupAlarm alarm = alarmStack[i];
                            alarmStack.Remove(alarm);
                            alarm.Dispose();
                        }
                    }
                    if (alarmHistoryItems != null)
                    {
                        for (int i = alarmHistoryItems.Count - 1; i >= 0; i--)
                        {
                            AlarmBase alarm = alarmHistoryItems[i];
                            alarmHistoryItems.Remove(alarm);
                            alarm.Dispose();
                        }
                    }
                }
                else
                {
                    this.Dispatcher.Invoke(ClearAllAlarms);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void AddAlarm<T>(ObservableCollection<T> list, T alarm)
        {
            try
            {
                if (this.CheckAccess())
                {
                    int alarmIndex = list.IndexOf(alarm);

                    if (alarmIndex < 0)
                        list.Add(alarm);
                    else
                    {
                        var existingAlarm = list[alarmIndex];
                        if (existingAlarm != null && existingAlarm is IDisposable)
                            ((IDisposable)existingAlarm).Dispose();

                        list[alarmIndex] = alarm;
                    }

                    //PlaySound();
                }
                else
                    Dispatcher.Invoke(() => AddAlarm(list, alarm));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        /// <summary>
        /// Remove alarm from alarm list
        /// </summary>
        /// <param name="list">List to remove the alarm from</param>
        /// <param name="alarm">Null to clear list</param>
        private void RemoveAlarm<T>(ObservableCollection<T> list, T alarm, bool _dispose = true)
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (alarm != null)
                    {
                        if (list.Contains(alarm))
                            list.Remove(alarm);

                        if (_dispose && alarm is IDisposable)
                            ((IDisposable)alarm).Dispose();
                    }
                    else
                    {
                        for (int i = list.Count - 1; i >= 0; i--)
                        {
                            var tmpAlarm = list[i];
                            list.RemoveAt(i);
                            if (tmpAlarm is IDisposable)
                                ((IDisposable)tmpAlarm).Dispose();
                        }
                    }
                }
                else
                    this.Dispatcher.Invoke(() => RemoveAlarm(list, alarm, _dispose));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void AddAlarmComment(AlarmComment _Comment)
        {
            try
            {
                if (this.CheckAccess())
                {
                    foreach (AlarmBase alarm in alarmStack)
                    {
                        if (alarm.ID == _Comment.AlarmID)
                        {
                            alarm.AddComment(_Comment);
                        }
                    }                    
                }
                else
                    this.Dispatcher.Invoke((System.Action)(() => AddAlarmComment(_Comment)));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        //private delegate void CameraImageDelegate(object sender, CameraImage _cameraImage);
        private void AddAlarmImage(object sender, CameraImage _cameraImage)
        {
            try
            {
                if (this.Dispatcher.CheckAccess())
                {
                    if (sender is ObservableCollection<Alarm> && _cameraImage is EventImage)
                    {
                        EventImage alarmImage = (EventImage)_cameraImage;
                        ObservableCollection<Alarm> list = (ObservableCollection<Alarm>)sender;
                        Alarm tmpAlarm = new Alarm(alarmImage.EventID);
                        int index = list.IndexOf(tmpAlarm);
                        if (index >= 0)
                            list[index].AddImage(alarmImage);
                    }
                    else if (sender is ObservableCollection<GroupAlarm> && _cameraImage is EventImage)
                    {
                        EventImage alarmImage = (EventImage)_cameraImage;
                        ObservableCollection<GroupAlarm> list = (ObservableCollection<GroupAlarm>)sender;
                        foreach (GroupAlarm alarm in list)
                        {
                            if (alarm.ID == alarmImage.GroupID)
                            {
                                alarm.AddImage(alarmImage);

                                break;
                            }
                        }
                    }
                }
                else
                    this.Dispatcher.Invoke(() => { AddAlarmImage(sender, _cameraImage); });
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void AlarmImageUpdate(EventImageUpdate _imageUpdate)
        {
            try
            {
                if (this.Dispatcher.CheckAccess())
                {
                    bool isGroupAlarm = false;

                    foreach (Event e in alarmStack)
                    {
                        if (e.ID == _imageUpdate.EventID)
                        {
                            EventImageUpdate(e, _imageUpdate);

                            isGroupAlarm = true;

                            break;
                        }
                    }

                    if (!isGroupAlarm)
                    {
                        foreach (Event e in alarmHistoryItems)
                        {
                            if (e.ID == _imageUpdate.EventID)
                            {
                                EventImageUpdate(e, _imageUpdate);

                                break;
                            }
                        }
                    }
                }
                else
                    this.Dispatcher.Invoke(() => { AlarmImageUpdate(_imageUpdate); });
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private static void EventImageUpdate(Event _event, EventImageUpdate _imageUpdate)
        {
            foreach (EventImage img in _event.Images)
            {
                if (img.ID == _imageUpdate.ImageID)
                {
                    img.ImageData = _imageUpdate.ImageData;

                    break;
                }
            }
        }

        private void AlarmUpdate(GroupAlarmUpdate _update)
        {
            lock (alarmStack)
            {
                foreach (GroupAlarm alarm in alarmStack)
                {
                    if (alarm.ID == _update.ID)
                    {
                        if (this.Dispatcher.CheckAccess())
                            alarm.Update(_update);
                        else
                            this.Dispatcher.Invoke((System.Action)delegate() { alarm.Update(_update); });

                        if (mapControl != null && alarm != null)
                            mapControl.FlashDetector(alarm);

                        //PlaySound();

                        break;
                    }
                }

                //GroupAlarm alarm = alarmStack.First<GroupAlarm>(a => a.ID == update.ID);
                //if (alarm != null)
                //    alarm.Update(update.Triggers);
            }
        }

        #endregion Alarms

        #region DB Settings
        public void DisplayDBSettings(object message, bool tested)
        {
            try
            {
                Thread dbWindowThread = new Thread(() => DisplayDBWindow(message, tested));
                dbWindowThread.SetApartmentState(ApartmentState.STA);
                dbWindowThread.Start();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void DisplayDBWindow(object message, bool tested)
        {
            if (message != null) 
                try
                {
                    dbWindow = new DBSettingsWindow();
                    ConnectionDetails receivedDetails = (ConnectionDetails)message;
                    dbWindow.txtServername.Text = receivedDetails.Server;
                    dbWindow.txtDatabase.Text = receivedDetails.Database;
                    dbWindow.txtUserId.Text = receivedDetails.UserId;
                    dbWindow.txtPassword.Password = receivedDetails.Password;
                    dbWindow.btnTest.IsEnabled = false;
                    if ((receivedDetails.FailureReason != null) && (!string.IsNullOrWhiteSpace(receivedDetails.FailureReason)))
                        dbWindow.Title = receivedDetails.FailureReason;
                    if (tested) dbWindow.btnSubmit.IsEnabled = true;
                    if (dbWindow.ShowDialog().Value)
                    {
                        if (dbWindow.NewCredentials)
                        // 'Test' pressed.
                        {
                            ConnectionDetails newDbDetails = new ConnectionDetails();
                            newDbDetails.Database = dbWindow.Database;
                            newDbDetails.Server = dbWindow.Server;
                            newDbDetails.UserId = dbWindow.UserId;
                            newDbDetails.Password = dbWindow.Password;
                            newDbDetails.Valid = false;
                            Client.SendMessage(MessageType.TestDBSettings, newDbDetails);
                        }
                        else if (tested)
                        // 'Submit pressed'
                        {
                            if ((message as ConnectionDetails).Valid)
                                Client.SendMessage(MessageType.SendDBSettings, message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                        MethodBase.GetCurrentMethod().Name), "Error displaying DB settings", ex);
                }
                finally
                {
                    try
                    {
                        if (dbWindow != null)
                        {
                            dbWindow.Close();
                            dbWindow = null;
                            if (!CurrentUserAuthenticated) Authenticate();
                        }
                        /*if (this.CheckAccess())
                            btnUpdateDBSettings.IsEnabled = true;
                        else
                            btnUpdateDBSettings.Dispatcher.Invoke(new ButtonEnableStateDelegate(SetButtonEnabledStatus), new object[] { btnUpdateDBSettings, true });*/
                    }
                    catch (Exception ex)
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                            MethodBase.GetCurrentMethod().Name), "Error displaying DB settings", ex);
                    }
                }
        }

        private delegate void ButtonEnableStateDelegate(object sender, bool enabled);
        private void SetButtonEnabledStatus(object sender, bool enabled)
        {
            try
            {
                if (sender is Button)
                    ((Button)sender).IsEnabled = enabled;
                if (enabled) ((Button)sender).Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), "Error displaying DB settings", ex);
            }
        }            
        #endregion DB Settings   

        #region Maptree
        private delegate void MapNodeDelegate(XmlNode _mapNode, TreeViewItem _parentTreeviewItem);
        private void LoadMapTree(XmlNode _mapNode, TreeViewItem _parentTreeviewItem = null)
        {
            try
            {
                if (CheckAccess())
                {                    
                    if (_mapNode is MapDoc)
                    {
                        if (treeMap != null && treeMap.Items != null && treeMap.Items.Count > 0)
                        {
                            TreeViewItem selectedItem = treeMap.SelectedItem as TreeViewItem;
                            if (selectedItem != null)
                                selectedItem.IsSelected = false;

                            treeMap.Items.Clear();
                        }
                        _mapNode = ((MapDoc)_mapNode).GetHomeMap();
                    }
                    
                    TreeViewItem mapItem = new TreeViewItem();

                    string currentMapName = (_parentTreeviewItem != null ? _parentTreeviewItem.Tag + "_" : "") + _mapNode.Name;
                    string currentMapTitle = _mapNode["Title"] != null ? _mapNode["Title"].Value : currentMapName;
                    if (!string.IsNullOrEmpty(_mapNode.Value))
                    {
                        Dashboard dashboard = XMLSerializer.Deserialize(_mapNode.Value) as Dashboard;
                        if (dashboard != null)
                        {
                            currentMapTitle = dashboard.Title;
                            mapItem.DataContext = dashboard;
                        }
                    }
                    mapItem.Header = currentMapTitle;
                    mapItem.Tag = currentMapName;
                    mapItem.MouseDoubleClick += new MouseButtonEventHandler(mapTreeItem_MouseDoubleClick);

                    if (_mapNode["Maps"] != null)
                    {
                        foreach (XmlNode childMapNode in _mapNode["Maps"])
                        {
                            LoadMapTree(childMapNode, mapItem);
                        }
                    }

                    if (_parentTreeviewItem == null)
                    {
                        treeMap.Items.Add(mapItem);
                    }
                    else
                    {
                        _parentTreeviewItem.Items.Add(mapItem);
                    }
                }
                else
                {
                    Dispatcher.Invoke(delegate() { LoadMapTree(_mapNode, _parentTreeviewItem); });
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }              

        void mapTreeItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                e.Handled = true;

                TreeViewItem tvItem = sender as TreeViewItem;
                if (tvItem != null)
                {
                    if (tvItem.IsSelected)
                    {
                        if ((tvItem.DataContext != null) && (tvItem.DataContext.GetType() == typeof(Dashboard)))
                        {
                            mapControl.RemoveImages();
                            mapControl.Visibility = Visibility.Collapsed;
                            guiDashboardControl.Visibility = Visibility.Visible;
                            Dashboard dashboard = tvItem.DataContext as Dashboard;
                            dashboard.Node = mapDoc.GetMap((tvItem.Tag as string).Replace('_', '.'));
                            //dashboard.EventNames = guiDashboardControl.
                            guiDashboardControl.SetDashboard(dashboard);
                        }
                        else
                        {
                            mapControl.Visibility = Visibility.Visible;
                            guiDashboardControl.Visibility = Visibility.Collapsed;

                            string mapName = (string)tvItem.Tag;
                            mapControl.LoadMap(mapName.Replace('_', '.'));
                        }
                    }
                    tabCtrlMain.SelectedValue = tabControl;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        #endregion

        #region Users

        private void DisplayUserList(List<User> _users)
        {
            try
            {
                if (CheckAccess())
                {
                    systemConfig.ExistingUsers = new ObservableCollection<User>();
                    foreach (var user in _users)
                    {
                        systemConfig.ExistingUsers.Add(user);
                    }
                    systemConfig.boxUserList.DataContext = systemConfig.ExistingUsers;
                }
                else
                {
                    Dispatcher.Invoke(delegate { DisplayUserList(_users); });
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), "Error displaying user list", ex);
            }
        }

        private delegate void UserListDelegate(object sender, List<User> _users);
        private void LoadUsersList(object sender, List<User> _users)
        {
            try
            {
                if (sender is ItemsControl)
                {
                    ItemsControl listbox = (ItemsControl)sender;

                    listbox.Items.Clear();

                    if (_users != null)
                    {
                        foreach (User user in _users)
                        {
                            listbox.Items.Add(user);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private delegate void UserDelegate(object sender, User _user);

        private delegate void DisplayUsersDelegate(List<User> _users);

        private delegate void DisplayUserRightsDelegate(User _userRights);

        /// <summary>
        /// Add user to a list control
        /// </summary>
        /// <param name="sender">ItemsControl</param>
        /// <param name="_user">User to add</param>
        private void AddUser(object sender, User _user)
        {
            try
            {
                if (sender is ItemsControl)
                {
                    ItemsControl listbox = (ItemsControl)sender;

                    if (_user != null) //&& (!listbox.Items.Contains(_user)))
                    {
                        listbox.Items.Add(_user);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        /// <summary>
        /// Remove user that has been logged off.
        /// </summary>
        /// <param name="sender">ItemsControl</param>
        /// <param name="_user">User to remove</param>
        private void RemoveUser(object sender, User _user)
        {
            try
            {
                if (sender is ItemsControl)
                {
                    ItemsControl listbox = (ItemsControl)sender;

                    if (_user != null)
                    {
                        listbox.Items.Remove(_user);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        #endregion Users

        #region Online devices
        private void LoadOnlineDeviceList(object sender, List<RioDevice> _devices)
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (sender is ItemsControl)
                    {
                        ItemsControl listbox = (ItemsControl)sender;

                        if (_devices != null)
                        {
                            foreach (RioDevice device in _devices)
                            {
                                UpdateDeviceStatus(listDevices, device);
                                //AddDisplayDevice(listDevices, device, PluginStatus.Online);

                                /*string deviceNickName = devicesDoc.LookupDeviceName(device);
                                DisplayDevice newDevice = new DisplayDevice();
                                newDevice.MacAddress = device;
                                newDevice.Online = true;
                                if (deviceNickName != "")
                                {
                                    newDevice.Name = deviceNickName;
                                }
                                else
                                {
                                    newDevice.Name = device;
                                }
                                listbox.Items.Add(newDevice);*/
                            }
                        }
                    }
                }
                else
                    this.Dispatcher.Invoke(() => LoadOnlineDeviceList(sender, _devices));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }
        /// <summary>
        /// Display devices that are configured but not connected to the server
        /// </summary>
        private void LoadOfflineDevices()
        {
            try
            {
                if (this.CheckAccess())
                {
                    listDevices.Items.Clear();

                    List<Tuple<string, string>> deviceList = devicesDoc.DeviceList();
                    if (deviceList.Count > 0)
                    {
                        foreach (var device in deviceList)
                        {
                            AddDisplayDevice(listDevices, device.Item1, PluginStatus.Offline);
                        }
                    }
                }
                else
                    this.Dispatcher.Invoke(LoadOfflineDevices);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        /// <summary>
        /// To be discontinued and replaced by UpdateDeviceStatus function
        /// </summary>
        /// <param name="sender">ItemsControl</param>
        /// <param name="_user">Device to add</param>
        private void AddDisplayDevice(object sender, string _device, PluginStatus _pluginStatus)
        {
            try
            {
                if (this.Dispatcher.CheckAccess())
                {
                    if (sender is ItemsControl)
                    {
                        ItemsControl listbox = (ItemsControl)sender;

                        if (!string.IsNullOrWhiteSpace(_device) && devicesDoc != null)
                        {
                            RioDevice newDevice = new RioDevice();
                            bool found = false;
                            string deviceNickName = devicesDoc.LookupDeviceName(_device);
                            newDevice.Address = _device;
                            if (!string.IsNullOrWhiteSpace(deviceNickName))
                                newDevice.Name = deviceNickName;
                            else
                                newDevice.Name = _device;
                            newDevice.Online = _pluginStatus == PluginStatus.Online;
                            newDevice.Status = _pluginStatus;

                            foreach (RioDevice listDevice in listbox.Items)
                            {
                                if (listDevice.Name == newDevice.Name)
                                {
                                    listDevice.Online = _pluginStatus == PluginStatus.Online;
                                    listDevice.Status = _pluginStatus;
                                    found = true;
                                }
                            }
                            if (!found)
                                listbox.Items.Add(newDevice);
                        }
                    }
                }
                else
                    this.Dispatcher.Invoke(() => AddDisplayDevice(sender, _device, _pluginStatus));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void UpdateDeviceStatus(ItemsControl list, RioDevice _device)
        {
            try
            {
                if (Dispatcher.CheckAccess())
                {
                    if (list != null && _device != null)
                    {
                        if (string.IsNullOrWhiteSpace(_device.Name) && !string.IsNullOrWhiteSpace(_device.Address) && devicesDoc != null)
                        {
                            string deviceNickName = devicesDoc.LookupDeviceName(_device.Address);
                            if (!string.IsNullOrWhiteSpace(deviceNickName))
                                _device.Name = deviceNickName;
                            else
                                _device.Name = _device.Address;
                        }

                        if (!string.IsNullOrWhiteSpace(_device.Name))
                        {
                            bool found = false;
                            foreach (RioDevice deviceItem in list.Items)
                            {
                                if (deviceItem.Name == _device.Name)
                                {
                                    deviceItem.Status = _device.Status;
                                    found = true;
                                }
                            }
                            if (!found)
                                list.Items.Add(_device);
                        }
                    }
                }
                else
                    Dispatcher.Invoke(() => UpdateDeviceStatus(list, _device));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        /* Removed but not deleted yet - replaced by extending the functionality of AddOnlineDevice
        /// <summary>
        /// Remove device from a list control
        /// </summary>
        /// <param name="sender">ItemsControl</param>
        /// <param name="_user">Device to remove</param>
        private void RemoveOnlineDevice(object sender, string _device, bool isOnline)
        {
            try
            {
                if (sender is ItemsControl)
                {
                    ItemsControl listbox = (ItemsControl)sender;

                    if (_device != null)
                    {
                        string deviceNickName = devicesDoc.LookupDeviceName(_device);
                        if (deviceNickName == "")
                            deviceNickName = _device;
                        foreach (DisplayDevice displayDevice in listbox.Items)//.Cast<DisplayDevice>())
                        {
                            if (displayDevice.Name == deviceNickName)
                            {
                                displayDevice.Online = isOnline;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }*/
        #endregion Online devices
                
        #region Alarm buttons
        private void btnImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext != null)
                    {
                        Event eventItem = (Event)((FrameworkElement)sender).DataContext;

                        if (eventItem != null)
                        {
                            if (eventItem.Images.Count > 0)
                            {
                                if (eventItem.Images[0].Dewarp)
                                {
                                    GrandEyeWindow grandEyeWindow = new GrandEyeWindow();
                                    grandEyeWindow.ImageData = eventItem.Images[0].ImageData;
                                    grandEyeWindow.Show();
                                }
                                else
                                {
                                    FlatImageViewer flatImageViewer = new FlatImageViewer();
                                    flatImageViewer.Show();
                                    flatImageViewer.LoadImages(eventItem.Images);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void ShowImage(object sender, CameraImage _image)
        {
            try
            {
                if (_image.Dewarp)
                {
                    GrandEyeWindow grandEyeWindow = new GrandEyeWindow();
                    grandEyeWindow.ImageData = _image.ImageData;
                    grandEyeWindow.Show();
                }
                else
                {
                    FlatImageViewer flatImageViewer = new FlatImageViewer();
                    flatImageViewer.Show();
                    flatImageViewer.LoadImage(_image.ImageData);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Displaying image", ex);
            }
        }

        private void btnViewMap_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext != null)
                    {
                        Alarm alarm = (Alarm)((FrameworkElement)sender).DataContext;

                        if (alarm != null)
                        {
                            if (alarm.HasMap)
                            {
                                if (!tabControl.IsFocused)
                                    tabControl.Focus();

                                if (mapControl.CurrentMapName != alarm.GetMapName)
                                    mapControl.LoadMap(alarm.GetMapName);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }
        #endregion Alarm buttons
                
        #region Alarm Detail
        private void btnViewAlarmDetail_Click(object sender, RoutedEventArgs e)
        {
            AlarmDetailWindow alarmDetailWindow = null;

            try
            {
                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext != null)
                    {
                        AlarmBase alarm = (AlarmBase)((FrameworkElement)sender).DataContext;

                        if (alarm != null)
                        {
                            alarmDetailWindow = new AlarmDetailWindow();
                            alarmDetailWindow.Alarm = alarm;
                            alarmDetailWindow.AlarmResolved += alarmDetailWindow_AlarmResolved;
                            alarmDetailWindow.SiteInstructionStepChanged += alarmDetailWindow_SiteInstructionStepChanged;

                            alarmDetailWindow.Show();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        void alarmDetailWindow_AlarmResolved(object sender, AlarmBase _alarm, AlarmResolve _resolveDetailItem)
        {
            try
            {
                Client.SendMessage(_alarm is GroupAlarm ? MessageType.GroupAlarmResolve : MessageType.AlarmResolve, _resolveDetailItem);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Error resolving alarm", ex);
            }
        }

        void alarmDetailWindow_SiteInstructionStepChanged(object sender, SiteInstructionStep _siteInstructionStep)
        {
            try
            {
                Client.SendMessage(MessageType.SiteInstructionStep, _siteInstructionStep);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Error updating site instruction step", ex);
            }
        }
        #endregion Alarm Detail

        #region Resolve Panel
        private void btnViewAlarmResolver_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext != null)
                    {
                        AlarmBase alarm = (AlarmBase)((FrameworkElement)sender).DataContext;

                        if (alarm != null)
                        {
                            alarmToResolve = alarm;

                            panelAlarmResolver.DataContext = alarmToResolve;

                            //cboResolveAlarmType.DataContext = alarmToResolve;
                            cboResolveAlarmType.SelectedValue = alarmToResolve.Alarm_Type;
                            //lbSiteInstructions.DataContext = alarmToResolve;
                            lbSiteInstructions.Visibility = alarmToResolve.SiteInstructionCount > 0 ? Visibility.Visible : Visibility.Collapsed;

                            txtresolveDescription.Text = "";

                            panelAlarmResolver.Visibility = Visibility.Visible;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Error resolving alarm", ex);
            }
        }

        private void btnResolve_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (alarmToResolve.CanResolve)
                {
                    AlarmType alarmType = (AlarmType)cboResolveAlarmType.SelectedValue;

                    if (!string.IsNullOrWhiteSpace(txtresolveDescription.Text) && alarmType != AlarmType.Unresolved && alarmToResolve != null)
                    {
                        alarmToResolve.Alarm_Type = (AlarmType)cboResolveAlarmType.SelectedValue;

                        AlarmResolve alarmResolveItem = new AlarmResolve() { AlarmID = alarmToResolve.ID, ResolveID = Guid.NewGuid(), ResolveDT = DateTime.Now,
                            Alarm_Type = alarmToResolve.Alarm_Type, Description = txtresolveDescription.Text, User = CurrentUser };

                        alarmToResolve.Resolve(alarmResolveItem);

                        Client.SendMessage(alarmToResolve is GroupAlarm ? MessageType.GroupAlarmResolve : MessageType.AlarmResolve, alarmResolveItem);

                        alarmToResolve = null;

                        panelAlarmResolver.Visibility = Visibility.Collapsed;
                    }
                    else
                        MessageBox.Show("Make sure to specify a description and alarm type.", "Can't resolve", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                    MessageBox.Show("Make sure all compulsory site instructions are completed before resolving the alarm.", "Can't resolve", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void btnComment_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (alarmToResolve != null && !string.IsNullOrWhiteSpace(txtComment.Text))
                {
                    AlarmComment alarmComment = new AlarmComment() { AlarmID = alarmToResolve.ID, CommentID = Guid.NewGuid(), CommentDT = DateTime.Now, Comment = txtComment.Text, User = CurrentUser };
                    alarmToResolve.Comments.Add(alarmComment);

                    MainWindow.Client.SendMessage(MessageType.AlarmComment, alarmComment);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        //private void ResolveAll(object _baseAlarm)
        //{
        //    if (_baseAlarm is Alarm)
        //    {
        //        Alarm baseAlarm = (Alarm)_baseAlarm;
        //        for (int i = alarmStack.Count - 1; i >= 0; i--)
        //        {
        //            Alarm curAlarm = alarmStack[i];
        //            if (Alarm.MatchAlarms(curAlarm, baseAlarm))
        //            {
        //                curAlarm.Alarm_Type = baseAlarm.Alarm_Type;

        //                if (curAlarm.ID != baseAlarm.ID)
        //                {
        //                    if (baseAlarm.SiteInstructionCount > 0)
        //                    {
        //                        //curAlarm.ClearSiteInstructionSteps();
        //                        for (int instructionIndex = 0; instructionIndex < baseAlarm.SiteInstructionCount; instructionIndex++)
        //                        {

        //                            SiteInstructionStep step = baseAlarm.GetSiteInstructionStep(instructionIndex);
        //                            step = ObjectCopier.Clone<SiteInstructionStep>(step);
        //                            step.SiteInstructionStepID = Guid.NewGuid();
        //                            step.AlarmID = curAlarm.ID;
        //                            curAlarm.AddSiteInstructionStep(step);
        //                            client.SendMessage(MessageType.SiteInstructionStep, step);
        //                        }
        //                    }
        //                }

        //                Thread.Sleep(100);

        //                ResolveDetailItem baseResolveItem = baseAlarm.LastResolveItem;

        //                ResolveDetailItem resolveDetailItem = new ResolveDetailItem(curAlarm.ID, Guid.NewGuid(), DateTime.Now, curAlarm.Alarm_Type, (baseResolveItem != null ? baseResolveItem.Description : ""), currentUser);
        //                curAlarm.Resolve(resolveDetailItem);

        //                client.SendMessage(MessageType.AlarmResolve, resolveDetailItem);

        //                RemoveAlarm(alarmStack, curAlarm, false);

        //                if (AlarmHistoryFilter.Test(curAlarm, alarmHistoryFilter))
        //                    AddAlarm(alarmHistoryItems, curAlarm);
        //            }
        //        }
        //    }
        //}

        private void SiteInstructionsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (alarmToResolve != null)
                {
                    foreach (SiteInstructionStep siteInstructionStep in e.AddedItems)
                    {
                        if (siteInstructionStep.AlarmID == alarmToResolve.ID)
                        {
                            siteInstructionStep.Completed = true;
                            siteInstructionStep.CompletedDT = DateTime.Now;
                            siteInstructionStep.User = CurrentUser;

                            Client.SendMessage(MessageType.SiteInstructionStep, siteInstructionStep);
                        }
                    }

                    foreach (SiteInstructionStep siteInstructionStep in e.RemovedItems)
                    {
                        if (siteInstructionStep.AlarmID == alarmToResolve.ID)
                        {
                            siteInstructionStep.Completed = false;
                            siteInstructionStep.CompletedDT = DateTime.MinValue;
                            siteInstructionStep.User = CurrentUser;

                            Client.SendMessage(MessageType.SiteInstructionStep, siteInstructionStep);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Error changing selection", ex);
            }
        }

        private void btnCloseAlarmResolver_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                panelAlarmResolver.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Error closing alarm resolver", ex);
            }
        }
        #endregion Resolve Panel

        #region Alarm History
        private void cboHist_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Delete || e.Key == Key.Back)
                    ((ComboBox)sender).Text = "";
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }

        private void btnHistSearch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime? fromDateTime = dpHistFrom.SelectedDate != null ? dpHistFrom.SelectedDate + tpHistFrom.Value : null;
                DateTime? toDateTime = dpHistTo.SelectedDate != null ? dpHistTo.SelectedDate + tpHistTo.Value : null;
                AlarmType? alarmType = cboHistAlarmType.SelectedValue is AlarmType ? (AlarmType?)(cboHistAlarmType.SelectedValue) : null;
                EscalationLevel? escalationLevel = cboHistEscalationLevel.SelectedValue is EscalationLevel ? 
                    (EscalationLevel?)(cboHistEscalationLevel.SelectedValue) : null;

                alarmHistoryFilter = new AlarmHistoryFilter(fromDateTime, toDateTime, alarmType, txtHistDescription.Text, escalationLevel, null,
                    cbxHistResolved.IsChecked == true ? true : (bool?)null);

                Client.SendMessage(MessageType.RequestAlarmHistory, alarmHistoryFilter);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Error on history search", ex);
            }
        }

        private void btnHistClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                alarmHistoryFilter = null;

                RemoveAlarm(alarmHistoryItems, null);

                cboHistAlarmType.Text = "";
                cboHistEscalationLevel.Text = "";
                dpHistFrom.SelectedDate = null;
                tpHistFrom.Value = TimeSpan.Zero;
                dpHistTo.SelectedDate = null;
                tpHistTo.Value = TimeSpan.Zero;
                txtHistDescription.Text = "";
                cbxHistResolved.IsChecked = false;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Error clearing history", ex);
            }
        }
        #endregion Alarm History
        
        private void CopyContextMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (listDevices.SelectedItem == null)
                return;
            else
            {
                if(listDevices.SelectedItem is RioDevice)
                    Clipboard.SetText(((RioDevice)listDevices.SelectedItem).Address);
            }            
        }

        private void BtnViewCameras_Click(object sender, RoutedEventArgs e)
        {
            //TODO add camera positions to alarm, so that you can populate them correctly.
            var cameraPositions = new Dictionary<string, string>(6)
            {
                { "0_0_2", "http://192.168.1.235/onvif/device" },
                { "2_0_1", "http://192.168.1.31/onvif/device" },
                { "2_1_1", "http://192.168.1.29/onvif/device" },
                { "0_2_1", "http://192.168.1.111/onvif/device" },
                { "1_2_1", "http://192.168.1.31/onvif/device" },
                { "2_2_1", "http://192.168.1.235/onvif/device" }
            };
            string videoWindowName = "Live video for incident";
            if (((FrameworkElement)sender).DataContext is AlarmBase alarm)
            {
                videoWindowName = videoWindowName.Replace("incident", alarm.Name);
            }
            // TODO deduce layout from alarm details.

            if (cameraWindows == null)
                cameraWindows = new List<VideoWindow.MainWindow>();
            VideoWindow.MainWindow newWindow = new VideoWindow.MainWindow(videoWindowName, CurrentUser, CameraLayouts.TwoByTwo, Client.ServerConfig, cameraPositions);
            newWindow.Show();
            newWindow.Closed += VideoHost_Closing;
            cameraWindows.Add(newWindow);
        }

        private void VideoHost_Closing(object sender, EventArgs e)
        {
            if (Client != null)
            {
                VideoWindow.MainWindow window = sender as VideoWindow.MainWindow;
                if (window != null)
                {
                    Client.SendMessage(MessageType.DeleteCameraLayout, window.Title);
                    window.Closed -= VideoHost_Closing;
                    cameraWindows.Remove(window);
                    window.Dispose();
                }
            }
        }
    }

    [ValueConversion(typeof(string), typeof(bool))]
    public class HeaderToImageConverter : IValueConverter
    {
        public static HeaderToImageConverter Instance = new HeaderToImageConverter();
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Uri uri = new Uri("pack://application:,,/Icons/HomeBlue.png");
            BitmapImage source = new BitmapImage(uri);
            return source;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, CultureInfo culture)
        {
            throw new NotSupportedException("Cannot convert back");
        }
    }
}
