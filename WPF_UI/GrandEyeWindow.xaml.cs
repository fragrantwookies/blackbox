﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for GrandEyeWindow.xaml
    /// </summary>
    public partial class GrandEyeWindow : Window, IDisposable
    {
        private byte[] imageData = null;

        System.Windows.Forms.Integration.WindowsFormsHost host;
        ActiveXControlLib.OnCamGrandEyeControl grandEye;

        public GrandEyeWindow()
        {
            InitializeComponent();
        }

        ~GrandEyeWindow()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (grandEye != null)
            {
                grandEye.Dispose();
                grandEye = null;
            }

            if (host != null)
            {
                host.Dispose();
                host = null;
            }
        }

        private void GrandEyeWindow1_Loaded(object sender, RoutedEventArgs e)
        {
            // Create the interop host control.
            host = new System.Windows.Forms.Integration.WindowsFormsHost();

            // Create the ActiveX control.
            grandEye = new ActiveXControlLib.OnCamGrandEyeControl();

            // Assign the ActiveX control as the host control's child.
            host.Child = grandEye;

            // Add the interop host control to the Grid control's collection of child controls. 
            dockpanelGrandEye.Children.Add(host);
            
            if(imageData != null)
                grandEye.LoadImage(imageData);            
        }

        private void GrandEyeWindow1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (grandEye != null)
            {               
                grandEye.Dispose();
                grandEye = null;
            }

            if (host != null)
            {                
                host.Dispose();
                host = null;
            }
        }

        public byte[] ImageData
        {
            get { return imageData; }
            set { imageData = value; }
        }
    }
}
