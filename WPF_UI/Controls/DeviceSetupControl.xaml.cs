﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using BlackBoxEngine;

using eThele.Essentials;
using eNervePluginInterface;
using eNerve.DataTypes;
using System.Collections.Generic;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for DeviceSetupControl.xaml
    /// </summary>
    public partial class DeviceSetupControl : UserControl, IDisposable
    {
        private ObservableCollection<Base> devices;
        private ObservableCollection<Base> cameras;
        //private ObservableCollection<Base> pluginTypes;

        private ObservableCollection<UICondition> conditions;
        private XMLDoc conditionsDocBackup;

        private ObservableCollection<UIAlarmName> actions;
        private XMLDoc actionsDocBackup;

        private DevicesDoc backupDevicesDoc;

        public delegate void DeviceDocEventHandler(object sender, DevicesDoc _doc);
        public event DeviceDocEventHandler SaveDevices;

        public delegate void SendMessageEventHandler(SendMessageEventArgs e);
        public SendMessageEventHandler SendMessageEvent;

        public DeviceSetupControl()
        {
            InitializeComponent();

            devices = new ObservableCollection<Base>();
            lbDevices.DataContext = devices;

            cameras = new ObservableCollection<Base>();
            lbCameras.DataContext = cameras;

            //pluginTypes = new ObservableCollection<Base>();
            //lbPlugins.DataContext = pluginTypes;

            imgBox.DataContext = new UIDevice();
            imgCard.DataContext = new UIDetectorGroup();
            imgAnalogIn.DataContext = new UIDetector(true);
            imgDigitalIn.DataContext = new UIDetector(false);
            imgRelay.DataContext = new UIRelay();
            imgCamera.DataContext = new UICamera();
        }

        ~DeviceSetupControl()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (Dispatcher.CheckAccess())
            {                
                ClearDeviceList();

                IDisposable disposableObject = null;

                if (imgBox != null)
                {
                    disposableObject = imgBox.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgCard != null)
                {
                    disposableObject = imgCard.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgAnalogIn != null)
                {
                    disposableObject = imgAnalogIn.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgDigitalIn != null)
                {
                    disposableObject = imgDigitalIn.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgRelay != null)
                {
                    disposableObject = imgRelay.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgCamera != null)
                {
                    disposableObject = imgCamera.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
            }
            else
                try
                {
                    Dispatcher.Invoke(() => { Dispose(disposing); });
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), 
                        "Error disposing DeviceSetupControl: ", ex);
                }
        }

        public void PopulatePlugins(ObservableCollection<INervePlugin> pluginList)
        {
            if (CheckAccess())
            {
                foreach (var plugin in pluginList)
                {
                    var pluginInstance = new UIDevice();
                    pluginInstance.Name = plugin.Name;
                    if (!devices.Contains(pluginInstance))
                    {
                        var uiPlugin = new UIPlugin(plugin) { Parent = pluginInstance };
                        devices.Add(uiPlugin);
                    }
                }
            }
            else
            {
                Dispatcher.Invoke(delegate () { PopulatePlugins(pluginList); });
            }
        }

        ObservableCollection<INervePlugin> pluginTypes;

        public void PopulatePluginTypes(ObservableCollection<INervePlugin> _pluginTypes)
        {
            if (_pluginTypes != null)
            {
                pluginTypes = _pluginTypes;
                lbDeviceTypes.DataContext = _pluginTypes;
            }
        }

        private void ClearDeviceList()
        {
            try
            {
                if (CheckAccess())
                {
                    if (devices != null)
                    {
                        for (int i = devices.Count - 1; i >= 0; i--)
                        {
                            Base device = devices[i];
                            devices.RemoveAt(i);
                            device.Dispose();
                        }
                    }
                    if (cameras != null)
                    {
                        for (int i = cameras.Count - 1; i >= 0; i--)
                        {
                            Base camera = cameras[i];
                            cameras.RemoveAt(i);
                            camera.Dispose();
                        }
                    }
                }
                else
                {
                    Dispatcher.Invoke(ClearDeviceList);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "",
                    ex);
            }
        }

        private void ClearConditions()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (conditions != null)
                    {
                        for (int i = conditions.Count - 1; i >= 0; i--)
                        {
                            UICondition condition = conditions[i];
                            conditions.RemoveAt(i);
                            condition.Dispose();
                        }
                    }
                }
                else
                {
                    this.Dispatcher.Invoke(ClearConditions);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void ClearActions()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (actions != null)
                    {
                        for (int i = actions.Count - 1; i >= 0; i--)
                        {
                            UIAlarmName alarm = actions[i];
                            actions.RemoveAt(i);
                            alarm.Dispose();
                        }
                    }
                }
                else
                {
                    this.Dispatcher.Invoke(ClearActions);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void SetConditions(XMLDoc conditionsDoc)
        {
            try
            {
                if (conditionsDoc != null)
                {
                    if (this.CheckAccess())
                    {
                        ClearConditions();

                        if (conditions == null)
                            conditions = new ObservableCollection<UICondition>();

                        conditionsDocBackup = conditionsDoc;

                        List<UICondition> conditionList = ConditionsControl.ParseConditions(conditionsDoc);

                        if (conditionList != null)
                        {
                            foreach (UICondition c in conditionList)
                                conditions.Add(c);

                            conditionList.Clear();
                        }
                    }
                    else
                        this.Dispatcher.Invoke(() => SetConditions(conditionsDoc));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void SetActions(XMLDoc _doc)
        {
            try
            {
                if (_doc != null)
                {
                    if (this.CheckAccess())
                    {
                        ClearActions();

                        if (actions == null)
                            actions = new ObservableCollection<UIAlarmName>();

                        actionsDocBackup = _doc;

                        List<UIAlarmName> actionList = ActionsControl.ParseActions(_doc);

                        if (actionList != null)
                        {
                            foreach (UIAlarmName a in actionList)
                                actions.Add(a);

                            actionList.Clear();
                        }
                    }
                    else
                        this.Dispatcher.Invoke(() => SetActions(_doc));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void SetDevices(DevicesDoc _devicesDoc)
        {
            try
            {
                if (_devicesDoc != null)
                {
                    ClearDeviceList();

                    backupDevicesDoc = _devicesDoc;

                    if (CheckAccess())
                    {
                        foreach (XmlNode deviceNode in _devicesDoc)
                        {
                            UIDevice device = new UIDevice()
                            {
                                Name = deviceNode.Name,
                                DeviceAddress = XmlNode.ParseString(deviceNode["DeviceAddress"], ""),
                                PluginTypes = lbDeviceTypes.DataContext as ObservableCollection<INervePlugin>,
                                IsEnabled = XmlNode.ParseBool(deviceNode, true, "Enabled")
                            };

                            if (deviceNode.Name.ToLower() == "cameras")
                            {
                                foreach (XmlNode cameraNode in deviceNode)
                                {
                                    UICamera camera = (UICamera)ParseIOItemNode(cameraNode, "camera");
                                    if (camera != null)
                                    {
                                        if ((camera.Admin) && (MainWindow.CurrentUser.Rights.UserType != UserType.Admin) && (MainWindow.CurrentUser.Rights.UserType != UserType.Super))
                                            continue;// RISK of deleting cameras. Currently this is a black hole but wiil have to be revisited.
                                        AddCamera(camera);
                                        cameras.Add(camera);
                                    }
                                }
                            }
                            else //(deviceNode.Name.ToLower() != "cameras")
                            {
                                if (deviceNode["Properties"] != null)
                                {
                                    var isPluginPropertyNode = deviceNode["Properties"].FindChildNode("IsPlugin");
                                    if (isPluginPropertyNode == null)
                                    {
                                        deviceNode["Properties"].Add("IsPlugin", "false");
                                    }
                                    else if (isPluginPropertyNode.Value.Equals("True", StringComparison.OrdinalIgnoreCase))
                                    {// Populate required properties of plugins
                                        device.IsPlugin = true;
                                        var pluginTypeNode = deviceNode["Properties"].FindChildNode("PluginType");
                                        if (pluginTypeNode == null)
                                        {
                                            deviceNode["Properties"].Add("PluginType", "");
                                        }
                                        else if (!string.IsNullOrEmpty(pluginTypeNode.Value))
                                        {
                                            AddPluginProperties(pluginTypeNode.Value, device.Properties);
                                        }
                                    }
                                    else
                                    {
                                        device.IsPlugin = false;
                                    }

                                    foreach (XmlNode propertyNode in deviceNode["Properties"])
                                    {
                                        UICustomProperty property = null;
                                        UICustomPropertyList propertyList = null;
                                        if (propertyNode.Children.Count > 0)
                                        {
                                            propertyList = new UICustomPropertyList(propertyNode);
                                            property = propertyList;
                                        }
                                        else
                                            property = new UICustomProperty(propertyNode);

                                        if (propertyNode.Name.Contains("Plugin"))
                                        {
                                            property.Display = false;
                                            if (string.Compare(propertyNode.Name, "PluginType", true) == 0)
                                            {
                                                var pluginType = device.PluginTypes.FirstOrDefault(x => PluginManager.PrettifyString(propertyNode.Value).Contains(x.Name));
                                                if (pluginType != null)
                                                {
                                                    device.PluginType = pluginType;
                                                }
                                            }

                                            continue;
                                        }

                                        var propertyIndex = device.Properties.IndexOf(property);
                                        if (propertyIndex < 0)
                                        {// Property does not exist - should only happen for non-plugin devices.
                                            device.Properties.Add(property);
                                        }
                                        else
                                        {// Update the value of the existing property
                                            UpdateCustomProperty(device.Properties[propertyIndex], property);

                                            //if (!string.IsNullOrEmpty(property.Value))
                                            //{
                                            //    device.Properties[propertyIndex].Value = property.Value;
                                            //    device.Properties[propertyIndex].Display = property.Display;
                                            //}

                                            //if (propertyList != null)
                                            //    device.Properties[propertyIndex] = propertyList;
                                        }
                                    }
                                }
                                if (deviceNode["Cameras"] == null) // If no cameras, deduce device.
                                {
                                    foreach (XmlNode cardNode in deviceNode["Groups"])
                                    {
                                        UIDetectorGroup group = new UIDetectorGroup()
                                        {
                                            GroupName = cardNode.Name,
                                            GroupIndex = XmlNode.ParseInteger(cardNode, 0, "Index")
                                        };

                                        foreach (XmlNode ioNode in cardNode)
                                        {
                                            UIIOItem io = ParseIOItemNode(ioNode);
                                            if (io != null)
                                            {
                                                io.Parent = group;
                                                group.IOs.Add(io);
                                            }
                                        }

                                        group.Parent = device;
                                        device.Groups.Add(group);
                                    }
                                    devices.Add(device);
                                }
                                else
                                {
                                    UICameraServer cameraServer = new UICameraServer()
                                    {
                                        Properties = device.Properties,
                                        Name = device.Name,
                                        IpAddress = device.DeviceAddress,
                                        PluginTypes = lbDeviceTypes.DataContext as ObservableCollection<INervePlugin>,
                                        IsEnabled = device.IsEnabled
                                    };
                                    cameraServer.IsPlugin = true;
                                    cameraServer.PluginType = device.PluginType;

                                    foreach (XmlNode cameraNode in deviceNode["Cameras"])
                                    {
                                        UICamera camera = (UICamera)ParseIOItemNode(cameraNode, "camera");
                                        if (camera != null)
                                        {
                                            var propertiesNode = cameraNode["Properties"];
                                            if (propertiesNode != null)
                                            {
                                                foreach (XmlNode propertyNode in propertiesNode)
                                                {
                                                    UICustomProperty property = new UICustomProperty(propertyNode);
                                                    if (propertyNode.Name.Contains("Name"))
                                                        property.Display = false;
                                                    var propertyIndex = device.Properties.IndexOf(property);
                                                    if (propertyIndex < 0)
                                                    {// Property does not exist - create.
                                                        camera.Properties.Add(property);
                                                    }
                                                    else
                                                    {// Update the value of the existing property
                                                        if (!string.IsNullOrEmpty(property.Value))
                                                        {
                                                            camera.Properties[propertyIndex].Value = property.Value;
                                                            camera.Properties[propertyIndex].Display = property.Display;
                                                        }
                                                    }
                                                }
                                            }
                                            cameraServer.Cameras.Add(camera);
                                        }
                                    }
                                    devices.Add(cameraServer);
                                }
                            }
                        }
                    }
                    else
                        Dispatcher.Invoke(() => SetDevices(_devicesDoc));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "",
                    ex);
            }
        }

        private void UpdateCustomProperty(UICustomProperty propertyToUpdate, UICustomProperty newData)
        {
            if (propertyToUpdate != null && newData != null)
            {
                if (propertyToUpdate is UICustomPropertyList && newData is UICustomPropertyList)
                {
                    UICustomPropertyList propertyToUpdateList = propertyToUpdate as UICustomPropertyList;
                    UICustomPropertyList newDataList = newData as UICustomPropertyList;

                    while (propertyToUpdateList.Properties.Count < newDataList.Properties.Count)
                    {
                        if (propertyToUpdateList.Properties[0] is UICustomPropertyList)
                        {
                            var newClone = ObjectCopier.CloneXML<UICustomPropertyList>(propertyToUpdateList.Properties[0] as UICustomPropertyList);
                            propertyToUpdateList.Properties.Add(newClone);   
                        }
                        else
                        {
                            var newClone = ObjectCopier.CloneXML<UICustomProperty>(propertyToUpdateList.Properties[0]);
                            propertyToUpdateList.Properties.Add(newClone);
                        }
                    }

                    for (int i = 0; i < newDataList.Properties.Count; i++)
                    {
                        UpdateCustomProperty(propertyToUpdateList.Properties[i], newDataList.Properties[i]);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(newData.Value))
                        propertyToUpdate.Value = newData.Value;
                    propertyToUpdate.Display = newData.Display;
                }
            }
        }

        private UICustomProperty LoadCustomProperty(XmlNode _PropertyNode)
        {
            UICustomProperty result = null;



            return result;
        }

        private void AddCamera(UICamera camera)
        {
            var index = devices.IndexOf(camera.Parent);
            if (index > 0)
            {
                var cameraServer = devices[index] as UICameraServer;
                if (cameraServer != null)
                {
                    if (cameraServer.Cameras == null)
                    {
                        cameraServer.Cameras = new ObservableCollection<UICamera>();
                        cameraServer.Cameras.Add(camera);
                    }
                    else
                    {
                        index = cameraServer.Cameras.IndexOf(camera);

                        if (index > 0)
                        {// Update camera details
                            UICamera existingCamera = cameraServer.Cameras[index];
                            if (existingCamera == null)
                            {// Black hole but included for completeness
                                cameraServer.Cameras.Add(camera);
                            }
                            else
                            {
                                existingCamera = camera;
                            }
                        }
                        else
                        {
                            cameraServer.Cameras.Add(camera);
                        }
                    }
                }
                else
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                       MethodBase.GetCurrentMethod().Name), "Unable to find camera's parent - throw away the orphan " + camera.DisplayName);
                }
            }
        }

        private void AddPluginProperties(string value, ObservableCollection<UICustomProperty> properties)
        {
            if (!string.IsNullOrEmpty(value))
            {
                if (properties == null)
                    properties = new ObservableCollection<UICustomProperty>();
                GetPluginProperties(value, properties);
            }
        }

        private void GetPluginProperties(string value, ObservableCollection<UICustomProperty> properties)
        {
            var currentDevice = lbDeviceTypes.ItemsSource.Cast<INervePlugin>().FirstOrDefault(x => PluginManager.PrettifyString(value).Contains(x.Name));
            if (currentDevice != null)
            {
                GetPluginProperties(currentDevice, properties);
            }
        }

        public static void GetPluginProperties(INervePlugin plugin, ObservableCollection<UICustomProperty> properties)
        {
            foreach (var property in plugin.CustomPropertyDefinitions)
            {
                UICustomProperty uiCustomProperty = new UICustomProperty(property);

                UICustomPropertyList uiCustomPropertyList = null;
                if (property.PropertyType == typeof(CustomPropertyList))
                {
                    uiCustomPropertyList = new UICustomPropertyList(property);
                    //AddCustomPropertyField(uiCustomPropertyList);
                    uiCustomProperty = uiCustomPropertyList;
                }

                if (property.PropertyName.Contains("Plugin"))
                    uiCustomProperty.Display = false;
                if (!string.IsNullOrEmpty(uiCustomProperty.Name))
                {
                    var propertyIndex = properties.IndexOf(uiCustomProperty);
                    if (propertyIndex < 0)
                    {
                        properties.Add(uiCustomProperty);
                    }
                    else
                    {                  
                        if (uiCustomPropertyList != null)
                        {
                            properties[propertyIndex] = uiCustomPropertyList;
                        }
                        else
                        {
                            properties[propertyIndex].Value = uiCustomProperty.Value;
                            properties[propertyIndex].Display = uiCustomProperty.Display;
                        }
                    }
                }
            }
        }

        private static void AddCustomPropertyField(UICustomPropertyList customPropertyList)
        {
            foreach (CustomPropertyDefinition def in customPropertyList.PropertyDefinition.ChildDefinitions)
            {
                if (def.ChildDefinitions.Count > 0)
                    customPropertyList.Properties.Add(new UICustomPropertyList(def) { });
                else
                    customPropertyList.Properties.Add(new UICustomProperty(def));
            }
        }

        //private Base ParseIONode(XmlNode _ioNode)
        //{
        //    Base result = null;
        //    try
        //    {
        //        if (_ioNode != null)
        //        {
        //            string ioType = XmlNode.ParseString(_ioNode, "", "Type");

        //            if (ioType.ToLower() == "AnalogIn".ToLower() || ioType.ToLower() == "DigitalIn".ToLower())
        //            {
        //                UIDetectors uiDetectors = (UIDetectors)(result = new UIDetectors());
        //                uiDetectors.Name = _ioNode.Name;
        //                uiDetectors.IOType = ioType;

        //                foreach (XmlNode itemNode in _ioNode)
        //                {
        //                    UIIOItem item = ParseIOItemNode(itemNode, ioType);
        //                    if (item is UIDetector)
        //                    {
        //                        item.Parent = uiDetectors;
        //                        uiDetectors.Detectors.Add((UIDetector)item);
        //                    }
        //                }
        //            }                        
        //            else if (ioType.ToLower() == "Relay".ToLower())
        //            {
        //                result = ParseIOItemNode(_ioNode, ioType);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
        //    }

        //    return result;
        //}

        private UIIOItem ParseIOItemNode(XmlNode _ioItemNode, string ioType = "")
        {
            UIIOItem result = null;
            try
            {
                if (_ioItemNode != null)
                {
                    if (string.IsNullOrWhiteSpace(ioType))
                        ioType = XmlNode.ParseString(_ioItemNode, "", "Type");

                    switch (ioType.ToLower())
                    {
                        case "analogin":
                            UIDetector analogDetector = (UIDetector)(result = new UIDetector(true));
                            analogDetector.MinThreshold = XmlNode.ParseDouble(_ioItemNode["MinThreshold"], 0);
                            analogDetector.MaxThreshold = XmlNode.ParseDouble(_ioItemNode["MaxThreshold"], 0);
                            analogDetector.ShowThresholds = true;
                            break;
                        case "digitalin":
                            UIDetector digitalDetector = (UIDetector)(result = new UIDetector(false));
                            digitalDetector.ShowThresholds = false;
                            break;
                        case "relay":
                            UIRelay relay = (UIRelay)(result = new UIRelay());
                            break;
                        case "camera":
                            UICamera camera = (UICamera)(result = new UICamera());
                            camera.Admin = XmlNode.ParseBool(_ioItemNode, false, "Admin");
                            camera.IsEnabled = XmlNode.ParseBool(_ioItemNode, true, "Enabled");
                            camera.Name = _ioItemNode.Name;
                            camera.IPAddress = XmlNode.ParseString(_ioItemNode["IPAddress"], "");
                            camera.Snapshot = XmlNode.ParseString(_ioItemNode["Snapshot"], "");
                            camera.PreCurrPost = XmlNode.ParseString(_ioItemNode["PreCurrPost"], "");
                            camera.Username = XmlNode.ParseString(_ioItemNode["UserName"], "");
                            camera.Password = XmlNode.ParseString(_ioItemNode["Password"], "");
                            camera.Dewarp = XmlNode.ParseBool(_ioItemNode["Dewarp"], false);
                            camera.DisplayName = XmlNode.ParseString(_ioItemNode["DisplayName"], camera.Name);
                            var propertiesNode = _ioItemNode["Properties"];
                            if (propertiesNode != null)
                            {
                                camera.DisplayName = XmlNode.ParseString(propertiesNode["DisplayName"], camera.Name);
                            }
                            break;
                    }

                    if (result != null)
                    {
                        result.Name = XmlNode.ParseString(_ioItemNode["Name"], _ioItemNode.Name);
                        result.IOType = ioType;
                        result.Index = XmlNode.ParseInteger(_ioItemNode, 0, "Index");
                        result.Description = XmlNode.ParseString(_ioItemNode["Description"], "");
                        result.Location = XmlNode.ParseString(_ioItemNode["Location"], "");

                        result.ImagePath = XmlNode.ParseString(_ioItemNode["Image"], "");
                        result.AltImagePath = XmlNode.ParseString(_ioItemNode["AltImage"], "");
                        result.ImageMap = XmlNode.ParseString(_ioItemNode["Image"], "", "Map");
                        result.ImageX = XmlNode.ParseDouble(_ioItemNode["Image"], 0, "Left");
                        result.ImageY = XmlNode.ParseDouble(_ioItemNode["Image"], 0, "Top");
                        result.ImageWidth = XmlNode.ParseDouble(_ioItemNode["Image"], 0, "Width");
                        result.ImageHeight = XmlNode.ParseDouble(_ioItemNode["Image"], 0, "Height");
                        result.ImageRotation = XmlNode.ParseDouble(_ioItemNode["Image"], 0, "Rotation");

                        if (result is UIDetector)
                        {
                            UIDetector detector = (UIDetector)result;
                            detector.EventName = XmlNode.ParseString(_ioItemNode["EventName"], "");
                            detector.EventsToRaiseAlarm = XmlNode.ParseInteger(_ioItemNode["EventsToRaiseAlarm"], 1);
                            detector.EventsToRaiseAlarmInterval = XmlNode.ParseInteger(_ioItemNode["EventsToRaiseAlarmInterval"], 0);
                            detector.CoolDown = XmlNode.ParseInteger(_ioItemNode["CoolDown"], 0);
                            detector.Sensitivity = XmlNode.ParseDouble(_ioItemNode["Sensitivity"], -1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public DevicesDoc BuildDeviceDoc()
        {
            DevicesDoc result = null;

            try
            {
                result = new DevicesDoc("Devices");

                foreach (Base device in devices)
                {
                    XmlNode node = BuildNode(device);

                    if (node != null)
                        result.Add(node);
                }

                XmlNode camerasNode = result.Add("Cameras", "");
                foreach (Base device in cameras)
                {
                    XmlNode node = BuildNode(device);

                    if (node != null)
                        camerasNode.Add(node);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private XmlNode BuildNode(Base _baseItem, string _nodeName = "")
        {
            XmlNode result = null;

            try
            {
                if (_baseItem is UIDevice)
                {
                    UIDevice device = (UIDevice)_baseItem;
                    XmlNode deviceNode = result = new XmlNode(device.Name, "");
                    deviceNode.AddAttribute("Enabled", device.IsEnabled.ToString());
                    deviceNode.Add("DeviceAddress", device.DeviceAddress);
                    XmlNode groupsNode = deviceNode.Add("Groups", "");
                    XmlNode propertiesNode = deviceNode.Add("Properties", "");
                    foreach (UIDetectorGroup group in device.Groups)
                    {
                        XmlNode groupNode = BuildNode(group);
                        if (groupNode != null)
                            groupsNode.Add(groupNode);
                    }
                    foreach (UICustomProperty property in device.Properties)
                    {
                        XmlNode propertyNode = BuildNode(property);
                        if(propertyNode != null)
                            propertiesNode.Add(propertyNode);
                    }
                }
                else if (_baseItem is UICustomPropertyList)
                {
                    UICustomPropertyList list = (UICustomPropertyList)_baseItem;

                    XmlNode propertyListNode = result = new XmlNode(list.Name, "");

                    foreach (UICustomProperty property in list.Properties)
                    {
                        XmlNode propertyNode = BuildNode(property);
                        if (propertyNode != null)
                            propertyListNode.Add(propertyNode);
                    }
                }                   
                else if (_baseItem is UICustomProperty)
                {
                    UICustomProperty property = (UICustomProperty)_baseItem;
                    XmlNode propertyNode = result = new XmlNode(property.Name, property.Value);
                }
                else if (_baseItem is UIDetectorGroup)
                {
                    UIDetectorGroup group = (UIDetectorGroup)_baseItem;
                    XmlNode groupNode = result = new XmlNode(group.Name, "");
                    groupNode.AddAttribute("Index", group.GroupIndex.ToString());

                    foreach (Base io in group.IOs)
                    {
                        XmlNode ioNode = BuildNode(io);
                        if (ioNode != null)
                            groupNode.Add(ioNode);
                    }
                }
                else if (_baseItem is UIDetector)
                {
                    UIDetector detector = (UIDetector)_baseItem;
                    XmlNode detectorNode = result = new XmlNode(!string.IsNullOrWhiteSpace(_nodeName) ? _nodeName : string.Format("{0}{1}", detector.IOType, detector.Index), "");
                    detectorNode.AddAttribute("Type", detector.IOType);
                    detectorNode.AddAttribute("Index", detector.Index.ToString());
                    detectorNode.Add("Name", detector.Name);
                    detectorNode.Add("EventName", detector.EventName);
                    detectorNode.Add("Location", detector.Location);

                    XmlNode imageNode = detectorNode.Add("Image", detector.ImagePath);
                    imageNode.AddAttribute("Map", detector.ImageMap);
                    imageNode.AddAttribute("Left", detector.ImageX.ToString());
                    imageNode.AddAttribute("Top", detector.ImageY.ToString());

                    if (detector.ImageHeight > 0)
                        imageNode.AddAttribute("Height", detector.ImageHeight.ToString());
                    if (detector.ImageWidth > 0)
                        imageNode.AddAttribute("Width", detector.ImageWidth.ToString());
                    if (detector.ImageRotation > 0)
                        imageNode.AddAttribute("Rotation", detector.ImageRotation.ToString());

                    detectorNode.Add("AltImage", detector.AltImagePath);
                    detectorNode.Add("EventsToRaiseAlarm", detector.EventsToRaiseAlarm.ToString());
                    detectorNode.Add("EventsToRaiseAlarmInterval", detector.EventsToRaiseAlarmInterval.ToString());
                    detectorNode.Add("CoolDown", detector.CoolDown.ToString());
                    detectorNode.Add("Sensitivity", detector.Sensitivity.ToString());

                    if (detector.IsAnalog)
                    {
                        detectorNode.Add("MinThreshold", detector.MinThreshold.ToString());
                        detectorNode.Add("MaxThreshold", detector.MaxThreshold.ToString());
                    }
                }
                else if (_baseItem is UIRelay)
                {
                    UIRelay relay = (UIRelay)_baseItem;
                    XmlNode relayNode = result = new XmlNode(relay.Name, "");
                    relayNode.AddAttribute("Type", "Relay");
                    relayNode.AddAttribute("Index", relay.Index.ToString());
                    relayNode.Add("Name", relay.Name);
                    relayNode.Add("Description", relay.Description);
                    relayNode.Add("Location", relay.Location);

                    XmlNode imageNode = relayNode.Add("Image", relay.ImagePath);
                    imageNode.AddAttribute("Map", relay.ImageMap);
                    imageNode.AddAttribute("Left", relay.ImageX.ToString());
                    imageNode.AddAttribute("Top", relay.ImageY.ToString());

                    if (relay.ImageHeight > 0)
                        imageNode.AddAttribute("Height", relay.ImageHeight.ToString());
                    if (relay.ImageWidth > 0)
                        imageNode.AddAttribute("Width", relay.ImageWidth.ToString());
                    if (relay.ImageRotation > 0)
                        imageNode.AddAttribute("Rotation", relay.ImageRotation.ToString());

                    if (!string.IsNullOrWhiteSpace(relay.AltImagePath))
                        relayNode.Add("AltImage", relay.AltImagePath);
                }
                else if (_baseItem is UICameraServer)
                {
                    UICameraServer camServer = (UICameraServer)_baseItem;
                    XmlNode camServerNode = result = new XmlNode(camServer.Name, "");
                    camServerNode.AddAttribute("Enabled", camServer.IsEnabled.ToString());
                    camServerNode.Add("DeviceAddress", camServer.IpAddress);
                    XmlNode propertiesNode = camServerNode.Add("Properties", "");
                    foreach (UICustomProperty property in camServer.Properties)
                    {
                        propertiesNode.Add(property.Name, property.Value);
                    }
                    XmlNode camerasNode = camServerNode.Add("Cameras", "");
                    foreach (UICamera camera in camServer.Cameras)
                    {
                        XmlNode cameraNode = BuildNode(camera);
                        if (cameraNode != null)
                            camerasNode.Add(cameraNode);
                    }
                }
                else if (_baseItem is UICamera)
                {
                    UICamera camera = (UICamera)_baseItem;
                    XmlNode cameraNode = result = new XmlNode(camera.Name, "");
                    XmlNode propertiesNode = cameraNode.Add("Properties", "");
                    foreach (UICustomProperty property in camera.Properties)
                    {
                        if (property.Name == "DisplayName")
                            propertiesNode.Add(property.Name, camera.DisplayName);
                        else
                            propertiesNode.Add(property.Name, property.Value);
                    }
                    cameraNode.AddAttribute("Admin", camera.Admin.ToString());
                    cameraNode.AddAttribute("Enabled", camera.IsEnabled.ToString());
                    cameraNode.AddAttribute("Index", camera.Index.ToString());
                    cameraNode.AddAttribute("Type", camera.IOType);
                    cameraNode.Add("IPAddress", camera.IPAddress);
                    cameraNode.Add("Snapshot", camera.Snapshot);
                    cameraNode.Add("PreCurrPost", camera.PreCurrPost);
                    cameraNode.Add("Dewarp", camera.Dewarp ? "True" : "False");
                    cameraNode.Add("Description", camera.Description);
                    cameraNode.Add("Location", camera.Location);

                    if (!string.IsNullOrWhiteSpace(camera.Username))
                        cameraNode.Add("UserName", camera.Username);
                    if (!string.IsNullOrWhiteSpace(camera.Password))
                        cameraNode.Add("Password", camera.Password);

                    XmlNode imageNode = cameraNode.Add("Image", camera.ImagePath);
                    imageNode.AddAttribute("Map", camera.ImageMap);
                    imageNode.AddAttribute("Left", camera.ImageX.ToString());
                    imageNode.AddAttribute("Top", camera.ImageY.ToString());

                    if (camera.ImageHeight > 0)
                        imageNode.AddAttribute("Height", camera.ImageHeight.ToString());
                    if (camera.ImageWidth > 0)
                        imageNode.AddAttribute("Width", camera.ImageWidth.ToString());
                    if (camera.ImageRotation > 0)
                        imageNode.AddAttribute("Rotation", camera.ImageRotation.ToString());

                    if (!string.IsNullOrWhiteSpace(camera.AltImagePath))
                        cameraNode.Add("AltImage", camera.AltImagePath);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement && ((FrameworkElement)sender).DataContext is IExpandable)
                {
                    IExpandable expandableControl = (IExpandable)((FrameworkElement)sender).DataContext;

                    if (e.LeftButton == MouseButtonState.Pressed)
                        expandableControl.Expand = !expandableControl.Expand;
                    else if (e.RightButton == MouseButtonState.Pressed)
                    {
                        if (expandableControl.Expand)
                            expandableControl.CollapseAll();
                        else
                            expandableControl.ExpandAll();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private FrameworkElement _dragged;

        private void DragDrop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_dragged != null)
                return;

            if (sender is UIElement)
            {
                UIElement lb = (UIElement)sender;

                UIElement element = lb.InputHitTest(e.GetPosition(lb)) as UIElement;

                while (element != null)
                {
                    if (element is FrameworkElement)
                    {
                        _dragged = (FrameworkElement)element;
                        break;
                    }
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }
            }
        }

        private void DragDrop_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (_dragged == null)
                    return;
                if (e.LeftButton == MouseButtonState.Released)
                {
                    _dragged = null;
                    return;
                }
                else if (e.LeftButton == MouseButtonState.Pressed)
                {
                    if (_dragged is FrameworkElement)
                    {
                        object data = _dragged.DataContext;
                        if ((data is Base) || (data is INervePlugin))
                        {
                            Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new System.Threading.ParameterizedThreadStart(DoDragDrop), data);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                _dragged = null;
            }
        }

        private void DoDragDrop(object parameter)
        {
            if (_dragged != null)
            {
                DragDrop.DoDragDrop(_dragged, parameter, DragDropEffects.All);
                _dragged = null;
            }
        }

        private void NewDrop(object sender, DragEventArgs e)
        {
            object deviceObj = e.Data.GetData(typeof(UIDevice));
            if (!(deviceObj is UIDevice))
            {
                deviceObj = e.Data.GetData(typeof(UICamera));
                if (!(deviceObj is UICamera))
                {
                    deviceObj = null;
                }
            }

            if (deviceObj == null) // Neither Device nor Camera - check for Plugin.
            {
                deviceObj = e.Data.GetData(e.Data.GetFormats()[0]);
                if (deviceObj != null)
                {// Successfully obtained plugin data
                    UIDevice newDevice = new UIDevice() { IsEnabled = true, IsPlugin = true, PluginType = (deviceObj as INervePlugin), PluginTypes = lbDeviceTypes.DataContext as ObservableCollection<INervePlugin> };
                    GetPluginProperties(newDevice.PluginType, newDevice.Properties);
                    newDevice.Name = devices.GetUniqueName(newDevice.PluginType.Name);
                    devices.Add(newDevice);
                }
            }
            else
            {

                Base device = ObjectCopier.Clone((Base)deviceObj);

                if (device is UIDevice)
                {
                    device.Name = devices.GetUniqueName("Box");
                    devices.Add(device);
                }
                else if (device is UICamera)
                {
                    device.Name = cameras.GetUniqueName("Camera");
                    cameras.Add(device);
                }
            }

            e.Handled = true;
        }

        private void DeleteDrop(object sender, DragEventArgs e)
        {
            object obj = e.Data.GetData(typeof(UIDevice));
            if (!(obj is UIDevice))
            {
                obj = e.Data.GetData(typeof(UICameraServer));
                if (!(obj is UICameraServer))
                {
                    obj = e.Data.GetData(typeof(UIDetectorGroup));
                    if (!(obj is UIDetectorGroup))
                    {
                        obj = e.Data.GetData(typeof(UIDetectors));
                        if (!(obj is UIDetectors))
                        {
                            obj = e.Data.GetData(typeof(UIDetector));
                            if (!(obj is UIDetector))
                            {
                                obj = e.Data.GetData(typeof(UIRelay));
                                if (!(obj is UIRelay))
                                    obj = null;
                            }
                        }
                    }
                }
            }

            if (obj != null)
            {
                Base baseObj = (Base)obj;

                if (baseObj.Parent != null)
                {
                    if (Dispatcher.CheckAccess())
                        baseObj.Parent.RemoveChild(baseObj);
                    else
                        Dispatcher.Invoke(new Base.RemoveChildDelegate(baseObj.Parent.RemoveChild), baseObj);
                }
                else
                {
                    if (baseObj is UIDevice)
                        devices.Remove(baseObj);
                    else if (baseObj is UICameraServer)
                        devices.Remove(baseObj);
                }

                baseObj.Dispose();
            }

            e.Handled = true;
        }

        private void CardDrop(object sender, DragEventArgs e)
        {
            object obj = e.Data.GetData(typeof(UIDetectorGroup));
            if ((obj is UIDetectorGroup))
            {
                UIDetectorGroup group = ObjectCopier.Clone((UIDetectorGroup)obj);

                if (sender is FrameworkElement)
                {
                    object dropdata = ((FrameworkElement)sender).DataContext;

                    if (dropdata is UIDevice)
                    {
                        UIDevice device = (UIDevice)dropdata;
                        group.Name = device.Groups.GetUniqueName<UIDetectorGroup>("Group");
                        group.Parent = device;
                        device.Groups.Add(group);
                        device.Expand = true;

                        e.Handled = true;
                    }
                }
            }
        }

        private void DetectorDrop(object sender, DragEventArgs e)
        {
            object obj = e.Data.GetData(typeof(UIDetector));
            if (!(obj is UIDetector))
            {
                obj = e.Data.GetData(typeof(UIRelay));
                if (!(obj is UIRelay))
                    obj = null;
            }

            if (obj != null)
            {
                if ((obj is UIDetector))
                {
                    UIDetector detector = (UIDetector)obj;

                    if (sender is FrameworkElement)
                    {
                        object dropdata = ((FrameworkElement)sender).DataContext;

                        if (dropdata is UIDetectorGroup)
                        {
                            UIDetectorGroup group = (UIDetectorGroup)dropdata;

                            if (detector.IsAnalog)
                            {
                                UIDetector d = ObjectCopier.Clone<UIDetector>(detector);
                                d.Name = group.IOs.GetUniqueName<Base>("AnalogInput");
                                d.IOType = "AnalogIn";
                                d.Parent = group;
                                group.IOs.Add(d);
                                group.Expand = true;
                                d.Expand = true;
                            }
                            else
                            {
                                UIDetector d = ObjectCopier.Clone<UIDetector>(detector);
                                d.Name = group.IOs.GetUniqueName<Base>("DigitalInput");
                                d.IOType = "DigitalIn";
                                d.Parent = group;
                                group.IOs.Add(d);
                                group.Expand = true;
                                d.Expand = true;
                            }

                            e.Handled = true;
                        }
                        //else if (dropdata is UIDetectors)
                        //{
                        //    UIDetectors detectorGroup = (UIDetectors)dropdata;

                        //    if ((detector.IsAnalog && detectorGroup.IOType == "AnalogIn") || (!detector.IsAnalog && detectorGroup.IOType == "DigitalIn"))
                        //    {
                        //        UIDetector d = ObjectCopier.Clone<UIDetector>(detector);
                        //        d.Name = detectorGroup.Detectors.GetUniqueName<UIDetector>("Pin");
                        //        d.Parent = detectorGroup;
                        //        detectorGroup.Detectors.Add(d);
                        //        detectorGroup.Expand = true;

                        //        e.Handled = true;
                        //    }
                        //}
                    }
                }
                else if ((obj is UIRelay))
                {
                    UIRelay relay = ObjectCopier.Clone<UIRelay>((UIRelay)obj);

                    if (sender is FrameworkElement)
                    {
                        object dropdata = ((FrameworkElement)sender).DataContext;

                        if (dropdata is UIDetectorGroup)
                        {
                            UIDetectorGroup card = (UIDetectorGroup)dropdata;

                            relay.Name = card.IOs.GetUniqueName<Base>("Relay");
                            relay.Parent = card;
                            card.IOs.Add(relay);

                            e.Handled = true;
                        }
                    }
                }
            }
        }

        private void Drop_PreviewDragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            e.Handled = true;
        }

        private void Drop_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            e.Handled = true;
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {// TODO instead of reloading local copy, reload server copy to obtain other users' changes.
            ClearDeviceList();
            OnSendMessage(MessageType.RequestDeviceDoc);

            //SetDevices(backupDevicesDoc); - this will be done when the doc is received from server.
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            DevicesDoc doc = BuildDeviceDoc();

            if (doc != null && SaveDevices != null)
            {
                SaveDevices(this, doc);
            }
        }

        private void MenuItemExpandAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext is IExpandable)
                    {
                        IExpandable expandableControl = (IExpandable)((FrameworkElement)sender).DataContext;

                        expandableControl.ExpandAll();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void MenuItemCollapseAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext is IExpandable)
                    {
                        IExpandable expandableControl = (IExpandable)((FrameworkElement)sender).DataContext;

                        expandableControl.CollapseAll();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void OnSendMessage(MessageType type, object data = null)
        {
            if (SendMessageEvent != null)
                SendMessageEvent.Invoke(new SendMessageEventArgs(type, data));
        }

        private void AddCustomPluginField_Button_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement fElement = sender as FrameworkElement;
            if (fElement != null)
            {
                UICustomPropertyList customPropertyList = fElement.DataContext as UICustomPropertyList;

                AddCustomPropertyField(customPropertyList);
            }
        }

        private void RemoveCustomPluginField_Button_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement fElement = sender as FrameworkElement;
            if (fElement != null)
            {
                UICustomPropertyList customPropertyList = fElement.DataContext as UICustomPropertyList;

                for (int i = customPropertyList.Properties.Count - 1; i >= 0; i--)
                {
                    UICustomProperty prop = customPropertyList.Properties[i];
                    customPropertyList.Properties.RemoveAt(i);
                    prop.Dispose();
                }
            }
        }

        private void btnWizard_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement fElement = sender as FrameworkElement;
            if (fElement != null)
            {
                UIDevice device = fElement.DataContext as UIDevice;
                if (device != null)
                {
                    if (device.PluginType.HasWizard)
                    {
                        PluginWizardWindow wizardWindow = new PluginWizardWindow();
                        try
                        {
                            wizardWindow.WizardControl = device.PluginType.Wizard;
                            wizardWindow.WizardControl.Plugin = device.PluginType;
                            wizardWindow.WizardControl.UIDevice = XMLSerializer.Serialize(device);
                            wizardWindow.WizardControl.TemplateActions = CreateSerializedList(actions.ToList<UIAlarmName>());

                            device.PluginType.Wizard.Saved -= WizardControl_Saved;
                            device.PluginType.Wizard.Saved += WizardControl_Saved;
                        wizardWindow.Show();
                        }
                        catch(Exception ex)
                        {
                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
                        }
                    }
                }
            }
        }

        private string[] CreateSerializedList<T>(List<T> list)
        {
            string[] resultList = new string[list.Count];

            for (int i = 0; i < list.Count; i++)
            {
                resultList[i] = XMLSerializer.Serialize(list[i]);
            }

            return resultList;
        }

        void WizardControl_Saved(object sender, PluginWizardSavedDetailsEventArgs e)
        {
            try
            {
                foreach (UIDevice device in devices)
                {
                    if (device.Name == e.DeviceName)
                    {
                        UIDevice newDevice = XMLSerializer.Deserialize(e.DeviceDetails) as UIDevice;

                        if (newDevice != null)
                        {
                            device.Properties = newDevice.Properties;
                            device.Groups = newDevice.Groups;
                        }

                        break;
                    }
                }

                foreach (string c_string in e.ConditionsToRemove)
                {
                    UICondition condition = XMLSerializer.Deserialize(c_string) as UICondition;

                    string[] hiddenFields = condition.HiddenField.Split(new char[] { ';', ',' });
                    string hiddenDevice = "";
                    string hiddenIP = "*";
                    string hiddenOID = "*";

                    if(hiddenFields.Length >= 3)
                    {
                        hiddenDevice = hiddenFields[0];
                        hiddenIP = hiddenFields[1];
                        hiddenOID = hiddenFields[2];
                    }

                    if (!string.IsNullOrWhiteSpace(hiddenDevice))
                    {
                        for (int i = this.conditions.Count - 1; i >= 0; i--)
                        {
                            UICondition c = this.conditions[i];

                            if (!string.IsNullOrWhiteSpace(c.HiddenField))
                            {
                                string[] c_hiddenFields = c.HiddenField.Split(new char[] { ';', ',' });
                                string c_hiddenDevice = "";
                                string c_hiddenIP = "";
                                string c_hiddenOID = "";

                                if (c_hiddenFields.Length >= 3)
                                {
                                    c_hiddenDevice = c_hiddenFields[0];
                                    c_hiddenIP = c_hiddenFields[1];
                                    c_hiddenOID = c_hiddenFields[2];

                                    if (c_hiddenDevice == hiddenDevice && !string.IsNullOrWhiteSpace(c_hiddenIP) && !string.IsNullOrWhiteSpace(c_hiddenOID)
                                        && (c_hiddenIP == hiddenIP && c_hiddenOID == hiddenOID
                                        || c_hiddenIP == hiddenIP && hiddenOID == "*"
                                        || hiddenIP == "*" && c_hiddenOID == hiddenOID
                                        || hiddenIP == "*" && hiddenOID == "*"))
                                    {
                                        this.conditions.RemoveAt(i);
                                    }
                                }
                            }
                        }
                    }
                }

                foreach (string c_string in e.Conditions)
                {
                    UICondition c = XMLSerializer.Deserialize(c_string) as UICondition;
                    
                    if(c != null)
                        this.conditions.Add(c);
                }

                foreach (string a_string in e.ActionsToRemove)
                {
                    UIAlarmName action = XMLSerializer.Deserialize(a_string) as UIAlarmName;

                    string[] hiddenFields = action.HiddenField.Split(new char[] { ';', ',' });
                    string hiddenDevice = "";
                    string hiddenIP = "*";
                    string hiddenOID = "*";

                    if (hiddenFields.Length >= 3)
                    {
                        hiddenDevice = hiddenFields[0];
                        hiddenIP = hiddenFields[1];
                        hiddenOID = hiddenFields[2];
                    }

                    if (!string.IsNullOrWhiteSpace(hiddenDevice))
                    {
                        for (int i = this.actions.Count - 1; i >= 0; i--)
                        {
                            UIAlarmName a = this.actions[i];

                            if (!string.IsNullOrWhiteSpace(a.HiddenField))
                            {
                                string[] a_hiddenFields = a.HiddenField.Split(new char[] { ';', ',' });
                                string a_hiddenDevice = "";
                                string a_hiddenIP = "";
                                string a_hiddenOID = "";

                                if (a_hiddenFields.Length >= 3)
                                {
                                    a_hiddenDevice = a_hiddenFields[0];
                                    a_hiddenIP = a_hiddenFields[1];
                                    a_hiddenOID = a_hiddenFields[2];

                                    if (a_hiddenDevice == hiddenDevice && !string.IsNullOrWhiteSpace(a_hiddenIP) && !string.IsNullOrWhiteSpace(a_hiddenOID)
                                        && (a_hiddenIP == hiddenIP && a_hiddenOID == hiddenOID
                                        || a_hiddenIP == hiddenIP && hiddenOID == "*"
                                        || hiddenIP == "*" && a_hiddenOID == hiddenOID
                                        || hiddenIP == "*" && hiddenOID == "*"))
                                    {
                                        this.actions.RemoveAt(i);
                                    }
                                }
                            }
                        }
                    }
                }

                foreach (string a_string in e.Actions)
                {
                    UIAlarmName a = XMLSerializer.Deserialize(a_string) as UIAlarmName;

                    if (a != null)
                        this.actions.Add(a);
                }

                if (MessageBox.Show("The wizzard made some changes, would you like to save it to the server?", "Save changes", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    DevicesDoc doc = BuildDeviceDoc();

                    if (doc != null && SaveDevices != null)
                    {
                        SaveDevices(this, doc);
                    }

                    XMLDoc conditionsDoc = ConditionsControl.BuildConditionsDoc(this.conditions);

                    if (conditionsDoc != null)
                        MainWindow.Client.SendMessage(MessageType.ConditionsDoc, conditionsDoc);

                    XMLDoc actionsDoc = ActionsControl.BuildActionsDoc(this.actions);

                    if (actionsDoc != null)
                        MainWindow.Client.SendMessage(MessageType.ActionsDoc, actionsDoc);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void MultipleDevices_Click(object sender, RoutedEventArgs e)
        {
            if (lbDeviceTypes == null)
                return;
            else
            {
                try
                {
                    INervePlugin selectedType;
                    if (sender == null)
                        selectedType = lbDeviceTypes.Items.CurrentItem as INervePlugin;
                    else
                        selectedType = (sender as MenuItem).DataContext as INervePlugin;

                    if ((selectedType is ICameraServerPlugin) || (selectedType is ICameraPlugin))
                    {
                        MessageBox.Show("Unable to add camera serve rplugins as it contains child devices.");
                    }
                    else
                    {
                        var deviceTypes = lbDeviceTypes.DataContext as ObservableCollection<INervePlugin>;
                        if (deviceTypes != null)
                        {
                            DeviceWizard.DeviceWizardDialog deviceDialog = new DeviceWizard.DeviceWizardDialog(deviceTypes, selectedType);
                            if (deviceDialog.ShowDialog() == true) // Successfully created devices, and clicked finish.
                            {
                                if ((deviceDialog.NewDevices != null) && (deviceDialog.NewDevices.Count > 0))
                                {
                                    foreach (Base device in deviceDialog.NewDevices)
                                    {
                                        UIDevice existingDevice = devices.FirstOrDefault(a => a.Name == device.Name) as UIDevice;
                                        if (existingDevice != null)
                                        {
                                            UIDevice tempDevice = device as UIDevice;
                                            if (tempDevice != null)
                                            {
                                                device.Name = devices.GetUniqueName(tempDevice.PluginType.Name);
                                            }
                                        }
                                        devices.Add(device);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) // When item is not selected anymore, can't continue.
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                        MethodBase.GetCurrentMethod().Name), "", ex);
                }
            }
        }
    }
}
