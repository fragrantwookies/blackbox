﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BlackBoxEngine;
using eThele.Essentials;
using eNerve.DataTypes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for AlarmScheduleControl.xaml
    /// </summary>
    public partial class AlarmScheduleControl : UserControl
    {
        private ObservableCollection<string> alarmNames;
        private ObservableCollection<Schedule> scheduleItems;
        private List<Schedule> scheduleItemsBackup; //we get this in any case, so might as well keep it and use it for the Reload function.

        public delegate void ScheduleEventHandler(object sender, List<Schedule> _scheduleItems);
        public event ScheduleEventHandler SaveSchedule;

        public AlarmScheduleControl()
        {
            InitializeComponent();

            alarmNames = new ObservableCollection<string>();
            cboAlarmNamesAdd.DataContext = alarmNames;
            cboAlarmNamesAdd.ItemsSource = alarmNames;

            scheduleItems = new ObservableCollection<Schedule>();
            lbSchedule.DataContext = scheduleItems;

            scheduleItemsBackup = new List<Schedule>();
        }

        public delegate void AlarmNamesDelegate(List<string> _names);
        public void SetAlarmNames(List<string> _names)
        {
            if (this.CheckAccess())
            {
                alarmNames.Clear();

                foreach (string name in _names)
                {
                    alarmNames.Add(name);
                }

                alarmNames.Add("Everything");
            }
            else
            {
                this.Dispatcher.Invoke(new AlarmNamesDelegate(SetAlarmNames), new object[] { _names });
            }
        }

        private void ClearSchedule()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (scheduleItems != null)
                    {
                        for (int i = scheduleItems.Count - 1; i >= 0; i--)
                        {
                            Schedule schedule = scheduleItems[i];
                            scheduleItems.Remove(schedule);
                            schedule.Dispose();
                        }
                    }
                }
                else
                {
                    this.Dispatcher.Invoke(ClearSchedule);
                }

                if (scheduleItemsBackup != null)
                {
                    for (int i = scheduleItemsBackup.Count - 1; i >= 0; i--)
                    {
                        Schedule schedule = scheduleItemsBackup[i];
                        scheduleItemsBackup.Remove(schedule);
                        schedule.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public delegate void AlarmScheduleDelegate(List<Schedule> _schedule);
        public void SetSchedule(List<Schedule> _schedule)
        {   
            if (this.CheckAccess())
            {
                ClearSchedule();

                foreach (Schedule schedule in _schedule)
                {
                    scheduleItems.Add(schedule);

                    scheduleItemsBackup.Add(ObjectCopier.Clone<Schedule>(schedule));
                }
            }
            else
                this.Dispatcher.Invoke(new AlarmScheduleDelegate(SetSchedule), new object[] { _schedule });
        }        

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext != null)
                    {
                        Schedule schedule = (Schedule)((FrameworkElement)sender).DataContext;

                        schedule.Expand = !schedule.Expand;
                    }
                }
            }
            catch(Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (SaveSchedule != null)
            {
                List<Schedule> result = null;

                try
                {
                    result = new List<Schedule>();
                    foreach (Schedule item in scheduleItems)
                        result.Add(item);

                    SaveSchedule(this, result);
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                }
                finally
                {
                    if (result != null)
                    {
                        for (int i = result.Count - 1; i >= 0; i--)
                        {
                            Schedule itm = result[i];
                            result.Remove(itm);
                            itm.Dispose();
                        }
                    }
                }
            }
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            if (scheduleItems != null)
            {
                for (int i = scheduleItems.Count - 1; i >= 0; i--)
                {
                    Schedule schedule = scheduleItems[i];
                    scheduleItems.Remove(schedule);
                    schedule.Dispose();
                }
            }

            foreach (Schedule schedule in scheduleItemsBackup)
            {
                scheduleItems.Add(ObjectCopier.Clone<Schedule>(schedule));
            }
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            Schedule item = new Schedule();
            item.Name = cboAlarmNamesAdd.Text;
            scheduleItems.Add(item);
        }

        private void AddScheduleZoneButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext != null)
                    {
                        Schedule schedule = (Schedule)((FrameworkElement)sender).DataContext;

                        ScheduleZone zone = new ScheduleZone();
                        zone.Name = "Zone";
                        zone.DayOfTheWeek = schedule.NewZoneDayOfTheWeek;
                        zone.OffTime = schedule.NewZoneOffTime;
                        zone.OnTime = schedule.NewZoneOnTime;
                        schedule.Zones.Add(zone);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void DeleteZoneButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    FrameworkElement zoneItem = (FrameworkElement)sender;

                    if (zoneItem.DataContext is ScheduleZone)  
                    {
                        ScheduleZone zone = (ScheduleZone)zoneItem.DataContext;

                        foreach(Schedule schedule in scheduleItems)
                        {
                            if (schedule.Zones.Contains(zone))
                            {
                                schedule.Zones.Remove(zone);
                                break;
                            }

                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void DeleteScheduleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    FrameworkElement scheduleItem = (FrameworkElement)sender;

                    if (scheduleItem.DataContext is Schedule)
                    {
                        Schedule schedule = (Schedule)scheduleItem.DataContext;

                        scheduleItems.Remove(schedule);

                        schedule.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
    }
}
