﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using eThele.Essentials;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for PluginsControl.xaml
    /// </summary>
    public partial class PluginsControl : UserControl, IDisposable
    {
        public event EventHandler PluginTypeMessage;

        public PluginsControl()
        {
            InitializeComponent();
        }

        private FrameworkElement dragged;

        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);
            
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            SendPluginTypeMessage();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
           /* if ((plugins != null) && (plugins))
            {
                SendPluginTypeMessage(plugins); 
            }*/
        }

        private void DeletePluginDrop(object sender, DragEventArgs e)
        {

        }

        private void DoDragDrop(object parameter)
        {
            try
            {
                if (dragged != null)
                {
                    DragDrop.DoDragDrop(dragged, parameter, DragDropEffects.All);
                    dragged = null;
                }
            }
            catch (Exception ex)
            {
                dragged = null;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name,
                    System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void NewPluginDrop(object sender, DragEventArgs e)
        {

        }

        private void OnSendPluginTypeMessage()
        {
            if (PluginTypeMessage != null)
                PluginTypeMessage(this, new EventArgs());
        }

        private void PluginsControl_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (dragged != null)
                return;

            if (sender is UIElement)
            {
                UIElement lb = (UIElement)sender;

                UIElement element = lb.InputHitTest(e.GetPosition(lb)) as UIElement;

                while (element != null)
                {
                    if (element is FrameworkElement)
                    {
                        dragged = (FrameworkElement)element;
                        break;
                    }
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }
            }
        }

        private void PluginsControl_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (dragged == null)
                    return;
                if (e.LeftButton == MouseButtonState.Released)
                {
                    dragged = null;
                    return;
                }
                else if (e.LeftButton == MouseButtonState.Pressed)
                {
                        object data = dragged.DataContext;
                    if (data is UIAlarmName || data is UIAction || data is UIPluginActionCustomBaseField)
                    {
                        Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new System.Threading.ParameterizedThreadStart(DoDragDrop),
                            data);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                dragged = null;
            }
        }

        private void SendPluginTypeMessage()
        {
            OnSendPluginTypeMessage();
        }
    }
}
