﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BlackBoxEngine;
using eThele.Essentials;
using eNerve.DataTypes;
using System.Reflection;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for AlarmDetail.xaml
    /// </summary>
    public partial class AlarmDetail : UserControl
    {
        public delegate void AlarmResolveEventHandler(object sender, AlarmBase _alarm, AlarmResolve _resolveDetailItem);
        public event AlarmResolveEventHandler AlarmResolved;

        public delegate void SiteInstructionStepEventHandler(object sender, SiteInstructionStep _siteInstructionStep);
        public event SiteInstructionStepEventHandler SiteInstructionStepChanged;

        public AlarmDetail()
        {
            InitializeComponent();
        }

        public AlarmBase Alarm
        {
            get
            {
                return this.DataContext is AlarmBase ? (AlarmBase)this.DataContext : null;
            }
            set
            {
                this.DataContext = value;
            }
        }

        private void btnAddResolveItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AlarmBase alarmToResolve = Alarm;

                if (alarmToResolve.CanResolve)
                {
                    AlarmType alarmType = (AlarmType)cboResolveAlarmType.SelectedValue;

                    if (!string.IsNullOrWhiteSpace(txtresolveDescription.Text) && alarmType != AlarmType.Unresolved && alarmToResolve != null)
                    {
                        alarmToResolve.Alarm_Type = (AlarmType)cboResolveAlarmType.SelectedValue;

                        AlarmResolve resolveDetailItem = new AlarmResolve() { AlarmID = alarmToResolve.ID, ResolveID = Guid.NewGuid(), ResolveDT = DateTime.Now, Alarm_Type = alarmToResolve.Alarm_Type, Description = txtresolveDescription.Text, User = MainWindow.CurrentUser};

                        alarmToResolve.Resolve(resolveDetailItem);

                        lbResolveItems.Items.Refresh();

                        if (AlarmResolved != null)
                            AlarmResolved(this, alarmToResolve, resolveDetailItem);
                    }
                    else
                        MessageBox.Show("Make sure to specify a description and alarm type.", "Can't resolve", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                    MessageBox.Show("Make sure all compulsory site instructions are completed before resolving the alarm.", "Can't resolve", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void btnAddComment_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtComment.Text))
            {
                AlarmComment alarmComment = new AlarmComment() { AlarmID = Alarm.ID, CommentID = Guid.NewGuid(), CommentDT = DateTime.Now, Comment = txtComment.Text, User = MainWindow.CurrentUser };
                Alarm.AddComment(alarmComment);

                MainWindow.Client.SendMessage(MessageType.AlarmComment, alarmComment);
            }

            txtComment.Text = string.Empty;
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (SiteInstructionStep siteInstructionStep in e.AddedItems)
            {
                siteInstructionStep.Completed = true;
                siteInstructionStep.CompletedDT = DateTime.Now;
                siteInstructionStep.User = MainWindow.CurrentUser;

                if (SiteInstructionStepChanged != null)
                    SiteInstructionStepChanged(this, siteInstructionStep);
            }

            foreach (SiteInstructionStep siteInstructionStep in e.RemovedItems)
            {
                siteInstructionStep.Completed = false;
                siteInstructionStep.CompletedDT = DateTime.MinValue;
                siteInstructionStep.User = MainWindow.CurrentUser;

                if (SiteInstructionStepChanged != null)
                    SiteInstructionStepChanged(this, siteInstructionStep);
            }
        }

        private void btnImages_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext != null)
                    {
                        Event eventItem = ((FrameworkElement)sender).DataContext as Event;

                        if (eventItem != null)
                        {
                            if (eventItem.Images.Count > 0)
                            {
                                if (eventItem.Images[0].Dewarp)
                                {
                                    GrandEyeWindow grandEyeWindow = new GrandEyeWindow();
                                    grandEyeWindow.ImageData = eventItem.Images[0].ImageData;
                                    grandEyeWindow.Show();
                                }
                                else
                                {
                                    FlatImageViewer flatImageViewer = new FlatImageViewer();
                                    flatImageViewer.Show();
                                    flatImageViewer.LoadImages(eventItem.Images);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "", ex);
            }
        }
    }
}
