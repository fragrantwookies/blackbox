﻿using eNerve.DataTypes.Gauges;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_UI.Controls
{
    /// <summary>
    /// Interaction logic for GaugePropertiesControl.xaml
    /// </summary>
    public partial class GaugePropertiesControl : UserControl
    {
        List<string> eventNames;

        public GaugePropertiesControl()
        {
            InitializeComponent();
        }

        public void SetGauge(Gauge _gauge, List<string> _eventNames)
        {
            this.DataContext = _gauge;
            eventNames = _eventNames;

            mainGrid.Children.Clear();

            mainGrid.RowDefinitions.Clear();

            int rowCount = 0;

            if(_gauge != null)
            {
                PropertyInfo info = (_gauge.GetType()).GetProperty("EventName");
                if(info != null)
                {
                    mainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25) });

                    Label label = new Label() { Margin = new Thickness(0, 0, 5, 0), Content = "Event Name" };                    
                    mainGrid.Children.Add(label);
                    Grid.SetRow(label, rowCount);
                    Grid.SetColumn(label, 0);

                    object val = info.GetValue(_gauge);

                    ComboBox dataControl = new ComboBox() { ItemsSource = eventNames, MinWidth = 50 };
                    dataControl.SetBinding(ComboBox.SelectedItemProperty, new Binding(info.Name));
                    mainGrid.Children.Add(dataControl);
                    Grid.SetRow(dataControl, rowCount);
                    Grid.SetColumn(dataControl, 1);

                    rowCount++;
                }

                info = (_gauge.GetType()).GetProperty("StartValue");
                if (info != null)
                {
                    mainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25) });

                    Label label = new Label() { Margin = new Thickness(0, 0, 5, 0), Content = "Start Value" };
                    mainGrid.Children.Add(label);
                    Grid.SetRow(label, rowCount);
                    Grid.SetColumn(label, 0);

                    object val = info.GetValue(_gauge);

                    TextBox dataControl = new TextBox() { MinWidth = 50 };
                    dataControl.SetBinding(TextBox.TextProperty, new Binding(info.Name));
                    mainGrid.Children.Add(dataControl);
                    Grid.SetRow(dataControl, rowCount);
                    Grid.SetColumn(dataControl, 1);

                    rowCount++;
                }

                info = (_gauge.GetType()).GetProperty("EndValue");
                if (info != null)
                {
                    mainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25) });

                    Label label = new Label() { Margin = new Thickness(0, 0, 5, 0), Content = "End Value" };
                    mainGrid.Children.Add(label);
                    Grid.SetRow(label, rowCount);
                    Grid.SetColumn(label, 0);

                    object val = info.GetValue(_gauge);

                    TextBox dataControl = new TextBox() { Text = val.ToString(), MinWidth = 50 };
                    dataControl.SetBinding(TextBox.TextProperty, new Binding(info.Name));
                    mainGrid.Children.Add(dataControl);
                    Grid.SetRow(dataControl, rowCount);
                    Grid.SetColumn(dataControl, 1);

                    rowCount++;
                }

                info = (_gauge.GetType()).GetProperty("MajorIntervalCount");
                if (info != null)
                {
                    mainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25) });

                    Label label = new Label() { Margin = new Thickness(0, 0, 5, 0), Content = "Major Interval Count" };
                    mainGrid.Children.Add(label);
                    Grid.SetRow(label, rowCount);
                    Grid.SetColumn(label, 0);

                    object val = info.GetValue(_gauge);

                    TextBox dataControl = new TextBox() { Text = val.ToString(), MinWidth = 50 };
                    dataControl.SetBinding(TextBox.TextProperty, new Binding(info.Name));
                    mainGrid.Children.Add(dataControl);
                    Grid.SetRow(dataControl, rowCount);
                    Grid.SetColumn(dataControl, 1);

                    rowCount++;
                }

                info = (_gauge.GetType()).GetProperty("MinorIntervalCount");
                if (info != null)
                {
                    mainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25) });

                    Label label = new Label() { Margin = new Thickness(0, 0, 5, 0), Content = "Minor Interval Count" };
                    mainGrid.Children.Add(label);
                    Grid.SetRow(label, rowCount);
                    Grid.SetColumn(label, 0);

                    object val = info.GetValue(_gauge);

                    TextBox dataControl = new TextBox() { Text = val.ToString(), MinWidth = 50 };
                    dataControl.SetBinding(TextBox.TextProperty, new Binding(info.Name));
                    mainGrid.Children.Add(dataControl);
                    Grid.SetRow(dataControl, rowCount);
                    Grid.SetColumn(dataControl, 1);

                    rowCount++;
                }

                info = (_gauge.GetType()).GetProperty("StartAngle");
                if (info != null)
                {
                    mainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25) });

                    Label label = new Label() { Margin = new Thickness(0, 0, 5, 0), Content = "Start Angle" };
                    mainGrid.Children.Add(label);
                    Grid.SetRow(label, rowCount);
                    Grid.SetColumn(label, 0);

                    object val = info.GetValue(_gauge);

                    TextBox dataControl = new TextBox() { Text = val.ToString(), MinWidth = 50 };
                    dataControl.SetBinding(TextBox.TextProperty, new Binding(info.Name));
                    mainGrid.Children.Add(dataControl);
                    Grid.SetRow(dataControl, rowCount);
                    Grid.SetColumn(dataControl, 1);

                    rowCount++;
                }

                info = (_gauge.GetType()).GetProperty("EndAngle");
                if (info != null)
                {
                    mainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(25) });

                    Label label = new Label() { Margin = new Thickness(0, 0, 5, 0), Content = "End Angle" };
                    mainGrid.Children.Add(label);
                    Grid.SetRow(label, rowCount);
                    Grid.SetColumn(label, 0);

                    object val = info.GetValue(_gauge);

                    TextBox dataControl = new TextBox() { Text = val.ToString(), MinWidth = 50 };
                    dataControl.SetBinding(TextBox.TextProperty, new Binding(info.Name));
                    mainGrid.Children.Add(dataControl);
                    Grid.SetRow(dataControl, rowCount);
                    Grid.SetColumn(dataControl, 1);

                    rowCount++;
                }
            }
        }
    }
}
