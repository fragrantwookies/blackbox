﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for BrowserControl.xaml
    /// </summary>
    public partial class BrowserControl : UserControl
    {
        public BrowserControl()
        {
            InitializeComponent();
            webBrowser.Navigated += Browser_Navigated;
            if (!string.IsNullOrWhiteSpace(txtAddress.Text))
                Button_Click(this, null);
        }
        public string Address
        {
            get { return txtAddress.Text; }
            set { txtAddress.Text = value; Button_Click(this, null); }
        }

        private void Browser_Navigated(object sender, NavigationEventArgs e)
        {
            if (webBrowser != null)
            {
                if (webBrowser.Document is IOleServiceProvider sp)
                {
                    Guid IID_IWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");
                    Guid IID_IWebBrowser2 = new Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E");

                    object webBrowser;
                    sp.QueryService(ref IID_IWebBrowserApp, ref IID_IWebBrowser2, out webBrowser);
                    if (webBrowser != null)
                    {
                        webBrowser.GetType().InvokeMember("Silent", BindingFlags.Instance | BindingFlags.Public | BindingFlags.PutDispProperty, null, webBrowser, new object[] { true });
                    }
                }
            }
        }

        private void BtnExpand_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (btnExpand.Text == "▲")
            {
                rowAddress.Height = new GridLength(0);
                btnExpand.Text = "▼";
            }
            else
            {
                rowAddress.Height = new GridLength(30);
                btnExpand.Text = "▲";
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            webBrowser.Navigate(txtAddress.Text);
        }

        [ComImport, Guid("6D5140C1-7436-11CE-8034-00AA006009FA"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        private interface IOleServiceProvider
        {
            [PreserveSig]
            int QueryService([In] ref Guid guidService, [In] ref Guid riid, [MarshalAs(UnmanagedType.IDispatch)] out object ppvObject);
        }
    }
}
