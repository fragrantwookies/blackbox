﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BlackBoxEngine;
using eThele.Essentials;
using eNerve.DataTypes;
using eNerve.DataTypes.Gauges;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for MapSettingsControl.xaml
    /// </summary>
    public partial class MapSetupControl : UserControl, IDisposable
    {
        private ObservableCollection<Base> maps;
        private MapDoc backupMapDoc;

        public delegate void MapDocEventHandler(object sender, MapDoc _doc);
        public event MapDocEventHandler SaveMaps; 

        public MapSetupControl()
        {
            InitializeComponent();

            maps = new ObservableCollection<Base>();
            lbMaps.DataContext = maps;
            imgMap.DataContext = new UIMap();
            imgDashboard.DataContext = new Dashboard();
        }

        ~MapSetupControl()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (Dispatcher.CheckAccess())
            {
                ClearMapList();

                if (imgMap != null)
                {
                    if (imgMap.DataContext is IDisposable)
                        ((IDisposable)imgMap.DataContext).Dispose();
                }
            }
            else
                try
                {
                    Dispatcher.Invoke(() => { Dispose(disposing); });
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, 
                        System.Reflection.MethodBase.GetCurrentMethod().Name), "Error disposing MapSetupControl: ", ex);
                }
        }

        private void ClearMapList()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (maps != null)
                    {
                        for (int i = maps.Count - 1; i >= 0; i--)
                        {
                            Base map = maps[i];
                            maps.RemoveAt(i);
                            map.Dispose();
                        }
                    }
                }
                else
                {
                    this.Dispatcher.Invoke(ClearMapList);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private delegate void MapDocDelegate(MapDoc _mapDoc);
        public void SetMap(MapDoc _mapDoc)
        {
            try
            {
                if (_mapDoc != null)
                {
                    ClearMapList();

                    backupMapDoc = _mapDoc;

                    if (CheckAccess())
                    {
                        foreach (XmlNode mapNode in _mapDoc)
                        {
                            UIMap map = ParseMapNode(mapNode);
                            if (map != null)
                                maps.Add(map);

                            break; //only 1 map in root
                        }
                    }
                    else
                        Dispatcher.Invoke(new MapDocDelegate(SetMap), _mapDoc);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private UIMap ParseMapNode(XmlNode _mapNode)
        {
            UIMap result = null;

            try
            {
                if (_mapNode != null)
                {
                    UIMap map = result = new UIMap();
                    map.Name = _mapNode.Name;
                    map.Title = XmlNode.ParseString(_mapNode["Title"], _mapNode.Name);
                    map.ImagePath = XmlNode.ParseString(_mapNode["BackgoundImage"], "");
                    map.ThumbnailPath = XmlNode.ParseString(_mapNode["ThumbnailImage"], "");
                    map.ThumbnailX = XmlNode.ParseDouble(_mapNode["ThumbnailImage"], 0, "Left");
                    map.ThumbnailY = XmlNode.ParseDouble(_mapNode["ThumbnailImage"], 0, "Top");
                    map.ThumbnailWidth = XmlNode.ParseDouble(_mapNode["ThumbnailImage"], 0, "Width");
                    map.ThumbnailHeight = XmlNode.ParseDouble(_mapNode["ThumbnailImage"], 0, "Height");
                    map.ThumbnailRotation = XmlNode.ParseDouble(_mapNode["ThumbnailImage"], 0, "Rotation");
                    map.DefaultIconWidth = XmlNode.ParseDouble(_mapNode["DefaultIconWidth"], 0);
                    map.DefaultIconHeight = XmlNode.ParseDouble(_mapNode["DefaultIconHeight"], 0);

                    if (_mapNode["Maps"] != null)
                    {
                        foreach (XmlNode childMapNode in _mapNode["Maps"])
                        {
                            if (string.IsNullOrEmpty(childMapNode.Value))
                            {
                                UIMap childMap = ParseMapNode(childMapNode);
                                if (childMap != null)
                                {
                                    map.Maps.Add(childMap);
                                    childMap.Parent = map;
                                }
                            }
                            else
                            {
                                Dashboard dashboard = ParseDashboardNode(childMapNode);
                                if (dashboard != null)
                                {
                                    map.Maps.Add(dashboard);
                                    dashboard.Parent = map; // Redundant ?
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private Dashboard ParseDashboardNode(XmlNode childMapNode)
        {
            Dashboard result = null;
            try
            {
                if (childMapNode != null)
                {
                    result = XMLSerializer.Deserialize(childMapNode.Value) as Dashboard;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public MapDoc BuildMapDoc()
        {
            MapDoc result = null;

            try
            {
                result = new MapDoc("Maps");

                foreach(UIMap map in maps)
                {
                    XmlNode node = BuildNode(map);

                    if (node != null)
                        result.Add(node);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public XmlNode BuildNode(Dashboard _dashboard, string _nodeName = "")
        {
            XmlNode result = null;
            try
            {
                result = new XmlNode(_dashboard.Name, XMLSerializer.Serialize(_dashboard));
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                result = null;
            }
            
            return result;
        }

        private XmlNode BuildNode(UIMap _map, string _nodeName = "")
        {
            XmlNode result = null;

            try
            {
                XmlNode mapNode = result = new XmlNode(_map.Name.Replace('_', ' '), "");
                mapNode.Add("Title", _map.Title);
                mapNode.Add("BackgoundImage", _map.ImagePath);

                if (!string.IsNullOrWhiteSpace(_map.ThumbnailPath))
                {
                    XmlNode imageNode = mapNode.Add("ThumbnailImage", _map.ThumbnailPath);
                    imageNode.AddAttribute("Left", _map.ThumbnailX.ToString());
                    imageNode.AddAttribute("Top", _map.ThumbnailY.ToString());

                    if (_map.ThumbnailHeight > 0)
                        imageNode.AddAttribute("Height", _map.ThumbnailHeight.ToString());
                    if (_map.ThumbnailWidth > 0)
                        imageNode.AddAttribute("Width", _map.ThumbnailWidth.ToString());
                    if (_map.ThumbnailRotation > 0)
                        imageNode.AddAttribute("Rotation", _map.ThumbnailRotation.ToString());
                }

                if(_map.DefaultIconWidth > 0)
                    mapNode.Add("DefaultIconWidth", _map.DefaultIconWidth.ToString());
                if(_map.DefaultIconHeight > 0)
                    mapNode.Add("DefaultIconHeight", _map.DefaultIconHeight.ToString());

                if (_map.Maps.Count > 0)
                {
                    XmlNode childMapsNode = mapNode.Add("Maps", "");
                    foreach (Base child in _map.Maps)
                    {
                        UIMap childMap = child as UIMap;
                        if (childMap != null)
                            childMapsNode.Add(BuildNode(childMap));
                        else
                        {
                            Dashboard dashboard = child as Dashboard;
                            if (dashboard != null)
                            {
                                childMapsNode.Add(BuildNode(dashboard));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement && ((FrameworkElement)sender).DataContext is IExpandable)
                {
                    IExpandable expandableControl = (IExpandable)((FrameworkElement)sender).DataContext;

                    if (e.LeftButton == MouseButtonState.Pressed)
                        expandableControl.Expand = !expandableControl.Expand;
                    else if (e.RightButton == MouseButtonState.Pressed)
                    {
                        if (expandableControl.Expand)
                            expandableControl.CollapseAll();
                        else
                            expandableControl.ExpandAll();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        #region DragAndDrop
        private FrameworkElement _dragged;

        private void DragDrop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_dragged != null)
                return;

            if (sender is UIElement)
            {
                UIElement lb = (UIElement)sender;

                UIElement element = lb.InputHitTest(e.GetPosition(lb)) as UIElement;

                while (element != null)
                {
                    if (element is FrameworkElement)
                    {
                        _dragged = (FrameworkElement)element;
                        break;
                    }
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }
            }
        }

        private void DragDrop_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (_dragged == null)
                    return;
                if (e.LeftButton == MouseButtonState.Released)
                {
                    _dragged = null;
                    return;
                }
                else if (e.LeftButton == MouseButtonState.Pressed)
                {
                    if (_dragged is FrameworkElement)
                    {
                        object data = ((FrameworkElement)_dragged).DataContext;
                        if (data is Base)
                        {
                            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new System.Threading.ParameterizedThreadStart(DoDragDrop), data);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                _dragged = null;
            }
        }

        private void DoDragDrop(object parameter)
        {
            if (_dragged != null)
            {
                System.Windows.DragDrop.DoDragDrop(_dragged, parameter, DragDropEffects.All);
                _dragged = null;
            }
        }

        private void NewDrop(object sender, DragEventArgs e)
        {
            object mapObj = e.Data.GetData(typeof(UIMap));
            if (!(mapObj is UIMap))
            {
                mapObj = null;
            }

            UIMap map = ObjectCopier.Clone((UIMap)mapObj);

            if (map is UIMap)
            {
                if (maps.Count == 0)
                {
                    map.Name = maps.GetUniqueName("Map");
                    maps.Add(map);
                }
                else
                    MessageBox.Show("There can be only 1 map in the root");
            }
            e.Handled = true;
        }

        private void DeleteDrop(object sender, DragEventArgs e)
        {
            object obj = e.Data.GetData(typeof(UIMap));
            if (obj is UIMap)
            {
                UIMap map = (UIMap)obj;

                if (map.Parent != null)
                {
                    if (Dispatcher.CheckAccess())
                        map.Parent.RemoveChild(map);
                    else
                        Dispatcher.Invoke(new Base.RemoveChildDelegate(map.Parent.RemoveChild), map);
                }
                else
                {
                    maps.Remove(map);
                }

                map.Dispose();
            }
            else
            {
                Dashboard dashboard = e.Data.GetData(typeof(Dashboard)) as Dashboard;
                if (dashboard != null)
                {
                    if (dashboard.Parent != null)
                    {
                        if (Dispatcher.CheckAccess())
                            dashboard.Parent.RemoveChild(dashboard);
                        else
                            Dispatcher.Invoke(new Base.RemoveChildDelegate(dashboard.Parent.RemoveChild), dashboard);
                    }
                    dashboard.Dispose();
                }
            }

            e.Handled = true;
        }

        private void ChilMapDrop(object sender, DragEventArgs e)
        {
            object obj = e.Data.GetData(typeof(UIMap));
            if ((obj is UIMap))
            {
                UIMap map = ObjectCopier.Clone((UIMap)obj);

                if (sender is FrameworkElement)
                {
                    object dropdata = ((FrameworkElement)sender).DataContext;

                    if (dropdata is UIMap)
                    {
                        UIMap parentMap = (UIMap)dropdata;
                        map.Name = parentMap.Maps.GetUniqueName("Map");
                        map.Parent = parentMap;
                        parentMap.Maps.Add(map);
                        parentMap.Expand = true;

                        e.Handled = true;
                    }
                }
            }
            else
            {
                obj = e.Data.GetData(typeof(Dashboard));
                if (obj is Dashboard)
                {
                    Dashboard dashboard = ObjectCopier.Clone((Dashboard)obj);
                    if (sender is FrameworkElement)
                    {
                        object dropdata = ((FrameworkElement)sender).DataContext;

                        if (dropdata is UIMap)
                        {
                            UIMap parentMap = (UIMap)dropdata;
                            dashboard.Title = parentMap.Maps.GetUniqueName(parentMap.Name + " Dashboard");
                            dashboard.Name = dashboard.Title;
                            dashboard.Parent = parentMap;
                            dashboard.Rows = 1;
                            dashboard.Columns = 1;
                            parentMap.Maps.Add(dashboard);
                            parentMap.Expand = true;

                            e.Handled = true;
                        }
                    }
                }
            }
        }
        #endregion DragAndDrop

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            //ClearMapList(); // This function is called in SetMap
            SetMap(backupMapDoc);
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            MapDoc doc = BuildMapDoc();

            if (doc != null && SaveMaps != null)
            {
                SaveMaps(this, doc);
            }
        }

        private void Image_MouseLeave(object sender, MouseEventArgs e)
        {
            if (sender is Image)
                ((Image)sender).Height = 70;
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is Image)
                ((Image)sender).Height = 70;
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Image)
                ((Image)sender).Height = 140;
        }
    }
}
