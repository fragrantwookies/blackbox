﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using BlackBoxEngine;
using eNerve.DataTypes;
using eThele.Essentials;

namespace WPF_UI
{
    public class VideoDevice
    {
        public string Name { get; set; }
        public System.Net.IPEndPoint EndPoint { get; set; }
    }
    public class VideoDevices
    {
        public ObservableCollection<VideoDevice> Devices { get; set; }
    }
    /// <summary>
    /// Interaction logic for SystemConfig.xaml
    /// </summary>
    public partial class SystemConfig : UserControl, IDisposable
    {
        public SystemConfig()
        {
            InitializeComponent();
        }
        public VideoDevices Cameras;
        public VideoDevices Servers;
        public delegate void AuthenticateEventHandler(AuthenticateEventArgs e);
        public AuthenticateEventHandler AuthenticateEvent;

        public event EventHandler ClosingTime;

        private Thread changePasswordThread;

        public ObservableCollection<User> ExistingUsers;

        public delegate void SendMessageEventHandler(SendMessageEventArgs e);
        public SendMessageEventHandler SendMessageEvent;

        private CameraLayouts selectedLayout = CameraLayouts.None;
        private List<string> videoHosts = new List<string>();

        public void ShowRights(User showUser)
        {
            try
            {
                boxUserRights.Items.Clear();
                boxUserRights.Items.Add(showUser.Rights);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "Error displaying user rights", ex);
            }
        }

        public void ChangePassword(string message)
        {
            try
            {
                if (changePasswordThread != null)
                {
                    changePasswordThread.Abort();
                }
                changePasswordThread = new Thread(new ThreadStart(() => ChangePasswordSP(message)));
                changePasswordThread.SetApartmentState(ApartmentState.STA);
                changePasswordThread.IsBackground = true;
                changePasswordThread.Start();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "Error changing password", ex);
            }
        }

        private void ChangePasswordSP(string message)
        {
            try
            {
                if ((message.ToLower().Contains("no")) || (message.ToLower().Contains("error")))
                {// Either invalid user name or no password saved
                    OnAuthenticate(message + " Contact your administrator.");
                }
                else if (message.Contains("Invalid"))
                {
                    OnAuthenticate("Invalid password - please try again.");
                }
                else
                {
                    ChangePasswordWindow changePassW = null;
                    try
                    {
                        bool creatingUser = false;
                        changePassW = new ChangePasswordWindow();
                        if (!string.IsNullOrWhiteSpace(message))
                        {
                            changePassW.Title = message;
                            if (message.Contains("new user"))
                            {// Creating a new user.
                                changePassW.btnChange.Content = "OK";
                                changePassW.btnChange.IsDefault = true;
                                creatingUser = true;
                            }
                        }
                        if ((MainWindow.DisplayUser != null) || (creatingUser))
                        {
                            if (MainWindow.DisplayUser != null)
                                changePassW.UserName = MainWindow.DisplayUser.UserName;
                            if (changePassW.ShowDialog() == true)
                            {
                                if (!string.IsNullOrWhiteSpace(changePassW.NewPassword))
                                {// Only to change passwords
                                 //User changingUser = new User(changePassW.UserName);// BAD - don't replace existing user.
                                 /*Rfc2898DeriveBytes pbkdf = new Rfc2898DeriveBytes(changePassW.OldPassword, Encoding.Unicode.GetBytes("@-thel@"));
                                 pbkdf.IterationCount = 24;
                                 if (changePassW.UserName != currentUser.UserName)
                                 {
                                     changingUser.Password = Encoding.Unicode.GetString(pbkdf.GetBytes(42));
                                 }
                                 else
                                 {
                                     currentUser.Password = Encoding.Unicode.GetString(pbkdf.GetBytes(42));
                                 }*/
                                    Rfc2898DeriveBytes pbkdf = new Rfc2898DeriveBytes(changePassW.NewPassword, Encoding.Unicode.GetBytes("@-thel@"))
                                    { IterationCount = 24 };
                                    if (creatingUser)
                                    {// Creating a new user
                                        MainWindow.NewUser = new User(changePassW.txtUserName.Text);
                                        if (ExistingUsers.Contains(MainWindow.NewUser))
                                        {// User already in list - don't allow
                                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                                                MethodBase.GetCurrentMethod().Name), "Unable to create user as it already exists.");
                                            //MessageBox.Show();
                                            OnSendMessage(MessageType.StatusMessage, new StatusMessage()
                                            {
                                                StatusID = Guid.NewGuid(),
                                                Message = "User already exists - please use a different user name.",
                                                MessageTimeStamp = DateTime.Now,
                                                Animation = false,
                                                AutoRemove = TimeSpan.FromSeconds(5)
                                            });
                                            MainWindow.NewUser = null;
                                        }
                                        else
                                        {
                                            MainWindow.NewUser.Password = Encoding.Unicode.GetString(pbkdf.GetBytes(42));
                                            OnSendMessage(MessageType.CreateUser, MainWindow.NewUser);
                                        }
                                    }
                                    else //if (changePassW.UserName != currentUser.UserName)
                                    {// Changing another user's password
                                        MainWindow.DisplayUser.NewPassword = Encoding.Unicode.GetString(pbkdf.GetBytes(42));
                                        OnSendMessage(MessageType.ChangePassword, MainWindow.DisplayUser);
                                    }
                                    /*else
                                    {// Onnodig - displayUser sal currentUser wees, as currenUser se password verander word.
                                        currentUser.NewPassword = Encoding.Unicode.GetString(pbkdf.GetBytes(42));
                                        client.SendMessage(MessageType.ChangePassword, currentUser);
                                    }*/
                                }
                            }
                            else if (MainWindow.CurrentUserAuthenticated)
                            {
                                AtClosingTime();
                            }
                        }
                    }
                    finally
                    {
                        if (changePassW != null)
                        {
                            changePassW.Close();
                            changePassW = null;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "Error changing password", ex);
            }
        }

        private void btnNewUser_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow.CurrentUser.Rights.CreateUser)
            {
                try
                {
                    MainWindow.DisplayUser = null;
                    /*boxUserList.IsReadOnly = false;
                    boxUserList.IsEditable = true;
                    /*ExistingUsers.Clear();
                    if (PropertyChanged != null)
                        PropertyChanged(this, new PropertyChangedEventArgs("ExistingUsers"));*/
                    /*boxUserList.Text = "";
                    boxUserList.Focus();*/
                    ChangePassword("Enter the new user name, and the password twice.");
                    try
                    {
                        //selectedUserName = "";
                        MainWindow.Client.SendMessage(MessageType.RequestUserList);
                    }
                    catch (Exception ex)
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                            "Error requesting user list", ex);
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                        MethodBase.GetCurrentMethod().Name), "Error adding new user ", ex);
                }
            }
            else
            {
                btnNewUser.Visibility = Visibility.Hidden;
                btnNewUser.IsEnabled = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    "Unable to create new user as current user doesn't have the right. User: " + MainWindow.CurrentUser.UserName);
            }
        }


        private void btnUpdateDBSettings_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MainWindow.Client.SendMessage(MessageType.RequestDBSettings, null);
                //btnUpdateDBSettings.IsEnabled = false;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "Error updating DB settings", ex);
            }
        }

        private void btnChangePassword_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ChangePassword("");
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), 
                    "Error displaying DB settings", ex);
            }
        }

        private void OnAuthenticate(string v)
        {
            if (AuthenticateEvent != null)
            {
                AuthenticateEvent(new AuthenticateEventArgs(v));
            }
        }

        private void OnSendMessage(MessageType type, object data)
        {
            if (SendMessageEvent != null)
            {
                SendMessageEvent(new SendMessageEventArgs(type, data));
            }
        }

        private void AtClosingTime()
        {
            if ((!MainWindow.CurrentUser.Authenticated) && (ClosingTime != null))
            {
                ClosingTime(this, new EventArgs());
            }
        }

        public void DisplayUserRights(User _displayUser)
        {
            try
            {
                MainWindow.DisplayUser = _displayUser;
                ShowRights(_displayUser);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "Error displaying DB settings", ex);
            }
        }


        private void UserList_DropDown(object sender, EventArgs e)
        {
            try
            {
                MainWindow.Client.SendMessage(MessageType.RequestUserList);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "Error requesting user list", ex);
            }
        }

        private void UserList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {// Several scenarios: Changing user selection while creating a new one, means you have to destroy the new user and display 
            // the new selection's user rights.
            // In this case newUser == null
            try
            {
                if ((e.AddedItems != null) && (e.AddedItems.Count > 0))
                { // Display selected.
                    var displayUser = e.AddedItems[0] as User;
                    if (displayUser != null)
                        DisplayUserRights(displayUser);
                    if ((MainWindow.NewUser != null) && (MainWindow.NewUser.UserName != displayUser.UserName))
                        // Selected another user while creating - discard new user
                    {
                        if (ExistingUsers.Contains(MainWindow.NewUser))
                        {
                            if (CheckAccess())
                            {
                                ExistingUsers.Remove(MainWindow.NewUser);
                            }
                            else
                            {
                                Dispatcher.Invoke(delegate { ExistingUsers.Remove(MainWindow.NewUser); });
                            }
                            /*if (PropertyChanged != null)
                                PropertyChanged(this, new PropertyChangedEventArgs("ExistingUsers"));*/
                        }
                        MainWindow.NewUser.Dispose();
                        MainWindow.NewUser = null;
                        boxUserList.IsReadOnly = true;
                        boxUserList.IsEditable = false;
                    }
                }
                else if (boxUserList.SelectedItem != null)
                {
                    User selectedUser = (User)boxUserList.SelectedItem;
                    if (selectedUser != null)
                        DisplayUserRights(selectedUser);
                }
                /*if (!string.IsNullOrWhiteSpace(selectedUserName))
                {
                    if (MainWindow.NewUser == null)
                    {
                        if ((boxUserList.SelectedItem == null) && (e.RemovedItems != null) && (e.RemovedItems.Count > 0))
                            boxUserList.SelectedItem = e.RemovedItems[0];
                    }

                    //client.SendMessage(MessageType.RequestUserRights, selectedUserName); Obsolete?
                }*/
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "Error changing user selection", ex);
            }
        }

        private void btnSaveUser_Click(object sender, RoutedEventArgs e)
        {
            if ((MainWindow.CurrentUser.Rights.ChangeRights) || (MainWindow.CurrentUser.Rights.CreateUser))
            {
                try
                {
                    if (MainWindow.NewUser != null)
                    {
                        boxUserList.IsReadOnly = true;
                        boxUserList.IsEditable = false;
                        if (MainWindow.CurrentUser.Rights.CreateUser)
                        { // Creating a new user
                            if (MainWindow.NewUser != null)
                            {//Creating a new user - prompt for new password
                                MainWindow.NewUser.Rights = (UserRights)boxUserRights.Items[0];
                                //ChangePassword("Please enter new password twice.");
                            }
                            else
                            {// Updating existing user
                                MainWindow.DisplayUser.Rights = (UserRights)boxUserRights.Items[0];
                                OnSendMessage(MessageType.ChangePassword, MainWindow.DisplayUser);
                            }
                        }
                    }
                    else if (MainWindow.CurrentUser.Rights.ChangeRights) // Edit existing
                    {
                        MainWindow.DisplayUser.Rights = (UserRights)boxUserRights.Items[0];
                        OnSendMessage(MessageType.ChangePassword, MainWindow.DisplayUser);
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                        MethodBase.GetCurrentMethod().Name), "Error displaying DB settings", ex);
                }
            }
            else
            {
                btnSaveUser.Visibility = Visibility.Hidden;
                btnSaveUser.IsEnabled = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "Unable to edit user as current user doesn't have the right. User: " +
                    MainWindow.CurrentUser.UserName);
            }
        }

        /// <summary>
        /// The RIO dev team has decided together that no users can be deleted. This will instead disable a user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteUser_Click(object sender, RoutedEventArgs e)
        {
            if (MainWindow.CurrentUser.Rights.DeleteUser)
            {
                try
                {
                    if (boxUserList.SelectedItem != null)
                    {
                        //if (boxUserList.SelectedItem.ToString() == MainWindow.CurrentUser.UserName)
                        //{
                        //    OnSendMessage(MessageType.UserLogout, MainWindow.CurrentUser);
                        //}
                        OnSendMessage(MessageType.DeleteUser, boxUserList.SelectedItem);
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                        MethodBase.GetCurrentMethod().Name), "Error displaying DB settings", ex);
                }
            }
            else
            {
                btnDeleteUser.Visibility = Visibility.Hidden;
                btnDeleteUser.IsEnabled = false;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                        MethodBase.GetCurrentMethod().Name), "Unable to delete user due to insufficient rights. User name: " + 
                        MainWindow.CurrentUser.UserName);
            }
        }


        private void BoxUserType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var boxUserType = (sender as ComboBox);
            if (boxUserType != null && boxUserType.Items != null && boxUserType.Items.Count > 4)
            {
                //if (boxUserRights != null && boxUserRights.Items.Count > 0)
                {
                    if (e.AddedItems != null && e.AddedItems.Count > 0)// Avoid null references
                    {
                        UserRights newRights = new UserRights((UserType)e.AddedItems[0]);
                        if (newRights != null)
                        {
                            if (MainWindow.NewUser == null)// Edit existing user - use with caution
                            {
                                User selectedUser = boxUserList.SelectedItem as User;
                                if (MainWindow.CurrentUser.Rights.ChangeRights && selectedUser != null && selectedUser.Rights != e.AddedItems[0] as
                                    UserRights)
                                {
                                    selectedUser.Rights = newRights;
                                    DisplayUserRights(selectedUser);
                                }
                            }
                            else
                            {// Edit new user - specify desired user type
                                if (newRights != MainWindow.NewUser.Rights)
                                {// Don't try to do anything if the selection didn't change.
                                    MainWindow.NewUser.Rights = newRights;
                                    DisplayUserRights(MainWindow.NewUser);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void btnGenerateLayout_Click(object sender, RoutedEventArgs e)
        {
            //VideoHost.MainWindow one = new VideoHost.MainWindow(CameraLayouts.One.ToString(), MainWindow.CurrentUser, CameraLayouts.One);
            //one.Show();
            //VideoHost.MainWindow four = new VideoHost.MainWindow(CameraLayouts.TwoByTwo.ToString(), MainWindow.CurrentUser, CameraLayouts.TwoByTwo);
            //four.Show();
            //VideoHost.MainWindow six = new VideoHost.MainWindow(CameraLayouts.FivePlusOne.ToString(), MainWindow.CurrentUser, CameraLayouts.FivePlusOne);
            //six.Show();
            //VideoHost.MainWindow nine = new VideoHost.MainWindow(CameraLayouts.ThreeByThree.ToString(), MainWindow.CurrentUser, CameraLayouts.ThreeByThree);
            //nine.Show();
            //VideoHost.MainWindow thirteen = new VideoHost.MainWindow(CameraLayouts.TwelvePlusOne.ToString(), MainWindow.CurrentUser, CameraLayouts.TwelvePlusOne);
            //thirteen.Show();
            //VideoHost.MainWindow sixteen = new VideoHost.MainWindow(CameraLayouts.FourByFour.ToString(), MainWindow.CurrentUser, CameraLayouts.FourByFour);
            //sixteen.Show();

            VideoHostConfiguration config = new VideoHostConfiguration(selectedLayout, null, selectedLayout.ToString());//TODO replace in new config window. txtLayoutName.Text);
            OnSendMessage(MessageType.CameraLayouts, config);
            /*var layoutName = txtLayoutName.Text;
            if ((selectedLayout != CameraLayouts.None) && (!string.IsNullOrEmpty(layoutName)))
            {
                if (videoHosts.Contains("RIOVideoHost" + layoutName))
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                            MethodBase.GetCurrentMethod().Name), "Unable to create video window as one with the name  " +
                            layoutName + " already exists.");
                }
                else
                {// TODO separate processes.
                    try
                    {
                        videoHosts.Add("RIOVideoHost" + layoutName);
                        VideoHost.MainWindow newWindow = new VideoHost.MainWindow(txtLayoutName.Text, MainWindow.CurrentUser, selectedLayout, MainWindow.Client.ServerConfig);
                        newWindow.Show();
                    }
                    catch (Exception ex)
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                            MethodBase.GetCurrentMethod().Name), "Unable to create video window ERROR " + ex);
                    }
                }
            }*/
        }

        private void btnSelectLayout(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            if (sender != null)
            {
                int row = (int)btn.GetValue(Grid.RowProperty);
                int column = (int)btn.GetValue(Grid.ColumnProperty);
                switch (column)
                {
                    case 0:
                        if (row == 1)
                            selectedLayout = CameraLayouts.One;
                        else if (row == 2)
                            selectedLayout = CameraLayouts.ThreeByThree;
                        else selectedLayout = CameraLayouts.None;
                        break;
                    case 1:
                        if (row == 1)
                            selectedLayout = CameraLayouts.TwoByTwo;
                        else if (row == 2)
                            selectedLayout = CameraLayouts.TwelvePlusOne;
                        else selectedLayout = CameraLayouts.None;
                        break;
                    case 2:
                        if (row == 1)
                            selectedLayout = CameraLayouts.FivePlusOne;
                        else if (row == 2)
                            selectedLayout = CameraLayouts.FourByFour;
                        else selectedLayout = CameraLayouts.None;
                        break;
                    default:
                        selectedLayout = CameraLayouts.None;
                        break;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~SystemConfig() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            if ((videoHosts != null) && (videoHosts.Count > 0))
            {
                videoHosts.Clear();
                videoHosts = null;
            }
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        
        private void btnTestReport_Click(object sender, RoutedEventArgs e)
        {
            Uri result;
            reportAddress.Text = reportAddress.Text.StartsWith("http:") ? reportAddress.Text : "http://" + reportAddress.Text;

            if (Uri.TryCreate(reportAddress.Text, UriKind.Absolute, out result))
                btnUpdateSealAddress.IsEnabled = true; 
        }

        private void btnUpdateSealAddress_Click(object sender, RoutedEventArgs e)
        {
            btnUpdateSealAddress.IsEnabled = false;
            OnSendMessage(MessageType.ReportAddress, reportAddress.Text);
        }

        private void tabCtrlMain_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tabControl = sender as TabControl;
            if (tabControl != null)
            {
                var tab = tabControl.SelectedItem as TabItem;
                if (tab != null)
                {
                    if (tab.Name == "tabAdministration")
                    {
                        OnSendMessage(MessageType.ReportAddress, null);
                    }
                }
            }
        }
    }
}
