﻿using eNerve.DataTypes;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for Statusbar.xaml
    /// </summary>
    public partial class StatusbarControl : UserControl, INotifyPropertyChanged
    {
        public ObservableCollection<StatusMessage> StatusMessages { get; private set; }     
   
        public StatusMessage LastMessage
        {
            get
            {
                StatusMessage result = null;
                if (StatusMessages != null)
                    if (StatusMessages.Count > 0)
                        result = StatusMessages[StatusMessages.Count - 1];

                return result;
            }
        }

        public bool Animation
        {
            get
            {
                bool result = false;
                StatusMessage lastMsg = LastMessage;
                if (lastMsg != null)
                    result = lastMsg.Animation;

                return result;
            }
        }

        public void AddMessage(StatusMessage _msg)
        {
            if (this.Dispatcher.CheckAccess())
            {
                if (_msg != null)
                {
                    if (StatusMessages == null)
                        StatusMessages = new ObservableCollection<StatusMessage>();

                    if (!StatusMessages.Contains(_msg))
                    {
                        StatusMessages.Add(_msg);

                        if (_msg.AutoRemove.TotalMilliseconds > 0)
                        {
                            _msg.OnRemoveNotification += Msg_OnRemoveNotification;
                            _msg.StartAutoRemoveReminder();
                        }
                    }

                    PropertyChangedHandler("LastMessage");
                    PropertyChangedHandler("Animation");
                }
            }
            else
                this.Dispatcher.Invoke(delegate() { AddMessage(_msg); });
        }

        public void RemoveMessage(StatusMessage _msg)
        {
            if (this.Dispatcher.CheckAccess())
            {
                if (_msg != null && StatusMessages != null)
                {
                    if (StatusMessages.Contains(_msg))
                        StatusMessages.Remove(_msg);

                    _msg.OnRemoveNotification -= Msg_OnRemoveNotification;

                    _msg.Dispose();

                    PropertyChangedHandler("LastMessage");
                    PropertyChangedHandler("Animation");
                }
            }
            else
                this.Dispatcher.Invoke(delegate() { RemoveMessage(_msg); });
        }

        void Msg_OnRemoveNotification(object sender, StatusMessageEventArgs e)
        {
            RemoveMessage(e.Message);
        }

        public StatusbarControl()
        {
            InitializeComponent();

            DataContext = this;

            StatusMessages = new ObservableCollection<StatusMessage>();
        }

        #region PropertyChanged
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }
        #endregion PropertyChanged

        private void menuClear_Click(object sender, RoutedEventArgs e)
        {
            if (StatusMessages != null)
                for(int i = StatusMessages.Count - 1; i >= 0; i--)
                    RemoveMessage(StatusMessages[i]);
        }
    }

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isVisible = (bool)value;

            return (isVisible ? Visibility.Visible : Visibility.Hidden);
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ((Visibility)value == Visibility.Visible);
        }
    }
}
