﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eNerve.DataTypes;
using eNerve.DataTypes.Gauges;

namespace WPF_UI.Controls
{
    /// <summary>
    /// Interaction logic for DashboardImagePropertiesControl.xaml
    /// </summary>
    public partial class DashboardImagePropertiesControl : UserControl
    {
        public DashboardImagePropertiesControl()
        {
            InitializeComponent();
        }
        public void SetImage(DashboardImage image)
        {
            DataContext = image;
            imagePath.Text = image.Path;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".png";
            dlg.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            if (dlg.ShowDialog() == true)
            {
                if (File.Exists(dlg.FileName))
                {
                    imagePath.Text = dlg.FileName;
                    DashboardImage image = DataContext as DashboardImage;
                    if (image != null)
                    {
                        image.Path = dlg.FileName;
                        image.Name = dlg.SafeFileName.Substring(0, dlg.SafeFileName.IndexOf('.'));
                        image.SaveImageFile(dlg.FileName);
                        
                        image.Value = new BitmapImage(new Uri(imagePath.Text));
                    }
                }
            }
        }
    }
}
