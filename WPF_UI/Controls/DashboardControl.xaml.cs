﻿using BlackBoxEngine;
using DevExpress.Xpf.Gauges;
using eNerve.DataTypes;
using eNerve.DataTypes.Gauges;
using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WPF_UI.Controls;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for DashboardControl.xaml
    /// </summary>
    public partial class DashboardControl : UserControl
    {
        private Dashboard dashboard;

        private ImageBrush dropSiteBrush;

        public DashboardControl()
        {
            InitializeComponent();

            imgRound.DataContext = new RoundGauge() { Value = 3, StartValue = 0, EndValue = 10, StartAngle = 0, EndAngle = 300, MajorIntervalCount = 10, MinorIntervalCount = 2, EventName = "", Row = -1, Column = -1};
            imgVert.DataContext = new VerticalGauge() { Value = 2, StartValue = 0, EndValue = 10, MajorIntervalCount = 10, MinorIntervalCount = 2, EventName = "", Row = -1, Column = -1 };
            imgDigital.DataContext = new DigitalGauge() { EventName = "", Row = -1, Column = -1 };
            imgStatus.DataContext = new StatusGauge() { Value = false, EventName = "", Row = -1, Column = -1 };
            imgImage.DataContext = new DashboardImage() { Row = -1, Column = -1 };

            BitmapSource source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                        Properties.Resources.gaugeDropSite2.GetHbitmap(),
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromWidthAndHeight(Properties.Resources.gaugeDropSite2.Width, Properties.Resources.gaugeDropSite2.Height));
            dropSiteBrush = new ImageBrush(source) { Opacity = 0.5, Stretch = Stretch.Uniform };

            LoadGrid((int)spinColumns.Value, (int)spinRows.Value);
        }

        public void SetDashboard(Dashboard _dashboard)
        {
            if (Dispatcher.CheckAccess())
            {
                if ((_dashboard != null) && (_dashboard.EventNames == null) && (dashboard != null)) // Added to preserve guidashboard's events.
                    _dashboard.EventNames = dashboard.EventNames;
                dashboard = _dashboard;

                if (dashboard != null)
                {
                    spinColumns.Value = dashboard.Columns;
                    spinRows.Value = dashboard.Rows;

                    LoadGrid(dashboard.Columns, dashboard.Rows);

                    if (dashboard.Gauges == null)
                        dashboard.Gauges = new List<Gauge>();

                    LoadGauges(dashboard.Gauges);
                }
            }
            else
                Dispatcher.Invoke(() => SetDashboard(_dashboard));
        }

        public void SetEventNames(List<string> _eventNames)
        {
            if (dashboard == null)
                dashboard = new Dashboard();

            dashboard.EventNames = _eventNames;
        }

        public void ProcessEvent(Event _event)
        {
            if (dashboard != null && dashboard.Gauges != null)
            {
                foreach (Gauge gauge in dashboard.Gauges)
                {
                    if (gauge.EventName == _event.Name)
                    {
                        foreach (IEventTrigger trigger in _event.Triggers)
                        {
                            if (trigger.DetectorType == DetectorType.Analog)
                                gauge.SetValue(trigger.EventValue);
                            else if(gauge is DigitalGauge)
                            {
                                gauge.SetValue(trigger.State);
                                break;
                            }
                            else
                                gauge.SetValue(trigger.State);
                        }
                    }                    
                }
            }
        }

        private void ClearGrid()
        {
            dashGrid.Children.Clear();
                        
            dashGrid.ColumnDefinitions.Clear();
            dashGrid.RowDefinitions.Clear();
        }

        private void LoadGrid(int columns, int rows)
        {
            ClearGrid();

            for (int i = 0; i < columns; i++)
                dashGrid.ColumnDefinitions.Add(new ColumnDefinition());

            for (int i = 0; i < rows; i++)
                dashGrid.RowDefinitions.Add(new RowDefinition());

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < columns; c++)
                {
                    Border border = new Border() { Margin = new Thickness(10, 10, 10, 35), AllowDrop = true };
                    border.Drop += gauge_Drop;
                    dashGrid.Children.Add(border);
                    Grid.SetRow(border, r);
                    Grid.SetColumn(border, c);
                }
            }            
        }

        private void LoadGauges(List<Gauge> _gauges)
        {
            if (_gauges != null)
            {
                foreach (Gauge gauge in _gauges)
                {
                    Border border = FindElement<Border>(gauge.Row, gauge.Column);
                    if (border != null)
                        border.Child = CreateGaugeUI(gauge);
                    border.Visibility = Visibility.Visible;

                    DashboardImage image = gauge as DashboardImage;
                    if (image != null)
                        CreateImageProperties(image);
                    else
                        CreateGaugeProperties(gauge);

                    CreateGaugePropertiesToggleImage(gauge);

                    CreateGaugeLabel(gauge);
                }
            }
        }

        private void btnExpand_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (btnExpand.Text == "▲")
            {
                gridToolbar.Visibility = Visibility.Collapsed;
                btnExpand.Text = "▼";
            }
            else
            {
                gridToolbar.Visibility = Visibility.Visible;
                btnExpand.Text = "▲";
            }
        }

        private FrameworkElement _dragged;

        private void DragDrop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_dragged != null)
                return;

            _dragged = sender as FrameworkElement;
        }

        private void UserControl_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (_dragged == null)
                    return;
                if (e.LeftButton == MouseButtonState.Released)
                {
                    _dragged = null;
                    return;
                }
                else if (e.LeftButton == MouseButtonState.Pressed)
                {
                    if (_dragged is FrameworkElement)
                    {
                        object data = (_dragged).DataContext;
                        if (data is RoundGauge || data is VerticalGauge || data is DigitalGauge || data is StatusGauge || data is Image || data is DashboardImage)
                        {
                            SetDropSiteBackbrounds(dropSiteBrush);
                            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new System.Threading.ParameterizedThreadStart(DoDragDrop), data);
                            SetDropSiteBackbrounds();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                _dragged = null;
            }
        }

        private void SetDropSiteBackbrounds(ImageBrush brush = null)
        {
            foreach (UIElement element in dashGrid.Children)
            {
                Border border = element as Border;

                if (border != null)
                {
                    border.Background = brush;

                    if (border.Child != null)
                    {
                        if (brush != null)
                            border.Child.Opacity = 0.2;
                        else
                            border.Child.Opacity = 1;
                    }
                }
            }
        }

        private void DoDragDrop(object parameter)
        {
            try
            {
                if (_dragged != null)
                {
                    DragDrop.DoDragDrop(_dragged, parameter, DragDropEffects.All);
                    _dragged = null;
                }
            }
            catch (Exception ex)
            {
                _dragged = null;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void gauge_Drop(object sender, DragEventArgs e)
        {
            DashboardImage dropImage = null;
            object gaugeObj = e.Data.GetData(typeof(RoundGauge));
            if (gaugeObj == null)
            {
                gaugeObj = e.Data.GetData(typeof(VerticalGauge));
                if (gaugeObj == null)
                {
                    gaugeObj = e.Data.GetData(typeof(DigitalGauge));
                    if (gaugeObj == null)
                    {
                        gaugeObj = e.Data.GetData(typeof(StatusGauge));
                        if (gaugeObj == null)
                        {
                            dropImage = e.Data.GetData(typeof(DashboardImage)) as DashboardImage;
                            if (dropImage == null)
                                return;
                        }
                    }
                }
            }
            if (dashboard != null)
            {
                if (gaugeObj != null)
                {
                    Gauge gauge = ObjectCopier.Clone((Gauge)gaugeObj);

                    if (sender is Border)
                    {
                        Border border = sender as Border;
                        gauge.Row = Grid.GetRow(border);
                        gauge.Column = Grid.GetColumn(border);

                        for(int i = dashboard.Gauges.Count - 1; i >= 0; i--)
                        {
                            Gauge tempG = dashboard.Gauges[i];
                            if (tempG.Row == gauge.Row && tempG.Column == gauge.Column)
                            {
                                RemoveGauge(tempG);
                                //dashboard.Gauges.RemoveAt(i);
                            }
                        }

                        border.DataContext = gauge;
                        dashboard.Gauges.Add(gauge);

                        border.Child = CreateGaugeUI(gauge);

                        CreateGaugeProperties(gauge);

                        CreateGaugePropertiesToggleImage(gauge);

                        CreateGaugeLabel(gauge);
                    }
                }
                else if (dropImage != null)
                {
                    DashboardImage image = ObjectCopier.Clone(dropImage);
                    Border border = sender as Border;
                    if (border != null)
                    {
                        image.Column = Grid.GetColumn(border);
                        image.Row = Grid.GetRow(border);

                        for (int i = dashboard.Gauges.Count - 1; i >= 0; i--)
                        {
                            Gauge tempG = dashboard.Gauges[i];
                            if (tempG.Row == image.Row && tempG.Column == image.Column)
                            {
                                RemoveGauge(tempG);
                                //dashboard.Gauges.RemoveAt(i);
                            }
                        }

                        border.DataContext = image;
                        dashboard.Gauges.Add(image);

                        border.Child = CreateGaugeUI(image);

                        CreateImageProperties(image);
                        CreateGaugePropertiesToggleImage(image);
                        CreateGaugeLabel(image);

                        propertiesToggle_MouseDown(image.Value, null);
                    }
                }
            }
        }

        private void Trash_Drop(object sender, DragEventArgs e)
        {
            object guageObj = e.Data.GetData(typeof(RoundGauge));
            if (guageObj == null)
            {
                guageObj = e.Data.GetData(typeof(VerticalGauge));
                if (guageObj == null)
                {
                    guageObj = e.Data.GetData(typeof(DigitalGauge));
                    if (guageObj == null)
                    {
                        guageObj = e.Data.GetData(typeof(StatusGauge));
                        if (guageObj == null)
                        {
                            guageObj = e.Data.GetData(typeof(DashboardImage));
                        }
                    }
                }
            }

            if (guageObj != null && dashboard != null)
            {
                Gauge gauge = guageObj as Gauge;

                RemoveGauge(gauge);
            }
        }

        private void RemoveGauge(Gauge gauge)
        {
            if (gauge != null && gauge.Row > -1 && gauge.Column > -1)
            {
                Border border = FindElement<Border>(gauge.Row, gauge.Column);
                if (border != null)
                {
                    border.DataContext = null;
                    border.Child = null;
                }

                GaugePropertiesControl properties = FindElement<GaugePropertiesControl>(gauge.Row, gauge.Column);
                if (properties == null)
                {
                    DashboardImagePropertiesControl imageProperties = FindElement<DashboardImagePropertiesControl>(gauge.Row, gauge.Column);
                    if (imageProperties != null)
                        dashGrid.Children.Remove(imageProperties);
                }
                else
                    dashGrid.Children.Remove(properties);

                Image toggle = FindElement<Image>(gauge.Row, gauge.Column);
                if (toggle != null)
                    dashGrid.Children.Remove(toggle);

                TextBlock label = FindElement<TextBlock>(gauge.Row, gauge.Column);
                if (label != null)
                    dashGrid.Children.Remove(label);

                if (dashboard.Gauges != null)
                    dashboard.Gauges.Remove(gauge);
            }
        }

        void propertiesToggle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Image propertiesToggle = sender as Image;
            if(propertiesToggle != null)
            {
                int row = Grid.GetRow(propertiesToggle);
                int column = Grid.GetColumn(propertiesToggle);
                Border border = FindElement<Border>(row, column);
                if (border != null)
                    border.Visibility = border.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;

                GaugePropertiesControl gaugeProperties = FindElement<GaugePropertiesControl>(row, column);
                if (gaugeProperties == null)
                {
                    DashboardImagePropertiesControl imageProperties = FindElement<DashboardImagePropertiesControl>(row, column);
                    if (imageProperties != null)
                    {
                        if (border.Visibility == Visibility.Visible)
                        {
                            imageProperties.Visibility = Visibility.Collapsed;
                            WpfAnimatedGif.ImageBehavior.SetAnimatedSource((border.Child as DashboardImageControl).image, (border.Child as DashboardImageControl).DashboardImage.Value);
                        }
                        else
                        {
                            imageProperties.Visibility = Visibility.Visible;
                        }
                    }
                }
                else
                    gaugeProperties.Visibility = border.Visibility == Visibility.Visible ? Visibility.Collapsed : Visibility.Visible;

                TextBlock label = FindElement<TextBlock>(row, column);
                if (label != null)
                    label.Visibility = border.Visibility;
            }
        }

        private FrameworkElement CreateGaugeUI(Gauge _gauge)
        {
            FrameworkElement result = null;

            if(_gauge is RoundGauge)
            {
                CircularGaugeControl roundG = new CircularGaugeControl();
                roundG.DataContext = _gauge;
                roundG.Model = new CircularClassicModel();
                
                ArcScale arcScale = new ArcScale();
                BindingOperations.SetBinding(arcScale, ArcScale.StartValueProperty, new Binding("StartValue"));
                BindingOperations.SetBinding(arcScale, ArcScale.EndValueProperty, new Binding("EndValue"));
                BindingOperations.SetBinding(arcScale, ArcScale.StartAngleProperty, new Binding("StartAngle"));
                BindingOperations.SetBinding(arcScale, ArcScale.EndAngleProperty, new Binding("EndAngle"));
                BindingOperations.SetBinding(arcScale, ArcScale.MajorIntervalCountProperty, new Binding("MajorIntervalCount"));
                BindingOperations.SetBinding(arcScale, ArcScale.MinorIntervalCountProperty, new Binding("MinorIntervalCount"));
               
                ArcScaleNeedle needle = new ArcScaleNeedle();
                BindingOperations.SetBinding(needle, ArcScaleNeedle.ValueProperty, new Binding("Value"));
                arcScale.Needles.Add(needle);

                arcScale.Layers.Add(new ArcScaleLayer());
                
                roundG.Scales.Add(arcScale);

                result = roundG;
            }
            else if (_gauge is VerticalGauge)
            {
                LinearGaugeControl vertG = new LinearGaugeControl() { Width = 150 };
                vertG.DataContext = _gauge;
                vertG.Model = new LinearClassicModel();

                LinearScale linearScale = new LinearScale();
                BindingOperations.SetBinding(linearScale, LinearScale.StartValueProperty, new Binding("StartValue"));
                BindingOperations.SetBinding(linearScale, LinearScale.EndValueProperty, new Binding("EndValue"));
                BindingOperations.SetBinding(linearScale, LinearScale.MajorIntervalCountProperty, new Binding("MajorIntervalCount"));
                BindingOperations.SetBinding(linearScale, LinearScale.MinorIntervalCountProperty, new Binding("MinorIntervalCount"));

                LinearScaleLevelBar bar = new LinearScaleLevelBar();
                BindingOperations.SetBinding(bar, LinearScaleLevelBar.ValueProperty, new Binding("Value"));
                linearScale.LevelBars.Add(bar);

                linearScale.Layers.Add(new LinearScaleLayer());

                vertG.Scales.Add(linearScale);

                result = vertG;
            }
            else if (_gauge is DigitalGauge)
            {
                DigitalGaugeControl digitalG = new DigitalGaugeControl() { Text = "Test" };
                digitalG.DataContext = _gauge;
                digitalG.Model = new DigitalClassicModel();

                BindingOperations.SetBinding(digitalG, DigitalGaugeControl.TextProperty, new Binding("Value"));

                result = digitalG;
            }
            else if (_gauge is StatusGauge)
            {
                StateIndicatorControl statusG = new StateIndicatorControl() { StateIndex = 0, MaxWidth = 150, MaxHeight = 150 };
                statusG.DataContext = _gauge;
                statusG.Model = new LampStateIndicatorModel();

                BindingOperations.SetBinding(statusG, StateIndicatorControl.StateIndexProperty, new Binding("Value") { Converter = new BoolToStateIndexConverter() });

                result = statusG;
            }
            else if (_gauge is DashboardImage)
            {
                DashboardImageControl control = new DashboardImageControl() { DashboardImage = _gauge as DashboardImage};
                control.DataContext = _gauge;

                WpfAnimatedGif.ImageBehavior.SetAnimatedSource(control.image, control.DashboardImage.Value);

                result = control;
            }

            if (result != null)
                result.PreviewMouseLeftButtonDown += DragDrop_PreviewMouseLeftButtonDown;

            return result;
        }

        private void CreateGaugeProperties(Gauge _gauge)
        {
            GaugePropertiesControl gaugeProperties = FindElement<GaugePropertiesControl>(_gauge.Row, _gauge.Column);
            if (gaugeProperties == null)
            {
                gaugeProperties = new GaugePropertiesControl() { Margin = new Thickness(10, 30, 10, 10) };
                dashGrid.Children.Add(gaugeProperties);
                Grid.SetRow(gaugeProperties, _gauge.Row);
                Grid.SetColumn(gaugeProperties, _gauge.Column);
            }
            gaugeProperties.SetGauge(_gauge, dashboard.EventNames);
            gaugeProperties.Visibility = Visibility.Collapsed;
        }

        private void CreateImageProperties(DashboardImage dropImage)
        {
            DashboardImagePropertiesControl imageProperties = FindElement<DashboardImagePropertiesControl>(dropImage.Row, dropImage.Column);
            if (imageProperties == null)
            {
                imageProperties = new DashboardImagePropertiesControl() { Margin = new Thickness(10, 30, 10, 10) };
                dashGrid.Children.Add(imageProperties);
                Grid.SetRow(imageProperties, dropImage.Row);
                Grid.SetColumn(imageProperties, dropImage.Column);
            }
            imageProperties.SetImage(dropImage);
            imageProperties.Visibility = Visibility.Collapsed;
        }

        private void CreateGaugePropertiesToggleImage(Gauge _gauge)
        {
            Image propertiesToggle = FindElement<Image>(_gauge.Row, _gauge.Column);
            if (propertiesToggle == null)
            {
                BitmapSource propertiesToggleSource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                    Properties.Resources.PropertiesGear.GetHbitmap(),
                    IntPtr.Zero,
                    Int32Rect.Empty,
                    BitmapSizeOptions.FromWidthAndHeight(Properties.Resources.PropertiesGear.Width, Properties.Resources.PropertiesGear.Height));
                propertiesToggle = new Image() { Source = propertiesToggleSource, Margin = new Thickness(0,5,5,0), Opacity = 0.5, Width = 15, Height = 15, ToolTip = "Properties", HorizontalAlignment = HorizontalAlignment.Right, VerticalAlignment = VerticalAlignment.Top };
                dashGrid.Children.Add(propertiesToggle);
                propertiesToggle.MouseDown += propertiesToggle_MouseDown;
                Grid.SetRow(propertiesToggle, _gauge.Row);
                Grid.SetColumn(propertiesToggle, _gauge.Column);
            }
        }

        private void CreateGaugeLabel(Gauge _gauge)
        {
            TextBlock label = FindElement<TextBlock>(_gauge.Row, _gauge.Column);
            if(label == null)
            {
                PropertyInfo info = (_gauge.GetType()).GetProperty("EventName");

                label = new TextBlock() { Height = 30, Margin = new Thickness(0, 0, 0, 5), HorizontalAlignment = HorizontalAlignment.Center, VerticalAlignment = VerticalAlignment.Bottom, FontSize = 20, FontWeight = FontWeights.Bold };
                label.DataContext = _gauge;
                BindingOperations.SetBinding(label, TextBlock.TextProperty, new Binding(info.Name));
                dashGrid.Children.Add(label);
                Grid.SetRow(label, _gauge.Row);
                Grid.SetColumn(label, _gauge.Column);
            }
        }

        private T FindElement<T>(int _row, int _col) where T : UIElement
        {
            T result = null;

            foreach (UIElement element in dashGrid.Children)
            {
                if (element is T && Grid.GetRow(element) == _row && Grid.GetColumn(element) == _col)
                {
                    result = element as T;
                    break;
                }
            }

            return result;
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(dashboard.Title))
                MainWindow.Client.SendMessage(string.IsNullOrWhiteSpace(dashboard.User) ? MessageType.PublicDashboard : MessageType.PrivateDashboard, dashboard);
            else
            {
                dashboard.Node.Value = XMLSerializer.Serialize(dashboard);
                MainWindow.Client.SendMessage(MessageType.MapDoc, dashboard.Node.Root as XMLDoc);
            }
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {
            dashboard.Columns = (int)spinColumns.Value;
            dashboard.Rows = (int)spinRows.Value;
            LoadGrid((int)spinColumns.Value, (int)spinRows.Value);
        }
    }
}
