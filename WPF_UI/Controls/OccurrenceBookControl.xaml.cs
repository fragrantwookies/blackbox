﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BlackBoxEngine;
using eThele.Essentials;
using eNerve.DataTypes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for OccurrenceBook.xaml
    /// </summary>
    public partial class OccurrenceBookControl : UserControl, IDisposable
    {
        public ObservableCollection<OccurrenceBook> OBItems { get; set; }

        private ObservableCollection<OBType> obTypes;

        public delegate void OBEntryEventHandler(object sender, OBEntry _obEntry);
        public event OBEntryEventHandler OBEntryAdded;

        public delegate void OBHistoryFilterEventHandler(object sender, OccurrenceBookHistoryFilter _obHistoryFilter);
        public event OBHistoryFilterEventHandler OBHistorySearch;
        
        private OccurrenceBookHistoryFilter occurrenceBookHistoryFilter;

        public OccurrenceBookControl()
        {
            InitializeComponent();

            OBItems = new ObservableCollection<OccurrenceBook>();
            lbOccurrenceBook.DataContext = OBItems;

            obTypes = new ObservableCollection<OBType>();
            cboTypeDescription.DataContext = obTypes;
            cboTypeDescription.ItemsSource = obTypes;
            cboOBType.DataContext = obTypes;
            cboOBType.ItemsSource = obTypes;
            
        }

        ~OccurrenceBookControl()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            ClearOccurrenceBook();
            ClearOBTypes();
        }

        private void ClearOccurrenceBook()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (OBItems != null)
                    {
                        for (int i = OBItems.Count - 1; i >= 0; i--)
                        {
                            OccurrenceBook obItem = OBItems[i];
                            OBItems.Remove(obItem);
                            obItem.Dispose();
                        }
                    }                    
                }
                else
                {
                    this.Dispatcher.Invoke(ClearOccurrenceBook);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void ClearOBTypes()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (obTypes != null)
                    {
                        for (int i = obTypes.Count - 1; i >= 0; i--)
                        {
                            OBType obType = obTypes[i];
                            obTypes.Remove(obType);
                            obType.Dispose();
                        }
                    }
                }
                else
                {
                    this.Dispatcher.Invoke(ClearOBTypes);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public delegate void OBTypesDelegate(List<OBType> _obTypes);
        public void SetTypes(List<OBType> _obTypes)
        {
            try
            {
                if (this.CheckAccess())
                {
                    ClearOBTypes();

                    foreach (OBType obType in _obTypes)
                    {
                        obTypes.Add(obType);
                    }
                }
                else
                    this.Dispatcher.Invoke(new OBTypesDelegate(SetTypes), new object[] {_obTypes});
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public delegate void OBEntryDelegate(OBEntry _obEntry);
        public void AddOBEntry(OBEntry _obEntry)
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (_obEntry is OccurrenceBook)
                    {
                        OccurrenceBook newEntry = (OccurrenceBook)_obEntry;

                        if (!OBItems.Contains(newEntry))
                        {
                            newEntry.SubTypes = OBType.FindSubTypes(obTypes, newEntry.OccurrenceType);
                            OBItems.Add(newEntry);
                        }
                    }
                    else if (_obEntry is OBSubEntry)
                    {
                        OBSubEntry newSubEntry = (OBSubEntry)_obEntry;

                        OccurrenceBook searchEntry = new OccurrenceBook(newSubEntry.OBID);

                        int index = OBItems.IndexOf(searchEntry);
                        if (index >= 0)
                        {
                            if (!OBItems[index].SubEntries.Contains(newSubEntry))
                                OBItems[index].SubEntries.Add(newSubEntry);
                        }
                    }
                }
                else
                    this.Dispatcher.Invoke(new OBEntryDelegate(AddOBEntry), new object[] {_obEntry});
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void LoadOBHistory(List<OccurrenceBook> _obHistory)
        {
            try
            {
                ClearOccurrenceBook();

                if (_obHistory != null)
                {
                    foreach (OccurrenceBook obItem in _obHistory)
                    {
                        if (this.CheckAccess())
                            AddOBEntry(obItem);
                        else
                            this.Dispatcher.Invoke(new OBEntryDelegate(AddOBEntry), new object[] { obItem });
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void OBPropertyUpdate(PropertyUpdate _propertyUpdate)
        {
            try
            {
                bool found = false;
                int tries = 3;
                while (!found)
                {
                    foreach (OccurrenceBook occurrenceBook in OBItems)
                    {
                        if (occurrenceBook.ID == _propertyUpdate.Id)
                        {
                            occurrenceBook.GetType().GetProperty(_propertyUpdate.PropertyName).SetValue(occurrenceBook, _propertyUpdate.Value);
                            found = true;
                            break;
                        }
                    }

                    if (found || tries-- == 0)
                        break;

                    System.Threading.Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void AddOBItemButtonClick(object sender, RoutedEventArgs e)
        {
            OccurrenceBook newOB = new OccurrenceBook(cboTypeDescription.Text, txtDescription.Text);
            newOB.User = MainWindow.CurrentUser;
            newOB.SubTypes = OBType.FindSubTypes(obTypes, newOB.OccurrenceType);
            OBItems.Add(newOB);

            if (OBEntryAdded != null)
                OBEntryAdded(this, newOB);
        }

        private void AddOBSubItemButtonClick(object sender, RoutedEventArgs e)
        {
            if (sender is FrameworkElement)
            {
                FrameworkElement btn = (FrameworkElement)sender;
                if (btn.DataContext != null)
                {
                    OccurrenceBook obItem = (OccurrenceBook)btn.DataContext;                    

                    if (obItem != null)
                    {
                        OBSubEntry newOBSubEntry = new OBSubEntry(obItem.ID, obItem.NewTypeDescription, obItem.NewDescription);
                        newOBSubEntry.User = MainWindow.CurrentUser;
                        obItem.SubEntries.Add(newOBSubEntry);

                        if (OBEntryAdded != null)
                            OBEntryAdded(this, newOBSubEntry);
                    }
                }
            }
        }

        private void ButtonExpandClick(object sender, RoutedEventArgs e)
        {
            if (sender is FrameworkElement)
            {
                if (((FrameworkElement)sender).DataContext != null)
                {
                    OccurrenceBook obItem = (OccurrenceBook)((FrameworkElement)sender).DataContext;

                    obItem.Expand = !obItem.Expand;
                }
            }
        }

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is FrameworkElement)
            {
                if (((FrameworkElement)sender).DataContext != null)
                {
                    OccurrenceBook obItem = (OccurrenceBook)((FrameworkElement)sender).DataContext;

                    obItem.Expand = !obItem.Expand;
                }
            }
        }

        private void cboOBType_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete || e.Key == Key.Back)
                ((ComboBox)sender).Text = "";
        }

        private void btnOBSearch_Click(object sender, RoutedEventArgs e)
        {
            DateTime? fromDateTime = dpOBFrom.SelectedDate != null ? dpOBFrom.SelectedDate + tpOBFrom.Value : null;
            DateTime? toDateTime = dpOBTo.SelectedDate != null ? dpOBTo.SelectedDate + tpOBTo.Value : null;            

            occurrenceBookHistoryFilter = new OccurrenceBookHistoryFilter(fromDateTime, toDateTime, cboOBType.Text, txtOBDescription.Text, txtOBUser.Text);

            if (OBHistorySearch != null)
                OBHistorySearch(this, occurrenceBookHistoryFilter);
        }

        private void btnOBClear_Click(object sender, RoutedEventArgs e)
        {
            occurrenceBookHistoryFilter = null;

            ClearOccurrenceBook();

            dpOBFrom.SelectedDate = null;
            tpOBFrom.Value = TimeSpan.Zero;
            dpOBTo.SelectedDate = null;
            tpOBTo.Value = TimeSpan.Zero;
            txtOBDescription.Text = "";
            cboOBType.Text = "";
        }
    }
}
