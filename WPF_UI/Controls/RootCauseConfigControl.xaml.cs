﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eThele.Essentials;
using eNerve.DataTypes;
using System.Collections.ObjectModel;
using System.Net;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for RootCauseConfigControl.xaml
    /// </summary>
    public partial class RootCauseConfigControl : UserControl
    {
        private ObservableCollection<UIDevice> devices;
        private ObservableCollection<UICondition> conditions;
        private XMLDoc conditionsDocBackup;


        public RootCauseConfigControl()
        {
            InitializeComponent();
        }

        public void SetServerAddres(IPEndPoint address)
        {
            if (this.CheckAccess())
            {
                txtIP.Text = address.Address.ToString();
                txtPort.Text = address.Port.ToString();
            }
            else
                this.Dispatcher.Invoke(() => SetServerAddres(address));
        }

        private void ClearConditions()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (conditions != null)
                    {
                        for (int i = conditions.Count - 1; i >= 0; i--)
                        {
                            UICondition condition = conditions[i];
                            conditions.Remove(condition);
                            condition.Dispose();
                        }
                    }
                }
                else
                {
                    Dispatcher.Invoke(ClearConditions);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void SetConditions(XMLDoc conditionsDoc)
        {
            try
            {
                if (conditionsDoc != null)
                {
                    if (CheckAccess())
                    {
                        ClearConditions();

                        conditionsDocBackup = conditionsDoc;

                        List<UICondition> conditionList = ParseConditions(conditionsDoc);

                        if (conditionList != null)
                        {
                            foreach (UICondition c in conditionList)
                                conditions.Add(c);

                            conditionList.Clear();
                        }
                    }
                    else
                        Dispatcher.Invoke(() => SetConditions(conditionsDoc));
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public static List<UICondition> ParseConditions(XMLDoc conditionsDoc)
        {
            List<UICondition> resultConditions = new List<UICondition>();

            foreach (XmlNode conditionNode in conditionsDoc)
            {
                UICondition condition = new UICondition();

                condition.Name = conditionNode.Name;
                condition.HiddenField = XmlNode.ParseString(conditionNode["HiddenField"], "");
                condition.Description = XmlNode.ParseString(conditionNode["Description"], "");
                condition.PercentageTriggersNeeded = XmlNode.ParseDouble(conditionNode["PercentageTriggersNeeded"], 100);
                condition.CauseAlarm = XmlNode.ParseBool(conditionNode["CauseAlarm"], true);
                condition.TimeFrame = XmlNode.ParseInteger(conditionNode["TriggersTimeFrame"], 0);
                condition.Cooldown = XmlNode.ParseInteger(conditionNode["CoolDown"], 0);

                XmlNode triggersNode = conditionNode["Triggers"];
                if (triggersNode != null)
                {
                    foreach (XmlNode triggerNode in triggersNode)
                    {
                        UITrigger trigger = new UITrigger();
                        trigger.DeviceName = XmlNode.ParseString(triggerNode, "", "DeviceName");
                        trigger.DetectorName = XmlNode.ParseString(triggerNode, "", "DetectorName");
                        trigger.Mandatory = XmlNode.ParseBool(triggerNode, false, "Mandatory");
                        trigger.IsAnalog = XmlNode.ParseBool(triggerNode, false, "IsAnalog");
                        trigger.TrueState = XmlNode.ParseBool(triggerNode, true, "TrueState");
                        trigger.FalseState = XmlNode.ParseBool(triggerNode, false, "FalseState");
                        trigger.TriggersToRaiseEvent = XmlNode.ParseInteger(triggerNode, 1, "TriggersToRaiseEvent");
                        trigger.TriggersToRaiseEventTimeFrame = XmlNode.ParseDouble(triggerNode, 0, "TriggersToRaiseEventTimeFrame");
                        trigger.MinThreshold = XmlNode.ParseDouble(triggerNode, double.MinValue, "MinThreshold");
                        trigger.MaxThreshold = XmlNode.ParseDouble(triggerNode, double.MaxValue, "MaxThreshold");
                        condition.Triggers.Add(trigger);
                        trigger.Condition = condition;
                    }
                }

                resultConditions.Add(condition);
            }

            return resultConditions;
        }

        public static XMLDoc BuildConditionsDoc(IEnumerable<UICondition> conditions)
        {
            XMLDoc result = null;

            try
            {
                result = new XMLDoc("AlarmConditions");

                foreach (UICondition condition in conditions)
                {
                    XmlNode conditionNode = result.Add(condition.Name, "");
                    if (conditionNode != null)
                    {
                        conditionNode.Add("HiddenField", condition.HiddenField);
                        conditionNode.Add("Description", condition.Description);
                        conditionNode.Add("PercentageTriggersNeeded", condition.PercentageTriggersNeeded.ToString());
                        conditionNode.Add("CauseAlarm", condition.CauseAlarm.ToString());
                        conditionNode.Add("TriggersTimeFrame", condition.TimeFrame.ToString());
                        conditionNode.Add("CoolDown", condition.Cooldown.ToString());
                        XmlNode triggersNode = conditionNode.Add("Triggers", "");

                        if (condition.Triggers != null && triggersNode != null)
                        {
                            int i = 1;
                            foreach (UITrigger trigger in condition.Triggers)
                            {
                                XmlNode triggerNode = triggersNode.Add(string.Format("Trigger{0}", i.ToString()), "");
                                if (triggerNode != null)
                                {
                                    triggerNode.AddAttribute("DeviceName", trigger.DeviceName);
                                    triggerNode.AddAttribute("DetectorName", trigger.DetectorName);
                                    triggerNode.AddAttribute("Mandatory", trigger.Mandatory.ToString());
                                    triggerNode.AddAttribute("IsAnalog", trigger.IsAnalog.ToString());
                                    triggerNode.AddAttribute("TriggersToRaiseEvent", trigger.TriggersToRaiseEvent.ToString());
                                    triggerNode.AddAttribute("TriggersToRaiseEventTimeFrame", trigger.TriggersToRaiseEventTimeFrame.ToString());
                                    triggerNode.AddAttribute("TrueState", trigger.TrueState.ToString());
                                    triggerNode.AddAttribute("FalseState", trigger.FalseState.ToString());
                                    triggerNode.AddAttribute("MinThreshold", trigger.MinThreshold.ToString());
                                    triggerNode.AddAttribute("MaxThreshold", trigger.MaxThreshold.ToString());
                                }

                                i++;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement && ((FrameworkElement)sender).DataContext is IExpandable)
                {
                    IExpandable expandableControl = (IExpandable)((FrameworkElement)sender).DataContext;

                    if (e.LeftButton == MouseButtonState.Pressed)
                        expandableControl.Expand = !expandableControl.Expand;
                    else if (e.RightButton == MouseButtonState.Pressed)
                    {
                        if (expandableControl.Expand)
                            expandableControl.CollapseAll();
                        else
                            expandableControl.ExpandAll();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private FrameworkElement _dragged;

        private void DragDrop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_dragged != null)
                return;

            if (sender is UIElement)
            {
                UIElement lb = (UIElement)sender;

                UIElement element = lb.InputHitTest(e.GetPosition(lb)) as UIElement;

                while (element != null)
                {
                    if (element is FrameworkElement)
                    {
                        _dragged = (FrameworkElement)element;
                        break;
                    }
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }
            }
        }

        private void UserControl_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (_dragged == null)
                    return;
                if (e.LeftButton == MouseButtonState.Released)
                {
                    _dragged = null;
                    return;
                }
                else if (e.LeftButton == MouseButtonState.Pressed)
                {
                    if (_dragged is FrameworkElement)
                    {
                        object data = ((FrameworkElement)_dragged).DataContext;
                        if (data is UITrigger || data is UICondition)
                        {
                            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                                new System.Threading.ParameterizedThreadStart(DoDragDrop),
                                data);

                            //DragDrop.DoDragDrop(_dragged, data, DragDropEffects.All);
                            //System.Diagnostics.Debug.WriteLine("DoDragDrop");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                _dragged = null;
            }
        }

        private void DoDragDrop(object parameter)
        {
            if (_dragged != null)
            {
                System.Windows.DragDrop.DoDragDrop(_dragged, parameter, DragDropEffects.All);
                _dragged = null;
            }
        }

        private void NewConditionDrop(object sender, DragEventArgs e)
        {
            object triggerobj = e.Data.GetData(typeof(UITrigger));
            if (triggerobj is UITrigger)
            {
                UITrigger trigger = ObjectCopier.Clone<UITrigger>((UITrigger)triggerobj);

                UICondition condition = new UICondition();
                condition.Name = "ConditionX";
                condition.TimeFrame = 5;
                condition.Cooldown = 5;
                condition.PercentageTriggersNeeded = 100;
                condition.Triggers.Add(trigger);
                trigger.Condition = condition;
                conditions.Add(condition);
                condition.Expand = true;
            }
        }

        private void ConditionDrop(object sender, DragEventArgs e)
        {
            object triggerobj = e.Data.GetData(typeof(UITrigger));
            if (triggerobj is UITrigger)
            {
                UITrigger trigger = ObjectCopier.Clone<UITrigger>((UITrigger)triggerobj);

                if (sender is FrameworkElement)
                {
                    if (((FrameworkElement)sender).DataContext is UICondition)
                    {
                        UICondition condition = (UICondition)((FrameworkElement)sender).DataContext;
                        if (!condition.Triggers.Contains(trigger))
                        {
                            condition.Triggers.Add(trigger);
                            trigger.Condition = condition;
                        }

                        condition.Expand = true;
                    }
                }
            }
        }

        private void DeleteDrop(object sender, DragEventArgs e)
        {
            try
            {
                object triggerobj = e.Data.GetData(typeof(UITrigger));
                if (triggerobj is UITrigger)
                {
                    UITrigger trigger = (UITrigger)triggerobj;

                    if (trigger.Condition != null)
                        trigger.Condition.Triggers.Remove(trigger);
                }

                object conditionobj = e.Data.GetData(typeof(UICondition));
                if (conditionobj is UICondition)
                {
                    UICondition condition = (UICondition)conditionobj;

                    RemoveCondition(condition);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public void RemoveCondition(UICondition _condition)
        {
            if (this.CheckAccess())
            {
                conditions.Remove(_condition);
                _condition.Dispose();
            }
            else
            {
                this.Dispatcher.Invoke(() => RemoveCondition(_condition));
            }
        }

        private void TextBox_PreviewDragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            e.Handled = true;
        }

        private void TextBox_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            e.Handled = true;
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            ClearConditions();
            SetConditions(conditionsDocBackup);
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            XMLDoc doc = BuildConditionsDoc(conditions);

            if (doc != null)
                MainWindow.Client.SendMessage(MessageType.RootCauseConditionsDoc, doc);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                IPEndPoint address = new IPEndPoint(IPAddress.Parse(txtIP.Text), int.Parse(txtPort.Text));
                MainWindow.Client.SendMessage(MessageType.RootCauseServerAddress, address);
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
    }
}
