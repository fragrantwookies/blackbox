﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BlackBoxEngine;
using eThele.Essentials;
using eNerve.DataTypes;
using eNervePluginInterface;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for ActionsControl.xaml
    /// </summary>
    public partial class ActionsControl : UserControl, IDisposable
    {
        private ObservableCollection<UIAlarmName> alarmNamesList;
        private ObservableCollection<UIAlarmName> alarmActionsList;

        private static ObservableCollection<string> alarmNameStringList;

        private XMLDoc actionsDocBackup;

        public delegate void ActionsDocEventHandler(object sender, XMLDoc _doc);
        public event ActionsDocEventHandler SaveActions;

        public PluginManager Plugin_Manager { get; set; }

        public ActionsControl()
        {
            InitializeComponent();

            alarmNamesList = new ObservableCollection<UIAlarmName>();
            lbAlarmNames.DataContext = alarmNamesList;
            alarmActionsList = new ObservableCollection<UIAlarmName>();
            lbAlarmActions.DataContext = alarmActionsList;

            if(alarmNameStringList == null)
                alarmNameStringList = new ObservableCollection<string>(); 

            imgSiren.DataContext = new UISirenAction();
            imgRelay.DataContext = new UIRelayAction();
            imgCamera.DataContext = new UICameraAction();
            imgEmail.DataContext = new UIEmailAction();
            imgEscalations.DataContext = new UIEscalationAction();
            imgSiteInstructions.DataContext = new UISiteInstructionAction();
            imgPlugin.DataContext = new UIPluginAction();
            imgAutoResolve.DataContext = new UIAutoResolveAction() { AlarmNamesList = alarmNameStringList };
        }

        ~ActionsControl()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.Dispatcher.CheckAccess())
            {
                ClearAlarmNamesList();
                ClearAlarmActionsList();

                IDisposable disposableObject = null;

                if (imgSiren != null)
                {
                    disposableObject = imgSiren.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgRelay != null)
                {
                    disposableObject = imgRelay.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgCamera != null)
                {
                    disposableObject = imgCamera.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgEmail != null)
                {
                    disposableObject = imgEmail.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgEscalations != null)
                {
                    disposableObject = imgEscalations.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgSiteInstructions != null)
                {
                    disposableObject = imgSiteInstructions.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgAutoResolve != null)
                {
                    disposableObject = imgAutoResolve.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
                if (imgPlugin != null)
                {
                    disposableObject = imgPlugin.DataContext as IDisposable;
                    if (disposableObject != null)
                        disposableObject.Dispose();
                }
            }
            else
                try
                {
                    Dispatcher.Invoke(() => { Dispose(disposing); });
                }

                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, 
                        System.Reflection.MethodBase.GetCurrentMethod().Name), "Error disposing actions control: ", ex);
                }
        }

        private void ClearAlarmNamesList()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (alarmNamesList != null)
                    {
                        for (int i = alarmNamesList.Count - 1; i >= 0; i--)
                        {
                            UIAlarmName alarm = alarmNamesList[i];
                            alarmNamesList.RemoveAt(i);
                            alarm.Dispose();
                        }
                    }

                    if (alarmNameStringList != null)
                        alarmNameStringList.Clear();
                }
                else
                {
                    this.Dispatcher.Invoke(ClearAlarmNamesList);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void ClearAlarmActionsList()
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (alarmActionsList != null)
                    {
                        for (int i = alarmActionsList.Count - 1; i >= 0; i--)
                        {
                            UIAlarmName alarm = alarmActionsList[i];
                            alarmActionsList.RemoveAt(i);
                            alarm.Dispose();
                        }
                    }
                }
                else
                {
                    this.Dispatcher.Invoke(ClearAlarmActionsList);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public delegate void AlarmNamesDelegate(List<string> _alarmNames);
        public void SetAlarmNames(List<string> _alarmNames)
        {
            try
            {
                if (_alarmNames != null)
                {
                    if (this.CheckAccess())
                    {
                        ClearAlarmNamesList();

                        bool defaultLoaded = false;
                        foreach (string name in _alarmNames)
                        {
                            UIAlarmName alarmName = new UIAlarmName();
                            alarmName.Name = name.ToLower() == "everything" ? "Default" : name;

                            if (alarmName.Name == "Default")
                                defaultLoaded = true;
                            else 
                                alarmNameStringList.Add(name);

                            alarmNamesList.Add(alarmName);
                        }

                        if (!defaultLoaded)
                        {
                            UIAlarmName alarmName = new UIAlarmName();
                            alarmName.Name = "Default";
                            alarmNamesList.Add(alarmName);
                        }
                    }
                    else
                        this.Dispatcher.Invoke(new AlarmNamesDelegate(SetAlarmNames), new object[] { _alarmNames });
                }
            }
            catch(Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public ObservableCollection<string> PluginNames { get; set; }

        public void PopulatePlugins(List<string> pluginList)
        {
            if (CheckAccess())
            {
                if (PluginNames == null)
                    PluginNames = new ObservableCollection<string>();

                foreach (var plugin in pluginList)
                {
                    if (!PluginNames.Contains(plugin))
                        PluginNames.Add(plugin);
                }
            }
            else
            {
                Dispatcher.Invoke(() => PopulatePlugins(pluginList));
            }
        }

        private delegate void XMLDocDelegate(XMLDoc _doc);
        public void SetActions(XMLDoc actionsDoc = null)
        {
            try
            {
                if (actionsDoc == null)
                    actionsDoc = actionsDocBackup;

                if (actionsDoc != null)
                {
                    if (this.CheckAccess())
                    {
                        ClearAlarmActionsList();

                        actionsDocBackup = actionsDoc;

                        foreach (XmlNode alarmNode in actionsDoc)
                        {
                            UIAlarmName alarm = new UIAlarmName();

                            alarm.Name = alarmNode.Name;
                            alarm.Priority = XmlNode.ParseInteger(alarmNode, 0, "Priority");

                            foreach (XmlNode actionsNode in alarmNode)
                            {
                                UIAction action = ParseActionsNode(actionsNode);
                                if (action != null)
                                {
                                    action.Parent = alarm;
                                    alarm.Actions.Add(action);
                                }
                            }                            

                            alarmActionsList.Add(alarm);
                        }
                    }
                    else
                        this.Dispatcher.Invoke(new XMLDocDelegate(SetActions), new object[] { actionsDoc });
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public delegate void UIAlarmNameDelegate(UIAlarmName _alarmName);
        public void RemoveAlarmName(UIAlarmName _alarmName)
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (_alarmName != null)
                    {
                        int i = alarmActionsList.IndexOf(_alarmName);
                        if (i >= 0) 
                            alarmActionsList.RemoveAt(i);

                        _alarmName.Dispose();
                    }
                }
                else
                    this.Dispatcher.Invoke(new UIAlarmNameDelegate(RemoveAlarmName), new object[] { _alarmName });
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public delegate void UIActionDelegate(UIAction _action);
        public void RemoveAction(UIAction _action)
        {
            try
            {
                if (this.CheckAccess())
                {
                    if (_action != null)
                    {
                        if (_action.Parent is UIAlarmName)
                        {
                            UIAlarmName alarm = (UIAlarmName)_action.Parent;
                            int i = alarm.Actions.IndexOf(_action);
                            if (i >= 0)
                                alarm.Actions.RemoveAt(i);                            
                        }
                        else if (_action.Parent is UIActionGroup)
                        {
                            UIActionGroup actionParent = (UIActionGroup)_action.Parent;
                            int i = actionParent.Actions.IndexOf(_action);
                            if (i >= 0)
                                actionParent.Actions.RemoveAt(i);
                        }
                        else if (_action.Parent is UIEscalationAction)
                        {
                            UIEscalationAction actionParent = (UIEscalationAction)_action.Parent;
                            int i = actionParent.GreenActions.IndexOf(_action);
                            if (i >= 0)
                                actionParent.GreenActions.RemoveAt(i);

                            i = actionParent.YellowActions.IndexOf(_action);
                            if (i >= 0)
                                actionParent.YellowActions.RemoveAt(i);

                            i = actionParent.RedActions.IndexOf(_action);
                            if (i >= 0)
                                actionParent.RedActions.RemoveAt(i);
                        }

                        _action.Dispose();
                    }
                }
                else
                    this.Dispatcher.Invoke(new UIActionDelegate(RemoveAction), new object[] { _action });
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private static UIAction ParseActionsNode(XmlNode _actionsNode)
        {
            UIAction result = null;
            try
            {
                if (_actionsNode != null)
                {
                    if (_actionsNode.Name == "Escalations")
                    {
                        UIEscalationAction escalationAction = (UIEscalationAction)(result = new UIEscalationAction());
                        escalationAction.Name = _actionsNode.Name;

                        XmlNode greenNode = _actionsNode["Green"];
                        if(greenNode == null)
                            greenNode = _actionsNode["Blue"]; //for compatibility's sake
                        if(greenNode != null)
                        {
                            escalationAction.GreenDelay = XmlNode.ParseInteger(greenNode, 0, "Delay");
                            if (greenNode["Actions"] != null)
                            {
                                foreach (XmlNode greenActionNode in greenNode["Actions"])
                                {
                                    UIAction greenActions = ParseActionsNode(greenActionNode);
                                    greenActions.Parent = escalationAction;
                                    escalationAction.GreenActions.Add(greenActions);
                                }
                            }
                        }
                        XmlNode yellowNode = _actionsNode["Yellow"];
                        if (yellowNode != null)
                        {
                            escalationAction.YellowDelay = XmlNode.ParseInteger(yellowNode, 0, "Delay");

                            if (yellowNode["Actions"] != null)
                            {
                                foreach (XmlNode yellowActionNode in yellowNode["Actions"])
                                {
                                    UIAction yellowActions = ParseActionsNode(yellowActionNode);
                                    yellowActions.Parent = escalationAction;
                                    escalationAction.YellowActions.Add(yellowActions);
                                }
                            }
                        }
                        XmlNode redNode = _actionsNode["Red"];
                        if (redNode != null)
                        {
                            escalationAction.RedDelay = XmlNode.ParseInteger(redNode, 0, "Delay");
                            if (redNode["Actions"] != null)
                            {
                                foreach (XmlNode redActionNode in redNode["Actions"])
                                {
                                    UIAction redActions = ParseActionsNode(redActionNode);
                                    redActions.Parent = escalationAction;
                                    escalationAction.RedActions.Add(redActions);
                                }
                            }
                        }
                    }
                    else
                    {
                        UIActionGroup group = (UIActionGroup)(result = new UIActionGroup());
                        group.Name = _actionsNode.Name;

                        foreach (XmlNode actionNode in _actionsNode)
                        {
                            UIAction action = null;

                            //if (group.Name == "Relays")
                            //{
                            //    UIRelayAction relayAction = (UIRelayAction)(action = new UIRelayAction());
                            //    relayAction.RelayName = XmlNode.ParseString(actionNode, actionNode.Name, "RelayName");
                            //    relayAction.DeviceName = XmlNode.ParseString(actionNode, "", "DeviceName");
                            //    relayAction.Duration = XmlNode.ParseDouble(actionNode, 0, "Duration");
                            //}
                            //else if (group.Name == "Cameras")
                            //{
                            //    action = new UICameraAction();
                            //}
                            //else if (group.Name == "Email")
                            //{
                            //    UIEmailAction emailAction = (UIEmailAction)(action = new UIEmailAction());
                            //    emailAction.EmailAddress = XmlNode.ParseString(actionNode, "", "Address");
                            //}
                            //else if (group.Name == "SiteInstructions")
                            //{
                            //    UISiteInstructionAction siteInstructionAction = (UISiteInstructionAction)(action = new UISiteInstructionAction());
                            //    siteInstructionAction.Description = XmlNode.ParseString(actionNode, "");
                            //    siteInstructionAction.CompleteToResolve = XmlNode.ParseBool(actionNode, false, "CompleteToResolve");
                            //}

                            switch(group.Name)
                            {
                                case "Relays":
                                    UIRelayAction relayAction = (UIRelayAction)(action = new UIRelayAction());
                                    relayAction.RelayName = XmlNode.ParseString(actionNode, actionNode.Name, "RelayName");
                                    relayAction.DeviceName = XmlNode.ParseString(actionNode, "", "DeviceName");
                                    relayAction.Duration = XmlNode.ParseDouble(actionNode, 0, "Duration");
                                    break;
                                case "Sirens":
                                    UISirenAction sirenAction = (UISirenAction)(action = new UISirenAction());
                                    sirenAction.SoundFile = XmlNode.ParseString(actionNode, "", "SoundFile");
                                    sirenAction.Duration = XmlNode.ParseDouble(actionNode, 0, "Duration");
                                    break;
                                case "Cameras":
                                    action = new UICameraAction();
                                    break;
                                case "Email":
                                    UIEmailAction emailAction = (UIEmailAction)(action = new UIEmailAction());
                                    emailAction.EmailAddress = XmlNode.ParseString(actionNode, "", "Address");
                                    break;
                                case "SiteInstructions":
                                    UISiteInstructionAction siteInstructionAction = (UISiteInstructionAction)(action = new UISiteInstructionAction());
                                    siteInstructionAction.Description = XmlNode.ParseString(actionNode, "");
                                    siteInstructionAction.CompleteToResolve = XmlNode.ParseBool(actionNode, false, "CompleteToResolve");
                                    break;
                                case "AutoResolves":
                                    UIAutoResolveAction autoResolveAction = (UIAutoResolveAction)(action = new UIAutoResolveAction());
                                    autoResolveAction.AlarmToResolve = XmlNode.ParseString(actionNode, "", "AlarmToResolve");
                                    autoResolveAction.TimeDelay = XmlNode.ParseInteger(actionNode, 0, "TimeDelay");
                                    autoResolveAction.AlarmNamesList = alarmNameStringList;
                                    break;
                                case "Plugins":
                                    UIPluginAction pluginAction = (UIPluginAction)(action = new UIPluginAction());
                                    pluginAction.PluginName = XmlNode.ParseString(actionNode, "", "PluginName");
                                    pluginAction.ActionName = XmlNode.ParseString(actionNode, "", "ActionName");

                                    foreach (XmlItem customFieldNode in actionNode.Attributes)
                                    {
                                        if (customFieldNode.Name != "PluginName" && customFieldNode.Name != "ActionName" && customFieldNode.Name != "ActionOrder")
                                        {
                                            bool val;
                                            if (bool.TryParse(customFieldNode.Value, out val))
                                                pluginAction.CustomFields.Add(new UIPluginActionCustomBoolField() { Parent = pluginAction, FieldName = customFieldNode.Name, FieldValue = val });
                                            else
                                                pluginAction.CustomFields.Add(new UIPluginActionCustomStringField() { Parent = pluginAction, FieldName = customFieldNode.Name, FieldValue = customFieldNode.Value });
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }

                            if (action != null)
                            {
                                action.Name = actionNode.Name;
                                action.Order = XmlNode.ParseInteger(actionNode, 0, "ActionOrder");
                                action.Parent = group;
                                group.Actions.Add(action);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private void btnReload_Click(object sender, RoutedEventArgs e)
        {
            ClearAlarmActionsList();
            SetActions(actionsDocBackup);
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            XMLDoc doc = BuildActionsDoc(alarmActionsList);

            if (doc != null && SaveActions != null)
            {
                SaveActions(this, doc);
            }
        }

        public static List<UIAlarmName> ParseActions(XMLDoc actionsDoc)
        {
            List<UIAlarmName> resultActions = new List<UIAlarmName>();

            foreach (XmlNode alarmNode in actionsDoc)
            {
                UIAlarmName alarm = new UIAlarmName();

                alarm.Name = alarmNode.Name;
                alarm.HiddenField = XmlNode.ParseString(alarmNode, "", "HiddenField");
                alarm.Priority = XmlNode.ParseInteger(alarmNode, 0, "Priority");

                foreach (XmlNode actionsNode in alarmNode)
                {
                    UIAction action = ParseActionsNode(actionsNode);
                    if (action != null)
                    {
                        action.Parent = alarm;
                        alarm.Actions.Add(action);
                    }
                }

                resultActions.Add(alarm);
            }

            return resultActions;
        }

        public static XMLDoc BuildActionsDoc(IEnumerable<UIAlarmName> actions)
        {
            XMLDoc result = null;

            try
            {
                result = new XMLDoc("EventActions");

                foreach (UIAlarmName alarm in actions)
                {
                    XmlNode alarmNode = result.Add(alarm.Name, "");
                    if (alarmNode != null)
                    {
                        alarmNode.AddAttribute("Priority", alarm.Priority.ToString());
                        alarmNode.AddAttribute("HiddenField", alarm.HiddenField);

                        foreach (UIAction action in alarm.Actions)
                        {
                            XmlNode actionNode = BuildActionNode(action);
                            if(actionNode != null)
                                alarmNode.Add(actionNode);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        private static XmlNode BuildActionNode(UIAction _action)
        {
            XmlNode resultNode = null;

            try
            {
                if (_action != null)
                {
                    resultNode = new XmlNode(_action.Name, "");

                    if (_action is UIActionGroup)
                    {
                        UIActionGroup groupAction = (UIActionGroup)_action;

                        foreach (UIAction action in groupAction.Actions)
                        {
                            XmlNode actionNode = BuildActionNode(action);
                            if (actionNode != null)
                                resultNode.Add(actionNode);
                        }
                    }
                    else if(_action is UIEscalationAction)
                    {
                        UIEscalationAction escalationAction = (UIEscalationAction)_action;

                        XmlNode greenNode = resultNode.Add("Green", "");
                        greenNode.AddAttribute("Delay", escalationAction.GreenDelay.ToString());
                        XmlNode greenActionsGroupNode = greenNode.Add("Actions", "");
                        foreach (UIAction action in escalationAction.GreenActions)
                        {
                            XmlNode actionNode = BuildActionNode(action); 
                            if (actionNode != null)
                                greenActionsGroupNode.Add(actionNode);
                        }

                        XmlNode yellowNode = resultNode.Add("Yellow", "");
                        yellowNode.AddAttribute("Delay", escalationAction.YellowDelay.ToString());
                        XmlNode yellowActionsGroupNode = yellowNode.Add("Actions", "");

                        foreach (UIAction action in escalationAction.YellowActions)
                        {
                            XmlNode actionNode = BuildActionNode(action);
                            if (actionNode != null)
                                yellowActionsGroupNode.Add(actionNode);
                        }

                        XmlNode redNode = resultNode.Add("Red", "");
                        redNode.AddAttribute("Delay", escalationAction.RedDelay.ToString());
                        XmlNode redActionsGroupNode = redNode.Add("Actions", "");

                        foreach (UIAction action in escalationAction.RedActions)
                        {
                            XmlNode actionNode = BuildActionNode(action);
                            if (actionNode != null)
                                redActionsGroupNode.Add(actionNode);
                        }
                    }
                    else
                    {
                        resultNode.AddAttribute("ActionOrder", _action.Order.ToString());
                        resultNode.Value = "True";

                        if(_action is UIRelayAction)
                        {
                            UIRelayAction relayAction = (UIRelayAction)_action;
                            resultNode.AddAttribute("RelayName", relayAction.RelayName);
                            resultNode.AddAttribute("DeviceName", relayAction.DeviceName);
                            resultNode.AddAttribute("Duration", relayAction.Duration.ToString());
                        }
                        else if (_action is UISirenAction)
                        {
                            UISirenAction sirenAction = (UISirenAction)_action;
                            resultNode.AddAttribute("SoundFile", sirenAction.SoundFile);
                            resultNode.AddAttribute("Duration", sirenAction.Duration.ToString());
                        }
                        else if (_action is UIEmailAction)
                        {
                            UIEmailAction emailAction = (UIEmailAction)_action;
                            resultNode.AddAttribute("Address",emailAction.EmailAddress);
                        }
                        else if(_action is UISiteInstructionAction)
                        {
                            UISiteInstructionAction siteInstructionAction = (UISiteInstructionAction)_action;
                            resultNode.Value = siteInstructionAction.Description;
                            resultNode.AddAttribute("CompleteToResolve", siteInstructionAction.CompleteToResolve ? "True" : "False");
                        }
                        else if (_action is UIAutoResolveAction)
                        {
                            UIAutoResolveAction siteInstructionAction = (UIAutoResolveAction)_action;
                            resultNode.AddAttribute("AlarmToResolve", siteInstructionAction.AlarmToResolve);
                            resultNode.AddAttribute("TimeDelay", siteInstructionAction.TimeDelay.ToString());
                        }
                        else if(_action is UIPluginAction)
                        {
                            UIPluginAction pluginAction = (UIPluginAction)_action;
                            resultNode.AddAttribute("PluginName", pluginAction.PluginName);
                            resultNode.AddAttribute("ActionName", pluginAction.ActionName);

                            foreach(UIPluginActionCustomBaseField customField in pluginAction.CustomFields)
                            {
                                if(customField is UIPluginActionCustomStringField)
                                    resultNode.AddAttribute(customField.FieldName, ((UIPluginActionCustomStringField)customField).FieldValue);
                                else if (customField is UIPluginActionCustomBoolField)
                                    resultNode.AddAttribute(customField.FieldName, ((UIPluginActionCustomBoolField)customField).FieldValue.ToString());
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return resultNode;
        }

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement && ((FrameworkElement)sender).DataContext is IExpandable)
                {
                    IExpandable expandableControl = (IExpandable)((FrameworkElement)sender).DataContext;

                    if (e.LeftButton == MouseButtonState.Pressed)
                        expandableControl.Expand = !expandableControl.Expand;
                    else if (e.RightButton == MouseButtonState.Pressed)
                    {
                        if (expandableControl.Expand)
                            expandableControl.CollapseAll();
                        else
                            expandableControl.ExpandAll();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private FrameworkElement _dragged;

        private void DragDrop_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_dragged != null)
                return;

            if (sender is UIElement)
            {
                UIElement lb = (UIElement)sender;

                UIElement element = lb.InputHitTest(e.GetPosition(lb)) as UIElement;

                while (element != null)
                {
                    if (element is FrameworkElement)
                    {
                        _dragged = (FrameworkElement)element;
                        break;
                    }
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }
            }
        }

        private void DragDrop_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (_dragged == null)
                    return;
                if (e.LeftButton == MouseButtonState.Released)
                {
                    _dragged = null;
                    return;
                }
                else if (e.LeftButton == MouseButtonState.Pressed)
                {
                    if (_dragged is FrameworkElement)
                    {
                        object data = ((FrameworkElement)_dragged).DataContext;
                        if (data is UIAlarmName || data is UIAction || data is UIPluginActionCustomBaseField)
                        {
                            this.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new System.Threading.ParameterizedThreadStart(DoDragDrop), data);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                _dragged = null;
            }
        }

        private void DoDragDrop(object parameter)
        {
            try
            {
                if (_dragged != null)
                {
                    System.Windows.DragDrop.DoDragDrop(_dragged, parameter, DragDropEffects.All);
                    _dragged = null;
                }
            }
            catch (Exception ex)
            {
                _dragged = null;
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void NewActionDrop(object sender, DragEventArgs e)
        {
            object alarmNameObj = e.Data.GetData(typeof(UIAlarmName));
            if (alarmNameObj is UIAlarmName)
            {
                UIAlarmName alarmName = ObjectCopier.Clone<UIAlarmName>((UIAlarmName)alarmNameObj);

                int foundIndex = alarmActionsList.IndexOf(alarmName);

                if (foundIndex < 0)
                {
                    alarmActionsList.Add(alarmName);
                    alarmName.Expand = true;
                }
                else
                    alarmActionsList[foundIndex].Expand = true;
            }
        }

        private void ActionDrop(object sender, DragEventArgs e)
        {
            try
            {
                object actionObj = e.Data.GetData(typeof(UIRelayAction));
                if (!(actionObj is UIRelayAction))
                {
                    actionObj = e.Data.GetData(typeof(UISirenAction));
                    if (!(actionObj is UISirenAction))
                    {
                        actionObj = e.Data.GetData(typeof(UICameraAction));
                        if (!(actionObj is UICameraAction))
                        {
                            actionObj = e.Data.GetData(typeof(UIEmailAction));
                            if (!(actionObj is UIEmailAction))
                            {
                                actionObj = e.Data.GetData(typeof(UIEscalationAction));
                                if (!(actionObj is UIEscalationAction))
                                {
                                    actionObj = e.Data.GetData(typeof(UISiteInstructionAction));
                                    if (!(actionObj is UISiteInstructionAction))
                                    {
                                        actionObj = e.Data.GetData(typeof(UIAutoResolveAction));
                                        if (!(actionObj is UIAutoResolveAction))
                                        {
                                            actionObj = e.Data.GetData(typeof(UIPluginAction));
                                            if (!(actionObj is UIPluginAction))
                                            {
                                                actionObj = null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (actionObj != null)
                {
                    UIAction action = ObjectCopier.Clone<UIAction>((UIAction)actionObj);

                    if (action is UIPluginAction)
                    {
                        UIPluginAction pluginAction = action as UIPluginAction;

                        if (pluginAction.CustomFields != null)
                        {
                            foreach (UIPluginActionCustomBaseField field in pluginAction.CustomFields)
                                field.Parent = pluginAction;
                        }
                    }

                    if (sender is FrameworkElement)
                    {
                        object dropdata = ((FrameworkElement)sender).DataContext;
                        if (dropdata is UIAlarmName)
                        {
                            UIAlarmName alarm = (UIAlarmName)dropdata;
                            if (action is UIRelayAction || action is UISirenAction || action is UIEmailAction || action is UICameraAction || action is UISiteInstructionAction || action is UIAutoResolveAction || action is UIPluginAction)
                            {
                                bool found = false;
                                foreach (UIAction currAction in alarm.Actions)
                                {
                                    if (currAction is UIActionGroup)
                                    {
                                        if ((currAction.Name == "Relays" && action is UIRelayAction)
                                            || (currAction.Name == "Sirens" && action is UISirenAction)
                                            || (currAction.Name == "Email" && action is UIEmailAction)
                                            || (currAction.Name == "Cameras" && action is UICameraAction)
                                            || (currAction.Name == "SiteInstructions" && action is UISiteInstructionAction)
                                            || (currAction.Name == "AutoResolves" && action is UIAutoResolveAction)
                                            || (currAction.Name == "Plugins" && action is UIPluginAction))
                                        {
                                            found = true;
                                            action.Parent = currAction;

                                            ((UIActionGroup)currAction).AddAction(action);

                                            alarm.Expand = true;
                                            ((UIActionGroup)currAction).Expand = true;
                                            break;
                                        }
                                    }
                                }

                                if (!found)
                                {
                                    UIActionGroup group = new UIActionGroup();
                                    if (action is UIRelayAction)
                                        group.Name = "Relays";
                                    else if (action is UISirenAction)
                                        group.Name = "Sirens";
                                    else if (action is UIEmailAction)
                                        group.Name = "Email";
                                    else if (action is UICameraAction)
                                        group.Name = "Cameras";
                                    else if (action is UISiteInstructionAction)
                                        group.Name = "SiteInstructions";
                                    else if (action is UIAutoResolveAction)
                                        group.Name = "AutoResolves";
                                    else if (action is UIPluginAction)
                                        group.Name = "Plugins";

                                    action.Parent = group;
                                    group.AddAction(action);
                                    group.Parent = alarm;
                                    alarm.Actions.Add(group);
                                    alarm.Expand = true;
                                    group.Expand = true;
                                }
                            }
                            else if (action is UIEscalationAction)
                            {
                                bool found = false;
                                foreach (UIAction currAction in alarm.Actions)
                                {
                                    if (currAction is UIEscalationAction)
                                    {
                                        found = true;
                                        break;
                                    }
                                }

                                if (!found)
                                {
                                    action.Parent = alarm;
                                    alarm.Actions.Add(action);
                                    alarm.Expand = true;
                                    action.Name = "Escalations";
                                }
                            }

                            e.Handled = true;
                        }
                        else if (dropdata is UIActionGroup)
                        {
                            if ((((UIActionGroup)dropdata).Name == "Relays" && action is UIRelayAction)
                                || (((UIActionGroup)dropdata).Name == "Sirens" && action is UISirenAction)
                                || (((UIActionGroup)dropdata).Name == "Email" && action is UIEmailAction)
                                || (((UIActionGroup)dropdata).Name == "Cameras" && action is UICameraAction)
                                || (((UIActionGroup)dropdata).Name == "SiteInstructions" && action is UISiteInstructionAction)
                                || (((UIActionGroup)dropdata).Name == "AutoResolves" && action is UIAutoResolveAction)
                                || (((UIActionGroup)dropdata).Name == "Plugins" && action is UIPluginAction))
                            {
                                action.Parent = (UIActionGroup)dropdata;
                                ((UIActionGroup)dropdata).AddAction(action);
                                ((UIActionGroup)dropdata).Expand = true;

                                e.Handled = true;
                            }
                        }
                        else if (dropdata is UIEscalationAction)
                        {
                            UIEscalationAction escalation = (UIEscalationAction)dropdata;
                            ObservableCollection<UIAction> actions = null;
                            string dropName = ((FrameworkElement)sender).Name;
                            if (dropName == "lbGreen" || dropName == "brdrGreen")
                            {
                                actions = escalation.GreenActions;
                            }
                            else if (dropName == "lbYellow" || dropName == "brdrYellow")
                            {
                                actions = escalation.YellowActions;
                            }
                            else if (dropName == "lbRed" || dropName == "brdrRed")
                            {
                                actions = escalation.RedActions;
                            }

                            if (actions != null && (action is UIRelayAction || action is UISirenAction || action is UIEmailAction || action is UICameraAction || action is UIPluginAction))
                            {
                                bool found = false;
                                foreach (UIAction currAction in actions)
                                {
                                    if (currAction is UIActionGroup)
                                    {
                                        if ((currAction.Name == "Relays" && action is UIRelayAction)
                                            || (currAction.Name == "Sirens" && action is UISirenAction)
                                            || (currAction.Name == "Email" && action is UIEmailAction)
                                            || (currAction.Name == "Cameras" && action is UICameraAction)
                                            || (currAction.Name == "SiteInstructions" && action is UISiteInstructionAction)
                                            || (currAction.Name == "Plugins" && action is UIPluginAction))
                                        {
                                            found = true;
                                            action.Parent = currAction;
                                            ((UIActionGroup)currAction).AddAction(action);
                                            ((UIActionGroup)currAction).Expand = true;
                                            break;
                                        }
                                    }
                                }

                                if (!found)
                                {
                                    UIActionGroup group = new UIActionGroup();
                                    if (action is UIRelayAction)
                                        group.Name = "Relays";
                                    else if (action is UISirenAction)
                                        group.Name = "Sirens";
                                    else if (action is UIEmailAction)
                                        group.Name = "Email";
                                    else if (action is UICameraAction)
                                        group.Name = "Cameras";
                                    else if (action is UISiteInstructionAction)
                                        group.Name = "SiteInstructions";
                                    else if (action is UIPluginAction)
                                        group.Name = "Plugins";

                                    action.Parent = group;
                                    group.AddAction(action);
                                    group.Parent = escalation;
                                    group.Expand = true;
                                    actions.Add(group);
                                }
                            }

                            e.Handled = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void DeleteDrop(object sender, DragEventArgs e)
        {
            object actionObj = e.Data.GetData(typeof(UIPluginActionCustomBaseField));
            if (!(actionObj is UIPluginActionCustomBaseField))
            {
                actionObj = e.Data.GetData(typeof(UIPluginAction));
                if (!(actionObj is UIPluginAction))
                {
                    actionObj = e.Data.GetData(typeof(UISirenAction));
                    if (!(actionObj is UISirenAction))
                    {
                        actionObj = e.Data.GetData(typeof(UIRelayAction));
                        if (!(actionObj is UIRelayAction))
                        {
                            actionObj = e.Data.GetData(typeof(UICameraAction));
                            if (!(actionObj is UICameraAction))
                            {
                                actionObj = e.Data.GetData(typeof(UIEmailAction));
                                if (!(actionObj is UIEmailAction))
                                {
                                    actionObj = e.Data.GetData(typeof(UIEscalationAction));
                                    if (!(actionObj is UIEscalationAction))
                                    {
                                        actionObj = e.Data.GetData(typeof(UISiteInstructionAction));
                                        if (!(actionObj is UISiteInstructionAction))
                                        {
                                            actionObj = e.Data.GetData(typeof(UIAutoResolveAction));
                                            if (!(actionObj is UIAutoResolveAction))
                                            {
                                                actionObj = e.Data.GetData(typeof(UIActionGroup));
                                                if (!(actionObj is UIActionGroup))
                                                    actionObj = null;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (actionObj is UIAction)
            {
                RemoveAction((UIAction)actionObj);
            }
            else if(actionObj is UIPluginActionCustomBaseField)
            {
                UIPluginActionCustomBaseField field = actionObj as UIPluginActionCustomBaseField;

                if (field != null)
                {
                    field.Parent.RemoveChild(field);
                    field.Dispose();
                }
            }
            else
            {
                object alarmObj = e.Data.GetData(typeof(UIAlarmName));

                if (alarmObj != null)
                {
                    RemoveAlarmName((UIAlarmName)alarmObj);
                }
            }
        }

        private void TextBox_PreviewDragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            e.Handled = true;
        }

        private void TextBox_PreviewDragOver(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Copy;
            e.Handled = true;
        }

        private void RemoveCustomPluginField_Button_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement fElement = sender as FrameworkElement;
            if(fElement != null)
            {
                UIPluginActionCustomBaseField field = fElement.DataContext as UIPluginActionCustomBaseField;

                if (field != null && field.Parent != null)
                {
                    field.Parent.RemoveChild(field);
                    field.Dispose();
                }
            }
        }

        private void AddCustomPluginField_Button_Click(object sender, RoutedEventArgs e)
        {
            FrameworkElement fElement = sender as FrameworkElement;
            if (fElement != null)
            {
                UIPluginAction pluginAction = fElement.DataContext as UIPluginAction;

                UIPluginActionCustomBaseField field = new UIPluginActionCustomStringField() { Parent = pluginAction, Name = pluginAction.CustomFields.GetUniqueName<UIPluginActionCustomBaseField>("CustomField") };

                pluginAction.CustomFields.Add(field);
                pluginAction.ExpandAll();
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox senderComboBox = (sender as ComboBox);

            if (senderComboBox != null)
            {
                senderComboBox.SelectionChanged -= ComboBox_SelectionChanged;

                UIPluginActionCustomBaseField field = senderComboBox.DataContext as UIPluginActionCustomBaseField;

                if (field != null && field.Parent != null)
                {
                    UIPluginAction action = field.Parent as UIPluginAction;

                    if (action != null)
                    {
                        action.RemoveChild(field);

                        if (action.GetCustomFieldType(field.FieldName).Equals(typeof(bool)))
                            action.CustomFields.Add(new UIPluginActionCustomBoolField() { Parent = action, Name = action.CustomFields.GetUniqueName<UIPluginActionCustomBaseField>("CustomField"), FieldName = field.FieldName });
                        else
                            action.CustomFields.Add(new UIPluginActionCustomStringField() { Parent = action, Name = action.CustomFields.GetUniqueName<UIPluginActionCustomBaseField>("CustomField"), FieldName = field.FieldName });
                        field.Dispose();
                    }
                }

                senderComboBox.SelectionChanged += ComboBox_SelectionChanged;
            }
        }
    }
}
