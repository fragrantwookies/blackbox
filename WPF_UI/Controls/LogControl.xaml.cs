﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eNerve.DataTypes;
using eThele.Essentials;
using System.Collections.ObjectModel;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for LogControl.xaml
    /// </summary>
    public partial class LogControl : UserControl
    {
        private ObservableCollection<object> log;

        public LogControl()
        {
            InitializeComponent();

            log = new ObservableCollection<object>();
            lbLog.DataContext = log;
        }

        public void AddItem(object _item)
        {
            if (CheckAccess())
            {
                log.Insert(0, _item);

                while (log.Count > 1000)
                {
                    log.RemoveAt(log.Count - 1);
                }
            }
            else
                this.Dispatcher.Invoke(delegate() { AddItem(_item); });
        }

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement && ((FrameworkElement)sender).DataContext is ExceptionItem)
                {
                    ExceptionItem expandableControl = (ExceptionItem)((FrameworkElement)sender).DataContext;
                    if (e.LeftButton == MouseButtonState.Pressed)
                        expandableControl.Expand = !expandableControl.Expand;
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
    }
}
