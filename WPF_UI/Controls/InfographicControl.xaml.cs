﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for InfographicControl.xaml
    /// </summary>
    public partial class InfographicControl : UserControl
    {
        public InfographicControl()
        {
            InitializeComponent();
            browser.Navigated += Browser_Navigated;
            OverviewClick(this, null);
        }

        private void Browser_Navigated(object sender, NavigationEventArgs e)
        {
            if (browser != null)
            {
                if (browser.Document is IOleServiceProvider sp)
                {
                    Guid IID_IWebBrowserApp = new Guid("0002DF05-0000-0000-C000-000000000046");
                    Guid IID_IWebBrowser2 = new Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E");

                    object webBrowser;
                    sp.QueryService(ref IID_IWebBrowserApp, ref IID_IWebBrowser2, out webBrowser);
                    if (webBrowser != null)
                    {
                        webBrowser.GetType().InvokeMember("Silent", BindingFlags.Instance | BindingFlags.Public | BindingFlags.PutDispProperty, null, webBrowser, new object[] { true });
                    }
                }
            }
        }

        private void AlarmNameClick(object sender, RoutedEventArgs e)
        {
            browser.Navigate(@"http://192.168.1.230:3000/public/question/b2c1fa88-04de-4baf-80f2-f3aaf8d92c64#theme=night&refresh=60");
        }

        [ComImport, Guid("6D5140C1-7436-11CE-8034-00AA006009FA"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
        private interface IOleServiceProvider
        {
            [PreserveSig]
            int QueryService([In] ref Guid guidService, [In] ref Guid riid, [MarshalAs(UnmanagedType.IDispatch)] out object ppvObject);
        }

        private void OverviewClick(object sender, RoutedEventArgs e)
        {
            browser.Navigate(@"http://192.168.1.230:3000/public/dashboard/6932c329-2608-4328-9173-8e3ae6c65ab8#theme=night&refresh=60");
        }

        private void AlarmDeviceClick(object sender, RoutedEventArgs e)
        {
            browser.Navigate(@"http://192.168.1.230:3000/public/question/e688830c-0bf7-4e5e-8a2d-4427a431ee91#theme=night&refresh=60");
        }
    }
}
