﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace WPF_UI.Controls
{
    /// <summary>
    /// Interaction logic for DashboardImageControl.xaml
    /// </summary>
    public partial class DashboardImageControl : UserControl
    {
        public eNerve.DataTypes.DashboardImage DashboardImage { get { return dashboardImage; }
            set { dashboardImage = value; } }

        private eNerve.DataTypes.DashboardImage dashboardImage;

        public DashboardImageControl()
        {
            InitializeComponent();
            DashboardImage = new eNerve.DataTypes.DashboardImage();
            //image.Source = DashboardImage.Value;
        }
    }
}
