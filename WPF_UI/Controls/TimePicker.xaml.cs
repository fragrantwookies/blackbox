﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for TimePicker.xaml
    /// </summary>
    public partial class TimePicker : UserControl
    {
        public TimePicker()
        {
            InitializeComponent();
        }

        public TimeSpan Value
        {
            get { return (TimeSpan)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty =
        DependencyProperty.Register("Value", typeof(TimeSpan), typeof(TimePicker),
        new UIPropertyMetadata(TimeSpan.Zero, new PropertyChangedCallback(OnValueChanged)));

        private static void OnValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            TimePicker control = obj as TimePicker;
            if(control.Hours != ((TimeSpan)e.NewValue).Hours)
                control.Hours = ((TimeSpan)e.NewValue).Hours;
            if(control.Minutes != ((TimeSpan)e.NewValue).Minutes)
                control.Minutes = ((TimeSpan)e.NewValue).Minutes;
            if(control.Seconds != ((TimeSpan)e.NewValue).Seconds)
                control.Seconds = ((TimeSpan)e.NewValue).Seconds;
        }

        public int Hours
        {
            get { return (int)GetValue(HoursProperty); }
            set 
            {
                if (value >= 24)
                    value = 0;
                else if (value < 0)
                    value = 23;

                SetValue(HoursProperty, value); 
            }
        }
        public static readonly DependencyProperty HoursProperty =
        DependencyProperty.Register("Hours", typeof(int), typeof(TimePicker),
        new UIPropertyMetadata(0, new PropertyChangedCallback(OnTimeChanged)));

        public int Minutes
        {
            get { return (int)GetValue(MinutesProperty); }
            set 
            {
                if (value >= 60)
                    value = 0;
                else if (value < 0)
                    value = 59;

                SetValue(MinutesProperty, value); 
            }
        }
        public static readonly DependencyProperty MinutesProperty =
        DependencyProperty.Register("Minutes", typeof(int), typeof(TimePicker),
        new UIPropertyMetadata(0, new PropertyChangedCallback(OnTimeChanged)));

        public int Seconds
        {
            get { return (int)GetValue(SecondsProperty); }
            set 
            {
                if (value >= 60)
                    value = 0;
                else if (value < 0)
                    value = 59;

                SetValue(SecondsProperty, value); 
            }
        }

        public static readonly DependencyProperty SecondsProperty =
        DependencyProperty.Register("Seconds", typeof(int), typeof(TimePicker),
        new UIPropertyMetadata(0, new PropertyChangedCallback(OnTimeChanged)));


        private static void OnTimeChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            TimePicker control = obj as TimePicker;

            int hours = control.Hours;
            hours = hours > 23 ? 23 : hours < 0 ? 0 : hours;
            int mins = control.Minutes;
            mins = mins > 59 ? 59 : mins < 0 ? 0 : mins;
            int secs = control.Seconds;
            secs = secs > 59 ? 59 : secs < 0 ? 0 : secs;
            
            control.Value = new TimeSpan(hours, mins, secs);
        }

        private void Down(object sender, KeyEventArgs args)
        {
            if (sender is TextBox)
            {
                switch (((TextBox)sender).Name)
                {
                    case "txtSS":
                        if (args.Key == Key.Up)
                            this.Seconds++;
                        if (args.Key == Key.Down)
                            this.Seconds--;
                        break;

                    case "txtMM":
                        if (args.Key == Key.Up)
                            this.Minutes++;
                        if (args.Key == Key.Down)
                            this.Minutes--;
                        break;

                    case "txtHH":
                        if (args.Key == Key.Up)
                            this.Hours++;
                        if (args.Key == Key.Down)
                            this.Hours--;
                        break;
                }
            }
        }

        private void OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (sender is TextBox)
            {
                switch (((TextBox)sender).Name)
                {
                    case "txtSS":
                        this.Seconds += e.Delta >= 0 ? 1 : -1;
                        break;

                    case "txtMM":
                        this.Minutes += e.Delta >= 0 ? 1 : -1;
                        break;

                    case "txtHH":
                        this.Hours += e.Delta >= 0 ? 1 : -1;
                        break;
                }
            }
        }

        private void Validate(System.Object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            bool valid = false;
            int val = 0;
            if (int.TryParse(e.Text, out val))
                valid = val >= 0 && val <= 59;

            e.Handled = !valid;
        }
    }
}
