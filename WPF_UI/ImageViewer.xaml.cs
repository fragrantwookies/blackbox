﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for ImageViewer.xaml
    /// </summary>
    public partial class ImageViewer : Window
    {
        private ZoomAndPan.ZoomAndPanControl currentZoomAndPanControl = null;
        private Canvas currentCanvasContent = null;

        private MouseHandlingMode mouseHandlingMode = MouseHandlingMode.None;
        private MouseButton mouseButtonDown;
        private Point origZoomAndPanControlMouseDownPoint;
        private Point origContentMouseDownPoint;

        public ImageViewer()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Defines the current state of the mouse handling logic.
        /// </summary>
        public enum MouseHandlingMode
        {
            /// <summary>
            /// Not in any special mode.
            /// </summary>
            None,

            /// <summary>
            /// The user is left-dragging elements with the mouse.
            /// </summary>
            DraggingElements,

            /// <summary>
            /// The user is left-mouse-button-dragging to pan the viewport.
            /// </summary>
            Panning,

            /// <summary>
            /// The user is holding down shift and left-clicking or right-clicking to zoom in or out.
            /// </summary>
            Zooming,

            /// <summary>
            /// The user is holding down shift and left-mouse-button-dragging to select a region to zoom to.
            /// </summary>
            DragZooming,
        }

        /// <summary>
        /// Zoom the viewport out, centering on the specified point (in content coordinates).
        /// </summary>
        private void ZoomOut(Point contentZoomCenter)
        {
            if (currentZoomAndPanControl.ViewportWidth < currentZoomAndPanControl.ExtentWidth || currentZoomAndPanControl.ViewportHeight < currentZoomAndPanControl.ExtentHeight)
                currentZoomAndPanControl.ZoomAboutPoint(currentZoomAndPanControl.ContentScale - 0.1, contentZoomCenter);
        }

        /// <summary>
        /// Zoom the viewport in, centering on the specified point (in content coordinates).
        /// </summary>
        private void ZoomIn(Point contentZoomCenter)
        {
            currentZoomAndPanControl.ZoomAboutPoint(currentZoomAndPanControl.ContentScale + 0.1, contentZoomCenter);
        }

        public void ZoomToExtent()
        {
            if (this.CheckAccess())
            {
                ZoomToExtentTS();
            }
            else
            {
                this.Dispatcher.Invoke(ZoomToExtentTS);
            }
        }

        protected void ZoomToExtentTS()
        {
            currentZoomAndPanControl.AnimatedZoomTo(new Rect(0, 0, currentCanvasContent.Width + 10, currentCanvasContent.Height + 10));
        }

        private void zoomAndPanControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is ZoomAndPan.ZoomAndPanControl)
            {
                currentZoomAndPanControl = (ZoomAndPan.ZoomAndPanControl)sender;                

                if (currentZoomAndPanControl.Name == "zoomAndPanPreControl")
                    currentCanvasContent = canvasPre;
                else if (currentZoomAndPanControl.Name == "zoomAndPanCurrControl")
                    currentCanvasContent = canvasCurr;
                else if (currentZoomAndPanControl.Name == "zoomAndPanPostControl")
                    currentCanvasContent = canvasPost;

                if (currentCanvasContent != null)
                {
                    currentCanvasContent.Focus();
                    Keyboard.Focus(currentCanvasContent);

                    mouseButtonDown = e.ChangedButton;
                    origZoomAndPanControlMouseDownPoint = e.GetPosition(currentZoomAndPanControl);
                    origContentMouseDownPoint = e.GetPosition(currentCanvasContent);

                    if ((Keyboard.Modifiers & ModifierKeys.Shift) != 0 &&
                        (e.ChangedButton == MouseButton.Left ||
                         e.ChangedButton == MouseButton.Right))
                    {
                        // Shift + left- or right-down initiates zooming mode.
                        mouseHandlingMode = MouseHandlingMode.Zooming;
                    }
                    else if (mouseButtonDown == MouseButton.Left)
                    {
                        // Just a plain old left-down initiates panning mode.
                        mouseHandlingMode = MouseHandlingMode.Panning;
                    }

                    if (mouseHandlingMode != MouseHandlingMode.None)
                    {
                        // Capture the mouse so that we eventually receive the mouse up event.
                        currentZoomAndPanControl.CaptureMouse();
                        e.Handled = true;
                    }
                }
            }
        }

        private void zoomAndPanControl_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sender is ZoomAndPan.ZoomAndPanControl)
            {
                if (sender == currentZoomAndPanControl)
                {
                    if (mouseHandlingMode != MouseHandlingMode.None)
                    {
                        if (mouseHandlingMode == MouseHandlingMode.Zooming)
                        {
                            if (mouseButtonDown == MouseButton.Left)
                            {
                                // Shift + left-click zooms in on the content.
                                ZoomIn(origContentMouseDownPoint);
                            }
                            else if (mouseButtonDown == MouseButton.Right)
                            {
                                // Shift + left-click zooms out from the content.
                                ZoomOut(origContentMouseDownPoint);
                            }
                        }
                        else if (mouseHandlingMode == MouseHandlingMode.DragZooming)
                        {
                            // When drag-zooming has finished we zoom in on the rectangle that was highlighted by the user.
                            ApplyDragZoomRect();
                        }

                        zoomAndPanControl.ReleaseMouseCapture();
                        mouseHandlingMode = MouseHandlingMode.None;
                        e.Handled = true;
                    }
                }
            }
        }

        private void zoomAndPanControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseHandlingMode == MouseHandlingMode.Panning)
            {
                // The user is left-dragging the mouse.
                // Pan the viewport by the appropriate amount.
                Point curContentMousePoint = e.GetPosition(canvasContent);
                Vector dragOffset = curContentMousePoint - origContentMouseDownPoint;

                zoomAndPanControl.ContentOffsetX -= dragOffset.X;
                zoomAndPanControl.ContentOffsetY -= dragOffset.Y;

                e.Handled = true;
            }
            else if (mouseHandlingMode == MouseHandlingMode.Zooming)
            {
                Point curZoomAndPanControlMousePoint = e.GetPosition(zoomAndPanControl);
                Vector dragOffset = curZoomAndPanControlMousePoint - origZoomAndPanControlMouseDownPoint;
                double dragThreshold = 10;
                if (mouseButtonDown == MouseButton.Left &&
                    (Math.Abs(dragOffset.X) > dragThreshold ||
                     Math.Abs(dragOffset.Y) > dragThreshold))
                {
                    // When Shift + left-down zooming mode and the user drags beyond the drag threshold,
                    // initiate drag zooming mode where the user can drag out a rectangle to select the area
                    // to zoom in on.
                    mouseHandlingMode = MouseHandlingMode.DragZooming;
                    Point curContentMousePoint = e.GetPosition(canvasContent);
                    InitDragZoomRect(origContentMouseDownPoint, curContentMousePoint);
                }

                e.Handled = true;
            }
            else if (mouseHandlingMode == MouseHandlingMode.DragZooming)
            {
                // When in drag zooming mode continously update the position of the rectangle
                // that the user is dragging out.
                Point curContentMousePoint = e.GetPosition(canvasContent);
                SetDragZoomRect(origContentMouseDownPoint, curContentMousePoint);

                e.Handled = true;
            }
        }

        private void zoomAndPanControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            e.Handled = true;

            if (e.Delta > 0)
            {
                Point curContentMousePoint = e.GetPosition(canvasContent);
                ZoomIn(curContentMousePoint);
            }
            else if (e.Delta < 0)
            {
                Point curContentMousePoint = e.GetPosition(canvasContent);
                ZoomOut(curContentMousePoint);
            }
        }

        /// <summary>
        /// Event raised when the user has double clicked in the zoom and pan control.
        /// </summary>
        private void zoomAndPanControl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Shift) == 0)
            {
                Point doubleClickPoint = e.GetPosition(canvasContent);
                zoomAndPanControl.AnimatedSnapTo(doubleClickPoint);
            }
        }
    }
}
