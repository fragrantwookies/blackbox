﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BlackBoxEngine;
using eNerve.DataTypes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for AlarmDetailWindow.xaml
    /// </summary>
    public partial class AlarmDetailWindow : Window
    {
        public event AlarmDetail.AlarmResolveEventHandler AlarmResolved;
        public event AlarmDetail.SiteInstructionStepEventHandler SiteInstructionStepChanged;

        public AlarmDetailWindow()
        {
            InitializeComponent();

            this.AlarmDetailControl.AlarmResolved += AlarmDetailControl_AlarmResolved;
            this.AlarmDetailControl.SiteInstructionStepChanged += AlarmDetailControl_SiteInstructionStepChanged;
        }

        void AlarmDetailControl_AlarmResolved(object sender, AlarmBase _alarm, AlarmResolve _resolveDetailItem)
        {
            if (AlarmResolved != null)
                AlarmResolved(sender, _alarm, _resolveDetailItem);
        }

        void AlarmDetailControl_SiteInstructionStepChanged(object sender, SiteInstructionStep _siteInstructionStep)
        {
            if (SiteInstructionStepChanged != null)
                SiteInstructionStepChanged(this, _siteInstructionStep);
        }

        public AlarmBase Alarm
        {
            get
            {
                return this.AlarmDetailControl.Alarm; 
            }
            set
            {
                this.AlarmDetailControl.Alarm = value;
            }
        }
    }
}
