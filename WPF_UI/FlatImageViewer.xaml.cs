﻿using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for FlatImageViewer.xaml
    /// </summary>
    public partial class FlatImageViewer : Window
    {
        private List<EventImage> images;
        private int currentImageIndex = 0;

        public FlatImageViewer()
        {
            InitializeComponent();
        }

        public void LoadImages(List<EventImage> _images)
        {
            images = _images;
            if (images != null && images.Count() > 0)
                LoadCurrAlarmImage(images[currentImageIndex = 0].ImageData);
        }

        public void LoadImage(byte[] image)
        {
            LoadCurrAlarmImage(image);
        }

        private void LoadCurrAlarmImage(byte[] image)
        {
            currViewer.LoadBackgroud(image);

            if (images != null)
            {
                this.Title = string.Format("Image Viewer : {0} - {1}", images[currentImageIndex].CameraAddress, images[currentImageIndex].TimeStamp.ToString("yyyy/MM/dd HH:mm:ss"));
            }

                if (currentImageIndex > 0)
                    imgBack.Visibility = System.Windows.Visibility.Visible;
                else
                    imgBack.Visibility = System.Windows.Visibility.Hidden;

                if (images != null && currentImageIndex < images.Count - 1)
                    imgForward.Visibility = System.Windows.Visibility.Visible;
                else
                    imgForward.Visibility = System.Windows.Visibility.Hidden;

            currViewer.ZoomToExtent();
        }

        private void imgForward_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (currentImageIndex < (images.Count - 1))
                LoadCurrAlarmImage(images[++currentImageIndex].ImageData);
        }

        private void imgBack_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (currentImageIndex > 0)
                LoadCurrAlarmImage(images[--currentImageIndex].ImageData);
        }

        private DateTime lastResize = DateTime.Now;

        private bool sizeChangedThreadRunning = false;

        private void SizeChangedWait()
        {
            sizeChangedThreadRunning = true;

            while ((DateTime.Now - lastResize).TotalMilliseconds < 300)
                Thread.Sleep(10);

            currViewer.ZoomToExtent();

            sizeChangedThreadRunning = false;
        }

        private void currViewer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            lastResize = DateTime.Now;

            if (!sizeChangedThreadRunning)
            {
                Thread sizeChangedThread = new Thread(SizeChangedWait);
                sizeChangedThread.Start();
            }
        }
    }
}
