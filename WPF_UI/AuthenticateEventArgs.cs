using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    public class AuthenticateEventArgs : EventArgs
    {
        public string Message { get; private set; }
        public AuthenticateEventArgs(string message = null)
        {
            Message = message;
        }
    }
}
