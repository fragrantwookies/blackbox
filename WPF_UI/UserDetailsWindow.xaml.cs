﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for ChangePasswordWindow.xaml
    /// </summary>
    public partial class ChangePasswordWindow : Window
    {
        public string UserName
        {
            get
            {
                return txtUserName.Text;
            }
            set
            {
                txtUserName.Text = value;
            }
        }

        public string NewPassword
        {
            get
            {
                return txtNewPassword.Password;
            }
            set
            {
                txtNewPassword.Password = value;
            }
        }

        public ChangePasswordWindow()
        {
            InitializeComponent();
        }

        private void btnChange_Click(object sender, RoutedEventArgs e)
        {
            ChangePassword();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void btnChange_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key.Equals(Key.Enter)) || (e.Key.Equals(Key.Space)))
                ChangePassword();
        }

        private void ChangePassword()
        {
            btnChange.IsEnabled = false;
            if (!string.IsNullOrWhiteSpace(txtUserName.Text)) // Set user name rules here
            {
                this.DialogResult = true;
                this.Close();
            }
        }

        private void txtConfirmPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (txtConfirmPassword.Password == txtNewPassword.Password)
                btnChange.IsEnabled = true;
            else
                btnChange.IsEnabled = false;
        }

        private void txtUserName_TextChanged(object sender, TextChangedEventArgs e)
        {
            btnChange.IsEnabled = !string.IsNullOrEmpty(txtUserName.Text);
        }
    }
}
