﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using eNervePluginInterface;

namespace WPF_UI.DeviceWizard
{
    public class DeviceTypeModel : DeviceWizardPageBase
    {
        #region Fields        

        private ObservableCollection<INervePlugin> _availablePluginTypes = new ObservableCollection<INervePlugin>();
        private static INervePlugin _selectedType;

        #endregion Fields

        #region Constructor
        public DeviceTypeModel(ObservableCollection<INervePlugin> pluginTypes, INervePlugin selectedType)
            : base()
        {
            _selectedType = selectedType;
            foreach (INervePlugin pluginType in pluginTypes)
            {
                if (pluginType as ICameraServerPlugin == null)
                    _availablePluginTypes.Add(pluginType);
            }
        }

        #endregion Constructor

        #region Properties

        public ObservableCollection<INervePlugin> AvailablePluginTypes
        {
            get { return _availablePluginTypes; }
        }

        public override string DisplayName
        {
            get
            {
                return "Device type and quantity";
            }
        }

        public static int NumberOfDevices { get; set; }
        
        public static INervePlugin SelectedType
        {
            get { return _selectedType; }
            set { _selectedType = value; }
        }

        #endregion Properties

        #region Methods

        internal override bool IsValid()
        {
            return (NumberOfDevices > 0);
        }

        #endregion Methods
    }
}
