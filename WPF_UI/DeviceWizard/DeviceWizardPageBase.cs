﻿using System;
using System.ComponentModel;
using System.Collections.ObjectModel;

using eNervePluginInterface;
using eNerve.DataTypes;

namespace WPF_UI.DeviceWizard
{
    public abstract class DeviceWizardPageBase : INotifyPropertyChanged
    {
        #region Fields
        bool _isCurrentPage;
        #endregion Fields

        #region Constructor
        protected DeviceWizardPageBase() { }

        #endregion Constructor

        #region Properties


        public abstract string DisplayName { get; }
        public bool IsCurrentPage
        {
            get { return _isCurrentPage; }
            set
            {
                if (value == _isCurrentPage)
                    return;

                _isCurrentPage = value;
                OnPropertyChanged("IsCurrentPage");
            }
        }

        #endregion Properties

        #region Methods
        /// <summary>
        /// Returns true if the user has filled in this page properly
        /// and the wizard should allow the user to progress to the 
        /// next page in the workflow.
        /// </summary>
        internal abstract bool IsValid();

        #endregion Methods

        #region INotifyPropertyChanged members
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
