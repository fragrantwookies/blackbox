﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;
using WPF_UI;

namespace WPF_UI.DeviceWizard
{
    /// <summary>
    /// Interaction logic for DevicePropertiesView.xaml
    /// </summary>
    public partial class DevicePropertiesView : UserControl
    {
        public static string LastPropertyName { get; set; }
        public DevicePropertiesView()
        {
            InitializeComponent();
        }

        private void ExpandMouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender is FrameworkElement && ((FrameworkElement)sender).DataContext is IExpandable)
                {
                    IExpandable expandableControl = (IExpandable)((FrameworkElement)sender).DataContext;

                    if (e.LeftButton == MouseButtonState.Pressed)
                        expandableControl.Expand = !expandableControl.Expand;
                    else if (e.RightButton == MouseButtonState.Pressed)
                    {
                        if (expandableControl.Expand)
                            expandableControl.CollapseAll();
                        else
                            expandableControl.ExpandAll();
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void TextBox_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            LastPropertyName = (sender as TextBox).Tag as string;
        }
    }

    public class BindingProxy : Freezable
    {
        protected override Freezable CreateInstanceCore()
        {
            return new BindingProxy();
        }
        public object Data
        {
            get { return (object)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        public static readonly DependencyProperty DataProperty =
               DependencyProperty.Register("Data", typeof(object), typeof(BindingProxy), new UIPropertyMetadata(null));
    }
}
