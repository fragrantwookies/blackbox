﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using eNervePluginInterface;
using eNerve.DataTypes;

namespace WPF_UI.DeviceWizard
{
    /// <summary>
    /// Interaction logic for DeviceWizardDialog.xaml
    /// </summary>
    public partial class DeviceWizardDialog : Window
    {
        readonly DeviceWizardModel _deviceWizardModel;
        public DeviceWizardDialog(ObservableCollection<INervePlugin> pluginTypes, INervePlugin selectedType)
        {
            InitializeComponent();

            _deviceWizardModel = new DeviceWizardModel(pluginTypes, selectedType);
            _deviceWizardModel.RequestClose += DeviceWizardModel_RequestClose;
            DataContext = _deviceWizardModel;

        }

        public ObservableCollection<Base> NewDevices { get { return DeviceWizardModel.NewDevices; } }

        private void DeviceWizardModel_RequestClose(object sender, EventArgs e)
        {
            DialogResult = NewDevices != null;
        }
    }
}
