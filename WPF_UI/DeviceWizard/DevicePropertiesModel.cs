﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;

using Microsoft.Win32;

using eNerve.DataTypes;
using eNervePluginInterface;


namespace WPF_UI.DeviceWizard
{
    public class DevicePropertiesModel : DeviceWizardPageBase
    {
        #region Fields

        RelayCommand _importAllCommand;
        RelayCommand<object> _importThisCommand;
        bool _isValid = false;
        ObservableCollection<INervePlugin> _pluginTypes;

        #endregion Fields

        #region Constructor
        public DevicePropertiesModel(ObservableCollection<INervePlugin> pluginTypes)
            : base()
        {
            _pluginTypes = pluginTypes;
        }

        #endregion Constructor

        #region Properties

        public ObservableCollection<Base> Devices { get; set; }

        /// <summary>
        /// It's absolutely unacceptable to perform this complex action, in this getter - but I can't currently think of a better way.
        /// </summary>
        public override string DisplayName
        {
            get
            {
                if (IsCurrentPage)
                {
                    PopulateDevices();
                }
                return "Proprties for each device";
            }
        }


        public ICommand ImportThisCommand
        {
            get
            {
                if (_importThisCommand == null)
                    _importThisCommand = new RelayCommand<object>(ImportThis);
                return _importThisCommand;
            }
        }

        public ICommand ImportAllCommand
        {
            get
            {
                if (_importAllCommand == null)
                    _importAllCommand = new RelayCommand(() => ImportAll());
                return _importAllCommand;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Import values for all properties, for all devices in the list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImportAll()
        {
            ImportProperties(true);
        }


        /// <summary>
        /// Import values for this property, for all devices in the list. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImportThis(object target)
        {
            ImportProperties(false);
        }

        private void ImportProperties(bool allProperties)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".csv";
            ofd.Filter = "CSV files | *.csv";
            ofd.Multiselect = false;
            if (ofd.ShowDialog() == true)
            {
                if (!string.IsNullOrEmpty(ofd.FileName))
                {
                    string[] lines = File.ReadAllLines(ofd.FileName);

                    //TODO check if there is only one line with lots of values and try to read it.
                    if (lines.Length == 1)
                    {
                        lines = lines[0].Split(new char[] { ',' });
                    }

                    int counter = lines.Length;
                    if (Devices.Count < counter) // Counter must be the lesser of imported properties, and devices.
                        counter = Devices.Count;

                    for (int i = 0; i < counter; i++)
                    {
                        UIDevice currentDevice = Devices[i] as UIDevice;
                        if ((string.IsNullOrEmpty(lines[i])) || currentDevice == null)
                            continue;
                        if (allProperties)
                        {// TODO split strings to populate all properties.
                            var values = lines[i].Split(new char[] { ',' });
                            if (values.Length > currentDevice.Properties.Count - 2) // IsPlugin and PluginType can never be populated from file.
                            {// more values than properties - assume first value is name.
                                int start = 1;
                                currentDevice.Name = values[0];
                                if (values.Length > currentDevice.Properties.Count - 1)
                                {// Several more values than properties - assume second value is address.
                                    currentDevice.DeviceAddress = values[1];
                                    start++;
                                }
                                int end = values.Length > currentDevice.Properties.Count ? currentDevice.Properties.Count : values.Length;
                                for (int j = start; j < end; j++)
                                {
                                    UICustomProperty property = new UICustomProperty(currentDevice.Properties[j].Name, values[j]);
                                    if (property != null)
                                    {
                                        var index = currentDevice.Properties.IndexOf(property);
                                        if (index > 0)
                                        {
                                            currentDevice.Properties[index].Value = values[j];
                                        }
                                    }
                                }
                            }
                            else
                            {// Same number or less values than properties - populate sequentially and safely.
                                int end = values.Length > currentDevice.Properties.Count ? currentDevice.Properties.Count : values.Length;
                                for (int j = 0; j < end; j++)
                                {
                                    UICustomProperty property = currentDevice.Properties.FirstOrDefault(a => a.DisplayName == DevicePropertiesView.LastPropertyName);
                                    if (property != null)
                                    {
                                        var index = currentDevice.Properties.IndexOf(property);
                                        if (index > 0)
                                        {
                                            currentDevice.Properties[index].Value = values[j];
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {// Import only the selected property.
                            if (!string.IsNullOrEmpty(DevicePropertiesView.LastPropertyName))
                            {
                                switch (DevicePropertiesView.LastPropertyName.ToUpperInvariant())
                                {
                                    case "DEVICE NAME":
                                        currentDevice.Name = lines[i];
                                        break;
                                    case "DEVICE ADDRESS":
                                        currentDevice.DeviceAddress = lines[i];
                                        break;
                                    default:// Custom property
                                        {
                                            UICustomProperty property = currentDevice.Properties.FirstOrDefault(a => a.DisplayName == DevicePropertiesView.LastPropertyName);
                                            var index = currentDevice.Properties.IndexOf(property);
                                            if (index > 0)
                                            {
                                                currentDevice.Properties[index].Value = lines[i];
                                            }
                                            else
                                                currentDevice.Properties.Add(property);// This should never happen as the name is taken from the existing properties.
                                        }
                                        break;
                                }
                            }
                        }
                        // No issues, assume success
                        DeviceWizardModel.NewDevices = Devices;
                        _isValid = true;
                    }
                }
            }
        }

        /// <summary>
        /// Always false unless all properties have been populated.
        /// </summary>
        /// <returns></returns>
        internal override bool IsValid()
        {
            return _isValid;
        }

        private void PopulateDevices()
        {
            if (Devices == null)
                Devices = new ObservableCollection<Base>();
            else if (Devices.Count > 0)
            {
                var temp = Devices[0] as UIDevice;
                if (temp != null && temp.PluginType != DeviceTypeModel.SelectedType)
                {
                    Devices.Clear();
                    //Devices = new ObservableCollection<Base>(); // Redundant.
                }
            }

            if (Devices.Count < DeviceTypeModel.NumberOfDevices)
            {
                for (int i = Devices.Count; i < DeviceTypeModel.NumberOfDevices; i++)
                {
                    UIDevice newDevice = new UIDevice() { IsEnabled = true, IsPlugin = true, PluginType = DeviceTypeModel.SelectedType,
                        PluginTypes = _pluginTypes };
                    DeviceSetupControl.GetPluginProperties(DeviceTypeModel.SelectedType, newDevice.Properties);
                    Devices.Add(newDevice);
                }
            }
        }

        #endregion Methods
    }

}
