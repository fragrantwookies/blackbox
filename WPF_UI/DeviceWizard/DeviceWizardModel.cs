﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Input;

using eNervePluginInterface;
using eNerve.DataTypes;
using Microsoft.Win32;

namespace WPF_UI.DeviceWizard
{
    public class DeviceWizardModel : INotifyPropertyChanged
    {
        #region Fields

        RelayCommand _cancelCommand;
        DeviceWizardPageBase _currentPage;

        RelayCommand<object> _importThisCommand;
        RelayCommand _importAllCommand;

        RelayCommand _moveNextCommand;
        RelayCommand _movePreviousCommand;
        static ObservableCollection<Base> _newDevices;
        ReadOnlyCollection<DeviceWizardPageBase> _pages;
        ObservableCollection<INervePlugin> _pluginTypes;
        INervePlugin _selectedType;

        #endregion Fields

        #region Constructor

        public DeviceWizardModel(ObservableCollection<INervePlugin> pluginTypes, INervePlugin selectedType)
        {
            _newDevices = new ObservableCollection<Base>();
            _pluginTypes = pluginTypes;
            _selectedType = selectedType;
            CurrentPage = Pages[0];
        }

        #endregion Constructor

        public ICommand CancelCommand
        {
            get
            {
                if (_cancelCommand == null)
                    _cancelCommand = new RelayCommand(() => CancelSetup());

                return _cancelCommand;
            }
        }

        void CancelSetup()
        {
            if (_newDevices != null)
            {
                _newDevices.Clear();
                _newDevices = null;
            }
            OnRequestClose();
        }

        public ICommand MovePreviousCommand
        {
            get
            {
                if (_movePreviousCommand == null)
                    _movePreviousCommand = new RelayCommand(
                        () => MoveToPreviousPage(),
                        () => CanMoveToPreviousPage);

                return _movePreviousCommand;
            }
        }

        bool CanMoveToPreviousPage
        {
            get { return 0 < CurrentPageIndex; }
        }

        public ICommand ImportThisCommand
        {
            get
            {
                if (_importThisCommand == null)
                    _importThisCommand = new RelayCommand<object>(ImportThis);
                return _importThisCommand;
            }
        }

        public ICommand ImportAllCommand
        {
            get
            {
                if (_importAllCommand == null)
                    _importAllCommand = new RelayCommand(() => ImportAll());
                return _importAllCommand;
            }
        }


        void MoveToPreviousPage()
        {
            if (CanMoveToPreviousPage)
                CurrentPage = Pages[CurrentPageIndex - 1];
        }
        public ICommand MoveNextCommand
        {
            get
            {
                if (_moveNextCommand == null)
                    _moveNextCommand = new RelayCommand(
                        () => MoveToNextPage(),
                        () => CanMoveToNextPage);

                return _moveNextCommand;
            }
        }

        bool CanMoveToNextPage
        {
            get { return CurrentPage != null && CurrentPage.IsValid(); }
        }

        void MoveToNextPage()
        {
            if (CanMoveToNextPage)
            {
                if (CurrentPageIndex < Pages.Count - 1)
                    CurrentPage = Pages[CurrentPageIndex + 1];
                else
                    OnRequestClose();
            }
        }

        /// <summary>
        /// Returns the page ViewModel that the user is currently viewing.
        /// </summary>
        public DeviceWizardPageBase CurrentPage
        {
            get { return _currentPage; }
            private set
            {
                if (value == _currentPage)
                    return;

                if (_currentPage != null)
                    _currentPage.IsCurrentPage = false;

                _currentPage = value;

                if (_currentPage != null)
                    _currentPage.IsCurrentPage = true;                

                OnPropertyChanged("CurrentPage");
                OnPropertyChanged("IsOnLastPage");
            }
        }
        public bool IsOnLastPage
        {
            get { return CurrentPageIndex == Pages.Count - 1; }
        }

        public static ObservableCollection<Base> NewDevices
        {
            get { return _newDevices; }
            set { _newDevices = value; }
        }

        public ReadOnlyCollection<DeviceWizardPageBase> Pages
        {
            get
            {
                if (_pages == null)
                    CreatePages();
                return _pages;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        public event EventHandler RequestClose;

        void OnRequestClose()
        {
            if (RequestClose != null)
                RequestClose(this, EventArgs.Empty);
        }
        void CreatePages()
        {
            /*var typeSizeVM = new CoffeeTypeSizePageViewModel(this.CupOfCoffee);
            var extrasVM = new CoffeeExtrasPageViewModel(this.CupOfCoffee);*/
            
            var pages = new List<DeviceWizardPageBase>();

            pages.Add(new DeviceTypeModel(_pluginTypes, _selectedType));
            pages.Add(new DevicePropertiesModel(_pluginTypes));
            //_pages.Add(extrasVM);
            /*_pages.Add(new CoffeeSummaryPageViewModel(
                 typeSizeVM.AvailableBeanTypes,
                 typeSizeVM.AvailableDrinkSizes,
                 extrasVM.AvailableFlavorings,
                 extrasVM.AvailableTemperatures));*/

            _pages = new ReadOnlyCollection<DeviceWizardPageBase>(pages);
        }

        /// <summary>
        /// Import values for all properties, for all devices in the list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImportAll()
        {
            ImportProperties(true);
        }


        /// <summary>
        /// Import values for this property, for all devices in the list. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ImportThis(object target)
        {
            ImportProperties(false);
        }

        private void ImportProperties(bool allProperties)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".csv";
            ofd.Filter = "CSV files | (*.csv)";
            ofd.Multiselect = false;
            if (ofd.ShowDialog() == true)
            {
                if (!string.IsNullOrEmpty(ofd.FileName))
                {
                    string[] lines = File.ReadAllLines(ofd.FileName);
                    int counter = lines.Length;
                    if (_newDevices.Count < counter) // Counter must be the lesser of imported properties, and devices.
                        counter = _newDevices.Count;

                    for (int i = 0; i < counter; i++)
                    {
                        if (allProperties)
                        {// TODO split strings to populate all properties.
                            
                        }
                        else
                        {// Import only the selected property.

                        }
                    }
                }
            }
        }

        int CurrentPageIndex
        {
            get
            {

                if (CurrentPage == null)
                {
                    Debug.Fail("Why is the current page null?");
                    return -1;
                }

                return Pages.IndexOf(CurrentPage);
            }
        }
    }
}
