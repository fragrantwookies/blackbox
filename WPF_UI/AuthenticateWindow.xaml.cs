﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using eThele.Essentials;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for AuthenticateWindow.xaml
    /// </summary>
    public partial class AuthenticateWindow : Window
    {
        public bool serverDetailsChanged = false;
        public string ServerIpAddress
        {
            get
            {
                return txtIpAddress.Text;
            }
            set
            {
                txtIpAddress.Text = value;
            }
        }
        public int ServerPortNo
        {
            get
            {                
                return int.Parse(txtPortNo.Text);
            }
            set
            {
                txtPortNo.Text = value.ToString();
            }
        }
        public AuthenticateWindow()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            LogIn();
        }

        private void LogIn()
        {
            if (!string.IsNullOrWhiteSpace(txtUsername.Text)) // Set user name rules here
            {
                if ((TestConnection(txtIpAddress.Text, int.Parse(txtPortNo.Text)) == false))
                {
                    Title = "Server not found";
                }
                else
                {
                    btnLogin.IsEnabled = false;
                    DialogResult = true;
                    Close();
                }
            }
            else
            {
                Title = "Please enter a user name.";
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        public string Username
        {
            get
            {
                return txtUsername.Text;
            }
        }

        public string Password
        {
            get
            {
                Rfc2898DeriveBytes pbkdf = new Rfc2898DeriveBytes(txtPassword.Password, Encoding.Unicode.GetBytes("@-thel@")) { IterationCount = 24 };
                return Encoding.Unicode.GetString(pbkdf.GetBytes(42));
            }
        }

        private void btnAdvanced_Click(object sender, RoutedEventArgs e)
        {
            Row5.Height = (GridLength)(new GridLengthConverter().ConvertFromString("*"));
            btnAdvanced.IsEnabled = false;
            btnAdvanced.Visibility = Visibility.Hidden;
            txtIpAddress.Visibility = Visibility.Visible;
            txtPortNo.Visibility = Visibility.Visible;
            lblIpAddress.Visibility = Visibility.Visible;
            lblPortNo.Visibility = Visibility.Visible;
        }

        private void txtPortNo_TextChanged(object sender, TextChangedEventArgs e)
        {
            serverDetailsChanged = true;
            int value = 0;
            string cleanString = "";
            if (int.TryParse(txtPortNo.Text, out value) == false)
            {
                foreach (char character in txtPortNo.Text)
                {
                    if (char.IsDigit(character))
                        cleanString += character;
                }
                txtPortNo.Text = cleanString;
            }
        }

        private bool TestConnection(string ipAddress, int portNo)
        {
            bool connected = false;
            Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                IAsyncResult connectResult = s.BeginConnect(ipAddress, portNo, null, null);
                bool success = connectResult.AsyncWaitHandle.WaitOne(2000, true);

                if (success)
                    connected = true;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
            finally
            {
                s.Close();
            }
            return connected;
        }

        private void txtIpAddress_TextChanged(object sender, TextChangedEventArgs e)
        {
            serverDetailsChanged = true;
        }

        private void btnLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key.Equals(Key.Enter)) || (e.Key.Equals(Key.Space)))
                LogIn();
        }
    }
}
