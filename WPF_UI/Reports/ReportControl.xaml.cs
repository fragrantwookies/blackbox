﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

using BlackBoxEngine;
using eNerve.DataTypes;
using eThele.Essentials;
using Microsoft.Reporting.WinForms;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for BrowserControl.xaml
    /// Desktop Applications
    /// For spatial operations on desktop applications, add the following line of code to run before they are performed: 
    /// SqlServerTypes.Utilities.LoadNativeAssemblies(AppDomain.CurrentDomain.BaseDirectory);
    /// </summary>
    public partial class ReportControl : UserControl
    {
        public ParameterItems Parameters;
        public ConnectionDetails DatabaseDetails;
        private bool isReportViewerLoaded;

        public delegate void SendMessageEventHandler(SendMessageEventArgs e);
        public SendMessageEventHandler SendMessageEvent;
        private StatusMessage tempMessage;
        public StatusMessage ReportMessage
        {
            get
            {
                if (tempMessage == null)
                    tempMessage = new StatusMessage() { Animation = true, Message = "Please wait while loading reports" };
                return tempMessage;
            }
        }

        public ReportControl()
        {
            InitializeComponent();
            reportViewer.Load += ReportViewer_Load;
        }

        public string GetFriendlyName(string name)
        {
            if (string.IsNullOrEmpty(name) || !name.Contains("\\"))
                return name;
            return name.Substring(name.LastIndexOf('\\') + 1);
        }

        public void ShowReports(List<ReportFile> reportFiles)
        {
            if (CheckAccess())
            {
                ctlReports.Items.Clear();
                if (reportFiles != null && reportFiles.Count > 0)
                {
                    foreach (ReportFile reportFile in reportFiles)
                    {
                        reportViewer.Reset();
                        reportViewer.ProcessingMode = ProcessingMode.Local;
                        if (File.Exists(reportFile.Name))
                            File.Delete(reportFile.Name);
                        LocalReport report = reportViewer.LocalReport;
                        using (Stream file = File.Open(reportFile.Name, FileMode.OpenOrCreate))
                        {
                            MemoryStream stream = new MemoryStream(reportFile.Data);
                            file.Write(reportFile.Data, 0, (int)stream.Length);
                            report.ReportPath = reportFile.Name;
                            report.LoadReportDefinition(stream);
                        }
                        if (string.IsNullOrEmpty(report.DisplayName))
                            report.DisplayName = reportFile.Name.Substring(0, reportFile.Name.IndexOf('.'));
                        ctlReports.Items.Add(report);
                    }
                }
                btnReload.IsEnabled = true;
                btnGenerate.IsEnabled = true;
                if (tempMessage != null)
                    OnSendMessage(MessageType.StatusMessageRemove, tempMessage);
                OnSendMessage(MessageType.SendDBSettings, null);
            }
            else
            {
                Dispatcher.Invoke(() => ShowReports(reportFiles));
            }
        }

        private void OnSendMessage(MessageType type, object data)
        {
            if (SendMessageEvent != null)
            {
                SendMessageEvent(new SendMessageEventArgs(type, data));
            }
        }

        private void ReportViewer_Load(object sender, EventArgs e)
        {
            if (!isReportViewerLoaded)
            {
                ReportDataSource reportDS = new ReportDataSource();
                DataSet dataSet = new DataSet();
                dataSet.BeginInit();
                dataSet.DataSetName = "RIODataSet";
                reportDS.Value = dataSet.Tables;
            }
        }

        private void Generate_Click(object sender, RoutedEventArgs e)
        {
            if (DatabaseDetails == null)
            {
                OnSendMessage(MessageType.StatusMessage, new StatusMessage() { Animation = true,
                    Message = "No connection details found!", AutoRemove = TimeSpan.FromSeconds(10) });
            }
            else
            {
                try
                {
                    btnGenerate.IsEnabled = false;
                    btnReload.IsEnabled = false;
                    clmConfig.Width = new GridLength(0);
                    clmViewer.Width = new GridLength(1, GridUnitType.Star);
                    //betterBrowser.Address = "http://" + address.Text;

                    reportViewer.ProcessingMode = ProcessingMode.Local;
                    
                    string reportPath = (ctlReports.SelectedItem as LocalReport).ReportPath;

                    reportViewer.LocalReport.ReportPath = reportPath;
                    var reportParameters = new List<ReportParameter>();
                    foreach (var parameter in Parameters.Parameters)
                    {
                        reportParameters.Add(new ReportParameter(parameter.Name, parameter.Value));
                    }
                    reportViewer.LocalReport.SetParameters(reportParameters);

                    using (SqlConnection connection = new SqlConnection(DatabaseDetails.ConnectionString()))
                    {
                        connection.Open();
                        XMLDoc doc = new XMLDoc();
                        doc.Load(reportPath);
                        //DataSet dataSet = new DataSet();
                        foreach (XmlNode datasetDefinition in doc["Datasets"].Children)
                        {
                            using (SqlDataAdapter dataAdapter = new SqlDataAdapter())
                            {
                                using (SqlCommand command = connection.CreateCommand())
                                {
                                    command.CommandText = datasetDefinition["Query"]["CommandText"].Value;
                                    dataAdapter.SelectCommand = command;
                                    foreach (var parameter in Parameters.Parameters)
                                    {
                                        dataAdapter.SelectCommand.Parameters.Add(new SqlParameter(parameter.Name, parameter.Value));
                                    }

                                    
                                    //dataSet.DataSetName = datasetDefinition.Attribute("Name").Value;

                                    //dataAdapter.Fill(dataSet);

                                    DataTable table = new DataTable();
                                    table.Load(dataAdapter.SelectCommand.ExecuteReader());
                                    table.TableName = datasetDefinition.Attribute("Name").Value;

                                    //dataSet.Tables.Add(table);

                                    reportViewer.LocalReport.DataSources.Add(new ReportDataSource(table.TableName, table));
                                }
                            }
                        }
                        connection.Close();
                    }

                                     
                    /*reportViewer.ServerReport.ReportServerUrl = new Uri("http://" + address.Text);
                    reportViewer.ServerReport.ReportPath = "/Alarm Details";
                    ArrayList reportParam = new ArrayList();
                    reportParam = ReportDefaultParam();

                    ReportParameter[] parameters = new ReportParameter[reportParam.Count];
                    for (int i = 0; i < reportParam.Count; i++)
                    {
                        parameters[i] = (ReportParameter)reportParam[i];
                    }
                    reportViewer.ServerReport.SetParameters(parameters);*/
                    reportViewer.LocalReport.Refresh();
                    reportViewer.RefreshReport();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }/*

        private ReportParameter CreateReportParameter(string paramName, string paramValue)
        {
            ReportParameter aParam = new ReportParameter(paramName, paramValue);
            return aParam;
        }

        private ArrayList ReportDefaultParam()
        {
            ArrayList arrLstDefaultParam = new ArrayList();
            arrLstDefaultParam.Add(CreateReportParameter("AlarmID", "01A17D18-07B7-4AB0-A5F4-640319D53AD9"));
            return arrLstDefaultParam;
        }*/

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            btnGenerate.IsEnabled = true;
            btnReload.IsEnabled = true;
            reportViewer.Clear();
            clmViewer.Width = new GridLength(0);
            clmConfig.Width = new GridLength(1, GridUnitType.Star);
        }

        private void Reload_Click(object sender, RoutedEventArgs e)
        {
            btnGenerate.IsEnabled = false;
            btnReload.IsEnabled = false;
            ctlReports.Items.Clear();
            OnSendMessage(MessageType.StatusMessage, ReportMessage);
            MainWindow.Client.SendMessage(MessageType.ReportList);
        }

        private void ctlReports_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Parameters == null)
                Parameters = new ParameterItems();
            else
                Parameters.Parameters.Clear();
            if (e.AddedItems != null && e.AddedItems.Count > 0)
            {
                reportViewer.Reset();
                reportViewer.ProcessingMode = ProcessingMode.Local;
                LocalReport report = reportViewer.LocalReport;
                report = e.AddedItems[0] as LocalReport;
                try
                {
                    var parameters = report.GetParameters();
                    if (parameters != null && parameters.Count > 0)
                    {
                        //ctlParemeters.Items.Clear();
                        foreach (ReportParameterInfo parameter in parameters)
                        {
                            if (parameter.DataType == ParameterDataType.DateTime)
                                Parameters.Add(new DateTimeParameterItem(parameter.Name));
                            else
                                Parameters.Add(new ParameterItem(parameter.Name, parameter.DataType));
                            //ctlParemeters.Items.Add(parameter);
                        }
                        ctlParameters.DataContext = Parameters;
                    }
                }
                catch (Exception ex)
                {
                    Exceptions.ExceptionsManager.Add(System.Reflection.MethodBase.GetCurrentMethod().Name, "Error while changing selected report.", ex);
                }
            }
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            var textBox = sender as DatePicker;
            if (textBox != null)
            {
                var grid = textBox.Parent as Grid;
                if (grid != null)
                {
                    var item = grid.DataContext as ParameterItem;
                    if (item != null)
                    {
                        item.Value = textBox.SelectedDate.Value.ToString("yyyy-MM-dd");
                    }
                }
            }
        }
    }
}
