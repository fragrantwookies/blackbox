﻿using System.Collections.ObjectModel;

namespace WPF_UI
{
    public class ParameterItems
    {
        public ParameterItems()
        {
            Parameters = new ObservableCollection<ParameterItem>();
        }
        public ObservableCollection<ParameterItem> Parameters { get; set;  }
        public void Add(ParameterItem item)
        {
            if (Parameters == null)
                Parameters = new ObservableCollection<ParameterItem>();
            Parameters.Add(item);
        }
        public void Add(string name, string value = "")
        {
            Add(new ParameterItem(name, Microsoft.Reporting.WinForms.ParameterDataType.String, value));
        }
    }
}
