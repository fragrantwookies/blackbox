﻿using System.Collections.ObjectModel;
using Microsoft.Reporting.WinForms;

namespace WPF_UI
{
    public class ParameterItem
    {
        public ParameterItem(string name, ParameterDataType type = ParameterDataType.String, string value = "")
        {
            Name = name;
            Value = value;
            DisplayName = eNervePluginInterface.PluginManager.PrettifyString(name);
            Type = type;
        }
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public ParameterDataType Type { get; set; }
        public string Value { get; set; }
    }
    public class DateTimeParameterItem : ParameterItem
    {
        public DateTimeParameterItem(string name, ParameterDataType type = ParameterDataType.DateTime, string value = "") : base(name, type, value)
        {
            Value = System.DateTime.Now.ToShortDateString();
        }
    }
}
