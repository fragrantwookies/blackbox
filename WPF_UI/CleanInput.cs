﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace WPF_UI
{
    public static class CleanInput
    {
        const string Alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        const string Numeric = "0123456789";
        static Key[] AlphaKeys = new Key[] { Key.A, Key.B, Key.C, Key.D, Key.E, Key.F, Key.G, Key.H, Key.I, Key.J, Key.K, Key.L, Key.M, Key.N, Key.O, Key.P, Key.Q, Key.R, Key.S, Key.T, Key.U, Key.V, Key.W, Key.X, Key.Y, Key.Z};
        static Key[] NumberKeys = new Key[] { Key.NumPad0, Key.NumPad1, Key.NumPad2, Key.NumPad3, Key.NumPad4, Key.NumPad5, Key.NumPad6, Key.NumPad7, Key.NumPad8, Key.NumPad9 };
        /// <summary>
        /// Forces first character to be alphabet letter. Any subsequent characters can be numeric as well.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string CleanString(string input)
        {
            int length = input.Length;
            StringBuilder output = new StringBuilder();
            if (string.IsNullOrEmpty(input))
                return input;
            int i = 0;
            while (i < length)
            {
                if (!ValidFirstCharacter(input[i]))
                    i++;
                else
                {
                    output.Append(input[i]);
                    break;
                }
            }
            for (int j = i; j < length; j++)
            {
                if (ValidOtherCharacter(input[j]))
                    output.Append(input[j]);
            }

            return output.ToString();
        }
        public static bool ValidFirstCharacter(char character)
        {
            if (Alpha.Contains(character.ToString())) return true;
            return false;
        }
        public static bool ValidFirstKey(Key inputKey)
        {
            if (AlphaKeys.Contains(inputKey)) return true;
            return false;
        }
        public static bool ValidOtherCharacter(char character)
        {
            if (Alpha.Contains(character.ToString()) || Numeric.Contains(character.ToString())) return true;
            return false;
        }
        public static bool ValidOtherKey(Key inputKey)
        {
            if ((ValidFirstKey(inputKey)) || (NumberKeys.Contains(inputKey))) return true;
            return false;
        }
    }
}
