﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows;

namespace WPF_UI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
           /* Mutex mutex;

        public App()
        {
            bool isNew;
            mutex = new Mutex(true, "e-Thele.RioSoft.Client", out isNew);
            if (!isNew)
                Environment.Exit(0);
        }*/
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainWindow = new MainWindow();


            /*MainWindow.Topmost = true;
            MainWindow.Show();
            MainWindow.Activate();
            MainWindow.Topmost = false;*/

            ((MainWindow)MainWindow).Authenticate();
        }
    }
}
