﻿using eNervePluginInterface;
using eNervePluginInterface.CustomAttributes;
using System;

namespace SMSPlugin
{
    [Serializable]
    public class SMSPluginAction : PluginAction
    {
        public SMSPluginAction() : base() { }

        public SMSPluginAction(string _name, bool _value, string _pluginName, int _actionOrder = 0) : base(_name, _value, _pluginName, _actionOrder) {}

        [DisplayInPluginActionPropertiesList]
        public string CellPhoneNumber { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string MessageBody { get; set; }

        public System.DateTime TimeStamp { get; set; }
    }
}
