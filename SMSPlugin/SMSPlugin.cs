﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using BlackBoxEngine;
using eNervePluginInterface;
using eThele.Essentials;

namespace SMSPlugin
{
    public class SMSPlugin : INerveActionPlugin, IDisposable, INerveEventPlugin
    {
        private string fileName;
        private CustomProperties properties;

        private int _connectTimeout = 20000;
        private int _portNo;
        private System.Timers.Timer connectTimer;
        private Client gsmClient;
        private SMSPluginAction lastSms;
        private List<SMSPluginAction> smsQueue;
        private System.Timers.Timer sendTimer;
        private bool smsPending = false;
        

        #region Cleanup
        ~SMSPlugin()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            SendTcpMessage(GsmTcpMessages.MessageBuilder(MessageCharacters.Cancel));
            if (gsmClient != null)
            {
                gsmClient.ConnectionStatusChanged -= Client_ConnectionStatusChanged;
                gsmClient.OnBinaryMessageReceived -= Client_OnBinaryMessageReceived;
                if (gsmClient.ConnectionStatus != Client.ConnectState.Disconnected)
                    gsmClient.Disconnect();
                gsmClient.Dispose();
            }
            if (connectTimer != null)
            {
                connectTimer.Elapsed -= ConnectionFailed;
                connectTimer.Dispose();
            }
            if (sendTimer != null)
            {
                sendTimer.Elapsed -= SendTimer_Elapsed;
                sendTimer.Dispose();
            }
        }
        #endregion

        #region Properties

        public string Author
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company; }
        }

        //public string[] CustomPropertyNames
        //{
        //    get
        //    {
        //        return new string[] { "IP", "Port", "Pin", "ConnectTimeout", "SendReceiveTimeout", "PollFrequency", "DefaultCellNumber" };
        //    }
        //}

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                result.Add(new CustomPropertyDefinition("IP", "IP Address", "IP Address", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Port", "Port", "Port", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Pin", "Pin", "Pin", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("ConnectTimeout", "ConnectTimeout", "ConnectTimeout", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("SendReceiveTimeout", "SendReceiveTimeout", "SendReceiveTimeout", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("PollFrequency", "PollFrequency", "PollFrequency", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("DefaultCellNumber", "DefaultCellNumber", "DefaultCellNumber", typeof(CustomProperty)));
                return result;
            }
        }

        public string Description
        {
            get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description; }
        }

        public string[] EventNames { get { return new string[] { "SMSFailed", "DeviceFailed" }; } }
        public bool Equals(INervePlugin other)
        {
            return (Name == other.Name && FileName == other.FileName);
        }

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }

        public string Version
        {
            get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        #endregion Properties

        public event EventHandler<PluginErrorEventArgs> Error;
        public event EventHandler<PluginTriggerEventArgs> Trigger;                                                                 
        public event EventHandler DeviceOnline;
        public event EventHandler DeviceOffline;

        public bool Online { get; set; }

        public bool Enabled
        { get; set; }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }

        public void Start()
        {
            if (gsmClient == null)
                gsmClient = new Client();
            try
            {
                _portNo = 26674;
                if (!int.TryParse(Properties["Port"], out _portNo))
                {
                    OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                        MethodBase.GetCurrentMethod().Name), "Unable to parse port from config file.", new FormatException()));

                }
                gsmClient.ConnectionStatusChanged -= Client_ConnectionStatusChanged;
                gsmClient.ConnectionStatusChanged += Client_ConnectionStatusChanged;
                gsmClient.OnBinaryMessageReceived -= Client_OnBinaryMessageReceived;
                gsmClient.OnBinaryMessageReceived += Client_OnBinaryMessageReceived;
                gsmClient.Connect(Properties["IP"], _portNo);
                int number;
                string err;
                if (TimeSpanParser.TryParse(Properties["SendReceiveTimeout"], out number, out err))
                {
                    if (sendTimer == null)
                        sendTimer = new System.Timers.Timer(number);
                }
                else
                {
                    OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                           MethodBase.GetCurrentMethod().Name), "Unable to parse SendReceiveTimeout from config file.", new FormatException()));
                    if (sendTimer == null)
                        sendTimer = new System.Timers.Timer(_connectTimeout);
                }
                sendTimer.Elapsed += SendTimer_Elapsed;
                if (TimeSpanParser.TryParse(Properties["ConnectTimeout"], out number, out err))
                {
                    _connectTimeout = number;
                }
                if (connectTimer == null)
                    connectTimer = new System.Timers.Timer(_connectTimeout);
                connectTimer.Elapsed -= ConnectionFailed;
                connectTimer.Elapsed += ConnectionFailed;
                connectTimer.Start();
            }
            catch (SocketException)
            {
                OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                           string.Format("Unable to connect to {0}:{1}", Properties["IP"], Properties["Port"]), new OperationCanceledException()));
            }
        }

        private void SendNextSms()
        {
            lock (smsQueue)
            {
                //if (!smsPending)
                if ((smsQueue != null) && (smsQueue.Count > 0))
                {

                    smsPending = true;
                    lastSms = smsQueue[0];
                    if ((lastSms.CellPhoneNumber.Contains(";")))
                    {
                        string nextNumber = lastSms.CellPhoneNumber.Substring(0, lastSms.CellPhoneNumber.IndexOf(";")).TrimEnd(';');
                        if (!string.IsNullOrWhiteSpace(nextNumber))
                            SendTcpMessage(GsmTcpMessages.MessageBuilder(new GsmSms(nextNumber, lastSms.MessageBody, lastSms.TimeStamp)));
                        lastSms.CellPhoneNumber = lastSms.CellPhoneNumber.Substring(lastSms.CellPhoneNumber.IndexOf(";") + 1);
                    }
                    else
                    {
                        SendTcpMessage(GsmTcpMessages.MessageBuilder(new GsmSms(lastSms.CellPhoneNumber, lastSms.MessageBody, lastSms.TimeStamp)));
                        try
                        {
                            smsQueue.RemoveAt(0);
                        }
                        catch (ArgumentOutOfRangeException ex)
                        {
                            OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                              "Error clearing SMS Queue", new OperationCanceledException(ex.Message)));
                        }
                    }
                }
                else
                {// else all sms's scented
                    lastSms = null;
                    smsPending = false;
                }
            }
        }

        private void SendTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if ((lastSms == null)  && ((smsQueue == null) || (smsQueue.Count == 0)))
            {
                smsPending = false;
                sendTimer.Stop();
            }
            else
            {
                if ((smsQueue == null) || (smsQueue.Count < 1))
                {
                    OnTrigger(EventNames[0]);
                }
                else
                {
                    OnTrigger(EventNames[0], smsQueue[0]);
                }
                lastSms = null;
                //smsPending = false;
                SendNextSms();
            }
        }

        private void ConnectionFailed(object sender, ElapsedEventArgs e)
        {
            if (gsmClient.ConnectionStatus == Client.ConnectState.Disconnected)
            {
                OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                        MethodBase.GetCurrentMethod().Name), "Connection timed out.", new TimeoutException()));
                gsmClient.Connect(Properties["IP"], _portNo);
            }
            else if ((gsmClient.ConnectionStatus == Client.ConnectState.Connected) && (connectTimer != null))
                connectTimer.Stop();
        }

        private void Client_OnBinaryMessageReceived(object sender, CommsNode.BinaryMessageEventArgs e)
        {
            if ((e != null) && (e.BinaryData != null))
                ProcessMessage(e.BinaryData);
        }

        private void Client_ConnectionStatusChanged(object sender, Client.ConnectState connectionState)
        {
            if (connectionState == Client.ConnectState.Connected)
            {
                if (connectTimer != null)
                    connectTimer.Stop();
                int connectTimeout = 20000;
                string err;
                if (!TimeSpanParser.TryParse(Properties["ConnectTimeout"], out connectTimeout, out err))
                    OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                        MethodBase.GetCurrentMethod().Name), "Unable to parse connect timeout from config file - using default 20000", new FormatException()));
                _connectTimeout = connectTimeout;

                SendTcpMessage(GsmTcpMessages.MessageBuilder(MessageCharacters.Idle, connectTimeout));
                if (DeviceOnline != null)
                    DeviceOnline(this, new EventArgs());

            }// TODO start a reconnect attempt if it disconnects.

            else if (connectionState == Client.ConnectState.Disconnected)
            {
                if (Online && DeviceOffline != null)
                    DeviceOffline(this, new EventArgs());

                if (connectTimer == null)
                    connectTimer = new System.Timers.Timer(_connectTimeout);
                connectTimer.Start();
            }

            Online = connectionState == Client.ConnectState.Connected;
        }

        public bool PerformAction(string eventName, IEnumerable<IEventTrigger> trigger, IAction action)
        {
            bool success = false;

            if (gsmClient == null || gsmClient.ConnectionStatus != Client.ConnectState.Connected)
                Start();
            var smsAction = action as SMSPluginAction;
            if (smsAction != null)
            {
                if (string.IsNullOrEmpty(smsAction.MessageBody))
                {// Nothing to send
                    OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                           string.Format("Invalid SMSPluginAction: Missing message body: {0}", action.ToString()), new Exception()));
                }
                else
                {
                    if ((smsAction.TimeStamp == null) || (smsAction.TimeStamp.ToUniversalTime() == DateTime.MinValue))
                    {
                        if (trigger != null && trigger.Count() > 0)
                            smsAction.TimeStamp = trigger.FirstOrDefault().TriggerDT;
                        else
                            smsAction.TimeStamp = DateTime.Now;// Ensure correct timestamp is retained.
                    }
                    if (string.IsNullOrEmpty(smsAction.CellPhoneNumber))
                        smsAction.CellPhoneNumber = Properties["DefaultCellNumber"];

                    if (smsQueue == null)
                        smsQueue = new List<SMSPluginAction>();
                    smsQueue.Add(smsAction);
                    if (sendTimer.Enabled == false)
                        sendTimer.Enabled = true;

                    success = true;
                }
            }
            else
            {
                OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                    string.Format("Invalid SMSPluginAction {0}", action), new Exception()));
            }

            return success;
        }

        private void OnError(PluginErrorEventArgs e)
        {
            if ((Error != null) && (e != null))
                Error(this, e);
        }

        /// <summary>
        /// Critical failure - device offline or unreachable.
        /// </summary>
        /// <param name="error"></param>
        private void OnTrigger(string error)
        {
            if (Trigger != null)
            {
                var trigger = new SMSTrigger() { EventName = error, DetectorName = error, TriggerID = Guid.NewGuid(), DeviceName = Name,
                    DetectorType = DetectorType.Unknown, TriggerDT = DateTime.Now, Data = error };

                Trigger(this, new PluginTriggerEventArgs(trigger));
            }
        }

        /// <summary>
        /// Last SMS failed - provide detail back to RIO
        /// </summary>
        /// <param name="error"></param>
        /// <param name="action"></param>
        private void OnTrigger(string error, SMSPluginAction action)
        {
            if (Trigger != null)
            {//TODO replace with SMS - specific detail
                if (action.TimeStamp == null)
                    action.TimeStamp = DateTime.Now;
                var trigger = new SMSTrigger() { EventName = error, DetectorName = error, TriggerID = Guid.NewGuid(), DeviceName = Name,
                    DetectorType = DetectorType.Unknown, TriggerDT = DateTime.Now, Data = action.CellPhoneNumber + action.MessageBody + action.TimeStamp };

                Trigger(this, new PluginTriggerEventArgs(trigger));
            }
        }

        public Type GetActionType(string actionType = "")
        {
            return typeof(SMSPluginAction);
        }

        public IEnumerable<Type> GetActionTypes()
        {
            return new Type[] { typeof(SMSPluginAction) };
        }

        private void ProcessMessage(byte[] message)
        {
            if (message.Length == 1) // ACK, PIN request, Nack, or error.
            {
                switch (message[0])
                {
                    case (byte)MessageCharacters.Enquiry: // GSM service is asking for the PIN
                        int pin;
                        if ((Properties["Pin"].Length < 10) && (int.TryParse(Properties["Pin"], out pin)))
                            SendTcpMessage(GsmTcpMessages.MessageBuilder(Properties["Pin"]));
                        else
                        {
                            OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                              string.Format("Invalid PIN in config: {0}", Properties["Pin"]), new FormatException()));
                        }
                        break;
                    case (byte)MessageCharacters.Nack:
                        //StatusUpdate("Last command failed."); // TODO if SMS then notify.
                        OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                              "Last command failed.", new Exception()));
                        if (lastSms != null)
                        {
                            OnTrigger(EventNames[0], lastSms);
                            lastSms = null;
                            smsPending = false; //don't cause concurrency issues, let sendnext handle the bool.
                            SendNextSms();
                        }
                        break;
                    case (byte)MessageCharacters.Cancel:
                        OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                              "GSM modem inoperational.", new Exception()));
                        OnTrigger(EventNames[1]);
                        SendTcpMessage(GsmTcpMessages.MessageBuilder(MessageCharacters.Ack));
                        //StatusUpdate("Critical failure. Modem not found or not functional.");
                        //Disconnect();
                        break;
                    case (byte)MessageCharacters.Ack:
                        //MessageBox.Show("GSM modem online and ready."); // No need to report as the signal status message will do the same.
                        break;
                    default: // Unknown message or not implemented yet - either be glad or don't know what to do.
                        break;
                }
            }
            else if (message.Length == 11)
            {
                int number;
                if (int.TryParse(Encoding.ASCII.GetString(message, 1, 10), out number))
                {
                    switch ((MessageCharacters)message[0])
                    {
                        case MessageCharacters.Idle:
                            // Signal strength / keepalive received.
                            //MessageBox.Show("Signal is now " + (number - 11) * 5);
                            SendTcpMessage(GsmTcpMessages.MessageBuilder(MessageCharacters.Ack));
                            break;
                        case MessageCharacters.Ack: // SMS sent successfully.
                            smsPending = false;
                            SendNextSms();
                            //MessageBox.Show("SMS sent with reference: " + number);
                            break;
                    }
                }
                else
                {
                    //StatusUpdate("Unable to parse signal strength");
                }
            }
            else if (message.Length > 37)
            {// Not working yet :(
                GsmSms newSms = new GsmSms(message);
            }
        }

        private void SendTcpMessage(byte[] message)
        {
            if ((gsmClient != null) && (gsmClient.ConnectionStatus == Client.ConnectState.Connected))
                gsmClient.SendBinaryMessage(message, true);
        }
    }
}
