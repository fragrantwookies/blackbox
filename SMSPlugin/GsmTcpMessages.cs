﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace SMSPlugin
{
    static class GsmTcpMessages
    {
        /// <summary>
        /// Basic messages - only ack, nack, cancel or idle.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static byte[] MessageBuilder(MessageCharacters type)
        {
            byte[] message = new byte[1];
            message[0] = (byte)type;
            return message;
        }

        /// <summary>
        /// PIN Message. Changed to string to avoid automatic truncation.
        /// </summary>
        /// <param name="pin"></param>
        /// <returns></returns>
        public static byte[] MessageBuilder(string pin)
        {
            byte[] pinBytes = Encoding.ASCII.GetBytes(pin.PadLeft(4, '0'));
            byte[] message = new byte[pinBytes.Length + 2];
            message[0] = (byte)MessageCharacters.Enquiry;
            message[1] = Encoding.ASCII.GetBytes(pinBytes.Length.ToString())[0];
            Array.ConstrainedCopy(pinBytes, 0, message, 2, pinBytes.Length);
            return message;
        }

        /// <summary>
        /// SIM Confirmation message with reference, or idle message with signal strength, or enquiry message with send/receive timeout.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static byte[] MessageBuilder(MessageCharacters type, int reference)
        {// SMS confirmation message. Could be adjusted for network confirmation.
            byte[] refBytes = Encoding.ASCII.GetBytes(reference.ToString().PadLeft(10, '0'));
            byte[] message = new byte[11];
            int counter = 0;
            message[counter++] = (byte)type;
            Array.ConstrainedCopy(refBytes, 0, message, counter, 10);
            return message;

        }

        /// <summary>
        /// SMS message.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="destinationNumber"></param>
        /// <param name="dateString"></param>
        /// <returns></returns>
        private static byte[] MessageBuilder(string content, string destinationNumber, string dateString)// Must be SMS message
        {
            // First sanitize content to avoid escape sequences escaping.
            content = Regex.Replace(content, @"[^A-Za-z 0-9 \.,\?'""!@#£\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]~]*", "");
            if (content.Length > 0)
            {
                byte[] message = new byte[content.Length + 36];//39
                int counter = 0;
                Array.ConstrainedCopy(Encoding.ASCII.GetBytes(message.Length.ToString().PadLeft(3, '0').Substring(0, 3)), 0, message, counter, 3);
                counter += 3;//counter = 3
                message[counter++] = (byte)MessageCharacters.RecordSeperator;//counter = 4
                if ((destinationNumber.StartsWith("+27")) && (destinationNumber.Length == 12)) // Only truncate of you're sure it's prefixed with +27
                    destinationNumber = "0" + destinationNumber.Substring(destinationNumber.IndexOf("7") + 1);
                Array.ConstrainedCopy(Encoding.ASCII.GetBytes(destinationNumber), 0, message, counter, 10);
                counter += 10;//counter = 14
                message[counter++] = (byte)MessageCharacters.RecordSeperator;//counter = 15
                if (dateString.Length != 17)
                    dateString = DateTime.Now.ToString("yy/MM/dd,HH:mm:ss");
                Array.ConstrainedCopy(Encoding.ASCII.GetBytes(dateString), 0, message, counter, 17);
                counter += 17;//counter = 32
                message[counter++] = (byte)MessageCharacters.RecordSeperator;//counter = 33
                Array.ConstrainedCopy(Encoding.ASCII.GetBytes(content.Length.ToString().PadLeft(3, '0').Substring(0, 3)), 0, message, counter, 3);
                counter += 3;//counter = 36
                Array.ConstrainedCopy(Encoding.ASCII.GetBytes(content), 0, message, counter, content.Length);
                return message;
            }
            else return null;
        }

        public static byte[] MessageBuilder(GsmSms sms)
        {// SMS from GSM to RIO - populate TCP message to be able to send through network.
            return MessageBuilder(sms.Body, sms.Number, sms.TimeStamp.ToString("yy/MM/dd,HH:mm:ss"));
        }
    }

    public enum MessageCharacters : byte
    {
        Null,
        StartOfHeading,
        STX,
        ETX,
        EndOfTransmission,
        Enquiry,
        Ack,
        Nack = 21,
        Idle = 22,
        Cancel = 24,
        RecordSeperator = 30
    }
}
