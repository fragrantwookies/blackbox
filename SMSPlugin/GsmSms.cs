﻿using System;
using System.Globalization;
using System.Text;

namespace SMSPlugin
{
    public class GsmSms
    {
        string body;
        string number;
        DateTime timeStamp;
        public string Body { get { return body; } }
        public string Number { get { return number; } }
        public DateTime TimeStamp { get { return timeStamp; } }
        public GsmSms(string data)
        {
            string[] sections = data.Substring(data.IndexOf("CMGR:")).Split(new char[] { ',', '\n' });
            if (sections.Length > 5)
            {
                number = sections[1].Trim(new char[] { '"' });
                string dateString = (sections[3] + "," + sections[4].Substring(0, sections[4].IndexOf('+'))).Trim(new char[] { '"', '\r' });
                if (!DateTime.TryParseExact(dateString, "yy/MM/dd,HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out timeStamp))
                {
                    timeStamp = DateTime.Now;
                }
                body = sections[5];
            }
        }
        /// <param name="message"></param>
        /// <exception cref="FormatException" Thrown if the received message is invalid.</exception>
        public GsmSms(byte[] message)
        {// Extract SMS data from TCP message sent from RIO
            try
            {
                number = Encoding.ASCII.GetString(message, 4, 10);
                if (number.StartsWith("0"))
                    number = "+27" + number.TrimStart(new char[] { '0' });// Must start with +27 to send via modem.
                string dateString = Encoding.ASCII.GetString(message, 15, 17);
                if (!DateTime.TryParseExact(dateString, "yy/MM/dd,HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out timeStamp))
                {
                    timeStamp = DateTime.Now;
                }
                string lengthString = Encoding.ASCII.GetString(message, 33, 3);
                int bodyLength;
                if (int.TryParse(lengthString, out bodyLength))
                {
                    body = Encoding.ASCII.GetString(message, 36, bodyLength);
                }
                else
                {
                    throw new FormatException("Unable to get message body length from byte array.");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public GsmSms(string _number, string _body)
        {
            body = _body;
            if (_number.StartsWith("0"))
                number = "+27" + _number.TrimStart(new char[] { '0' });
            else
                number = _number;
            timeStamp = DateTime.Now;
        }

        public GsmSms(string _number, string _body, DateTime _timeStamp): this(_number, _body)
        {
            if (_timeStamp != null)
                timeStamp = _timeStamp;
            else
                timeStamp = DateTime.Now;
        }

        public string FormatMessageForSending()
        {
            return body + (char)26 + "\r\n";
        }

        public string FormatNumberForSending()
        {
            if (number.StartsWith("0"))// Trim 0 and add country prefix
                number = "+27" + number.TrimStart(new char[] { '0' });
            return "AT+CMGS=\"" + number + "\"\r";
        }

        public override string ToString()
        {
            return "Sender: " + number + ". Time: " + timeStamp.ToString() + ". Body: \n" + body;
        }
    }
}