﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BlackBoxEngine;
using eThele.Essentials;

namespace TestRig2
{
    public partial class Form1 : Form
    {
        Server svr;
        Client clnt;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnStartStop_Click(object sender, EventArgs e)
        {
            if (btnStartStop.Text == "Start")
            {
                if (svr == null)
                {
                    svr = new Server(4060);
                    svr.OnBinaryMessageReceived += svr_OnBinaryMessageReceived;
                }

                if (clnt == null)
                {
                    clnt = new Client();
                    clnt.OnBinaryMessageReceived += clnt_OnBinaryMessageReceived;
                }

                btnStartStop.Text = "Stop";
            }
            else
                btnStartStop.Text = "Start";
        }

        private void btnCRC_Click(object sender, EventArgs e)
        {
            uint crc = CRC.CalcCRC32(Encoding.UTF8.GetBytes(txtCRC.Text));
            byte[] crcArray = BitConverter.GetBytes(crc);

        }

        void svr_OnBinaryMessageReceived(object sender, CommsNode.BinaryMessageEventArgs e)
        {
            throw new NotImplementedException();
        }

        void clnt_OnBinaryMessageReceived(object sender, CommsNode.BinaryMessageEventArgs e)
        {
            throw new NotImplementedException();
        }

        protected static byte[] ConvertHexStringToByteArray(string pHexString)
        {
            if (pHexString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format("The binary key cannot have an odd number of digits: {0}", pHexString));
            }

            byte[] hexAsBytes = new byte[pHexString.Length / 2];
            for (int index = 0; index < hexAsBytes.Length; index++)
            {
                string byteValue = pHexString.Substring(index * 2, 2);
                hexAsBytes[index] = byte.Parse(byteValue, System.Globalization.NumberStyles.HexNumber);
            }

            return hexAsBytes;
        }

        protected static string ConvertByteArrayToHexString(byte[] data)
        {
            string result = "";

            if (data != null)
            {
                for (int i = 0; i < data.Length; i++)
                {
                    result += data[i].ToString("X2");
                }
            }

            return result;
        }
    }
}
