﻿using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SnapshotPlugin
{
    public class SnapshotPlugin : INerveActionPlugin, INerveSnapshotPlugin, IDisposable
    {     
        public event EventHandler<PluginErrorEventArgs> Error;     
        public event EventHandler DeviceOnline;     
        public event EventHandler DeviceOffline;
        public event EventHandler<ImageCapturedEventArgs> ImageCaptured;

        #region Dispose
        ~SnapshotPlugin()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {

        }
        #endregion
        
        public void Start()
        {
            Online = true;

            if (DeviceOnline != null)
                DeviceOnline(this, new EventArgs());
        }

        public bool Online { get; set; }

        public bool Enabled { get; set; }

        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public string Description
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
            }
        }

        public string Author
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company;
            }
        }

        public string Version
        {
            get
            {
                return Assembly.GetCallingAssembly().GetName().Version.ToString();
            }
        }

        private string fileName;

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        private CustomProperties properties;
        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
        }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                result.Add(new CustomPropertyDefinition("DefaultFolder", "DefaultFolder", "DefaultFolder", typeof(CustomProperty)));
                return result;
            }
        }

        public bool Equals(INervePlugin other)
        {
            if (Author == other.Author
                    && Description == other.Description
                    && FileName == other.FileName
                    && Host == other.Host
                    && Name == other.Name
                    && Online == other.Online
                    && Version == other.Version)
                return true;
            else
                return false;
        }

        public Type GetActionType(string actionType = "")
        {
            return typeof(SnapshotAction);
        }

        public IEnumerable<Type> GetActionTypes()
        {
            return new Type[] { typeof(SnapshotAction) };
        }

        public bool PerformAction(string eventName, IEnumerable<IEventTrigger> triggers, IAction action)
        {
            WebClient webClient = null;
            bool success = false;
            Impersonation impersonation = null;

            try
            {
                var snapshotAction = action as SnapshotAction;
                if (snapshotAction != null)
                {
                    webClient = new WebClient();

                    if (!string.IsNullOrWhiteSpace(snapshotAction.WebClientUserName))
                    {
                        webClient.Credentials = new NetworkCredential(snapshotAction.WebClientUserName, snapshotAction.WebClientPassword);
                    }

                    byte[] imagedata = webClient.DownloadData(snapshotAction.SnapshotURL);

                    DateTime eventDT = triggers.FirstOrDefault().TriggerDT;

                    if (ImageCaptured != null)
                        ImageCaptured(this, new ImageCapturedEventArgs(imagedata, eventName, eventDT));

                    if (snapshotAction.Export)
                    {
                        string folder = snapshotAction.ExportLocation;

                        if (string.IsNullOrWhiteSpace(folder))
                            folder = Path.Combine(this.Properties["DefaultFolder"], eventName);

                        if (string.IsNullOrWhiteSpace(folder))
                            folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Ethele", "RioSoft", "Snapshots", eventName);

                        if (!Directory.Exists(folder))
                            Directory.CreateDirectory(folder);

                        string fileName = "";

                        if (!string.IsNullOrWhiteSpace(snapshotAction.ExportNamePrefix))
                            fileName = snapshotAction.ExportNamePrefix;

                        if (snapshotAction.UseExportNameDateTimeSuffix)
                            fileName += eventDT.ToString(string.IsNullOrWhiteSpace(snapshotAction.ExportNameSuffixFormat) ? "yyyyMMdd_HHmmssff" : snapshotAction.ExportNameSuffixFormat);

                        if(string.IsNullOrWhiteSpace(fileName))
                            fileName = "You should specify a file name";

                        if (!string.IsNullOrWhiteSpace(snapshotAction.ImpersonationUserName))
                        {
                            impersonation = new Impersonation(snapshotAction.ImpersonationDomain, snapshotAction.ImpersonationUserName, snapshotAction.ImpersonationPassword);
                        }

                        using (FileStream fs = new FileStream(string.Format("{0}\\{1}.jpg", folder, fileName), FileMode.Create))
                        {
                            using (BinaryWriter bw = new BinaryWriter(fs))
                            {
                                bw.Write(imagedata);
                                bw.Close();
                            }
                        }
                    }

                    success = true;
                }
            }
            catch(Exception ex)
            {
                if (Error != null)
                    Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), ex.Message, ex));
            }
            finally
            {
                if (webClient != null)
                    webClient.Dispose();

                if (impersonation != null)
                    impersonation.Dispose();
            }

            return success;
        }
    }
}
