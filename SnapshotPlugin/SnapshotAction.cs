﻿using eNervePluginInterface;
using eNervePluginInterface.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnapshotPlugin
{
    class SnapshotAction : PluginAction
    {
        public SnapshotAction() {}

        public SnapshotAction(string _name, bool _value, string _pluginName, int _actionOrder = 0) : base(_name, _value, _pluginName, _actionOrder) {}

        [DisplayInPluginActionPropertiesList]
        public string SnapshotURL { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string WebClientUserName { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string WebClientPassword { get; set; }

        [DisplayInPluginActionPropertiesList]        
        public bool Export { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ExportLocation { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ExportNamePrefix { get; set; }

        [DisplayInPluginActionPropertiesList]
        public bool UseExportNameDateTimeSuffix { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ExportNameSuffixFormat { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ImpersonationDomain { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ImpersonationUserName { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string ImpersonationPassword { get; set; }
    }
}
