﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eNervePluginInterface;
using eNervePluginInterface.CustomAttributes;

namespace SMSPortalPlugin
{
    class SendSMSAction : PluginAction
    {
        public SendSMSAction() : base() { }

        [DisplayInPluginActionPropertiesList]
        public string CellPhoneNumbers { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string GroupName { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string GroupNumber { get; set; }

        [DisplayInPluginActionPropertiesList]
        public string MessageBody { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}
