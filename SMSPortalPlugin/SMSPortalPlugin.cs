﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Timers;
using System.Xml.Linq;

using eNervePluginInterface;
using eThele.Essentials;

namespace SMSPortalPlugin
{
    public class SMSPortalPlugin : INerveActionPlugin, INerveEventPlugin, IDisposable, INotifyPropertyChanged
    {
        Timer pollTimer;

        ~SMSPortalPlugin()
        {
            Dispose(false);
        }

        public string Author
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company;
            }
        }

        private List<CustomPropertyDefinition> customPropertyDefinitions = null;

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                if (customPropertyDefinitions != null)
                    return customPropertyDefinitions;
                else
                {
                    customPropertyDefinitions = new List<CustomPropertyDefinition>();
                    customPropertyDefinitions.Add(new CustomPropertyDefinition("SMSPollInterval", "SMS Poll Interval", 
                        "Interval between Polls for new SMS's (Default is 60000 ms)", typeof(CustomProperty)) { DefaultValue = "1" });
                    customPropertyDefinitions.Add(new CustomPropertyDefinition("PortalUsername", "Portal Username",
                         "User name on the SMS portal", typeof(CustomProperty)));
                    customPropertyDefinitions.Add(new CustomPropertyDefinition("PortalPassword", "Portal Password",
                        "Password on the SMS Portal", typeof(CustomProperty)));
                    customPropertyDefinitions.Add(new CustomPropertyDefinition("LastReplyID", "Last SMS received from portal",
                        "Last SMS from Portal", typeof(CustomProperty))
                    { DefaultValue = "0"});

                    return customPropertyDefinitions;
                }
            }
        }

        public string Description
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
            }
        }

        public bool Enabled { get; set; }

        public string[] EventNames
        {
            get { return new string[] { "SMSReceived" }; }
        }

        private string fileName;

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public INerveHostPlugin Host { get; set; }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }

        public string Name { get; set; }

        public bool Online { get; set; }

        private CustomProperties properties;

        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }

        public string Version
        {
            get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
        }public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        }

        public event EventHandler DeviceOffline;
        public event EventHandler DeviceOnline;
        public event EventHandler<PluginErrorEventArgs> Error;
        public event EventHandler<PluginTriggerEventArgs> Trigger;

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        public Type GetActionType(string actionType = "")
        {
            Type result = null;
            try
            {
                result = ActionTypeDictionary[actionType];
            }
            catch (KeyNotFoundException)
            {
                result = typeof(SendSMSAction);
            }

            return result;
        }

        public IEnumerable<Type> GetActionTypes()
        {
            return ActionTypeDictionary.Values.ToArray();
        }

        public bool PerformAction(string eventName, IEnumerable<IEventTrigger> triggers, IAction action)
        {
            bool success = false;
            if (string.IsNullOrWhiteSpace(Properties["PortalUsername"]))
            {
                OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                           string.Format("Unable to send SMS - Missing Portal Username", action.ToString()), new Exception()));
                return false;
            }
            if (string.IsNullOrWhiteSpace(Properties["PortalPassword"]))
            {
                OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                           string.Format("Unable to send SMS - Missing Portal Password", action.ToString()), new Exception()));
                return false;
            }
            var smsAction = action as SendSMSAction;
            if (smsAction != null)
            {
                Guid messageId = Guid.NewGuid();
                if (string.IsNullOrWhiteSpace(smsAction.MessageBody))
                {
                    OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name),
                           string.Format("Invalid SendSMSAction: Missing message body: {0}", action.ToString()), new Exception()));
                    return false;
                }
                else
                {
                    if ((smsAction.TimeStamp == null) || (smsAction.TimeStamp.ToUniversalTime() == DateTime.MinValue))
                    {
                        if (triggers != null && triggers.Count() > 0)
                            smsAction.TimeStamp = triggers.FirstOrDefault().TriggerDT;
                        else
                            smsAction.TimeStamp = DateTime.Now;// Ensure correct timestamp is retained.
                    }
                    if (triggers != null && triggers.Count() > 0)
                        messageId = triggers.FirstOrDefault().TriggerID;
                }
                if (string.IsNullOrWhiteSpace(smsAction.GroupNumber))
                {
                    if (string.IsNullOrWhiteSpace(smsAction.GroupName))
                        if (string.IsNullOrWhiteSpace(smsAction.CellPhoneNumbers))
                            OnError(new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                                MethodBase.GetCurrentMethod().Name), string.Format("Invalid SendSMSAction: Missing destination: {0}", action.ToString()),
                                new Exception()));
                        else
                        {
                            success = SendSMSToNumbers(smsAction, messageId);
                        }
                    else
                    {
                        smsAction.GroupNumber = GetGroupNumber(smsAction.GroupName);
                        if (string.IsNullOrWhiteSpace(smsAction.GroupNumber))
                            OnError(new PluginErrorEventArgs(MethodBase.GetCurrentMethod().Name, "Unable to find groupnumber from Web Service for group: " +
                                smsAction.GroupName, new Exception()));
                        else
                        {
                            success = SendSMSToGroup(smsAction, messageId);
                        }
                    }
                }
                else
                {
                    success = SendSMSToGroup(smsAction, messageId);
                }
            }

            return success;
        }


        #region PropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(_propertyName));
        }
        #endregion PropertyChanged

        public void Start()
        {
            CreateTimer();
            using (var ping = new Ping())
            {
                if (ping.Send("www.mymobileapi.com").Status == IPStatus.Success)
                {

                    if (DeviceOnline != null)
                        DeviceOnline(this, new EventArgs());
                    Online = true;
                    pollTimer.Start();
                }
                else
                {
                    if (DeviceOffline != null)
                        DeviceOffline(this, new EventArgs());
                    Online = false;
                }
            }
        }

        private static Dictionary<string, Type> ActionTypeDictionary = new Dictionary<string, Type>(StringComparer.InvariantCultureIgnoreCase)
        {
            { "SendSMSAction", typeof(SendSMSAction) }
        };        

        private static bool checkNumber(string number)
        {
            if (string.IsNullOrWhiteSpace(number))
                return false;
            if (number[0] == '0' && number.Length == 10)
                return true;
            if (number[0] == '2' && number[1] == '7' && number.Length == 11)
                return true;
            if (number[0] == '+' && number[1] == '2' && number[2] == '7' && number.Length == 12)
                return true;

            return false;
        }

        private void CreateTimer()
        {
            int pollMS = 1;
            string err;
            if (TimeSpanParser.TryParse(Properties["SMSPollInterval"], out pollMS, out err))
            {
                if (pollMS < 60000)
                    pollMS = 60000;
                else if (pollMS > 3600000)
                    pollMS = 60;
                pollTimer = new Timer(pollMS);
            }
            else
            {
                pollTimer = new Timer(1 * 60000);
                Properties["SMSPollInterval"] = "60000";
                PropertyChangedHandler("Properties");
                if (Error != null)
                {
                    Error.Invoke(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing SMSPollInterval: {0}", err), null));
                }
            }
            pollTimer.Elapsed += PollTimer_Elapsed;
            pollTimer.AutoReset = false;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (pollTimer != null)
            {
                pollTimer.Elapsed -= PollTimer_Elapsed;
                pollTimer.Dispose();
                pollTimer = null;
            }
        }

        private string GetGroupNumber(string groupName)
        {
            XDocument doc = new XDocument();
            XElement baseNode = new XElement("options");
            XElement settingsNode = new XElement("settings");
            settingsNode.Add(new XElement("cols_returned", "groupid,groupname"));
            baseNode.Add(settingsNode);
            doc.Add(baseNode);
            string result = "";

            using (SMSPortalWebServiceFix api = new SMSPortalWebServiceFix())
            {
                result = api.Groups_List_STR_STR(Properties["PortalUsername"], Properties["PortalPassword"], doc.ToString());
                if (!string.IsNullOrWhiteSpace(result))
                {
                    doc = XDocument.Parse(result);
                    result = "";
                    foreach (var item in doc.Element("api_result").Elements("data"))
                    {
                        if (item.Element("groupname").Value == groupName)
                        {
                            result = item.Element("groupid").Value;
                            continue;
                        }
                    }
                }
            }

            return result;
        }

        private void OnError(PluginErrorEventArgs e)
        {
            if ((Error != null) && (e != null))
                Error(this, e);
        }

        private void PollTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            int pollMS = 60000;
            if (pollTimer == null)
                CreateTimer();
            else
            {
                pollTimer.Stop();
                string err;
                if (TimeSpanParser.TryParse(Properties["SMSPollInterval"], out pollMS, out err))
                {
                    if (pollMS < 60000)
                    {
                        pollMS = 60000;
                    }
                    else if (pollMS > 3600000)
                    {
                        pollMS = 3600000;
                    }
                    pollTimer.Interval = pollMS;
                }
                else if (Error != null)
                    Error.Invoke(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing SMSPollInterval: {0}", err), null));
            }

            using (var ping = new Ping())
            {
                if (ping.Send("www.mymobileapi.com").Status == IPStatus.Success)
                {
                    if (!Online)
                    {
                        if (DeviceOnline != null)
                            DeviceOnline(this, new EventArgs());
                        Online = true;
                    }
                }
                else
                {
                    if (Online)
                    {
                        if (DeviceOffline != null)
                            DeviceOffline(this, new EventArgs());
                        Online = false;
                    }
                }
            }

            if (Online)
            {
                XDocument doc = new XDocument();
                XElement baseNode = new XElement("reply");
                XElement settingsNode = new XElement("settings");
                settingsNode.Add(new XElement("id", Properties["LastReplyID"]));
                settingsNode.Add(new XElement("cols_returned", "numfrom,receiveddata,received,sentdata,sentcustomerid"));
                baseNode.Add(settingsNode);
                doc.Add(baseNode);

                using (SMSPortalWebServiceFix api = new SMSPortalWebServiceFix())
                {
                    string result = api.Reply_STR_STR(Properties["PortalUsername"], Properties["PortalPassword"], doc.ToString());
                    doc = XDocument.Parse(result);
                }
                IEnumerable<XElement> messages = doc.Element("api_result").Elements("data");
                if (messages.Count() > 0)
                {
                    int lastReply = 0;
                    int newReply;

                    foreach (var message in messages)
                    {
                        if (message != null)
                        {
                            if (Properties["LastReplyID"] != "0") // Moenie boodskappe lees as dit 'n nuwe plugin is nie.
                            {
                                if (Trigger != null)
                                {
                                    Trigger(this, new PluginTriggerEventArgs(new SMSPortalTrigger()
                                    {
                                        TriggerID = Guid.Parse(doc.Element("api_result").Element("data").Element("sentcustomerid").Value),
                                        DeviceName = Name,
                                        DetectorName = EventNames[0],
                                        TriggerDT = DateTime.Now,
                                        EventName = EventNames[0],
                                        DetectorType = DetectorType.Unknown,
                                        Data = doc.Element("api_result").Element("data").Element("receiveddata").Value,
                                        State = true
                                    }));
                                }
                            }
                            if (int.TryParse(message.Element("replyid").Value, out newReply))
                            {
                                if (doc.Element("api_result").Element("call_result").Element("result").Value == "True")
                                {
                                    if (newReply > lastReply)
                                    {
                                        lastReply = newReply;
                                    }
                                }
                            }
                        }
                    }
                    Properties["LastReplyID"] = lastReply.ToString();
                    PropertyChangedHandler("Properties");
                }
                pollTimer.Start();
            }
        }

        private bool SendDoc(string username, string password, XDocument doc)
        {
            try
            {
                using (SMSPortalWebServiceFix api = new SMSPortalWebServiceFix())
                {
                    doc = XDocument.Parse(api.Send_STR_STR(username, password, doc.ToString()));
                    return doc.Element("api_result").Element("call_result").Element("result").Value == "True";
                }
            }
            catch (Exception ex)
            {
                OnError(new PluginErrorEventArgs(MethodBase.GetCurrentMethod().Name, "Error sending SMS to web service: " +
                                doc.ToString(), ex));
            }
            return false;
        }

        private bool SendSMSToGroup(SendSMSAction smsAction, Guid messageId)
        {
            if (smsAction.MessageBody.Length > 155)
                smsAction.MessageBody = smsAction.MessageBody.Substring(0, 155);

            XDocument doc = new XDocument();
            XElement baseNode = new XElement("senddata");
            XElement settingsNode = new XElement("settings");
            settingsNode.Add(new XElement("return_credits", "True"));
            settingsNode.Add(new XElement("return_msgs_credits_used", "False"));
            settingsNode.Add(new XElement("return_msgs_success_count", "False"));
            settingsNode.Add(new XElement("return_msgs_failed_count", "False"));
            settingsNode.Add(new XElement("return_entries_success_status", "False"));
            settingsNode.Add(new XElement("return_entries_failed_status", "True"));
            settingsNode.Add(new XElement("default_senderid"));
            settingsNode.Add(new XElement("default_date", DateTime.Now.ToString("dd/MMM/yyyy")));
            settingsNode.Add(new XElement("default_time", DateTime.Now.ToString("HH:mm")));
            settingsNode.Add(new XElement("default_curdate", smsAction.TimeStamp.ToString("dd/MMM/yyyy")));
            settingsNode.Add(new XElement("default_curtime", smsAction.TimeStamp.ToString("HH:mm")));
            settingsNode.Add(new XElement("default_data1", smsAction.MessageBody));
            settingsNode.Add(new XElement("default_data2"));
            settingsNode.Add(new XElement("default_flash", "False"));
            settingsNode.Add(new XElement("default_type", "SMS"));

            settingsNode.Add(new XElement("default_costcentre", "NA"));
            settingsNode.Add(new XElement("default_validityperiod", "0"));
            settingsNode.Add(new XElement("send_groupid", smsAction.GroupNumber));


            baseNode.Add(settingsNode);
            doc.Add(baseNode);
            return SendDoc(Properties["PortalUsername"], Properties["PortalPassword"], doc);
        }

        private bool SendSMSToNumbers(SendSMSAction smsAction, Guid messageId)
        {
            if (smsAction.MessageBody.Length > 155)
                smsAction.MessageBody = smsAction.MessageBody.Substring(0, 155);

            XDocument doc = new XDocument();
            XElement baseNode = new XElement("senddata");
            XElement settingsNode = new XElement("settings");
            settingsNode.Add(new XElement("default_date", smsAction.TimeStamp.ToString("dd/MMM/yyyy")));
            settingsNode.Add(new XElement("default_time", smsAction.TimeStamp.ToString("HH:mm")));
            baseNode.Add(settingsNode);

            foreach (string number in smsAction.CellPhoneNumbers.Split(new char[] { ',', '.', ';' }))
            {
                if (checkNumber(number))
                {
                    XElement entry = new XElement("entries");
                    entry.Add(new XElement("numto", number));
                    entry.Add(new XElement("customerid", messageId));
                    entry.Add(new XElement("senderid", "e-Thele"));
                    entry.Add(new XElement("data1", smsAction.MessageBody));
                    entry.Add(new XElement("data2"));
                    entry.Add(new XElement("flash", "False"));
                    entry.Add(new XElement("type", "SMS"));
                    entry.Add(new XElement("costcentre", "NA"));
                    entry.Add(new XElement("validityperiod", "0"));
                    baseNode.Add(entry);
                }
            }
            doc.Add(baseNode);
            return SendDoc(Properties["PortalUsername"], Properties["PortalPassword"], doc);
        }
    }
}
