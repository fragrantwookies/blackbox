﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using eNervePluginInterface;

namespace SMSPortalPlugin
{
    public class SMSPortalTrigger : IEventTrigger
    {
        protected Guid eventID;
        protected string data = "";
        protected int detectorGroupIndex = -1;
        protected int detectorIndex = -1;
        protected string detectorName;
        protected DetectorType detectorType = DetectorType.Unknown;
        protected string deviceName;
        protected double eventValue = 0;
        protected string location = "";
        protected string mapName = "";
        protected string name = string.Empty;
        protected bool state = true;
        protected DateTime triggerDT;
        protected Guid triggerID;

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(_propertyName));
        }

        public Guid EventID
        {
            get { return eventID; }
            set { eventID = value; PropertyChangedHandler("EventID"); }
        }

        public string Data
        {
            get { return data; }
            set { data = value; ; PropertyChangedHandler("Data"); }
        }

        public int DetectorGroupIndex
        {
            get { return detectorGroupIndex; }
            set { detectorGroupIndex = value; PropertyChangedHandler("DetectorGroupIndex"); }
        }

        public int DetectorIndex
        {
            get { return detectorIndex; }
            set { detectorIndex = value; PropertyChangedHandler("DetectorIndex"); }
        }

        public string DetectorName
        {
            get { return detectorName; }
            set { detectorName = value; PropertyChangedHandler("DetectorName"); }
        }

        public DetectorType DetectorType
        {
            get { return detectorType; }
            set { detectorType = value; PropertyChangedHandler("DetectorType"); }
        }

        public string DeviceName
        {
            get { return deviceName; }
            set { deviceName = value; PropertyChangedHandler("DeviceName"); }
        }

        public string EventName
        {
            get { return name; }
            set { name = value; PropertyChangedHandler("EventName"); }
        }

        public double EventValue
        {
            get { return eventValue; }
            set { eventValue = value; PropertyChangedHandler("EventValue"); }
        }

        public string Location
        {
            get { return location; }
            set { location = value; PropertyChangedHandler("Location"); }
        }

        public string MapName
        {
            get { return mapName; }
            set { mapName = value; PropertyChangedHandler("MapName"); }
        }

        public bool State
        {
            get { return state; }
            set { state = value; PropertyChangedHandler("State"); }
        }

        public DateTime TriggerDT
        {
            get { return triggerDT; }
            set { triggerDT = value; PropertyChangedHandler("TriggerDT"); }
        }

        public Guid TriggerID
        {
            get { return triggerID; }
            set { triggerID = value; }
        }

        public int Compare(IEventTrigger x, IEventTrigger y)
        {
            if (x.DeviceName == y.DeviceName)
                return x.DetectorName.CompareTo(y.DetectorName);
            else
                return x.DeviceName.CompareTo(y.DeviceName);
        }

        public int CompareTo(IEventTrigger other)
        {
            if (DeviceName == other.DeviceName)
                return DetectorName.CompareTo(other.DetectorName);
            else
                return DeviceName.CompareTo(other.DeviceName);
        }

        public bool Equals(IEventTrigger other)
        {
            return TriggerID.Equals(other.TriggerID);
        }

        public bool Equals(Guid other)
        {
            return TriggerID.Equals(other);
        }
    }
}
