﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SMSPortalPlugin
{
    class SMSPortalWebServiceFix : WebServiceAPI.API
    {
        protected override WebRequest GetWebRequest(Uri uri)
        {
            ServicePointManager.Expect100Continue = false;
            HttpWebRequest request = (HttpWebRequest)base.GetWebRequest(uri);
            request.KeepAlive = false;
            return request;
        }
    }
}
