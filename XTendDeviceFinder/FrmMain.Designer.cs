﻿namespace XPointAudiovisual.Xtend {
    partial class frmMain {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing ) {
            if ( disposing && ( components != null ) ) {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.trvwFoundDevices = new System.Windows.Forms.TreeView();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.prgbrFind = new System.Windows.Forms.ProgressBar();
            this.prgbrUpdate = new System.Windows.Forms.ProgressBar();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnTestGood = new System.Windows.Forms.Button();
            this.btnTestBad = new System.Windows.Forms.Button();
            this.btnDebugOn = new System.Windows.Forms.Button();
            this.btnDebugOff = new System.Windows.Forms.Button();
            this.txtbxOutput = new System.Windows.Forms.TextBox();
            this.btnDtr = new System.Windows.Forms.Button();
            this.btnRts = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnFullRS232 = new System.Windows.Forms.Button();
            this.btnRxOnlyRS232 = new System.Windows.Forms.Button();
            this.btnResynch = new System.Windows.Forms.Button();
            this.btnCopyMac = new System.Windows.Forms.Button();
            this.btnRelay2 = new System.Windows.Forms.Button();
            this.btnRelay1 = new System.Windows.Forms.Button();
            this.lblCurrentCard = new System.Windows.Forms.Label();
            this.btnLoadCfg = new System.Windows.Forms.Button();
            this.lblCurrentElement = new System.Windows.Forms.Label();
            this.lblCurrentMap = new System.Windows.Forms.Label();
            this.lblValueDisplay = new System.Windows.Forms.Label();
            this.btnMonitorAnalogs = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFDMCollect = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnFDMMonitor = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // trvwFoundDevices
            // 
            this.trvwFoundDevices.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.trvwFoundDevices.HideSelection = false;
            this.trvwFoundDevices.Location = new System.Drawing.Point(9, 10);
            this.trvwFoundDevices.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.trvwFoundDevices.Name = "trvwFoundDevices";
            this.trvwFoundDevices.Size = new System.Drawing.Size(524, 305);
            this.trvwFoundDevices.TabIndex = 1;
            this.trvwFoundDevices.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.trvwFoundDevices_AfterSelect);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(544, 62);
            this.btnRefresh.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(80, 21);
            this.btnRefresh.TabIndex = 2;
            this.btnRefresh.Text = "refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // prgbrFind
            // 
            this.prgbrFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prgbrFind.Location = new System.Drawing.Point(544, 88);
            this.prgbrFind.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.prgbrFind.Name = "prgbrFind";
            this.prgbrFind.Size = new System.Drawing.Size(80, 9);
            this.prgbrFind.TabIndex = 3;
            // 
            // prgbrUpdate
            // 
            this.prgbrUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.prgbrUpdate.Location = new System.Drawing.Point(544, 134);
            this.prgbrUpdate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.prgbrUpdate.Name = "prgbrUpdate";
            this.prgbrUpdate.Size = new System.Drawing.Size(80, 9);
            this.prgbrUpdate.TabIndex = 6;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUpdate.ForeColor = System.Drawing.Color.Red;
            this.btnUpdate.Location = new System.Drawing.Point(544, 102);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(80, 28);
            this.btnUpdate.TabIndex = 5;
            this.btnUpdate.Text = "update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnTestGood
            // 
            this.btnTestGood.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestGood.Location = new System.Drawing.Point(637, 171);
            this.btnTestGood.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnTestGood.Name = "btnTestGood";
            this.btnTestGood.Size = new System.Drawing.Size(80, 21);
            this.btnTestGood.TabIndex = 7;
            this.btnTestGood.Text = "comm test";
            this.btnTestGood.UseVisualStyleBackColor = true;
            this.btnTestGood.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnTestBad
            // 
            this.btnTestBad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTestBad.Location = new System.Drawing.Point(637, 145);
            this.btnTestBad.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnTestBad.Name = "btnTestBad";
            this.btnTestBad.Size = new System.Drawing.Size(80, 21);
            this.btnTestBad.TabIndex = 8;
            this.btnTestBad.Text = "error test";
            this.btnTestBad.UseVisualStyleBackColor = true;
            this.btnTestBad.Click += new System.EventHandler(this.btnTestBad_Click);
            // 
            // btnDebugOn
            // 
            this.btnDebugOn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDebugOn.Location = new System.Drawing.Point(637, 72);
            this.btnDebugOn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDebugOn.Name = "btnDebugOn";
            this.btnDebugOn.Size = new System.Drawing.Size(35, 19);
            this.btnDebugOn.TabIndex = 9;
            this.btnDebugOn.Text = "d-on";
            this.btnDebugOn.UseVisualStyleBackColor = true;
            this.btnDebugOn.Click += new System.EventHandler(this.btnDebugOn_Click);
            // 
            // btnDebugOff
            // 
            this.btnDebugOff.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDebugOff.Location = new System.Drawing.Point(682, 72);
            this.btnDebugOff.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDebugOff.Name = "btnDebugOff";
            this.btnDebugOff.Size = new System.Drawing.Size(35, 19);
            this.btnDebugOff.TabIndex = 10;
            this.btnDebugOff.Text = "d-off";
            this.btnDebugOff.UseVisualStyleBackColor = true;
            this.btnDebugOff.Click += new System.EventHandler(this.btnDebugOff_Click);
            // 
            // txtbxOutput
            // 
            this.txtbxOutput.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtbxOutput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtbxOutput.Location = new System.Drawing.Point(9, 330);
            this.txtbxOutput.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.txtbxOutput.Multiline = true;
            this.txtbxOutput.Name = "txtbxOutput";
            this.txtbxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtbxOutput.Size = new System.Drawing.Size(524, 132);
            this.txtbxOutput.TabIndex = 11;
            // 
            // btnDtr
            // 
            this.btnDtr.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDtr.Location = new System.Drawing.Point(637, 95);
            this.btnDtr.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDtr.Name = "btnDtr";
            this.btnDtr.Size = new System.Drawing.Size(35, 19);
            this.btnDtr.TabIndex = 12;
            this.btnDtr.Text = "dtr";
            this.btnDtr.UseVisualStyleBackColor = true;
            this.btnDtr.Click += new System.EventHandler(this.btnDtr_Click);
            // 
            // btnRts
            // 
            this.btnRts.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRts.Location = new System.Drawing.Point(682, 95);
            this.btnRts.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRts.Name = "btnRts";
            this.btnRts.Size = new System.Drawing.Size(35, 19);
            this.btnRts.TabIndex = 13;
            this.btnRts.Text = "rts";
            this.btnRts.UseVisualStyleBackColor = true;
            this.btnRts.Click += new System.EventHandler(this.btnRts_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(545, 353);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(80, 28);
            this.btnClear.TabIndex = 14;
            this.btnClear.Text = "clear text";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnFullRS232
            // 
            this.btnFullRS232.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFullRS232.Location = new System.Drawing.Point(637, 119);
            this.btnFullRS232.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnFullRS232.Name = "btnFullRS232";
            this.btnFullRS232.Size = new System.Drawing.Size(35, 19);
            this.btnFullRS232.TabIndex = 15;
            this.btnFullRS232.Text = "full";
            this.btnFullRS232.UseVisualStyleBackColor = true;
            this.btnFullRS232.Click += new System.EventHandler(this.btnFullRS232_Click);
            // 
            // btnRxOnlyRS232
            // 
            this.btnRxOnlyRS232.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRxOnlyRS232.Location = new System.Drawing.Point(682, 119);
            this.btnRxOnlyRS232.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRxOnlyRS232.Name = "btnRxOnlyRS232";
            this.btnRxOnlyRS232.Size = new System.Drawing.Size(35, 19);
            this.btnRxOnlyRS232.TabIndex = 16;
            this.btnRxOnlyRS232.Text = "rx";
            this.btnRxOnlyRS232.UseVisualStyleBackColor = true;
            this.btnRxOnlyRS232.Click += new System.EventHandler(this.btnRxOnlyRS232_Click);
            // 
            // btnResynch
            // 
            this.btnResynch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnResynch.Location = new System.Drawing.Point(544, 36);
            this.btnResynch.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnResynch.Name = "btnResynch";
            this.btnResynch.Size = new System.Drawing.Size(80, 21);
            this.btnResynch.TabIndex = 17;
            this.btnResynch.Text = "resynch";
            this.btnResynch.UseVisualStyleBackColor = true;
            this.btnResynch.Click += new System.EventHandler(this.btnResynch_Click);
            // 
            // btnCopyMac
            // 
            this.btnCopyMac.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyMac.Location = new System.Drawing.Point(545, 329);
            this.btnCopyMac.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCopyMac.Name = "btnCopyMac";
            this.btnCopyMac.Size = new System.Drawing.Size(80, 20);
            this.btnCopyMac.TabIndex = 18;
            this.btnCopyMac.Text = "copy *MAC*";
            this.btnCopyMac.UseVisualStyleBackColor = true;
            this.btnCopyMac.Click += new System.EventHandler(this.btnCopyMac_Click);
            // 
            // btnRelay2
            // 
            this.btnRelay2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRelay2.Location = new System.Drawing.Point(682, 37);
            this.btnRelay2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRelay2.Name = "btnRelay2";
            this.btnRelay2.Size = new System.Drawing.Size(35, 19);
            this.btnRelay2.TabIndex = 20;
            this.btnRelay2.Text = "ro2";
            this.btnRelay2.UseVisualStyleBackColor = true;
            this.btnRelay2.Click += new System.EventHandler(this.btnRelay2_Click);
            // 
            // btnRelay1
            // 
            this.btnRelay1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRelay1.Location = new System.Drawing.Point(637, 37);
            this.btnRelay1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnRelay1.Name = "btnRelay1";
            this.btnRelay1.Size = new System.Drawing.Size(35, 19);
            this.btnRelay1.TabIndex = 19;
            this.btnRelay1.Text = "ro1";
            this.btnRelay1.UseVisualStyleBackColor = true;
            this.btnRelay1.Click += new System.EventHandler(this.btnRelay1_Click);
            // 
            // lblCurrentCard
            // 
            this.lblCurrentCard.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentCard.AutoSize = true;
            this.lblCurrentCard.Location = new System.Drawing.Point(544, 159);
            this.lblCurrentCard.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCurrentCard.Name = "lblCurrentCard";
            this.lblCurrentCard.Size = new System.Drawing.Size(41, 13);
            this.lblCurrentCard.TabIndex = 21;
            this.lblCurrentCard.Text = "Card: 0";
            // 
            // btnLoadCfg
            // 
            this.btnLoadCfg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadCfg.Location = new System.Drawing.Point(637, 197);
            this.btnLoadCfg.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnLoadCfg.Name = "btnLoadCfg";
            this.btnLoadCfg.Size = new System.Drawing.Size(80, 21);
            this.btnLoadCfg.TabIndex = 22;
            this.btnLoadCfg.Text = "load cfg";
            this.btnLoadCfg.UseVisualStyleBackColor = true;
            this.btnLoadCfg.Click += new System.EventHandler(this.btnLoadCfg_Click);
            // 
            // lblCurrentElement
            // 
            this.lblCurrentElement.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentElement.AutoSize = true;
            this.lblCurrentElement.Location = new System.Drawing.Point(544, 173);
            this.lblCurrentElement.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCurrentElement.Name = "lblCurrentElement";
            this.lblCurrentElement.Size = new System.Drawing.Size(42, 13);
            this.lblCurrentElement.TabIndex = 23;
            this.lblCurrentElement.Text = "Elem: 0";
            // 
            // lblCurrentMap
            // 
            this.lblCurrentMap.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCurrentMap.AutoSize = true;
            this.lblCurrentMap.Location = new System.Drawing.Point(544, 187);
            this.lblCurrentMap.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCurrentMap.Name = "lblCurrentMap";
            this.lblCurrentMap.Size = new System.Drawing.Size(40, 13);
            this.lblCurrentMap.TabIndex = 24;
            this.lblCurrentMap.Text = "Map: 0";
            // 
            // lblValueDisplay
            // 
            this.lblValueDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblValueDisplay.Location = new System.Drawing.Point(667, 223);
            this.lblValueDisplay.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblValueDisplay.Name = "lblValueDisplay";
            this.lblValueDisplay.Size = new System.Drawing.Size(50, 19);
            this.lblValueDisplay.TabIndex = 25;
            this.lblValueDisplay.Text = "  ";
            this.lblValueDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnMonitorAnalogs
            // 
            this.btnMonitorAnalogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMonitorAnalogs.Location = new System.Drawing.Point(637, 223);
            this.btnMonitorAnalogs.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnMonitorAnalogs.Name = "btnMonitorAnalogs";
            this.btnMonitorAnalogs.Size = new System.Drawing.Size(23, 19);
            this.btnMonitorAnalogs.TabIndex = 26;
            this.btnMonitorAnalogs.Text = "?";
            this.btnMonitorAnalogs.UseVisualStyleBackColor = true;
            this.btnMonitorAnalogs.Click += new System.EventHandler(this.btnMonitorAnalogs_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(629, 8);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(4, 452);
            this.panel1.TabIndex = 27;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Location = new System.Drawing.Point(722, 8);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(4, 452);
            this.panel2.TabIndex = 28;
            // 
            // btnFDMCollect
            // 
            this.btnFDMCollect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFDMCollect.Location = new System.Drawing.Point(729, 36);
            this.btnFDMCollect.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnFDMCollect.Name = "btnFDMCollect";
            this.btnFDMCollect.Size = new System.Drawing.Size(80, 21);
            this.btnFDMCollect.TabIndex = 29;
            this.btnFDMCollect.Text = "FDM collect";
            this.btnFDMCollect.UseVisualStyleBackColor = true;
            this.btnFDMCollect.Click += new System.EventHandler(this.btnFDMCollect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(646, 14);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "IO Testing";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(736, 14);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Development";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(561, 14);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "System";
            // 
            // btnFDMMonitor
            // 
            this.btnFDMMonitor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFDMMonitor.Location = new System.Drawing.Point(729, 62);
            this.btnFDMMonitor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnFDMMonitor.Name = "btnFDMMonitor";
            this.btnFDMMonitor.Size = new System.Drawing.Size(80, 21);
            this.btnFDMMonitor.TabIndex = 33;
            this.btnFDMMonitor.Text = "FDM monitor";
            this.btnFDMMonitor.UseVisualStyleBackColor = true;
            this.btnFDMMonitor.Click += new System.EventHandler(this.btnFDMMonitor_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(815, 471);
            this.Controls.Add(this.btnFDMMonitor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnFDMCollect);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnMonitorAnalogs);
            this.Controls.Add(this.lblValueDisplay);
            this.Controls.Add(this.lblCurrentMap);
            this.Controls.Add(this.lblCurrentElement);
            this.Controls.Add(this.btnLoadCfg);
            this.Controls.Add(this.lblCurrentCard);
            this.Controls.Add(this.btnRelay2);
            this.Controls.Add(this.btnRelay1);
            this.Controls.Add(this.btnCopyMac);
            this.Controls.Add(this.btnResynch);
            this.Controls.Add(this.btnRxOnlyRS232);
            this.Controls.Add(this.btnFullRS232);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnRts);
            this.Controls.Add(this.btnDtr);
            this.Controls.Add(this.txtbxOutput);
            this.Controls.Add(this.btnDebugOff);
            this.Controls.Add(this.btnDebugOn);
            this.Controls.Add(this.btnTestBad);
            this.Controls.Add(this.btnTestGood);
            this.Controls.Add(this.prgbrUpdate);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.prgbrFind);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.trvwFoundDevices);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "frmMain";
            this.Text = "XPoint Audiovisual - Xtend device finder  v1.12";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView trvwFoundDevices;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.ProgressBar prgbrFind;
        private System.Windows.Forms.ProgressBar prgbrUpdate;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnTestGood;
        private System.Windows.Forms.Button btnTestBad;
        private System.Windows.Forms.Button btnDebugOn;
        private System.Windows.Forms.Button btnDebugOff;
        private System.Windows.Forms.TextBox txtbxOutput;
        private System.Windows.Forms.Button btnDtr;
        private System.Windows.Forms.Button btnRts;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnFullRS232;
        private System.Windows.Forms.Button btnRxOnlyRS232;
        private System.Windows.Forms.Button btnResynch;
        private System.Windows.Forms.Button btnCopyMac;
        private System.Windows.Forms.Button btnRelay2;
        private System.Windows.Forms.Button btnRelay1;
        private System.Windows.Forms.Label lblCurrentCard;
        private System.Windows.Forms.Button btnLoadCfg;
        private System.Windows.Forms.Label lblCurrentElement;
        private System.Windows.Forms.Label lblCurrentMap;
        private System.Windows.Forms.Label lblValueDisplay;
        private System.Windows.Forms.Button btnMonitorAnalogs;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnFDMCollect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnFDMMonitor;
    }
}

