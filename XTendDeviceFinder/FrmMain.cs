﻿#region Copyright
//*****************************************************************************
//
//  Copyright (c) 2002-2011 XPoint Audiovisual CC.  All rights reserved.
//  
//  Software License Agreement
// 
//  XPoint Audiovisual (XPAV) is supplying this software for use solely and exclusively with 
//  XPAV's microcontroller- and instance-based products. The software is owned by XPAV 
//  and/or its suppliers, and is protected under applicable copyright laws. You may not 
//  combine this software with "viral" open-source software in order to form a larger program.
// 
//  THIS SOFTWARE IS PROVIDED "AS IS" AND WITH ALL FAULTS.
//  NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT
//  NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//  A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. XPAV SHALL NOT, UNDER ANY
//  CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR CONSEQUENTIAL
//  DAMAGES, FOR ANY REASON WHATSOEVER.
// 
//*****************************************************************************
#endregion
#region Using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Xml;
using System.Xml.XPath;
#endregion

namespace XPointAudiovisual.Xtend {
    public partial class frmMain : Form {

        public frmMain() {
            InitializeComponent();
            XIO.Open();
        }

        // the Found buffer returned by Finder will have all devices found since the start of this cycle.
        // this may cause duplicates to appear in the display.
        // avoid inserting duplicates, as below, or resync the entire display every update.
        delegate void AddNodesCallback( TreeNode t );
        private void AddNode( TreeNode t ) {
            if ( !this.trvwFoundDevices.InvokeRequired )
                trvwFoundDevices.Nodes.Add( t );
            else
                this.Invoke( new AddNodesCallback( AddNode ), new object[] { t } );
        }
        private void UpdateDevicesDisplay() {
            IEnumerable<XHProxy> devices = XIO.Finder.Found();
            foreach ( XHProxy fh in devices ) {
                if  (trvwFoundDevices.Nodes.Find( fh.ToString(), false ).Length == 0 ) {
                    TreeNode t = new TreeNode( fh.ToString() );
                    t.Name = fh.ToString();
                    t.Tag = fh;
                    foreach ( var c in fh.Cards ) {
                        TreeNode cn = new TreeNode( c.ToString() );
                        t.Nodes.Add( cn );
                        cn.Tag = c;
                        foreach ( var e in c.Elems ) {
                            TreeNode en = new TreeNode( e.ToString() );
                            cn.Nodes.Add( en );
                            en.Tag = e;
                            foreach ( var m in e.Maps ) {
                                TreeNode mn = new TreeNode( m.ToString() );
                                en.Nodes.Add( mn );
                                mn.Tag = m;
                            }
                        }
                    }
                    AddNode( t );
                }
            }
        }

        // set the Finder to report on progress and completion.
        // also set the total period for which to scan the network.
        private void frmMain_Load( object sender, EventArgs e ) {
            //finder.SetScanPeriod( 500 );
            XIO.Finder.OnProgressChanged( new ProgressChangedEventHandler( Finder_ProgressChanged ) );
            XIO.Finder.OnCompleted( new RunWorkerCompletedEventHandler( Finder_RunWorkerCompleted ) );
            //updater.OnProgressChanged( new ProgressChangedEventHandler( Updater_ProgressChanged ) );
            //updater.OnCompleted( new RunWorkerCompletedEventHandler( Updater_RunWorkerCompleted ) );
        }

        private void frmMain_FormClosing( object sender, FormClosingEventArgs e ) {
            XIO.Close();
        }

        // make use of the percent progress reported by Finder to the Handler.
        delegate void SetProgressCallback( int progress );
        private void UpdateProgress( int pct ) {
            if ( !this.prgbrFind.InvokeRequired )
                prgbrFind.Value = pct < 0 ? 0 : pct > 100 ? 100 : pct;
            else
                this.Invoke( new SetProgressCallback( UpdateProgress ), new object[] { pct } );
        }
        void Finder_ProgressChanged( object sender, ProgressChangedEventArgs e ) {
            UpdateProgress( e.ProgressPercentage );
            UpdateDevicesDisplay();
        }

        // update progress to 100% done, and examine and notify user of error status if any.
        void Finder_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e ) {
            UpdateProgress( 100 );
            if ( e.Cancelled ) { }
            else if ( e.Result != null ) {
                Thread.Sleep( 200 );
                UpdateDevicesDisplay();
            }
            else
                MessageBox.Show(
                    "Could not create required aSocketsList. This is either\n"
                    + "a permission problem or another application\n"
                    + "may already be using the required ports.",
                    "ErrorException", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }

        // ignored by Finder if it is not currently busy.
        private void btnCancel_Click( object sender, EventArgs e ) {
        }

        // start running Finder unless it is already running.
        // (Finder will just ignore Start if already busy, but tell the user what's happening)
        private void btnRefresh_Click( object sender, EventArgs e ) {
            if ( XIO.Finder.IsBusy() )
                MessageBox.Show( "Still busy finding Xtend devices.", "Sorry ...", MessageBoxButtons.OK );
            else {
                trvwFoundDevices.Nodes.Clear();
                XIO.Finder.Start();
            }
        }

        private void btnResynch_Click( object sender, EventArgs e ) {
            if ( XIO.Finder.IsBusy() )
                MessageBox.Show( "Still busy finding Xtend devices.", "Sorry ...", MessageBoxButtons.OK );
            else {
                trvwFoundDevices.Nodes.Clear();
                XIO.Finder.Restart();
            }
        }




        // make use of the percent progress reported by Finder to the Handler.
        void Updater_ProgressChanged( object sender, ProgressChangedEventArgs e ) {
            int pct = e.ProgressPercentage;
            prgbrUpdate.Value = pct < 0 ? 0 : pct > 100 ? 100 : pct;
        }

        // update progress to 100% done, and examine and notify user of error status if any.
        void Updater_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e ) {
            prgbrUpdate.Value = 100;
            if ( e.Cancelled ) { }
            else if ( e.Result != null ) { }
            else
                MessageBox.Show(
                    "Could not update application", "ErrorException", MessageBoxButtons.OK, MessageBoxIcon.Error );
        }

        private void btnUpdate_Click( object sender, EventArgs e ) {
            if ( XUpdateServer.Instance.IsBusy() )
                MessageBox.Show( "Still busy updating Xtend device.", "Sorry ...", MessageBoxButtons.OK );
            else if ( trvwFoundDevices.SelectedNode == null ) {
                string sp = "There's a small problem ...";
                string msg = trvwFoundDevices.Nodes.Count != 0 ? "Please select a remote device for updating."
                    : "Please refresh the finder to get devices for updating.";
                MessageBox.Show( msg, sp, MessageBoxButtons.OK );
            }
            else {
                var ofd = new OpenFileDialog();
                ofd.InitialDirectory = Application.StartupPath;
                ofd.Filter = "Binary files (*.bin)|*.bin";
                if ( ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK ) {
                    string ipAddress = ( (XHProxy)( trvwFoundDevices.SelectedNode.Tag ) ).IpAddress.ToString();
                    string macAddress = ( (XHProxy)( trvwFoundDevices.SelectedNode.Tag ) ).MacAddress.ToString();
                    string[] nodeData = trvwFoundDevices.SelectedNode.Text.Split( new string[] { "(", "/", ")" }, StringSplitOptions.RemoveEmptyEntries );
                    XUpdateServer.Instance.Update( ipAddress, macAddress, ofd.FileName );
                }
            }
        }

        private delegate void WrappedLambda();
        private void DoWrappedLambda( WrappedLambda aLambda ) {
            try { aLambda(); }
            catch ( Exception exc ) {
                MessageBox.Show(
                    "There has been an error during this operation. \r\n\r\n" + exc.Message,
                    "Sorry about this ...", MessageBoxButtons.OK, MessageBoxIcon.Exclamation );
            }
        }

        delegate void PutText( string Text );
        private void btnTest_Click( object sender, EventArgs e ) {
            PutText pt = ( txt ) => { txtbxOutput.Text = txt + txtbxOutput.Text; };
            DoWrappedLambda( () => { new XIOTester( ( s ) => { this.Invoke( pt, s ); } ).TestGood(); } );


        }

        private void btnDebugOn_Click( object sender, EventArgs e ) {
            DoWrappedLambda( () => { new XIOTester().SetDebug( true ); } );
        }

        private void btnDebugOff_Click( object sender, EventArgs e ) {
            DoWrappedLambda( () => { new XIOTester().SetDebug( false ); } );
        }

        bool dtrActive = false;
        private void btnDtr_Click( object sender, EventArgs e ) {
            DoWrappedLambda( () => { new XIOTester().SetDtr( ( dtrActive = !dtrActive ) ); } );
        }

        bool rtsActive = false;
        private void btnRts_Click( object sender, EventArgs e ) {
            DoWrappedLambda( () => { new XIOTester().SetRts( ( rtsActive = !rtsActive ) ); } );
        }

        private void btnTestBad_Click( object sender, EventArgs e ) {
            DoWrappedLambda( () => { new XIOTester().TestBad(); } );
        }

        private void btnClear_Click( object sender, EventArgs e ) {
            txtbxOutput.Text = "";
        }

        private void SetFullRS232( bool isFull ) {
            DoWrappedLambda( () => { new XIOTester().SetFullRS232( isFull ); } );
        }
        private void btnFullRS232_Click( object sender, EventArgs e ) { SetFullRS232( true ); }
        private void btnRxOnlyRS232_Click( object sender, EventArgs e ) { SetFullRS232( false ); }

        private void btnCopyMac_Click( object sender, EventArgs e ) {
            var node = trvwFoundDevices.SelectedNode;
            if ( node != null ) {
                var s = node.ToString();
                s = s.Split( new string[ 1 ] { "/" }, StringSplitOptions.RemoveEmptyEntries )[ 1 ];
                s = s.Replace( ":", "" ).Trim();
                s = "*" + s.Substring( 0, 6 ) + "-" + s.Substring( 6, 6 ) + "*";
                Clipboard.SetText( s );
            }
        }

        private bool relay1Active = false, relay2Active = false;
        private void SetRelayState( int aCard, int aRelay, bool aState ) {
            DoWrappedLambda( () => { new XIOTester().SetRelayState( aCard, aRelay, aState ); } );
        }
        private void btnRelay1_Click( object sender, EventArgs e ) {
            SetRelayState( CurrentSelectedCard, 1, relay1Active = !relay1Active );
        }
        private void btnRelay2_Click( object sender, EventArgs e ) {
            SetRelayState( CurrentSelectedCard, 2, relay2Active = !relay2Active );
        }

        private int CurrentSelectedCard = 0;
        private int CurrentSelectedElement = 0;
        private int CurrentSelectedMap = 0;

        private bool GetItemIndex( string anItemType, string aNodeText, out int anIndex ) {
            int result = 0;
            if ( aNodeText.Contains( anItemType ) ) {
                string[] parts = aNodeText.Split( new string[] { " " }, StringSplitOptions.RemoveEmptyEntries );
                if ( parts[ 0 ].Contains( anItemType ) )
                    if ( int.TryParse( parts[ 1 ], out result ) ) {
                        anIndex = result;
                        return true;
                    }
            }
            anIndex = result;
            return false;
        }

        private void trvwFoundDevices_AfterSelect( object sender, TreeViewEventArgs e ) {
            CurrentSelectedCard = CurrentSelectedElement = CurrentSelectedMap = 0;
            XMBase m = null;
            var x = e.Node;
            if ( x.Text.Contains( "(Map" ) ) {
                m = (XMBase)x.Tag;
                GetItemIndex( "Map", x.Text, out CurrentSelectedMap );
                x = x.Parent;
            }
            lblCurrentMap.Text = "Map: " + CurrentSelectedMap.ToString();
            if ( x.Text.Contains( "(Elem" ) ) {
                GetItemIndex( "Elem", x.Text, out CurrentSelectedElement );
                x = x.Parent;
            }
            lblCurrentElement.Text = "Elem: " + CurrentSelectedElement.ToString();
            if ( x.Text.Contains( "(Card" ) ) {
                GetItemIndex( "Card", x.Text, out CurrentSelectedCard );
                x = x.Parent;
            }
            lblCurrentCard.Text = "Card: " + CurrentSelectedCard.ToString();
            if ( x.Tag != XIOTester.CurrentHub ) {
                XIOTester.PreviousHub = XIOTester.CurrentHub;
                XIOTester.CurrentHub = (XHProxy)( x.Tag );
            }
            if ( ( m != null ) && ( m.MapType == EXMapType.AnalogInput ) ) {
                //var m = (XMAnalogInput)XIOTester.GetMap(CurrentSelectedCard,CurrentSelectedElement,CurrentSelectedMap);
                //((XMAnalogInput)m).OnChange += ( s, v ) => { lblValueDisplay.Text =  v.ToString(); };

                PutText pt = ( txt ) => { lblValueDisplay.Text = txt.ToString(); };
                var map = (XMAnalogInput)m;
                double factor = map.Range / map.Calibration;
                map.OnChange += ( s, v ) => {
                    double f = factor * v;
                    this.Invoke( pt, f.ToString( "F2" ) + "V" );
                };

            }
        }

        private void btnLoadCfg_Click( object sender, EventArgs e ) {
            XIOTester.CES1CfgXPathDoc = null;
            XIOTester.CES1CfgFileName = "";
            var fd = new OpenFileDialog();
            fd.InitialDirectory = "c:\\ProgramData\\XPointAudiovisual\\Configuration";
            fd.Filter = "XML files (*.xml)|*.xml";
            if ( fd.ShowDialog() == DialogResult.OK ) {
                try {
                    XIOTester.CES1CfgFileName = fd.FileName;
                    XIOTester.CES1CfgXPathDoc = new XPathDocument( fd.FileName );
                    XIOTester.LoadDefaultSerialCfg();
                }
                catch ( Exception ex ) {
                    MessageBox.Show( "Error: Could not read file from disk. Original error: " + ex.Message );
                }
            }

        }

        private void btnMonitorAnalogs_Click( object sender, EventArgs e ) {
            XCProxy card = XIOTester.GetCard( CurrentSelectedCard );
            if ( card == null )
                MessageBox.Show( "Cannot do this because no card has been selected.", "Sorry ..." );
            else if ( card.CardType == XCardType.GPIO_8AnalogIn_12V_ADC10_M0202_v0100 ) {
                for ( int i = 1; i <= 8; ++i ) {
                    var elem = card.Elems[ i ];
                    //var map = (XMAnalogInput)( elem.Maps[ (int)EXMapType.AnalogInput ] );
                    var map = (XMAnalogInput)( elem.Maps[ 0 ] );
                    PutText pt = ( txt ) => { lblValueDisplay.Text = txt.ToString(); };
                    double factor = map.Range / map.Calibration;
                    map.OnChange += ( s, v ) => {
                        double f = factor * v;
                        this.Invoke( pt, f.ToString( "F2" ) + "V" );
                    };
                }
            }
        }

        bool IsCollectingSamples = false;
        XCProxy CollectingCard = null;
        List<object> CollectedSamples;

        void AMRU_Handler( object aSender, AnalogMonitorReading aReading ) {
            CollectedSamples.Add( aReading );
        }
        void AMFSU_Handler( object aSender, AnalogMonitorState aState ) {
            CollectedSamples.Add( aState );
        }

        bool IsMonitoringFDM = false;
        void AMDU_Handler( object aSender, AnalogMonitorDisturbance anEvent ) {
            anEvent.Process();
            string txt =
                anEvent.ChannelNumber.ToString() + "    --    " 
                + anEvent.Average.ToString() + "   "
                + anEvent.NoiseLevel.ToString() + "   "
                + anEvent.Tracking.ToString() + "\r\n";
            PutText pt = ( t ) => { txtbxOutput.Text = t + txtbxOutput.Text; };
            this.Invoke( pt, txt );
        }

        private void btnFDMCollect_Click( object sender, EventArgs e ) {
            // collect data from FDM card configured to report sampled data continuously.
            // decide whether this click is to start or stop collecting.
            if ( !IsCollectingSamples ) {
                // if not currently collecting then must be a request to start
                //      must have an FDM card selected.
                //      enable event handlers for both integration samples and slow-filtered values.
                XCProxy card = XIOTester.GetCard( CurrentSelectedCard );
                if ( card == null ) {
                    MessageBox.Show( "Cannot do this because no card has been selected.", "Sorry ..." );
                    return;
                }
                else if ( card.CardType != XCardType.ES_Fibre_Disturbance_4Sensor_M0401_v0100 ) {
                    MessageBox.Show( "Cannot do this because this card does not support streaming data collection.", "Sorry ..." );
                    return;
                }
                IsCollectingSamples = !IsCollectingSamples;
                CollectingCard = card;
                btnFDMCollect.ForeColor = Color.Red;
                CollectedSamples = new List<object>();
                for ( int i = 1; i <= 4; ++i ) {
                    var elem = CollectingCard.Elems[ i ];
                    var map = (XMAnalogMonitor)( elem.Maps[ 0 ] );
                    map.OnReadingsUpdate += AMRU_Handler;
                    map.OnFilteredStateUpdate += AMFSU_Handler;
                }
            }
            else {
                // else if currently collecting then this is a request to stop
                //      disable event handers.
                //      parse all saved data.
                //      save parsed data to file.
                IsCollectingSamples = !IsCollectingSamples;
                btnFDMCollect.ForeColor = Color.Black;
                for ( int i = 1; i <= 4; ++i ) {
                    var elem = CollectingCard.Elems[ i ];
                    var map = (XMAnalogMonitor)( elem.Maps[ 0 ] );
                    map.OnReadingsUpdate -= AMRU_Handler;
                    map.OnFilteredStateUpdate -= AMFSU_Handler;
                }
                UInt64 Timestamp = 0;
                StringBuilder s = new StringBuilder();
                foreach ( var v in CollectedSamples ) {
                    if ( v is AnalogMonitorReading ) {
                        AnalogMonitorReading r = (AnalogMonitorReading)v;
                        r.Process();
                        if ( r.Timestamp != Timestamp ) {
                            s.Append( "\n" );
                            Timestamp = r.Timestamp;
                            s.Append( Timestamp.ToString() );
                            s.Append( "," );
                        }
                        s.Append( ( r.Value * 256 ).ToString() );
                        s.Append( "," );
                    }
                    else if ( v is AnalogMonitorState ) {
                        AnalogMonitorState r = (AnalogMonitorState)v;
                        r.Process();
                        s.Append( r.LatchedValue.ToString() + "," );
                        s.Append( r.AverageValue.ToString() + "," );
                        s.Append( ( r.AverageValue + 2 * r.NoiseLevel ).ToString() + "," );
                        s.Append( ( r.AverageValue - 2 * r.NoiseLevel ).ToString() + "," );
                    }
                }
                CollectedSamples.Clear();
                CollectedSamples = null;
            }
        }

        private void btnFDMMonitor_Click( object sender, EventArgs e ) {

            // collect data from FDM card configured to report sampled data continuously.
            // decide whether this click is to start or stop collecting.
            if ( !IsMonitoringFDM ) {
                // if not currently collecting then must be a request to start
                //      must have an FDM card selected.
                //      enable event handlers for both integration samples and slow-filtered values.
                XCProxy card = XIOTester.GetCard( CurrentSelectedCard );
                if ( card == null ) {
                    MessageBox.Show( "Cannot do this because no card has been selected.", "Sorry ..." );
                    return;
                }
                IsMonitoringFDM = !IsMonitoringFDM;
                CollectingCard = card;
                btnFDMMonitor.ForeColor = Color.Red;
                for ( int i = 1; i <= 4; ++i ) {
                    var elem = CollectingCard.Elems[ i ];
                    var map = (XMAnalogMonitor)( elem.Maps[ 0 ] );
                    map.OnDisturbance += AMDU_Handler;
                }
            }
            else {
                // else if currently collecting then this is a request to stop
                //      disable event handers.
                //      parse all saved data.
                //      save parsed data to file.
                IsMonitoringFDM = !IsMonitoringFDM;
                btnFDMMonitor.ForeColor = Color.Black;
                for ( int i = 1; i <= 4; ++i ) {
                    var elem = CollectingCard.Elems[ i ];
                    var map = (XMAnalogMonitor)( elem.Maps[ 0 ] );
                    map.OnDisturbance -= AMDU_Handler;
                }
            }
        }

    }
}

