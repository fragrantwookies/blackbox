﻿using System;
using System.ServiceProcess;
using BlackBoxEngine;
using eThele.Essentials;

namespace RioSoftService
{
    public partial class RioSoftServerService : ServiceBase
    {
        private Service service = null;

        private AppDomain currentDomain;

        public RioSoftServerService()
        {
            InitializeComponent();

            eventLog.Source = "RioSoftServerService";

            currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += currentDomain_UnhandledException;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            if(currentDomain != null)
                currentDomain.UnhandledException -= currentDomain_UnhandledException;
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                if (service == null)
                        service = new Service();

                if(!service.Start())
                {
                    eventLog.WriteEntry("Invalid install, service will not start.");
                    Stop();
                }
            }
            catch (Exception ex)
            {
                eventLog.WriteEntry(string.Format("Exception on RioSoftServerService start: {0}", ex.Message));
            }
        }

        protected override void OnStop()
        {
            try
            {
                if (service != null)
                {
                    service.Stop();
                    service.Dispose();
                    service = null;
                }
            }
            catch (Exception ex)
            {
                eventLog.WriteEntry(string.Format("Exception on RioSoftServerService stop: {0}", ex.Message));
            }
        }

        void currentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exceptions.ExceptionsManager.Save();
                eventLog.WriteEntry(string.Format("Unhandled exception in RioSoftServerService: {0}", ((Exception)e.ExceptionObject).Message));
            }
            catch
            {
                eventLog.WriteEntry("Unhandled exception in RioSoftServerService");
            }
        }
    }
}
