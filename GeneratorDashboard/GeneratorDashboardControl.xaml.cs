﻿using eNervePluginInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GeneratorDashboard
{
    /// <summary>
    /// Interaction logic for GeneratorDashboardControl.xaml
    /// </summary>
    public partial class GeneratorDashboardControl : UserControl, IUserInterfacePlugin
    {
        public GeneratorDashboardControl()
        {
            InitializeComponent();
        }

        #region Properties
        public string PluginName
        {
            get { return Assembly.GetAssembly(this.GetType()).GetCustomAttribute<AssemblyTitleAttribute>().Title; }
        }

        public string Description
        {
            //get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description; }
            get { return Assembly.GetAssembly(this.GetType()).GetCustomAttribute<AssemblyDescriptionAttribute>().Description; }
        }

        public string Author
        {
            //get { return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company; }
            get { return Assembly.GetAssembly(this.GetType()).GetCustomAttribute<AssemblyCompanyAttribute>().Company; }
        }

        public string Version
        {
            //get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
            get { return Assembly.GetAssembly(this.GetType()).GetName().Version.ToString(); }
        }

        private string fileName;
        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }
        #endregion Properties

        #region Connect
        private bool connected = false;
        public void Start()
        {
            try
            {
                gridMain.DataContext = this;
                StartGuages();
                connected = true;
            }
            catch(Exception ex)
            {
                if(Error != null)
                    Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex));
            }
        }

        public void Stop()
        {
            try
            {
                StopGuages();
                connected = false;
            }
            catch (Exception ex)
            {
                if (Error != null)
                    Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex));
            }
        }
        #endregion Connect

        #region Events
        public event EventHandler<PluginErrorEventArgs> Error;
        #endregion Events

        #region Guages
        private Timer timer;
        private Random rand;

        private void StartGuages()
        {
            if (timer == null)
            {
                timer = new Timer();
                timer.Elapsed += timer_Elapsed;
            }

            timer.Interval = 1000;
            timer.Start();

            rand = new Random();
        }

        private void StopGuages()
        {
            if (timer != null)
            {
                timer.Stop();
                timer.Elapsed -= timer_Elapsed;
                timer.Dispose();
                timer = null;
            }
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            UpdateGuages();
        }

        private void UpdateGuages()
        {
            if (this.CheckAccess())
            {
                int hour = DateTime.Now.Hour % 12;
                double minute = (double)DateTime.Now.Minute / 60 * 12;
                double second = (double)DateTime.Now.Second / 60 * 12;

                asnHour.Value = hour;
                asnMinute.Value = minute;
                asmSecond.Value = second;
                
                lslbTemp.Value = rand.Next(-40, 120);
            }
            else
                this.Dispatcher.Invoke(UpdateGuages);
        }
        #endregion Guages
    }
}
