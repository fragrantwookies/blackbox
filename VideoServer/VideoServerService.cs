﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using eThele.Essentials;

namespace VideoServer
{
    partial class VideoServerService : ServiceBase
    {
        private AppDomain currentDomain;
        private Service service;

        public VideoServerService()
        {
            InitializeComponent();

            eventLog.Source = "RioSoftVideoServerService";

            currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += CurrentDomain_UnhandledException;
            service = new Service();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);

            if (currentDomain != null)
                currentDomain.UnhandledException -= CurrentDomain_UnhandledException;
        }

        protected override void OnStart(string[] args)
        {
            service.Start();
        }

        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            service.Stop();
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                Exceptions.ExceptionsManager.Save();
                eventLog.WriteEntry(string.Format("Unhandled exception in RioSoftServerService: {0}", ((Exception)e.ExceptionObject).Message));
            }
            catch
            {
                eventLog.WriteEntry("Unhandled exception in RioSoftServerService");
            }
        }
    }
}
