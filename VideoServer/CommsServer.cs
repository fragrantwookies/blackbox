﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Reflection;

using BlackBoxEngine;
using eThele.Essentials;
using eNerve.DataTypes;

namespace VideoServer
{
    public class CommsServer : IDisposable
    {
        public delegate void GetCamerasEventHandler(EventArgs e);
        public GetCamerasEventHandler GetCamerasEvent;
        private static Client client;
        private static Server server;
        private IPEndPoint videoServerDetails;

        public CommsServer()
        {
        }

        public void SendMessage(SendMessageEventArgs e)
        {
            if (client.ConnectionStatus == Client.ConnectState.Connected)
            {
                client.SendMessage(e.MessageType, e.Message);
            }
        }

        private void Client_ConnectionStatusChanged(object sender, Client.ConnectState connectionState)
        {
            switch (connectionState)
            {
                case Client.ConnectState.Connected:
                    client.SendMessage(MessageType.VideoServerOnline, videoServerDetails);
                    break;
                case Client.ConnectState.Disconnected:
                    Exceptions.ExceptionsManager.Add("Client_ConnectionStatusChanged", "RIO server stopped - waiting to restart.", null, ExceptionPriority.Low);
                    break;
            }
        }

        /// <summary>
        /// Commands from RIO server. Add camera / list cameras / config
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Client_OnObjectMessageReceived(object sender, CommsNode.ObjectMessageEventArgs e)
        {
            try
            {
                switch (e.Message_Type)
                {
                    case MessageType.VideoCamerasRequest:
                        {
                            OnGetCameras();
                        }
                        break;
                    default:
                        break;

                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name,
                    MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private void OnGetCameras()
        {
            GetCamerasEvent?.Invoke(new EventArgs());
        }

        private void Connect()
        {
            client = new Client();
            var serverDetails = Client.GetServerDetails();
            if (IPAddress.TryParse(serverDetails.IpAddress, out IPAddress address))
            {
                client.ConnectionStatusChanged += Client_ConnectionStatusChanged;
                client.OnObjectMessageReceived += Client_OnObjectMessageReceived;
                client.Connect(serverDetails.IpAddress, serverDetails.PortNo);
            }
        }

        /// <summary>
        /// Only RIO Video Windows. Ensure camera streams die gracefully (should be handled in CameraServer)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Server_ClientDisconnected(object sender, TcpClient e)
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Messages from RIO Video, such as record, export, LPR config, etc.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Server_OnObjectMessageReceived(object sender, CommsNode.ObjectMessageEventArgs e)
        {
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        public bool Start()
        {
            bool result = false;

            try
            {
                if (server == null)
                {
                    server = new Server(0);
                    videoServerDetails = new IPEndPoint(NetworkTools.MyIPAddress(), server.Port);
                    server.OnObjectMessageReceived += Server_OnObjectMessageReceived;
                    server.ClientDisconnected += Server_ClientDisconnected;
                }

                result = true;
                Connect();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        public bool Stop()
        {
            bool result = false;

            try
            {
                if (server != null)
                {
                    server.ClientDisconnected -= Server_ClientDisconnected;
                    server.OnObjectMessageReceived -= Server_OnObjectMessageReceived;
                    server.Dispose();
                    server = null;
                }
                if (client != null)
                {
                    client.ConnectionStatusChanged -= Client_ConnectionStatusChanged;
                    client.OnObjectMessageReceived -= Client_OnObjectMessageReceived;
                    client.Disconnect();
                }

                Settings.Selfdesctruct();

                result = true;
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }

            return result;
        }

        protected void Dispose(bool disposing)
        {
            Stop();
        }
    }
}
