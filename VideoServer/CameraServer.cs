﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.ServiceModel.Discovery;
using System.Timers;
using System.Xml;

using eNerve.DataTypes;
using eNervePluginInterface;
using Microsoft.FSharp.Core;
using odm.core;
using onvif.services;

namespace VideoServer
{
    class CameraServer
    {
        public delegate void SendMessageEventHandler(SendMessageEventArgs e);
        public SendMessageEventHandler SendMessageEvent;

        private List<CameraPlugin> cameras;
        private Timer cameraTimer;
        private List<string> cameraUris;
        private readonly FindCriteria criteria;
        DiscoveryClient discoveryClient;
        static string localIP;
        INvtSession session;
        private List<int> usedPorts;


        public CameraServer()
        {
            localIP = eThele.Essentials.NetworkTools.MyIPAddress().ToString();
            cameraTimer = new Timer(252000)
            {
                Enabled = false
            };
            cameraTimer.Elapsed += CameraTimer_Elapsed;
        }

        private void CameraTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (cameraTimer != null)
                cameraTimer.Stop();
            discoveryClient.FindAsync(criteria);
            //IPCameraFactory.DiscoverDevices();
            if (cameraTimer != null)
                cameraTimer.Start();
        }

        /// <summary>
        /// Add all discovered cameras. In the next release, only 'allocated' cameras are to be specified from RIO, with discovery only providing a list.
        /// </summary>
        public void Start()
        {
            UdpDiscoveryEndpoint discoveryEndpoint = new UdpDiscoveryEndpoint(DiscoveryVersion.WSDiscoveryApril2005);

            discoveryClient = new DiscoveryClient(discoveryEndpoint);
            discoveryClient.FindProgressChanged += DiscoveryClient_FindProgressChanged;
            ServicePointManager.Expect100Continue = false;
            FindCriteria criteria = new FindCriteria
            {
                Duration = TimeSpan.MaxValue
            };
            criteria.ContractTypeNames.Add(new XmlQualifiedName("NetworkVideoTransmitter", "http://www.onvif.org/ver10/network/wsdl"));

            discoveryClient.FindAsync(criteria);
            /*IPCameraFactory.DeviceDiscovered += IPCameraFactory_DeviceDiscovered;
            IPCameraFactory.DiscoverDevices();*/

            cameraUris = new List<string>();
            cameras = new List<CameraPlugin>();
        }

        public void Stop()
        {
            if (cameraTimer != null)
            {
                cameraTimer.Stop();
                cameraTimer.Elapsed -= CameraTimer_Elapsed;
                cameraTimer.Dispose();
            }
            discoveryClient.FindProgressChanged += DiscoveryClient_FindProgressChanged;
            //IPCameraFactory.DeviceDiscovered -= IPCameraFactory_DeviceDiscovered;
        }

        internal void SendCameras()
        {
            if (cameras != null && cameras.Count > 0)
            {
                lock (cameras)
                {
                    foreach (CameraPlugin camera in cameras)
                    {
                        OnSendMessage(MessageType.VideoCameraAdded, camera);
                    }
                }
            }
        }

        private void DiscoveryClient_FindProgressChanged(object sender, FindProgressChangedEventArgs e)
        {

            bool camera = false;
            foreach (var typeName in e.EndpointDiscoveryMetadata.ContractTypeNames)
            {
                if (typeName.Name == "NetworkVideoTransmitter")
                {
                    camera = true;
                    break;
                }
            }
            if (camera)
            {
                if (e.EndpointDiscoveryMetadata.ListenUris != null && e.EndpointDiscoveryMetadata.ListenUris.Count > 0)
                {
                    Uri uri = e.EndpointDiscoveryMetadata.ListenUris[0];
                    if (cameraTimer != null)
                        cameraTimer.Stop();
                    if (!uri.AbsoluteUri.StartsWith("https"))
                    {
                        if (cameraUris == null)
                            cameraUris = new List<string>();
                        if (!cameraUris.Contains(uri.AbsoluteUri))//Added to avoid connecting to the same camera multiple times.
                        {
                            lock (cameras)
                            {
                                CameraPlugin cameraPlugin = new CameraPlugin
                                {
                                    Admin = false,
                                    Host = null,
                                    Name = uri.AbsoluteUri
                                };
                                cameraPlugin.Streams = new List<StreamDefinition>();
                                NetworkCredential creds = new NetworkCredential("admin", "admin");
                                NvtSessionFactory sessionFactory = new NvtSessionFactory(creds);
                                session = sessionFactory.CreateSession(uri);

                                bool found = false;
                                while (!found)
                                {

                                    try
                                    {
                                        var profilesF = session.GetProfiles();
                                        Profile[] profiles = Microsoft.FSharp.Control.FSharpAsync.RunSynchronously(profilesF, timeout: FSharpOption<int>.None,
                                            cancellationToken: FSharpOption<System.Threading.CancellationToken>.None);

                                        if (profiles.Length > 0)
                                        {
                                            found = true;
                                        }

                                        foreach (Profile item in profiles)
                                        {
                                            if (item.videoEncoderConfiguration != null)
                                            {
                                                StreamSetup streamSetup = new StreamSetup() { transport = new Transport() { protocol = TransportProtocol.rtsp } };
                                                var mediaUriF = session.GetStreamUri(streamSetup, item.token);
                                                MediaUri mediaUri = Microsoft.FSharp.Control.FSharpAsync.RunSynchronously(mediaUriF, timeout: FSharpOption<int>.None,
                                                            cancellationToken: FSharpOption<System.Threading.CancellationToken>.None);
                                                var streamDef = new StreamDefinition(item.name, new Resolution(item.videoEncoderConfiguration.resolution.height,
                                                    item.videoEncoderConfiguration.resolution.width), new Uri(mediaUri.uri));
                                                int index = cameraPlugin.Streams.BinarySearch(streamDef);
                                                if (index < 0)
                                                    cameraPlugin.Streams.Insert(~index, streamDef);
                                            }
                                        }
                                        if (found)
                                        {
                                            var deviceInfoF = session.GetScopes();
                                            Scope[] scopes = Microsoft.FSharp.Control.FSharpAsync.RunSynchronously(deviceInfoF, timeout: FSharpOption<int>.None,
                                                            cancellationToken: FSharpOption<System.Threading.CancellationToken>.None);
                                            foreach (var scope in scopes)
                                            {
                                                string[] strings = scope.scopeItem.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                                                if (strings.Length > 3)
                                                {
                                                    if (strings[2] == "name")
                                                    {
                                                        if (!string.IsNullOrWhiteSpace(strings[3]))
                                                            cameraPlugin.Properties["DisplayName"] = strings[3];
                                                        break;
                                                    }
                                                }
                                            }
                                            
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        if (creds.Password == "admin")
                                            creds = new NetworkCredential("admin", "123456");
                                        /*else if (creds.Password == "123456")
                                            creds.Password = "l3tm31n";*/
                                        else
                                        {
                                            found = true; //Give up.
                                        }
                                        sessionFactory = new NvtSessionFactory(creds);
                                        session = sessionFactory.CreateSession(uri);
                                    }
                                }
                                cameraPlugin.Properties["Uri"] = uri.ToString();
                                if (cameras == null)
                                    cameras = new List<CameraPlugin>();
                                if (cameraPlugin.Streams.Count > 0)
                                {
                                    cameras.Add(cameraPlugin);
                                    OnSendMessage(MessageType.VideoCameraAdded, cameraPlugin);
                                    cameraUris.Add(uri.AbsoluteUri);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This will only send a list to RIO. RIO will then send back the selected cameras to be allocated to this specific server.
        /// ABANDONED due to resource usage, stability issues, and license cost.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /*private void IPCameraFactory_DeviceDiscovered(object sender, DiscoveryEventArgs e)
        {
            if (cameraTimer != null)
                cameraTimer.Stop();
            if (!e.Device.Uri.AbsoluteUri.StartsWith("https"))
            {
                if (cameraUris == null)
                    cameraUris = new List<string>();
                if (!cameraUris.Contains(e.Device.Uri.AbsoluteUri))//Added to avoid connecting to the same camera multiple times.
                {
                    IPCamera tempCamera = new IPCamera(e.Device.Host, string.Empty, string.Empty, CameraTransportType.UDP, true);
                    if (tempCamera.AvailableStreams.Count > 0)
                    //                    
                    {
                        Uri uri = e.Device.Uri;
                        AddCamera(tempCamera, uri);
                        
                    }
                    else
                    {
                        tempCamera.Stop();
                        tempCamera.Disconnect();
                        tempCamera.Dispose();
                        OzekiCamera ozekiCamera = new OzekiCamera(e.Device.Host + ":80;Username=admin;Password=admin;Transport=UDP;");
                        ozekiCamera.Start();
                        //if (ozekiCamera.AvailableStreams.Count > 0)
                        ozekiCamera.AvailableStreamsChanged += OzekiCamera_AvailableStreamsChanged;
                        ozekiCamera.RtspUriReceived += OzekiCamera_RtspUriReceived;
                    }
                }
            }
            if (cameraTimer != null)
                cameraTimer.Start();
        }

        private void OzekiCamera_RtspUriReceived(object sender, CameraRtspUriEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(e.RtspURI))
            {
                IPCamera tempCamera = new IPCamera(string.Format("http://{0}/onvif/device_service", e.RtspURI.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries)[1]), "admin", "admin", CameraTransportType.UDP, false);
                Uri uri = new Uri(e.RtspURI);
                AddCamera(tempCamera, uri);
            }
        }*/

        /*private void AddCamera(IIPCamera ozekiCamera, Uri uri)
        {
            CameraPlugin cameraPlugin = new CameraPlugin
            {
                Admin = false,
                Host = null,
                Name = uri.AbsoluteUri
            };
            cameraPlugin.Streams = new List<StreamDefinition>();
            foreach (var item in ozekiCamera.AvailableStreams)
            {
                if (item.VideoEncoding != null && item.VideoEncoding.Resolution != null)
                {
                    var streamDef = new StreamDefinition(item.Name, new Resolution(item.VideoEncoding.Resolution.Height, item.VideoEncoding.Resolution.Width), item.RtspUri);
                    int index = cameraPlugin.Streams.BinarySearch(streamDef);
                    if (index < 0)
                        cameraPlugin.Streams.Insert(~index, streamDef);
                }
            }
            cameraPlugin.Properties["Uri"] = uri.ToString();
            if (cameras == null)
                cameras = new List<CameraPlugin>();
            cameras.Add(cameraPlugin);
            OnSendMessage(MessageType.VideoCameraAdded, cameraPlugin);
            cameraUris.Add(uri.AbsoluteUri);
            ozekiCamera.Disconnect();
            ozekiCamera.Stop();
            ozekiCamera.Dispose();
        }*/

        /*private void OzekiCamera_AvailableStreamsChanged(object sender, EventArgs e)
        {
            if (sender is OzekiCamera camera)
            {
                camera.AvailableStreamsChanged -= OzekiCamera_AvailableStreamsChanged;
                AddCamera(camera, new Uri(camera.DomainHost.Replace(":80", "")));
            }
        }*/

        private void OnSendMessage(MessageType type, object data)
        {
            SendMessageEvent?.Invoke(new SendMessageEventArgs(type, data));
        }

        /// <summary>
        /// Find first available TCP port after specified port number.
        /// </summary>
        /// <param name="port"></param>
        /// <returns></returns>
        private int GetNextPort(int port = 8554)
        {
            if (usedPorts == null)
            {
                usedPorts = new List<int>();
                IPGlobalProperties iPGlobal = IPGlobalProperties.GetIPGlobalProperties();
                foreach (var item in iPGlobal.GetActiveTcpConnections())
                {
                    usedPorts.Add(item.LocalEndPoint.Port);
                }
                foreach (var item in iPGlobal.GetActiveTcpListeners())
                {
                    usedPorts.Add(item.Port);
                }
            }
            for (int i = port; i <= 65535; i++)
            {
                if (!usedPorts.Contains(i))
                {
                    usedPorts.Add(i);
                    return i;
                }
            }
            return 0;
        }
    }
}
