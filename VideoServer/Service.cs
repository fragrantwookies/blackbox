﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eNerve.DataTypes;
using eThele.Essentials;

namespace VideoServer
{
    public class Service : IDisposable
    {
        private CommsServer commsServer = null;
        private CameraServer cameraServer = null;
        private bool running = false;

        public Service()
        {
            cameraServer = new CameraServer();
            cameraServer.SendMessageEvent += CameraServer_SendMessageEvent;
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public bool Start()
        {
            try
            {
                if (commsServer == null)
                    commsServer = new CommsServer();
                commsServer.GetCamerasEvent += CommsServer_GetCamerasEvent;
                running = commsServer.Start();

                cameraServer.Start();
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(new ExceptionItem("VideoServerStart", "", ex, ExceptionPriority.High));
            }

            return running;
        }

        private void CommsServer_GetCamerasEvent(EventArgs e)
        {
            cameraServer.SendCameras();
        }

        public void Stop()
        {
            if (commsServer != null)
            {
                commsServer.GetCamerasEvent -= CommsServer_GetCamerasEvent;
                commsServer.Stop();
            }

            if (cameraServer != null)
            {
                cameraServer.SendMessageEvent -= CameraServer_SendMessageEvent;
                cameraServer.Stop();
            }
        }

        private void CameraServer_SendMessageEvent(SendMessageEventArgs e)
        {
            if (e != null && commsServer != null)
            {
                commsServer.SendMessage(e);
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            Stop();
        }
    }
}
