﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using XPointAudiovisual.Xtend;

namespace BlackBoxAPI
{
    #region enums
    public enum ElementKind { DigitalInput, AnalogInput };
    public enum InputConditions
    {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        IsActive = 8,
    }
    public enum SerialPortConditions
    {
        // generic map conditions.
        MapIsActive = 0,
        MapHasError,
        MapIsLogging,
        MapIsDebugging,
        // general port status.
        PortIsOpen = 8,
        AutobaudIsActive,
        BreakCharReceived,
        // port TX buffer status.
        TxBufferIsEmpty = 16,
        TxBufferIsBelowHalfFull,
        TxBufferIsFull,
        TxBufferHasOverflowed,
        // port RX buffer status.
        RxBufferIsEmpty = 24,
        RxBufferIsBelowHalfFull,
        RxBufferIsFull,
        RxBufferHasOverflowed,
    }
    #endregion enums

    #region delegates
    public delegate void CardInputEvent(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, InputConditions _eventKind, bool _status);
    public delegate void CardInputChange(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, float _value);
    public delegate void CardSerialDataReceive(Device _device, int _slotIndex, byte[] _dataArray);
    public delegate void CardSerialEvent(Device _device, int _slotIndex, SerialPortConditions _event, bool _status);
    public delegate void CardSerialError(Device _device, int _slotIndex, string _error);
    #endregion delegates

    public class Device : IDisposable, IComparable<Device>, IComparer<Device>
    {
        private const int pingIntervalMax = 10000;
        private const int pingIntervalMin = 5000;

        private XHProxy hub;
        public XHProxy Hub
        {
            get { return hub; }
        }

        public string DeviceName { get; set; }
        public string MacAddress { get; set; }

        private bool onLine = false;
        public bool OnLine 
        { 
            get
            {
                return onLine;
            }
            set 
            {
                if (onLine && !value)
                {
                    if (DeviceOffline != null)
                        DeviceOffline(this);
                }
                else if (!onLine && value)
                {
                    if (DeviceOnline != null)
                        DeviceOnline(this);
                }

                onLine = value;
            }
        }

        public event DeviceEventHandler DeviceOnline;
        public event DeviceEventHandler DeviceOffline;
        public event APIExceptionEventHandler OnException;

        private List<Relay> relays;
        //private XESerialRS232DTE serialPort;

        #region events
        public event CardInputEvent OnDeviceCardInputEvent;
        public event CardInputChange OnDeviceCardInputChange;
        public event CardSerialDataReceive OnDeviceCardSerialDataReceive;
        public event CardSerialEvent OnDeviceCardSerialEvent;
        public event CardSerialError OnDeviceCardSerialError;
        #endregion events

        private bool registeredForEvents = false; //this is to keep track of which wrapper device registered for events on the hub. 

        public DateTime LastSeen { get; set; }

        private System.Timers.Timer pingTimer;
        private ElapsedEventHandler pingTimerElapsedHandler;

        public Device(string _deviceName)
        {
            DeviceName = _deviceName;
            MacAddress = _deviceName;
        }
        
        public Device(XHProxy _hub)
        {
            DeviceName = _hub.ToString();
            MacAddress = hub.MacAddress.ToString();

            UpdateDevice(_hub);
        }

        ~Device()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);   
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (pingTimer != null)
            {
                if (pingTimerElapsedHandler != null)
                    pingTimer.Elapsed -= pingTimerElapsedHandler;

                pingTimer.Stop();
                pingTimer.Dispose();
                pingTimer = null;
            }

            ClearDevice();
        }

        public void UpdateDevice(XHProxy _hub)
        {
            hub = _hub;

            //OnLine = true;
            MacAddress = hub.MacAddress.ToString();            

            //LastSeen = DateTime.Now;

            SetupRelays();

            //foreach (XCProxy card in hub.Cards)
            //{
            //    if (card is XCProxy_Hub_Master_M0003_v0100)
            //    {
            //        serialPort = ((XCProxy_Hub_Master_M0003_v0100)card).SerialRS232DTE;
            //        serialPort.RS232.PassiveReceiveOnly = false;
            //        serialPort.SerialPort.SetConfiguration(9600, 8, 1, System.IO.Ports.Parity.None);
            //        serialPort.SerialPort.IsOpen = true;
            //        serialPort.SerialPort.ReplaceErrorChar(true, '?');
            //        serialPort.SerialPort.SetReceiveCountThreshold(20);
            //        serialPort.SerialPort.SetReceiveTimeoutThreshold(10);

            //        break;
            //    }
            //}
        }

        public static XCProxy FindCard(XHProxy _hub, int _cardIndex, bool _offset = false)
        {
            XCProxy card = null;
            if (_hub != null)
            {
                foreach (XCProxy t_card in _hub.Cards)
                {
                    if (t_card.Index == _cardIndex || _offset && t_card.Index == _cardIndex + 16)
                    {
                        card = t_card;

                        if (!_offset || t_card.Index >= 16)
                            break;
                    }
                }
            }

            return card;
        }
        private int RandomInterval(int _maximum, int _minimum = 0)
        {
            int result = _maximum;
            Random random = new Random((int)DateTime.Now.Ticks & 0x0000FFFF);
            result = random.Next(Math.Abs(_maximum - _minimum)) + (_minimum < _maximum ? _minimum : _maximum);
            return result;
        }

        #region Ping
        private void pingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Thread pingThread = new Thread(new ThreadStart(DoPing));
            pingThread.Start();
        }

        public void StartPing()
        {
            if (pingTimer == null)
            {
                pingTimer = new System.Timers.Timer();
                pingTimerElapsedHandler = new ElapsedEventHandler(pingTimer_Elapsed);
                pingTimer.Elapsed += pingTimerElapsedHandler;
                pingTimer.Interval = RandomInterval(pingIntervalMax, pingIntervalMin);
            }
            if (!pingTimer.Enabled)
                pingTimer.Start();

            pingAttempts = 0;
        }

        public void StopPing()
        {
            if (pingTimer != null)
                pingTimer.Stop();
        }

        public void DoPing()
        {
            try
            {
                pingTimer.Stop();

                if (hub != null)
                {
                    if (!Ping(hub))
                    {
                        OnLine = false;
                    }
                    else
                    {
                        LastSeen = DateTime.Now;
                        OnLine = true;
                        StartListening();
                    }

                    pingTimer.Interval = RandomInterval(pingIntervalMax, pingIntervalMin);
                    pingTimer.Start();
                }
                else
                    OnLine = false;
            }
            catch (Exception ex)
            {
                if (OnException != null)
                    OnException(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        private int pingAttempts = 0;

        public bool Ping(XHProxy _hub)
        {
            
            bool success = false;

            try
            {
                if (_hub != null && _hub.Cards != null && _hub.Cards.Count > 0 && (_hub.Cards[0] is XCProxy_Hub_Master_M0003_v0100 || _hub.Cards[0] is XCProxy_FSS_Hub_Master_M0601_v0200 || _hub.Cards[0] is XCProxy_Hub_Master_M0011_v0100))
                {     
                    try
                    {
                        ++pingAttempts;
                        _hub.Cards[0].Admin.Service.Ping(); //exception will be thrown if device can't be pinged

                        success = true;
                        pingAttempts = 0; //reset counter
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.Message;
                        System.Diagnostics.Debug.WriteLine(string.Format("Ping fail: {0}", this.MacAddress));

                        success = pingAttempts <= 6; //we only want to take it offline if it failed 6 attempts.
                    }
                }
            }
            catch
            {
                success = false;
            }

            return success;
        }
        #endregion Ping

        private void SetupRelays()
        {
            if (hub != null)
            {
                if (relays == null)
                    relays = new List<Relay>();

                relays.Clear();

                for (int cardIndex = 0; cardIndex < hub.Cards.Count; cardIndex++)
                {
                    if (hub.Cards[cardIndex] is XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100)
                    {
                        XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100 card = (XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100)hub.Cards[cardIndex];

                        relays.Add(new Relay(card.Relay_1, card.Index, 1));
                        relays.Add(new Relay(card.Relay_2, card.Index, 2));
                    }
                    else if (hub.Cards[cardIndex] is XCProxy_GPIO_8DigitalOut_M0204_v0100)
                    {
                        XCProxy_GPIO_8DigitalOut_M0204_v0100 card = (XCProxy_GPIO_8DigitalOut_M0204_v0100)hub.Cards[cardIndex];
                        relays.Add(new Relay(card.Output_1, card.Index, 1));
                        relays.Add(new Relay(card.Output_2, card.Index, 2));
                        relays.Add(new Relay(card.Output_3, card.Index, 3));
                        relays.Add(new Relay(card.Output_4, card.Index, 4));
                        relays.Add(new Relay(card.Output_5, card.Index, 5));
                        relays.Add(new Relay(card.Output_6, card.Index, 6));
                        relays.Add(new Relay(card.Output_7, card.Index, 7));
                        relays.Add(new Relay(card.Output_8, card.Index, 8));
                    }
                }
            }
        }

        public void StartListening()
        {
            if(hub != null)
            {
                registeredForEvents = true;

                foreach (XCProxy card in hub.Cards)
                {
                    if (card is XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100)
                    {
                        var c = (XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100)card;

                        if (c.Input_1.DigitalInput.OnEvent == null)
                            c.Input_1.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 1, (InputConditions)_digitalInputConditions, _status); };
                        if (c.Input_2.DigitalInput.OnEvent == null)
                            c.Input_2.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 2, (InputConditions)_digitalInputConditions, _status); };
                        if (c.Input_3.DigitalInput.OnEvent == null)
                            c.Input_3.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 3, (InputConditions)_digitalInputConditions, _status); };
                        if (c.Input_4.DigitalInput.OnEvent == null)
                            c.Input_4.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 4, (InputConditions)_digitalInputConditions, _status); };
                    }

                    else if (card is XCProxy_GPIO_8AnalogIn_Base)
                    {
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_1.AnalogInput.OnEvent == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_1.AnalogInput.OnEvent = (_sender, _event, _status) => { CardOnInputEvent(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 1, (InputConditions)_event, _status); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_2.AnalogInput.OnEvent == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_2.AnalogInput.OnEvent = (_sender, _event, _status) => { CardOnInputEvent(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 2, (InputConditions)_event, _status); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_3.AnalogInput.OnEvent == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_3.AnalogInput.OnEvent = (_sender, _event, _status) => { CardOnInputEvent(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 3, (InputConditions)_event, _status); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_4.AnalogInput.OnEvent == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_4.AnalogInput.OnEvent = (_sender, _event, _status) => { CardOnInputEvent(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 4, (InputConditions)_event, _status); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_5.AnalogInput.OnEvent == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_5.AnalogInput.OnEvent = (_sender, _event, _status) => { CardOnInputEvent(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 5, (InputConditions)_event, _status); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_6.AnalogInput.OnEvent == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_6.AnalogInput.OnEvent = (_sender, _event, _status) => { CardOnInputEvent(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 6, (InputConditions)_event, _status); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_7.AnalogInput.OnEvent == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_7.AnalogInput.OnEvent = (_sender, _event, _status) => { CardOnInputEvent(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 7, (InputConditions)_event, _status); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_8.AnalogInput.OnEvent == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_8.AnalogInput.OnEvent = (_sender, _event, _status) => { CardOnInputEvent(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 8, (InputConditions)_event, _status); };

                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_1.AnalogInput.OnChange == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_1.AnalogInput.OnChange = (_sender, _value) => { CardOnInputChange(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 1, _value); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_2.AnalogInput.OnChange == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_2.AnalogInput.OnChange = (_sender, _value) => { CardOnInputChange(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 2, _value); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_3.AnalogInput.OnChange == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_3.AnalogInput.OnChange = (_sender, _value) => { CardOnInputChange(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 3, _value); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_4.AnalogInput.OnChange == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_4.AnalogInput.OnChange = (_sender, _value) => { CardOnInputChange(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 4, _value); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_5.AnalogInput.OnChange == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_5.AnalogInput.OnChange = (_sender, _value) => { CardOnInputChange(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 5, _value); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_6.AnalogInput.OnChange == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_6.AnalogInput.OnChange = (_sender, _value) => { CardOnInputChange(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 6, _value); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_7.AnalogInput.OnChange == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_7.AnalogInput.OnChange = (_sender, _value) => { CardOnInputChange(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 7, _value); };
                        if (((XCProxy_GPIO_8AnalogIn_Base)card).Input_8.AnalogInput.OnChange == null)
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_8.AnalogInput.OnChange = (_sender, _value) => { CardOnInputChange(((XMAnalogInput)_sender).Parent.Parent.Index, ElementKind.AnalogInput, 8, _value); };
                    }
                    else if (card is XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100)
                    {
                        var c = (XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100)card;

                        if (c.Sensor_1.DigitalInput.OnEvent == null)
                            c.Sensor_1.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 1, (InputConditions)_digitalInputConditions, _status); };
                        if (c.Sensor_2.DigitalInput.OnEvent == null)
                            c.Sensor_2.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 2, (InputConditions)_digitalInputConditions, _status); };
                    }
                    else if (card is XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100)
                    {
                        var c = (XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100)card;

                        if (c.Sensor_1.DigitalInput.OnEvent == null)
                            c.Sensor_1.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 1, (InputConditions)_digitalInputConditions, _status); };
                        if (c.Sensor_2.DigitalInput.OnEvent == null)
                            c.Sensor_2.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 2, (InputConditions)_digitalInputConditions, _status); };
                    }
                    else if (card is XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100)
                    {
                        var c = (XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100)card;

                        if (c.Sensor_1.DigitalInput.OnEvent == null)
                            c.Sensor_1.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 1, (InputConditions)_digitalInputConditions, _status); };
                        if (c.Sensor_2.DigitalInput.OnEvent == null)
                            c.Sensor_2.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(((XMDigitalInput)_sender).Parent.Parent.Index, ElementKind.DigitalInput, 2, (InputConditions)_digitalInputConditions, _status); };
                    }
                    else if(card is XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100)
                    {
                        var c = (XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100)card;

                        int cardIndex = card.Index;
                        int sensorIndex = 1;
                        if (cardIndex >= 16) //Logically fibre cards will report as 2 cards with one sensor each, but we want it to work the same as before, 1 card with 2 sensors. The second sensor will always report with an offset of 16, thus we know if the index is 16 or bigger, we can assume it is the second sensor.
                        {
                            cardIndex -= 16;
                            sensorIndex = 2;
                        }

                        if (c.Sensor_1.DigitalInput.OnEvent == null)
                            c.Sensor_1.DigitalInput.OnEvent = (_sender, _digitalInputConditions, _status) => { CardOnInputEvent(cardIndex, ElementKind.DigitalInput, sensorIndex, (InputConditions)_digitalInputConditions, _status); };
                    }
                    else
                    {
                        var c = card;
                    }
                }

                //if (serialPort != null)
                //    if (serialPort.SerialPort != null)
                //    {
                //        if (serialPort.SerialPort.OnReceiveData == null)
                //            serialPort.SerialPort.OnReceiveData = (object _sender, byte[] _dataArray) => { CardOnSerialDataReceive(((XMSerialPort)_sender).Parent.Parent.Index, _dataArray); };
                //            //serialPort.SerialPort.OnReceiveData = new SerialReceiveDataHandler(CardOnSerialDataReceive2);
                //        if (serialPort.SerialPort.OnEvent == null)
                //            serialPort.SerialPort.OnEvent = (object _sender, XPointAudiovisual.Xtend.ESerialPortConditions _event, bool _status) => { CardOnSerialEvent(((XMSerialPort)_sender).Parent.Parent.Index, _event, _status); };
                //        if (serialPort.SerialPort.OnError == null)
                //            serialPort.SerialPort.OnError = (object _sender, XMError _error) => { CardOnSerialError(((XMSerialPort)_sender).Parent.Parent.Index, _error); };
                //    }
            }
        }

        private void ClearDevice()
        {
            if (hub != null && registeredForEvents) //don't remove eventhandlers if you didn't create them.
            {
                if (hub.Cards != null)
                {
                    foreach (XCProxy card in hub.Cards)
                    {
                        if (card is XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100)
                        {
                            ((XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100)card).Input_1.DigitalInput.OnEvent = null;
                            ((XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100)card).Input_2.DigitalInput.OnEvent = null;
                            ((XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100)card).Input_3.DigitalInput.OnEvent = null;
                            ((XCProxy_GPIO_2RelayOut_4DigitalIn_M0201_v0100)card).Input_4.DigitalInput.OnEvent = null;
                        }
                        else if (card is XCProxy_GPIO_8AnalogIn_Base)
                        {
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_1.AnalogInput.OnEvent = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_2.AnalogInput.OnEvent = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_3.AnalogInput.OnEvent = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_4.AnalogInput.OnEvent = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_5.AnalogInput.OnEvent = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_6.AnalogInput.OnEvent = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_7.AnalogInput.OnEvent = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_8.AnalogInput.OnEvent = null;

                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_1.AnalogInput.OnChange = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_2.AnalogInput.OnChange = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_3.AnalogInput.OnChange = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_4.AnalogInput.OnChange = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_5.AnalogInput.OnChange = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_6.AnalogInput.OnChange = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_7.AnalogInput.OnChange = null;
                            ((XCProxy_GPIO_8AnalogIn_Base)card).Input_8.AnalogInput.OnChange = null;
                        }
                        else if (card is XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100)
                        {
                            var c = (XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100)card;

                            c.Sensor_1.DigitalInput.OnEvent = null;
                            c.Sensor_2.DigitalInput.OnEvent = null;
                        }
                        else if (card is XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100)
                        {
                            var c = (XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100)card;

                            c.Sensor_1.DigitalInput.OnEvent = null;
                            c.Sensor_2.DigitalInput.OnEvent = null;
                        }
                        else if (card is XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100)
                        {
                            var c = (XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100)card;

                            c.Sensor_1.DigitalInput.OnEvent = null;
                            c.Sensor_2.DigitalInput.OnEvent = null;
                        }
                        else if(card is XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100)
                        {
                            var c = (XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100)card;

                            c.Sensor_1.DigitalInput.OnEvent = null;
                        }
                    }
                }

                //if (serialPort != null)
                //    if(serialPort.SerialPort != null)
                //    {                    
                //        serialPort.SerialPort.OnReceiveData = null;
                //        serialPort.SerialPort.OnEvent = null;
                //        serialPort.SerialPort.OnError = null;
                //    }
            }

            if (relays != null)
            {
                for (int i = relays.Count - 1; i >= 0; i--)
                    relays[i].Dispose();

                 relays.Clear();                
            }

            hub = null;
        }

        public void SetRelayState(int _cardIndex, int _relayIndex, bool _state, double _duration = 0)
        {
            try
            {
                if (relays != null)
                {
                    foreach (Relay relay in relays)
                    {
                        if (relay.CardIndex == _cardIndex && relay.RelayIndex == _relayIndex)
                        {
                            relay.SetRelayState(_state, _duration);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (OnException != null)
                    OnException(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        
        /// <summary>
        /// Not a great way to do it. Rather use the overload that sepcifies the cardindex
        /// </summary>
        public void SetRelayState(int _relayIndex, bool _state, double _duration = 0)
        {
            try
            {
                if (relays != null)
                {
                    if (_relayIndex < relays.Count && _relayIndex >= 0)
                    {
                        relays[_relayIndex].SetRelayState(_state, _duration);
                    }
                }
            }
            catch (Exception ex)
            {
                if (OnException != null)
                    OnException(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        public enum FibreSensorName { Sensor_1, Sensor_2 };
        public void SetFibreSensitivity(int _cardIndex, FibreSensorName _fibreSensorName, float _sensitivity)
        {
            try
            {
                XCProxy card = FindCard(hub, _cardIndex, _fibreSensorName == FibreSensorName.Sensor_2);
                
                if (card != null)
                {
                    XEFibreSensor sensor = null;

                    if (card is XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100)
                    {
                        if (_fibreSensorName == FibreSensorName.Sensor_1)
                            sensor = ((XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100)card).Sensor_1;
                        else
                            sensor = ((XCProxy_FS_Fibre_Sensor_2Zone_Base_M0404_v0100)card).Sensor_2;
                    }
                    else if (card is XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100)
                    {
                        if (_fibreSensorName == FibreSensorName.Sensor_1)
                            sensor = ((XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100)card).Sensor_1;
                        else
                            sensor = ((XCProxy_FS_Fibre_Sensor_2Zone_Node_M0403_v0100)card).Sensor_2;
                    }
                    else if (card is XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100)
                    {
                        if (_fibreSensorName == FibreSensorName.Sensor_1)
                            sensor = ((XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100)card).Sensor_1;
                        else
                            sensor = ((XCProxy_FS_Fibre_Sensor_2Zone_Looped_M0402_v0100)card).Sensor_2;
                    }
                    else if (card is XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100)
                    {
                        sensor = ((XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100)card).Sensor_1;

                        //if (_fibreSensorName == FibreSensorName.Sensor_1)
                        //    sensor = ((XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100)card).Sensor_1;
                        //else
                        //    sensor = ((XCProxy_FS_Fibre_Sensor_1Zone_Node_M0405_v0100)hub.Cards[_cardIndex + 16]).Sensor_1;
                    }

                    if (sensor != null)
                    {
                        foreach (XMBase map in sensor.Maps)
                        {
                            if (map is XMFibrePort)
                                ((XMFibrePort)map).SetSensitivity(_sensitivity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (OnException != null)
                    OnException(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        #region Serial functions
        //public void TransmitSerialData(byte[] _data)
        //{
        //    if (serialPort != null)
        //        if (serialPort.SerialPort != null)
        //        {
        //            serialPort.SerialPort.TransmitData(_data);
        //        }
        //}

        //public void TransmitSerialData(string _data)
        //{
        //    if (serialPort != null)
        //        if (serialPort.SerialPort != null)
        //        {
        //            serialPort.SerialPort.TransmitData(_data);
        //        }
        //}

        //// set master serial DTR pin active or inactive.
        //public void SetDtr(bool _choice)
        //{
        //    if (serialPort != null)
        //        if (serialPort.SerialPort != null)
        //        {
        //            serialPort.RS232.SetPinState(_choice, ERS232Pins.DataTerminalReady);
        //        }
        //}

        //// set master serial RTS pin active or inactive.
        //public void SetRts(bool _choice)
        //{
        //    if (serialPort != null)
        //        if (serialPort.SerialPort != null)
        //        {
        //            serialPort.RS232.SetPinState(_choice, ERS232Pins.RequestToSend);
        //        }
        //}

        //// set master serial RTS pin active or inactive.
        //public void SetDsr(bool _choice)
        //{
        //    if (serialPort != null)
        //        if (serialPort.SerialPort != null)
        //        {
        //            serialPort.RS232.SetPinState(_choice, ERS232Pins.DataSetReady);
        //        }
        //}

        //// set master serial RTS pin active or inactive.
        //public void SetCts(bool _choice)
        //{
        //    if (serialPort != null)
        //        if (serialPort.SerialPort != null)
        //        {
        //            serialPort.RS232.SetPinState(_choice, ERS232Pins.ClearToSend);
        //        }
        //}

        //// set passive receive only or full RS232
        //public void SetFullRS232(bool _isFull)
        //{
        //    if (serialPort != null)
        //        if (serialPort.SerialPort != null)
        //        {
        //            serialPort.RS232.PassiveReceiveOnly = !_isFull;
        //        }
        //} 
        #endregion Serial functions

        #region callbackHandlers

        #region CardOnInputEvent
        private void CardOnInputEvent(int _slotIndex, ElementKind _elementKind, int _elementIndex, InputConditions _eventKind, bool _status)
        {
            Thread thread = new Thread(new ParameterizedThreadStart(CardOnInputEventAsync));
            thread.Start(new CardOnInputEventArgs(this, _slotIndex, _elementKind, _elementIndex, _eventKind, _status));
        }

        private class CardOnInputEventArgs : EventArgs
        {
            public Device device { get; set; }
            public int slotIndex { get; set; }
            public ElementKind elementKind { get; set; }
            public int elementIndex { get; set; }
            public InputConditions eventKind { get; set; }
            public bool status { get; set; }

            public CardOnInputEventArgs(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, InputConditions _eventKind, bool _status) : base()
            {
                device = _device;
                slotIndex = _slotIndex;
                elementKind = _elementKind;
                elementIndex = _elementIndex;
                eventKind = _eventKind;
                status = _status;
            }
        }

        private void CardOnInputEventAsync(object _args)
        {
            if (_args is CardOnInputEventArgs)
            {
                CardOnInputEventArgs args = (CardOnInputEventArgs)_args;

                if (OnDeviceCardInputEvent != null)
                    OnDeviceCardInputEvent(args.device, args.slotIndex, args.elementKind, args.elementIndex, args.eventKind, args.status);
            }
        }
        #endregion CardOnInputEvent

        #region CardOnInputChange
        private void CardOnInputChange(int _slotIndex, ElementKind _elementKind, int _elementIndex, float _value)
        {
            Thread thread = new Thread(new ParameterizedThreadStart(CardOnInputChangeAsync));
            thread.Start(new CardOnInputChangeArgs(this, _slotIndex, _elementKind, _elementIndex, _value));
        }

        private class CardOnInputChangeArgs : EventArgs
        {
            public Device device { get; set; }
            public int slotIndex { get; set; }
            public ElementKind elementKind { get; set; }
            public int elementIndex { get; set; }
            public float value { get; set; }

            public CardOnInputChangeArgs(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, float _value) : base()
            {
                device = _device;
                slotIndex = _slotIndex;
                elementKind = _elementKind;
                elementIndex = _elementIndex;
                value = _value;
            }
        }

        private void CardOnInputChangeAsync(object _args)
        {
            if (_args is CardOnInputChangeArgs)
            {
                CardOnInputChangeArgs args = (CardOnInputChangeArgs)_args;

                if (OnDeviceCardInputChange != null)
                    OnDeviceCardInputChange(args.device, args.slotIndex, args.elementKind, args.elementIndex, args.value);
            }
        } 
        #endregion CardOnInputChange

        private void CardOnSerialDataReceive(int _slotIndex, byte[] _dataArray)
        {
            if(OnDeviceCardSerialDataReceive != null)
                OnDeviceCardSerialDataReceive(this, _slotIndex, _dataArray);
        }

        private void CardOnSerialDataReceive2(object _sender, byte[] _dataArray)
        {
            if (OnDeviceCardSerialDataReceive != null)
                OnDeviceCardSerialDataReceive(this, 1, _dataArray);
        }

        private void CardOnSerialEvent(int _slotIndex, XPointAudiovisual.Xtend.ESerialPortConditions _event, bool _status)
        {
            if (OnDeviceCardSerialEvent != null)
                OnDeviceCardSerialEvent(this, _slotIndex, (SerialPortConditions)_event, _status);
        }

        private void CardOnSerialError(int _slotIndex, XMError _error)
        {
            if (OnDeviceCardSerialError != null)
                OnDeviceCardSerialError(this, _slotIndex, _error.Message);
        }

        #endregion callbackHandlers

        public System.Net.IPAddress IPAddress
        {
            get
            {
                return hub != null ? hub.IpAddress : null;
            }
        }

        public int CompareTo(Device other)
        {
            return this.MacAddress.CompareTo(other.MacAddress);
        }

        public int Compare(Device x, Device y)
        {
            return x.MacAddress.CompareTo(y.MacAddress);
        }

        public override string ToString()
        {
            return DeviceName;
        }
    }

    public class Relay : IDisposable
    {
        protected XEProxy xeOutput;

        private int cardIndex = 0;
        private int relayIndex = 0;

        private System.Timers.Timer timer = null;
        private bool timerState = false; //we must keep track of the state so that we can reset it after the timer elapsed.

        public Relay(XEProxy _xeOutput, int _cardIndex, int _relayIndex)
        {
            xeOutput = _xeOutput;
            cardIndex = _cardIndex;
            relayIndex = _relayIndex;
        }

        ~Relay()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool Disposing)
        {
            if (Disposing)
                GC.SuppressFinalize(this);
            
            if (timer != null)
            {
                timer.Stop();
                timer.Elapsed -= timer_Elapsed;
                timer.Dispose();
                timer = null;
            }
        }

        public void SetRelayState(bool _state, double _duration = 0)
        {
            if (xeOutput is XERelayOutput)
            {
                XERelayOutput relay = (XERelayOutput)xeOutput;

                relay.RelayOutput.IsActive = _state;
            }
            else if (xeOutput is XEDigitalOutput)
            {
                XEDigitalOutput output = (XEDigitalOutput)xeOutput;

                output.DigitalOutput.IsActive = _state;
            }


            if (_duration > 0)
            {
                timerState = _state;
                if (timer == null)
                {
                    timer = new System.Timers.Timer();
                    timer.Elapsed += timer_Elapsed;
                }

                timer.Interval = _duration * 1000;
                timer.Start();
            }
        }

        public int CardIndex
        {
            get { return cardIndex; }
            set { cardIndex = value; }
        }

        public int RelayIndex
        {
            get { return relayIndex; }
            set { relayIndex = value; }
        }

        //public bool RelayIsActive
        //{
        //    get
        //    {
        //        bool result = false;

        //        if (xeOutput is XERelayOutput)
        //            result = ((XERelayOutput)xeOutput).RelayOutput.IsActive;
        //        else if (xeOutput is XEDigitalOutput)
        //            result = ((XEDigitalOutput)xeOutput).DigitalOutput.IsActive;

        //        return result;
        //    }
        //}

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            SetRelayState(false);
            //xeRelay.RelayOutput.IsActive = !timerState;
        }
    }
}
