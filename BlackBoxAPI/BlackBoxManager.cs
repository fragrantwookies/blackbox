﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XPointAudiovisual.Xtend;
using System.Timers;
using System.ComponentModel;
using System.Threading;
using System.Diagnostics;

namespace BlackBoxAPI
{
    public delegate void DeviceEventHandler(Device _device);
    public delegate void APIExceptionEventHandler(string _functionName, string _description, Exception _exception);
        
    public class BlackBoxManager : IDisposable
    {
        private List<Device> devices;

        #region EventHandlers
        private DeviceEventHandler deviceOfflineEventH;
        private DeviceEventHandler deviceOnlineEventH;
        private APIExceptionEventHandler deviceExceptionEventH;
        private CardInputEvent cardInputEventH;
        private CardInputChange cardInputChangeH;
        private CardSerialDataReceive cardSerialDataReceiveH;
        private CardSerialEvent cardSerialEventH;
        private CardSerialError cardSerialErrorH;
        #endregion EventHandlers

        private RunHandler pollHandler;
        private System.Timers.Timer updateTimer;
        private ElapsedEventHandler updateTimerElapsedEH;
        private DateTime lastUpdate = DateTime.Now;
        private int updateTimerInterval = 5;
        public int UpdateTimerInterval
        {
            get { return updateTimerInterval; }
            set 
            { 
                updateTimerInterval = value;
                updateTimer.Interval = updateTimerInterval;
            }
        }

        #region Events
        public event DeviceEventHandler OnDeviceAdded;
        public event DeviceEventHandler OnRemovingDevice;
        public event CardInputEvent OnCardInputEvent;
        public event CardInputChange OnCardInputChange;
        public event CardSerialDataReceive OnCardSerialDataReceive;
        public event CardSerialEvent OnCardSerialEvent;
        public event CardSerialError OnCardSerialError;

        public event APIExceptionEventHandler OnException;
        #endregion Events

        public BlackBoxManager()
        {
            devices = new List<Device>();           

            pollHandler = new RunHandler(UpdateDevices);

            XIO.Open(pollHandler);

            deviceOfflineEventH = new DeviceEventHandler(OnDeviceOffline);
            deviceOnlineEventH = new DeviceEventHandler(OnDeviceOnline);
            deviceExceptionEventH = new APIExceptionEventHandler(OnDeviceException);                      

            cardInputEventH = new CardInputEvent(Device_OnCardInputEvent);
            cardInputChangeH = new CardInputChange(Device_OnCardInputChange);
            cardSerialDataReceiveH = new CardSerialDataReceive(Device_OnCardSerialDataReceive);
            cardSerialEventH = new CardSerialEvent(Device_OnCardSerialEvent);
            cardSerialErrorH = new CardSerialError(Device_OnCardSerialError);

            updateTimer = new System.Timers.Timer(updateTimerInterval * 1000);
            updateTimerElapsedEH = new ElapsedEventHandler(updateTimer_Elapsed);
            updateTimer.Elapsed += updateTimerElapsedEH;
        }

        #region Cleanup
        ~BlackBoxManager()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            updateTimer.Stop();
            updateTimer.Elapsed -= updateTimerElapsedEH;
            updateTimer.Dispose();

            DiscardDevices();
            XIO.Close();
        }

        private void DiscardDevices()
        {
            try
            {
                if (devices != null)
                {
                    for (int i = devices.Count - 1; i >= 0; i--)
                    {
                        Device d = devices[i];

                        if (d != null)
                        {
                            if (deviceOfflineEventH != null)
                                d.DeviceOffline -= deviceOfflineEventH;

                            if (deviceOnlineEventH != null)
                                d.DeviceOnline -= deviceOnlineEventH;

                            if (deviceExceptionEventH != null)
                                d.OnException -= deviceExceptionEventH;

                            if (cardInputEventH != null)
                                d.OnDeviceCardInputEvent -= cardInputEventH;

                            if (cardInputChangeH != null)
                                d.OnDeviceCardInputChange -= cardInputChangeH;

                            if (cardSerialDataReceiveH != null)
                                d.OnDeviceCardSerialDataReceive -= cardSerialDataReceiveH;

                            if (cardSerialEventH != null)
                                d.OnDeviceCardSerialEvent -= cardSerialEventH;

                            if (cardSerialErrorH != null)
                                d.OnDeviceCardSerialError -= cardSerialErrorH;

                            d.Dispose();
                        }
                    }

                    devices.Clear();
                }
            }
            catch (Exception ex)
            {
                if (OnException != null)
                    OnException(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Cleanup

        #region Start and stop
        /// <summary>
        /// Start looking for devices on a specified interval.
        /// </summary>
        public void Start()
        {
            updateTimer.Start();
            //Find();
        }

        /// <summary>
        /// Stop looking for devices.
        /// </summary>
        public void Stop()
        {
            updateTimer.Stop();
            XIO.Close();
        }

        public void Reset()
        {
            try
            {
                updateTimer.Stop();

                XIO.Close();
                DiscardDevices();
                XIO.Open();
                Poll();

                updateTimer.Start();

                if (OnException != null)
                    OnException(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "BlackboxManager Reset", null);
            }
            catch (Exception ex)
            {
                if (OnException != null)
                    OnException(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Start and stop

        #region Polling
        private void updateTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Poll();
        }

        public void Poll()
        {
            try
            {
                XIO.Find(pollHandler);
                UpdateDevices();
            }
            catch (Exception ex)
            {
                if (OnException != null)
                    OnException(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        /// <summary>
        /// callback for Find, don't call this directly
        /// </summary>
        private void UpdateDevices()
        {
            try
            {
                lastUpdate = DateTime.Now;

                IEnumerable<XHProxy> detectedHubs = XIO.DetectedHubsCopy;
                //IEnumerable<XHProxy> detectedHubs2 = XIO.Finder.Found();

                if (devices == null)
                {
                    devices = new List<Device>();
                }

                //Add the new devices
                if (detectedHubs != null)
                {
                    if (detectedHubs.Count() > 0)
                    {
                        if (devices != null)
                        {
                            foreach (XHProxy hub in detectedHubs)
                            {
                                Device tmpDevice = new Device(hub.MacAddress.ToString());

                                int searchResult = devices.BinarySearch(tmpDevice);

                                if (searchResult < 0)
                                {
                                    //if (tmpDevice.Ping(hub))
                                    {
                                        devices.Insert(~searchResult, tmpDevice);
                                        Debug.WriteLine(tmpDevice.MacAddress);
                                        tmpDevice.UpdateDevice(hub);
                                        tmpDevice.StartListening();
                                        tmpDevice.StartPing();

                                        if (cardInputEventH != null)
                                            tmpDevice.OnDeviceCardInputEvent += cardInputEventH;

                                        if (cardInputChangeH != null)
                                            tmpDevice.OnDeviceCardInputChange += cardInputChangeH;

                                        if (cardSerialDataReceiveH != null)
                                            tmpDevice.OnDeviceCardSerialDataReceive += cardSerialDataReceiveH;

                                        if (cardSerialEventH != null)
                                            tmpDevice.OnDeviceCardSerialEvent += cardSerialEventH;

                                        if (cardSerialErrorH != null)
                                            tmpDevice.OnDeviceCardSerialError += cardSerialErrorH;

                                        //if (OnDeviceAdded != null)
                                        //    OnDeviceAdded(tmpDevice);

                                        tmpDevice.DeviceOffline += deviceOfflineEventH;
                                        tmpDevice.DeviceOnline += deviceOnlineEventH;
                                        tmpDevice.OnException += deviceExceptionEventH;
                                    }
                                }
                                else
                                {
                                    Device device = devices[searchResult];

                                    if (hub != device.Hub)
                                    {
                                        device.UpdateDevice(hub);
                                        device.StartListening();
                                        device.StartPing();
                                    }

                                    //if (device.Ping(hub))
                                    //{
                                    //    if (!device.OnLine)
                                    //    {
                                    //        device.UpdateDevice(hub);
                                    //        device.StartListening();

                                    //        if (OnDeviceAdded != null)
                                    //            OnDeviceAdded(device);
                                    //    }
                                    //    else if (hub != device.Hub)
                                    //    {
                                    //        device.UpdateDevice(hub);
                                    //        device.StartListening();
                                    //    }
                                    //    else
                                    //        device.LastSeen = DateTime.Now;
                                    //}     

                                    tmpDevice.Dispose();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (OnException != null)
                    OnException(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion Polling

        #region Misc
        public int Count
        {
            get
            {
                return devices.Count;
            }
        }

        public Device this[int _index]
        {
            get
            {
                return devices[_index];
            }
        }

        public Device GetDeviceByMacAddress(string _macAddress)
        {
            Device result = null;
            foreach (Device device in devices)
            {
                if (device.MacAddress == _macAddress)
                {
                    result = device;
                    break;
                }
            }

            return result;
        }
        #endregion Misc

        #region Device eventhandler methods
        void OnDeviceException(string _functionName, string _description, Exception _exception)
        {
            if (OnException != null)
                OnException(_functionName, _description, _exception);
        }

        void OnDeviceOffline(Device _device)
        {
            if (OnRemovingDevice != null)
                OnRemovingDevice(_device);
        }

        void OnDeviceOnline(Device _device)
        {
            if (OnDeviceAdded != null)
                OnDeviceAdded(_device);
        }

        private void Device_OnCardInputEvent(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, InputConditions _eventKind, bool _status)
        {
            Debug.WriteLine(string.Format("Device: {0}, Card: {1}, Pin: {2}, Status: {3}", _device, _slotIndex, _elementIndex, _status));
            if (OnCardInputEvent != null)
                OnCardInputEvent(_device, _slotIndex, _elementKind, _elementIndex, _eventKind, _status);
        }

        private void Device_OnCardInputChange(Device _device, int _slotIndex, ElementKind _elementKind, int _elementIndex, float _value)
        {
            Debug.WriteLine(string.Format("Device: {0}, Card: {1}, Pin: {2}, Value: {3}", _device, _slotIndex, _elementIndex, _value));
            if (OnCardInputChange != null)
                OnCardInputChange(_device, _slotIndex, _elementKind, _elementIndex, _value);
        }

        private void Device_OnCardSerialDataReceive(Device _device, int _slotIndex, byte[] _dataArray)
        {
            if (OnCardSerialDataReceive != null)
                OnCardSerialDataReceive(_device, _slotIndex, _dataArray);
        }

        private void Device_OnCardSerialEvent(Device _device, int _slotIndex, SerialPortConditions _event, bool _status)
        {
            if (OnCardSerialEvent != null)
                OnCardSerialEvent(_device, _slotIndex, _event, _status);
        }

        private void Device_OnCardSerialError(Device _device, int _slotIndex, string _error)
        {
            if (OnCardSerialError != null)
                OnCardSerialError(_device, _slotIndex, _error);
        }

        #endregion Device eventhandler methods
    }
}
