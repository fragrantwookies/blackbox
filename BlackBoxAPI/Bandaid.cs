﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace BlackBoxAPI
{
    /// <summary>
    /// Cover up for devices going missing where restarting the service finds them again. This will monitor the devices and should cover up situations like that until the xtend driver get's fixed.
    /// If a device goes offline we'll only try to reset it if it has been offline for at least 10 minutes.
    /// Reset can't happen more frequently than every 20 minutes.
    /// The same device can't force more than 1 reset per hour.
    /// Timer will sweep every 5 minutes.
    /// </summary>
    public class Bandaid : IDisposable
    {
        private static readonly TimeSpan minTimeOffline = TimeSpan.FromMinutes(5);
        private static readonly TimeSpan maxRebootFreq = TimeSpan.FromMinutes(20);
        private static readonly TimeSpan maxRebootFreqPerDevice = TimeSpan.FromHours(1);
        private static readonly TimeSpan timerInterval = TimeSpan.FromMinutes(5);

        private Timer timer;

        private BlackBoxManager bbm;

        public BlackBoxManager Bbm
        {
            get { return bbm; }
            set 
            { 
                bbm = value;

                bbm.OnDeviceAdded += bbm_OnDeviceAdded;
                bbm.OnRemovingDevice += bbm_OnRemovingDevice;

                if (deviceStates != null)
                {
                    deviceStates.Clear();

                    for (int i = 0; i < bbm.Count; i++)
                        deviceStates.Add(new DeviceStatus(deviceStates[i].Mac, deviceStates[i].Online));
                }
            }
        }

        private DateTime lastReboot = DateTime.MinValue;

        /// <summary>
        /// list of device MAC, online status, last time offline, last reboot command issued
        /// </summary>
        private List<DeviceStatus> deviceStates;

        public Bandaid()
        {
            deviceStates = new List<DeviceStatus>();

            timer = new Timer() { Interval = timerInterval.TotalMilliseconds };
            timer.Elapsed += timer_Elapsed;
            timer.Start();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {            
            if(DateTime.Now - lastReboot > maxRebootFreq)
            {
                foreach(var status in deviceStates)
                {
                    if (!status.Online 
                        && (DateTime.Now - status.LastOffline) > minTimeOffline 
                        && (DateTime.Now - status.RebootIssued) > maxRebootFreqPerDevice)
                    {
                        status.RebootIssued = DateTime.Now;
                        Reboot();
                        break;
                    }
                }
            }
        }

        #region Cleanup
        ~Bandaid()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                GC.SuppressFinalize(this);

            if (timer != null)
            {
                timer.Stop();
                timer.Elapsed -= timer_Elapsed;
                timer.Dispose();
            }

            if (bbm != null)
            {
                bbm.OnDeviceAdded -= bbm_OnDeviceAdded;
                bbm.OnRemovingDevice -= bbm_OnRemovingDevice;
            }

            if(deviceStates != null)
                deviceStates.Clear();
        }
        #endregion Cleanup

        private void Reboot()
        {
            if(bbm != null)
            {
                bbm.Reset();

                lastReboot = DateTime.Now;
            }
        }

        #region State change
        void bbm_OnDeviceAdded(Device _device)
        {
            DeviceStatus status = new DeviceStatus(_device.MacAddress, true);
            int index = -1;
            if ((index = deviceStates.IndexOf(status)) > 0) //DeviceStatus is equatable on MAC address field.
            {
                deviceStates[index].Online = true;
            }
            else
                deviceStates.Add(status);
        }

        void bbm_OnRemovingDevice(Device _device)
        {
            DeviceStatus status = new DeviceStatus(_device.MacAddress, false);
            int index = -1;
            if ((index = deviceStates.IndexOf(status)) > 0)
            {
                status = deviceStates[index];
                status.Online = false;
                status.LastOffline = DateTime.Now;
            }
            else //shouldn't really ever happen, but what the hell
                deviceStates.Add(status);

            //if (DateTime.Now - status.RebootIssued >= maxRebootFreqPerDevice && DateTime.Now - lastReboot > maxRebootFreq)
            //{
            //    status.RebootIssued = DateTime.Now;
            //    Reboot();
            //}
        }
        #endregion State change
    }

    public class DeviceStatus : IEquatable<DeviceStatus>
    {
        private string mac = string.Empty;
        public string Mac
        {
            get { return mac; }
            set { mac = value; }
        }

        private bool online = false;
        public bool Online
        {
            get { return online; }
            set { online = value; }
        }

        private DateTime lastOffline = DateTime.MinValue;
        public DateTime LastOffline
        {
            get { return lastOffline; }
            set { lastOffline = value; }
        }

        private DateTime rebootIssued = DateTime.MinValue;
        public DateTime RebootIssued
        {
            get { return rebootIssued; }
            set { rebootIssued = value; }
        }

        public DeviceStatus(string _mac, bool _online)
        {
            mac = _mac;
            online = _online;
        }

        public bool Equals(DeviceStatus other)
        {
            return this.Mac == other.Mac;
        }
    }
}