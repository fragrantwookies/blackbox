﻿using BlackBoxEngine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject
{
    
    
    /// <summary>
    ///This is a test class for ConnectionDetailsTest and is intended
    ///to contain all ConnectionDetailsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class ConnectionDetailsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for Valid
        ///</summary>
        [TestMethod()]
        public void ValidTest()
        {
            ConnectionDetails target = new ConnectionDetails(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            target.Valid = expected;
            actual = target.Valid;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ConnectionString
        ///</summary>
        [TestMethod()]
        public void ConnectionStringTest()
        {
            ConnectionDetails target = new ConnectionDetails(); // TODO: Initialize to an appropriate value
            ConnectionDetails TestCredentials = new ConnectionDetails(); ; // TODO: Initialize to an appropriate value
            TestCredentials.Server = "HENDRIK_TEST";
            TestCredentials.Database = "BlackBox";
            TestCredentials.UserId = "BlackBox";
            TestCredentials.Password = "Bl@ckB0x";
            string expected = "Data Source=HENDRIK_TEST;Initial Catalog=BlackBox;User ID=BlackBox;Password=Bl@ckB0x;MultipleActiveResultSets=True;";
            string actual;
            actual = target.ConnectionString(TestCredentials);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ConnectionString. Ensure DBInterface.xml contains details as above.
        ///</summary>
        [TestMethod()]
        public void ConnectionStringTest1()
        {
            ConnectionDetails target = new ConnectionDetails(); // TODO: Initialize to an appropriate value
            string expected = "Data Source=HENDRIK_TEST;Initial Catalog=BlackBox;User ID=BlackBox;Password=Bl@ckB0x;MultipleActiveResultSets=True;";
            string actual;
            target.Load();
            actual = target.ConnectionString();
            Assert.AreEqual(expected, actual);
        }
    }
}
