﻿using BlackBoxAPI;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject
{
    
    
    /// <summary>
    ///This is a test class for BlackBoxManagerTest and is intended
    ///to contain all BlackBoxManagerTest Unit Tests
    ///</summary>
    [TestClass()]
    public class BlackBoxManagerTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        /// <summary>
        ///A test for Reset
        ///</summary>
        [TestMethod()]
        public void ResetTest()
        {
            BlackBoxManager target = new BlackBoxManager(); // TODO: Initialize to an appropriate value
            target.Reset();
            Assert.AreEqual(target.Count, 0);
        }

        /// <summary>
        ///A test for Dispose
        ///</summary>
        [TestMethod()]
        public void DisposeTest()
        {
            BlackBoxManager target = new BlackBoxManager(); // TODO: Initialize to an appropriate value
            target.Dispose();
            target.Dispose();
            Assert.AreEqual(target.Count, 0);
        }

        /// <summary>
        ///A test for BlackBoxManager Constructor
        ///</summary>
        [TestMethod()]
        public void BlackBoxManagerConstructorTest()
        {
            BlackBoxManager target = new BlackBoxManager();
            Assert.IsNotNull(target.Count);
        }
    }
}
