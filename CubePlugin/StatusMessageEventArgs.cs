﻿using System;

namespace CubePlugin
{
    public enum CubeDeviceStatus
    {
        Default,
        Restart,
        Keepalive,
        Reserved,
        Reserved2,
        SensorFault
    }
    public delegate void StatusReceivedEventHandler(object sender, StatusMessageEventArgs e);
    public class StatusMessageEventArgs : EventArgs
    {
        public int DatabaseEntryID { get; private set; }
        public string DeviceID { get; private set; }
        public CubeDeviceStatus DeviceStatus { get; private set; }
        
        public StatusMessageEventArgs(string rawMessage)
        {
            if (!string.IsNullOrEmpty(rawMessage))
            {
                string [] parts = rawMessage.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 3)// Don't allow messages in unexpected format. TODO test with appended commas.
                {
                    int temp;
                    if (int.TryParse(parts[0], out temp))
                        DatabaseEntryID = temp;
                    DeviceID = parts[1];
                    if (int.TryParse(parts[2], out temp))
                    {
                        DeviceStatus = (CubeDeviceStatus)temp;
                    }
                }
            }
        }
    }
}
