﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;

using eNervePluginInterface;
using eThele.Essentials;

namespace CubePlugin
{
    public class CubePlugin : INerveEventPlugin, IDisposable
    {
        private string fileName;
        private WebServiceHost host;
        private CustomProperties properties;
        private CubeDeviceStatus status;
        private System.Timers.Timer heartbeatTimer;
        private DateTime lastHeartbeat;
        private System.Threading.Timer threadingTimer;
        private Dictionary<string, CubeStatus> Cubes;

        public string Author
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company;
            }
        }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                result.Add(new CustomPropertyDefinition("ID", "Device ID", "Device ID", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Password", "Password", "Password", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("MinimumCharge", "Battery Minimum Charge", "Min Charge (%)", 
                    typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("MinimumVoltage", "Battery Minimum Voltage", "Min Voltage (mV)", 
                    typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("Heartbeat", "Number of minutes before alarm", "Heartbeat minutes", typeof(CustomProperty)));
                return result;
            }
        }

        public string Description
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
            }
        }

        public bool Enabled
        { get; set; }
        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public INerveHostPlugin Host { get; set; }

        public Location Location { get; private set; }

        public string Name { get; set; }

        public bool Online { get; set; }

        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
            set { properties = value; }
        }

        public string Version
        {
            get { return Assembly.GetCallingAssembly().GetName().Version.ToString(); }
        }

        public string[] EventNames { get { return new string[] { "CubeAlarm", "CubeLocation", "CubeStatus", "CubeBatteryLow",
            "CubeVoltageLow", "CubeCommunicationLost", "CubeCommunicationRestored" }; } }
        
        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }

        public event EventHandler DeviceOffline;
        public event EventHandler DeviceOnline;
        public event EventHandler<PluginErrorEventArgs> Error;
        public event EventHandler<PluginTriggerEventArgs> Trigger;

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Start()
        {
            host = new WebServiceHost(typeof(CubeWebService), new Uri("http://localhost:587/"));
            Location = new Location(0, 0);
            try
            {
                ServiceEndpoint endPoint = host.AddServiceEndpoint(typeof(ICubeWebService), new WebHttpBinding(), "");
                CubeWebService.MessageEvent += CubeWebService_MessageEvent;
                CubeWebService.BatteryEvent += CubeWebService_BatteryEvent;
                CubeWebService.AlarmEvent += CubeWebService_AlarmEvent;
                CubeWebService.LocationEvent += CubeWebService_LocationEvent;
                CubeWebService.StatusEvent += CubeWebService_StatusEvent;
                host.Open();
                using (ChannelFactory<ICubeWebService> cf = new ChannelFactory<ICubeWebService>(new WebHttpBinding(), "http://localhost:587"))
                {
                    cf.Endpoint.EndpointBehaviors.Add(new WebHttpBehavior());
                    ICubeWebService channel = cf.CreateChannel();
                    if (DeviceOnline != null)
                        DeviceOnline(this, new EventArgs());
                    Online = true;
                }
                heartbeatTimer = new System.Timers.Timer(60000);
                heartbeatTimer.AutoReset = false;
                heartbeatTimer.Elapsed += HeartbeatTimer_Elapsed;
                heartbeatTimer.Start();
            }
            catch (CommunicationException cEx)
            {
                if (DeviceOffline != null)
                    DeviceOffline(this, new EventArgs());
                Online = false;
                if (Error != null)
                    Error(this, new PluginErrorEventArgs(MethodBase.GetCurrentMethod().Name, "Error while starting web service host.", cEx));
                host.Abort();
            }
        }

        /// <summary>
        /// This will happen if no messages are received from the CubeWebService for the configured number of minutes. Raise alarm.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HeartbeatTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            heartbeatTimer.Stop();
            var ping = new Ping();
            if (ping.Send(CubeWebService.LastAddress).Status == IPStatus.Success)
            {
                if (Online)
                {
                    int heartbeatMilis = 0;
                    string err = "";
                    TimeSpan heartbeat = new TimeSpan(1, 1, 0);// Default to one hour plus one minute if no heartbeat specified.
                    if (TimeSpanParser.TryParse(Properties["Heartbeat"], out heartbeatMilis, out err))
                    {
                        heartbeat = TimeSpan.FromMilliseconds(heartbeatMilis);
                    }
                    else if (Error != null)
                        Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing Heartbeat: {0}", err), null));

                    if (Cubes != null)
                    {
                        lock (Cubes)
                        {
                            foreach (var cube in Cubes)
                            {
                                if (cube.Value.Online && (DateTime.Now - cube.Value.LastMessage) > heartbeat)
                                {
                                    if (Trigger != null)
                                        Trigger(this, new PluginTriggerEventArgs(new CubeTrigger()
                                        {
                                            TriggerID = Guid.NewGuid(),
                                            DeviceName = Name,
                                            DetectorName = cube.Key,
                                            EventName = EventNames[5],
                                            Data = cube.Value.ToString(),
                                            State = true,
                                            TriggerDT = DateTime.Now
                                        }));
                                    cube.Value.Online = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (DeviceOnline != null)
                        DeviceOnline(this, new EventArgs());
                    Online = true;
                    foreach (var cube in Cubes)
                    {
                        if (cube.Value.Online)
                            cube.Value.LastMessage = DateTime.Now;
                    }
                }
            }
            else
            {
                if (Online)
                {
                    if (DeviceOffline != null)
                        DeviceOffline(this, new EventArgs());
                    Online = false;
                }
            }
            lastHeartbeat = DateTime.Now;
            heartbeatTimer.Start();
        }

        private void CubeWebService_MessageEvent(object sender, GenericMessageEventArgs e)
        {
            if (heartbeatTimer != null)
                heartbeatTimer.Stop();
            if (Cubes == null)
                Cubes = new Dictionary<string, CubeStatus>();
            lock (Cubes)
            {
                if (Cubes.Count == 0)
                {
                    Cubes.Add(e.DeviceID, new CubeStatus(true, DateTime.Now));
                }
                else
                {
                    if (Cubes.ContainsKey(e.DeviceID))
                    {

                        if (!Cubes[e.DeviceID].Online)// Device was offline, now recovered.
                        {
                            if (Trigger != null)
                                Trigger(this, new PluginTriggerEventArgs(new CubeTrigger()
                                {
                                    TriggerID = Guid.NewGuid(),
                                    DeviceName = Name,
                                    DetectorName = e.DeviceID,
                                    EventName = EventNames[6],
                                    Data = Cubes[e.DeviceID].ToString(),
                                    State = true,
                                    TriggerDT = DateTime.Now
                                }));
                            Cubes[e.DeviceID].Online = true;
                        }
                        
                        if (Trigger != null)
                            Trigger(this, new PluginTriggerEventArgs(new CubeTrigger()
                            {
                                TriggerID = Guid.NewGuid(),
                                DeviceName = Name,
                                DetectorName = e.DeviceID,
                                EventName = EventNames[6],
                                Data = Cubes[e.DeviceID].ToString(),
                                State = true,
                                TriggerDT = DateTime.Now
                            }));
                        Cubes[e.DeviceID].LastMessage = DateTime.Now;
                    }
                    else
                    {
                        Cubes.Add(e.DeviceID, new CubeStatus(true, DateTime.Now));
                    }
                }
            }
            if (heartbeatTimer != null)
            {
                if ((DateTime.Now - lastHeartbeat) > TimeSpan.FromMinutes(1))
                {
                    lastHeartbeat = DateTime.Now;
                    HeartbeatTimer_Elapsed(this, null);
                }
                else
                    heartbeatTimer.Start();
            }
        }

        private void CubeWebService_AlarmEvent(object sender, AlarmMessageEventArgs e)
        {
            if (Trigger != null)
                Trigger(this, new PluginTriggerEventArgs(new CubeTrigger()
                {
                    TriggerID = Guid.NewGuid(),
                    DeviceName = Name,
                    DetectorName = e.DeviceID + e.AlarmType.ToString(),
                    TriggerDT = e.TimeStamp,
                    EventName = EventNames[0],
                    DetectorType = DetectorType.Digital,
                    Data = e.DatabaseEntryID.ToString(),
                    Location = Location.Lattitude + "," + Location.Longitude,
                    Name = e.AlarmType.ToString() + " Alarm",
                    State = true
                }));
        }

        private void CubeWebService_BatteryEvent(object sender, BatteryMessageEventArgs e)
        {
            if (Trigger != null)
            {
                bool raiseEvent = false;
                double temp;
                if (double.TryParse(Properties["MinimumCharge"], out temp))
                {
                    if (e.BatteryPercentage < temp)
                        raiseEvent = true;
                }
                else
                {// Property not specified - use default valdues.
                    if (e.BatteryPercentage < 60.0)
                        raiseEvent = true;
                }
                if (raiseEvent)
                {
                    Trigger(this, new PluginTriggerEventArgs(new CubeTrigger()
                    {
                        TriggerID = Guid.NewGuid(),
                        DeviceName = Name,
                        DetectorName = e.DeviceID + EventNames[3],
                        EventName = EventNames[3],
                        Data = e.DatabaseEntryID.ToString(),
                        TriggerDT = DateTime.Now,
                        EventValue = e.BatteryPercentage
                    }));
                    raiseEvent = false;
                }
                if (double.TryParse(Properties["MinimumVoltage"], out temp))
                {
                    if (e.BatteryVoltage < temp)
                        raiseEvent = true;
                }
                else
                {
                    if (e.BatteryVoltage < 3700)
                        raiseEvent = true;
                }
                if (raiseEvent)
                    Trigger(this, new PluginTriggerEventArgs(new CubeTrigger()
                    {
                        TriggerID = Guid.NewGuid(),
                        DeviceName = Name,
                        DetectorName = e.DeviceID + EventNames[4],
                        EventName = EventNames[4],
                        Data = e.DatabaseEntryID.ToString(),
                        TriggerDT = DateTime.Now,
                        EventValue = e.BatteryVoltage
                    }));
            }
        }

        private void CubeWebService_LocationEvent(object sender, LocationMessageEventArgs e)
        {
            Location = e.Location;
            if (Trigger != null)
                Trigger(this, new PluginTriggerEventArgs(new CubeTrigger()
                {
                    TriggerID = Guid.NewGuid(),
                    DeviceName = Name,
                    DetectorName = e.DeviceID,
                    EventName = EventNames[1],
                    Data = e.DatabaseEntryID.ToString(),
                    TriggerDT = DateTime.Now
                }));
        }

        private void CubeWebService_StatusEvent(object sender, StatusMessageEventArgs e)
        {
            status = e.DeviceStatus;
            if (Trigger != null)
                Trigger(this, new PluginTriggerEventArgs(new CubeTrigger()
                {
                    TriggerID = Guid.NewGuid(),
                    DeviceName = Name,
                    DetectorName = e.DeviceID,
                    EventName = EventNames[2] + " " + status.ToString(),
                    Data = e.DatabaseEntryID.ToString(),
                    State = e.DeviceStatus == CubeDeviceStatus.Keepalive || e.DeviceStatus == CubeDeviceStatus.Restart,
                    TriggerDT = DateTime.Now
                }));
        }

        protected void Dispose(bool disposing)
        {
            if (heartbeatTimer != null)
            {
                heartbeatTimer.Stop();
                heartbeatTimer.Elapsed -= HeartbeatTimer_Elapsed;
            }


            CubeWebService.MessageEvent -= CubeWebService_MessageEvent;
            CubeWebService.BatteryEvent -= CubeWebService_BatteryEvent;
            CubeWebService.AlarmEvent -= CubeWebService_AlarmEvent;
            CubeWebService.LocationEvent -= CubeWebService_LocationEvent;
            CubeWebService.StatusEvent -= CubeWebService_StatusEvent;
            if (host != null)
            {
                host.Close();
                host = null;
            }
        }
    }
}
