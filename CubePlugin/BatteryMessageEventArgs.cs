﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CubePlugin
{
    public delegate void BatteryReceivedEventHandler(object sender, BatteryMessageEventArgs e);
    public class BatteryMessageEventArgs : EventArgs
    {
        public double BatteryPercentage { get; private set; }
        public double BatteryVoltage { get; private set; }
        public int DatabaseEntryID { get; private set; }
        public string DeviceID { get; private set; }
        
        public BatteryMessageEventArgs(string rawMessage)
        {
            if (!string.IsNullOrEmpty(rawMessage))
            {
                string [] parts = rawMessage.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 4)// Don't allow messages in unexpected format. TODO test with appended commas.
                {
                    int temp;
                    if (int.TryParse(parts[0], out temp))
                        DatabaseEntryID = temp;
                    DeviceID = Regex.Match(parts[1], @"\d+").Value;// Discard BAT string as it's always the same.
                    double voltage;
                    if (double.TryParse(parts[2], out voltage))
                        BatteryVoltage = voltage;
                    if (double.TryParse(parts[3], out voltage))
                        BatteryPercentage = voltage;
                }
                else
                {
                    throw new FormatException("Unable to parse message");
                }
            }
        }
    }
}
