﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubePlugin
{
    public class CubeStatus
    {
        public DateTime LastMessage { get; set; }
        public bool Online { get; set; }
        public CubeStatus(bool online, DateTime lastMessage)
        {
            LastMessage = lastMessage;
            Online = online;
        }
        public override string ToString()
        {
            string message = Online ? "Online;" : "Offline;";
            return message + "last message:" + LastMessage;
        }
    }
}
