﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace CubePlugin
{
    public class CubeWebService : ICubeWebService
    {
        public static string LastAddress { get { return lastAddress; } }
        public static event AlarmReceivedEventHandler AlarmEvent;
        public static event BatteryReceivedEventHandler BatteryEvent;
        public static event LocationReceivedEventHandler LocationEvent;
        public static event StatusReceivedEventHandler StatusEvent;
        public static event GenericMessageEventHandler MessageEvent;

        private static string lastAddress;
        public string EchoWithGet(string s)
        {
            if (s != null)
            {
                GetAddress();
                ReadMessage(s);
            }
            return s;
        }

        public string EchoWithPost(string s)
        {
            return s;
        }

        private void GetAddress()
        {
            OperationContext context = OperationContext.Current;
            MessageProperties prop = context.IncomingMessageProperties;
            HttpRequestMessageProperty loadBalancer = prop[HttpRequestMessageProperty.Name] as HttpRequestMessageProperty;
            if (string.IsNullOrEmpty(loadBalancer.Headers["X-Forwarded-For"]))
            {
                RemoteEndpointMessageProperty endpoint = prop[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                if (!string.IsNullOrEmpty(endpoint.Address))
                    lastAddress = endpoint.Address;
            }
            else
            {
                lastAddress = loadBalancer.Headers["X-Forwarded-For"];
            }
        }

        private void ReadMessage(string rawMessage)
        {
            if (!string.IsNullOrEmpty(rawMessage))
            {
                string device = string.Empty;
                string[] parts = rawMessage.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                switch (parts.Length)
                {
                    case 3://Status message.
                        StatusMessageEventArgs status = new StatusMessageEventArgs(rawMessage);
                        if (StatusEvent != null)
                            StatusEvent(this, status);
                         device = status.DeviceID;
                        break;
                    case 4://Location message OR Battery message
                        if (rawMessage.ToUpper().Contains("BAT"))
                        {
                            BatteryMessageEventArgs bat = new BatteryMessageEventArgs(rawMessage);
                            if (BatteryEvent != null)
                                BatteryEvent(this, bat);
                            device = bat.DeviceID;
                        }
                        else
                        {
                            LocationMessageEventArgs location = new LocationMessageEventArgs(rawMessage);
                            if (LocationEvent != null)
                                LocationEvent(this, location);
                            device = location.DeviceID;
                        }
                        break;
                    case 5://Alarm message
                        AlarmMessageEventArgs alarm = new AlarmMessageEventArgs(rawMessage);
                        if (AlarmEvent != null)
                            AlarmEvent(this, alarm);
                        device = alarm.DeviceID;
                        break;
                    default:// Unknown - log text.
                        break;
                }

                if (!string.IsNullOrEmpty(device))
                {
                    if (MessageEvent != null)
                        MessageEvent(this, new GenericMessageEventArgs(device));
                }
            }
        }
    }
}
