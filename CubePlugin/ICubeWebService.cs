﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace CubePlugin
{
    [ServiceContract]
    public interface ICubeWebService
    {
        [OperationContract]
        [WebGet]
        string EchoWithGet(string s);

        [OperationContract]
        [WebInvoke]
        string EchoWithPost(string s);
    }
}
