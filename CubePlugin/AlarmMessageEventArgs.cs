﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CubePlugin
{
    public enum AlarmType
    {
        Default,
        Vibration,
        Audio
    }
    public delegate void AlarmReceivedEventHandler(object sender, AlarmMessageEventArgs e);
    public class AlarmMessageEventArgs : EventArgs
    {
        public AlarmType AlarmType { get; private set; }
        public int DatabaseEntryID { get; private set; }
        public string DeviceID { get; private set; }
        public DateTime TimeStamp { get; private set; }
        
        public AlarmMessageEventArgs(string rawMessage)
        {
            if (!string.IsNullOrEmpty(rawMessage))
            {
                string [] parts = rawMessage.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 5)// Don't allow messages in unexpected format. TODO test with appended commas.
                {
                    int temp;
                    if (int.TryParse(parts[0], out temp))
                        DatabaseEntryID = temp;
                    DeviceID = Regex.Match(parts[1], @"\d+").Value;// Discard ALM string as it's always the same.
                    if (parts[2] == "1")
                        AlarmType = AlarmType.Vibration;
                    else if (parts[3] == "1")
                        AlarmType = AlarmType.Audio;
                    DateTime dateTime;
                    if (DateTime.TryParseExact(parts[4], new string[] { "yy/MM/dd-HH:mm:ss", "yy/MM/dd-H:mm:ss" }, 
                        CultureInfo.CurrentUICulture, DateTimeStyles.AssumeUniversal, out dateTime))
                        TimeStamp = dateTime;
                    else
                        TimeStamp = DateTime.UtcNow;
                }
                else
                {
                    throw new FormatException("Unable to parse message");
                }
            }
        }
    }
}
