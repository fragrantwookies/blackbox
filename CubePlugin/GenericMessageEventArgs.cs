﻿using System;

namespace CubePlugin
{
    public delegate void GenericMessageEventHandler(object sender, GenericMessageEventArgs e);
    public class GenericMessageEventArgs : EventArgs
    {
        public string DeviceID { get; private set; }
        
        public GenericMessageEventArgs(string device)
        {
            if (!string.IsNullOrEmpty(device))
            {
                    DeviceID = device;
            }
        }
    }
}
