﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CubePlugin
{
    public struct Location
    {
        private double _lattitude;
        private double _longitude;
        public double Lattitude { get { return _lattitude; } private set { _lattitude = value; } }
        public double Longitude { get { return _longitude; } private set { _longitude = value;} }
        public Location(double lattitude, double longitude)
        {
            _lattitude = lattitude;
            _longitude = longitude;
        }
    }
    public delegate void LocationReceivedEventHandler(object sender, LocationMessageEventArgs e);
    public class LocationMessageEventArgs : EventArgs
    {
        public int DatabaseEntryID { get; private set; }
        public string DeviceID { get; private set; }
        public Location Location { get; private set; }
        
        public LocationMessageEventArgs(string rawMessage)
        {
            if (!string.IsNullOrEmpty(rawMessage))
            {
                string [] parts = rawMessage.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 4)// Don't allow messages in unexpected format. TODO test with appended commas.
                {
                    int temp;
                    if (int.TryParse(parts[0], out temp))
                        DatabaseEntryID = temp;
                    DeviceID = Regex.Match(parts[1], @"\d+").Value;// Discard LOC string as it's always the same.
                    double lattitude;
                    if (double.TryParse(parts[2], out lattitude))
                    {
                        double longitude;
                        if (double.TryParse(parts[3], out longitude))
                        {
                            Location = new Location(lattitude, longitude);
                        }
                    }
                }
                else
                {
                    throw new FormatException("Unable to parse message");
                }
            }
        }
    }
}
