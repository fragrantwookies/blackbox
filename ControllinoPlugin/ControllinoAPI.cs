﻿using ControllinoPlugin.EventArguments;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControllinoPlugin
{
    public class ControllinoAPI : IDisposable
    {
        public ControllinoAPI()
        {
            ResponseWaitTimeout = 3000;
            PingTimeout = 30000;
            PingInterval = 10000;
            PollIntervalAnalogIn = 10000;
            AnalogFilter = "000000000000";
        }

        #region Dispose
        ~ControllinoAPI()
        {            
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(pingTimer != null)
            {
                pingTimer.Elapsed -= pingTimer_Elapsed;
                pingTimer.Dispose();
                pingTimer = null;
            }

            if (pollTimer != null)
            {
                pollTimer.Elapsed -= pollTimer_Elapsed;
                pollTimer.Dispose();
                pollTimer = null;
            }

            Disconnect();
        }
        #endregion

        public string IpAddress { get; set; }
        public int SendPort { get; set; }
        public int ReceivePort { get; set; }

        public int ResponseWaitTimeout { get; set; }
        public int PingInterval { get; set; }
        public int PingTimeout { get; set; }
        public int PollIntervalAnalogIn { get; set; }

        private bool online = false;

        public const int DigitalInputs = 10;
        public const int AnalogInputs = 10;
        public const int TemperatureInputs = 10;
        public const int DigitalOutputs = 12;
        public const int RelayOutputs = 10;
        
        /// <summary>
        /// A for Analog value, T for Temperature, 0 to igonore
        /// </summary>
        public string AnalogFilter { get; set; }

        private IPEndPoint inEP;
        private IPEndPoint outEP;
        private UdpClient inbox;
        private UdpClient outbox;

        private bool listen;

        private DateTime lastPing;
        private System.Timers.Timer pingTimer;
        private System.Timers.Timer pollTimer;

        private Queue<string> outboundMessages;
                            
        public event EventHandler<AnalogEventArgs> ReceivedAnalog;
        public event EventHandler<AnalogEventArgs> ReceivedTemp;
        public event EventHandler<DigitalEventArgs> ReceivedDigitalOut;
        public event EventHandler<DigitalEventArgs> ReceivedDigitalIn;
        public event EventHandler<DigitalEventArgs> ReceivedRelay;
        public event EventHandler<ErrorEventArgs> Error;
        public event EventHandler<OfflineEventArgs> Offline;
        public event EventHandler<EventArgs> Online;
        public event EventHandler<AckNackEventArgs> Nack;
        public event EventHandler<AckNackEventArgs> Ack;

        #region Connect and disconnect
        public bool Connect()
        {
            bool result = false;

            try
            {
                inEP = new IPEndPoint(System.Net.IPAddress.Parse(IpAddress), ReceivePort);
                outEP = new IPEndPoint(System.Net.IPAddress.Parse(IpAddress), SendPort);

                if (inbox != null)
                    inbox.Close();

                inbox = new UdpClient(ReceivePort);

                if (outbox != null)
                    outbox.Close();

                outbox = new UdpClient(SendPort);

                Thread readThread = new Thread(() => { Read(); });
                readThread.Start();

                if (pingTimer == null)
                {
                    pingTimer = new System.Timers.Timer();
                    pingTimer.Elapsed += pingTimer_Elapsed;
                }

                if (PingTimeout < PingInterval * 2)
                    PingTimeout = PingInterval * 2;

                if (PingTimeout < ResponseWaitTimeout * 2)
                    PingTimeout = ResponseWaitTimeout * 2;

                pingTimer.Interval = PingInterval;
                pingTimer.Start();

                if (pollTimer == null)
                {
                    pollTimer = new System.Timers.Timer();
                    pollTimer.Elapsed += pollTimer_Elapsed;
                }

                pollTimer.Interval = PollIntervalAnalogIn;
                pollTimer.Start();

                SendMessage("GET,HEART");

                result = true;
            }
            catch(Exception ex)
            {
                if (Error != null)
                    Error(this, new ErrorEventArgs("Error connecting controllino", ex));
            }
            
            return result;
        }

        public void Disconnect()
        {
            try
            {
                listen = false;
                online = false;

                if (pingTimer != null)
                    pingTimer.Stop();

                if (pollTimer != null)
                    pollTimer.Stop();

                if (inbox != null)
                    inbox.Close();
                inbox = null;

                if (outbox != null)
                    outbox.Close();
                outbox = null;
            }            
            catch(Exception ex)
            {
                if (Error != null)
                    Error(this, new ErrorEventArgs("Error disconnecting Controllino", ex));
            }
        }
        #endregion
        
        #region Read
        private void Read()
        {
            listen = true;
            IPEndPoint ep = new IPEndPoint(System.Net.IPAddress.Any, 0);

            byte[] ack = Encoding.ASCII.GetBytes("ACK\0");

            while (listen && inbox != null)
            {
                try
                {
                    byte[] packet = inbox.Receive(ref inEP);

                    inbox.Send(ack, ack.Length, inEP);

                    string message = Encoding.ASCII.GetString(packet, 0, packet.Length);

                    ThreadPool.QueueUserWorkItem(state => ProcessMessage(message));
                }
                catch (SocketException ex)
                {
                    //Ignore
                    break;
                }
                catch (Exception ex)
                {
                    if (Error != null)
                        Error(this, new ErrorEventArgs("Error reading from Controllino", ex));
                }
            }
        }

        private void ProcessMessage(string _message)
        {
            if (_message.ToUpper() == "ACK" || _message.ToUpper() == "STARTUP")
            {
                lastPing = DateTime.Now;

                if (!online)
                {
                    online = true;

                    if (Online != null)
                        Online(this, new EventArgs());
                }
            }
            else if(_message.ToUpper() == "NACK")
            {
                if (Nack != null)
                    Nack(this, new AckNackEventArgs(_message));
            }
            else
            {
                string[] msgParts = _message.Split(':');

                if (msgParts.Length == 3)
                {
                    switch (msgParts[0].ToUpper())
                    {
                        case "ALOG":
                            {
                                int port;
                                double val;

                                if (ReceivedAnalog != null && int.TryParse(msgParts[1], out port) && double.TryParse(msgParts[2], out val))
                                    ReceivedAnalog(this, new AnalogEventArgs(port, val));

                                break;
                            }
                        case "TEMP":
                            {
                                int port;
                                double val;

                                if (ReceivedTemp != null && int.TryParse(msgParts[1], out port) && double.TryParse(msgParts[2], out val))
                                    ReceivedTemp(this, new AnalogEventArgs(port, val));

                                break;
                            }
                        case "DO":
                            {
                                int port;
                                bool val;

                                if (ReceivedDigitalOut != null && int.TryParse(msgParts[1], out port) && bool.TryParse(msgParts[2], out val))
                                    ReceivedDigitalOut(this, new DigitalEventArgs(port, val));

                                break;
                            }
                        case "DI":
                            {
                                int port;
                                bool val;

                                if (ReceivedDigitalIn != null && int.TryParse(msgParts[1], out port) && bool.TryParse(msgParts[2], out val))
                                    ReceivedDigitalIn(this, new DigitalEventArgs(port, val));

                                break;
                            }
                        case "R":
                            {
                                int port;
                                bool val;

                                if (ReceivedRelay != null && int.TryParse(msgParts[1], out port) && bool.TryParse(msgParts[2], out val))
                                    ReceivedRelay(this, new DigitalEventArgs(port, val));

                                break;
                            }
                        default:
                            {
                                if(Error != null)
                                    Error(this, new ErrorEventArgs(string.Format("Unknown message received: {0}", _message)));

                                break;
                            }
                    }
                }
            }
        }
        #endregion

        #region Send
        private void SendMessage(string _message)
        {
            if (outboundMessages == null)
                outboundMessages = new Queue<string>();

            outboundMessages.Enqueue(_message);

            if (!sending)
                ThreadPool.QueueUserWorkItem(state => SendMessages());
        }

        bool sending = false;

        private UdpClient CreateOutbox()
        {
            return new UdpClient();
        }

        private void SendMessages()
        {
            sending = true;

            try
            {
                if (outbox != null)
                {
                    lock (outbox)
                    {
                        while (outbox != null && outboundMessages.Count > 0)
                        {
                            string msgString = outboundMessages.Dequeue();
                            byte[] data = Encoding.ASCII.GetBytes(msgString + "\0");

                            outbox.Send(data, data.Length, outEP);

                            Stopwatch stopwatch = new Stopwatch();
                            stopwatch.Start();
                            while (outbox != null && outbox.Available <= 0 && stopwatch.ElapsedMilliseconds < ResponseWaitTimeout)
                                Thread.Sleep(10);

                            stopwatch.Stop();

                            if (outbox != null && outbox.Available <= 0)
                            {
                                if (Error != null)
                                    Error(this, new ErrorEventArgs(string.Format("Response wait timed out for {0} after {1}ms", msgString, stopwatch.ElapsedMilliseconds.ToString())));
                            }
                            else
                            {
                                while (outbox != null && outbox.Available > 0)
                                {
                                    IPEndPoint receiveEP = new IPEndPoint(System.Net.IPAddress.Parse(IpAddress), SendPort);
                                    byte[] packet = outbox.Receive(ref receiveEP);

                                    if (receiveEP.Address.ToString() == IpAddress && receiveEP.Port == SendPort)
                                    {
                                        string message = Encoding.ASCII.GetString(packet, 0, packet.Length);

                                        if (message.ToUpper() == "ACK")
                                        {
                                            lastPing = DateTime.Now;

                                            if (Ack != null)
                                                Ack(this, new AckNackEventArgs(string.Format("{0} - {1}", msgString, outEP.Address.ToString())));

                                            if (!online)
                                            {
                                                online = true;

                                                if (Online != null)
                                                    Online(this, new EventArgs());
                                            }
                                        }
                                        else
                                        {
                                            if (Nack != null)
                                                Nack(this, new AckNackEventArgs(string.Format("{0} - {1}", msgString, outEP.Address.ToString())));
                                        }
                                    }
                                    else
                                    {
                                        string nope = "nope";
                                    }
                                }
                            }
                        }
                    }

                    Thread.Sleep(10);
                }
            }
            catch (Exception ex)
            {
                if (Error != null)
                    Error(this, new ErrorEventArgs("Error sending messages to Controllino", ex));
            }
            finally
            {
                sending = false;
            }
        }

        public void GetAnalog(int _portNumber)
        {
            SendMessage(string.Format("GET,ANALOG:{0}", _portNumber.ToString()));
        }

        public void GetTemperature(int _portNumber)
        {
            SendMessage(string.Format("GET,TEMP:{0}", _portNumber.ToString()));
        }

        public void GetDigitalInputState(int _portNumber)
        {
            SendMessage(string.Format("GET,DI:{0}", _portNumber.ToString()));
        }

        public void GetALLDigitalInputStates()
        {
            SendMessage("GET,DIS");
        }

        public void GetDigitalOutputState(int _portNumber)
        {
            SendMessage(string.Format("GET,DO:{0}", _portNumber.ToString()));
        }

        public void GetALLDigitalOutputStates()
        {
            SendMessage("GET,DOS");
        }

        public void SetDigitalOutput(int _portNumber, bool _state)
        {
            SendMessage(string.Format("{0},DO:{1}", _state ? "ON" : "OFF", _portNumber.ToString()));
        }

        public void GetRelayState(int _portNumber)
        {
            SendMessage(string.Format("GET,RELAYSTATE:{0}", _portNumber.ToString()));
        }

        public void GetALLRelayStates()
        {
            SendMessage("GET,RELAYS");
        }

        public void SetRelayState(int _portNumber, bool _state)
        {
            SendMessage(string.Format("{0},RELAY:{1}", _state ? "ON" : "OFF", _portNumber.ToString()));
        }
        #endregion

        void pingTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            DateTime now = DateTime.Now;

            if ((now - lastPing).TotalMilliseconds > PingTimeout)
            {
                if (online && Offline != null)
                    Offline(this, new OfflineEventArgs(lastPing));
                
                online = false;
            }

            SendMessage("GET,HEART");
        }

        void pollTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            for(int i = 0; i < AnalogFilter.Length; i++)
            {
                switch(AnalogFilter[i])
                {
                    case 'A':
                        GetAnalog(i);
                        break;
                    case 'T':
                        GetTemperature(i);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
