﻿using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ControllinoPlugin
{
    public class ControllinoPlugin : INerveEventPlugin, INerveActionPlugin, INerveRelaySwitchPlugin, INerveDigitalOutputSwitchPlugin, IDisposable
    {
        ControllinoAPI controllino;

        public event EventHandler<PluginTriggerEventArgs> Trigger;                                                                  
        public event EventHandler<PluginErrorEventArgs> Error;                                                                  
        public event EventHandler DeviceOnline;                                                                  
        public event EventHandler DeviceOffline;

        #region Dispose
        ~ControllinoPlugin()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (controllino != null)
            {
                controllino.Disconnect();
                controllino.Ack -= controllino_Ack;
                controllino.Error -= controllino_Error;
                controllino.Nack -= controllino_Nack;
                controllino.Offline -= controllino_Offline;
                controllino.Online -= controllino_Online;
                controllino.ReceivedAnalog -= controllino_ReceivedAnalog;
                controllino.ReceivedDigitalIn -= controllino_ReceivedDigitalIn;
                controllino.ReceivedDigitalOut -= controllino_ReceivedDigitalOut;
                controllino.ReceivedRelay -= controllino_ReceivedRelay;
                controllino.ReceivedTemp -= controllino_ReceivedTemp;
                controllino.Dispose();
                controllino = null;
            }

            Online = false;
        }
        #endregion
        
        public void Start()
        {
            if(controllino == null)
            {
                controllino = new ControllinoAPI();
                controllino.Ack += controllino_Ack;
                controllino.Error += controllino_Error;
                controllino.Nack += controllino_Nack;
                controllino.Offline += controllino_Offline;
                controllino.Online += controllino_Online;
                controllino.ReceivedAnalog += controllino_ReceivedAnalog;
                controllino.ReceivedDigitalIn += controllino_ReceivedDigitalIn;
                controllino.ReceivedDigitalOut += controllino_ReceivedDigitalOut;
                controllino.ReceivedRelay += controllino_ReceivedRelay;
                controllino.ReceivedTemp += controllino_ReceivedTemp;
            }

            controllino.AnalogFilter = Properties["AnalogFilter"];
            controllino.IpAddress = Properties["IpAddress"];

            string err;
            int pingInterval;
            if(TimeSpanParser.TryParse(Properties["PingInterval"], out pingInterval, out err))
                controllino.PingInterval = pingInterval;
            else if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing PingInterval: {0}", err), null));

            int pingTimeOut;
            if(TimeSpanParser.TryParse(Properties["PingTimeout"], out pingTimeOut, out err))
                controllino.PingTimeout = pingTimeOut;
            else if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing PingTimeout: {0}", err), null));

            int pollIntervalAnalogIn;
            if(TimeSpanParser.TryParse(Properties["PollIntervalAnalogIn"], out pollIntervalAnalogIn, out err))
            controllino.PollIntervalAnalogIn = pollIntervalAnalogIn;
            else if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing PollIntervalAnalogIn: {0}", err), null));

            int receivePort;
            if(int.TryParse(Properties["ReceivePort"], out receivePort))
                controllino.ReceivePort = receivePort;
            else if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing ReceivePort: {0}", Properties["ReceivePort"]), null));

            int responseWaitTimeout;
            if(TimeSpanParser.TryParse(Properties["ResponseWaitTimeout"], out responseWaitTimeout, out err))
                controllino.ResponseWaitTimeout = responseWaitTimeout;
            else if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing ResponseWaitTimeout: {0}", err), null));

            int sendPort;
            if(int.TryParse(Properties["SendPort"], out sendPort))
                controllino.SendPort = sendPort;
            else if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), string.Format("Error parsing SendPort: {0}", Properties["SendPort"]), null));

            controllino.Connect();
        }

        public bool Online { get; set; }

        public bool Enabled { get; set; }

        public INerveHostPlugin Host { get; set; }

        public string Name { get; set; }

        public string Description
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>().Description;
            }
        }

        public string Author
        {
            get
            {
                return Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyCompanyAttribute>().Company;
            }
        }

        public string Version
        {
            get
            {
                return Assembly.GetCallingAssembly().GetName().Version.ToString();
            }
        }

        private string fileName;

        public string FileName
        {
            get
            {
                return string.IsNullOrWhiteSpace(fileName) ? Assembly.GetCallingAssembly().Location : fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public string[] EventNames
        {
            get
            {
                if (controllino != null)
                {
                    string[] result = new string[ControllinoAPI.DigitalInputs + ControllinoAPI.AnalogInputs + ControllinoAPI.TemperatureInputs];

                    for (int i = 0; i < ControllinoAPI.DigitalInputs; i++)
                    {
                        result[i] = LookupEventName("DigitalInput", i);
                    }

                    for (int i = 0; i < ControllinoAPI.AnalogInputs; i++)
                    {
                        result[i] = LookupEventName("AnalogInput", i);
                    }

                    for (int i = 0; i < ControllinoAPI.TemperatureInputs; i++)
                    {
                        result[i] = LookupEventName("AnalogInput", i);
                    }

                    return result;
                }
                else
                    return new string[0];
            }
        }

        public bool HasWizard
        {
            get
            {
                return false;
            }
        }

        public IPluginWizard Wizard
        {
            get
            {
                return null;
            }
        } 

        private string LookupEventName(string _Prefix, int _IONumber)
        {
            string result = Properties[string.Format("{0}{1}", _Prefix, _IONumber.ToString())];

            return string.IsNullOrWhiteSpace(result) ? string.Format("{0}{1}", _Prefix, _IONumber.ToString()) : result;
        }

        private CustomProperties properties;
        public CustomProperties Properties
        {
            get
            {
                if (properties == null)
                    properties = new CustomProperties();

                return properties;
            }
        }

        public List<CustomPropertyDefinition> CustomPropertyDefinitions
        {
            get
            {
                List<CustomPropertyDefinition> result = new List<CustomPropertyDefinition>();
                result.Add(new CustomPropertyDefinition("IpAddress", "IP Address", "IP Address", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("ReceivePort", "Receive Port", "Receive Port", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("SendPort", "Send Port", "Send Port", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("PingInterval", "Ping Interval", "Ping Interval", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("PingTimeout", "Ping Timeout", "Ping Timeout", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("ResponseWaitTimeout", "Response Wait Timeout", "Response Wait Timeout", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("AnalogFilter", "Analog Filter", "Analog Filter", typeof(CustomProperty)));
                result.Add(new CustomPropertyDefinition("PollIntervalAnalogIn", "Poll Interval Analog Input", "Poll Interval Analog Input", typeof(CustomProperty)));
                return result;
            }
        }

        public string[] MiscellaneousEventNames
        {
            get
            {
                return null;
            }
        }

        public bool Equals(INervePlugin other)
        {
            if (Author == other.Author 
                    && Description == other.Description 
                    && FileName == other.FileName 
                    && Host == other.Host 
                    && Name == other.Name 
                    && Online == other.Online 
                    && Version == other.Version)
                return true;
            else
                return false;
        }

        public Type GetActionType(string actionType = "")
        {
            return typeof(ControllinoAction);
        }

        public IEnumerable<Type> GetActionTypes()
        {
            return new Type[] { typeof(ControllinoAction) };
        }

        public bool PerformAction(string eventName, IEnumerable<IEventTrigger> triggers, IAction action)
        {
            if (controllino != null)
            {
                ControllinoAction controllinoAction = action as ControllinoAction;

                if (controllinoAction != null)
                {
                    int portNo = int.Parse(controllinoAction.OutputIndex);
                    bool outputValue = controllinoAction.OutputValue.ToLower().Contains('t');

                    int delay = 0;
                    if (int.TryParse(controllinoAction.Delay, out delay) && delay > 0)
                        Thread.Sleep(delay);

                    if (controllinoAction.OutputType.ToLower().Contains("relay"))
                        controllino.SetRelayState(portNo, outputValue);
                    else if (controllinoAction.OutputType.ToLower().Contains("digital"))
                        controllino.SetDigitalOutput(portNo, outputValue);
                }
            }

            return true;
        }

        public void SwitchRelay(int index, bool value, int duration = 0)
        {
            if (controllino != null)
            {
                if (duration >= 0)
                {
                    controllino.SetRelayState(index, value);

                    if (duration > 0)
                    {
                        ThreadPool.QueueUserWorkItem((state) => { SwitchRelay(index, !value, ~duration); });
                    }
                }
                else if (duration < 0)
                {
                    Thread.Sleep(~duration);

                    controllino.SetRelayState(index, value);
                }
            }
        }

        public void SwitchOutput(int index, bool value, int duration = 0)
        {
            if (controllino != null)
            {
                if (duration >= 0)
                {
                    controllino.SetDigitalOutput(index, value);

                    if (duration > 0)
                    {
                        ThreadPool.QueueUserWorkItem((state) => { SwitchOutput(index, !value, ~duration); });
                    }
                }
                else if (duration < 0)
                {
                    Thread.Sleep(~duration);

                    controllino.SetDigitalOutput(index, value);
                }
            }
        }

        void controllino_ReceivedDigitalIn(object sender, EventArguments.DigitalEventArgs e)
        {
            if (Trigger != null)
                Trigger(this, new PluginTriggerEventArgs(new ControllinoTrigger()
                {
                    TriggerID = Guid.NewGuid(),
                    DeviceName = this.Name,
                    DetectorGroupIndex = 0,
                    DetectorIndex = e.Port,
                    DetectorName = string.Format("DigitalInput{0}", e.Port.ToString()),
                    TriggerDT = DateTime.Now,
                    EventName = LookupEventName("DigitalInput", e.Port),
                    DetectorType = DetectorType.Digital,
                    State = e.Value,
                    Data = XMLSerializer.Serialize(e)
                }));
        }

        void controllino_ReceivedAnalog(object sender, EventArguments.AnalogEventArgs e)
        {
            if (Trigger != null)
                Trigger(this, new PluginTriggerEventArgs(new ControllinoTrigger()
                    {
                        TriggerID = Guid.NewGuid(),
                        DeviceName = this.Name,
                        DetectorGroupIndex = 1,
                        DetectorIndex = e.Port,
                        DetectorName = string.Format("AnalogInput{0}", e.Port.ToString()),
                        TriggerDT = DateTime.Now,
                        EventName = LookupEventName("AnalogInput", e.Port),
                        DetectorType = DetectorType.Analog,
                        EventValue = e.Value,
                        Data = XMLSerializer.Serialize(e)
                    }));
        }

        void controllino_ReceivedTemp(object sender, EventArguments.AnalogEventArgs e)
        {
            if (Trigger != null)
                Trigger(this, new PluginTriggerEventArgs(new ControllinoTrigger()
                    {
                        TriggerID = Guid.NewGuid(),
                        DeviceName = this.Name,
                        DetectorGroupIndex = 1,
                        DetectorIndex = e.Port,
                        DetectorName = string.Format("AnalogInput{0}", e.Port.ToString()),
                        TriggerDT = DateTime.Now,
                        EventName = LookupEventName("AnalogInput", e.Port),
                        DetectorType = DetectorType.Analog,
                        EventValue = e.Value,
                        Data = XMLSerializer.Serialize(e)
                    }));
        }

        void controllino_ReceivedDigitalOut(object sender, EventArguments.DigitalEventArgs e)
        {
            //not used
        }

        void controllino_ReceivedRelay(object sender, EventArguments.DigitalEventArgs e)
        {
            //not used
        }

        void controllino_Offline(object sender, EventArguments.OfflineEventArgs e)
        {
            Online = false;

            if (DeviceOffline != null)
                DeviceOffline(this, new EventArgs());
        }

        void controllino_Online(object sender, EventArgs e)
        {
            Online = true;

            if (DeviceOnline != null)
                DeviceOnline(this, new EventArgs());
        }

        void controllino_Ack(object sender, EventArguments.AckNackEventArgs e)
        {
            if (!Online && DeviceOnline != null)
                DeviceOnline(this, new EventArgs());

            Online = true;
        }

        void controllino_Nack(object sender, EventArguments.AckNackEventArgs e)
        {
            if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), string.Format("Controllino received a nack for this message: {0}", e.Message), null));
        }

        void controllino_Error(object sender, EventArguments.ErrorEventArgs e)
        {
            if (Error != null)
                Error(this, new PluginErrorEventArgs(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), e.Message, e.Exception));
        }
    }
}
