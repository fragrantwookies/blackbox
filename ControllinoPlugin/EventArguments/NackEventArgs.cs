using System;
using System.Collections.Generic;
using System.Linq;

namespace ControllinoPlugin.EventArguments
{
    public class AckNackEventArgs : EventArgs
    {
        public string Message { get; set; }

        public AckNackEventArgs()
        {
            Message = "";
        }

        public AckNackEventArgs(string _Message)
        {
            Message = _Message;
        }
    }
}
