﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllinoPlugin.EventArguments
{
    public class AnalogEventArgs : EventArgs
    {
        public int Port { get; set; }
        public double Value { get; set; }

        public AnalogEventArgs() {}

        public AnalogEventArgs(int _Port, double _Value)        
        {
            Port = _Port;
            Value = _Value;
        }
    }
}