using System;
using System.Collections.Generic;
using System.Linq;

namespace ControllinoPlugin.EventArguments
{
    public class OfflineEventArgs : EventArgs
    {
        public DateTime LastSeen { get; set; }

        public OfflineEventArgs()
        {
            LastSeen = DateTime.MinValue;
        }

        public OfflineEventArgs(DateTime _LastSeen)
        {
            LastSeen = _LastSeen;
        }
    }
}