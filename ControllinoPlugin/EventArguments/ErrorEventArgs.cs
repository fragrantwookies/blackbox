﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllinoPlugin.EventArguments
{
    public class ErrorEventArgs : EventArgs
    {
        public string Message { get; set; }

        public Exception Exception { get; set; }

        public ErrorEventArgs()
        {
            Message = "";
            Exception = null;
        }

        public ErrorEventArgs(string _Message)
        {
            Message = _Message;
            Exception = null;
        }

        public ErrorEventArgs(string _Message, Exception _Exception)
        {
            Message = _Message;
            Exception = _Exception;
        }
    }
}