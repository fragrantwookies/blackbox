using System;
using System.Collections.Generic;
using System.Linq;

namespace ControllinoPlugin.EventArguments
{
    public class DigitalEventArgs : EventArgs
    {
        public int Port { get; set; }
        public bool Value { get; set; }

        public DigitalEventArgs() { }

        public DigitalEventArgs(int _Port, bool _Value)
        {
            Port = _Port;
            Value = _Value;
        }
    }
}
