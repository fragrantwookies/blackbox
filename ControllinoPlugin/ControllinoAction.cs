﻿using eNervePluginInterface;
using eNervePluginInterface.CustomAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControllinoPlugin
{
    class ControllinoAction : PluginAction
    {
        public ControllinoAction() : base() {}

        public ControllinoAction(string _name, bool _value, string _pluginName, int _actionOrder = 0)
            : base(_name, _value, _pluginName, _actionOrder) {}

        /// <summary>
        /// Relay or DigitalOut
        /// </summary>
        [DisplayInPluginActionPropertiesList]
        public string OutputType { get; set; }
        
        /// <summary>
        /// 0-11 for DigitalOut
        /// 0-9 for Relay
        /// </summary>
        [DisplayInPluginActionPropertiesList]
        public string OutputIndex { get; set; }

        /// <summary>
        /// True or False
        /// </summary>
        [DisplayInPluginActionPropertiesList]
        public string OutputValue { get; set; }

        /// <summary>
        /// Delay in miliseconds before you trigger the relay
        /// </summary>
        [DisplayInPluginActionPropertiesList]
        public string Delay { get; set; }
    }
}
