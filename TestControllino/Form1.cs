﻿using ControllinoPlugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestControllino
{
    public partial class Form1 : Form
    {
        ControllinoAPI controllino;

        public Form1()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (controllino != null)
                controllino.Dispose();

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void WriteToList(string _message)
        {
            if (this.InvokeRequired)
                this.Invoke((MethodInvoker)(() => { WriteToList(_message); }));
            else
                lstMessages.Items.Insert(0, _message);
        }

        private void SetRelay(int _port, bool _val)
        {
            if (this.InvokeRequired)
                this.Invoke((MethodInvoker)(() => { SetRelay(_port, _val); }));
            else
            {
                switch (_port)
                {
                    case 0:
                        cx0.Checked = _val;
                        break;
                    case 1:
                        cx1.Checked = _val;
                        break;
                    case 2:
                        cx2.Checked = _val;
                        break;
                    case 3:
                        cx3.Checked = _val;
                        break;
                    case 4:
                        cx4.Checked = _val;
                        break;
                    case 5:
                        cx5.Checked = _val;
                        break;
                    case 6:
                        cx6.Checked = _val;
                        break;
                    case 7:
                        cx7.Checked = _val;
                        break;
                    case 8:
                        cx8.Checked = _val;
                        break;
                    case 9:
                        cx9.Checked = _val;
                        break;
                    default:
                        break;
                }
            }
        }

        void controllino_ReceivedTemp(object sender, ControllinoPlugin.EventArguments.AnalogEventArgs e)
        {
            WriteToList(string.Format("Temperature for port {0} is {1} degrees celcius", e.Port.ToString(), e.Value.ToString()));
        }

        void controllino_ReceivedRelay(object sender, ControllinoPlugin.EventArguments.DigitalEventArgs e)
        {
            WriteToList(string.Format("Relay state for port {0} is {1}", e.Port.ToString(), e.Value.ToString()));

            SetRelay(e.Port, e.Value);
        }

        void controllino_ReceivedDigitalOut(object sender, ControllinoPlugin.EventArguments.DigitalEventArgs e)
        {
            WriteToList(string.Format("Output state for port {0} is {1}", e.Port.ToString(), e.Value.ToString()));
        }

        void controllino_ReceivedDigitalIn(object sender, ControllinoPlugin.EventArguments.DigitalEventArgs e)
        {
            WriteToList(string.Format("Digital Input value for port {0} is {1}", e.Port.ToString(), e.Value.ToString()));
        }

        void controllino_ReceivedAnalog(object sender, ControllinoPlugin.EventArguments.AnalogEventArgs e)
        {
            WriteToList(string.Format("Analog Input value for port {0} is {1}", e.Port.ToString(), e.Value.ToString()));
        }

        void controllino_Offline(object sender, ControllinoPlugin.EventArguments.OfflineEventArgs e)
        {
            WriteToList(string.Format("Device Offline since {0}", e.LastSeen.ToString(@"yyyy/MM/dd HH:mm:ss")));
        }

        void controllino_Ack(object sender, ControllinoPlugin.EventArguments.AckNackEventArgs e)
        {
            //WriteToList(string.Format("Ack: {0}", e.Message));
        }

        void controllino_Nack(object sender, ControllinoPlugin.EventArguments.AckNackEventArgs e)
        {
            WriteToList(string.Format("Nack: {0}", e.Message));
        }

        void controllino_Error(object sender, ControllinoPlugin.EventArguments.ErrorEventArgs e)
        {
            WriteToList(string.Format("Error: {0} - {1}", e.Message, e.Exception != null ? e.Exception.Message : ""));
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (btnConnect.Text == "Connect")
            {
                if (controllino == null)
                {
                    controllino = new ControllinoAPI();
                    controllino.Error += controllino_Error;
                    controllino.Nack += controllino_Nack;
                    controllino.Ack += controllino_Ack;
                    controllino.Offline += controllino_Offline;
                    controllino.ReceivedAnalog += controllino_ReceivedAnalog;
                    controllino.ReceivedDigitalIn += controllino_ReceivedDigitalIn;
                    controllino.ReceivedDigitalOut += controllino_ReceivedDigitalOut;
                    controllino.ReceivedRelay += controllino_ReceivedRelay;
                    controllino.ReceivedTemp += controllino_ReceivedTemp;
                }
                controllino.IpAddress = "192.168.1.162";
                controllino.ReceivePort = 11001;
                controllino.SendPort = 11002;
                controllino.PingInterval = 10000;
                controllino.PingTimeout = 20000;
                controllino.ResponseWaitTimeout = 3000;
                controllino.AnalogFilter = txtAnalogFilter.Text;
                controllino.Connect();

                btnConnect.Text = "Disconnect";
            }
            else
            {
                controllino.Disconnect();
                btnConnect.Text = "Connect";
            }
        }

        private void btnRetrieveAll_Click(object sender, EventArgs e)
        {
            if (controllino != null)
            {
                controllino.GetALLDigitalInputStates();
                controllino.GetALLDigitalOutputStates();
                controllino.GetALLRelayStates();
            }
        }

        private void cx0_CheckedChanged(object sender, EventArgs e)
        {
            if(controllino != null)
            {
                controllino.SetRelayState(int.Parse(((CheckBox)sender).Name.Substring(2)), ((CheckBox)sender).Checked);
            }
        }
    }
}
