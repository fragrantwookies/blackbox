﻿namespace TestControllino
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnConnect = new System.Windows.Forms.Button();
            this.lstMessages = new System.Windows.Forms.ListBox();
            this.btnRetrieveAll = new System.Windows.Forms.Button();
            this.txtAnalogFilter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cx0 = new System.Windows.Forms.CheckBox();
            this.cx1 = new System.Windows.Forms.CheckBox();
            this.cx2 = new System.Windows.Forms.CheckBox();
            this.cx3 = new System.Windows.Forms.CheckBox();
            this.cx4 = new System.Windows.Forms.CheckBox();
            this.cx9 = new System.Windows.Forms.CheckBox();
            this.cx8 = new System.Windows.Forms.CheckBox();
            this.cx7 = new System.Windows.Forms.CheckBox();
            this.cx6 = new System.Windows.Forms.CheckBox();
            this.cx5 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(12, 87);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 0;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lstMessages
            // 
            this.lstMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lstMessages.FormattingEnabled = true;
            this.lstMessages.Location = new System.Drawing.Point(12, 116);
            this.lstMessages.Name = "lstMessages";
            this.lstMessages.Size = new System.Drawing.Size(859, 420);
            this.lstMessages.TabIndex = 1;
            // 
            // btnRetrieveAll
            // 
            this.btnRetrieveAll.Location = new System.Drawing.Point(93, 87);
            this.btnRetrieveAll.Name = "btnRetrieveAll";
            this.btnRetrieveAll.Size = new System.Drawing.Size(75, 23);
            this.btnRetrieveAll.TabIndex = 2;
            this.btnRetrieveAll.Text = "Retrieve All";
            this.btnRetrieveAll.UseVisualStyleBackColor = true;
            this.btnRetrieveAll.Click += new System.EventHandler(this.btnRetrieveAll_Click);
            // 
            // txtAnalogFilter
            // 
            this.txtAnalogFilter.Location = new System.Drawing.Point(93, 12);
            this.txtAnalogFilter.Name = "txtAnalogFilter";
            this.txtAnalogFilter.Size = new System.Drawing.Size(100, 20);
            this.txtAnalogFilter.TabIndex = 3;
            this.txtAnalogFilter.Text = "A00000000000";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Analog Filter";
            // 
            // cx0
            // 
            this.cx0.AutoSize = true;
            this.cx0.Location = new System.Drawing.Point(223, 14);
            this.cx0.Name = "cx0";
            this.cx0.Size = new System.Drawing.Size(40, 17);
            this.cx0.TabIndex = 5;
            this.cx0.Text = "R0";
            this.cx0.UseVisualStyleBackColor = true;
            this.cx0.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // cx1
            // 
            this.cx1.AutoSize = true;
            this.cx1.Location = new System.Drawing.Point(269, 14);
            this.cx1.Name = "cx1";
            this.cx1.Size = new System.Drawing.Size(40, 17);
            this.cx1.TabIndex = 6;
            this.cx1.Text = "R1";
            this.cx1.UseVisualStyleBackColor = true;
            this.cx1.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // cx2
            // 
            this.cx2.AutoSize = true;
            this.cx2.Location = new System.Drawing.Point(315, 14);
            this.cx2.Name = "cx2";
            this.cx2.Size = new System.Drawing.Size(40, 17);
            this.cx2.TabIndex = 7;
            this.cx2.Text = "R2";
            this.cx2.UseVisualStyleBackColor = true;
            this.cx2.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // cx3
            // 
            this.cx3.AutoSize = true;
            this.cx3.Location = new System.Drawing.Point(361, 14);
            this.cx3.Name = "cx3";
            this.cx3.Size = new System.Drawing.Size(40, 17);
            this.cx3.TabIndex = 8;
            this.cx3.Text = "R3";
            this.cx3.UseVisualStyleBackColor = true;
            this.cx3.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // cx4
            // 
            this.cx4.AutoSize = true;
            this.cx4.Location = new System.Drawing.Point(407, 14);
            this.cx4.Name = "cx4";
            this.cx4.Size = new System.Drawing.Size(40, 17);
            this.cx4.TabIndex = 9;
            this.cx4.Text = "R4";
            this.cx4.UseVisualStyleBackColor = true;
            this.cx4.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // cx9
            // 
            this.cx9.AutoSize = true;
            this.cx9.Location = new System.Drawing.Point(637, 14);
            this.cx9.Name = "cx9";
            this.cx9.Size = new System.Drawing.Size(40, 17);
            this.cx9.TabIndex = 14;
            this.cx9.Text = "R9";
            this.cx9.UseVisualStyleBackColor = true;
            this.cx9.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // cx8
            // 
            this.cx8.AutoSize = true;
            this.cx8.Location = new System.Drawing.Point(591, 14);
            this.cx8.Name = "cx8";
            this.cx8.Size = new System.Drawing.Size(40, 17);
            this.cx8.TabIndex = 13;
            this.cx8.Text = "R8";
            this.cx8.UseVisualStyleBackColor = true;
            this.cx8.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // cx7
            // 
            this.cx7.AutoSize = true;
            this.cx7.Location = new System.Drawing.Point(545, 14);
            this.cx7.Name = "cx7";
            this.cx7.Size = new System.Drawing.Size(40, 17);
            this.cx7.TabIndex = 12;
            this.cx7.Text = "R7";
            this.cx7.UseVisualStyleBackColor = true;
            this.cx7.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // cx6
            // 
            this.cx6.AutoSize = true;
            this.cx6.Location = new System.Drawing.Point(499, 14);
            this.cx6.Name = "cx6";
            this.cx6.Size = new System.Drawing.Size(40, 17);
            this.cx6.TabIndex = 11;
            this.cx6.Text = "R6";
            this.cx6.UseVisualStyleBackColor = true;
            this.cx6.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // cx5
            // 
            this.cx5.AutoSize = true;
            this.cx5.Location = new System.Drawing.Point(453, 14);
            this.cx5.Name = "cx5";
            this.cx5.Size = new System.Drawing.Size(40, 17);
            this.cx5.TabIndex = 10;
            this.cx5.Text = "R5";
            this.cx5.UseVisualStyleBackColor = true;
            this.cx5.CheckedChanged += new System.EventHandler(this.cx0_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(883, 548);
            this.Controls.Add(this.cx9);
            this.Controls.Add(this.cx8);
            this.Controls.Add(this.cx7);
            this.Controls.Add(this.cx6);
            this.Controls.Add(this.cx5);
            this.Controls.Add(this.cx4);
            this.Controls.Add(this.cx3);
            this.Controls.Add(this.cx2);
            this.Controls.Add(this.cx1);
            this.Controls.Add(this.cx0);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAnalogFilter);
            this.Controls.Add(this.btnRetrieveAll);
            this.Controls.Add(this.lstMessages);
            this.Controls.Add(this.btnConnect);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.ListBox lstMessages;
        private System.Windows.Forms.Button btnRetrieveAll;
        private System.Windows.Forms.TextBox txtAnalogFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox cx0;
        private System.Windows.Forms.CheckBox cx1;
        private System.Windows.Forms.CheckBox cx2;
        private System.Windows.Forms.CheckBox cx3;
        private System.Windows.Forms.CheckBox cx4;
        private System.Windows.Forms.CheckBox cx9;
        private System.Windows.Forms.CheckBox cx8;
        private System.Windows.Forms.CheckBox cx7;
        private System.Windows.Forms.CheckBox cx6;
        private System.Windows.Forms.CheckBox cx5;
    }
}

