﻿namespace ActiveXControlLib
{
    partial class OnCamGrandEyeControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnCamGrandEyeControl));
            this.axGEIAXDisplay1 = new AxGEIAXDisplayLib.AxGEIAXDisplay();
            ((System.ComponentModel.ISupportInitialize)(this.axGEIAXDisplay1)).BeginInit();
            this.SuspendLayout();
            // 
            // axGEIAXDisplay1
            // 
            this.axGEIAXDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axGEIAXDisplay1.Enabled = true;
            this.axGEIAXDisplay1.Location = new System.Drawing.Point(0, 0);
            this.axGEIAXDisplay1.Name = "axGEIAXDisplay1";
            this.axGEIAXDisplay1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axGEIAXDisplay1.OcxState")));
            this.axGEIAXDisplay1.Size = new System.Drawing.Size(916, 659);
            this.axGEIAXDisplay1.TabIndex = 0;
            // 
            // OnCamGrandEyeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.axGEIAXDisplay1);
            this.Name = "OnCamGrandEyeControl";
            this.Size = new System.Drawing.Size(916, 659);
            ((System.ComponentModel.ISupportInitialize)(this.axGEIAXDisplay1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxGEIAXDisplayLib.AxGEIAXDisplay axGEIAXDisplay1;
    }
}
