﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GEISDKLib;
using System.Net;
using System.Runtime.InteropServices;

namespace ActiveXControlLib
{
    public partial class OnCamGrandEyeControl: UserControl
    {
        private GEIAPI api;
        private GEIView view;

        public OnCamGrandEyeControl()
        {
            InitializeComponent();

            api = new GEIAPI();
            view = new GEIView();
            api.AddView(view);
            axGEIAXDisplay1.SetGEIViewPtr(view);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (axGEIAXDisplay1 != null)
            {
                axGEIAXDisplay1.Dispose();
                axGEIAXDisplay1 = null;
            }            

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }        

        public byte[] DownloadLoadImage(string ipAddress, string userName, string password)
        {
            byte[] result = null;
            try
            {
                string url = @"http://" + ipAddress + @"/mjpg/snapshot.cgi";

                WebClient webClient = new WebClient();
                
                if(!string.IsNullOrWhiteSpace(userName))
                    webClient.Credentials = new NetworkCredential(userName, password);
                
                result = webClient.DownloadData(url);
                webClient.Dispose();
            }
            catch { }

            return result;
        }

        unsafe public void LoadImage(byte[] image)
        {
            try
            {
                if (image != null)
                {
                    if (image.Length > 0)
                    {
                        byte* bufferPtr = null;

                        System.IntPtr ptrToBuffer = new IntPtr(&bufferPtr);
                        api.GetBuffer(ptrToBuffer, image.Length);

                        if (ptrToBuffer != null)
                        {
                            for (int i = 0; i < image.Length; i++)
                            {
                                bufferPtr[i] = image[i];
                            }

                            api.ReturnBufferJPEG(0);
                        }
                    }
                }
            }
            catch { }
        }
    }
}
