﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace eTheleSoundPlayer
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("CodeRush", "Fields should be disposed")]
    public class SoundPlayer : IDisposable
    {
        private IWavePlayer waveOutDevice;
        private AudioFileReader audioFileReader;

        private Timer timer;

        private bool playing = false;

        public SoundPlayer()
        {
            waveOutDevice = new WaveOut();
            waveOutDevice.PlaybackStopped += waveOutDevice_PlaybackStopped;
        }

        ~SoundPlayer()
        {
            Dispose(true);
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(timer != null)
            {
                timer.Elapsed -= timer_Elapsed;
                timer.Dispose();
                timer = null;
            }

            if (waveOutDevice != null)
            {
                waveOutDevice.PlaybackStopped -= waveOutDevice_PlaybackStopped;
                IDisposable disposablewaveOutDevice = waveOutDevice as IDisposable;
                if (disposablewaveOutDevice != null)
                {
                    disposablewaveOutDevice.Dispose();
                    waveOutDevice = null;
                }
            }

            IDisposable disposableaudioFileReader = audioFileReader as IDisposable;
            if (disposableaudioFileReader != null)
            {
                disposableaudioFileReader.Dispose();
                audioFileReader = null;
            }
        }

        public void PlaySound(string _soundFile, double _duration = 0)
        {
            if(playing && waveOutDevice != null)
            {
                Stop();
            }

            playing = true;

            if (audioFileReader != null)
                audioFileReader.Dispose();

            audioFileReader = new AudioFileReader(_soundFile);

            waveOutDevice.Init(audioFileReader);
            waveOutDevice.Play();

            if(_duration > 0)
            {
                if (timer == null)
                {
                    timer = new Timer(_duration * 1000);
                    timer.Elapsed += timer_Elapsed;
                }
                else
                    timer.Interval = _duration * 1000;

                timer.Start();
            }
        }

        public void Stop()
        {
            if (waveOutDevice != null) 
                waveOutDevice.Stop();

            playing = false;

            IDisposable disposableaudioFileReader = audioFileReader as IDisposable;
            if (disposableaudioFileReader != null)
            {
                disposableaudioFileReader.Dispose();
                audioFileReader = null;
            }
        }

        void waveOutDevice_PlaybackStopped(object sender, StoppedEventArgs e)
        {
            Stop();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            timer.Stop();
            Stop();
        }
    }
}
