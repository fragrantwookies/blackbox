﻿using BlackBoxEngine.RootCauseEngine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestRigRootCauseServer
{
    public partial class Form1 : Form
    {
        RootCauseService service = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (service == null)
                service = new RootCauseService();

            if (!service.Start())
                MessageBox.Show("Invalid install, could not start");
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (service != null)
                service.Stop();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (service != null)
            {
                service.Stop();
                service.Dispose();
                service = null;
            }
        }
    }
}
