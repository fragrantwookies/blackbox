using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UIAutoResolveAction : UIAction
    {
        private int timeDelay;
        public int TimeDelay
        {
            get { return timeDelay; }
            set { timeDelay = value; PropertyChangedHandler("TimeDelay"); }
        }

        private string alarmToResolve;
        public string AlarmToResolve
        {
            get { return alarmToResolve; }
            set { alarmToResolve = value; PropertyChangedHandler("AlarmToResolve"); }
        }

        private ObservableCollection<string> alarmNamesList;

        public ObservableCollection<string> AlarmNamesList
        {
            get { return alarmNamesList; }
            set { alarmNamesList = value; PropertyChangedHandler("AlarmNamesList"); }
        }
    }
}
