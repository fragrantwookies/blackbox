using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    public class UIPluginActionCustomBoolField : UIPluginActionCustomBaseField
    {
        private bool fieldValue = false;

        public bool FieldValue
        {
            get { return fieldValue; }
            set { fieldValue = value; PropertyChangedHandler("FieldValue"); }
        }
    }
}
