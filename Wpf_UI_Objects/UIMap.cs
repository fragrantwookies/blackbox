using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UIMap : Base
    {
        private ObservableCollection<Base> maps;

        private string title;
        private string imagePath;

        private string thumbnailPath;
        private double thumbnailX;
        private double thumbnailY;
        private double thumbnailWidth;
        private double thumbnailHeight;
        private double thumbnailRotation;

        private double defaultIconWidth;
        private double defaultIconHeight;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (maps != null)
            {
                for (int i = maps.Count - 1; i >= 0; i--)
                {
                    Base map = maps[i];
                    maps.RemoveAt(i);
                    map.Dispose();
                }

                maps = null;
            }
        }

        public override void RemoveChild(Base _child)
        {
            base.RemoveChild(_child);

            if (_child is Base && maps != null)
            {
                maps.Remove((Base)_child);
            }
        }

        public ObservableCollection<Base> Maps
        {
            get
            {
                if (maps == null)
                    maps = new ObservableCollection<Base>();

                return maps;
            }
            set { maps = value; PropertyChangedHandler("Maps"); }
        }

        public string Title
        {
            get { return title; }
            set { title = value; PropertyChangedHandler("Title"); }
        }

        public string ImagePath
        {
            get { return imagePath; }
            set { imagePath = value; PropertyChangedHandler("ImagePath"); }
        }

        public string ThumbnailPath
        {
            get { return thumbnailPath; }
            set { thumbnailPath = value; PropertyChangedHandler("ThumbnailPath"); }
        }

        public double ThumbnailX
        {
            get { return thumbnailX; }
            set { thumbnailX = value; PropertyChangedHandler("ThumbnailX"); }
        }

        public double ThumbnailY
        {
            get { return thumbnailY; }
            set { thumbnailY = value; PropertyChangedHandler("ThumbnailY"); }
        }

        public double ThumbnailWidth
        {
            get { return thumbnailWidth; }
            set { thumbnailWidth = value; PropertyChangedHandler("ThumbnailWidth"); }
        }

        public double ThumbnailHeight
        {
            get { return thumbnailHeight; }
            set { thumbnailHeight = value; PropertyChangedHandler("ThumbnailHeight"); }
        }

        public double ThumbnailRotation
        {
            get { return thumbnailRotation; }
            set { thumbnailRotation = value; PropertyChangedHandler("ThumbnailRotation"); }
        }

        public double DefaultIconWidth
        {
            get { return defaultIconWidth; }
            set { defaultIconWidth = value; PropertyChangedHandler("DefaultIconWidth"); }
        }

        public double DefaultIconHeight
        {
            get { return defaultIconHeight; }
            set { defaultIconHeight = value; PropertyChangedHandler("DefaultIconHeight"); }
        }
    }
}
