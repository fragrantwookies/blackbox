﻿using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WPF_UI
{
    [Serializable]
    public class UIPluginAction : UIAction
    {
        private string pluginName;

        public string PluginName
        {
            get { return pluginName; }
            set 
            { 
                pluginName = value;

                ActionNames.Clear();

                CustomFieldNames.Clear();

                foreach (string actionTypeName in UIPluginManager.GetPluginActions(PluginName))
                    ActionNames.Add(actionTypeName);

                PropertyChangedHandler("PluginName");

                PropertyChangedHandler("ActionNames"); 
            }
        }

        private string actionName;

        public string ActionName
        {
            get { return actionName; }
            set 
            {
                actionName = value;

                CustomFieldNames.Clear();

                foreach (PropertyInfo prop in UIPluginManager.GetActionProperties(PluginName, actionName))
                    CustomFieldNames.Add(prop.Name);

                PropertyChangedHandler("ActionName");

                PropertyChangedHandler("CustomFieldNames"); 
            }
        }

        private ObservableCollection<string> actionNames = new ObservableCollection<string>();

        public ObservableCollection<string> ActionNames
        {
            get { return actionNames; }
            set { actionNames = value; PropertyChangedHandler("ActionNames"); }
        }

        private ObservableCollection<UIPluginActionCustomBaseField> customFields;
        public ObservableCollection<UIPluginActionCustomBaseField> CustomFields
        {
            get { return customFields; }
            set { customFields = value; PropertyChangedHandler("CustomFields"); }
        }

        private ObservableCollection<string> customFieldNames = new ObservableCollection<string>();

        public ObservableCollection<string> CustomFieldNames
        {
            get { return customFieldNames; }
            set { customFieldNames = value; PropertyChangedHandler("CustomFieldNames"); }
        }

        public Type GetCustomFieldType(string customFieldName)
        {
            Type resultType = Type.Missing.GetType();

            foreach (PropertyInfo prop in UIPluginManager.GetActionProperties(PluginName, ActionName))
            {
                if(prop.Name == customFieldName)
                {
                    resultType = prop.PropertyType;
                    break;
                }
            }

            return resultType;
        }
        
        public UIPluginAction()
        {
            customFields = new ObservableCollection<UIPluginActionCustomBaseField>();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (CustomFields != null)
            {
                for (int i = CustomFields.Count - 1; i >= 0 ; i--)
                {
                    Base child = CustomFields[i];
                    customFields.RemoveAt(i);
                    child.Dispose();
                }  
            }
        }

        public override void RemoveChild(Base _child)
        {
            base.RemoveChild(_child);

            UIPluginActionCustomBaseField child = _child as UIPluginActionCustomBaseField;

            if (child != null && CustomFields != null)
            {
                CustomFields.Remove(child);
            }
        }

        public override void ExpandAll()
        {
            base.ExpandAll();

            if (CustomFields != null)
                foreach (Base item in CustomFields)
                    item.ExpandAll();
        }

        public override void CollapseAll()
        {
            base.CollapseAll();

            if (CustomFields != null)
                foreach (Base item in CustomFields)
                    item.CollapseAll();
        }
    }
}
