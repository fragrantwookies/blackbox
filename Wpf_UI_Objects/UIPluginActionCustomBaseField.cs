using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public abstract class UIPluginActionCustomBaseField : Base
    {
        private string fieldName = "";

        public string FieldName
        {
            get { return fieldName; }
            set { fieldName = value; PropertyChangedHandler("FieldName"); }
        }
    }
}
