﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using eNervePluginInterface;

using eNerve.DataTypes;

namespace WPF_UI
{
    [Serializable]
    public class UIPlugin : Base
    {
        public string Author { get; private set; }
        public string FileName { get; private set; }
        public bool Online { get; set; }
        public string Version { get; private set; }

        public UIPluginSetting Properties { get; private set; }

        /// <summary>
        /// Names and types of the Custom Properties you expect to be set. Return null if none needed.
        /// </summary>
        List<CustomPropertyDefinition> CustomPropertyDefinitions { get; set; }

        /// <summary>
        /// Construct display device from plugin object
        /// </summary>
        /// <param name="plugin"></param>
        public UIPlugin(INervePlugin plugin)
        {
            Author = plugin.Author;
            CustomPropertyDefinitions = plugin.CustomPropertyDefinitions;
            FileName = plugin.FileName;
            Name = plugin.Name;
            Properties = new UIPluginSetting(plugin.Properties);
            PropertyChangedHandler("Properties");
            Version = plugin.Version;
        }
    }
}
