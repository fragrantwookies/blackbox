using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public abstract class UIAction : Base
    {
        private int order = 0;

         public int Order
        {
            get { return order; }
            set { order = value; PropertyChangedHandler("Order"); }
        }
    }
}
