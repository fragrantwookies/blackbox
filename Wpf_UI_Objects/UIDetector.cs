using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UIDetector : UIIOItem
    {
        private int eventsToRaiseAlarm = 1;
        private int eventsToRaiseAlarmInterval = 0;
        private int coolDown = 0;
        private double sensitivity = -1;

        private bool showThresholds = false;
        private double minThreshold = 0;
        private double maxThreshold = 0;

        /// <summary>
        /// Create a new detector. A detector can be Analog or Digital.
        /// </summary>
        /// <param name="Analog">True if you want an Analog detector. Analog detectors has threshold properties.</param>
        public UIDetector(bool Analog)
        {
            ShowThresholds = Analog;
        }

        public string EventName
        {
            get { return this.Description; }
            set { this.Description = value; }
        }

        public int EventsToRaiseAlarm
        {
            get { return eventsToRaiseAlarm; }
            set { eventsToRaiseAlarm = value; PropertyChangedHandler("EventsToRaiseAlarm"); }
        }

        public int EventsToRaiseAlarmInterval
        {
            get { return eventsToRaiseAlarmInterval; }
            set { eventsToRaiseAlarmInterval = value; PropertyChangedHandler("EventsToRaiseAlarmInterval"); }
        }

        public int CoolDown
        {
            get { return coolDown; }
            set { coolDown = value; PropertyChangedHandler("CoolDown"); }
        }

        public double Sensitivity
        {
            get { return sensitivity; }
            set { sensitivity = value; PropertyChangedHandler("Sensitivity"); }
        }

        public bool ShowThresholds
        {
            get { return showThresholds; }
            set { showThresholds = value; PropertyChangedHandler("ShowThresholds"); }
        }

        public bool IsAnalog
        {
            get { return showThresholds; }
        }

        public double MinThreshold
        {
            get { return minThreshold; }
            set { minThreshold = value; PropertyChangedHandler("MinThreshold"); }
        }

        public double MaxThreshold
        {
            get { return maxThreshold; }
            set { maxThreshold = value; PropertyChangedHandler("MaxThreshold"); }
        }
    }
}
