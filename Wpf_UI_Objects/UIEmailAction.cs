using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UIEmailAction : UIAction
    {
        private string emailAddress;

        public string EmailAddress
        {
            get { return emailAddress; }
            set { emailAddress = value; PropertyChangedHandler("EmailAddress"); }
        }
    }
}
