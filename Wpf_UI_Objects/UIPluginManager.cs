﻿using eNerve.DataTypes;
using eNervePluginInterface;
using eThele.Essentials;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WPF_UI
{
    public class UIPluginManager : IDisposable
    {
        private List<IUserInterfacePlugin> plugins;

        public class UserControlPluginEventArgs : EventArgs
        {
            public UserControl PluginControl { get; set; }

            public UserControlPluginEventArgs(UserControl _pluginControl)
                : base()
            {
                PluginControl = _pluginControl;
            }

            public UserControlPluginEventArgs() {}
             
        }

        public event EventHandler<UserControlPluginEventArgs> ControlLoaded;
        public event EventHandler<UserControlPluginEventArgs> ControlRemoved;

        public UIPluginManager()
        {
            plugins = new List<IUserInterfacePlugin>();
        }

        #region Cleanup
        ~UIPluginManager()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            ClearPlugins();
        }

        public void ClearPlugins()
        {
            try
            {
                if (plugins != null)
                {
                    for (int i = plugins.Count - 1; i >= 0; i--)
                    {
                        if (ControlRemoved != null && plugins[i] is UserControl)
                            ControlRemoved(this, new UserControlPluginEventArgs((UserControl)plugins[i]));

                        plugins[i].Stop();

                        plugins[i].Error -= plugin_Error;

                        if (plugins[i] is IDisposable)
                            ((IDisposable)plugins[i]).Dispose();
                            
                        plugins.RemoveAt(i);
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        } 
        #endregion Cleanup

        #region Load Plug-ins
        /// <summary>
        /// Find all the plug-ins in the plugin directory. It will also clear the list of plugins and deregister all event handlers there might have been.
        /// </summary>
        public void FindPlugins()
        {
            try
            {
                ClearPlugins();

                string pluginsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Ethele\\RioSoft\\Plugins"); //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).TrimEnd('\\') + @"\Plugins";
                if (Directory.Exists(pluginsFolder))
                {
                    foreach (string fileOn in Directory.GetFiles(pluginsFolder))
                    {
                        FileInfo file = new FileInfo(fileOn);

                        //Preliminary check, must be .dll
                        if (file.Extension.Equals(".dll"))
                        {
                            LoadPlugins(fileOn);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                    MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        /// <summary>
        /// Loads all the plugins (INervePlugin) found in this dll
        /// </summary>
        /// <param name="fileName">Filename of dll</param>
        public void LoadPlugins(string fileName)
        {
            try
            {
                Assembly pluginAssembly = Assembly.LoadFrom(fileName);
                foreach (Type pluginType in pluginAssembly.GetTypes())
                {
                    try
                    {
                        if (pluginType.IsPublic) //Only look at public types
                        {
                            if (!pluginType.IsAbstract)  //Only look at non-abstract types
                            {
                                //Gets a type object of the interface we need the plugins to match
                                Type typeInterface = pluginType.GetInterface("eNervePluginInterface.IUserInterfacePlugin", true);

                                //Make sure the interface we want to use actually exists
                                if (typeInterface != null)
                                {
                                    if (plugins == null)
                                        plugins = new List<IUserInterfacePlugin>();

                                    IUserInterfacePlugin plugin = (IUserInterfacePlugin)Activator.CreateInstance(pluginAssembly.GetType(
                                        pluginType.ToString()));

                                    plugins.Add(plugin);

                                    //plugin.Host = this;
                                    plugin.FileName = fileName;

                                    //PluginsDoc.GetCustomPoperties(plugin);

                                    plugin.Error += plugin_Error;

                                    if (ControlLoaded != null && plugin is UserControl)
                                        ControlLoaded(this, new UserControlPluginEventArgs((UserControl)plugin));

                                    plugin.Start();
                                }
                                typeInterface = null;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, 
                            MethodBase.GetCurrentMethod().Name), "", ex);
                    }
                }
            }
            catch (Exception)
            { // Bury this exception, as most dll's in the plugin folder are NOT IUserInterfacePlugins and this causes unwarranted errors.
                //Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", MethodBase.GetCurrentMethod().DeclaringType.Name, MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }

        void plugin_Error(object sender, PluginErrorEventArgs e)
        {
            string pluginName = "Plugin";
            if (sender is IUserInterfacePlugin)
                pluginName = ((IUserInterfacePlugin)sender).PluginName;

            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", pluginName, e.FunctionName), e.Description, e.Exception);
        }
        #endregion Load Plug-ins

        #region plugin meta
        /// <summary>
        /// Plugin type name, action type name, list of properties
        /// </summary>
        private static List<Tuple<string, string, List<PropertyInfo>>> ActionProperties;

        public static List<string> GetPluginActions(string _PluginName)
        {
            List<string> resultActionTypeNames = new List<string>();

            if (ActionProperties == null)
                LoadActionProperties();

            lock (ActionProperties)
            {
                foreach (var action in ActionProperties)
                {
                    if (action.Item1.Equals(deviceDoc.LookupPluginType(_PluginName).Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase))
                        resultActionTypeNames.Add(action.Item2);
                }
            }

            return resultActionTypeNames;
        }

        public static List<PropertyInfo> GetActionProperties(string _PluginName, string _ActionTypeName)
        {
            List<PropertyInfo> resultActionProperties = new List<PropertyInfo>();

            if (ActionProperties == null)
                LoadActionProperties();

            lock (ActionProperties)
            {
                foreach (var action in ActionProperties)
                {
                    if (action.Item1.Equals(deviceDoc.LookupPluginType(_PluginName).Replace(" ", ""), StringComparison.InvariantCultureIgnoreCase)
                        && action.Item2.Equals(_ActionTypeName, StringComparison.InvariantCultureIgnoreCase))
                    {
                        resultActionProperties = action.Item3;

                        break;
                    }
                }
            }

            return resultActionProperties;
        }

        public static List<string> GetPluginNames()
        {
            List<string> resultPluginNames = new List<string>();



            return resultPluginNames;
        }

        private static DevicesDoc deviceDoc;
        public static DevicesDoc DeviceDoc 
        { 
            get
            {
                return deviceDoc;
            }
            set
            {
                deviceDoc = value;
                LoadActionProperties();
            }
        }

        private string LookupPluginType(string pluginName)
        {
            string resultType = "";

            if(deviceDoc != null)
            {

            }

            return resultType;
        }

        private static void LoadActionProperties()
        {
            try
            {
                if (ActionProperties == null)
                    ActionProperties = new List<Tuple<string, string, List<PropertyInfo>>>();
                else
                    ActionProperties.Clear();

                lock (ActionProperties)
                {
                    string pluginsFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "Ethele\\RioSoft\\Plugins"); //Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location).TrimEnd('\\') + @"\Plugins";
                    if (Directory.Exists(pluginsFolder))
                    {
                        foreach (string fileName in Directory.GetFiles(pluginsFolder))
                        {
                            FileInfo file = new FileInfo(fileName);

                            //Preliminary check, must be .dll
                            if (file.Extension.Equals(".dll"))
                            {
                                try
                                {
                                    Assembly pluginAssembly = Assembly.LoadFrom(fileName);
                                    foreach (Type pluginType in pluginAssembly.GetTypes())
                                    {
                                        try
                                        {
                                            if (pluginType.IsPublic && !pluginType.IsAbstract) //Only look at public non-abstract types
                                            {
                                                //Gets a type object of the interface we need the plugins to match
                                                Type typeInterface = pluginType.GetInterface("eNervePluginInterface.INerveActionPlugin", true);

                                                if (typeInterface != null)
                                                {
                                                    INerveActionPlugin plugin = (INerveActionPlugin)Activator.CreateInstance(pluginType);
                                                    IEnumerable<Type> actionTypes = plugin.GetActionTypes();

                                                    foreach (Type actionType in actionTypes)
                                                    {
                                                        //Make sure the interface we want to use actually exists
                                                        if (actionType != null)
                                                        {
                                                            //INerveActionPlugin actionPlugin = pluginType as INerveActionPlugin;

                                                            //if (actionPlugin != null)
                                                            {
                                                                List<PropertyInfo> propertyInfo = new List<PropertyInfo>();
                                                                foreach (PropertyInfo propInfo in actionType.GetProperties())
                                                                {
                                                                    if (propInfo.DisplayInPluginActionPropertiesList())
                                                                        propertyInfo.Add(propInfo);
                                                                }                                                                

                                                                ActionProperties.Add(new Tuple<string, string, List<PropertyInfo>>(pluginType.Name, actionType.Name, propertyInfo));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Exceptions.ExceptionsManager.Add(string.Format("{0},{1}", System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name, System.Reflection.MethodBase.GetCurrentMethod().Name), "", ex);
            }
        }
        #endregion
    }
}
