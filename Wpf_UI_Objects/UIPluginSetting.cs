using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace WPF_UI
{
    [Serializable]
    public class UIPluginSetting : Base
    {
        public UIPluginSetting(eNervePluginInterface.CustomProperties properties)
        {
            if (settings == null)
                settings = new ObservableCollection<UIPluginSetting>();
            foreach (var item in properties.GetProperties())
            {
                var pluginSetting = new UIPluginSetting(item);
                settings.Add(pluginSetting);
            }
        }
        private UIPluginSetting(eNervePluginInterface.ICustomProperty property)
        {
            name = property.PropertyName;
            value = property.PropertyValue;
        }

        private ObservableCollection<UIPluginSetting> settings;
        private string value = "";
        private bool isPlugin = false;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        private void ClearSettings()
        {
            if (settings != null)
            {
                if (Application.Current != null)
                {
                    if (Application.Current.Dispatcher.CheckAccess())
                    {
                        for (int i = settings.Count - 1; i >= 0; i--)
                        {
                            UIPluginSetting setting = settings[i];
                            settings.RemoveAt(i);
                            setting.Dispose();
                        }
                    }
                    else
                        Application.Current.Dispatcher.Invoke(ClearSettings);
                }
            }
        }

        public ObservableCollection<UIPluginSetting> Settings
        {
            get
            {
                if (settings == null)
                    settings = new ObservableCollection<UIPluginSetting>();

                return settings;
            }
            set { settings = value; PropertyChangedHandler("Settings"); }
        }

        public string Value
        {
            get { return this.value; }
            set { this.value = value; PropertyChangedHandler("Value"); }
        }

        public bool IsPlugin
        {
            get { return isPlugin; }
            set { isPlugin = value; PropertyChangedHandler("IsPlugin"); }
        }
    }
}
