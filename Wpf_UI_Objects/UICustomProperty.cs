﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eNerve.DataTypes;
using eThele.Essentials;

namespace WPF_UI
{
    [Serializable]
    public class UICustomProperty : Base, IEquatable<UICustomProperty>
    {
        public UICustomProperty(eNervePluginInterface.CustomPropertyDefinition property)
        {
            Display = true;
            Name = property.PropertyName;
            DisplayName = string.IsNullOrWhiteSpace(property.DisplayName) ? property.PropertyName : property.DisplayName;
            ToolTip = string.IsNullOrWhiteSpace(property.ToolTip) ? "Property Value" : property.ToolTip;
            Value = property.DefaultValue;
            PropertyDefinition = property;
        }
        public UICustomProperty(eThele.Essentials.XmlNode xmlProperty)
        {
            Display = true;
            Name = xmlProperty.Name;
            DisplayName = XmlNode.ParseString(xmlProperty, Name, "DisplayName");
            ToolTip = XmlNode.ParseString(xmlProperty, "Property Value", "ToolTip");
            Value = xmlProperty.Value;
        }
        public UICustomProperty(string name, string value)
        {
            Display = true;
            Name = name;
            DisplayName = name;
            ToolTip = "Property Value";
            Value = value;
        }

        private string displayName;
        public string DisplayName { get { return displayName; } set { displayName = value; PropertyChangedHandler("DisplayName"); } }

        private string toolTip;
        public string ToolTip { get { return toolTip; } set { toolTip = value; PropertyChangedHandler("ToolTip"); } }

        public bool Display { get; set; }

        public bool CanRemoveItem { get; set; }

        private string _value;
        public string Value { get {return _value; } set { _value = value; PropertyChangedHandler("Value"); } }

        public eNervePluginInterface.CustomPropertyDefinition PropertyDefinition { get; set; }

        public override int GetHashCode()
        {
            return name.GetHashCode() + (Parent != null ? Parent.GetHashCode() : 0);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as Base);
        }

        public bool Equals(UICustomProperty other)
        {
            return base.Equals(other);
        }

        public bool ReadOnly
        {
            get { return Name.StartsWith("Plugin", StringComparison.OrdinalIgnoreCase); }
        }
    }
}