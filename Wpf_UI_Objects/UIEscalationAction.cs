﻿using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPF_UI
{   

    [Serializable]
    public class UIEscalationAction : UIAction
    {
        private int greenDelay;
        private int yellowDelay;
        private int redDelay;

        ObservableCollection<UIAction> greenActions;
        ObservableCollection<UIAction> yellowActions;
        ObservableCollection<UIAction> redActions;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (greenActions != null)
            {
                for (int i = greenActions.Count - 1; i >= 0; i--)
                {
                    UIAction action = greenActions[i];
                    greenActions.RemoveAt(i);
                    action.Dispose();
                }

                greenActions = null;
            }

            if (yellowActions != null)
            {
                for (int i = yellowActions.Count - 1; i >= 0; i--)
                {
                    UIAction action = yellowActions[i];
                    yellowActions.RemoveAt(i);
                    action.Dispose();
                }

                yellowActions = null;
            }

            if (redActions != null)
            {
                for (int i = redActions.Count - 1; i >= 0; i--)
                {
                    UIAction action = redActions[i];
                    redActions.RemoveAt(i);
                    action.Dispose();
                }

                redActions = null;
            }
        }

        public int GreenDelay
        {
            get { return greenDelay; }
            set { greenDelay = value; PropertyChangedHandler("GreenDelay"); }
        }

        public int YellowDelay
        {
            get { return yellowDelay; }
            set { yellowDelay = value; PropertyChangedHandler("YellowDelay"); }
        }

        public int RedDelay
        {
            get { return redDelay; }
            set { redDelay = value; PropertyChangedHandler("RedDelay"); }
        }

        public ObservableCollection<UIAction> GreenActions
        {
            get 
            {
                if (greenActions == null)
                    greenActions = new ObservableCollection<UIAction>();
                
                return greenActions; 
            }
            set { greenActions = value; PropertyChangedHandler("GreenActions"); }
        }

        public ObservableCollection<UIAction> YellowActions
        {
            get
            {
                if (yellowActions == null)
                    yellowActions = new ObservableCollection<UIAction>(); 
                
                return yellowActions;
            }
            set { yellowActions = value; PropertyChangedHandler("YellowActions"); }
        }

        public ObservableCollection<UIAction> RedActions
        {
            get
            {
                if (redActions == null)
                    redActions = new ObservableCollection<UIAction>();

                return redActions;
            }
            set { redActions = value; PropertyChangedHandler("RedActions"); }
        }

        public override void ExpandAll()
        {
            base.ExpandAll();

            if (greenActions != null)
                foreach (Base item in greenActions)
                    item.ExpandAll();

            if (yellowActions != null)
                foreach (Base item in yellowActions)
                    item.ExpandAll();

            if (redActions != null)
                foreach (Base item in redActions)
                    item.ExpandAll();
        }

        public override void CollapseAll()
        {
            base.CollapseAll();

            if (greenActions != null)
                foreach (Base item in greenActions)
                    item.ExpandAll();

            if (yellowActions != null)
                foreach (Base item in yellowActions)
                    item.ExpandAll();

            if (redActions != null)
                foreach (Base item in redActions)
                    item.ExpandAll();
        } 
    }
}
