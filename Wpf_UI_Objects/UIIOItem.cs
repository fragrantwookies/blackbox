using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public abstract class UIIOItem : Base
    {
        private string ioType;
        private int index;
        private string description;
        private string location;
        private string imagePath;
        private string altImagePath;
        private string imageMap;
        private double imageX;
        private double imageY;
        private double imageWidth;
        private double imageHeight;
        private double imageRotation;

        public string IOType
        {
            get { return ioType; }
            set { ioType = value; PropertyChangedHandler("IOType"); }
        }

        public int Index
        {
            get { return index; }
            set { index = value; PropertyChangedHandler("Index"); }
        }

        public string Description
        {
            get { return description; }
            set { description = value; PropertyChangedHandler("Description"); }
        }

        public string Location
        {
            get { return location; }
            set { location = value; PropertyChangedHandler("Location"); }
        }

        public string ImagePath
        {
            get { return imagePath; }
            set { imagePath = value; PropertyChangedHandler("ImagePath"); }
        }

        public string AltImagePath
        {
            get { return altImagePath; }
            set { altImagePath = value; PropertyChangedHandler("AltImagePath"); }
        }

        public string ImageMap
        {
            get { return imageMap; }
            set { imageMap = value; PropertyChangedHandler("ImageMap"); }
        }

        public double ImageX
        {
            get { return imageX; }
            set { imageX = value; PropertyChangedHandler("ImageX"); }
        }

        public double ImageY
        {
            get { return imageY; }
            set { imageY = value; PropertyChangedHandler("ImageY"); }
        }

        public double ImageWidth
        {
            get { return imageWidth; }
            set { imageWidth = value; PropertyChangedHandler("ImageWidth"); }
        }

        public double ImageHeight
        {
            get { return imageHeight; }
            set { imageHeight = value; PropertyChangedHandler("ImageHeight"); }
        }

        public double ImageRotation
        {
            get { return imageRotation; }
            set { imageRotation = value; PropertyChangedHandler("ImageRotation"); }
        }
    }
}
