using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace WPF_UI
{
    [Serializable]
    public class UIDetectors : Base
    {
        private string ioType;

        private ObservableCollection<UIDetector> detectors;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            ClearDetectors();
        }

        public override void RemoveChild(Base _child)
        {
            base.RemoveChild(_child);

            if (_child is UIDetector && detectors != null)
            {
                detectors.Remove((UIDetector)_child);
            }
        }

        private void ClearDetectors()
        {
            if (detectors != null)
            {
                if (Application.Current != null)
                {
                    if (Application.Current.Dispatcher.CheckAccess())
                    {
                        for (int i = detectors.Count - 1; i >= 0; i--)
                        {
                            UIDetector detector = detectors[i];
                            detectors.RemoveAt(i);
                            detector.Dispose();
                        }
                    }
                    else
                        Application.Current.Dispatcher.Invoke(ClearDetectors);
                }
            }
        }

        public string IOType
        {
            get { return ioType; }
            set { ioType = value; PropertyChangedHandler("IOType"); }
        }

        public ObservableCollection<UIDetector> Detectors
        {
            get
            {
                if (detectors == null)
                    detectors = new ObservableCollection<UIDetector>();

                return detectors;
            }
            set { detectors = value; PropertyChangedHandler("Detectors"); }
        }

        public override void ExpandAll()
        {
            base.ExpandAll();

            if (detectors != null)
                foreach (Base item in detectors)
                    item.ExpandAll();
        }

        public override void CollapseAll()
        {
            base.CollapseAll();

            if (detectors != null)
                foreach (Base item in detectors)
                    item.CollapseAll();
        }
    }
}
