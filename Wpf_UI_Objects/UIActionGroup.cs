using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UIActionGroup : UIAction
    {
        ObservableCollection<UIAction> actions;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (actions != null)
            {
                for (int i = actions.Count - 1; i >= 0; i--)
                {
                    UIAction action = actions[i];
                    actions.RemoveAt(i);
                    action.Dispose();
                }

                actions = null;
            }
        }

        public bool AddAction(UIAction _action)
        {
            bool result = false;

            if (_action.Name == "")
            {
                string name = "Action";

                if (_action is UIRelayAction)
                    name = "Relay";
                else if (_action is UIEmailAction)
                    name = "Email";
                else if (_action is UISiteInstructionAction)
                    name = "Step";
                else if (_action is UIPluginAction)
                    name = "PluginAction";
                else if (_action is UISirenAction)
                    name = "Siren";
                else if (_action is UIAutoResolveAction)
                    name = "Auto";

                _action.Name = UniqueActionName(this, name);
                this.Actions.Add(_action);
                result = true;
            }
            else
            {
                bool found = false;

                foreach (UIAction action in Actions)
                {
                    if (action.Name == _action.Name)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    this.Actions.Add(_action);
                    result = true;
                }
            }

            return result;
        }

        public static string UniqueActionName(UIActionGroup _group, string _prefix = "")
        {
            string result = "";
            if (_prefix == "")
                _prefix = "Action";

            if (_group.Actions.Count > 0)
            {
                int actionsCount = _group.Actions.Count;
                string tmpName = _prefix + (++actionsCount).ToString();
                                
                bool unique = false;
                while (!unique)
                {
                    bool found = false;

                    foreach (UIAction action in _group.Actions)
                    {
                        if (action.Name == tmpName)
                        {
                            found = true;
                            break;
                        }
                    }

                    if (!found)
                    {
                        unique = true;
                        break;
                    }
                    else
                    {
                        tmpName = _prefix + (++actionsCount).ToString(); 
                    }
                }

                result = tmpName;
            }
            else
            {
                result = _prefix + "1";
            }

            return result;
        }

        public ObservableCollection<UIAction> Actions
        {
            get
            {
                if (actions == null)
                    actions = new ObservableCollection<UIAction>();

                return actions;
            }
            set { actions = value; PropertyChangedHandler("Actions"); }
        }

        public override void ExpandAll()
        {
            base.ExpandAll();

            if (actions != null)
                foreach (Base item in actions)
                    item.ExpandAll();
        }

        public override void CollapseAll()
        {
            base.CollapseAll();

            if (actions != null)
                foreach (Base item in actions)
                    item.CollapseAll();
        }
    }
}
