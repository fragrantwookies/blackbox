using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UISirenAction : UIAction
    {
        private string soundFile;

        public string SoundFile
        {
            get { return soundFile; }
            set { soundFile = value; PropertyChangedHandler("SoundFile"); }
        }

        private double duration;

        public double Duration
        {
            get { return duration; }
            set { duration = value; PropertyChangedHandler("Duration"); }
        }
    }
}
