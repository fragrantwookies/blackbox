using System;
using System.Collections.Generic;
using System.Linq;
using eThele.Essentials;
using eNervePluginInterface;
using System.Collections.ObjectModel;
using eNerve.DataTypes;
using System.Windows;
namespace WPF_UI
{
    public class UICustomPropertyList : UICustomProperty, IEquatable<UICustomPropertyList>
    {   
        public UICustomPropertyList(eNervePluginInterface.CustomPropertyDefinition property)
            : base(property)
        {
            properties = new ObservableCollection<UICustomProperty>();

            foreach(CustomPropertyDefinition def in property.ChildDefinitions)
            {
                if (def.PropertyType == typeof(CustomPropertyList))
                    Properties.Add(new UICustomPropertyList(def));
                else
                    Properties.Add(new UICustomProperty(def));
            }
        }
        public UICustomPropertyList(eThele.Essentials.XmlNode xmlProperty)
            : base(xmlProperty)
        {
            properties = new ObservableCollection<UICustomProperty>();

            if (xmlProperty.Children.Count > 0)
            {
                foreach(XmlNode child in xmlProperty)
                {
                    if (child.Children.Count > 0)
                        properties.Add(new UICustomPropertyList(child));
                    else
                        properties.Add(new UICustomProperty(child));
                }
            }
        }
        public UICustomPropertyList(string name, string value)
            : base(name, value)
        {
            properties = new ObservableCollection<UICustomProperty>();
        }

        protected override void Dispose(bool disposing)
        {
            Clear();

            base.Dispose(disposing);
        }

        private ObservableCollection<UICustomProperty> properties;

        public ObservableCollection<UICustomProperty> Properties
        {
            get
            {
                if (properties == null)
                    properties = new ObservableCollection<UICustomProperty>();

                return properties;
            }
            set
            {
                properties = value;
            }
        }
        public override void ExpandAll()
        {
            base.ExpandAll();

            if (Properties != null)
                foreach (Base item in Properties)
                    item.ExpandAll();
        }

        public override void CollapseAll()
        {
            base.CollapseAll();

            if (Properties != null)
                foreach (Base item in Properties)
                    item.CollapseAll();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj as Base);
        }

        public bool Equals(UICustomPropertyList other)
        {
            return base.Equals(other);
        }

        public void Remove(UICustomProperty _child)
        {
            if (Application.Current != null)
            {
                if (Application.Current.Dispatcher.CheckAccess())
                {
                    if (properties != null && properties.Count > 0)
                        properties.Remove(_child); 
                }
                else
                    Application.Current.Dispatcher.Invoke(() => Remove(_child));
            }
        }

        public void Clear()
        {
            if (Application.Current != null)
            {
                if (Application.Current.Dispatcher.CheckAccess())
                {
                    if (properties != null && properties.Count > 0)
                    {
                        for (int i = properties.Count - 1; i >= 0; i--)
                        {
                            UICustomProperty prop = properties[i];

                            properties.RemoveAt(i);

                            prop.Dispose();
                        }
                    }
                }
                else
                    Application.Current.Dispatcher.Invoke(Clear);
            }
        }
    }
}
