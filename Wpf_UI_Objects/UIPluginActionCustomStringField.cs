using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UIPluginActionCustomStringField : UIPluginActionCustomBaseField
    {
        private string fieldValue = "";

        public string FieldValue
        {
            get { return fieldValue; }
            set { fieldValue = value; PropertyChangedHandler("FieldValue"); }
        }
    }
}