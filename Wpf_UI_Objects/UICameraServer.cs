﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;

using eNerve.DataTypes;
using eNervePluginInterface;

namespace WPF_UI
{
    [Serializable]
    public class UICameraServer : Base
    {
        private string ipAddress;
        private string deviceType;
        private bool isEnabled;
        private bool isPlugin;
        private INervePlugin pluginType;

        private ObservableCollection<INervePlugin> pluginTypes;

        private ObservableCollection<UICamera> cameras;
        private ObservableCollection<UICustomProperty> properties;

        public string DeviceType
        {
            get { return deviceType; }
            set { deviceType = value; PropertyChangedHandler("DeviceType"); }
        }

        public string IpAddress { get { return ipAddress; } set { ipAddress = value; PropertyChangedHandler("IpAddress"); } }

        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value; PropertyChangedHandler("IsEnabled");
                //AddProperty(new UICustomProperty("IsEnabled", value.ToString()) { Display = false });
            }
        }

        public bool IsPlugin
        {
            get { return isPlugin; }
            set
            {
                isPlugin = value; PropertyChangedHandler("IsPlugin");
                AddProperty(new UICustomProperty("IsPlugin", value.ToString()) { Display = false });
            }
        }
        public string ServerName { get { return Name; } set { Name = value; PropertyChangedHandler("ServerName"); } }

        public ObservableCollection<UICamera> Cameras { get { if (cameras == null) cameras = new ObservableCollection<UICamera>(); return cameras; } set { cameras = value;  PropertyChangedHandler("Cameras"); } }
        
        private void AddProperty(UICustomProperty property)
        {
            if (property != null)
            {
                if (properties == null)
                {
                    properties = new ObservableCollection<UICustomProperty>();
                    properties.Add(property);
                }
                if (properties.Count == 0)
                {
                    properties.Add(property);
                }
                else
                {
                    var index = properties.IndexOf(property);
                    if (index >= 0)
                        properties[index].Value = property.Value;
                    else
                        properties.Add(property);
                }
            }
        }

        public override void CollapseAll()
        {
            base.CollapseAll();

            if (cameras != null)
                foreach (Base item in cameras)
                    item.CollapseAll();
        }
        public override void ExpandAll()
        {
            base.ExpandAll();

            if (cameras != null)
                foreach (Base item in cameras)
                    item.ExpandAll();
        }

        public INervePlugin PluginType
        {
            get { return pluginType; }
            set
            {
                pluginType = value;
                if (value == null)
                {
                    AddProperty(new UICustomProperty("PluginType", "") { Display = false });
                }
                else
                {
                    AddProperty(new UICustomProperty("PluginType", value.Name) { Display = false });
                }
                PropertyChangedHandler("PluginType");
            }
        }

        public ObservableCollection<INervePlugin> PluginTypes
        {
            get { return pluginTypes; }
            set
            {
                pluginTypes = value;
                PropertyChangedHandler("PluginTypes");
            }
        }

        public ObservableCollection<UICustomProperty> Properties
        {
            get
            {
                if (properties == null)
                    properties = new ObservableCollection<UICustomProperty>();
                return properties;
            }
            set { properties = value; PropertyChangedHandler("Properties"); }
        }

        public override void RemoveChild(Base _child)
        {
            base.RemoveChild(_child);

            if (_child is UICamera && cameras != null)
            {
                cameras.Remove((UICamera)_child);
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            ClearCameras();
        }

        private void ClearCameras()
        {
            if (cameras != null)
            {
                if (Application.Current != null)
                {
                    if (Application.Current.Dispatcher.CheckAccess())
                    {
                        for (int i = cameras.Count - 1; i >= 0; i--)
                        {
                            Base camera = cameras[i];
                            cameras.RemoveAt(i);
                            camera.Dispose();
                        }
                    }
                    else
                        Application.Current.Dispatcher.Invoke(ClearCameras);
                }
            }
        }
    }
}
