using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UISiteInstructionAction : UIAction
    {
        string description;
        bool completeToResolve = false;

        public string Description
        {
            get { return description; }
            set { description = value; PropertyChangedHandler("Description"); }
        }

        public bool CompleteToResolve
        {
            get { return completeToResolve; }
            set { completeToResolve = value; PropertyChangedHandler("CompleteToResolve"); }
        }
    }
}
