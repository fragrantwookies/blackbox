using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UIRelay : UIIOItem
    {
        public int RelayIndex
        {
            get { return Index; }
            set { Index = value; PropertyChangedHandler("RelayIndex"); }
        }
    }
}
