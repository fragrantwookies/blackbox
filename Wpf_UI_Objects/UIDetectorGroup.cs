using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace WPF_UI
{
    [Serializable]
    public class UIDetectorGroup : Base
    {
        private int groupIndex;

        private ObservableCollection<UIIOItem> ios;

        private ObservableCollection<UITrigger> triggers;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            ClearIOs();
            ClearTriggers();
        }

        private void ClearIOs()
        {
            if (ios != null)
            {
                if (Application.Current != null)
                {
                    if (Application.Current.Dispatcher.CheckAccess())
                    {
                        for (int i = ios.Count - 1; i >= 0; i--)
                        {
                            Base io = ios[i];
                            ios.RemoveAt(i);
                            io.Dispose();
                        }
                    }
                    else
                        Application.Current.Dispatcher.Invoke(ClearIOs);
                }
            }
        }

        public override void RemoveChild(Base _child)
        {
            base.RemoveChild(_child);

            UIIOItem child = _child as UIIOItem;

            if (child != null && ios != null)
            {
                ios.Remove(child);
            }
        }

        private void ClearTriggers()
        {
            if (triggers != null)
            {
                if (Application.Current != null)
                {
                    if (Application.Current.Dispatcher.CheckAccess())
                        triggers.Clear();
                    else
                        Application.Current.Dispatcher.Invoke(ClearTriggers);
                }
            }
        }

        public string GroupName
        {
            get { return Name; }
            set { Name = value; PropertyChangedHandler("GroupName"); }
        }

        public int GroupIndex
        {
            get { return groupIndex; }
            set { groupIndex = value; PropertyChangedHandler("GroupIndex"); }
        }

        public ObservableCollection<UIIOItem> IOs
        {
            get
            {
                if (ios == null)
                    ios = new ObservableCollection<UIIOItem>();

                return ios;
            }
            set { ios = value; PropertyChangedHandler("IOs"); }
        }

        public ObservableCollection<UITrigger> Triggers
        {
            get
            {
                if (triggers == null)
                    triggers = new ObservableCollection<UITrigger>();

                return triggers;
            }
            set { triggers = value; PropertyChangedHandler("Triggers"); }
        }

        public override void ExpandAll()
        {
            base.ExpandAll();

            if (ios != null)
                foreach (Base item in ios)
                    item.ExpandAll();
        }

        public override void CollapseAll()
        {
            base.CollapseAll();

            if (ios != null)
                foreach (Base item in ios)
                    item.CollapseAll();
        }
    }
}
