using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace WPF_UI
{
    public class UICondition : Base
    {
        private string hiddenField;
        private string description;
        private int timeFrame;
        private int cooldown;
        private bool causeAlarm;
        private double percentageTriggersNeeded;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            ClearTriggers();
        }

        private void ClearTriggers()
        {
            if (triggers != null)
            {
                if (Application.Current != null)
                {
                    if (Application.Current.Dispatcher.CheckAccess())
                        triggers.Clear();
                    else
                        Application.Current.Dispatcher.Invoke(ClearTriggers);
                }
            }
        }

        public string HiddenField
        {
            get { return hiddenField; }
            set { hiddenField = value; PropertyChangedHandler("HiddenField"); }
        }

        public string Description
        {
            get { return description; }
            set { description = value; PropertyChangedHandler("Description"); }
        }

        public int TimeFrame
        {
            get { return timeFrame; }
            set { timeFrame = value; PropertyChangedHandler("TimeFrame"); }
        }

        public int Cooldown
        {
            get { return cooldown; }
            set { cooldown = value; PropertyChangedHandler("Cooldown"); }
        }

        public bool CauseAlarm
        {
            get { return causeAlarm; }
            set { causeAlarm = value; PropertyChangedHandler("CauseAlarm"); }
        }

        public double PercentageTriggersNeeded
        {
            get { return percentageTriggersNeeded; }
            set { percentageTriggersNeeded = value; PropertyChangedHandler("PercentageTriggersNeeded"); }
        }

        private ObservableCollection<UITrigger> triggers;

        public ObservableCollection<UITrigger> Triggers
        {
            get
            {
                if (triggers == null)
                    triggers = new ObservableCollection<UITrigger>();

                return triggers;
            }
            set { triggers = value; PropertyChangedHandler("Triggers"); }
        }
    }
}
