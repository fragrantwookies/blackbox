using System;
using System.Collections.Generic;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UIRelayAction : UIAction
    {
        private string relayName;
        private string deviceName;
        private double duration;

        public string RelayName
        {
            get { return relayName; }
            set { relayName = value; PropertyChangedHandler("RelayName"); }
        }

        public string DeviceName
        {
            get { return deviceName; }
            set { deviceName = value; PropertyChangedHandler("DeviceName"); }
        }

        public double Duration
        {
            get { return duration; }
            set { duration = value; PropertyChangedHandler("Duration"); }
        }
    }
}
