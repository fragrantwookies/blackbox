using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace WPF_UI
{
    [Serializable]
    public class UIAlarmName : Base, IEquatable<UIAlarmName>
    {
        private string hiddenField;
        private int priority = 1;

        private ObservableCollection<UIAction> actions;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (actions != null)
            {
                for (int i = actions.Count - 1; i >= 0; i--)
                {
                    UIAction action = actions[i];
                    actions.RemoveAt(i);
                    action.Dispose();
                }
            }
        }

        public string HiddenField
        {
            get { return hiddenField; }
            set { hiddenField = value; PropertyChangedHandler("HiddenField"); }
        }

        public int Priority
        {
            get { return priority; }
            set { priority = value; PropertyChangedHandler("Priority"); }
        }

        public ObservableCollection<UIAction> Actions
        {
            get
            {
                if (actions == null)
                    actions = new ObservableCollection<UIAction>();

                return actions;
            }
            set { actions = value; PropertyChangedHandler("Actions"); }
        }

        public bool Equals(UIAlarmName other)
        {
            return this.Name.Equals(other.Name);
        }

        public override void ExpandAll()
        {
            base.ExpandAll();

            if (actions != null)
                foreach (Base item in actions)
                    item.ExpandAll();
        }

        public override void CollapseAll()
        {
            base.CollapseAll();

            if (actions != null)
                foreach (Base item in actions)
                    item.CollapseAll();
        }
    }
}
