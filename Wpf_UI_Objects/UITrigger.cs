using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WPF_UI
{
    [Serializable]
    public class UITrigger : System.ComponentModel.INotifyPropertyChanged, IEquatable<UITrigger>
    {
        private string deviceName;
        private string detectorName;
        private int triggersToRaiseEvent = 1;
        private double triggersToRaiseEventTimeFrame = 0;
        private bool trueState = true;
        private bool falseState = false;
        private double minThreshold = 0;
        private double maxThreshold = 0;
        private bool isAnalog = false;
        private bool mandatory = false;

        [NonSerialized]
        private UICondition condition;

        public string DeviceName
        {
            get { return deviceName; }
            set { deviceName = value; PropertyChangedHandler("DeviceName"); }
        }

        public string DetectorName
        {
            get { return detectorName; }
            set { detectorName = value; PropertyChangedHandler("DetectorName"); }
        }

        public int TriggersToRaiseEvent
        {
            get { return triggersToRaiseEvent; }
            set { triggersToRaiseEvent = value; PropertyChangedHandler("TriggersToRaiseEvent"); }
        }

        public double TriggersToRaiseEventTimeFrame
        {
            get { return triggersToRaiseEventTimeFrame; }
            set { triggersToRaiseEventTimeFrame = value; PropertyChangedHandler("TriggersToRaiseEventTimeFrame"); }
        }

        public bool TrueState
        {
            get { return trueState; }
            set { trueState = value; PropertyChangedHandler("TrueState"); }
        }

        public bool FalseState
        {
            get { return falseState; }
            set { falseState = value; PropertyChangedHandler("FalseState"); }
        }

        public double MinThreshold
        {
            get { return minThreshold; }
            set { minThreshold = value; PropertyChangedHandler("MinThreshold"); }
        }

        public double MaxThreshold
        {
            get { return maxThreshold; }
            set { maxThreshold = value; PropertyChangedHandler("MaxThreshold"); }
        }

        public bool IsAnalog
        {
            get { return isAnalog; }
            set { isAnalog = value; PropertyChangedHandler("IsAnalog"); }
        }

        public bool Mandatory
        {
            get { return mandatory; }
            set { mandatory = value; PropertyChangedHandler("Mandatory"); }
        }

        public UICondition Condition
        {
            get { return condition; }
            set { condition = value; }
        }

        [field: NonSerialized]
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        protected void PropertyChangedHandler(string _propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(_propertyName));
        }

        public bool Equals(UITrigger other)
        {
            return this.DeviceName.Equals(other.DeviceName) && this.DetectorName.Equals(other.DetectorName);
        }
    }
}
