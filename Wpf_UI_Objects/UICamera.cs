using System;
using System.Collections.ObjectModel;

using eThele.Essentials;

namespace WPF_UI
{
    [Serializable]
    public class UICamera : UIIOItem
    {
        private bool isAdmin;
        private string ipAddress;
        private bool isEnabled;
        private string snapshot;
        private string preCurrPost;
        private bool dewarp;
        private string username;
        private string password;
        private string displayName;

        private ObservableCollection<UICustomProperty> properties;

        public ObservableCollection<UICustomProperty> Properties
        {
            get
            {
                if (properties == null)
                    properties = new ObservableCollection<UICustomProperty>();
                return properties;
            }
            set { properties = value; PropertyChangedHandler("Properties"); }
        }

        public bool Admin {
            get { return isAdmin; }
            set { isAdmin = value; PropertyChangedHandler("Admin"); }
        }

        public string DisplayName
        {
            get { return displayName; }
            set { displayName = value;  PropertyChangedHandler("DisplayName"); }
        }

        public string IPAddress
        {
            get { return ipAddress; }
            set { ipAddress = value; PropertyChangedHandler("IpAddress"); }
        }

        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value; PropertyChangedHandler("IsEnabled");
            }
        }

        public string Snapshot
        {
            get { return snapshot; }
            set { snapshot = value; PropertyChangedHandler("Snapshot"); }
        }

        public string PreCurrPost
        {
            get { return preCurrPost; }
            set { preCurrPost = value; PropertyChangedHandler("PreCurrPost"); }
        }

        public bool Dewarp
        {
            get { return dewarp; }
            set { dewarp = value; PropertyChangedHandler("Dewarp"); }
        }

        public string Username
        {
            get { return username; }
            set { username = value; PropertyChangedHandler("Username"); }
        }

        public string Password
        {
            get { return password; }
            set { password = value; PropertyChangedHandler("Password"); }
        }
    }
}
