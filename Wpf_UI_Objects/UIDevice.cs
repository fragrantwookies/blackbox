using eNerve.DataTypes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

using eNervePluginInterface;
using eThele.Essentials;
using System.Reflection;

namespace WPF_UI
{
    [Serializable]
    public class UIDevice : Base
    {
        private string macAddress;
        private string deviceType;
        private bool isEnabled;
        private bool isPlugin;
        
        [NonSerialized]
        [NonSerializedXML]
        private INervePlugin pluginType;

        [NonSerialized]
        [NonSerializedXML]
        private ObservableCollection<INervePlugin> pluginTypes;

        private ObservableCollection<UIDetectorGroup> groups;
        private ObservableCollection<UICustomProperty> properties;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            ClearProperties();

            ClearGroups();
        }

        private void ClearProperties()
        {
            if (Application.Current != null)
            {
                if (Application.Current.Dispatcher.CheckAccess())
                {
                    if (properties != null)
                    {
                        for (int i = properties.Count - 1; i >= 0; i--)
                        {
                            IDisposable prop = properties[i];
                            properties.RemoveAt(i);
                            prop.Dispose();
                        }
                    }
                }
                else
                    Application.Current.Dispatcher.Invoke(ClearProperties);
            }
        }

        private void ClearGroups()
        {
            if (groups != null)
            {
                if (Application.Current != null)
                {
                    if (Application.Current.Dispatcher.CheckAccess())
                    {
                        for (int i = groups.Count - 1; i >= 0; i--)
                        {
                            UIDetectorGroup group = groups[i];
                            groups.RemoveAt(i);
                            group.Dispose();
                        }
                    }
                    else
                        Application.Current.Dispatcher.Invoke(ClearGroups);
                }
            }
        }

        public override void RemoveChild(Base _child)
        {
            base.RemoveChild(_child);

            if (_child is UIDetectorGroup && groups != null)
            {
                groups.Remove((UIDetectorGroup)_child);
            }
        }

        public string DeviceAddress
        {
            get { return macAddress; }
            set { macAddress = value; PropertyChangedHandler("DeviceAddress"); }
        }

        public string DeviceType
        {
            get { return deviceType; }
            set { deviceType = value; PropertyChangedHandler("DeviceType"); }
        }

        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value; PropertyChangedHandler("IsEnabled");
                //AddProperty(new UICustomProperty("IsEnabled", value.ToString()) { Display = false });
            }
        }

        public bool IsPlugin
        {
            get { return isPlugin; }
            set 
            { 
                isPlugin = value; PropertyChangedHandler("IsPlugin");
                AddProperty(new UICustomProperty("IsPlugin", value.ToString()) { Display = false });
            }
        }

        public INervePlugin PluginType
        {
            get { return pluginType; }
            set 
            { 
                pluginType = value;
                if (value == null)
                {
                    AddProperty(new UICustomProperty("PluginType", "") { Display = false });
                }
                else
                {
                    AddProperty(new UICustomProperty("PluginType", value.Name) { Display = false });
                }
                PropertyChangedHandler("PluginType");
            }
        }

        public ObservableCollection<INervePlugin> PluginTypes
        { 
            get { return pluginTypes; }
            set 
            { 
                pluginTypes = value;
                PropertyChangedHandler("PluginTypes");
            } 
        }

        public ObservableCollection<UIDetectorGroup> Groups
        {
            get
            {
                if (groups == null)
                    groups = new ObservableCollection<UIDetectorGroup>();

                return groups;
            }
            set { groups = value; PropertyChangedHandler("Groups"); }
        }

        public ObservableCollection<UICustomProperty> Properties
        {
            get
            {
                if (properties == null)
                    properties = new ObservableCollection<UICustomProperty>();
                return properties;
            }
            set { properties = value; PropertyChangedHandler("Properties"); }
        }

        public override void ExpandAll()
        {
            base.ExpandAll();

            if (groups != null)
                foreach (Base item in groups)
                    item.ExpandAll();

            if(Properties != null)
                foreach (Base item in Properties)
                    item.ExpandAll();
        }

        public override void CollapseAll()
        {
            base.CollapseAll();

            if (groups != null)
                foreach (Base item in groups)
                    item.CollapseAll();

            if (Properties != null)
                foreach (Base item in Properties)
                    item.CollapseAll();
        }

        private void AddProperty(UICustomProperty property)
        {
            if (property != null)
            {
                if (properties == null)
                {
                    properties = new ObservableCollection<UICustomProperty>();
                    properties.Add(property);
                }
                if (properties.Count == 0)
                {
                    properties.Add(property);
                }
                else
                {
                    var index = properties.IndexOf(property);
                    if (index >= 0)
                        properties[index].Value = property.Value;
                    else
                        properties.Add(property);
                }
            }
        }
    }
}
